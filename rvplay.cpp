/*
 * Description: Display video sequences.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2010-01-27 17:32:40 $
 * Revision   : $Revision: 1.160 $
 * Copyright  : (c) 2005 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <sys/time.h>
#include <sys/stat.h>
#include "getopt.h"

#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <X11/XKBlib.h>

#include "rvutil.h"
#include "rvdisplay.h"
#include "rvy4m.h"
#include "rvstat.h"
#include "rvimage.h"
#include "rvlogfile.h"
#include "rvheader.h"

#include "rvh261.h"
#include "rvm2v.h"
#include "ludh264/ludh264.h"

#ifdef HAVE_LIBDE265
#include "libde265/libde265/de265.h"
#endif

/* locally decrease RV_MAXFILES to save memory */
#undef RV_MAXFILES
#define RV_MAXFILES 8

#define NUMSAMPLES 10001/* number of samples taken for timing statistics */
#define MAXFRAMES 10000 /* maximum number of frames to index */
#define MAXZOOM       8 /* maximum zoom level to protect X server */

const char *appname;
int verbose;

void usage(int exitcode)
{
    fprintf(stderr, "%s: raw yuv player\n", appname);
    fprintf(stderr, "usage: %s [options] [<file>] [<file>...]\n", appname);
    fprintf(stderr, "  -s, --size          : image size format\n");
    fprintf(stderr, "     (formats : %s)\n", rvformats);
    fprintf(stderr, "  -p, --fourcc        : image pixel format fourcc (default: I420)\n");
    fprintf(stderr, "     (formats : %s)\n", rvfourccs);
    fprintf(stderr, "  -m, --monochrome    : image is monochrome, i.e. fourcc=Y800\n");
    fprintf(stderr, "  -f, --fps           : framerate (default: 25.0)\n");
    fprintf(stderr, "  -z, --zoom          : zoom image by integer factor (default: 1)\n");
    fprintf(stderr, "  -g, --gain          : gain (multiplier) in difference image (default: 1)\n");
    fprintf(stderr, "  -y, --analyser      : analyser mode, start in single step with overlay\n");
    fprintf(stderr, "  -x, --exit-at-end   : exit at end of sequence\n");
    fprintf(stderr, "  -F, --follow        : follow file at end of sequence\n");
    fprintf(stderr, "  -d, --diff          : start playing in diff mode\n");
    fprintf(stderr, "  -t, --threshold     : start playing in threshold mode\n");
    fprintf(stderr, "  -l, --trace-file    : logfile to parse for overlay image\n");
    fprintf(stderr, "  -n, --mbaff         : use macroblock adaptive frame/field numbering\n");
    fprintf(stderr, "  -r, --refidct       : use reference (floating point) inverse dct (default: fast dct)\n");
    fprintf(stderr, "  -o, --output        : write output to file\n");
    fprintf(stderr, "  -q, --quiet         : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "  -v, --verbose       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "  --                  : disable argument processing\n");
    fprintf(stderr, "  -h, --help, --usage : print this usage message\n");
    exit(exitcode);
}

struct timeval timevaladd(struct timeval a, int b)
{
    a.tv_usec += b;
    while (a.tv_usec>1000000) {
        a.tv_sec++;
        a.tv_usec-=1000000;
    }
    return a;
}

int timevalsub(struct timeval a, struct timeval b)
{
    a.tv_sec  -= b.tv_sec;
    a.tv_usec -= b.tv_usec;
    /* watch for overflow */
    if (a.tv_sec<-2147)
        return -1<<31;
    else if (a.tv_sec>2147)
        return -((-1<<31)+1);
    else
        return a.tv_sec*1000000+a.tv_usec;
}

int read_frame_planar(unsigned char *frame, int width, int height, int hsub, int vsub, int bpp, int bufwidth, int bufheight, struct bitbuf *bb)
{
    int i;
    int bytes = 0;
    const int pelsize = (bpp+7)/8;

    //rvmessage("width=%d height=%d hsub=%d vsub=%d bpp=%d bufwidth=%d bufheight=%d", width, height, hsub, vsub, bpp, bufwidth, bufheight);

    /* check that file can be read */
    if (eofbits(bb))
        return -1;

    /* read next frame from file */

    /* read luma plane */
    for (i=0; i<height; i++)
        bytes += readbits(bb, frame+i*bufwidth*pelsize, width*pelsize);
    frame += bufwidth*pelsize*bufheight;

    if (hsub && vsub) {

        /* NOTE numplanes might be 2 or 3, but so far all pixel formats have
         * the same overall size for chroma data, so it doesn't matter */

        /* chroma subsampling */
        width /= hsub;
        bufwidth /= hsub;
        height /= vsub;
        bufheight /= vsub;

        for (i=0; i<height; i++)
            bytes += readbits(bb, frame+i*bufwidth*pelsize, width*pelsize);
        frame += bufwidth*pelsize*bufheight;
        for (i=0; i<height; i++)
            bytes += readbits(bb, frame+i*bufwidth*pelsize, width*pelsize);
    }

    return bytes;
}

int read_frame_packed(unsigned char *frame, int width, int height, fourcc_t fourcc, struct bitbuf *bb)
{
    //rvmessage("width=%d height=%d", width, height);

    /* check that file can be read */
    if (eofbits(bb))
        return -1;

    /* read next frame from file */
    int framesize = get_framesize(width, height, fourcc);
    return readbits(bb, frame, framesize);
}

void compare_frame(unsigned char *frame, unsigned char *ref, unsigned char *diff, int width, int height, int framesize, fourcc_t fourcc, int diffing, int gain)
{
    int j;

    /* no sense in comparing a frame with itself */
    if (frame==ref)
        return;

    switch (fourcc) {
        case I420:
        case YU12:
        case IYUV:
        case I422:
        case YU16:
            for (j=0; j<framesize; j++) {
                int chroma = j>=width*height;
                int d = abs(frame[j] - ref[j]);
                if (diffing==1) {
                    /* absolute difference with gain */
                    d = d*gain + (chroma? 128 : 0);
                    diff[j] = mmin(255,d);
                } else {
                    /* step function with infinite gain */
                    diff[j] = (d<gain)? (chroma? 128 : 0) : 255;
                }
            }
            break;

        case UYVY:
        case TWOVUY:
        case VYUY:
            for (j=0; j<framesize; j++) {
                int chroma = j%4==0 || j%4==2;
                int d = abs(frame[j] - ref[j]);
                if (diffing==1) {
                    /* absolute difference with gain */
                    d = d*gain + (chroma? 128 : 0);
                    diff[j] = mmin(255,d);
                } else {
                    /* step function with infinite gain */
                    diff[j] = (d<gain)? (chroma? 128 : 0) : 255;
                }
            }
            break;

        case YUY2:
            for (j=0; j<framesize; j++) {
                int chroma = j%4==1 || j%4==3;
                int d = abs(frame[j] - ref[j]);
                if (diffing==1) {
                    /* absolute difference with gain */
                    d = d*gain + (chroma? 128 : 0);
                    diff[j] = mmin(255,d);
                } else {
                    /* step function with infinite gain */
                    diff[j] = (d<gain)? (chroma? 128 : 0) : 255;
                }
            }
            break;

        case Y800:
        case RGB2:
            for (j=0; j<framesize; j++) {
                int d = abs(frame[j] - ref[j]);
                if (diffing==1) {
                    /* absolute difference with gain */
                    d = d*gain;
                    diff[j] = mmin(255,d);
                } else {
                    /* step function with infinite gain */
                    diff[j] = (d<gain)? 0 : 255;
                }
            }
            break;

    }
}

struct rv264macro *parse_logfile(const char *logfile, int width, int height, int numframes, int *mbaff)
{
    int i;
    const int nummacros = width*height/256;

    FILE *trace = fopen(logfile, "r");
    if (trace==NULL)
        rverror("failed to open log file \"%s\"", logfile);

    /* get sequence params from logfile */
    struct rv264seq_parameter_set sps;
    struct rv264pic_parameter_set pps;
    if (parse_jmlog_header(trace, &sps, &pps)<0)
        rvexit("failed to get sequence parameters");

    /* calculate picture (not frame) width and height from logfile params */
    int width_mbs  = (sps.pic_width_in_mbs_minus1 + 1);
    int height_mbs = (sps.pic_height_in_map_units_minus1 + 1);

    /* change to macroblock numbering if mbaff */
    *mbaff = sps.mb_adaptive_frame_field_flag;

    /* check logfile params are same as image dimensions */
    if (sps.mb_adaptive_frame_field_flag) {
        if ((width+15)/16!=width_mbs || (height+31)/32!=height_mbs)
            rvexit("logfile not compatible: display=%dx%d logfile=%dx%d", width, height, width_mbs*16, height_mbs*16);
    } else {
        if ((width+15)/16!=width_mbs || (height+15)/16!=height_mbs)
            rvexit("logfile not compatible: display=%dx%d logfile=%dx%d", width, height, width_mbs*16, height_mbs*16);
    }

    /*
    * nb: In the following we allocate an array of structs for all
    * macroblocks in the sequence and then parse the trace file for
    * every macroblock. This consumes a large amount of memory but
    * is the simplest method for handling flexible macroblock order,
    * arbitrary slice order and picture display order
    */

    /* allocate array of rv264macro structs */
    struct rv264macro *macro = (struct rv264macro *)rvalloc(NULL, numframes*nummacros*sizeof(struct rv264macro), 1);

    /* parse rest of trace file for all macroblocks */
    if (parse_jmlog_macros(trace, numframes*nummacros, macro, &sps, &pps)<0)
        rvexit("failed to parse trace file");

    /* sort macroblock array by pic_order_cnt and then by macroblock number */
    qsort(macro, numframes*nummacros, sizeof(struct rv264macro), seqsort);

    /* partially decode bitstream syntax */
    for (i=0; i<numframes; i++) {
        /* decode prev_intra_pred_mode_flag and rem_intra_pred_mode */
        decode_intra_pred_mode(nummacros, macro+i*nummacros, &sps, &pps);

        /* decode motion vector differences */
        decode_mvd(nummacros, macro+i*nummacros, &sps, &pps);
    }

    return macro;
}

int clampxoff(int xoff, int pixwidth, int winwidth)
{
    return mmin(mmax(xoff, 0), pixwidth-winwidth);
}

int clampyoff(int yoff, int pixheight, int winheight)
{
    return mmin(mmax(yoff, 0), pixheight-winheight);
}

int main(int argc, char *argv[])
{
    int i;
    char *logfile = NULL;
    int fileindex = 0;
    int numfiles = 0;
    int numframes = 0;
    long long timestats[NUMSAMPLES];

    /* file arrays */
    const char *filename[RV_MAXFILES] = {NULL};
    off_t *index[RV_MAXFILES] = {NULL};
    int lastframe[RV_MAXFILES] = {0};
    int nextframe[RV_MAXFILES] = {0};
    int framenum[RV_MAXFILES] = {0};
    int picturenum[RV_MAXFILES] = {0};
    struct bitbuf *bb[RV_MAXFILES] = {NULL};
    divine_t divine[RV_MAXFILES] = {{MISSING, 0}};
    int width[RV_MAXFILES] = {0};
    int height[RV_MAXFILES] = {0};
    fourcc_t fourcc[RV_MAXFILES] = {0};
    int hsub[RV_MAXFILES] = {0};
    int vsub[RV_MAXFILES] = {0};
    int bpp[RV_MAXFILES] = {0};
    size_t framesize[RV_MAXFILES] = {0};
    framerate_t framerate[RV_MAXFILES] = {0.0};

    /* data buffers */
    struct rvimage frame[RV_MAXFILES];
    struct rvimage field[RV_MAXFILES];
    struct rvimage diff[RV_MAXFILES];
    struct rvimage shift;
    double entropy[RV_MAXFILES] = {0.0};
    double mean[RV_MAXFILES] = {0.0};
    struct rvstats stats[RV_MAXFILES];

    /* decoded picture buffers */
    sample_t *decodedframe[RV_MAXFILES][MAXFRAMES] = {{NULL}};
    sample_t *referenceframe[RV_MAXFILES][2] = {{NULL, NULL}};
    struct rvm2vpicture *picturedata[RV_MAXFILES][2] = {{NULL, NULL}};  /* for reordering picture data for overlay */
    struct rvimage overlay[RV_MAXFILES];
    struct rvimage strength[RV_MAXFILES];

    /* command line defaults */
    int bufwidth = 0;
    int bufheight = 0;
    int bufhsub = 2;
    int bufvsub = 2;
    int bufbpp = 8;
    const char *format = NULL;
    const char *fccode = NULL;
    unsigned int zoom = 1;
    int gridsize = 16;
    framerate_t displayrate = 0.0;
    double speed = 1.0;
    int mbaff = 0;
    int refidct = 0;
    int analyser = 0;
    int waitatend = 1;
    int follow = 0;
    int diffing = 0;
    int gain = 1;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"size",        1, NULL, 's'},
            {"fourcc",      1, NULL, 'p'},
            {"monochrome",  0, NULL, 'm'},
            {"fps",         1, NULL, 'f'},
            {"zoom",        1, NULL, 'z'},
            {"gain",        1, NULL, 'g'},
            {"analyser",    0, NULL, 'y'},
            {"exit-at-end", 0, NULL, 'x'},
            {"follow",      0, NULL, 'F'},
            {"diff",        0, NULL, 'd'},
            {"threshold",   0, NULL, 't'},
            {"trace-file",  1, NULL, 'l'},
            {"mbaff",       0, NULL, 'n'},
            {"refidct",     0, NULL, 'r'},
            {"output",      1, NULL, 'o'},
            {"quiet",       0, NULL, 'q'},
            {"verbose",     0, NULL, 'v'},
            {"usage",       0, NULL, 'h'},
            {"help",        0, NULL, 'h'},
            {NULL,          0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "s:p:mb:f:z:g:yxFdtl:nro:qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 's':
                format = optarg;
                break;

            case 'p':
                fccode = optarg;
                /* convert fourcc string into integer */
                for (i=0; i<RV_MAXFILES; i++)
                    fourcc[i] = get_fourcc(fccode);
                break;

            case 'm':
                for (i=0; i<RV_MAXFILES; i++)
                    fourcc[i] = Y800;
                break;

            case 'f':
                displayrate = atof(optarg);
                if (displayrate<0.0001 || displayrate>1000.0)
                    rvexit("invalid value for frames per second: %f", displayrate);
                break;

            case 'z':
                zoom = atoi(optarg);
                if (zoom<1 || zoom>MAXZOOM)
                    rvexit("invalid value for zoom: %d", zoom);
                break;

            case 'g':
                gain = atoi(optarg);
                if (gain<1 || gain>255)
                    rvexit("invalid value for gain: %d", gain);
                break;

            case 'y':
                analyser = 1;
                break;

            case 'x':
                waitatend = 0;
                break;

            case 'F':
                follow = 1;
                break;

            case 'd':
                diffing = 1;
                break;

            case 't':
                diffing = 2;
                break;

            case 'l':
                logfile = optarg;
                break;

            case 'n':
                mbaff = 1;
                break;

            case 'r':
                refidct = 1;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < RV_MAXFILES)
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    /* read from filein if no files specified */
    if (numfiles==0)
        filename[numfiles++] = "-";

    /* initialise frame numbers */
    for (i=0; i<numfiles; i++)
        framenum[i] = -1;

    /* prepare to display each input file */
    struct rv261picture *picture[RV_MAXFILES] = {NULL};
    struct rvm2vsequence *sequence[RV_MAXFILES] = {NULL};
    struct rv264picture *pic = NULL;
    /* allocate h.264 bitstream buffer */
    const size_t ludsize = 2048*1024;
    uint8_t *luddata[RV_MAXFILES] = {NULL};
    size_t ludpos[RV_MAXFILES] = {0};
    struct rv264sequence *avc_sequence[RV_MAXFILES] = {NULL};
#ifdef HAVE_LIBDE265
    de265_decoder_context* decoder[RV_MAXFILES] = {NULL};
    const struct de265_image *de265_image[RV_MAXFILES] = {NULL};
    struct rvhevcvideo *video[RV_MAXFILES] = {NULL};
#endif
    FILE* fh[RV_MAXFILES] = {NULL};
    for (i=0; i<numfiles; i++) {
        /* prepare to parse file */
        bb[i] = initbits_filename(filename[i], B_GETBITS);
        if (bb[i]==NULL)
            rverror("failed to open file \"%s\"", filename[i]);

        /* determine filetype */
        divine[i] = divine_filetype(bb[i], filename[i], format, verbose);
        if (rewindbits(bb[i])<0)
            rverror("searched too far on non-seekable input file \"%s\"", filename[i]);

        if (verbose>=2)
            rvmessage("file \"%s\" has type %s", filename[i], filetypename[divine[i].filetype]);

        /* select what to do based on filetype */
        switch (divine[i].filetype) {
            case YUV:
                /* determine input file dimensions TODO support individual framerates */
                divine_image_dims(&width[i], &height[i], NULL, &framerate[i], NULL, format, filename[i]);

                /* override framerate width display rate from command line */
                if (displayrate>0.0)
                    framerate[i] = displayrate;

                /* determine input file chroma format */
                if (fourcc[i]==0)
                    fourcc[i] = divine_pixel_format(filename[i]);
                if (fourcc[i]==0)
                    fourcc[i] = I420;

                /* get image dimensions from command line as backup */
                if (!width[i] && !height[i]) {
                    width[i] = bufwidth;
                    height[i] = bufheight;
                }
                break;

            case YUV4MPEG:
                /* parse yuv4mpeg stream header */
                if (read_y4m_stream_header_bits(bb[i], &width[i], &height[i], &framerate[i], &fourcc[i])<0)
                    rverror("failed to read yuv4mpeg stream header");

                /* override framerate width display rate from command line */
                if (displayrate>0.0)
                    framerate[i] = displayrate;

                break;

            case H261:
                /* parse h.261 file */
                picture[i] = parse_h261(bb[i], filename[i], verbose);
                break;

            case M2V:
            {
                /* parse mpeg2 headers */
                sequence[i] = parse_m2v_headers(bb[i], verbose);
                if (sequence[i]==NULL) {
                    rvmessage("failed to parse headers in mpeg2 sequence \"%s\"", filename[i]);
                    break;
                }
                width[i]  = 16*sequence[i]->width_in_mbs;
                height[i] = 16*sequence[i]->height_in_mbs;
                framerate[i] = sequence[i]->frame_rate;
                switch (sequence[i]->chroma_format) {
                    case 3 : fourcc[i] = I444; break;
                    case 2 : fourcc[i] = I422; break;
                    case 1 : fourcc[i] = I420; break;
                }

                /* find first picture start code */
                if (find_m2v_next_picture(bb[i], sequence[i], 0, NULL, verbose)<0) {
                    rvmessage("failed to find first picture of mpeg2 sequence \"%s\"", filename[i]);
                    break;
                }

                /* parse first picture */
                picturedata[i][0] = parse_m2v_picture(bb[i], sequence[i], picturenum[i], verbose);
                if (picturedata[i][0]==NULL) {
                    rvmessage("failed to parse first picture of mpeg2 sequence \"%s\"", filename[i]);
                    break;
                } else
                    picturenum[i]++;

                /* decode first picture */
                sample_t *firstpic = decode_m2v(picturedata[i][0], sequence[i], NULL, NULL, 0, refidct);
                if (firstpic==NULL) {
                    rvmessage("failed to decode first picture of mpeg2 sequence \"%s\"", filename[i]);
                    break;
                }

                /* check if picture was field picture */
                if (field_picture(picturedata[i][0])) {
                    /* parse second picture */
                    picturedata[i][1] = parse_m2v_picture(bb[i], sequence[i], picturenum[i], verbose);
                    if (picturedata[i][1]==NULL) {
                        rvmessage("failed to parse second picture of mpeg2 sequence \"%s\"", filename[fileindex]);
                        break;
                    } else
                        picturenum[i]++;

                    /* prepare reference frame from just first field picture */
                    sample_t *refframe = copy_fields_into_frame(firstpic, firstpic, width[i], height[i], sequence[i]->chroma_format);

                    /* decode second picture */
                    sample_t *secondpic = decode_m2v(picturedata[i][1], sequence[i], NULL, refframe, 1, refidct);
                    if (secondpic==NULL) {
                        rvmessage("failed to decode second picture of mpeg2 sequence \"%s\"", filename[fileindex]);
                        break;
                    }
                    rvfree(refframe);

                    /* merge field pictures */
                    if (picturedata[i][1]->extension.picture_structure==PST_BOTTOM)
                        referenceframe[i][1] = copy_fields_into_frame(firstpic, secondpic, width[i], height[i], sequence[i]->chroma_format);
                    else
                        referenceframe[i][1] = copy_fields_into_frame(secondpic, firstpic, width[i], height[i], sequence[i]->chroma_format);
                    rvfree(firstpic);
                    rvfree(secondpic);
                } else
                    referenceframe[i][1] = firstpic;

                break;
            }

            case H264:
            {
                /* initialise decode library */
                avc_sequence[i] = alloc_264_sequence();
                ludh264_init(avc_sequence[i]);

                /* allocate input buffer */
                luddata[i] = (uint8_t *)rvalloc(NULL, (ludsize+4)*sizeof(uint8_t), 0);

                /* loop over input data chunks */
                bool eof = false;
                do {
                    if (!eof) {
                        size_t read = readbits(bb[i], luddata[i]+ludpos[i], ludsize-ludpos[i]);
                        if (read==0) {
                            /* check for errors */
                            if (errorbits(bb[i]))
                                rverror("failed to read from input");

                            /* otherwise check for end of file */
                            eof = eofbits(bb[i]);
                        }
                        ludpos[i] += read;
                    }

                    /* parse the bitstream for the next nal unit */
                    uint32_t consumed = ludpos[i];
                    uint8_t* buf;
                    uint32_t len;
                    RetCode r = ludh264_parse_bytestream_nalu(avc_sequence[i], luddata[i], &consumed, &buf, &len, eof);
                    if (r!=LUDH264_SUCCESS)
                        rvexit("failed to parse bytestream nal unit");

                    /* decode the nal unit */
                    r = ludh264_decode_nalu(avc_sequence[i], buf, len, NULL);
                    if (r!=LUDH264_SUCCESS)
                        rvexit("failed to decode nal unit");

                    /* track consumed bytes and remove them from the buffer */
                    ludpos[i] -= consumed;
                    if (consumed)
                        memmove(luddata[i], luddata[i] + consumed, ludpos[i]);

                    /* look for picture params available */
                    uint32_t w, h;
                    LUDH264_VIDEO_FORMAT format;
                    if (ludh264_get_picture_params(avc_sequence[i], &format, &w, &h)==LUDH264_SUCCESS) {

                        width[i] = w;
                        height[i] = h;
                        switch (format) {
                            case LUDH264_YUV420: fourcc[i] = I420; break;
                            case LUDH264_YUV422: fourcc[i] = I422; break;
                            case LUDH264_YUV444: fourcc[i] = I444; break;
                        }
                        framerate[i] = 25;

                        break;
                    }
                } while (ludpos[i] > 0 || eof==false);

                break;
            }

            case HEVC:
            {
#ifdef HAVE_LIBDE265
                decoder[i] = de265_new_decoder();

                /* configure de265 decoder */
                de265_set_parameter_bool(decoder[i], DE265_DECODER_PARAM_BOOL_SEI_CHECK_HASH, 1);
                de265_set_parameter_bool(decoder[i], DE265_DECODER_PARAM_SUPPRESS_FAULTY_PICTURES, false);
                //de265_set_parameter_bool(decoder[i], DE265_DECODER_PARAM_DISABLE_DEBLOCKING, disable_deblocking);
                //de265_set_parameter_bool(decoder[i], DE265_DECODER_PARAM_DISABLE_SAO, disable_sao);

                if (verbose>=2) {
                    de265_set_parameter_int(decoder[i], DE265_DECODER_PARAM_DUMP_SPS_HEADERS, 1);
                    de265_set_parameter_int(decoder[i], DE265_DECODER_PARAM_DUMP_VPS_HEADERS, 1);
                    de265_set_parameter_int(decoder[i], DE265_DECODER_PARAM_DUMP_PPS_HEADERS, 1);
                }
                if (verbose>=3) {
                    de265_set_parameter_int(decoder[i], DE265_DECODER_PARAM_DUMP_SLICE_HEADERS, 1);
                }
                if (0) {
                    de265_set_parameter_int(decoder[i], DE265_DECODER_PARAM_ACCELERATION_CODE, de265_acceleration_SCALAR);
                }
                if (0) {
                    //de265_set_limit_TID(decoder[i], highestTID);
                }

                de265_set_verbosity(verbose);
                if (verbose>=1)
                    rvmessage("libde265 v%s", de265_get_version());

                //err = de265_start_worker_threads(decoder[i], nThreads);

                /* open bitstream file */
                if (filename[i] && strcmp(filename[i], "-"))
                    fh[i] = fopen(filename[i], "rb");
                else
                    fh[i] = stdin;
                if (fh[i]==NULL)
                    rverror("cannot open bitstream file \"%s\"", filename[i]);

                /* decode the first hevc frame */
                de265_image[i] = de265_peek_next_picture(decoder[i]);
                while (!de265_image[i]) {

                    /* decode some more */
                    int more = 1;
                    de265_error err = de265_decode(decoder[i], &more);
                    if (more && de265_isOK(err))
                        de265_image[i] = de265_peek_next_picture(decoder[i]);
                    else if (more && err==DE265_ERROR_IMAGE_BUFFER_FULL)
                        de265_image[i] = de265_peek_next_picture(decoder[i]);
                    else if (more && err==DE265_ERROR_WAITING_FOR_INPUT_DATA) {
                        /* read a chunk of input data */
                        #define BUFFER_SIZE 64*1024
                        uint8_t buf[BUFFER_SIZE];
                        int n = fread(buf, 1, BUFFER_SIZE, fh[i]);
                        if (n) {
                            err = de265_push_data(decoder[i], buf, n, 0, NULL);
                            if (!de265_isOK(err)) {
                                rverror("failed to push hevc data to decoder: %s", de265_get_error_text(err));
                            }
                        }

                        if (feof(fh[i])) {
                            err = de265_flush_data(decoder[i]); // indicate end of stream
                        }
                    } else if (!more) {
                        /* decoding finished */
                        if (!de265_isOK(err))
                            rvmessage("error decoding frame: %s", de265_get_error_text(err));
                        break;
                    }
                }

                /* retrieve parameters from first frame */
                if (de265_image[i]) {
                    width[i]  = de265_get_image_width(de265_image[i],0);
                    height[i] = de265_get_image_height(de265_image[i],0);
                    framerate[i] = 25; // FIXME not sure how to extract this.
                    switch (de265_get_chroma_format(de265_image[i])) {
                        case 3 : fourcc[i] = I444; break;
                        case 2 : fourcc[i] = I422; break;
                        case 1 : fourcc[i] = I420; break;
                        case 0 : fourcc[i] = Y800; break;
                    }

                    /* initialise the hevc data struct */
                    video[i] = alloc_hevc_video();
#ifdef HAVE_LIBDE265
                    struct hevc_vid_parameter_set *vps = extract_hevc_vid_param(de265_image[i]);
                    video[i]->active_vps = vps->vid_parameter_set_id;
                    video[i]->vps[vps->vid_parameter_set_id] = vps;
                    struct hevc_seq_parameter_set *sps = extract_hevc_seq_param(de265_image[i]);
                    video[i]->active_sps = sps->seq_parameter_set_id;
                    video[i]->sps[sps->seq_parameter_set_id] = sps;
                    struct hevc_pic_parameter_set *pps = extract_hevc_pic_param(de265_image[i], video[i]->sps);
                    video[i]->active_pps = pps->pic_parameter_set_id;
                    video[i]->pps[pps->pic_parameter_set_id] = pps;
#endif

                    /* copy frame metadata for overlay buffer */
                    video[i]->picture[0] = alloc_hevc_picture(sps);
                    extract_hevc_slice_header(de265_image[i], video[i]->picture[0]->sh, video[i]->pps);
                    extract_hevc_ctus(de265_image[i], *video[i]->picture[0]);

                    /* set grid pitch to CTU size */
                    gridsize = video[i]->sps[video[i]->active_sps]->CtbSizeY;
                }

                break;
#else
                rvexit("hevc support is not compiled in, install libde265 and recompile");
#endif
            }

            case EMPTY:
                rvexit("empty file: \"%s\"", filename[i]);
                break;

            default:
                rvmessage("filetype not supported: %s", filetypename[divine[i].filetype]);
                break;
        }

        /* get chroma subsampling from fourcc */
        get_chromasub(fourcc[i], &hsub[i], &vsub[i]);
        if (hsub[i]==-1 || vsub[i]==-1)
            rvexit("unknown pixel format: %s", fourccname(fourcc[i]));

        /* get bits per pel from fourcc */
        bpp[i] = get_bitsperpel(fourcc[i]);
        if (bpp[i]<=0)
            rvexit("unknown pixel format: %s", fourccname(fourcc[i]));

        /* calculate framesize */
        framesize[i] = get_framesize(width[i], height[i], fourcc[i]);

        /* each image is displayed in a buffer the size of the largest image */
        if (width[i]>bufwidth)
            bufwidth = width[i];
        if (height[i]>bufheight)
            bufheight = height[i];
        if (hsub[i]<bufhsub && hsub[i])
            bufhsub = hsub[i];
        if (vsub[i]<bufvsub && vsub[i])
            bufvsub = vsub[i];
        if (bpp[i]>bufbpp)
            bufbpp = bpp[i];
        /* TODO individual frame rates */
        if (displayrate==0.0)
            displayrate = framerate[i]; // will be 0.
    }

    /* can't continue without image dimensions */
    if (!bufwidth || !bufheight)
        rvexit("unable to determine input image dimensions");
    if (verbose>=1) {
        rvmessage("display: width=%d height=%d hsub=%d vsub=%d bpp=%d fps=%.2f", bufwidth, bufheight, bufhsub, bufvsub, bufbpp, displayrate);
        for (i=0; i<numfiles; i++)
            rvmessage("file %2d: width=%d height=%d fourcc=%s", i, width[i], height[i], describe_fourcc(fourcc[i]));
    }

    /* allocate storage for file index */
    for (i=0; i<numfiles; i++)
        index[i] = (off_t *)rvalloc(NULL, MAXFRAMES*sizeof(off_t), 1);

    /* calculate buffer size */
    size_t bufframesize = get_framesize(bufwidth, bufheight, fourcc[0]);

    /* allocate frame buffer for raw video formats */
    for (i=0; i<numfiles; i++) {
        frame[i].init(bufwidth, bufwidth, bufheight, hsub[i], vsub[i], bpp[i]);
        /* zero frame buffer */
        frame[i].clear();
    }

    /* allocate field and difference buffers */
    for (i=0; i<numfiles; i++) {
        field[i].init(bufwidth, bufwidth, bufheight, hsub[i], vsub[i], bpp[i]);
        diff[i].init(bufwidth, bufwidth, bufheight, hsub[i], vsub[i], bpp[i]);
    }
    shift.init(bufwidth, bufwidth, bufheight, hsub[0], vsub[0], 8);

    /* initialise blank image data buffer */
    struct rvimage blank;
    if (bufhsub==0 && bufvsub==0)
        /* use a 4:2:0 blank for monochrome images */
        blank.init(bufwidth, bufwidth, bufheight, 2, 2, bufbpp);
    else
        blank.init(bufwidth, bufwidth, bufheight, bufhsub, bufvsub, bufbpp);
    blank.clear();

    /* initialise overlay and strength map image buffers */
    for (i=0; i<numfiles; i++) {
        overlay[i].init(bufwidth, bufwidth, bufheight, hsub[i], vsub[i], bpp[i]);
        strength[i].init(bufwidth, bufwidth, bufheight, hsub[i], vsub[i], bpp[i]);
    }

    /* parse logfile */
    unsigned int numoverlays = 0;
    struct rv264macro *macro = NULL;
    if (logfile) {
        /* stat all input files to find out maximum number of overlay frames */
        for (i=0; i<numfiles; i++) {
            struct stat st;
            if (stat(bb[i]->filename, &st)<0)
                rverror("failed to stat input file \"%s\"", filename[i]);
            if (st.st_size / framesize[i] > numoverlays)
                numoverlays = st.st_size / framesize[i];
        }

        if (verbose>=2)
            rvmessage("number of overlay frames=%d", numoverlays);

        macro = parse_logfile(logfile, bufwidth, bufheight, numoverlays, &mbaff);
    }

    /* open X display */
    Display *display = XOpenDisplay(NULL);
    if (display==NULL)
        rvexit("cannot open display");

    /* prepare window name */
    struct rvwin window;
    int len = snprintf(window.winname, sizeof(window.winname), "%s ", appname);
    for (i=0; i<numfiles; i++) {
        const char *p = get_basename(filename[i]);
        len += snprintf(window.winname+len, sizeof(window.winname)-len, "%s ", p);
    }

    /* initialise display */
    create_window(&window, display, numfiles*bufwidth*zoom, bufheight*zoom, verbose);

    /* create an ximage for RGB conversion */
    window.ximage = create_ximage(&window, bufwidth*zoom, bufheight*zoom);

    /* register event types to recieve */
    XSelectInput(display, window.window, KeyPressMask | ButtonPressMask | Button1MotionMask | ExposureMask | StructureNotifyMask);

    /* body of program */
    int playing = analyser? 0 : 1;
    int reverse = 0;
    int following = 0;
    int showgrid = logfile? 1 : 0;
    int shownums = 0;
    int showinfo = 0;
    int showpsnr = 0;
    int showstat = 0;
    int showprob = 0;
    int showqmat = 0;
    int showparm = 0;
    int showover = analyser? 1 : 0;
    int showmap  = 0;
    int drawmode = analyser? DRAWMODE_DCT | DRAWMODE_BAR | DRAWMODE_REF | DRAWMODE_VEC | DRAWMODE_INT | DRAWMODE_COL : 0;
    int showpels = 0;
    int showmark = 0;
    int showfield = 0;
    plane_t plane    = hsub[0] && vsub[0]? ALL_PLANES : LUMA_PLANE; // FIXME different plane[] for each sequence?

    /* inital comparison frame is right-hand frame */
    int comparison = numfiles-1;

    /* initial position of zoomed image in window */
    unsigned int xoff = 0;
    unsigned int yoff = 0;
    unsigned int xpos = 0;
    unsigned int ypos = 0;

    /* initial marked macroblock and pel */
    int xmark = -1;
    int ymark = -1;
    int xpel = 0;
    int ypel = 0;

    /* initial source image shift */
    int xshift = 0;
    int yshift = 0;

    /* fullscreen toggle */
    int fullscreen = 0;
    XWindowAttributes saved;

    /* initialise next frame presentation time to now */
    struct timeval now, next;
    if (gettimeofday(&next, NULL)<0)
        rverror("failed to get current time");

    /* work-around for when playing=0 at start */
    int first = 1;

    while (1)
    {
        /* don't assume each loop results in a display refresh */
        int refresh = 0;

        /* wait for more data if following a file */
        if (following)
            sleep(1);


        /* check for user input */
        XEvent xev;
        if (((playing || following) && XCheckMaskEvent(display, KeyPressMask, &xev)) ||
        (!playing && !following && !first && XNextEvent(display, &xev)==0)) {
            switch (xev.type) {
                case KeyPress:
                {
                    KeySym symbol = XkbKeycodeToKeysym(display, xev.xkey.keycode, 0, xev.xkey.state & ShiftMask ? 1 : 0);
                    //int shift = xev.xkey.state & ShiftMask;
                    switch (symbol) {
                        case XK_Return:
                        case XK_KP_Enter:
                            playing = 1;
                            reverse = 0;
                            for (i=0; i<numfiles; i++)
                                nextframe[i] = 0;
                            break;

                        case XK_Home:
                        case XK_Up:
                        case XK_a:
                        case XK_A:
                            playing = 0;
                            for (i=0; i<numfiles; i++)
                                nextframe[i] = 0;
                            break;

                        case XK_End:
                        case XK_Down:
                        case XK_e:
                        case XK_E:
                            playing = 0;
                            waitatend = 1;
                            /* this is only the last frame seen so far */
                            for (i=0; i<numfiles; i++)
                                nextframe[i] = lastframe[i];
                            break;

                        case XK_Left:
                        case XK_b:
                        case XK_B:
                        {
                            playing = 0;
                            /* try to resynchronise frame numbers */
                            int syncframe = 0;
                            for (i=0; i<numfiles; i++)
                                if (framenum[i]>syncframe)
                                    syncframe = framenum[i];
                            for (i=0; i<numfiles; i++)
                                if (framenum[i]>0)
                                    if (framenum[i]==syncframe)
                                        nextframe[i] = framenum[i]-1;
                            break;
                        }

                        case XK_Right:
                        case XK_F2:
                            playing = 0;
                            waitatend = 1;
                            for (i=0; i<numfiles; i++)
                                nextframe[i] = framenum[i]+1;
                            break;

                        case XK_Page_Up:
                            playing = 1;
                            for (i=0; i<numfiles; i++)
                                nextframe[i] = mmax(framenum[i]-lround(60.0*displayrate), 0);
                            break;

                        case XK_Page_Down:
                            playing = 1;
                            waitatend = 1;
                            for (i=0; i<numfiles; i++)
                                nextframe[i] = framenum[i]+lround(60.0*displayrate);
                            break;

                        case XK_KP_Add:
                        case XK_plus:
                        case XK_equal:
                            if (zoom<MAXZOOM) {
                                zoom++;
                                resize_pixmap(&window, numfiles*bufwidth*zoom, bufheight*zoom);
                                resize_ximage(&window, bufwidth*zoom, bufheight*zoom);
                                if ((numfiles*bufwidth*zoom>window.width) || (bufheight*zoom>window.height))
                                    resize_window(&window, numfiles*bufwidth*zoom, bufheight*zoom);
                                xoff = clampxoff(xoff, numfiles*bufwidth*zoom, window.width);
                                yoff = clampyoff(yoff, bufheight*zoom, window.height);
                                refresh = 1;
                            }
                            break;

                        case XK_KP_Subtract:
                        case XK_minus:
                            if (zoom>1) {
                                zoom--;
                                resize_pixmap(&window, numfiles*bufwidth*zoom, bufheight*zoom);
                                resize_ximage(&window, bufwidth*zoom, bufheight*zoom);
                                if ((numfiles*bufwidth*zoom<window.width) || (bufheight*zoom<window.height))
                                    resize_window(&window, numfiles*bufwidth*zoom, bufheight*zoom);
                                xoff = clampxoff(xoff, numfiles*bufwidth*zoom, window.width);
                                yoff = clampyoff(yoff, bufheight*zoom, window.height);
                                refresh = 1;
                            }
                            break;

                        case XK_f:
                        case XK_F:
                        {
                            struct Hints {
                                unsigned long   flags;
                                unsigned long   functions;
                                unsigned long   decorations;
                                long            inputMode;
                                unsigned long   status;
                            } hints;
                            hints.flags = 2; // specify that we're changing the window decorations.
                            Atom property = XInternAtom(display, "_MOTIF_WM_HINTS", True);
                            switch (fullscreen) {
                                case 0:
                                {
                                    XWindowAttributes root;
                                    Window child;
                                    /* save current window position */
                                    XGetWindowAttributes(display, window.window, &saved);
                                    XTranslateCoordinates(display, window.window, DefaultRootWindow(display), 0, 0, &saved.x, &saved.y, &child);
                                    /* disable window decorations */
                                    hints.decorations = 0;
                                    XChangeProperty(display, window.window, property, property, 32, PropModeReplace, (unsigned char *)&hints, 5);
                                    /* set window fullscreen */
                                    XGetWindowAttributes(display, DefaultRootWindow(display), &root);
                                    XMoveResizeWindow(display, window.window, 0, 0, root.width, root.height);
                                    XMapRaised(display, window.window);
                                    break;
                                }

                                case 1:
                                    /* return window to saved position */
                                    XMoveResizeWindow(display, window.window, saved.x, saved.y, saved.width, saved.height);
                                    /* enable window decorations */
                                    hints.decorations = 1;
                                    XChangeProperty(display, window.window, property, property, 32, PropModeReplace, (unsigned char *)&hints, 5);
                                    break;
                            }
                            fullscreen = !fullscreen;
                        }
                        break;

                        case XK_KP_Left:
                        case XK_KP_4:
                            if (diffing)
                                xshift--;
                            else if (showpels) {
                                if (xpel>0)
                                    xpel--;
                            } else if (showmark) {
                                if (xmark>0)
                                    xmark--;
                            } else {
                                if (xoff>16*zoom)
                                    xoff -= 16*zoom;
                                else
                                    xoff = 0;
                                xoff = xoff - xoff%(16*zoom);
                            }
                            refresh = 1;
                            break;

                        case XK_KP_Up:
                        case XK_KP_8:
                            if (diffing)
                                yshift--;
                            else if (showpels) {
                                if (ypel>0)
                                    ypel--;
                            } else if (showmark) {
                                if (ymark>0)
                                    ymark--;
                            } else {
                                if (yoff>16*zoom)
                                    yoff -= 16*zoom;
                                else
                                    yoff = 0;
                                yoff = yoff - yoff%(16*zoom);
                            }
                            refresh = 1;
                            break;

                        case XK_KP_Right:
                        case XK_KP_6:
                            if (diffing)
                                xshift++;
                            else if (showpels) {
                                if (xpel<bufwidth-1)
                                    xpel++;
                            } else if (showmark) {
                                if (xmark<bufwidth/16-1)
                                    xmark++;
                            } else {
                                if (xoff<numfiles*bufwidth*zoom-window.width-16*zoom)
                                    xoff += 16*zoom;
                                else
                                    xoff = numfiles*bufwidth*zoom-window.width;
                                xoff = xoff - xoff%(16*zoom);
                            }
                            refresh = 1;
                            break;

                        case XK_KP_Down:
                        case XK_KP_2:
                            if (diffing)
                                yshift++;
                            else if (showpels) {
                                if (ypel<bufheight-1)
                                    ypel++;
                            } else if (showmark) {
                                if (ymark<bufheight/16-1)
                                    ymark++;
                            } else {
                                if (yoff<bufheight*zoom-window.height-16*zoom)
                                    yoff += 16*zoom;
                                else
                                    yoff = bufheight*zoom-window.height;
                                yoff = yoff - yoff%(16*zoom);
                            }
                            refresh = 1;
                            break;

                        case XK_g:
                        case XK_G:
                        case XK_F6:
                            showgrid = !showgrid;
                            refresh = 1;
                            break;

                        case XK_n:
                        case XK_N:
                            shownums = (shownums+1) % 3;
                            refresh = 1;
                            break;

                        case XK_i:
                        case XK_I:
                        case XK_F5:
                            showinfo = (showinfo+1) % 3;
                            refresh = 1;
                            break;

                        case XK_p:
                        case XK_P:
                            if (numfiles>1)
                                showpsnr = !showpsnr;
                            showstat = 0;
                            refresh = 1;
                            break;

                        case XK_s:
                        case XK_S:
                            if (numfiles>1)
                                showstat = !showstat;
                            showpsnr = 0;
                            refresh = 1;
                            break;

                        case XK_h:
                        case XK_H:
                            showprob = !showprob;
                            refresh = 1;
                            break;

                        case XK_o:
                        case XK_O:
                            showover = !showover;
                            if (drawmode==0)
                                /* init overlay */
                                drawmode = DRAWMODE_DCT | DRAWMODE_BAR | DRAWMODE_REF | DRAWMODE_VEC | DRAWMODE_INT | DRAWMODE_COL;
                            refresh = 1;
                            break;

                        case XK_m:
                        case XK_M:
                            showmap = !showmap;
                            refresh = 1;
                            break;

                        case XK_r:
                        case XK_R:
                            showfield = !showfield;
                            refresh = 1;
                            break;

                        case XK_Tab:
                            /* toggle decoded display mode */
                            drawmode ^= DRAWMODE_DEC;
                            refresh = 1;
                            break;

                        case XK_1:
                            /* toggle intra display */
                            drawmode ^= DRAWMODE_INT;
                            drawmode &= ~DRAWMODE_MVS;
                            showover = drawmode? 1 : 0;
                            refresh = 1;
                            break;

                        case XK_2:
                            /* cycle motion vector display mode */
                            drawmode ^= DRAWMODE_VEC;
                            drawmode &= ~DRAWMODE_MVS;
                            showover = drawmode? 1 : 0;
                            refresh = 1;
                            break;

                        case XK_3:
                            /* cycle motion vector numeric display mode */
                            drawmode ^= DRAWMODE_MVS;
                            drawmode &= ~DRAWMODE_VEC;
                            showover = drawmode? 1 : 0;
                            refresh = 1;
                            break;

                        case XK_4:
                            /* toggle motion vector reference display */
                            drawmode ^= DRAWMODE_REF;
                            showover = drawmode? 1 : 0;
                            refresh = 1;
                            break;

                        case XK_5:
                            /* toggle dct mode/mbaff/transform split display */
                            drawmode ^= DRAWMODE_DCT;
                            showover = drawmode? 1 : 0;
                            refresh = 1;
                            break;

                        case XK_6:
                            /* toggle status bar display */
                            drawmode ^= DRAWMODE_BAR;
                            showover = drawmode? 1 : 0;
                            refresh = 1;
                            break;

                        case XK_7:
                            /* toggle number of bits display */
                            drawmode ^= DRAWMODE_NUM;
                            showover = drawmode? 1 : 0;
                            refresh = 1;
                            break;

                        case XK_8:
                            showqmat = !showqmat;
                            refresh = 1;
                            break;

                        case XK_9:
                            drawmode ^= DRAWMODE_COL;
                            showover = drawmode? 1 : 0;
                            refresh = 1;
                            break;

                        case XK_0:
                            showparm = !showparm;
                            refresh = 1;
                            break;

                        case XK_space:
                        case XK_F1:
                            playing = !playing;
                            break;

                        case XK_BackSpace:
                            reverse = !reverse;
                            /* start playback but don't stop it */
                            if (!playing)
                                playing = 1;
                            break;

                        case XK_d:
                        case XK_D:
                            if (numfiles>1) {
                                diffing = diffing==1? 0 : 1;
                                refresh = 1;
                            }
                            break;

                        case XK_t:
                        case XK_T:
                            if (numfiles>1) {
                                diffing = diffing==2? 0 : 2;
                                refresh = 1;
                            }
                            break;

                        case XK_comma:
                        case XK_less:
                            if (gain>1) {
                                gain--;
                                refresh = 1;
                            }
                            break;

                        case XK_period:
                        case XK_greater:
                            if (gain<255) {
                                gain++;
                                refresh = 1;
                            }
                            break;

                        case XK_bracketleft:
                        case XK_braceleft:
                            if (comparison>0) {
                                comparison--;
                                refresh = 1;
                            }
                            break;

                        case XK_bracketright:
                        case XK_braceright:
                            if (comparison<numfiles-1) {
                                comparison++;
                                refresh = 1;
                            }
                            break;

                        /*case XK_minus:
                            if (speed>0.125)
                                speed /= 2.0;
                            break;

                        case XK_plus:
                        case XK_equal:
                            if (speed<8.0)
                                speed *= 2.0;
                            break;*/

                        case XK_numbersign:
                            gridsize = gridsize==64 ? 4 : gridsize*2;
                            refresh = 1;
                            break;

                        case XK_asciitilde:
                            gridsize = gridsize==4 ? 64 : gridsize/2;
                            refresh = 1;
                            break;

                        case XK_l:
                        case XK_L:
                            // can't change planes when monochome.
                            if (hsub[0] && vsub[0]) {
                                plane = ALL_PLANES;
                                refresh = 1;
                            }
                            break;

                        case XK_y:
                        case XK_Y:
                            if (hsub[0] && vsub[0]) {
                                if (plane==LUMA_PLANE)
                                    plane = ALL_PLANES;
                                else
                                    plane = LUMA_PLANE;
                                refresh = 1;
                            }
                            break;

                        case XK_c:
                        case XK_C:
                            if (hsub[0] && vsub[0]) {
                                if (plane==CHROMA_PLANE)
                                    plane = ALL_PLANES;
                                else
                                    plane = CHROMA_PLANE;
                                refresh = 1;
                            }
                            break;

                        case XK_u:
                        case XK_U:
                            if (hsub[0] && vsub[0]) {
                                if (plane==CB_PLANE)
                                    plane = ALL_PLANES;
                                else
                                    plane = CB_PLANE;
                                refresh = 1;
                            }
                            break;

                        case XK_v:
                        case XK_V:
                            if (hsub[0] && vsub[0]) {
                                if (plane==CR_PLANE)
                                    plane = ALL_PLANES;
                                else
                                    plane = CR_PLANE;
                                refresh = 1;
                            }
                            break;

                        case XK_z:
                        case XK_Z:
                            follow = !follow;
                            following = follow? 1 : 0;
                            break;

                        case XK_Escape:
                        case XK_q:
                        case XK_Q:
                        case XK_F3:
                            /* exit */
                            fileindex = numfiles;
                            goto close_file;
                            break;

                        default:
                            continue;
                    }
                }
                break;

                case KeyRelease:
                    continue;

                case ButtonPress:
                    switch (xev.xbutton.button) {
                        case Button1:
                            // reset drag deltas.
                            xpos = xev.xbutton.x + xoff;
                            ypos = xev.xbutton.y + yoff;
                            refresh = 1;
                            break;

                        case Button2:
                            // mark the pel.
                            xpel = ((xev.xbutton.x + xoff) / zoom) % bufwidth;
                            ypel = ((xev.xbutton.y + yoff) / zoom) % bufwidth;
                            showpels = 1;
                            refresh = 1;
                            break;

                        case Button3:
                        {
                            // mark the macroblock.
                            int x = (((xev.xbutton.x + xoff) / zoom) % bufwidth) / 16;
                            int y = (((xev.xbutton.y + yoff) / zoom) % bufwidth) / 16;
                            if (x==xmark && y==ymark) {
                                showmark = 0;
                                xmark = ymark = -1;
                            } else {
                                showmark = 1;
                                xmark = x;
                                ymark = y;
                            }
                            refresh = 1;
                            break;
                        }

                        case Button4:
                            if (zoom<MAXZOOM) {
                                zoom++;
                                resize_pixmap(&window, numfiles*bufwidth*zoom, bufheight*zoom);
                                resize_ximage(&window, bufwidth*zoom, bufheight*zoom);
                                xoff = clampxoff(xoff, numfiles*bufwidth*zoom, window.width);
                                yoff = clampyoff(yoff, bufheight*zoom, window.height);
                                refresh = 1;
                            }
                            break;

                        case Button5:
                            if (zoom>1) {
                                zoom--;
                                resize_pixmap(&window, numfiles*bufwidth*zoom, bufheight*zoom);
                                resize_ximage(&window, bufwidth*zoom, bufheight*zoom);
                                if ((numfiles*bufwidth*zoom<window.width) || (bufheight*zoom<window.height))
                                    resize_window(&window, numfiles*bufwidth*zoom, bufheight*zoom);
                                xoff = clampxoff(xoff, numfiles*bufwidth*zoom, window.width);
                                yoff = clampyoff(yoff, bufheight*zoom, window.height);
                                refresh = 1;
                            }
                            break;

                        default:
                            continue;
                    }
                    break;

                case MotionNotify:
                    xoff = xpos - xev.xmotion.x;
                    yoff = ypos - xev.xmotion.y;
                    xoff = clampxoff(xoff, numfiles*bufwidth*zoom, window.width);
                    yoff = clampyoff(yoff, bufheight*zoom, window.height);
                    XCopyArea(xev.xmotion.display, window.pixmap, xev.xmotion.window, window.gc,
                              xoff, yoff, window.width, window.height, 0, 0);
                    continue;

                case Expose:
                    /* refresh damaged area from pixmap */
                    XCopyArea(xev.xexpose.display, window.pixmap, xev.xexpose.window, window.gc,
                            xev.xexpose.x+xoff, xev.xexpose.y+yoff, xev.xexpose.width, xev.xexpose.height,
                            xev.xexpose.x, xev.xexpose.y);
                    continue;

                case ConfigureNotify:
                    {
                        window.width = xev.xconfigure.width;
                        window.height = xev.xconfigure.height;
                        unsigned int xnew = clampxoff(xoff, numfiles*bufwidth*zoom, window.width);
                        unsigned int ynew = clampyoff(yoff, bufheight*zoom, window.height);
                        if (xnew!=xoff || ynew!=yoff) {
                            xoff = xnew;
                            yoff = ynew;
                            XCopyArea(xev.xconfigure.display, window.pixmap, xev.xconfigure.window, window.gc,
                                    xoff, yoff, window.width, window.height, 0, 0);
                        }
                    }
                    continue;

                case UnmapNotify:
                case MapNotify:
                    break;

                default:
                    rvmessage("got unknown event: type=%d window=%lu", xev.xany.type, xev.xany.window);
                    continue;
            }
        } else {
            /* playing or following */
            if (playing)
                for (i=0; i<numfiles; i++)
                    nextframe[i] = framenum[i] + (reverse?-1:1);
        }
        first = 0;

        /* read or decode next frame from each file */
        int errors = 0;
        for (i=0; i<numfiles; i++) {
            /* check that a new frame is required */
            if (nextframe[i] == framenum[i])
                continue;

            /* check that the nextframe is within the file */
            if (nextframe[i]<0) {
                nextframe[i] = 0;
                errors++;
                continue;
            }

            /* select what to do based on filetype */
            int bytes;
            switch (divine[i].filetype) {
                case YUV:
                    /* reposition file pointer */
                    if (nextframe[i] != framenum[i]+1) {
                        /* calculate index on the fly */
                        off_t offset = (off_t)framesize[i]*(off_t)nextframe[i]*8ll;

                        /* reposition file pointer */
                        off_t pos = seekbits(bb[i], offset);
                        if (pos!=offset) {
                            /* seek failed, probably file is not large enough */
                            nextframe[i] = framenum[i] + 1;
                        }
                    }

                    /* read next frame from file TODO reimplement follow mode */
                    if (fourcc_is_planar(fourcc[i]))
                        bytes = read_frame_planar(frame[i].data(), width[i], height[i], hsub[i], vsub[i], bpp[i], bufwidth, bufheight, bb[i]);
                    else
                        bytes = read_frame_packed(frame[i].data(), width[i], height[i], fourcc[i], bb[i]);
                    if (bytes<0) {
                        nextframe[i] = framenum[i];
                        errors++;
                    } else if (bytes==0 && !following) {
                        nextframe[i] = framenum[i];
                        errors++;
                    } else if (bytes!=framesize[i]) {
                        nextframe[i] = framenum[i];
                        errors++;
                        if (bytes<framesize[i]/2)
                            rvmessage("input is not a whole number of frames: %d bytes extra", bytes);
                        else
                            rvmessage("input is not a whole number of frames: %zd bytes remaining", framesize[i]-bytes);
                    } else {
                        /* frame read was successful */
                        framenum[i] = nextframe[i];
                        lastframe[i] = mmax(lastframe[i], framenum[i]);
                        refresh = 1;
                    }
                    break;

                case YUV4MPEG:
                    /* take note of frame position in index */
                    if (framenum[i]+1<MAXFRAMES)
                        if (index[i][framenum[i]+1]==0)
                            index[i][framenum[i]+1] = tellbits(bb[i]);

                    /* reposition file pointer */
                    if (nextframe[i] != framenum[i]+1) {
                        if (nextframe[i]<MAXFRAMES) {
                            /* reposition file pointer */
                            if (index[i][nextframe[i]] || nextframe[i]==0) {
                                off_t pos = seekbits(bb[i], index[i][nextframe[i]]);
                                if (pos!=index[i][nextframe[i]]) {
                                    /* seek failed, probably file is not large enough */
                                    nextframe[i] = framenum[i] + 1;
                                }
                            }

                            /* read yuv4mpeg file header if rewinding to beginning */
                            if (nextframe[i]==0)
                                if (read_y4m_stream_header_bits(bb[i], &width[i], &height[i], &framerate[0], &fourcc[i])<0)
                                    rverror("failed to read yuv4mpeg stream header");
                        }
                    }

                    /* read next frame from file */
                    read_y4m_frame_header_bits(bb[i]);
                    bytes = read_frame_planar(frame[i].data(), width[i], height[i], hsub[i], vsub[i], bpp[i], bufwidth, bufheight, bb[i]);
                    if (bytes<0) {
                        nextframe[i] = framenum[i];
                        errors++;
                    } else if (bytes==0 && !following) {
                        nextframe[i] = framenum[i];
                        errors++;
                    } else if (bytes!=framesize[i]) {
                        nextframe[i] = framenum[i];
                        errors++;
                        if (bytes<framesize[i]/2)
                            rvmessage("input is not a whole number of frames: %d bytes extra", bytes);
                        else
                            rvmessage("input is not a whole number of frames: %zd bytes remaining", framesize[i]-bytes);
                    } else {
                        /* frame read was successful */
                        framenum[i] = nextframe[i];
                        lastframe[i] = mmax(lastframe[i], framenum[i]);
                        refresh = 1;
                    }
                    break;

                case H261:
                    break;

                case M2V:
                    /* decode next frame if not decoded already and playing forward */
                    if (decodedframe[i][nextframe[i]]==NULL && nextframe[i]==framenum[i]+1) {

                        /* find next picture */
                        if (find_m2v_next_picture(bb[i], sequence[i], picturenum[i], NULL, verbose)<0) {
                            /* cound not find next picture */
                            if (referenceframe[i][1]) {
                                /* emit last frame from decoded buffer */
                                decodedframe[i][nextframe[i]] = referenceframe[i][1];
                                referenceframe[i][1] = NULL;
                                sequence[i]->picture[nextframe[i]] = picturedata[i][0];
                                sequence[i]->picture2[nextframe[i]] = picturedata[i][1];
                                refresh = 1;
                            }
                            /* otherwise last frame has been displayed */

                        } else {

                            struct rvm2vpicture *secondpicture = NULL;
                            struct rvm2vpicture *firstpicture = parse_m2v_picture(bb[i], sequence[i], picturenum[i], verbose);
                            if (firstpicture==NULL) {

                                rvmessage("failed to parse picture %d in file \"%s\"", picturenum[i], filename[i]);
                                if (referenceframe[i][1]) {
                                    /* emit last frame from decoded buffer */
                                    decodedframe[i][nextframe[i]] = referenceframe[i][1];
                                    referenceframe[i][1] = NULL;
                                    sequence[i]->picture[nextframe[i]] = picturedata[i][0];
                                    sequence[i]->picture2[nextframe[i]] = picturedata[i][1];
                                    refresh = 1;
                                }
                                /* otherwise last frame has been displayed */

                            } else {
                                picturenum[i]++;

                                /* decode first picture */
                                sample_t *firstpic = decode_m2v(firstpicture, sequence[i], referenceframe[i][0], referenceframe[i][1], picturenum[i]-1, refidct);

                                /* check if first picture was field picture */
                                sample_t *newframe;
                                if (field_picture(firstpicture)) {

                                    /* find next picture */
                                    if (find_m2v_next_picture(bb[i], sequence[i], picturenum[i], NULL, verbose)<0) {
                                        /* cound not find next picture */
                                        if (referenceframe[i][1]) {
                                            /* emit last frame from decoded buffer */
                                            decodedframe[i][nextframe[i]] = referenceframe[i][1];
                                            referenceframe[i][1] = NULL;
                                            sequence[i]->picture[nextframe[i]] = picturedata[i][0];
                                            sequence[i]->picture2[nextframe[i]] = picturedata[i][1];
                                            refresh = 1;
                                        }
                                        /* otherwise last frame has been displayed */
                                        break;
                                    }

                                    /* decode second picture */
                                    secondpicture = parse_m2v_picture(bb[i], sequence[i], picturenum[i], verbose);
                                    if (secondpicture!=NULL) {
                                        picturenum[i]++;

                                        sample_t *secondpic;
                                        if (secondpicture->picture_coding_type==PCT_P) {

                                            /* prepare reference frame from previous field pictures */
                                            sample_t *reffield, *refframe;
                                            if (secondpicture->extension.picture_structure==PST_BOTTOM) {
                                                reffield = copy_field_from_frame(referenceframe[i][1], 0, width[i], height[i], sequence[i]->chroma_format);
                                                refframe = copy_fields_into_frame(firstpic, reffield, width[i], height[i], sequence[i]->chroma_format);
                                            } else {
                                                reffield = copy_field_from_frame(referenceframe[i][1], 1, width[i], height[i], sequence[i]->chroma_format);
                                                refframe = copy_fields_into_frame(reffield, firstpic, width[i], height[i], sequence[i]->chroma_format);
                                            }

                                            /* decode second picture */
                                            secondpic = decode_m2v(secondpicture, sequence[i], NULL, refframe, picturenum[i]-1, refidct);

                                            rvfree(reffield);
                                            rvfree(refframe);

                                        } else {

                                            /* decode second picture */
                                            secondpic = decode_m2v(secondpicture, sequence[i], referenceframe[i][0], referenceframe[i][1], picturenum[i]-1, refidct);

                                        }

                                        /* merge field pictures */
                                        if (secondpicture->extension.picture_structure==PST_BOTTOM)
                                            newframe = copy_fields_into_frame(firstpic, secondpic, width[i], height[i], sequence[i]->chroma_format);
                                        else
                                            newframe = copy_fields_into_frame(secondpic, firstpic, width[i], height[i], sequence[i]->chroma_format);
                                        rvfree(firstpic);
                                        rvfree(secondpic);
                                    } else
                                        newframe = NULL;
                                } else
                                    newframe = firstpic;

                                /* reorder frames and picture data */
                                if (firstpicture->picture_coding_type==PCT_B) {
                                    decodedframe[i][nextframe[i]] = newframe;
                                    sequence[i]->picture[nextframe[i]] = firstpicture;
                                    sequence[i]->picture2[nextframe[i]] = secondpicture;
                                } else {
                                    decodedframe[i][nextframe[i]] = referenceframe[i][1];
                                    referenceframe[i][0] = referenceframe[i][1];
                                    referenceframe[i][1] = newframe;
                                    sequence[i]->picture[nextframe[i]] = picturedata[i][0];
                                    sequence[i]->picture2[nextframe[i]] = picturedata[i][1];
                                    picturedata[i][0] = firstpicture;
                                    picturedata[i][1] = secondpicture;
                                }
                            }
                        }
                    }
                    if (decodedframe[i][nextframe[i]]==NULL) {
                        /* decoded frame is not available */
                        nextframe[i] = framenum[i];
                        errors++;
                    } else {
                        /* frame decode was successful */
                        framenum[i] = nextframe[i];
                        lastframe[i] = mmax(lastframe[i], framenum[i]);
                        frame[i] = decodedframe[i][nextframe[i]];
                        refresh = 1;

                        /* free up some old frames, keeping a small buffer to rewind into */
                        const int freeframe = nextframe[i]-(32000000/bufwidth/bufheight);
                        if (freeframe>=0) {
                            rvfree(decodedframe[i][freeframe]);
                            decodedframe[i][freeframe] = NULL;
                            if (sequence[i]->picture[freeframe]) {
                                free_m2v_picture(sequence[i]->picture[freeframe]);
                                sequence[i]->picture[freeframe] = NULL;
                            }
                            if (sequence[i]->picture2[freeframe]) {
                                free_m2v_picture(sequence[i]->picture2[freeframe]);
                                sequence[i]->picture2[freeframe] = NULL;
                            }
                        }
                    }
                    break;

                case H264:
                {
                    /* decode next frame if not decoded already and playing forward */
                    if (decodedframe[i][nextframe[i]]==NULL && nextframe[i]==framenum[i]+1) {
                        /* loop over input data chunks */
                        bool eof = false;
                        do {
                            if (!eof) {
                                size_t read = readbits(bb[i], luddata[i]+ludpos[i], ludsize-ludpos[i]);
                                if (read==0) {
                                    /* check for errors */
                                    if (errorbits(bb[i]))
                                        rverror("failed to read from input");

                                    /* otherwise check for end of file */
                                    eof = eofbits(bb[i]);
                                }
                                ludpos[i] += read;
                            }

                            /* parse the bitstream for the next nal unit */
                            uint32_t consumed = ludpos[i];
                            uint8_t* buf;
                            uint32_t len;
                            RetCode r = ludh264_parse_bytestream_nalu(avc_sequence[i], luddata[i], &consumed, &buf, &len, eof);
                            if (r!=LUDH264_SUCCESS) {
                                /* probably unnecessary */
                                buf = NULL;
                                len = 0;
                            }

                            /* decode the nal unit */
                            r = ludh264_decode_nalu(avc_sequence[i], buf, len, &pic);
                            if (r!=LUDH264_SUCCESS)
                                rvexit("failed to decode nal unit");

                            /* track consumed bytes and remove them from the buffer */
                            ludpos[i] -= consumed;
                            if (consumed)
                                memmove(luddata[i], luddata[i] + consumed, ludpos[i]);

                            if (pic) {
                                void* buffer;
                                uint32_t buffer_size;
                                uint32_t width, height;
                                LUDH264_VIDEO_FORMAT format;

                                ludh264_get_picture(avc_sequence[i], pic, &buffer, &buffer_size, &format, &width, &height);

                                /* copy frame to history buffer */
                                sample_t *newframe = (sample_t *)rvalloc(NULL, get_framesize(bufwidth, bufheight, fourcc[i]), 0);
                                memcpy(newframe, buffer, buffer_size);
                                decodedframe[i][nextframe[i]] = newframe;

                                struct rv264seq_parameter_set *sps;
                                struct rv264pic_parameter_set *pps;
                                struct slice_header *sh;
                                struct rv264macro *macro;

                                ludh264_get_picture_metadata(avc_sequence[i], pic, &sps, &pps, &sh, &macro);

                                /* copy sequence metadata */
                                //avc_sequence[i]->active_sps = sps->seq_parameter_set_id;
                                // FIXME this causes invalid reads from free'd block, however it isn't actually used.
                                //avc_sequence[i]->active_pps = pps->pic_parameter_set_id;

                                /* copy frame or top field metadata for overlay buffer */
                                realloc_264_frame_array(avc_sequence[i], nextframe[i]+1);
                                avc_sequence[i]->frame[nextframe[i]].frame = alloc_264_picture();
                                add_264_slice(avc_sequence[i]->frame[nextframe[i]].frame, sh);
                                avc_sequence[i]->frame[nextframe[i]].frame->macro = (struct rv264macro *)rvalloc(NULL, sizeof(struct rv264macro) * sh->PicSizeInMbs, 0);
                                memcpy(avc_sequence[i]->frame[nextframe[i]].frame->macro, macro, sizeof(struct rv264macro) * sh->PicSizeInMbs);
                                memcpy(&avc_sequence[i]->frame[nextframe[i]].frame->sei_message, &pic->sei_message, sizeof(struct rv264sei_message));

                                /* bottom field metadata for overlay buffer */
                                if ( (avc_sequence[i]->frame[nextframe[i]].field_pic_flag = sh->field_pic_flag) ) {
                                    ludh264_get_picture_metadata_bottom_field(avc_sequence[i], pic, &sps, &pps, &sh, &macro);

                                    /* copy bottom field metadata for overlay buffer */
                                    avc_sequence[i]->frame[nextframe[i]].botfield = alloc_264_picture();
                                    add_264_slice(avc_sequence[i]->frame[nextframe[i]].botfield, sh);
                                    avc_sequence[i]->frame[nextframe[i]].botfield->macro = (struct rv264macro *)rvalloc(NULL, sizeof(struct rv264macro) * sh->PicSizeInMbs, 0);
                                    memcpy(avc_sequence[i]->frame[nextframe[i]].botfield->macro, macro, sizeof(struct rv264macro) * sh->PicSizeInMbs);
                                    memcpy(&avc_sequence[i]->frame[nextframe[i]].botfield->sei_message, &pic->sei_message, sizeof(struct rv264sei_message));
                                }

                                /* set mbaff numbering */
                                mbaff = !sps->frame_mbs_only_flag && sps->mb_adaptive_frame_field_flag;

                                ludh264_free_picture(avc_sequence[i], pic);

                                break;
                            }

                            /* check for decoding finished conditions */
                            if (!pic && !buf && !len)
                                break;
                        } while (1);
                    }

                    if (decodedframe[i][nextframe[i]]==NULL) {
                        /* decoded frame is not available */
                        nextframe[i] = framenum[i];
                        errors++;
                    } else {
                        /* frame decode was successful */
                        framenum[i] = nextframe[i];
                        lastframe[i] = mmax(lastframe[i], framenum[i]);
                        frame[i] = decodedframe[i][nextframe[i]];
                        refresh = 1;

                        /* free up some old frames, keeping a small buffer to rewind into */
                        const int freeframe = nextframe[i]-(32000000/bufwidth/bufheight);
                        if (freeframe>=0) {
                            rvfree(decodedframe[i][freeframe]);
                            decodedframe[i][freeframe] = NULL;
                            free_264_frame(avc_sequence[i]->frame+freeframe);
                        }
                    }
                    break;
                }

                case HEVC:
#ifdef HAVE_LIBDE265
                    /* decode next frame if not decoded already and playing forward */
                    if (decodedframe[i][nextframe[i]]==NULL && nextframe[i]==framenum[i]+1) {

                        /* decode the next hevc frame */
                        de265_image[i] = de265_get_next_picture(decoder[i]);
                        while (!de265_image[i]) {

                            /* decode some more */
                            int more = 1;
                            de265_error err = de265_decode(decoder[i], &more);
                            if (more && de265_isOK(err))
                                de265_image[i] = de265_get_next_picture(decoder[i]);
                            else if (more && err==DE265_ERROR_WAITING_FOR_INPUT_DATA) {
                                /* read a chunk of input data */
                                uint8_t buf[BUFFER_SIZE];
                                int n = fread(buf, 1, BUFFER_SIZE, fh[i]);
                                if (n) {
                                    err = de265_push_data(decoder[i], buf, n, 0, NULL);
                                    if (!de265_isOK(err)) {
                                        rverror("failed to push hevc data to decoder: %s", de265_get_error_text(err));
                                    }
                                }

                                if (feof(fh[i])) {
                                    err = de265_flush_data(decoder[i]); // indicate end of stream
                                }
                            } else if (!more) {
                                /* decoding finished */
                                if (!de265_isOK(err))
                                    rvmessage("error decoding frame: %s", de265_get_error_text(err));
                                break;
                            }
                        }

                        /* extract data from available frame */
                        if (de265_image[i]) {
                            int stride, line;

                            /* copy frame to history buffer FIXME 4:2:0 only */
                            sample_t *newframe = (sample_t *)rvalloc(NULL, get_framesize(bufwidth, bufheight, fourcc[i]), 0);
                            const unsigned char *plane = de265_get_image_plane(de265_image[i], 0, &stride);
                            for (line=0; line<de265_get_image_height(de265_image[i], 0); line++)
                                memcpy(newframe+line*bufwidth, plane+line*stride, de265_get_image_width(de265_image[i], 0));
                            plane = de265_get_image_plane(de265_image[i], 1, &stride);
                            for (line=0; line<de265_get_image_height(de265_image[i], 1); line++)
                                memcpy(newframe+bufwidth*bufheight+line*bufwidth/2, plane+line*stride, de265_get_image_width(de265_image[i], 1));
                            plane = de265_get_image_plane(de265_image[i], 2, &stride);
                            for (line=0; line<de265_get_image_height(de265_image[i], 2); line++)
                                memcpy(newframe+bufwidth*bufheight*5/4+line*bufwidth/2, plane+line*stride, de265_get_image_width(de265_image[i], 2));
                            decodedframe[i][nextframe[i]] = newframe;

                            /* copy frame metadata for overlay buffer */
                            video[i]->picture[nextframe[i]] = alloc_hevc_picture(video[i]->sps[video[i]->active_sps]);
                            extract_hevc_slice_header(de265_image[i], video[i]->picture[nextframe[i]]->sh, video[i]->pps);
                            extract_hevc_ctus(de265_image[i], *video[i]->picture[nextframe[i]]);
                        }

                        /* show warnings in decode */
                        while (1) {
                            de265_error warning = de265_get_warning(decoder[i]);
                            if (de265_isOK(warning))
                                break;

                            rvmessage("warning after decoding: %s", de265_get_error_text(warning));
                        }

                    }
                    if (decodedframe[i][nextframe[i]]==NULL) {
                        /* decoded frame is not available */
                        nextframe[i] = framenum[i];
                        errors++;
                    } else {
                        /* frame decode was successful */
                        framenum[i] = nextframe[i];
                        lastframe[i] = mmax(lastframe[i], framenum[i]);
                        frame[i] = decodedframe[i][nextframe[i]];
                        refresh = 1;

                        /* free up some old frames, keeping a small buffer to rewind into */
                        const int freeframe = nextframe[i]-(32000000/bufwidth/bufheight);
                        if (freeframe>=0) {
                            rvfree(decodedframe[i][freeframe]);
                            decodedframe[i][freeframe] = NULL;
                            if (video[i]->picture[freeframe]) {
                                free_hevc_picture(video[i]->picture[freeframe]);
                                video[i]->picture[freeframe] = NULL;
                            }
                        }
                    }
#endif
                    break;

                default:
                    break;
            }

        }

        /* stop playing at end of longest file */
        if (playing && !reverse && errors==numfiles) {
            playing = 0;
            if (follow)
                following = 1;
            if (!waitatend)
                break;
            else
                continue;
        }

        /* stop reversing at beginning of file */
        if (playing && reverse && errors==numfiles) {
            playing = 0;
            reverse = 0;
            continue;
        }

        /* only continue to redisplay the image if required */
        if (!refresh)
            continue;

        /* prepare overlay */
        int overframe = 0;
        int overfile = 0;
        int fieldpic = 0;
        if (showover || showparm || showmap || showqmat) {
            /* choose one frame to overlay on all images */
            for (i=0; i<numfiles; i++)
                if (framenum[i]>overframe)
                    overframe = framenum[i];

            /* choose one file to overlay on raw video */
            for (i=0; i<numfiles; i++) {
                if (picture[i]) {
                    overfile = i;
                    break;
                }
                if (sequence[i]) {
                    overfile = i;
                    break;
                }
                if (avc_sequence[i]) {
                    overfile = i;
                    if (avc_sequence[i]->frame[overframe].field_pic_flag)
                        fieldpic = 1;
                    break;
                }
#ifdef HAVE_LIBDE265
                if (video[i]) {
                    overfile = i;
                    break;
                }
#endif
            }
        }

        if (showover) {
            /* overlay colour specific macroblock information */
            for (i=0; i<numfiles; i++) {
                /*if (picture[0])
                    draw_261_overlay(overlay[i], picture[overframe]);
                else */ if (sequence[i])
                    draw_m2v_overlay(overlay[i], sequence[i]->picture[overframe], sequence[i]->picture2[overframe]);
                else if (sequence[overfile])
                    draw_m2v_overlay(overlay[i], sequence[overfile]->picture[overframe], sequence[overfile]->picture2[overframe]);
                else if (macro)
                    draw_h264_overlay(overlay[i], macro+overframe*width[i]*height[i]/256, mbaff && logfile, 0, 0);
                else if (avc_sequence[i] && avc_sequence[i]->frame[overframe].frame) {
                    if (!avc_sequence[i]->frame[overframe].field_pic_flag)
                        draw_h264_overlay(overlay[i], avc_sequence[i]->frame[overframe].frame->macro, mbaff && logfile, 0, 0);
                    else {
                        draw_h264_overlay(overlay[i], avc_sequence[i]->frame[overframe].topfield->macro, mbaff && logfile, 1, 0);
                        draw_h264_overlay(overlay[i], avc_sequence[i]->frame[overframe].botfield->macro, mbaff && logfile, 1, 1);
                    }
                } else if (avc_sequence[overfile] && avc_sequence[overfile]->frame[overframe].frame) {
                    if (!avc_sequence[overfile]->frame[overframe].field_pic_flag)
                        draw_h264_overlay(overlay[i], avc_sequence[overfile]->frame[overframe].frame->macro, mbaff && logfile, 0, 0);
                    else {
                        draw_h264_overlay(overlay[i], avc_sequence[overfile]->frame[overframe].topfield->macro, mbaff && logfile, 1, 0);
                        draw_h264_overlay(overlay[i], avc_sequence[overfile]->frame[overframe].botfield->macro, mbaff && logfile, 1, 1);
                    }
                }
#ifdef HAVE_LIBDE265
                else if (decoder[i] && video[i]->picture[overframe])
                    draw_265_overlay(overlay[i], video[i]->picture[overframe]);
                else if (decoder[overfile] && video[overfile]->picture[overframe])
                    draw_265_overlay(overlay[i], video[overfile]->picture[overframe]);
#endif
                else
                    overlay[i].clear_chroma();
            }
        }

        if (showmap) {
            /* brighten or darken image based on qp */
            for (i=0; i<numfiles; i++) {
                /* copy source image */
                strength[i].copy(frame[i]);
                if (showover)
                    strength[i].copy_chroma(overlay[i], 0, 0, 0, 0, width[i], height[i]);
                if (sequence[i])
                    draw_m2v_qpmap(strength[i], sequence[i]->picture[overframe], sequence[i]->picture2[overframe]);
                else if (sequence[overfile])
                    draw_m2v_qpmap(strength[i], sequence[overfile]->picture[overframe], sequence[overfile]->picture2[overframe]);
            }
        }

        /* separate frame into fields */
        if (showfield || fieldpic)
            for (i=0; i<numfiles; i++)
                field[i].split(frame[i], fourcc[i]);

        /* shift reference image */
        if (xshift || yshift) {
            /* black shift buffer */
            shift.clear();

            /* copy source image into shift buffer with shift */
            shift.shift(showfield || fieldpic? field[comparison] : frame[comparison], xshift, yshift);
        }

        /* prepare diffs and statistics */
        for (i=0; i<numfiles; i++) {
            struct rvimage &reference = xshift || yshift? shift : showfield || fieldpic? field[comparison] : frame[comparison];

            /* calculate differences */
            if (diffing)
                if (i!=comparison)
                    compare_frame(showfield || fieldpic? field[i].data() : frame[i].data(), reference.data(), diff[i].data(), bufwidth, bufheight, bufframesize, fourcc[i], diffing, gain);

            /* calculate entropy */
            if (showprob)
                entropy[i] = calc_entropy(frame[i], plane);

            /* calculate mean pel value */
            if (showprob)
                mean[i] = calc_mean(frame[i], plane);

            /* calculate statistics */
            if (showpsnr || showstat)
                if (i!=comparison) {
                    stats[i] = calculate_stats(showfield || fieldpic? field[i] : frame[i], reference, showstat? 4 : 2, plane);
                }
        }

        /* display frames */
        for (i=0; i<numfiles; i++) {
            const int pelsize = (bpp[i]+7)/8;
            unsigned char *image[3];

            /* use difference frame if required */
            image[0] = diffing && i!=comparison? diff[i].data() : (xshift || yshift) && i==comparison? shift.data() : showfield || fieldpic? field[i].data() : showmap? strength[i][0] : frame[i].data();

            /* use chroma overlay planes if required */
            if (hsub[0] && vsub[0]) {
                image[1] = showover && (drawmode&DRAWMODE_COL)? overlay[i][1] : image[0] + bufwidth*bufheight*pelsize;
                image[2] = showover && (drawmode&DRAWMODE_COL)? overlay[i][2] : image[1] + bufwidth*bufheight*pelsize/bufhsub/bufvsub;
            }

            /* swap individual planes if required */
            if (plane==LUMA_PLANE && fourcc_is_planar(fourcc[i])) {
                image[1] = blank[1];
                image[2] = blank[2];
            }
            if (plane==CHROMA_PLANE && fourcc_is_planar(fourcc[i])) {
                image[0] = blank[0];
            }
            if (plane==CB_PLANE && fourcc_is_planar(fourcc[i])) {
                image[0] = blank[0];
                image[2] = blank[2];
            }
            if (plane==CR_PLANE && fourcc_is_planar(fourcc[i])) {
                image[0] = blank[0];
                image[1] = blank[1];
            }

            /* display frames in a row */
            int x = i*bufwidth*zoom;
            int y = 0;

            /* convert to display format */
            dither_image(&window, image, fourcc[i], bpp[i], zoom);
            display_ximage(&window, x, y);

            /* display overlay */
            if (showover) {
                if (sequence[i])
                    display_m2v_overlay(&window, x, y, bufwidth, bufheight, zoom, drawmode, sequence[i]->picture[overframe], sequence[i]->picture2[overframe]);
                else if (macro)
                    display_264_overlay(&window, x, y, bufwidth, bufheight, zoom, drawmode, macro+overframe*width[i]*height[i]/256, mbaff && logfile, 0, 0);
                else if (avc_sequence[i] && avc_sequence[i]->frame[overframe].frame) {
                    if (!avc_sequence[i]->frame[overframe].field_pic_flag)
                        display_264_overlay(&window, x, y, bufwidth, bufheight, zoom, drawmode, avc_sequence[i]->frame[overframe].frame->macro, mbaff && logfile, 0, 0);
                    else {
                        display_264_overlay(&window, x, y, bufwidth, bufheight, zoom, drawmode, avc_sequence[i]->frame[overframe].topfield->macro, mbaff && logfile, 1, 0);
                        display_264_overlay(&window, x, y, bufwidth, bufheight, zoom, drawmode, avc_sequence[i]->frame[overframe].botfield->macro, mbaff && logfile, 1, 1);
                    }
                } else {
                    if (sequence[overfile])
                        display_m2v_overlay(&window, x, y, bufwidth, bufheight, zoom, drawmode, sequence[overfile]->picture[overframe], sequence[overfile]->picture2[overframe]);
                    else if (macro)
                        display_264_overlay(&window, x, y, bufwidth, bufheight, zoom, drawmode, macro+overframe*width[i]*height[i]/256, mbaff && logfile, 0, 0);
                    else if (avc_sequence[overfile] && avc_sequence[overfile]->frame[overframe].frame) {
                        if (!avc_sequence[overfile]->frame[overframe].field_pic_flag)
                            display_264_overlay(&window, x, y, bufwidth, bufheight, zoom, drawmode, avc_sequence[overfile]->frame[overframe].frame->macro, mbaff && logfile, 0, 0);
                        else {
                            display_264_overlay(&window, x, y, bufwidth, bufheight, zoom, drawmode, avc_sequence[overfile]->frame[overframe].topfield->macro, mbaff && logfile, 1, 0);
                            display_264_overlay(&window, x, y, bufwidth, bufheight, zoom, drawmode, avc_sequence[overfile]->frame[overframe].botfield->macro, mbaff && logfile, 1, 1);
                        }
                    }
#ifdef HAVE_LIBDE265
                    else if (decoder[i] && video[i]->picture[overframe])
                        display_265_overlay(&window, x, y, bufwidth, bufheight, zoom, drawmode, video[i]->picture[overframe]);
                    else if (decoder[i] && video[overfile]->picture[overframe])
                        display_265_overlay(&window, x, y, bufwidth, bufheight, zoom, drawmode, video[overfile]->picture[overframe]);
#endif
                }
            }

            /* display quant matrix */
            if (showqmat)
                if (sequence[i] && sequence[i]->picture[overframe]) {
                    display_m2v_quantmat(&window, x, y, sequence[i]->picture[overframe]);
                    //if (field_picture(sequence[i]->picture[overframe]))
                    //    display_m2v_params(&window, x, y+bufheight*zoom/2, sequence[i]->picture2[overframe]);
                }

            /* display picture parameters */
            if (showparm) {
                if (sequence[i] && sequence[i]->picture[overframe]) {
                    display_m2v_params(&window, x, y, sequence[i]->picture[overframe]);
                    display_m2v_ratios(&window, x, y, sequence[i], sequence[i]->picture[overframe]);
                    if (field_picture(sequence[i]->picture[overframe]))
                        display_m2v_params(&window, x, y+bufheight*zoom/2, sequence[i]->picture2[overframe]);
                }
                if (avc_sequence[i] && avc_sequence[i]->frame[overframe].frame) {
                    display_264_params(&window, x, y, avc_sequence[i]->frame[overframe].frame);
                    if (avc_sequence[i]->frame[overframe].field_pic_flag)
                        display_264_params(&window, x, y+bufheight*zoom/2, avc_sequence[i]->frame[overframe].botfield);
                }
#ifdef HAVE_LIBDE265
                if (decoder[i] && video[i]->picture[overframe]) {
                    display_265_params(&window, x, y, video[i]->picture[overframe]);
                }
#endif
            }

            /* display timecode */
            if (avc_sequence[i] && avc_sequence[i]->frame[framenum[i]].frame->sei_message.pic_timing.seconds_flag) {
                rv264pic_timing *pti = &avc_sequence[i]->frame[framenum[i]].frame->sei_message.pic_timing;
                display_timecode(&window, x, y, pti->hours_value, pti->minutes_value, pti->seconds_value, pti->n_frames);
            }

            /* display entropy */
            if (showprob)
                display_prob(&window, x, y, entropy[i], mean[i]);

            /* display image statistics */
            if (showstat && i!=comparison)
                display_stat(&window, x, y, stats[i]);

            /* display image quality metric */
            if (showpsnr && i!=comparison)
                display_psnr(&window, x, y, stats[i]);

            /* display filenames */
            if (showinfo>=2)
                display_name(&window, x, y, filename[i], numfiles);

            /* display grid */
            if (showgrid) {
                if (gridsize>=16)
                    /* use a thicker line for the grid at larger expansion factors */
                    display_grid(&window, x, y, bufwidth*zoom, bufheight*zoom, gridsize*zoom, zoom>2? 2 : 1);
                else
                    display_grid(&window, x, y, bufwidth*zoom, bufheight*zoom, gridsize*zoom, 1);
            }

            /* display marked macroblock */
            if (showmark)
                display_mark(&window, x, y, bufwidth, zoom, xmark, ymark);
            if (showmark && showparm)
                if (sequence[i] && sequence[i]->picture[overframe]) {
                    display_m2v_macro(&window, x, y, &sequence[i]->picture[overframe]->macro[ymark*sequence[i]->width_in_mbs+xmark]);
                    display_m2v_coeff(&window, x, y, &sequence[i]->picture[overframe]->macro[ymark*sequence[i]->width_in_mbs+xmark]);
                }

            /* display selected pel */
            if (showpels) {
                int value;

                if (fourcc_is_planar(fourcc[i])) {
                    if (plane==CR_PLANE)
                        value = image[2][(ypel/hsub[i])*(width[i]/hsub[i])+xpel/hsub[i]];
                    else if (plane==CB_PLANE)
                        value = image[1][(ypel/hsub[i])*(width[i]/hsub[i])+xpel/hsub[i]];
                    else
                        value = image[0][ypel*width[i]+xpel];
                } else {
                    value = image[0][2*(ypel*width[i]+xpel)];
                }

                if (plane==CR_PLANE || plane==CB_PLANE)
                    display_pel(&window, x, y, bufwidth, bufheight, zoom, xpel, ypel, 1, value);
                else
                    display_pel(&window, x, y, bufwidth, bufheight, zoom, xpel, ypel, 0, value);
            }

            /* display macroblock numbers */
            if (shownums)
                display_numbers(&window, x, y, bufwidth, bufheight, zoom, showgrid, showfield || fieldpic, mbaff, shownums==2);

            /* display information */
            if (showinfo>=1) {
                int time = macro? macro[framenum[i]*width[i]*height[i]/256].pic_order_cnt : 0;
                time = time? time : sequence[i]? sequence[i]->picture[framenum[i]]->decode_order : 0;
                display_info(&window, x, y, framenum[i], lastframe[i], time, diffing==1? gain : 0, diffing==2? gain : 0);
            }

        }

        /* check if it is time to display the next frame */
        gettimeofday(&now, NULL);
        int usecs = timevalsub(next, now);
        if (usecs>0) {
            /* maintain application responsiveness */
            /*if (usecs>10000) {
                usleep(10000);
                continue;
            }*/
            if (verbose>=3) rvmessage("sleep %d usecs", usecs);
            /* don't sleep for unreasonably short times */
            //if (usecs>1000)
            usleep(usecs);
        }

        /* finally, copy pixmap to the window */
        XCopyArea(window.display, window.pixmap, window.window, window.gc, xoff, yoff, window.width, window.height, 0, 0);

        /* calculate next frame presentation time */
        gettimeofday(&now, NULL);
        next = timevaladd(now, (int)(1000000.0/(displayrate*speed)+0.5));

        /* update statistics */
        if (numframes<NUMSAMPLES)
            timestats[numframes] = (long long)now.tv_sec*1000000 + (long long)now.tv_usec;
        numframes++;
    }

close_file:
    for (i=0; i<numfiles; i++)
        freebits(bb[i]);

    rvfree(macro);
    for (i=0; i<numfiles; i++) {
        rvfree(index[i]);
    }
    for (i=0; i<numfiles; i++) {
        rvfree(picture[i]);
        free_m2v_sequence(sequence[i]);
    }
    for (i=0; i<numfiles; i++) {
        free_264_sequence(avc_sequence[i]);
        rvfree(luddata[i]);
    }
    //ludh264_destroy();
    for (i=0; i<numfiles; i++) {
        if (fh[i])
            fclose(fh[i]);
#ifdef HAVE_LIBDE265
        if (decoder[i])
            de265_free_decoder(decoder[i]);
        if (video[i]) {
            free_hevc_video(video[i]);
        }
#endif
    }
    for (i=0; i<numfiles; i++) {
        for (int j=0; j<MAXFRAMES; j++)
            if (decodedframe[i][j]) {
                rvfree(decodedframe[i][j]);
                decodedframe[i][j] = NULL;
            }
    }

    /* report statistics on playback */
    if (verbose>=0) {
        int max = 0;
        for (i=0; i<numfiles; i++)
            if (lastframe[i]>max)
                max = lastframe[i];
        rvmessage("displayed %d frames", max+1);
    }
    if (verbose>=1 && numframes>2) {
        long long ssq=0, ssd=0, mean, var;
        int numsamples=NUMSAMPLES;
        numsamples = mmin(numsamples, numframes);
        for (i=1; i<numsamples; i++)
            ssq += timestats[i] - timestats[i-1];
        mean = ssq/(numsamples-1);
        for (i=1; i<numsamples; i++) {
            long long diff = timestats[i] - timestats[i-1] - mean;
            ssd += diff*diff;
        }
        var = ssd/(numsamples-2);
        rvmessage("frame timing: intended %d usecs, mean %lld usecs, variance %lld",
            (int)(1000000.0/displayrate+0.5), mean, var);
    }

    /* tidy up */
    free_ximage(&window);
    free_display(&window);
    XCloseDisplay(display);

    return 0;
}
