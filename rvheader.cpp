/*
 * Description: Parse the header data of various video coding standards.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-06-23 13:26:19 $
 * Revision   : $Revision: 1.37 $
 * Copyright  : (c) 2005 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>

#include "rvutil.h"
#include "rvbits.h"
#include "rvheader.h"

/*
 * bitmap headers
 */

int parse_bmp(struct bitbuf *bb, int *width, int *height, int *bits, int *compression)
{
    int signature = getword(bb);    /* signature */
    if (signature != 0x4D42) {
        rvmessage("bitmap header: incorrect signature: 0x%04x", signature);
        return -1;
    }
    flushbits(bb, 32);              /* size */
    flushbits(bb, 16);              /* reserved1 */
    flushbits(bb, 16);              /* reserved2 */
    flushbits(bb, 32);              /* offset */
    flushbits(bb, 32);              /* headersize */
    *width = getdword(bb);          /* width */
    *height = getdword(bb);         /* height */
    flushbits(bb, 16);              /* planes */
    *bits = getword(bb);            /* bits */
    *compression = getdword(bb);    /* compression */
    flushbits(bb, 32);              /* datasize */
    flushbits(bb, 32);              /* hres */
    flushbits(bb, 32);              /* vres */
    flushbits(bb, 32);              /* colours */
    flushbits(bb, 32);              /* important_colours */

    return 0;
}

/*
 * sgi image file headers
 */

int parse_sgi(struct bitbuf *bb, int *width, int *height, int *channels, int *rle, int *bpc)
{
    int magic = getbits(bb, 16);    /* magic */
    if (magic != 474) {
        rvmessage("sgi header: incorrect magic: 0x%d", magic);
        return -1;
    }
    *rle = getbits8(bb);            /* storage format */
    if (*rle!=0 && *rle!=1) {
        rvmessage("sgi header: invalid value of storage, should only be 0 or 1: %d", *rle);
        return -1;
    }
    *bpc = getbits8(bb);            /* number of bytes per pixel channel */
    if (*bpc!=1 && *bpc!=2) {
        rvmessage("sgi header: invalid value of bpc, should only be 1 or 2: %d", *bpc);
        return -1;
    }
    flushbits(bb, 16);              /* number of dimensions */
    *width = getbits(bb, 16);       /* x size in pixels */
    *height = getbits(bb, 16);      /* y size in pixels */
    *channels = getbits(bb, 16);    /* number of channels */
    flushbits(bb, 32);              /* minimum pixel value */
    flushbits(bb, 32);              /* maximum pixel value */
    flushbits(bb, 32);              /* ignored */
    flushbits(bb, 80*8);            /* image name */
    flushbits(bb, 32);              /* colormap id */
    flushbits(bb, 404*8);           /* ignored */

    return 0;
}

/*
 * avi headers.
 */

int parse_list(struct bitbuf *bb, int verbose, int *type, int size)
{
    /* read list type */
    *type = getdword(bb);
    if (verbose>=2) {
        rvmessage("LIST %s with %d bytes", describe_fourcc(*type), size);
    }
    return 0;
}

int parse_avih(struct bitbuf *bb, int verbose, int *frames, int *width, int *height)
{
    int usecs = getdword(bb);       /* microseconds per frame */
    int bps = getdword(bb);         /* maximum bytes per sec */
    flushbits(bb, 32);              /* padding granularity */
    flushbits(bb, 32);              /* flags */
    *frames = getdword(bb);         /* total frames */
    flushbits(bb, 32);              /* initial frames */
    int streams = getdword(bb);     /* number of streams */
    flushbits(bb, 32);              /* suggested buffer size */
    *width = getdword(bb);          /* width in pixels */
    *height = getdword(bb);         /* height in pixels */
    flushbits(bb, 32*4);
    if (verbose>=1) {
        rvmessage("avi file has %d %s", streams, streams>1? "streams" : "stream");
        rvmessage("avi video is %dx%d @%.2ffps max %dbps with %d frames", *width, *height, 1000000.0/usecs, bps*8, *frames);
    }
    return 0;
}

int parse_strh(struct bitbuf *bb, int verbose, int index, int *type)
{
    *type = getdword(bb);           /* FOURCC type */
    int handler = getdword(bb);     /* FOURCC handler */
    flushbits(bb, 32);              /* flags */
    flushbits(bb, 32);              /* priority and language */
    flushbits(bb, 32);              /* initial frames */
    flushbits(bb, 32);              /* scale */
    flushbits(bb, 32);              /* rate */
    flushbits(bb, 32);              /* start */
    flushbits(bb, 32);              /* length */
    flushbits(bb, 32);              /* suggested buffer size */
    flushbits(bb, 32);              /* quality */
    flushbits(bb, 32);              /* samplesize */
    flushbits(bb, 32);              /* left, top */
    flushbits(bb, 32);              /* right, bottom */
    if (verbose>=1)
        rvmessage("stream %d is %s with fourcc %s", index, fourccname(*type), fourccname(handler));
    return 0;
}

static const char *describe_compression[] = {
    "BI_RGB",
    "BI_RLE4",
    "BI_RLE8",
    "BI_BITFIELDS"
};

int parse_strf_vids(struct bitbuf *bb, int verbose)
{
    /* BITMAPINFOHEADER */
    int size = getdword(bb);    /* size in bytes */
    int width = getdword(bb);   /* width in pixels */
    int height = getdword(bb);  /* height in pixels, negative for top-down */
    flushbits(bb, 16);          /* number of planes */
    int bpp = getword(bb);      /* bits-per-pixel */
    int comp = getdword(bb);    /* type of compression */
    flushbits(bb, 32);          /* size in bytes */
    flushbits(bb, 32);          /* horizontal resolution */
    flushbits(bb, 32);          /* vertical resolution */
    flushbits(bb, 32);          /* colours used */
    flushbits(bb, 32);          /* colours required */
    if (verbose>=1)
        rvmessage("video stream format is %dx%d %d-bit with %s compression", width, height, bpp, describe_compression[comp]);
    if (size>40)
        flushbits(bb, (size-40)*8);
    return 0;
}

int parse_strf_auds(struct bitbuf *bb, int verbose)
{
    /* WAVEFORMATEX */
    getword(bb);                /* audio format type */
    int channels = getword(bb); /* number of channels */
    int samples = getdword(bb); /* sample rate */
    int abps = getdword(bb);    /* average bytes/sec */
    getword(bb);                /* block alignment */
    int bps = getword(bb);      /* bits per sample */
    int size = getword(bb);     /* extra format size in bytes */
    if (verbose>=1)
        rvmessage("audio stream format is %d-bit %s %.2fkHz @ %.1fkbps", bps, channels==1? "mono" : "stereo", (double)samples/1024.0, (double)abps*8.0/1024.0);
    if (size>0)
        flushbits(bb, size*8);
    return 0;
}

int parse_strn(struct bitbuf *bb, int verbose, int size)
{
    int i;
    size = (size + 1) & ~1;
    char *string = (char *)rvalloc(NULL, size, 0);
    for (i=0; i<size; i++)
        string[i] = getbyte(bb);
    if (verbose>=1)
        rvmessage("%s", string);
    return 0;
}

/*
 * jpeg headers.
 */

int parse_sof(struct bitbuf *bb, int *width, int *height, chromaformat_t *format)
{
    int i;
    int h[3], v[3];

    int len = 8*getbits(bb, 16) - 16;
    len -= flushbits(bb, 8);                                            /* sample precision */
    *height = getbits(bb, 16);                                          /* number of lines */
    len -= 16;
    *width = getbits(bb, 16);                                           /* number of samples */
    len -= 16;
    int comps = getbits8(bb);                                           /* number of components */
    len -= 8;
    for (i=0; i<comps; i++) {
        len -= flushbits(bb, 8);
        /* only store sampling factors for first three components */
        if (i<3) {
            h[i] = getbits(bb, 4);
            v[i] = getbits(bb, 4);
            len -= 8;
        } else {
            len -= flushbits(bb, 4);
            len -= flushbits(bb, 4);
        }
        len -= flushbits(bb, 8);
    }
    /* determine image format */
    *format = UNKNOWN;
    switch (comps) {
        case 1: *format = YUV400; break;
        case 3: if (h[0]==2 && v[0]==2) *format = YUV420;
                if (h[0]==2 && v[0]==1) *format = YUV422;
                break;
    }
    return len; /* should be zero */
}

int parse_dht(struct bitbuf *bb, struct huffman_table (*t)[2][4])
{
    int i,j;
    int tableclass, destination;

    int len = 8*getbits(bb, 16) - 16;
    do {
        tableclass = getbits(bb, 4);                                    /* table class */
        len -= 4;
        if (tableclass>1) {
            rvmessage("jpeg dht header: cannot have a huffman table class of %d", tableclass);
            return -1;
        }
        destination = getbits(bb, 4);                                   /* table destination */
        len -= 4;
        if (destination>3) {
            rvmessage("jpeg dht header: cannot have a huffman table destination of %d", destination);
            return -1;
        }
        for (i=0; i<16; i++) {
            ((*t)[tableclass][destination]).l[i] = getbits8(bb);        /* number of huffman codes */
            len -= 8;
        }
        for (i=0; i<16; i++) {
            for (j=0; j<((*t)[tableclass][destination]).l[i]; j++) {
                ((*t)[tableclass][destination]).v[i][j] = getbits8(bb); /* value of huffman code */
                len -= 8;
            }
        }
    } while (len>0);
    return len; /* should be zero */
}

/*
 * h.263 headers.
 */

int parse_263(struct bitbuf *bb, int *width, int *height, int *pct, int *umv, int *sac, int *apm, int *pb,
             int *aic, int *df, int *ss, int *rps, int *isd, int *aiv, int *mq, int *ipb, int *rpr, int *rru)
{
    flushbits(bb, 8);                                                   /* temporal reference */
    int ptype = getbits8(bb);                                           /* type information */
    if (ptype>>6 != 2) {
        rvmessage("h.263 picture header: ptype bits 1&2 not correct: %d", ptype>>6);
        return -1;
    }
    int plusptype = 0;
    switch (ptype&0x7) {
        case 0: rvmessage("h.263 picture header: forbidden source core format: %d", ptype&0x7); return -1;
        case 1: *width = 128; *height = 96; break;
        case 2: *width = 176; *height = 144; break;
        case 3: *width = 352; *height = 288; break;
        case 4: *width = 704; *height = 576; break;
        case 5: *width = 1408; *height = 1152; break;
        case 6: rvmessage("h.263 picture header: reserved source core format: %d", ptype&0x7); return -1;
        case 7: plusptype = 1; break;
    }
    int custom = 0;
    int pcf = 0;
    if (!plusptype) {
        *pct = getbit(bb);
        *umv = getbit(bb);
        *sac = getbit(bb);
        *apm = getbit(bb);
        *pb  = getbit(bb);
        *aic = 0;
        *df  = 0;
        *ss  = 0;
        *rps = 0;
        *isd = 0;
        *aiv = 0;
        *mq  = 0;
        *ipb = 0;
        *rpr = 0;
        *rru = 0;
    } else {
        *pb  = 0;
        *ipb = 0;
        int ufep = getbits(bb, 3);
        if (ufep==0) {
            *umv = 0;
            *sac = 0;
            *apm = 0;
            *aic = 0;
            *df  = 0;
            *ss  = 0;
            *rps = 0;
            *isd = 0;
            *aiv = 0;
            *mq  = 0;
        } else if (ufep==1) {
            int opptype = getbits(bb, 18);
            switch (opptype>>15) {
                case 0: rvmessage("h.263 picture header: reserved source core format: %d", opptype>>15); return -1;
                case 1: *width = 128; *height = 96; break;
                case 2: *width = 176; *height = 144; break;
                case 3: *width = 352; *height = 288; break;
                case 4: *width = 704; *height = 576; break;
                case 5: *width = 1408; *height = 1152; break;
                case 6: custom = 1; break;
                case 7: rvmessage("h.263 picture header: reserved source core format: %d", opptype>>15); return -1;
            }
            pcf  = opptype&0x4000? 1 : 0;
            *umv = opptype&0x2000? 1 : 0;
            *sac = opptype&0x1000? 1 : 0;
            *apm = opptype&0x0800? 1 : 0;
            *aic = opptype&0x0400? 1 : 0;
            *df  = opptype&0x0200? 1 : 0;
            *ss  = opptype&0x0100? 1 : 0;
            *rps = opptype&0x0080? 1 : 0;
            *isd = opptype&0x0040? 1 : 0;
            *aiv = opptype&0x0020? 1 : 0;
            *mq  = opptype&0x0010? 1 : 0;
        } else if (ufep>1) {
            rvmessage("h.263 picture header: reserved value of ufep: %d", ufep);
            return -1;
        }
        int mpptype = getbits(bb, 9);
        switch (mpptype>>6) {
            case 0: break;
            case 1: break;
            case 2: *ipb = 1; break;
            case 3:
            case 4:
            case 5: /* annex o */ break;
            case 6:
            case 7: rvmessage("h.263 picture header: reserved value of picture type code in mpptype: %d", mpptype>>6); return -1;
        }
        *rpr = mpptype&0x20 ? 1 : 0;
        *rru = mpptype&0x10 ? 1 : 0;
    }
    if (!plusptype)
        flushbits(bb, 5);                                               /* pquant */
    int cpm = getbit(bb);
    if (cpm)
        flushbits(bb, 2);                                               /* psbi */
    if (custom) {
        /* custom picture format, cpfmt */
        /*int par =*/ getbits(bb, 4);
        int pwi = getbits(bb, 9);
        if (getbit(bb)!=1)
            rvmessage("h.263 picture header: custom picture format: invalid value of bit 14: 0");
        int phi = getbits(bb, 9);
        *width  = (pwi + 1) * 4;
        *height = phi * 4;
    }
    if (pcf)
    flushbits(bb, 8);                                                   /* cpcfc */
    if (*pb || *ipb) {
        flushbits(bb, pcf? 5 : 3);                                      /* trb */
        flushbits(bb, 2);                                               /* dbquant */
    }
    int pei = getbit(bb);                                               /* pei */
    while (pei) {
        //printf("Annex L\n");
        flushbits(bb, 8);                                               /* psupp */
        pei = getbit(bb);                                               /* pei */
    }
    return 0;
}

/*
 * mpeg4 headers.
 */

int parse_user_data(struct bitbuf *bb, int *bytes, char *user_data, int num_elements)
{
    int i;
    for (i=0; showbits24(bb) != 1; i++) {
        char data = getbits8(bb);               /* user_data */
        if (i<num_elements)
            user_data[i] = data;
    }
    *bytes = i;
    return 0;
}

int parse_visual_object_sequence(struct bitbuf *bb, int *profile)
{
    *profile = getbits8(bb);                    /* profile_and_level_indication */
    return 0;
}

int parse_visual_object(struct bitbuf *bb, int *visual_object_verid, int *visual_object_type)
{
    if (getbit(bb)) {                           /* is_visual_object_identifier */
        *visual_object_verid = getbits(bb, 4);  /* visual_object_verid */
        flushbits(bb, 3);                       /* visual_object_priority */
    } else {
        *visual_object_verid = 0;
    }
    *visual_object_type = getbits(bb, 4);       /* visual_object_type */
    return 0;
}

int parse_vo(struct bitbuf *bb)
{
    return 0;
}

int parse_define_ce(struct bitbuf *bb, struct ce_header *ce)
{
    ce->estimation_method = getbits(bb, 2);     /* estimation_method */
    if (ce->estimation_method==0 || ce->estimation_method==1) {
        if (!getbit(bb)) {                      /* shape_complexity_estimation_disable */
            ce->opaque      = getbit(bb);       /* opaque */
            ce->transparent = getbit(bb);       /* transparent */
            ce->intra_cae   = getbit(bb);       /* intra_cae */
            ce->inter_cae   = getbit(bb);       /* inter_cae */
            ce->no_update   = getbit(bb);       /* no_update */
            ce->upsampling  = getbit(bb);       /* upsampling */
        }
        if (!getbit(bb)) {                      /* texture_complexity_estimation_set_1_disable */
            ce->intra_blocks     = getbit(bb);  /* intra_blocks */
            ce->inter_blocks     = getbit(bb);  /* inter_blocks */
            ce->inter4v_blocks   = getbit(bb);  /* inter4v_blocks */
            ce->not_coded_blocks = getbit(bb);  /* not_coded_blocks */
        }
        flushbits(bb, 1);                       /* marker_bit */
        if (!getbit(bb)) {                      /* texture_complexity_estimation_set_2_disable */
            ce->dct_coefs   = getbit(bb);       /* dct_coefs */
            ce->dct_lines   = getbit(bb);       /* dct_lines */
            ce->vlc_symbols = getbit(bb);       /* vlc_symbols */
            ce->vlc_bits    = getbit(bb);       /* vlc_bits */
        }
        if (!getbit(bb)) {                      /* motion_compensation_complexity_disable */
            ce->apm              = getbit(bb);  /* apm */
            ce->npm              = getbit(bb);  /* npm */
            ce->interpolate_mc_q = getbit(bb);  /* interpolate_mc_q */
            ce->forw_back_mc_q   = getbit(bb);  /* forw_back_mc_q */
            ce->halfpel2         = getbit(bb);  /* halfpel2 */
            ce->halfpel4         = getbit(bb);  /* halfpel4 */
        }
        flushbits(bb, 1);                       /* marker_bit */
        if (ce->estimation_method==1) {
            if (!getbit(bb)) {                  /* version2_complexity_estimation_disable */
                ce->sadct      = getbit(bb);    /* sadct */
                ce->quarterpel = getbit(bb);    /* quarterpel */
            }
        }
    }
    return 0;
}

int parse_vol(struct bitbuf *bb, int *video_object_type, int *video_object_verid, int *video_object_shape,
              int *vti_size, int *width, int *height, int *interlaced, int *obmc_disable, int *sprite_enable,
              int *quant_type, int *quarter_sample, int *data_partitioned, int *rvlc,
              int *scalability, int *enhancement_type, int *ce_disable, struct ce_header *ce)
{
    int i;

    if (*video_object_verid!=2)
        *video_object_verid = 1;

    flushbits(bb, 1);                           /* random_accessible_vol */
    *video_object_type = getbits8(bb);          /* video_object_type_indication */
    if (getbit(bb)) {                           /* is_object_layer_identifier */
        *video_object_verid = getbits(bb, 4);   /* visual_object_verid */
        flushbits(bb, 3);                       /* visual_object_priority */
    }
    int aspect_ratio_info = getbits(bb, 4);
    if (aspect_ratio_info==0xf) {               /* aspect_ratio_info */
        flushbits(bb, 8);                       /* par_width */
        flushbits(bb, 8);                       /* par_height */
    }
    if (getbit(bb)) {                           /* vol_control_parameters */
        flushbits(bb, 2);                       /* chroma_format */
        flushbits(bb, 1);                       /* low_delay */
        if(getbit(bb)) {                        /* vbv_parameters */
            flushbits(bb, 15);                  /* first_half_bit_rate */
            flushbits(bb, 1);                   /* marker_bit */
            flushbits(bb, 15);                  /* latter_half_bit_rate */
            flushbits(bb, 1);                   /* marker_bit */
            flushbits(bb, 15);                  /* first_half_vbv_buffer_size */
            flushbits(bb, 1);                   /* marker_bit */
            flushbits(bb, 3);                   /* latter_half_vbv_buffer_size */
            flushbits(bb, 11);                  /* first_half_vbv_occupancy */
            flushbits(bb, 1);                   /* marker_bit */
            flushbits(bb, 15);                  /* latter_half_vbv_occupancy */
            flushbits(bb, 1);                   /* marker_bit */
        }
    }
    *video_object_shape = getbits(bb, 2);       /* video_object_layer_shape */
    if(*video_object_shape==GRAYSCALE && *video_object_verid!=1)
        flushbits(bb, 4);                       /* video_object_layer_shape_extension */
    flushbits(bb, 1);                           /* marker_bit */
    int vti_res = getbits(bb, 16);              /* vop_time_increment_resolution */
    flushbits(bb, 1);                           /* marker_bit */
    for (*vti_size=1; 1<<(*vti_size) < vti_res; (*vti_size)++);
    if (getbit(bb))                             /* fixed_vop_rate */
        flushbits(bb, *vti_size);               /* fixed_vop_time_increment */
    if (*video_object_shape!=BINARY_ONLY) {
        if (*video_object_shape==RECTANGULAR) {
            flushbits(bb, 1);                   /* marker_bit */
            *width = getbits(bb, 13);           /* video_object_layer_width */
            flushbits(bb, 1);                   /* marker_bit */
            *height = getbits(bb, 13);          /* video_object_layer_height */
            flushbits(bb, 1);                   /* marker_bit */
        }
        *interlaced = getbit(bb);               /* interlaced */
        *obmc_disable = getbit(bb);             /* obmc_disable */
        if (*video_object_verid==1)
            *sprite_enable = getbits(bb, 1);    /* sprite_enable */
        else
            *sprite_enable = getbits(bb, 2);    /* sprite_enable */
        if (*sprite_enable==STATIC || *sprite_enable==GMC) {
            if (*sprite_enable!=GMC) {
                flushbits(bb, 13);              /* sprite_width */
                flushbits(bb, 1);               /* marker_bit */
                flushbits(bb, 13);              /* sprite_height */
                flushbits(bb, 1);               /* marker_bit */
                flushbits(bb, 13);              /* sprite_left_coordinate */
                flushbits(bb, 1);               /* marker_bit */
                flushbits(bb, 13);              /* sprite_top_coordinate */
                flushbits(bb, 1);               /* marker_bit */
            }
            flushbits(bb, 6);                   /* no_of_sprite_warping_points */
            flushbits(bb, 2);                   /* sprite_warping_accuracy */
            flushbits(bb, 1);                   /* sprite_brightness_change */
            if (*sprite_enable!=GMC)
                flushbits(bb, 1);               /* low_latency_sprite_enable */
        }
        if (*video_object_verid!=1 && *video_object_shape!=RECTANGULAR)
            flushbits(bb, 1);                   /* sadct_disable */
        if (getbit(bb)) {                       /* not_8_bit */
            flushbits(bb, 4);                   /* quant_precision */
            flushbits(bb, 4);                   /* bits_per_pixel */
        }
        if (*video_object_shape==GRAYSCALE) {
            flushbits(bb, 1);                   /* no_gray_quant_update */
            flushbits(bb, 1);                   /* composition_method */
            flushbits(bb, 1);                   /* linear_composition */
        }
        *quant_type = getbit(bb);               /* quant_type */
        if (*quant_type) {
            if (getbit(bb)) {                   /* load_intra_quant_mat */
                for (i=0; i<64; i++)
                    if (getbits8(bb)==0)
                        break;
            }
            if (getbit(bb)) {                   /* load_nonintra_quant_mat */
                for (i=0; i<64; i++)
                    if (getbits8(bb)==0)
                        break;
            }
            if(*video_object_shape==GRAYSCALE) {
                /* TODO */
            }
        }
        if (*video_object_verid!=1)
            *quarter_sample = flushbits(bb, 1); /* quarter_sample */
        else
            *quarter_sample = 0;
        *ce_disable = getbit(bb);               /* complexity_estimation_disable */
        if (!(*ce_disable))
            parse_define_ce(bb, ce);
        getbit(bb);                             /* resync_marker_disable */
        *data_partitioned = getbit(bb);
        if (*data_partitioned)
            *rvlc = getbit(bb);
        else
            *rvlc = 0;
        if (*video_object_verid!=1) {
            if (getbit(bb)) {                   /* newpred_enable */
                flushbits(bb, 2);               /* requested_upstream_message_type */
                flushbits(bb, 1);               /* newpred_segment_type */
            }
            flushbits(bb, 1);                   /* reduced_resolution_vop_enable */
        }
        *scalability = getbit(bb);              /* scalability */
        if (*scalability) {
            int hierarchy_type = getbit(bb);    /* hierarchy_type */
            flushbits(bb, 4);                   /* ref_layer_id */
            flushbits(bb, 1);                   /* ref_layer_sampling_direc */
            flushbits(bb, 5);                   /* hor_sampling_factor_n */
            flushbits(bb, 5);                   /* hor_sampling_factor_m */
            flushbits(bb, 5);                   /* vert_sampling_factor_n */
            flushbits(bb, 5);                   /* vert_sampling_factor_m */
            *enhancement_type = getbit(bb);     /* enhancement_type */
            if(*video_object_shape==BINARY && hierarchy_type==0) {
                flushbits(bb, 1);               /* use_ref_shape */
                flushbits(bb, 1);               /* use_ref_texture */
                flushbits(bb, 5);               /* shape_hor_sampling_factor_n */
                flushbits(bb, 5);               /* shape_hor_sampling_factor_m */
                flushbits(bb, 5);               /* shape_vert_sampling_factor_n */
                flushbits(bb, 5);               /* shape_vert_sampling_factor_m */
            }
        } else {
            *enhancement_type = 0;
        }
    } else {
        if(*video_object_verid!=1) {
            if(getbit(bb)) {                    /* scalability */
                flushbits(bb, 4);               /* ref_layer_id */
                flushbits(bb, 5);               /* shape_hor_sampling_factor_n */
                flushbits(bb, 5);               /* shape_hor_sampling_factor_m */
                flushbits(bb, 5);               /* shape_vert_sampling_factor_n */
                flushbits(bb, 5);               /* shape_vert_sampling_factor_m */
            }
        }
        flushbits(bb, 1);                       /* resync_marker_disable */
    }
    return 0;
}

void parse_ce_header(struct bitbuf *bb, int vop_coding_type, int sprite_enable, struct ce_header *ce)
{
    if (ce->estimation_method==0) {
        if (vop_coding_type==VOP_I) {
            if (ce->opaque)             flushbits(bb, 8);   /* dcecs_opaque */
            if (ce->transparent)        flushbits(bb, 8);   /* dcecs_transparent */
            if (ce->intra_cae)          flushbits(bb, 8);   /* dcecs_intra_cae */
            if (ce->inter_cae)          flushbits(bb, 8);   /* dcecs_inter_cae */
            if (ce->no_update)          flushbits(bb, 8);   /* dcecs_no_update */
            if (ce->upsampling)         flushbits(bb, 8);   /* dcecs_upsampling */
            if (ce->intra_blocks)       flushbits(bb, 8);   /* dcecs_intra_blocks */
            if (ce->not_coded_blocks)   flushbits(bb, 8);   /* dcecs_not_coded_blocks */
            if (ce->dct_coefs)          flushbits(bb, 8);   /* dcecs_dct_coefs */
            if (ce->dct_lines)          flushbits(bb, 8);   /* dcecs_dct_lines */
            if (ce->vlc_symbols)        flushbits(bb, 4);   /* dcecs_vlc_symbols */
            if (ce->vlc_bits)           flushbits(bb, 8);   /* dcecs_vlc_bits */
            if (ce->sadct)              flushbits(bb, 8);   /* dcecs_sadct */
        }
        if (vop_coding_type==VOP_P) {
            if (ce->opaque)             flushbits(bb, 8);   /* dcecs_opaque */
            if (ce->transparent)        flushbits(bb, 8);   /* dcecs_transparent */
            if (ce->intra_cae)          flushbits(bb, 8);   /* dcecs_intra_cae */
            if (ce->inter_cae)          flushbits(bb, 8);   /* dcecs_inter_cae */
            if (ce->no_update)          flushbits(bb, 8);   /* dcecs_no_update */
            if (ce->upsampling)         flushbits(bb, 8);   /* dcecs_upsampling */
            if (ce->intra_blocks)       flushbits(bb, 8);   /* dcecs_intra_blocks */
            if (ce->not_coded_blocks)   flushbits(bb, 8);   /* dcecs_not_coded_blocks */
            if (ce->dct_coefs)          flushbits(bb, 8);   /* dcecs_dct_coefs */
            if (ce->dct_lines)          flushbits(bb, 8);   /* dcecs_dct_lines */
            if (ce->vlc_symbols)        flushbits(bb, 8);   /* dcecs_vlc_symbols */
            if (ce->vlc_bits)           flushbits(bb, 4);   /* dcecs_vlc_bits */
            if (ce->inter_blocks)       flushbits(bb, 8);   /* dcecs_inter_blocks */
            if (ce->inter4v_blocks)     flushbits(bb, 8);   /* dcecs_inter4v_blocks */
            if (ce->apm)                flushbits(bb, 8);   /* dcecs_apm */
            if (ce->npm)                flushbits(bb, 8);   /* dcecs_npm */
            if (ce->forw_back_mc_q)     flushbits(bb, 8);   /* dcecs_forw_back_mc_q */
            if (ce->halfpel2)           flushbits(bb, 8);   /* dcecs_halfpel2 */
            if (ce->halfpel4)           flushbits(bb, 8);   /* dcecs_halfpel4 */
            if (ce->sadct)              flushbits(bb, 8);   /* dcecs_sadct */
            if (ce->quarterpel)         flushbits(bb, 8);   /* dcecs_quarterpel */
        }
        if (vop_coding_type==VOP_B) {
            if (ce->opaque)             flushbits(bb, 8);   /* dcecs_opaque */
            if (ce->transparent)        flushbits(bb, 8);   /* dcecs_transparent */
            if (ce->intra_cae)          flushbits(bb, 8);   /* dcecs_intra_cae */
            if (ce->inter_cae)          flushbits(bb, 8);   /* dcecs_inter_cae */
            if (ce->no_update)          flushbits(bb, 8);   /* dcecs_no_update */
            if (ce->upsampling)         flushbits(bb, 8);   /* dcecs_upsampling */
            if (ce->intra_blocks)       flushbits(bb, 8);   /* dcecs_intra_blocks */
            if (ce->not_coded_blocks)   flushbits(bb, 8);   /* dcecs_not_coded_blocks */
            if (ce->dct_coefs)          flushbits(bb, 8);   /* dcecs_dct_coefs */
            if (ce->dct_lines)          flushbits(bb, 8);   /* dcecs_dct_lines */
            if (ce->vlc_symbols)        flushbits(bb, 8);   /* dcecs_vlc_symbols */
            if (ce->vlc_bits)           flushbits(bb, 4);   /* dcecs_vlc_bits */
            if (ce->inter_blocks)       flushbits(bb, 8);   /* dcecs_inter_blocks */
            if (ce->inter4v_blocks)     flushbits(bb, 8);   /* dcecs_inter4v_blocks */
            if (ce->apm)                flushbits(bb, 8);   /* dcecs_apm */
            if (ce->npm)                flushbits(bb, 8);   /* dcecs_npm */
            if (ce->forw_back_mc_q)     flushbits(bb, 8);   /* dcecs_forw_back_mc_q */
            if (ce->halfpel2)           flushbits(bb, 8);   /* dcecs_halfpel2 */
            if (ce->halfpel4)           flushbits(bb, 8);   /* dcecs_halfpel4 */
            if (ce->interpolate_mc_q)   flushbits(bb, 8);   /* dcecs_interpolate_mc_q */
            if (ce->sadct)              flushbits(bb, 8);   /* dcecs_sadct */
            if (ce->quarterpel)         flushbits(bb, 8);   /* dcecs_quarterpel */
        }
        if (vop_coding_type==VOP_S && sprite_enable==STATIC) {
            if (ce->intra_blocks)       flushbits(bb, 8);   /* dcecs_intra_blocks */
            if (ce->not_coded_blocks)   flushbits(bb, 8);   /* dcecs_not_coded_blocks */
            if (ce->dct_coefs)          flushbits(bb, 8);   /* dcecs_dct_coefs */
            if (ce->dct_lines)          flushbits(bb, 8);   /* dcecs_dct_lines */
            if (ce->vlc_symbols)        flushbits(bb, 8);   /* dcecs_vlc_symbols */
            if (ce->vlc_bits)           flushbits(bb, 4);   /* dcecs_vlc_bits */
            if (ce->inter_blocks)       flushbits(bb, 8);   /* dcecs_inter_blocks */
            if (ce->inter4v_blocks)     flushbits(bb, 8);   /* dcecs_inter4v_blocks */
            if (ce->apm)                flushbits(bb, 8);   /* dcecs_apm */
            if (ce->npm)                flushbits(bb, 8);   /* dcecs_npm */
            if (ce->forw_back_mc_q)     flushbits(bb, 8);   /* dcecs_forw_back_mc_q */
            if (ce->halfpel2)           flushbits(bb, 8);   /* dcecs_halfpel2 */
            if (ce->halfpel4)           flushbits(bb, 8);   /* dcecs_halfpel4 */
            if (ce->interpolate_mc_q)   flushbits(bb, 8);   /* dcecs_interpolate_mc_q */
        }
    }
}

int parse_group_of_vop(struct bitbuf *bb)
{
    flushbits(bb, 18);                          /* time_code */
    flushbits(bb, 1);                           /* closed_gov */
    flushbits(bb, 1);                           /* broken_link */
    return 0;
}

int parse_vop(struct bitbuf *bb, int vti_size, int video_object_shape, int sprite_enable, int scalability, int enhancement_type,
              int ce_disable, struct ce_header *ce, int interlaced, int *vop_coding_type, int *vti, int *fcode)
{
    const char *where = "mpeg4 vop header";
    *vop_coding_type = getbits(bb, 2);          /* vop_coding_type */
    int modulo_time_base;
    do {
        modulo_time_base = getbit(bb);          /* modulo_time_base */
    } while (modulo_time_base!=0);
    flushbits(bb, 1);                           /* marker_bit */
    *vti = getbits(bb, vti_size);               /* vop_time_increment */
    flushbits(bb, 1);                           /* marker_bit */
    if (getbit(bb)==0)                          /* vop_coded */
        return 1;
#if 0
    if (newpred_enable) {
        flushbits(bb, );                        /* vop_id */
        if (getbit(bb)) {                       /* vop_id_for_prediction_indication */
            flushbits(bb,);                     /* vop_id_for_prediction */
            flushbits(bb, 1);                   /* marker_bit */
        }
    }
#endif
    if ((video_object_shape!=BINARY_ONLY) && (*vop_coding_type==VOP_P || (*vop_coding_type==VOP_S && sprite_enable==GMC)))
        flushbits(bb, 1);                       /* vop_rounding_type */
#if 0
    if ((reduced_resolution_vop_enable) && (video_object_shape==RECTANGULAR) && ((*vop_coding_type==VOP_P) || (*vop_coding_type==VOP_I)))
        flushbits(bb, 1);/* vop_reduced_resolution */
#endif
    if (video_object_shape!=RECTANGULAR) {
        if(!(sprite_enable==STATIC && *vop_coding_type==VOP_I)) {
            flushbits(bb, 13);              /* vop_width */
            flushbits(bb, 1);               /* marker_bit */
            flushbits(bb, 13);              /* vop_height */
            flushbits(bb, 1);               /* marker_bit */
            flushbits(bb, 13);              /* vop_horizontal_mc_spatial_ref */
            flushbits(bb, 1);               /* marker_bit */
            flushbits(bb, 13);              /* vop_vertical_mc_spatial_ref */
            flushbits(bb, 1);               /* marker_bit */
        }
        if ((video_object_shape!=BINARY_ONLY) && scalability && enhancement_type)
            flushbits(bb, 1);               /* background_composition */
        flushbits(bb, 1);                   /* change_conv_ratio_disable */
        if (getbit(bb))                     /* vop_constant_alpha */
            flushbits(bb, 8);               /* vop_constant_alpha_value */
    }
    if (video_object_shape!=BINARY_ONLY)
        if (!ce_disable)
            parse_ce_header(bb, *vop_coding_type, sprite_enable, ce);
    if (video_object_shape!=BINARY_ONLY) {
        flushbits(bb, 3);                   /* intra_dc_vlc_thr */
        if (interlaced) {
            flushbits(bb, 1);               /* top_field_first */
            flushbits(bb, 1);               /* alternate_vertical_scan_flag */
        }
    }
#if 0
    if ((sprite_enable==STATIC || sprite_enable==GMC) && *vop_coding_type==VOP_S) {
        if (no_of_sprite_warping_points > 0)
            sprite_trajectory()
        if (sprite_brightness_change)
            brightness_change_factor()
        if (sprite_enable==STATIC) {
            if (sprite_transmit_mode!=STOP && low_latency_sprite_enable) {
                do {
                    /* sprite_transmit_mode */
                    if ((sprite_transmit_mode==PIECE) || (sprite_transmit_mode==UPDATE))
                        decode_sprite_piece()
                } while (sprite_transmit_mode!=STOP && sprite_transmit_mode!=PAUSE)
            }
            next_start_code()
            return()
        }
    }
#endif
    if (video_object_shape!=BINARY_ONLY) {
        flushbits(bb, 5);                   /* vop_quant */
#if 0
        if(video_object_shape==GRAYSCALE)
            for(i=0; i<aux_comp_count; i++)
                flushbits(bb, 6);           /* vop_alpha_quant[i] */
#endif
        if (*vop_coding_type!=VOP_I) {
            *fcode = getbits(bb, 3);        /* vop_fcode_forward */
            if (*fcode==0)
                rvmessage("%s: vop_fcode_forward is zero", where);
        } else
            *fcode = 0;
        if (*vop_coding_type==VOP_B)
            flushbits(bb, 3);               /* vop_fcode_backward */
        if (!scalability) {
            if (video_object_shape!=RECTANGULAR && *vop_coding_type!=VOP_I)
                flushbits(bb, 1);           /* vop_shape_coding_type */
            return 0;
        } else {
            if (enhancement_type) {
                /* load_backward_shape */
                /* backward_shape_width */
                /* marker_bit */
                /* backward_shape_ height */
                /* marker_bit */
                /* backward_shape_horizontal_mc_spatial_ref */
                /* marker_bit */
                /* backward_shape_vertical_mc_spatial_ref */
                /*backward_shape();*/
                if (getbit(bb)) {           /* load_forward_shape */
                    /* forward_shape_width */
                    /* marker_bit */
                    /* forward_shape_height */
                    /* marker_bit */
                    /* forward_shape_horizontal_mc_spatial_ref */
                    /* marker_bit */
                    /* forward_shape_vertical_mc_spatial_ref */
                    /*forward_shape();*/
                }
            }
        }
        /* ref_select_code */
        return 0;
    } else {
        *fcode = 0;
        return 0;
    }
}

int parse_vp(struct bitbuf *bb, int video_object_shape, int sprite_enable, int vop_coding_type, int vti_size)
{
    if (video_object_shape!=RECTANGULAR) {
        if (getbit(bb) &&                   /* header_extension_code */
                !(sprite_enable==STATIC && vop_coding_type==VOP_I)) {
            flushbits(bb, 13);              /* vop_width */
            flushbits(bb, 1);               /* marker_bit */
            flushbits(bb, 13);              /* vop_height */
            flushbits(bb, 1);               /* marker_bit */
            flushbits(bb, 13);              /* vop_horizontal_mc_spatial_ref */
            flushbits(bb, 1);               /* marker_bit */
            flushbits(bb, 13);              /* vop_vertical_mc_spatial_ref */
            flushbits(bb, 1);               /* marker_bit */
        }
    }
#if 0
    macroblock_number
    if (video_object_shape!=BINARY_ONLY)
        flushbits(bb, 5);                   /* quant_scale */
    if (video_object_shape==RECTANGULAR)
        if (getbit(bb)) {                   /* header_extension_code */
            int modulo_time_base;
            do {
                modulo_time_base = getbit(bb);
            } while (modulo_time_base!=0)
                flushbits(bb, 1);               /* marker_bit */
                flushbtis(bb, vti_size);        /* vop_time_increment */
                flushbits(bb, 1);               /* marker_bit */
                            vop_coding_typestatic
                            if (video_object_layer_shape != ?rectangular?) {
                        change_conv_ratio_disable
                                if (vop_coding_type != ?I?)
                                vop_shape_coding_type
                            }
                            if (video_object_layer_shape != ?binary only?) {
                                intra_dc_vlc_thr
                                        if (sprite_enable == ?GMC? && vop_coding_type == ?S?
                                        && no_of_sprite_warping_points > 0)
                                        sprite_trajectory()
                                        if ((reduced_resolution_vop_enable)
                                        && (video_object_layer_shape == ?rectangular?)
                                        && ((vop_coding_type == ?P?) || (vop_coding_type == ?I?)))
                                        vop_reduced_resolution
                                        if (vop_coding_type != ?I?)
                                        vop_fcode_forward
                                        if (vop_coding_type == ?B?)
                                        vop_fcode_backward
                            }
                    }
                    if (newpred_enable) {
                        vop_id
                                vop_id_for_prediction_indication
                                if (vop_id_for_prediction_indication)
                                vop_id_for_prediction
                                marker_bit
                    }
#endif
    return 0;
}
