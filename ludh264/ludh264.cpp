/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/



#include "ludh264.h"
#include "decode.h"
#include "system.h"


RetCode ludh264_init(struct rv264sequence *seq)
{
  return decoder_init(seq);
}

RetCode ludh264_destroy(struct rv264sequence *seq)
{
  return decoder_destroy(seq);
}

RetCode ludh264_reset(struct rv264sequence *seq)
{
  return decoder_reset(seq);
}

RetCode ludh264_parse_bytestream_nalu(struct rv264sequence *seq, uint8_t* stream_buffer, uint32_t* stream_available_consumed, uint8_t** nalubuf, uint32_t* nalusize, bool endofstream)
{
  uint8_t* src = stream_buffer;
  uint32_t lenin = *stream_available_consumed;
  uint32_t i = 0;
  uint32_t s;

  // Check for initial start code
  while(!src[i++] && i<lenin);
  if ( i < 3 || src[i-1] != 1 || i == lenin)
  {
    *nalubuf = 0;
    *nalusize = 0;
    *stream_available_consumed = 0;
    return LUDH264_ERR_NO_START_CODE;
  }
  s = i; // start of the nalu

  int found = 0;
  while (i < lenin-1)
  {
    if (src[i] > 1) // May this be a startcode? if not just jump 3 bytes ahead
    {
      i += 3;
      continue;
    }
    while(!src[i] && i<lenin) i++; // go forward until non null value (end of start code)
    if (src[i] == 1 && src[i-1] == 0 && src[i-2] == 0)
    {
      i--;
      while(!src[i-1]) i--; // rewind to the end of the nalu
      found = 1;
      break;
    }
    else // false detection
      i+= 3;
  }

  if (!found)
  {
    if (!endofstream)
    {
      *nalubuf = 0;
      *nalusize = 0;
      *stream_available_consumed = 0;
      return LUDH264_ERR_NO_ENDING_START_CODE;
    }
    else
    {
      *nalubuf = stream_buffer+s;
      *nalusize = *stream_available_consumed-s;
      return LUDH264_SUCCESS;
    }
  }

  *nalubuf = stream_buffer+s;
  *nalusize = i-s;
  *stream_available_consumed = i;

  return LUDH264_SUCCESS;
}

RetCode ludh264_decode_nalu(struct rv264sequence *seq, uint8_t* nalubuf, uint32_t nalusize, struct rv264picture** pic)
{
  RetCode r = decode_nalu(seq, nalubuf, nalusize);
  if (pic)
    *pic = get_pic(&seq->dpb);

  return r;
}

RetCode ludh264_get_picture_params(struct rv264sequence *seq, LUDH264_VIDEO_FORMAT* format, uint32_t* width, uint32_t* height)
{
  struct slice_header *sh = seq->pdd.prev_sh;
  if (!sh || !sh->sps)
    return LUDH264_ERR_NO_PICTURE_PARAMS;

  switch (sh->sps->chroma_format_idc) {
    case 1 : *format = LUDH264_YUV420; break;
    case 2 : *format = LUDH264_YUV422; break;
    case 3 : *format = LUDH264_YUV444; break;
  }
  *width = sh->sps->PicWidthInMbs * 16;
  *height = sh->sps->FrameHeightInMbs * 16;

  return LUDH264_SUCCESS;
}

RetCode ludh264_get_picture(struct rv264sequence *seq, struct rv264picture* pic, void** buffer, uint32_t* buffer_size, LUDH264_VIDEO_FORMAT* format, uint32_t* width, uint32_t* height)
{
  *buffer = pic->Y;
  *buffer_size = pic->image_buffer_size;
  *width = pic->width;
  *height = pic->height;

  // TODO: fills in format
  return LUDH264_SUCCESS;
}

RetCode ludh264_get_picture_metadata(struct rv264sequence *seq, struct rv264picture* pic, struct rv264seq_parameter_set **sps, struct rv264pic_parameter_set **pps, struct slice_header **sh, struct rv264macro **macro)
{
  if (!pic->field_sh[0])
    return LUDH264_ERR_NO_PICTURE_PARAMS;

  *sps   = pic->field_sh[0][0]->sps;
  *pps   = pic->field_sh[0][0]->pps;
  *sh    = pic->field_sh[0][0];
  *macro = pic->field_data[0].mb_attr;

  return LUDH264_SUCCESS;
}

RetCode ludh264_get_picture_metadata_bottom_field(struct rv264sequence *seq, struct rv264picture* pic, struct rv264seq_parameter_set **sps, struct rv264pic_parameter_set **pps, struct slice_header **sh, struct rv264macro **macro)
{
  if (!pic->field_sh[1])
    return LUDH264_ERR_NO_PICTURE_PARAMS;

  *sps   = pic->field_sh[1][0]->sps;
  *pps   = pic->field_sh[1][0]->pps;
  *sh    = pic->field_sh[1][0];
  *macro = pic->field_data[1].mb_attr;

  return LUDH264_SUCCESS;
}

RetCode ludh264_free_picture(struct rv264sequence *seq, struct rv264picture* pic)
{
  if (pic) {
    release_picture_dec_data(pic);
    release_picture(pic);
  }
  return LUDH264_SUCCESS;
}
