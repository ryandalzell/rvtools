/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/


#ifndef __COMMON_H__
#define __COMMON_H__

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>

#include "ludh264.h"
#include "rvutil.h"

#define   TRACE_LEVEL TRACE_ALL

/* use a global verbosity rather than threading it through the code */
extern int verbose;

// Common enum and types


typedef enum
{
  TRACE_NOTHING = -1,

  TRACE_ERROR,
  TRACE_INFO,
  TRACE_DETAIL,

  TRACE_ALL,
} TraceLevel;


// Object header: add necessary fields in the object that need to be used/released
#define OBJECT_HEADER_DECL                \
  uint32_t ref_count;

#define OBJECT_HEADER_INST(o)             \
  (o)->ref_count = 0;


// For picture, use specific functions instead
#define use_object(o)                     \
  do {                                    \
    (o)->ref_count++;                     \
  } while(0)





#define LUD_TRACE(lvl, ...)   \
do                                   \
{                                    \
  if (verbose>=lvl)                  \
    rvmessage(__VA_ARGS__);           \
} while(0)


#define unused_param __attribute__((unused))
#define declare_aligned(n)  __attribute__ ((aligned (n)))

#define LUD_DEBUG_ASSERT(a) assert(a)

#endif //__COMMON_H__
