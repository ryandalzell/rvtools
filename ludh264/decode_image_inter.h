/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/


#ifndef DECODE_IMAGE_INTER_H_
#define DECODE_IMAGE_INTER_H_


void refrence_picture_selection_process(int refIdxLX, RefListEntry* RefPicListX, unsigned int field_pic_flag,
    unsigned int mb_field_decoding_flag, unsigned int mb_row,
    RefListEntry** refPic, unsigned int* field);
void decoding_process_for_inter_prediction_samples(struct rv264macro* curr_mb_attr, slice_type_t slice_type_modulo5,
    unsigned int weighted_pred_flag, unsigned int weighted_bipred_idc, unsigned int mb_row, unsigned int mb_col, int FieldOrderCnt[2],
    RefListEntry* RefPicList[2], unsigned int field_pic_flag, unsigned int bottom_field_flag, unsigned int mb_field_decoding_flag,
    unsigned int MbaffFrameFlag, pixel_t* mb_Y_samples_, pixel_t* mb_C_samples_[2], pred_weight_table_t* wt,
    unsigned int PicWidthY, unsigned int PicWidthC, unsigned int strideY, unsigned int strideC, unsigned int PicHeightY, unsigned int PicHeightC,
    unsigned int clipY, unsigned int clipC,
    unsigned int cfidc, unsigned int mbc_width, unsigned int mbc_height);
void inter_mb_inverse_transform(struct rv264macro* curr_mb_attr, mb_type_t mb_type, int16_t* mb_data,
    pixel_t* mb_Y_samples, pixel_t* mb_C_samples[2],
    unsigned int cfidc, int clipY, int clipC, unsigned int strideY, unsigned int strideC);
void decode_inter_mb(struct rv264macro* curr_mb_attr, mb_type_t mb_type, int16_t* mb_data, unsigned int mb_row,
    unsigned int mb_col, unsigned int mb_field_decoding_flag, unsigned int MbaffFrameFlag,
    RefListEntry* RefPicList[2], unsigned int field_pic_flag, unsigned int bottom_field_flag, int FieldOrderCnt[2],
    slice_type_t slice_type_modulo5, unsigned int weighted_pred_flag, unsigned int weighted_bipred_idc,
    pixel_t* mb_Y_samples, pixel_t* mb_C_samples[2], unsigned int PicWidthY, unsigned int PicWidthC,
    unsigned int strideY, unsigned int strideC, unsigned int PicHeightY, unsigned int PicHeightC, unsigned int clipY, unsigned int clipC,
    unsigned int cfidc, unsigned int mbc_width, unsigned int mbc_height, pred_weight_table_t* wt);

#endif
