/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/

#include <string.h>

#include "common.h"
#include "decode.h"
#include "decode_slice_data.h"
#include "intmath.h"
#include "defaulttables.h"
#include "motion_vector.h"

// SET / GET the part size (width or height) in the macroblock partWidth/Height 'array' (a uint32_t in fact)
#define SET_PART_SIZE(array, size, idx) (array) |= ((size)>>1) << (idx<<1)


// refIdxVal is the value that is stored into refIdx_cache.
//  -2 means the corresponding mb/mbpart/submbpart was not avail
//  -1 means the corresponding mb is INTRA or predFlagLX==0, or ...
// This function does not reset the mv_cache since it should be initialized to zero
void reset_one_mv_cache_entry(int16_t mv_cache[2][30][2], int8_t refIdx_cache[2][30], unsigned int idx,
    int repeat_mv, unsigned int need_L1, int refIdxVal)
{
  int i, j;

  LUD_DEBUG_ASSERT(repeat_mv);
  LUD_DEBUG_ASSERT(idx < 30);

  for (i = 0; i < 1 + (need_L1 != 0); i++)
    // Should be unrolled at compilation time
    if (repeat_mv > 0)
      for (j = 0; j < repeat_mv; j++)
        refIdx_cache[i][idx + j] = refIdxVal;
    else
      for (j = 0; j < -repeat_mv * 6; j += 6)
        refIdx_cache[i][idx + j] = refIdxVal;
}

// This function helps not to rewrite too much code.
// It works efficiently supposing the compiler optimize and remove unnecessary code (most of the params
//  are hard coded at function call (at compilation time then !))
// repeat_mv = 0 => this value is not allowed !
//             n => n horiz samples are copied
//            -n => n vert samples are copied
//            => -1 or +1 produce the same result: only one sample is copied
// scaler is used to scale the copied mv value
// scaler_biased = 0 or 3 => no scale
//        = 1 => Y mv value is multiplied by 2
//        = 2 => Y mv value is divided by 2
// src_idx (4x4)source row/column to copy the data from
// idx (5row x 6col): where to store the data in the cache
void fills_in_one_mv_cache_entry(int16_t mv_cache[2][30][2], int8_t refIdx_cache[2][30],
    unsigned int idx, struct rv264macro* mb_attr, unsigned int src_idx, int repeat_mv,
    unsigned int need_L1, unsigned int scaler_biased)
{
  int i, j, k;
  LUD_DEBUG_ASSERT(idx < 30);
  LUD_DEBUG_ASSERT(src_idx < 16);
  LUD_DEBUG_ASSERT(repeat_mv);
  LUD_DEBUG_ASSERT(scaler_biased <= 3);

  if (IS_INTRA(mb_attr))
  {
    reset_one_mv_cache_entry(mv_cache, refIdx_cache, idx, repeat_mv, need_L1, -1);
    return;
  }

  for (i = 0; i < 1 + (need_L1 != 0); i++) // Should be unrolled at compilation time !
  {
    if (repeat_mv > 0) // repeat horizontally
    {
      for (j = 0; j < repeat_mv; j++)
      {
        int8_t refIdx;
        LUD_DEBUG_ASSERT(idx + j < 30);
        LUD_DEBUG_ASSERT(src_idx + j < 16);
        refIdx = mb_attr->RefIdxL[i][src_idx + j];
        if (refIdx < 0)
        {
          refIdx_cache[i][idx + j] = -1;
        }
        else
        {
          refIdx_cache[i][idx + j] = refIdx;
          refIdx_cache[i][idx+j] = im_scale2_biased(refIdx_cache[i][idx+j], 3-scaler_biased);
          mv_cache[i][idx+j][0] = mb_attr->mv[i][src_idx+j][0];
          mv_cache[i][idx+j][1] = mb_attr->mv[i][src_idx+j][1];
          mv_cache[i][idx+j][1] = im_scale2_biased(mv_cache[i][idx+j][1], scaler_biased);
        }
      }
    }
    else // repeat vertically
    {
      for (j = 0, k = 0; j < -repeat_mv * 6; j += 6, k += 4)
      {
        int8_t refIdx;
        LUD_DEBUG_ASSERT(idx + j < 30);
        LUD_DEBUG_ASSERT(src_idx + k < 16);
        refIdx = mb_attr->RefIdxL[i][src_idx + k];
        if (refIdx < 0)
        {
          refIdx_cache[i][idx + j] = -1;
        }
        else
        {
          refIdx_cache[i][idx + j] = refIdx;
          refIdx_cache[i][idx+j] = im_scale2_biased(refIdx_cache[i][idx+j], 3-scaler_biased);
          mv_cache[i][idx+j][0] = mb_attr->mv[i][src_idx+k][0];
          mv_cache[i][idx+j][1] = mb_attr->mv[i][src_idx+k][1];
          mv_cache[i][idx+j][1] = im_scale2_biased(mv_cache[i][idx+j][1], scaler_biased);
        }
      }
    }
  }
}

// mv_cache[L0-L1][sub block row*6 +sub block col][X-Y]   <= MUST BE ALIGNED on 32 bits...
// refIdx_cache[L0-L1][block row*6 +block col]
void fills_in_mv_cache_for_nonmbaff_slice(struct rv264macro* curr_mb_attr, unsigned int PicWidthInMbs,
    int16_t mv_cache[2][30][2], int8_t refIdx_cache[2][30], unsigned int need_L1, unsigned int simple)
{
  int nb = simple ? 1 : 4;

  // Fills in 1st row
  // upper left data
  struct rv264macro* upper_mb = curr_mb_attr - PicWidthInMbs - 1;
  if (curr_mb_attr->upleft_mb_is_available) // Nothing to do when the mb is not available
    fills_in_one_mv_cache_entry(mv_cache, refIdx_cache, 0, upper_mb, 15, 1, need_L1, 0);

  // upper middle data
  upper_mb++;
  if (curr_mb_attr->up_mb_is_available) // Nothing to do when the mb is not available
    fills_in_one_mv_cache_entry(mv_cache, refIdx_cache, 1, upper_mb, 12, nb, need_L1, 0);

  if (curr_mb_attr->upright_mb_is_available) // Nothing to do when the mb is not available
  {
    upper_mb++;
    fills_in_one_mv_cache_entry(mv_cache, refIdx_cache, 5, upper_mb, 12, 1, need_L1, 0);
  }

  // Fills in left column
  if (curr_mb_attr->left_mb_is_available) // Nothing to do when the mb is not available
  {
    struct rv264macro* left_mb = curr_mb_attr - 1;
    fills_in_one_mv_cache_entry(mv_cache, refIdx_cache, 6, left_mb, 3, -nb, need_L1, 0);
  }
}

// This is the same function as above, but is is called when MbaffFrameFlag==1
void fills_in_mv_cache_for_mbaff_slice(struct rv264macro* curr_mb_attr, unsigned int curr_is_bot, unsigned int curr_is_field,
    unsigned int PicWidthInMbs, int16_t mv_cache[2][30][2], int8_t refIdx_cache[2][30], unsigned int need_L1,
    unsigned int simple)
{
  int nb = simple ? 1 : 4;

  // Fills in the 1st row

  // Fills in the left upper data
  if (curr_mb_attr->upleft_mb_is_available) // Nothing to do when the mb is not available
  {
    unsigned int upleft_is_field = curr_mb_attr->upleft_mb_is_field;
    struct rv264macro* upleft = get_upleft_mbaff_mb(curr_mb_attr, PicWidthInMbs, curr_is_field, upleft_is_field, curr_is_bot);
    unsigned int block = get_upleft_mbaff_4x4block(curr_is_field, upleft_is_field, curr_is_bot);
    fills_in_one_mv_cache_entry(mv_cache, refIdx_cache, 0, upleft, block, 1, need_L1, (curr_is_field<<1) | upleft_is_field);
  }
    // Fills in the upper middle data
  if (curr_mb_attr->up_mb_is_available) // Nothing to do when the mb is not available
  {
    unsigned int up_is_field = curr_mb_attr->up_mb_is_field;
    struct rv264macro* up = get_up_mbaff_mb(curr_mb_attr, PicWidthInMbs, curr_is_field, up_is_field, curr_is_bot);
    fills_in_one_mv_cache_entry(mv_cache, refIdx_cache, 1, up, 12, nb, need_L1, (curr_is_field<<1) | up_is_field);
  }

  // Fills in the upper right data
  if (curr_mb_attr->upright_mb_is_available) // Nothing to do when the mb is not available
  {
    unsigned int upright_is_field = curr_mb_attr->upright_mb_is_field;
    struct rv264macro* upright = get_upright_mbaff_mb(curr_mb_attr, PicWidthInMbs, curr_is_field, upright_is_field, curr_is_bot);
    fills_in_one_mv_cache_entry(mv_cache, refIdx_cache, 5, upright, 12, 1, need_L1, (curr_is_field<<1) | upright_is_field);
  }


  // Fills in the left column

  if (curr_mb_attr->left_mb_is_available)
  {
    unsigned int left_is_field = curr_mb_attr->left_mb_is_field;
    int i;
    struct rv264macro* temp_mb_attr;
    struct rv264macro* left_mb_attr;
    const uint8_t* mb_pos = left_mb_pos[curr_is_field][left_is_field][curr_is_bot];
    const uint8_t* block_pos = left_4x4block_pos[curr_is_field][left_is_field][curr_is_bot];
    temp_mb_attr = curr_mb_attr -1 -(PicWidthInMbs << curr_is_bot);

    for(i=0; i<nb; i++)
    {
      left_mb_attr = temp_mb_attr + (PicWidthInMbs << mb_pos[i]);
      fills_in_one_mv_cache_entry(mv_cache, refIdx_cache, 6+i*6, left_mb_attr, block_pos[i], -1, need_L1, (curr_is_field<<1) | left_is_field);
    }
  }
}

// when simple==1 => only derive 4 neighboring : [0,1], [1,0] and [0,5] and [0,0]
void fills_in_mv_cache(struct rv264macro* curr_mb_attr, unsigned int curr_is_bot, unsigned int curr_is_field,
    unsigned int PicWidthInMbs, int16_t mv_cache[2][30][2], int8_t refIdx_cache[2][30], unsigned int need_L1,
    unsigned int simple, unsigned int MbaffFrameFlag)
{

  // Fills in default values into the cache. This is needed because some values into the 4x4 array may be needed while doing the prediction
  // See NOTE2 of section 6.4.8.5, page 32
  // Also the case for instance a B_8x8 mb with 1st partition being B_8x4 sub parts. mbAddrC is not avail !
  memset(mv_cache, 0, sizeof(mv_cache[0][0][0]) * 2 * 30 * 2);
  memset(refIdx_cache, -2, sizeof(refIdx_cache[0][0]) * 2 * 30);

  if (MbaffFrameFlag)
  {
    fills_in_mv_cache_for_mbaff_slice(curr_mb_attr, curr_is_bot, curr_is_field, PicWidthInMbs, mv_cache, refIdx_cache, need_L1, simple);
    return;
  }
  else
  {
    fills_in_mv_cache_for_nonmbaff_slice(curr_mb_attr, PicWidthInMbs, mv_cache, refIdx_cache, need_L1, simple);
    return;
  }
}

void mv_cache_write_back(struct rv264macro* curr_mb_attr, unsigned int need_L1, int16_t mv_cache[2][30][2],
    int8_t refIdx_cache[2][30])
{

  unsigned int idx, i;

  for (i = 0; i < 1 + (need_L1 != 0); i++)
  {
    for (idx = 0; idx < 16; idx++)
    {
      unsigned int r6x5idx = raster4x4toraster6x5[idx];
      curr_mb_attr->RefIdxL[i][idx] = refIdx_cache[i][r6x5idx];
      curr_mb_attr->mv[i][idx][0] = mv_cache[i][r6x5idx][0];
      curr_mb_attr->mv[i][idx][1] = mv_cache[i][r6x5idx][1];
    }
  }

}

void mv_cache_fill_const(struct rv264macro* curr_mb_attr, unsigned int LX_mask, int refIdxLX, int16_t mv[2])
{
  unsigned int idx;

  if (refIdxLX > -16)
    if (LX_mask & 1)
      memset(&curr_mb_attr->RefIdxL[0][0], refIdxLX, 16);
  if (LX_mask & 2)
    memset(&curr_mb_attr->RefIdxL[1][0], refIdxLX, 16);

  if (mv)
    for (idx = 0; idx < 16; idx++)
    {
      if (LX_mask & 1)
      {
        curr_mb_attr->mv[0][idx][0] = mv[0];
        curr_mb_attr->mv[0][idx][1] = mv[1];
      }
      if (LX_mask & 2)
      {
        curr_mb_attr->mv[1][idx][0] = mv[0];
        curr_mb_attr->mv[1][idx][1] = mv[1];
      }
    }

}

void derive_mbAddrD_mv_cache(int16_t mvLXD[2], int8_t* refIdxLXD, struct rv264macro* mb_attr,
    unsigned int src_idx, unsigned int LX, unsigned int scaler_biased)
{
  LUD_DEBUG_ASSERT(src_idx < 16);
  LUD_DEBUG_ASSERT(scaler_biased <= 3);

  if (IS_INTRA(mb_attr))
  {
    *refIdxLXD = -1;
    mvLXD[0] = mvLXD[1] = 0;
    return;
  }

  int8_t refIdx;
  refIdx = mb_attr->RefIdxL[LX][src_idx];
  if (refIdx < 0)
  {
    *refIdxLXD = -1;
    mvLXD[0] = mvLXD[1] = 0;
  }
  else
  {
    *refIdxLXD = refIdx;
    *refIdxLXD = im_scale2_biased(*refIdxLXD, 3-scaler_biased);
    mvLXD[0] = mb_attr->mv[LX][src_idx][0];
    mvLXD[1] = mb_attr->mv[LX][src_idx][1];
    mvLXD[1] = im_scale2_biased(mvLXD[1], scaler_biased);
  }
}
// This function only fills in mbAddrD mv cache. It is necessary for the median_luma_motion_vector_pred,
//  see this function header for more information...
void fills_in_mbAddrD_mv_cache(struct rv264macro* curr_mb_attr, unsigned int mb_row, unsigned int PicWidthInMbs,
    unsigned int r6x5idx, unsigned int LX, int16_t mvLXD[2], int8_t* refIdxLXD)
{
  unsigned int curr_is_field = curr_mb_attr->mb_field_decoding_flag;
  unsigned int left_is_field = curr_mb_attr->left_mb_is_field;
  unsigned int left_is_available = curr_mb_attr->left_mb_is_available;
  static const uint8_t t[30] =
      { N_A, N_A, N_A, N_A, N_A, N_A, N_A, N_A, N_A, N_A, N_A, N_A, N_A, 0, N_A, N_A, N_A, N_A, N_A, 1, N_A, N_A, N_A, N_A, N_A, 2, N_A, N_A, N_A, N_A}; // used when curr is frame (left is field)
  unsigned int idx = t[r6x5idx];
  // Filter all the cases where re-derivation is not needed (= the value in the cache is already correct)
  if (!left_is_available ||
     //(r6x5idx != 13 && r6x5idx != 19 && r6x5idx != 25) ||
     idx == (uint8_t)N_A ||
     curr_is_field == left_is_field)
    return;


  // We will actually fills in the LEFT value, but supposing y (in pixel units) is equal to 7, 11 or 15
  // r6x5idx==13 => pixel=(0,4)  => upperleft pixel=(-1, 3)
  // r6x5idx==19 => pixel=(0,8)  => upperleft pixel=(-1, 7)
  // r6x5idx==25 => pixel=(0,12) => upperleft pixel=(-1, 11)

  // r6x5idx==7 => pixel=(0,0) => upperleft pixel=(-1, -1) is already derived and correct in the initial mv_cache, that's why
  //   there is no need to take care of this case here.

  static const int8_t l[2][2][3] = // [curr_is_field][curr_is_bot][idx] => row offset to add to current mb
    {{ {1, 1, 1}, {0, 0, 0}}, {{0, 0, 1}, {-1, -1, 0}} };
  static const uint8_t b[2][2][3] = // [curr_is_field][curr_is_bot][idx] => r4x4 index of the 4x4 block in the left mb
    {{{3, 3, 7}, {11,11,15}}, {{7,15,7}, {7,15,7}}};
  unsigned int curr_is_bot = mb_row&1;
  struct rv264macro* left_mb = curr_mb_attr - 1 + (int)PicWidthInMbs * l[curr_is_field][curr_is_bot][idx];
  unsigned int src_idx = b[curr_is_field][curr_is_bot][idx];


  derive_mbAddrD_mv_cache(mvLXD, refIdxLXD, left_mb, src_idx, LX, 1+curr_is_field);

  LUD_DEBUG_ASSERT(curr_mb_attr->slice_num == left_mb->slice_num); // if blocked here then we must add the test into derive_mbAddrD_mv_cache function
  // in order to reset the value !
}

// Implements (see Section ITU(03/2005) 8.4.1.3.1)
// And a part of 8.4.1.3
// partWidth and partHeight are values in unit of 4 pixels
// This function is quite complex because of the part that states:
//    "if mbAddrC/... is not available, replace it with mbAddrD/... (see Section ITU(03/2005) 8.4.1.3.2)"
// There is a prb for MBAFF frames, indeed getting the top left value for the sample at location (in pixel unit) (0,4) (=>r6x5idx=13)
//    is not the same as getting the left value for the sample at location (0,0) (=>r6x5idx=7). However, in the cache there is only
//    one location for this value, which is r6x5idx=6. This is actually a prb only when MbaffFrameFlag==1 and the 2 involved mb are
//    of a different kind (field/frame) and only for r6x5idx=[13, 19 or 25]
// So the solution is to re derive mbAddrD/... When the above condition are met (It actually happens rarely)
// If you do not believe this speech do the calculus => See the awful table 6-4 (p35)
void median_luma_motion_vector_pred(unsigned int partWidth, unsigned int partHeight,
    unsigned int mbPartIdx, unsigned int r6x5idx, unsigned int LX, unsigned int MbaffFrameFlag, struct rv264macro* curr_mb_attr,
    unsigned int mb_row, unsigned int PicWidthInMbs, int16_t mvLXA[2], int16_t mvLXB[2], int16_t mvLXC_orig[2],
    int16_t mvLXD[2], int8_t refIdxLXA, int8_t refIdxLXB, int8_t refIdxLXC, int8_t refIdxLXD, int8_t refIdxLX,
    int16_t mvpLX[2])
{

  int16_t* mvLXC;
  int16_t mvLXD_recalc[2]; // Only used if recalculation of mbAddrD is needed (when MbaffFrameFlag == 1)
  // if mbAddrC/... is not available, replace it with mbAddrD/... (see Section ITU(03/2005) 8.4.1.3.2)
  if (refIdxLXC < -1)
  {
    if (MbaffFrameFlag)
    {
      mvLXD_recalc[0] = mvLXD[0];
      mvLXD_recalc[1] = mvLXD[1];
      fills_in_mbAddrD_mv_cache(curr_mb_attr, mb_row, PicWidthInMbs, r6x5idx, LX, mvLXD_recalc, &refIdxLXD);
      mvLXC = mvLXD_recalc;
    }
    else
      mvLXC = mvLXD;
    refIdxLXC = refIdxLXD;
  }
  else
    mvLXC = mvLXC_orig;

  if (partWidth == 4 && partHeight == 2 && mbPartIdx == 0 && refIdxLXB == refIdxLX)
  {
    mvpLX[0] = mvLXB[0];
    mvpLX[1] = mvLXB[1];
  }
  else if (partWidth == 4 && partHeight == 2 && mbPartIdx == 1 && refIdxLXA == refIdxLX)
  {
    mvpLX[0] = mvLXA[0];
    mvpLX[1] = mvLXA[1];
  }
  else if (partWidth == 2 && partHeight == 4 && mbPartIdx == 0 && refIdxLXA == refIdxLX)
  {
    mvpLX[0] = mvLXA[0];
    mvpLX[1] = mvLXA[1];
  }
  else if (partWidth == 2 && partHeight == 4 && mbPartIdx == 1 && refIdxLXC == refIdxLX)
  {
    mvpLX[0] = mvLXC[0];
    mvpLX[1] = mvLXC[1];
  }

  else
  {
    if (refIdxLXB < -1 && refIdxLXC < -1) // MbB and MbC are not avail
    {
      mvpLX[0] = mvLXA[0];
      mvpLX[1] = mvLXA[1];
    }
    else
    {
      unsigned int same = (refIdxLXA == refIdxLX) + (refIdxLXB == refIdxLX) + (refIdxLXC == refIdxLX);
      if (same == 1)
      {
        if (refIdxLXA == refIdxLX)
        {
          mvpLX[0] = mvLXA[0];
          mvpLX[1] = mvLXA[1];
        }
        else if (refIdxLXB == refIdxLX)
        {
          mvpLX[0] = mvLXB[0];
          mvpLX[1] = mvLXB[1];
        }
        else
        {
          mvpLX[0] = mvLXC[0];
          mvpLX[1] = mvLXC[1];
        }
      }
      else
      {
        // Calculate the median value of the 3 motion vectors
        mvpLX[0] = mvLXA[0] + mvLXB[0] + mvLXC[0] - im_min(mvLXA[0], im_min(mvLXB[0], mvLXC[0])) - im_max(mvLXA[0],
            im_max(mvLXB[0], mvLXC[0]));
        mvpLX[1] = mvLXA[1] + mvLXB[1] + mvLXC[1] - im_min(mvLXA[1], im_min(mvLXB[1], mvLXC[1])) - im_max(mvLXA[1],
            im_max(mvLXB[1], mvLXC[1]));
      }
    }
  }
}

// Implements (see Section ITU(03/2005) 8.4.1.3.1)
// And a part of 8.4.1.3
// partWidth and partHeight are values in unit of 4 pixels
// This is a simplified version of the process since it only applies on full macroblocks (not (sub) partitions). This is
//  typically used for P/B Skip mb
void median_luma_motion_vector_pred_simple(int16_t mvLXA[2], int16_t mvLXB[2], int16_t mvLXC[2],
    int16_t mvLXD[2], int8_t refIdxLXA, int8_t refIdxLXB, int8_t refIdxLXC, int8_t refIdxLXD, int8_t refIdxLX,
    int16_t mvpLX[2])
{
  median_luma_motion_vector_pred(0, 0, 0, 0, 0, 0, 0, 0, 0, mvLXA, mvLXB, mvLXC, mvLXD, refIdxLXA, refIdxLXB,
      refIdxLXC, refIdxLXD, refIdxLX, mvpLX);
}

// Return the value as specified in (Section ITU(03/2005)8.4.1.2.2)
int MinPositive(int a, int b, int c)
{
  unsigned int r;
  r = im_min((unsigned int) a, im_min((unsigned int) b, (unsigned int) c)); // TODO: replace by an unsigned min function !
  return (int) r;

}

// Derive which is the colPic(=RefPic + the field that should be used), and some other necessary information
void derive_colPic(RefListEntry* RefPicList1, int PicOrderCnt, unsigned int mb_row,
    unsigned int field_pic_flag, unsigned int mb_field_decoding_flag, unsigned int MbaffFrameFlag,
    struct rv264picture** colPic_, unsigned int* field, unsigned int* topAbsDiffPOC, unsigned int* bottomAbsDiffPOC,
    unsigned int* CurrPic_PicCodingStruct, unsigned int* ColPic_PicCodingStruct)
{
  struct rv264picture* colPic = RefPicList1[0].ref_pic;
  unsigned int parity = RefPicList1[0].parity;
  unsigned int colPic_field_pic_flag;
  unsigned int colPic_MbaffFrameFlag;

  LUD_DEBUG_ASSERT(colPic->field_data[parity].mb_attr); // the designated field must exist in the picture data
  colPic_field_pic_flag = !(colPic->structure & 4);
  colPic_MbaffFrameFlag = colPic->field_sh[parity][0]->MbaffFrameFlag;

  // PicCodingStruct:
  //  0: FLD, 1: FRM, 2: AFRM
  *CurrPic_PicCodingStruct = MbaffFrameFlag + !field_pic_flag;
  *ColPic_PicCodingStruct = colPic_MbaffFrameFlag + !colPic_field_pic_flag;
  *colPic_ = colPic;

  if (!field_pic_flag)
  {
    unsigned int top, bot;
    //LUD_DEBUG_ASSERT((colPic->ref_struct & 3) == 3); // sanity check...
    top = im_abs(colPic->FieldOrderCnt[0] - PicOrderCnt);
    bot = im_abs(colPic->FieldOrderCnt[1] - PicOrderCnt);
    *topAbsDiffPOC = top;
    *bottomAbsDiffPOC = bot;
    if ((!mb_field_decoding_flag && top < bot) || (mb_field_decoding_flag && ((mb_row & 1) == 0)))
      parity = 0;
    else
      parity = 1;

  }

  *field = parity;

}

// Warning, when calling this function and direct_8x8_inference_flag==1, the caller must ensure to set subMbPartIdx = mbPartIdx before the call
void derive_colocated_4x4_submbpart(struct rv264picture* colPic, unsigned int field,
    unsigned int CurrPic_PicCodingStruct, unsigned int ColPic_PicCodingStruct, unsigned int topAbsDiffPOC,
    unsigned int bottomAbsDiffPOC, unsigned int block_row, unsigned int block_col, unsigned int mb_idx,
    unsigned int mb_row, unsigned int mb_col, unsigned int PicWidthInMbs, unsigned int mb_field_decoding_flag,
    unsigned int bottom_field_flag, unsigned int* mbIdxCol, unsigned int* block_rowCol,
    VERT_MV_SCALE_TYPE* vertMvScale, int16_t mvCol[2], int* refIdxCol, RefListEntry** refPicCol, unsigned int* refPicCol_field)
{

  unsigned int mbIdxCol_, block_rowCol_; // Define what is the  Mb idx/mbPart/subMbPart of the co-located mb. 0< i,j<3 => the 4x4 location into the 16x16 MB
  unsigned int fieldDecodingFlagX = 0;
  unsigned int mbCol_parity = 0; // 0 top field mb, 1: bot field mb. Used when colPic is AFRM

  // This macro enable giving null as value for the output pointer => compiler should remove the code knowing if the pointer is null or not
#define SET_IF_NEEDED(p, v) do { if (p) *(p) = (v);} while (0)

  LUD_DEBUG_ASSERT(colPic->field_data[field].mb_attr);

  // Table 8-8 implementation, page 141
  if (CurrPic_PicCodingStruct == 0)
  {
    if (ColPic_PicCodingStruct == 0)
    {
      mbIdxCol_ = mb_idx;
      block_rowCol_ = block_row;
      SET_IF_NEEDED(vertMvScale, One_To_One);
    }
    else if (ColPic_PicCodingStruct == 1)
    {
      mbIdxCol_ = mb_col + (2 * mb_row + (block_row >= 2)) * PicWidthInMbs;
      block_rowCol_ = (block_row * 2) % 4;
      SET_IF_NEEDED(vertMvScale, Frm_To_Fld);
    }
    else // ColPic_PicCodingStruct == 2
    {
      mbIdxCol_ = mb_col + (2 * mb_row) * PicWidthInMbs;
      fieldDecodingFlagX = colPic->field_data[field].mb_attr[mbIdxCol_].mb_field_decoding_flag;
      if (fieldDecodingFlagX)
      {
        mbIdxCol_ += (bottom_field_flag != 0) * PicWidthInMbs;
        block_rowCol_ = block_row;
        SET_IF_NEEDED(vertMvScale, One_To_One);
      }
      else
      {
        mbIdxCol_ += (block_row >= 2) * PicWidthInMbs;
        block_rowCol_ = (block_row * 2) % 4;
        SET_IF_NEEDED(vertMvScale, Frm_To_Fld);
      }
      mbCol_parity = (mbIdxCol_/PicWidthInMbs) % 2;
    }
  }

  else if (CurrPic_PicCodingStruct == 1)
  {
    if (ColPic_PicCodingStruct == 0)
    {
      mbIdxCol_ = mb_col + (mb_row / 2) * PicWidthInMbs;
      block_rowCol_ = 2 * (mb_row % 2) + (block_row >= 2);
      SET_IF_NEEDED(vertMvScale, Fld_To_Frm);
    }

    else // ColPic_PicCodingStruct == 1 (ColPic_PicCodingStruct cannot be equal to 2 by the standard)
    {
      mbIdxCol_ = mb_idx;
      block_rowCol_ = block_row;
      SET_IF_NEEDED(vertMvScale, One_To_One);
    }
  }

  else // CurrPic_PicCodingStruct == 2
  {
    if (ColPic_PicCodingStruct == 0)
    {
      mbIdxCol_ = mb_col + (mb_row / 2) * PicWidthInMbs;
      if (mb_field_decoding_flag)
      {
        block_rowCol_ = block_row;
        SET_IF_NEEDED(vertMvScale, One_To_One);
      }
      else //mb_field_decoding_flag==0
      {
        block_rowCol_ = 2 * (mb_row % 2) + (block_row >= 2);
        SET_IF_NEEDED(vertMvScale, Fld_To_Frm);
      }
    }
    else // ColPic_PicCodingStruct == 2 (ColPic_PicCodingStruct cannot be equal to 1 by the standard)
    {
      mbIdxCol_ = mb_idx;
      fieldDecodingFlagX = colPic->field_data[field].mb_attr[mbIdxCol_].mb_field_decoding_flag;
      if (mb_field_decoding_flag)
      {
        if (fieldDecodingFlagX)
        {
          block_rowCol_ = block_row;
          SET_IF_NEEDED(vertMvScale, One_To_One);
        }
        else // fieldDecodingFlagX == 0
        {
          mbIdxCol_ += ((block_row >= 2) - (mb_row % 2)) * PicWidthInMbs;
          block_rowCol_ = (block_row * 2) % 4;
          SET_IF_NEEDED(vertMvScale, Frm_To_Fld);
        }
      }
      else // mb_field_decoding_flag == 0
      {
        if (fieldDecodingFlagX)
        {
          int d = topAbsDiffPOC < bottomAbsDiffPOC ? 0 : 1;
          mbIdxCol_ += (d - (mb_row % 2)) * PicWidthInMbs;
          block_rowCol_ = 2 * (mb_row % 2) + (block_row >= 2);
          SET_IF_NEEDED(vertMvScale, Fld_To_Frm);
        }
        else // fieldDecodingFlagX == 0
        {
          block_rowCol_ = block_row;
          SET_IF_NEEDED(vertMvScale, One_To_One);
        }
      }
      mbCol_parity = (mbIdxCol_/PicWidthInMbs) % 2;
    }
  }

  {
    struct rv264macro* mbAttrCol = &colPic->field_data[field].mb_attr[mbIdxCol_];
    unsigned int predFlagL0Col, predFlagL1Col;
    unsigned int idx = block_rowCol_ * 4 + block_col;
    unsigned int mbAttrCol_sliceNum = mbAttrCol->slice_num;
    RefListEntry** colPic_RefPicList = colPic->field_sh[field][mbAttrCol_sliceNum]->RefPicList;

    predFlagL0Col = mbAttrCol->RefIdxL[0][idx] >= 0;
    predFlagL1Col = mbAttrCol->RefIdxL[1][idx] >= 0;

    if (is_IntraMb(mbAttrCol->mb_type) || (predFlagL0Col == 0 && predFlagL1Col == 0))
    {
      mvCol[0] = mvCol[1] = 0;
      *refIdxCol = -1;
      SET_IF_NEEDED(refPicCol, 0);
      SET_IF_NEEDED(refPicCol_field, 0);
    }
    else if (predFlagL0Col)
    {
      mvCol[0] = mbAttrCol->mv[0][idx][0];
      mvCol[1] = mbAttrCol->mv[0][idx][1];
      *refIdxCol = mbAttrCol->RefIdxL[0][idx];
      if (fieldDecodingFlagX)
      {
        SET_IF_NEEDED(refPicCol, &colPic_RefPicList[0][(*refIdxCol)>>1]); // may wonder why shifting ? see section 8.4.2.1, p150
        SET_IF_NEEDED(refPicCol_field, ((*refIdxCol)%2) ^ mbCol_parity); //  (refIdxL0 % 2) ^ mbCol_parity = refIdxL0 % 2 ? 1-mbCol_parity : mbCol_parity
      }
      else
      {
        SET_IF_NEEDED(refPicCol, &colPic_RefPicList[0][*refIdxCol]);
        SET_IF_NEEDED(refPicCol_field, colPic_RefPicList[0][*refIdxCol].parity);
      }
      LUD_DEBUG_ASSERT(!refPicCol || (*refPicCol)->ref_pic);

    }
    else // predFlagL1Col == 1
    {
      mvCol[0] = mbAttrCol->mv[1][idx][0];
      mvCol[1] = mbAttrCol->mv[1][idx][1];
      *refIdxCol = mbAttrCol->RefIdxL[1][idx];
      if (fieldDecodingFlagX)
      {
        SET_IF_NEEDED(refPicCol, &colPic_RefPicList[1][(*refIdxCol)>>1]); // may wonder why shifting ? see section 8.4.2.1, p150
        SET_IF_NEEDED(refPicCol_field, ((*refIdxCol)%2) ^ mbCol_parity); //  (refIdxL1 % 2) ^ mbCol_parity = refIdxL1 % 2 ? 1-mbCol_parity : mbCol_parity
      }
      else
      {
        SET_IF_NEEDED(refPicCol, &colPic_RefPicList[1][*refIdxCol]);
        SET_IF_NEEDED(refPicCol_field, colPic_RefPicList[1][*refIdxCol].parity);
      }
      LUD_DEBUG_ASSERT(!refPicCol || (*refPicCol)->ref_pic);

    }
  }

  SET_IF_NEEDED(mbIdxCol, mbIdxCol_);
  SET_IF_NEEDED(block_rowCol, block_rowCol_);

#undef SET_IF_NEEDED
}

int MapColToList0(struct rv264picture* colPic, RefListEntry* refPicCol, RefListEntry* RefPicList0, int refIdxCol,
    VERT_MV_SCALE_TYPE vertMvScale, unsigned int field_pic_flag, unsigned int bottom_field_flag,
    unsigned int mb_field_decoding_flag, unsigned int mb_parity, unsigned int refPicCol_field, int16_t mvCol[2])
{
  int refIdxL0Frm;
  for (refIdxL0Frm = 0; refIdxL0Frm < NB_OF_REF_PICS; refIdxL0Frm++)
    if (refPicCol->ref_pic == RefPicList0[refIdxL0Frm].ref_pic &&
        (!field_pic_flag || RefPicList0[refIdxL0Frm].parity == refPicCol_field))
      break;
  LUD_DEBUG_ASSERT(refIdxL0Frm < NB_OF_REF_PICS); // according to section (8.4.1.2.3) refPicCol must exist in RefPicList0

  if (vertMvScale == One_To_One)
  {
    if (!field_pic_flag && mb_field_decoding_flag)
    {
      for (refIdxL0Frm = 0; refIdxL0Frm < NB_OF_REF_PICS; refIdxL0Frm++)
        if (refPicCol->ref_pic == RefPicList0[refIdxL0Frm].ref_pic)
          break;
      LUD_DEBUG_ASSERT(refIdxL0Frm < NB_OF_REF_PICS); // according to section (8.4.1.2.3) refPicCol must exist in RefPicList0
      refIdxL0Frm <<= 1;
      if (mb_parity != refPicCol_field) // the parity of refPicCol is the one that is derived in section 8.4.2.1, p150 of the standard 03/2005
        refIdxL0Frm += 1;
    }
  }
  else if (vertMvScale == Frm_To_Fld)
  {
    if (!field_pic_flag)
    {
      for (refIdxL0Frm = 0; refIdxL0Frm < NB_OF_REF_PICS; refIdxL0Frm++)
        if (refPicCol->ref_pic == RefPicList0[refIdxL0Frm].ref_pic)
          break;
      LUD_DEBUG_ASSERT(refIdxL0Frm < NB_OF_REF_PICS); // according to section (8.4.1.2.3) refPicCol must exist in RefPicList0
      refIdxL0Frm <<= 1;
    }
    else // field_pic_flag = 1
    {
      for (refIdxL0Frm = 0; refIdxL0Frm < NB_OF_REF_PICS; refIdxL0Frm++)
        if (refPicCol->ref_pic == RefPicList0[refIdxL0Frm].ref_pic && RefPicList0[refIdxL0Frm].parity == bottom_field_flag)
          break;
      LUD_DEBUG_ASSERT(refIdxL0Frm < NB_OF_REF_PICS); // according to section (8.4.1.2.3) refPicCol must exist in RefPicList0
    }

    mvCol[1] = im_scale2(mvCol[1], -1);
  }
  else // vertMvScale == Fld_To_Frm
  {
    if (!field_pic_flag)
    {
      for (refIdxL0Frm = 0; refIdxL0Frm < NB_OF_REF_PICS; refIdxL0Frm++)
        if (refPicCol->ref_pic == RefPicList0[refIdxL0Frm].ref_pic)
          break;
    }
    else // field_pic_flag = 1
    {
      for (refIdxL0Frm = 0; refIdxL0Frm < NB_OF_REF_PICS; refIdxL0Frm++)
        if (refPicCol->ref_pic == RefPicList0[refIdxL0Frm].ref_pic && RefPicList0[refIdxL0Frm].parity == refPicCol_field)
          break;
    }
    LUD_DEBUG_ASSERT(refIdxL0Frm < NB_OF_REF_PICS); // according to section (8.4.1.2.3) refPicCol must exist in RefPicList0

    // nothing to do: refIdxL0Frm already hold the value
    mvCol[1] = im_scale2(mvCol[1], 1);
  }
  return refIdxL0Frm;
}

// derive MV pred only P_Skip macroblocks (Section ITU(03/2005)8.4.1.1)
// Only need 3 neighboring : [0,1], [1,0] and [0,5] ( and [0,0] if [0,5] is not available)
// The result of refIdxL and mvL are only set into [1,1] and are the same for the whole 4x4 array
void mv_pred_P_Skip_mb(int16_t mv_cache[2][30][2], int8_t refIdx_cache[2][30])
{

  // For this Mb we only set the value at location [1,1] of the cache since they are all identical
  // However, this value will be copied into the whole array of the curr_mb_attr... (cache write back)

  //refIdx_cache[0][7] = 0; // done in the calling func: fill the cache directly...
  //refIdx_cache[1][7] = -2; // List 1 is marked as not available ! // done in the calling func: fill the cache directly...

  if (refIdx_cache[0][1] < -1 || refIdx_cache[0][6] < -1 || (refIdx_cache[0][1] == 0 && mv_cache[0][1][0] == 0 && mv_cache[0][1][1] == 0) ||
    (refIdx_cache[0][6] == 0 && mv_cache[0][6][0] == 0 && mv_cache[0][6][1] == 0))
  {
    //mv_cache[0][7][0] = mv_cache[0][7][1] = 0; // mv_cache is initialized to 0
  }
  else
  {
    median_luma_motion_vector_pred_simple(mv_cache[0][6], mv_cache[0][1], mv_cache[0][5], mv_cache[0][0],
        refIdx_cache[0][6], refIdx_cache[0][1], refIdx_cache[0][5], refIdx_cache[0][0], 0, mv_cache[0][7]);
  }

}

int mv_pred_B_SKIP_B_Direct_init_mechanism(int16_t mv_cache[2][30][2], int8_t refIdx_cache[2][30],
    unsigned int mb_row, unsigned int field_pic_flag, unsigned int mb_field_decoding_flag, int PicOrderCnt,
    unsigned int MbaffFrameFlag, unsigned int direct_spatial_mv_pred_flag,
    RefListEntry* RefPicList[2], struct rv264picture** colPic, unsigned int* field, unsigned int* topAbsDiffPOC,
    unsigned int* bottomAbsDiffPOC, unsigned int* CurrPic_PicCodingStruct, unsigned int* ColPic_PicCodingStruct,
    int refIdxL[2], int16_t mvLX[2][2])
{
  unsigned int i;
  int ret;

  if (direct_spatial_mv_pred_flag)
  {
    for (i = 0; i < 2; i++)
    {
      int refIdxLXC = refIdx_cache[i][5];
      if (refIdxLXC < -1)
        refIdxLXC = refIdx_cache[i][0];
      // derivation of refIdxL0 and refIdxL1 can be done once for all 16 4x4 blocks according to Note 1 of (see Section ITU(03/2005) 8.4.1.2.2)
      // the neighboring partition are the same for all blocks
      refIdxL[i] = MinPositive(refIdx_cache[i][6], refIdx_cache[i][1], refIdxLXC);
      refIdxL[i] = im_max(refIdxL[i], -1); // Clamp the lower limit to -1: the current mb cannot have refIdx=-2 (it would mean that is is not available)
    }

    if (refIdxL[0] < 0 && refIdxL[1] < 0)
    {
      // If directZeroPerdictionFlag (<=>(refIdxL0<0 && refIdxL1<0)) is set, there is no need to derive the co-location process since all blocks will have
      //   the same motion vector equal to 0,0
      //refIdxL[0] = refIdxL[1] = 0;

      return -1;
    }

    else
    {
      for (i = 0; i < 2; i++)
      {
        // The motion vector is the same for all sub mb partition where it is not null and this process is invoked (see Section ITU(03/2005) 8.4.1.2.2, NOTE 3)
        //  so we derive it only once per MB  (worst case: it is not needed...)
        median_luma_motion_vector_pred_simple(mv_cache[i][6], mv_cache[i][1], mv_cache[i][5], mv_cache[i][0],
            refIdx_cache[i][6], refIdx_cache[i][1], refIdx_cache[i][5], refIdx_cache[i][0], refIdxL[i], mvLX[i]);
      }

      ret = 1;
    }
  }

  else // direct_spatial_mv_pred_flag == 0
  {
    // Nothing specific to do...
    ret = 2;
  }

  // The colPic is the same for all sub mb part
  derive_colPic(RefPicList[1], PicOrderCnt, mb_row, field_pic_flag, mb_field_decoding_flag,
      MbaffFrameFlag, colPic, field, topAbsDiffPOC, bottomAbsDiffPOC, CurrPic_PicCodingStruct,
      ColPic_PicCodingStruct);

  return ret;
}

void mv_pred_B_SKIP_B_Direct_iterate(unsigned int direct_spatial_mv_pred_flag,
    unsigned int direct_8x8_inference_flag, int16_t mv_cache[2][30][2], int8_t refIdx_cache[2][30],
    unsigned int scan4x4idx, unsigned int mb_idx, unsigned int mb_row, unsigned int mb_col,
    unsigned int mb_field_decoding_flag, unsigned int bottom_field_flag, unsigned int field_pic_flag,
    unsigned int PicWidthInMbs, RefListEntry* RefPicList[2], int FieldOrderCnt[2],
    struct rv264picture* colPic, unsigned int field, unsigned int topAbsDiffPOC, unsigned int bottomAbsDiffPOC,
    unsigned int CurrPic_PicCodingStruct, unsigned int ColPic_PicCodingStruct, int refIdxL[2], int16_t mvLX[2][2])
{
  LUD_DEBUG_ASSERT(scan4x4idx < 16);

  unsigned int raster6x5idx = scan4x4toraster6x5[scan4x4idx];
  // unsigned int raster4x4idx = scan4x4[scan4x4idx];
  unsigned int block_col = scan4x4_col[scan4x4idx];
  unsigned int block_row = scan4x4_row[scan4x4idx];
  int refIdxCol, i, j;
  int16_t mvCol[2];

  if (direct_spatial_mv_pred_flag)
  {
    unsigned int colZeroFlag;

    derive_colocated_4x4_submbpart(colPic, field, CurrPic_PicCodingStruct, ColPic_PicCodingStruct, topAbsDiffPOC,
        bottomAbsDiffPOC, block_row, block_col, mb_idx, mb_row, mb_col, PicWidthInMbs, mb_field_decoding_flag,
        bottom_field_flag, 0, 0, 0, mvCol, &refIdxCol, 0, 0);

    colZeroFlag = RefPicList[1][0].ref_pic_type == SHORT_TERM_REF && refIdxCol == 0 && im_abs(mvCol[0]) <= 1
        && im_abs(mvCol[1]) <= 1;

    // direct_8x8_inference_flag make the loop to assign 4 times the same value to the whole mb partition
    if (direct_8x8_inference_flag)
      scan4x4idx = scan4x4idx & ~3;

    for (i = 0; i < 2; i++)
    {
      unsigned int idx = scan4x4idx;
      for (j = 0; j <= (direct_8x8_inference_flag != 0) * 3; j++, idx++)
      {
        if (direct_8x8_inference_flag)
          raster6x5idx = scan4x4toraster6x5[idx];

        refIdx_cache[i][raster6x5idx] = refIdxL[i];
        if (refIdxL[i] < 0 || (refIdxL[i] == 0 && colZeroFlag))
        {
          //mv_cache[i][raster6x5idx][0] = mv_cache[i][raster6x5idx][1] = 0; // mv_cache is already initialized to 0
        }
        else
        {
          mv_cache[i][raster6x5idx][0] = mvLX[i][0];
          mv_cache[i][raster6x5idx][1] = mvLX[i][1];
        }
      }
    }
  }
  else // direct_spatial_mv_pred_flag==0
  {
    unsigned int mbIdxCol, block_rowCol;
    int refIdxL0;
    VERT_MV_SCALE_TYPE vertMvScale;
    int32_t currPicPOC, pic0POC, pic1POC;
    ref_pic_type_t pic0RefPicType;
    unsigned int mb_parity;
    unsigned int refPicCol_field;
    RefListEntry* refPicCol;

    derive_colocated_4x4_submbpart(colPic, field, CurrPic_PicCodingStruct, ColPic_PicCodingStruct, topAbsDiffPOC,
        bottomAbsDiffPOC, block_row, block_col, mb_idx, mb_row, mb_col, PicWidthInMbs, mb_field_decoding_flag,
        bottom_field_flag, &mbIdxCol, &block_rowCol, &vertMvScale, mvCol, &refIdxCol, &refPicCol, &refPicCol_field);

    mb_parity = mb_row % 2; // mb_parity will be used only when processing mbaff frames, so this is not a bug when doing standard frames or fields
    if (refIdxCol >= 0)
    {
      LUD_DEBUG_ASSERT(refPicCol);
      refIdxL0 = MapColToList0(colPic, refPicCol, RefPicList[0], refIdxCol, vertMvScale, field_pic_flag, bottom_field_flag,
          mb_field_decoding_flag, mb_parity, refPicCol_field, mvCol);
    }
    else
      refIdxL0 = 0;

    LUD_DEBUG_ASSERT(refIdxL0 <= NB_OF_REF_PICS);


    if (!field_pic_flag && mb_field_decoding_flag)
    {
      currPicPOC = FieldOrderCnt[mb_parity];
      pic1POC = RefPicList[1][0].ref_pic->FieldOrderCnt[mb_parity];
      pic0POC = RefPicList[0][refIdxL0 / 2].ref_pic->FieldOrderCnt[(refIdxL0 % 2) ^ mb_parity]; //  (refIdxL0 % 2) ^ mb_parity = refIdxL0 % 2 ? 1-mb_parity : mb_parity
      pic0RefPicType = RefPicList[0][refIdxL0 / 2].ref_pic_type;
    }
    else
    {
      currPicPOC = FieldOrderCnt[bottom_field_flag];
      pic1POC = RefPicList[1][0].ref_pic->FieldOrderCnt[RefPicList[1][0].parity];
      pic0POC = RefPicList[0][refIdxL0].ref_pic->FieldOrderCnt[RefPicList[0][refIdxL0].parity];
      pic0RefPicType = RefPicList[0][refIdxL0].ref_pic_type;
    }

    // direct_8x8_inference_flag make the loop to assign 4 times the same value to the whole mb partition
    if (direct_8x8_inference_flag)
      scan4x4idx = scan4x4idx & ~3;

    for (j = 0; j <= (direct_8x8_inference_flag != 0) * 3; j++, scan4x4idx++)
    {
      if (direct_8x8_inference_flag)
        raster6x5idx = scan4x4toraster6x5[scan4x4idx];

      refIdx_cache[0][raster6x5idx] = refIdxL0;
      refIdx_cache[1][raster6x5idx] = 0;

      if (pic0RefPicType == LONG_TERM_REF || pic1POC - pic0POC == 0)
      {
        mv_cache[0][raster6x5idx][0] = mvCol[0];
        mv_cache[0][raster6x5idx][1] = mvCol[1];
        //mv_cache[1][raster6x5idx][0] = mv_cache[1][raster6x5idx][1] = 0; // mv_cache is already initialized to 0
      }
      else
      {
        int tb = im_clip(-128, 127, currPicPOC - pic0POC);
        int td = im_clip(-128, 127, pic1POC - pic0POC);
        int tx = (16384 + im_abs(td / 2)) / td;
        int DistScaleFactor = im_clip(-1024, 1023, (tb * tx + 32) >> 6);

        for (i = 0; i < 2; i++)
        {
          mv_cache[0][raster6x5idx][i] = (DistScaleFactor * mvCol[i] + 128) >> 8;
          mv_cache[1][raster6x5idx][i] = mv_cache[0][raster6x5idx][i] - mvCol[i];
        }
      }
    }
  }
}

// derive mv pred only for B_Skip, B_Direct_16x16 macroblocks (Section ITU(03/2005)8.4.1.2)
// mbPartIdx proceed from 0 to 3
// subMbPartIdx proceed from 0 to 3
// However derivation of neighboring location is always done with mbPartIdx=subMbPartIdx=0
//   and predPartWidth is set to 16
//   => Only need 3 neighboring : [0,1], [1,0] and [0,5] ( and [0,0] if [0,5] is not available)
void mv_pred_B_Skip_B_Direct_16x16_mb(int16_t mv_cache[2][30][2], int8_t refIdx_cache[2][30],
    unsigned int mb_idx, unsigned int mb_row, unsigned int mb_col, unsigned int direct_spatial_mv_pred_flag,
    unsigned int direct_8x8_inference_flag, RefListEntry* RefPicList[2], unsigned int field_pic_flag,
    unsigned int bottom_field_flag, unsigned int mb_field_decoding_flag, unsigned int MbaffFrameFlag,
    int FieldOrderCnt[2], int PicOrderCnt, unsigned int PicWidthInMbs,
    uint32_t* partWidths, uint32_t* partHeights)
{

  unsigned int scan4x4idx;
  int type;

  struct rv264picture* colPic;
  unsigned int field;
  unsigned int topAbsDiffPOC;
  unsigned int bottomAbsDiffPOC;
  unsigned int CurrPic_PicCodingStruct, ColPic_PicCodingStruct;
  int refIdxL[2];
  int16_t mvLX[2][2];

  type = mv_pred_B_SKIP_B_Direct_init_mechanism(mv_cache, refIdx_cache, mb_row, field_pic_flag, mb_field_decoding_flag,
      PicOrderCnt, MbaffFrameFlag, direct_spatial_mv_pred_flag, RefPicList, &colPic, &field,
      &topAbsDiffPOC, &bottomAbsDiffPOC, &CurrPic_PicCodingStruct, &ColPic_PicCodingStruct, refIdxL, mvLX);

  if (type == -1)
  {
    // trivial case: direct_spatial_mv_pred_flag == 1 and, refIdxL0 and refIdxL1 < 0
    // The whole 4x4 need to be filled with 0
    for (scan4x4idx = 0; scan4x4idx < 16; scan4x4idx++)
    {
      unsigned int idx = scan4x4toraster6x5[scan4x4idx];
      //mv_cache[0][idx][0] = mv_cache[0][idx][1] = 0; // mv_cache is already initialized to 0
      refIdx_cache[0][idx] = 0;
      //mv_cache[1][idx][0] = mv_cache[1][idx][1] = 0; // mv_cache is already initialized to 0
      refIdx_cache[1][idx] = 0;
    }
    SET_PART_SIZE(*partWidths, 4, 0);
    SET_PART_SIZE(*partHeights, 4, 0);
  }
  else // non trivial case when direct_spatial_mv_pred_flag can be 0 or 1
  {
    if (!direct_8x8_inference_flag)
    {
      for (scan4x4idx = 0; scan4x4idx < 16; scan4x4idx++)
      {
        mv_pred_B_SKIP_B_Direct_iterate(direct_spatial_mv_pred_flag, 0, mv_cache, refIdx_cache, scan4x4idx, mb_idx,
            mb_row, mb_col, mb_field_decoding_flag, bottom_field_flag, field_pic_flag, PicWidthInMbs, RefPicList,
            FieldOrderCnt, colPic, field, topAbsDiffPOC, bottomAbsDiffPOC, CurrPic_PicCodingStruct,
            ColPic_PicCodingStruct, refIdxL, mvLX);
      }
      // let partWidths and partHeights equal to 0 => whole mb formed of 4x4 sub partitions...
    }
    else // direct_8x8_inference_flag==1
    {
      // We only need to run the iteration 4 times, at index 0, 5, 10, 15 (because subMbPartIdx=mbPartIdx)
      for (scan4x4idx = 0; scan4x4idx < 16; scan4x4idx += 5)
      {
        mv_pred_B_SKIP_B_Direct_iterate(direct_spatial_mv_pred_flag, 1, mv_cache, refIdx_cache, scan4x4idx, mb_idx,
            mb_row, mb_col, mb_field_decoding_flag, bottom_field_flag, field_pic_flag, PicWidthInMbs, RefPicList,
            FieldOrderCnt, colPic, field, topAbsDiffPOC, bottomAbsDiffPOC, CurrPic_PicCodingStruct,
            ColPic_PicCodingStruct, refIdxL, mvLX);
      }
      // The mb is formed of four 8x8 partitions
      *partWidths = 5;
      *partHeights = 5;
    }
  }
}

// table that help to navigate into the mb depending on the current position (mbPartIdx/subMbPartIdx) and the mb (sub) partition size
// the structure is table[partwidth][partheight][(sub)mbPartIdx] = (Sub)MbPart6x5Idx
// the (Sub)MbPart6x5Idx is for the 6x5 cache table
static const int8_t Part6x5Idx[2][2][4] =
  {
    {
      { 7, 9, 19, 21 }, // 8x8 part
          { 7, 9, -1, -1 } }, // 8x16 part
        {
          { 7, 19, -1, -1 }, // 16x8 part
              { 7, -1, -1, -1 } } // 16x16 part
  };

// Same as above, except those are offset to add in order to get the index of the sub partition
static const int8_t SubPart6x5IdxOffset[2][2][4] =
  {
    {
      { 0, 1, 6, 7 }, // 4x4 part
          { 0, 1, -1, -1 } }, // 4x8 part
        {
          { 0, 6, -1, -1 }, // 8x4 part
              { 0, -1, -1, -1 } } // 8x8 part
  };

// derive mv pred for B P mb (apart from the mb in the above functions) (Section ITU(03/2005)8.4.1)
// also derive B_8x8 / B_Direct_8x8 sub partition as a special case
void mv_pred_P_B_mb(struct rv264macro* curr_mb_attr, mb_type_t mb_type, unsigned int numMbPart,
    unsigned int has_sub_part, unsigned int is_B_mb, unsigned int PicWidthInMbs, unsigned int MbaffFrameFlag,
    int16_t mv_cache[2][30][2], int8_t refIdx_cache[2][30], unsigned int mb_idx, unsigned int mb_row,
    unsigned int mb_col, unsigned int field_pic_flag, unsigned int mb_field_decoding_flag,
    unsigned int direct_spatial_mv_pred_flag,
    unsigned int bottom_field_flag, unsigned int direct_8x8_inference_flag,
    RefListEntry* RefPicList[2], int PicOrderCnt, int FieldOrderCnt[2],
    uint32_t* partWidths, uint32_t* partHeights)
{

  unsigned int mbPartIdx, subMbPartIdx, numSubMbPart, c, r;
  unsigned int partHeight, partWidth;
  sub_mb_type_t sub_mb_type = (sub_mb_type_t)0;  // initialization not necessary, but prevents a warning...
  int i;
  unsigned int partSizeIdx = 0;

  // Only used when decoding B_Direct_8x8
  int direct_8x8_init = 0;
  struct rv264picture* colPic;
  unsigned int field, topAbsDiffPOC, bottomAbsDiffPOC, CurrPic_PicCodingStruct, ColPic_PicCodingStruct;
  int refIdxDirectL[2];
  int16_t mvDirectL[2][2];

  numSubMbPart = 1;
  for (mbPartIdx = 0; mbPartIdx < numMbPart; mbPartIdx++)
  {
    unsigned int part6x5idx;
    int predMode;
    partHeight = MbPartHeight(mb_type);
    partWidth = MbPartWidth(mb_type);
    part6x5idx = Part6x5Idx[partWidth >> 2][partHeight >> 2][mbPartIdx];
    LUD_DEBUG_ASSERT(part6x5idx < 22);

    if (has_sub_part)
    {
      sub_mb_type = (sub_mb_type_t) curr_mb_attr->sub_mb_type[mbPartIdx];
      predMode = (int) SubMbPredMode(sub_mb_type);
      partHeight = SubMbPartHeight(sub_mb_type);
      partWidth = SubMbPartWidth(sub_mb_type);
      numSubMbPart = NumSubMbPart(sub_mb_type);
      if (is_B_mb && sub_mb_type == B_Direct_8x8)
      {
        if (!direct_8x8_init)
          direct_8x8_init = mv_pred_B_SKIP_B_Direct_init_mechanism(mv_cache, refIdx_cache, mb_row, field_pic_flag,
              mb_field_decoding_flag, PicOrderCnt, MbaffFrameFlag, direct_spatial_mv_pred_flag,
              RefPicList, &colPic, &field, &topAbsDiffPOC, &bottomAbsDiffPOC, &CurrPic_PicCodingStruct,
              &ColPic_PicCodingStruct, refIdxDirectL, mvDirectL);
        if (direct_8x8_inference_flag && direct_8x8_init != -1)
        {
          numSubMbPart = 1; // set to one because the B_Direct_8x8 will fill the 4 sub partition at once
        }
      }
    }
    else
      // !has_sub_part
      predMode = (int) MbPartPredMode(mb_type, mbPartIdx, 0, 0);

    for (subMbPartIdx = 0; subMbPartIdx < numSubMbPart; subMbPartIdx++)
    {
      unsigned int subPart6x5idx = part6x5idx;
      int refIdxL[2];
      int16_t mvL[2][2];

      if (has_sub_part)
      {
        LUD_DEBUG_ASSERT((unsigned int) SubPart6x5IdxOffset[partWidth >> 1][partHeight >> 1][subMbPartIdx] < 8);
        subPart6x5idx += SubPart6x5IdxOffset[partWidth >> 1][partHeight >> 1][subMbPartIdx];
      }

      if (!is_B_mb || !has_sub_part || sub_mb_type != B_Direct_8x8) // we do not need this call if processing a B_Direct_8x8 sub mb
      {
        unsigned int idx = subPart6x5idx;
        for (i = 0; i < (is_B_mb != 0) + 1; i++)
        {
          refIdxL[i] = i==0? curr_mb_attr->ref_idx_l0[mbPartIdx] : curr_mb_attr->ref_idx_l1[mbPartIdx];
          // when deriving median_luma_motion_vector_pred partHeight and partWidth are only meaningful when both are >=2
          // that is not the case when processing sub mb partition, that's why they are set to 0 in the later case.
          median_luma_motion_vector_pred(partWidth * (!has_sub_part), partHeight * (!has_sub_part), mbPartIdx, idx, i,
              MbaffFrameFlag, curr_mb_attr, mb_row, PicWidthInMbs, mv_cache[i][idx - 1], mv_cache[i][idx - 6],
              mv_cache[i][idx + partWidth - 6], mv_cache[i][idx - 7], refIdx_cache[i][idx - 1],
              refIdx_cache[i][idx - 6], refIdx_cache[i][idx + partWidth - 6], refIdx_cache[i][idx - 7], refIdxL[i],
              mvL[i]);
        }
      }

      for (r = 0; r < partHeight; r++)
      {
        unsigned int idx = subPart6x5idx;

        for (c = 0; c < partWidth; c++)
        {
          if (!is_B_mb || !has_sub_part || sub_mb_type != B_Direct_8x8)
          {
            for (i = 0; i < (is_B_mb != 0) + 1; i++)
            {
              LUD_DEBUG_ASSERT(predMode <= BiPred);
              if (predMode == BiPred || predMode == Pred_L0 + i) // Pred_L1 is defined as Pred_L0+1
              {
                refIdx_cache[i][idx] = refIdxL[i];

                mv_cache[i][idx][0] = mvL[i][0] + curr_mb_attr->mvd[i][mbPartIdx][subMbPartIdx][0];
                mv_cache[i][idx][1] = mvL[i][1] + curr_mb_attr->mvd[i][mbPartIdx][subMbPartIdx][1];
              }
              else
              {
                refIdx_cache[i][idx] = -1;
                // mv_cache is already initialized to 0
              }
            }
          }
          else // B_Direct_8x8 sub mb !
          {
            if (direct_8x8_init == -1)
            {
              // trivial case: direct_spatial_mv_pred_flag == 1 and, refIdxL0 and refIdxL1 < 0
              // The whole 1x1 need to be filled with 0
              // mv_cache is already initialized to 0
              refIdx_cache[0][idx] = 0;
              // mv_cache is already initialized to 0
              refIdx_cache[1][idx] = 0;
            }
            else // non trivial case when direct_spatial_mv_pred_flag can be 0 or 1
            {
              unsigned int scan4x4idx = raster6x5toscan4x4[idx];
              LUD_DEBUG_ASSERT(scan4x4idx < 16);
              if (!direct_8x8_inference_flag)
              {
                mv_pred_B_SKIP_B_Direct_iterate(direct_spatial_mv_pred_flag, 0, mv_cache, refIdx_cache, scan4x4idx,
                    mb_idx, mb_row, mb_col, mb_field_decoding_flag, bottom_field_flag, field_pic_flag, PicWidthInMbs,
                    RefPicList, FieldOrderCnt, colPic, field, topAbsDiffPOC, bottomAbsDiffPOC,
                    CurrPic_PicCodingStruct, ColPic_PicCodingStruct, refIdxDirectL, mvDirectL);
              }
              else // direct_8x8_inference_flag==1
              {
                scan4x4idx += scan4x4idx >> 2;
                // this need to be called once (numSubPart=1) only as direct_8x8_inference_flag==1 => the 4 blocks are done at once
                mv_pred_B_SKIP_B_Direct_iterate(direct_spatial_mv_pred_flag, 1, mv_cache, refIdx_cache, scan4x4idx,
                    mb_idx, mb_row, mb_col, mb_field_decoding_flag, bottom_field_flag, field_pic_flag, PicWidthInMbs,
                    RefPicList, FieldOrderCnt, colPic, field, topAbsDiffPOC, bottomAbsDiffPOC,
                    CurrPic_PicCodingStruct, ColPic_PicCodingStruct, refIdxDirectL, mvDirectL);
              }
            }
          }

          idx++;
        }
        subPart6x5idx += 6;
      }
      // when direct_8x8_inference_flag, the partSize is actually 8x8
      if (is_B_mb && has_sub_part && sub_mb_type == B_Direct_8x8 && direct_8x8_inference_flag && direct_8x8_init != -1)
      {
        partWidth = 2;
        partHeight = 2;
      }

      SET_PART_SIZE(*partWidths, partWidth, partSizeIdx);
      SET_PART_SIZE(*partHeights, partHeight, partSizeIdx);
      partSizeIdx++;
    }
  }
}

void mv_pred_P_B_mb_simple(struct rv264macro* curr_mb_attr, mb_type_t mb_type, unsigned int numMbPart,
    unsigned int has_sub_part, unsigned int is_B_mb, int16_t mv_cache[2][30][2], int8_t refIdx_cache[2][30],
    unsigned int MbaffFrameFlag, unsigned int mb_row, unsigned int PicWidthInMbs,
    uint32_t* partWidths, uint32_t* partHeights)
{
  LUD_DEBUG_ASSERT(!is_B_mb || !has_sub_part); // ensure simplification at compile time !
  mv_pred_P_B_mb(curr_mb_attr, mb_type, numMbPart, has_sub_part, is_B_mb, PicWidthInMbs, MbaffFrameFlag,
      mv_cache, refIdx_cache, 0, mb_row, 0, 0, 0, 0, 0, 0, 0, 0, 0, partWidths, partHeights);
  return;
}

void print_mb_mv_pred(struct rv264macro* curr_mb_attr, mb_type_t mb_type, unsigned int mb_idx, unsigned int mb_row,
    unsigned int mb_col)
{
  //printf("MB %d (%d,%d) - mb_type:%d\n", mb_idx, mb_col, mb_row, curr_mb_attr->mb_type);
  printf("MB %d\n", mb_idx);
  int r, c, l;

  for (l = 0; l < 2; ++l)
  {
    for (r = 0; r < 4; ++r)
    {
      for (c = 0; c < 4; ++c)
      {
        int v;
        printf("%3d ", (v = curr_mb_attr->RefIdxL[l][r * 4 + c]) == -2 ? -1 : v);
        //printf("%3d ", curr_mb_attr->RefIdxL[l][r*4+c]);
      }
      printf("\t");
      for (c = 0; c < 4; ++c)
      {
        int vx, vy;
        //printf("%5d ", (v = curr_mb_attr->mv[l][r*4+c][0]) == -16192 ? 0 : v );
        //printf("%5d ", (v = curr_mb_attr->mv[l][r*4+c][1]) == -16192 ? 0 : v );
        if (curr_mb_attr->RefIdxL[l][r * 4 + c] >= 0 || curr_mb_attr->RefIdxL[l][r * 4 + c] < -2)
        {
          vx = curr_mb_attr->mvd[l][r][c][0];
          vy = curr_mb_attr->mvd[l][r][c][1];
        }
        else
        {
          vx = 0;
          vy = 0;
        }
        printf("(%3d,%3d) ", vx, vy);
      }
      printf("\n");
    }
    printf("\n");
  }
  printf("\n");
}

void derive_motion_vectors_internal(struct rv264macro* curr_mb_attr, mb_type_t mb_type, unsigned int mb_idx, unsigned int mb_row,
    unsigned int mb_col, unsigned int PicWidthInMbs, unsigned int mb_field_decoding_flag, unsigned int MbaffFrameFlag,
    unsigned int direct_spatial_mv_pred_flag, unsigned int direct_8x8_inference_flag,
    RefListEntry* RefPicList[2], unsigned int field_pic_flag, unsigned int bottom_field_flag,
    int FieldOrderCnt[2], int PicOrderCnt)
{
  int16_t mv_cache[2][30][2];
  int8_t refIdx_cache[2][30];
  unsigned int curr_is_bot = mb_row & 1;

  curr_mb_attr->partWidths = 0;
  curr_mb_attr->partHeights = 0;


  if (mb_type == P_Skip)
  {
    fills_in_mv_cache(curr_mb_attr, curr_is_bot, mb_field_decoding_flag, PicWidthInMbs, mv_cache, refIdx_cache, 0, 1, MbaffFrameFlag);

    mv_pred_P_Skip_mb(mv_cache, refIdx_cache);

    // refIdx values are constant, mv pred is only relevant for list 0 (and stored at location [1][1](=[7]) of the cache array
    mv_cache_fill_const(curr_mb_attr, 1, 0, mv_cache[0][7]);
    mv_cache_fill_const(curr_mb_attr, 2, -2, 0);
    SET_PART_SIZE(curr_mb_attr->partWidths, 4, 0);
    SET_PART_SIZE(curr_mb_attr->partHeights, 4, 0);
  }
  else if (mb_type == B_Skip || mb_type == B_Direct_16x16)
  {

    fills_in_mv_cache(curr_mb_attr, curr_is_bot, mb_field_decoding_flag, PicWidthInMbs, mv_cache, refIdx_cache, 1, 1, MbaffFrameFlag);

    mv_pred_B_Skip_B_Direct_16x16_mb(mv_cache, refIdx_cache, mb_idx, mb_row, mb_col, direct_spatial_mv_pred_flag,
        direct_8x8_inference_flag, RefPicList, field_pic_flag, bottom_field_flag, mb_field_decoding_flag,
        MbaffFrameFlag, FieldOrderCnt, PicOrderCnt, PicWidthInMbs,
        &curr_mb_attr->partWidths, &curr_mb_attr->partHeights);

    mv_cache_write_back(curr_mb_attr, 1, mv_cache, refIdx_cache);
  }
  else if (mb_type == B_8x8)
  {
    fills_in_mv_cache(curr_mb_attr, curr_is_bot, mb_field_decoding_flag, PicWidthInMbs, mv_cache, refIdx_cache, 1, 0, MbaffFrameFlag);

    mv_pred_P_B_mb(curr_mb_attr, B_8x8, 4, 1, 1, PicWidthInMbs, MbaffFrameFlag, mv_cache, refIdx_cache, mb_idx,
        mb_row, mb_col, field_pic_flag, mb_field_decoding_flag,
        direct_spatial_mv_pred_flag, bottom_field_flag, direct_8x8_inference_flag, RefPicList, PicOrderCnt,
        FieldOrderCnt,
        &curr_mb_attr->partWidths, &curr_mb_attr->partHeights);

    mv_cache_write_back(curr_mb_attr, 1, mv_cache, refIdx_cache);
  }
  else if (mb_type == P_8x8 || mb_type == P_8x8ref0)
  {
    fills_in_mv_cache(curr_mb_attr, curr_is_bot, mb_field_decoding_flag, PicWidthInMbs, mv_cache, refIdx_cache, 0, 0, MbaffFrameFlag);

    mv_pred_P_B_mb_simple(curr_mb_attr, mb_type, 4, 1, 0, mv_cache, refIdx_cache, MbaffFrameFlag, mb_row, PicWidthInMbs,
        &curr_mb_attr->partWidths, &curr_mb_attr->partHeights);

    mv_cache_write_back(curr_mb_attr, 0, mv_cache, refIdx_cache);
    mv_cache_fill_const(curr_mb_attr, 2, -2, 0);
  }
  else if (mb_type < B_Direct_16x16) // P mb without sub mb partitions
  {
    int numMbPart = NumMbPart(mb_type);

    if (numMbPart == 1)
    {
      fills_in_mv_cache(curr_mb_attr, curr_is_bot, mb_field_decoding_flag, PicWidthInMbs, mv_cache, refIdx_cache, 0, 1, MbaffFrameFlag);
      mv_pred_P_B_mb_simple(curr_mb_attr, mb_type, 1, 0, 0, mv_cache, refIdx_cache, MbaffFrameFlag, mb_row, PicWidthInMbs,
          &curr_mb_attr->partWidths, &curr_mb_attr->partHeights);
    }
    else
    {
      LUD_DEBUG_ASSERT(numMbPart == 2);
      fills_in_mv_cache(curr_mb_attr, curr_is_bot, mb_field_decoding_flag, PicWidthInMbs, mv_cache, refIdx_cache, 0, 0, MbaffFrameFlag);
      mv_pred_P_B_mb_simple(curr_mb_attr, mb_type, 2, 0, 0, mv_cache, refIdx_cache, MbaffFrameFlag, mb_row, PicWidthInMbs,
          &curr_mb_attr->partWidths, &curr_mb_attr->partHeights);
    }

    mv_cache_write_back(curr_mb_attr, 0, mv_cache, refIdx_cache);
    mv_cache_fill_const(curr_mb_attr, 2, -2, 0);
  }
  else // B mb without sub mb partitions
  {
    int numMbPart = NumMbPart(mb_type);

    if (numMbPart == 1)
    {
      fills_in_mv_cache(curr_mb_attr, curr_is_bot, mb_field_decoding_flag, PicWidthInMbs, mv_cache, refIdx_cache, 1, 1, MbaffFrameFlag);
      mv_pred_P_B_mb_simple(curr_mb_attr, mb_type, 1, 0, 1, mv_cache, refIdx_cache, MbaffFrameFlag, mb_row, PicWidthInMbs,
          &curr_mb_attr->partWidths, &curr_mb_attr->partHeights);
    }
    else
    {
      LUD_DEBUG_ASSERT(numMbPart == 2);
      fills_in_mv_cache(curr_mb_attr, curr_is_bot, mb_field_decoding_flag, PicWidthInMbs, mv_cache, refIdx_cache, 1, 0, MbaffFrameFlag);
      mv_pred_P_B_mb_simple(curr_mb_attr, mb_type, 2, 0, 1, mv_cache, refIdx_cache, MbaffFrameFlag, mb_row, PicWidthInMbs,
          &curr_mb_attr->partWidths, &curr_mb_attr->partHeights);
    }

    mv_cache_write_back(curr_mb_attr, 1, mv_cache, refIdx_cache);
  }


//  if (mb_idx == 5)
//    print_mb_mv_pred(curr_mb_attr, mb_type, mb_idx, mb_row, mb_col);
}


void derive_motion_vectors(PictureDecoderData* pdd)
{
  struct rv264picture* pic = pdd->pic;
  unsigned int bottom_field_flag = pdd->prev_sh->bottom_field_flag; // TODO: remove the use of prev_sh since it really depends on the pdd decoding context.
  struct slice_header* slice0 = pic->field_sh[bottom_field_flag][0]; // get a slice header. It is used for variables that are the same for the whole picture
  struct rv264seq_parameter_set* sps = slice0->sps;
  unsigned int PicWidthInMbs = sps->PicWidthInMbs;
  unsigned int PicHeightInMbs = slice0->PicHeightInMbs;
  int32_t* FieldOrderCnt = pic->FieldOrderCnt;
  int32_t PicOrderCnt = pic->PicOrderCnt;
  unsigned int MbaffFrameFlag = slice0->MbaffFrameFlag;
  unsigned int field_pic_flag = slice0->field_pic_flag;
  unsigned int direct_8x8_inference_flag = sps->direct_8x8_inference_flag;

  struct rv264macro* curr_mb_attr;
  unsigned int mb_idx;
  unsigned int mb_row;
  unsigned int mb_col;
  int slice_num;


  return;

  mb_idx = 0;
  curr_mb_attr = pic->field_data[bottom_field_flag].mb_attr;
  for (mb_row = 0; mb_row < PicHeightInMbs; ++mb_row)
  {
    for (mb_col = 0; mb_col < PicWidthInMbs; ++mb_col)
    {
      mb_type_t mb_type = curr_mb_attr->mb_type;

      if (mb_type > SI)
      {
        slice_num = curr_mb_attr->slice_num;
        struct slice_header* sh = pic->field_sh[bottom_field_flag][slice_num];
        unsigned int mb_field_decoding_flag = curr_mb_attr->mb_field_decoding_flag;
        unsigned int direct_spatial_mv_pred_flag = sh->direct_spatial_mv_pred_flag;


        derive_motion_vectors_internal(curr_mb_attr, mb_type, mb_idx, mb_row,
            mb_col, PicWidthInMbs, mb_field_decoding_flag, MbaffFrameFlag,
            direct_spatial_mv_pred_flag, direct_8x8_inference_flag,
            sh->RefPicList, field_pic_flag, bottom_field_flag,
            FieldOrderCnt, PicOrderCnt);
      }

      curr_mb_attr++;
      mb_idx++;
    }
  }
}
