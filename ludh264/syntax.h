/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/


#ifndef __SYNTAX_H__
#define __SYNTAX_H__

#include "bitstream_types.h"
#include "decode.h"



// Parsing function definition
RetCode parse_nal_unit(const uint8_t* buffer, uint32_t length, nal_unit_t** nalu);
void release_nalu(nal_unit_t* nalu);
RetCode parse_sps(nal_unit_t* nalu, struct rv264seq_parameter_set** sps);
void release_sps(struct rv264seq_parameter_set* sps);
RetCode parse_pps(struct rv264sequence *seq, nal_unit_t* nalu, struct rv264pic_parameter_set** pps);
void release_pps(struct rv264pic_parameter_set* pps);
RetCode parse_slice_header_lud(struct rv264sequence *seq, nal_unit_t* nalu, struct slice_header** sh);
void release_slice_header(struct slice_header* sh);
RetCode parse_filler_data(nal_unit_t* nalu);

RetCode parse_rbsp_slice_trailing_bits(nal_unit_t* nalu, unsigned int entropy_coding_mode_flag);
RetCode parse_rbsp_trailing_bits_lud(nal_unit_t* nalu, struct bitbuf* bs, unsigned int entropy_coding_mode_flag);
int more_rbsp_data_lud(nal_unit_t* nalu, struct bitbuf *bs);

#endif // __SYNTAX_H__

