/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/


#ifndef LUDH264_H_
#define LUDH264_H_


#include <stdint.h>
#include <stdbool.h>

#include "../rvh264.h"

typedef enum
{
  LUDH264_SUCCESS,
  LUDH264_ERR_NO_START_CODE,
  LUDH264_ERR_NO_ENDING_START_CODE,
  LUDH264_ERR_WORKING_BUFFER_TOO_SMALL,
  LUDH264_ERR_MEMORY_ALLOC_FAILED,
  LUDH264_ERR_NALU_EMPTY,
  LUDH264_ERR_PARSED_VALUE_OUT_OF_RANGE,
  LUDH264_ERR_REFERING_NON_EXISTING_SPS,
  LUDH264_ERR_REFERING_NON_EXISTING_PPS,
  LUDH264_ERR_NO_PICTURE_PARAMS,
} RetCode;

typedef enum
{
  LUDH264_YUV420,
  LUDH264_YUV422,
  LUDH264_YUV444,
} LUDH264_VIDEO_FORMAT;

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

/**
 * Initialize the ludh264 decoder library
 */
RetCode ludh264_init(struct rv264sequence *seq);

/**
 * Destroy and release resources used by the ludh264 library
 */
RetCode ludh264_destroy(struct rv264sequence *seq);

/**
 * Reset the decoding context. This is necessary when seeking in the stream.
 * Previously received SPS/PPS elements are still valid after a decoder reset, however
 *   the stream must restart with an IDR NAL unit so to re-initialize the decoding process.
 */
RetCode ludh264_reset(struct rv264sequence *seq);

/**
 * Extract a NAL unit from a bit stream.
 *
 * This function does not do a copy of the buffer, so the buffer must remain valid until the decoding of this nalu is done
 *  (the decoding of the nalu is done when the function ludh264_decode_nalu() returned)
 *
 * @param stream_buffer [in] the buffer that hold data to decode. Must start on a NAL unit boundary
 * @param stream_available_consumed [in,out] in: byte available to parse, out: byte actually consumed from the buffer.
 * @param nalubuf [out] pointer on the extracted NAL Unit
 * @param nalusize [out] size of the extracted NAL Unit
 * @param endofstream [in] should be set to non null when the stream not providing data anymore. This flag will change the
 *                         behavior of this function does not return an error if a second start code is not found.
 *
 * @return LUDH264_ERR_NO_START_CODE when the buffer does not start with a start code
 * @return LUDH264_ERR_NO_ENDING_START_CODE when the buffer does not contains a second start code (end of current NAL unit)
 *          and endofstream is set to null.
 *
 */
RetCode ludh264_parse_bytestream_nalu(struct rv264sequence *seq, uint8_t* stream_buffer, uint32_t* stream_available_consumed, uint8_t** nalubuf, uint32_t* nalusize, bool endofstream);

/**
 * Decode a NAL unit and return a picture if available.
 *
 * This function does not do a copy of the buffer, so the buffer must remain valid until the decoding of this nalu is done
 * (i.e. this function returned)
 * This function performs "emulation prevention three byte" process, and do it in place so this function may alter the buffer content.
 */
RetCode ludh264_decode_nalu(struct rv264sequence *seq, uint8_t* nalubuf, uint32_t nalusize, struct rv264picture** pic);

/**
 * Return the picture parameters without returning a decoded picture. This relies on
 * sufficient decoding to have been done already.
 */
RetCode ludh264_get_picture_params(struct rv264sequence *seq, LUDH264_VIDEO_FORMAT* format, uint32_t* width, uint32_t* height);

/**
 * Return a pointer on the picture decoded buffer. The buffer is composed of YUV planar buffer.
 * The buffer is composed of the Y component followed by Cb component, and then Cr component.
 * The Chroma format depends on the format of the video sequence.
 */
RetCode ludh264_get_picture(struct rv264sequence *seq, struct rv264picture* pic, void** buffer, uint32_t* buffer_size, LUDH264_VIDEO_FORMAT* format, uint32_t* width, uint32_t* height);

/**
 */
RetCode ludh264_get_picture_metadata(struct rv264sequence *seq, struct rv264picture* pic, struct rv264seq_parameter_set **sps, struct rv264pic_parameter_set **pps, struct slice_header **sh, struct rv264macro **macro);

/**
 */
RetCode ludh264_get_picture_metadata_bottom_field(struct rv264sequence *seq, struct rv264picture* pic, struct rv264seq_parameter_set **sps, struct rv264pic_parameter_set **pps, struct slice_header **sh, struct rv264macro **macro);

/**
 * Free a decoded picture.
 * Should be called only when the picture has been displayed and not used anymore.
 */
RetCode ludh264_free_picture(struct rv264sequence *seq, struct rv264picture* pic);

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif /* LUDH264_H_ */
