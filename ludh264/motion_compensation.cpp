/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/



#include <string.h>
#include "common.h"
#include "motion_compensation_types.h"
#include "defaulttables.h"
#include "intmath.h"


#define hpel_func hpel_func_c
#define qpel_func qpel_func_c

// Pixel interpolation process with motion vectors height
#define FILTER_6TAP(s, of, st) (s[of -2*(signed)(st)] -5*s[of -1*(signed)(st)] +20*s[of +0*(st)] +20*s[of +1*(st)] -5*s[of +2*(st)] +s[of +3*(st)])

// Load the border of the image into a buffer so that buffer can be used without checking in the interpolation functions.
void prefetch_unsafe_borders(const pixel_t* src, unsigned int src_dy, pixel_t* dst, unsigned int dst_dy,
    int partWidth, int partHeight, int x, int y, int picWidth, int picHeight)
{
  int row, col;
  const pixel_t* lastline = src + (picHeight-1)*src_dy;
  int colmax0 = im_min(partWidth, -x);
  int rowmax0 = im_min(partHeight, -y);
  int colmax1 = im_min(partWidth, picWidth-x);
  int rowmax1 = im_min(partHeight, picHeight-y);

  // No need to prefetch safe cases
  LUD_DEBUG_ASSERT(x<0 || y<0 || x+partWidth>=picWidth || y+partHeight>=picHeight);

  for (row = 0; row<rowmax0; row++)
  {
    for (col= 0; col<colmax0; col++)
      dst[col] = src[0];
    for (; col<colmax1; col++)
      dst[col] = src[x+col];
    for (; col<partWidth; col++)
      dst[col] = src[picWidth-1];
    dst += dst_dy;
  }

  src += src_dy*(y+row);
  for (; row<rowmax1; row++)
  {
    for (col=0; col<colmax0; col++)
      dst[col] = src[0];
    for (; col<colmax1; col++)
      dst[col] = src[x+col];
    for (; col<partWidth; col++)
      dst[col] = src[picWidth-1];
    dst += dst_dy;
    src += src_dy;
  }

  src = lastline;
  for (; row<partHeight; row++)
  {
    for (col=0; col<colmax0; col++)
      dst[col] = src[0];
    for (; col<colmax1; col++)
      dst[col] = src[x+col];
    for (; col<partWidth; col++)
      dst[col] = src[picWidth-1];
    dst += dst_dy;
  }
}

void luma_samples_interpolation_process(int x, int y, int mvx, int mvy, int partWidth4, int partHeight4,
    const pixel_t* src, unsigned int src_dy, pixel_t* dst, unsigned int dst_dy,
    unsigned int picWidth, unsigned int picHeight, unsigned int clipmax)
{
  pixel_t* s;
  int xFrac = mvx & 3;
  int yFrac = mvy & 3;
  int horiz_offset = xFrac != 0;
  int vert_offset = yFrac != 0;
  int partWidth = partWidth4 * 4;
  int partHeight = partHeight4 * 4;
  pixel_t cache_src[32*(16+6)];

  x += mvx>>2;
  y += mvy>>2;

  // detect if the src partition go out of the picture
  if (x-2*horiz_offset<0 || (x+3*horiz_offset+partWidth)>picWidth || y-2*vert_offset<0 || (y+3*vert_offset+partHeight)>picHeight)
  {
    // Horizontal offset (x-2 -> partWidth+3) => needed when xFrac!=0
    // Vertical offset (y-2 -> partHeight+3) => needed when yFrac!=0
    prefetch_unsafe_borders(src, src_dy, cache_src, 32,
        partWidth+5*horiz_offset, partHeight+5*vert_offset, x-2*horiz_offset, y-2*vert_offset, picWidth, picHeight);
    src_dy = 32;
    s = cache_src+2*horiz_offset + 2*src_dy*vert_offset;
  }
  else
  {
    s = (pixel_t*)src+x+y*src_dy;
  }

  qpel_func[partWidth4-1][xFrac][yFrac](partHeight4, s, src_dy, dst, dst_dy, clipmax);
}

void chroma_samples_interpolation_process(int x, int y, int mvx, int mvy, unsigned int partWidth4, unsigned int partHeight4,
    const pixel_t* src[2], unsigned int src_dy, pixel_t* dst[2], unsigned int dst_dy, unsigned int picWidth, unsigned int picHeight, unsigned int cfidc)
{
  int i;
  unsigned int xFrac;
  unsigned int yFrac;
  unsigned int horiz_offset;
  unsigned int vert_offset;
  unsigned int partWidth = partWidth4 * MbWidthCdiv4[cfidc];
  unsigned int partHeight = partHeight4 * MbHeightCdiv4[cfidc];
  pixel_t cache_src[(16+1)*(16+1)];

  x = x / SubWidthC[cfidc];
  y = y / SubHeightC[cfidc];
  x += mvx>>(1+SubWidthC[cfidc]);
  y += mvy>>(1+SubHeightC[cfidc]);

  if (cfidc == 1)
  {
    xFrac = mvx & 7;
    yFrac = mvy & 7;
  }
  else if (cfidc == 2)
  {
    xFrac = mvx & 7;
    yFrac = (mvy & 3) << 1;
  }
  else // cfidc == 3
  {
    xFrac = (mvx & 3) << 1;
    yFrac = (mvy & 3) << 1;
  }

  horiz_offset = xFrac != 0;
  vert_offset = yFrac != 0;


  for (i=0; i<2; i++)
  {
    unsigned int s_dy;
    pixel_t* s;

    // detect if the src partition goes out of the picture
    if (x<0 || (x+horiz_offset+partWidth)>picWidth || y<0 || (y+vert_offset+partHeight)>picHeight)
    {
      prefetch_unsafe_borders(src[i], src_dy, cache_src, 16+1, partWidth+horiz_offset, partHeight+vert_offset, x, y, picWidth, picHeight);
      s_dy = 16+1;
      s = cache_src;
    }
    else
    {
      s = (pixel_t*)src[i]+x+y*src_dy;
      s_dy = src_dy;
    }

    LUD_DEBUG_ASSERT(cfidc < 4);
    hpel_func[cfidc-1][partWidth4-1][horiz_offset][vert_offset](partHeight4, xFrac, yFrac, s, s_dy, dst[i], dst_dy);
  }
}


void default_weighted_sample_prediction_process(int partSize,
    const pixel_t* src0, const pixel_t* src1, unsigned int src_dy, pixel_t* dst, unsigned int dst_dy)
{
  dwsp[partSize](src0, src1, src_dy, dst, dst_dy);
}

void explicit_weigthed_sample_prediction_process_monopred(int partSize,
    const pixel_t* src, unsigned int src_dy, pixel_t* dst, unsigned int dst_dy, unsigned int clip,
    unsigned int log2_weight_denom,
    int refIdxLXWP, int8_t* weight_lX, int16_t* offset_lX)
{
  int logWD = log2_weight_denom;

  int w, o;
  w = weight_lX[refIdxLXWP];
  o = offset_lX[refIdxLXWP];
  wimp[partSize][logWD>0](src, src_dy, dst, dst_dy, clip, o, w, logWD);
}

void explicit_weigthed_sample_prediction_process_bipred(int partSize,
    const pixel_t* src0, const pixel_t* src1, unsigned int src_dy, pixel_t* dst, unsigned int dst_dy, unsigned int clip,
    unsigned int log2_weight_denom, int refIdxL0WP, int refIdxL1WP,
    int8_t* weight_l0, int8_t* weight_l1, int16_t* offset_l0, int16_t* offset_l1)
{
  int logWD = log2_weight_denom;
  int w0, o0;
  int w1, o1;
  w0 = weight_l0[refIdxL0WP];
  o0 = offset_l0[refIdxL0WP];
  w1 = weight_l1[refIdxL1WP];
  o1 = offset_l1[refIdxL1WP];

  wibp[partSize](src0, src1, src_dy, dst, dst_dy, clip, o0, w0, o1, w1, logWD);
}

void implicit_weigthed_sample_prediction_process_compute_wx(
    unsigned int mb_field_decoding_flag, unsigned int bottom_field_flag, unsigned int field_pic_flag, unsigned int mb_row,
    RefListEntry* RefPicList[2], int FieldOrderCnt[2], int refIdxL0, int refIdxL1, int* w0_, int* w1_)
{
  unsigned int mb_parity = mb_row%2;
  int currPicPOC, pic0POC, pic1POC;
  ref_pic_type_t pic0RefPicType, pic1RefPicType;
  int w0, w1;

  LUD_DEBUG_ASSERT(refIdxL0 >= 0 && refIdxL1 >= 0);

  if (!field_pic_flag && mb_field_decoding_flag)
  {
    currPicPOC = FieldOrderCnt[mb_parity];
    pic0POC = RefPicList[0][refIdxL0 / 2].ref_pic->FieldOrderCnt[(refIdxL0 % 2) ^ mb_parity]; //  (refIdxL0 % 2) ^ mb_parity = refIdxL0 % 2 ? 1-mb_parity : mb_parity
    pic1POC = RefPicList[1][refIdxL1 / 2].ref_pic->FieldOrderCnt[(refIdxL0 % 2) ^ mb_parity];
    pic0RefPicType = RefPicList[0][refIdxL0 / 2].ref_pic_type;
    pic1RefPicType = RefPicList[1][refIdxL1 / 2].ref_pic_type;
  }
  else
  {
    currPicPOC = FieldOrderCnt[bottom_field_flag];
    pic0POC = RefPicList[0][refIdxL0].ref_pic->FieldOrderCnt[RefPicList[0][refIdxL0].parity];
    pic1POC = RefPicList[1][refIdxL1].ref_pic->FieldOrderCnt[RefPicList[1][refIdxL1].parity];
    pic0RefPicType = RefPicList[0][refIdxL0].ref_pic_type;
    pic1RefPicType = RefPicList[1][refIdxL1].ref_pic_type;
  }

  if (pic0RefPicType == LONG_TERM_REF || pic1RefPicType == LONG_TERM_REF || (pic1POC - pic0POC) == 0)
    w0 = w1 = 32;
  else
  {
    int tb = im_clip(-128, 127, currPicPOC - pic0POC);
    int td = im_clip(-128, 127, pic1POC - pic0POC);
    int tx = (16384 + im_abs(td / 2)) / td;
    int DistScaleFactor = im_clip(-1024, 1023, (tb * tx + 32) >> 6);

    DistScaleFactor >>= 2;
    if (DistScaleFactor < -64 || DistScaleFactor>128)
      w0 = w1 = 32;
    else
    {
      w0 = 64 - DistScaleFactor;
      w1 = DistScaleFactor;
    }
  }
  *w0_ = w0;
  *w1_ = w1;
}


void implicit_weigthed_sample_prediction_process_apply(int partSize,
    const pixel_t* src0, const pixel_t* src1, unsigned int src_dy, pixel_t* dst, unsigned int dst_dy,
    unsigned int clip, int w0, int w1)
{
  // logWD = 5;
  // o0 = o1 = 0
  wibpi[partSize](src0, src1, src_dy, dst, dst_dy, clip, w0, w1);
}

// Letters in the function name is the same as the ones in illustration 8-4, p153 (H264 03/2005)
// xFrac = yFrac = 0
void luma_sample_interpolation_G(int partWidth4, int partHeight4, int xFrac, int yFrac,
    const pixel_t* src, int src_dy, pixel_t* dst, int dst_dy, int clipmax)
{
  int row4;
  int col;

  for (row4 = 0; row4 < partHeight4; row4++)
  {
    for (col = 0; col < partWidth4*4; col += 4)
    {
      dst[col]   = src[col];
      dst[col+1] = src[col+1];
      dst[col+2] = src[col+2];
      dst[col+3] = src[col+3];
    }
    dst += dst_dy;
    src += src_dy;
    for (col = 0; col < partWidth4*4; col += 4)
    {
      dst[col]   = src[col];
      dst[col+1] = src[col+1];
      dst[col+2] = src[col+2];
      dst[col+3] = src[col+3];
    }
    dst += dst_dy;
    src += src_dy;
    for (col = 0; col < partWidth4*4; col += 4)
    {
      dst[col]   = src[col];
      dst[col+1] = src[col+1];
      dst[col+2] = src[col+2];
      dst[col+3] = src[col+3];
    }
    dst += dst_dy;
    src += src_dy;
    for (col = 0; col < partWidth4*4; col += 4)
    {
      dst[col]   = src[col];
      dst[col+1] = src[col+1];
      dst[col+2] = src[col+2];
      dst[col+3] = src[col+3];
    }
    dst += dst_dy;
    src += src_dy;
  }
}

// xFrac = 1.2.3, yFrac = 0: abc samples
void luma_sample_interpolation_abc(int partWidth4, int partHeight4, int xFrac, int yFrac,
    const pixel_t* src, int src_dy, pixel_t* dst, int dst_dy, int clipmax)
{
  int row, col;
  int r0, r1, c0, c1;

  for (r0 = 0, row = 0; r0 < partHeight4; r0++)
  {
    for (r1 = 0; r1 < 4; r1++)
    {
      for (c0 = 0, col = 0; c0 < partWidth4; c0++)
      {
        for (c1 = 0; c1 < 4; c1++)
        {
          unsigned int val;
          int b = FILTER_6TAP(src, col, 1);
          b = im_clip(0, clipmax, (b + 16)>>5);
          if (xFrac == 2)     // b
            val = b;
          else if(xFrac == 1) // a
            val = (src[col]+b+1) >> 1;
          else // xFrac == 3  // c
            val = (src[col+1]+b+1) >> 1;
          dst[col] = val;
          col++;
        }
      }
      dst += dst_dy;
      src += src_dy;
      row++;
    }
  }
}

// xFrac = 0, yFrac = 1.2.3: dhn samples
void luma_sample_interpolation_dhn(int partWidth4, int partHeight4, int xFrac, int yFrac,
    const pixel_t* src, int src_dy, pixel_t* dst, int dst_dy, int clipmax)
{
  int row, col;
  int r0, r1, c0, c1;

  for (r0 = 0, row = 0; r0 < partHeight4; r0++)
  {
    for (r1 = 0; r1 < 4; r1++)
    {
      for (c0 = 0, col = 0; c0 < partWidth4; c0++)
      {
        for (c1 = 0; c1 < 4; c1++)
        {
          unsigned int val;
          int h = FILTER_6TAP(src, col, src_dy);
          h = im_clip(0, clipmax, (h + 16)>>5);
          if (yFrac == 2)     // d
            val = h;
          else if(yFrac == 1) // h
            val = (src[col]+h+1) >> 1;
          else // yFrac == 3  // n
            val = (src[col+src_dy]+h+1) >> 1;
          dst[col] = val;
          col++;
        }
      }
      dst += dst_dy;
      src += src_dy;
      row++;
    }
  }
}

// xFrac = 1.2.3, yFrac = 2, j horiz: ijk samples
void luma_sample_interpolation_ijk(int partWidth4, int partHeight4, int xFrac, int yFrac,
    const pixel_t* src, int src_dy, pixel_t* dst, int dst_dy, int clipmax)
{
  int row, col;
  int r0, r1, c0, c1;
  int cachebuffer[22]; // at least 2+partWidth+3
  int* cache = cachebuffer+2;


  for (r0 = 0, row = 0; r0 < partHeight4; r0++)
  {
    for (r1 = 0; r1 < 4; r1++)
    {
      // derive left vert half samples (cc, dd, h, m, ee) the 6th sample (ff) is derive at each iteration bellow
      for (col = -2; col < 3; ++col)
        cache[col] = FILTER_6TAP(src, col, src_dy);

      const pixel_t* s = src+3;

      for (c0 = 0, col = 0; c0 < partWidth4; c0++)
      {
        for (c1 = 0; c1 < 4; c1++)
        {
          // derive ff sample
          cache[3+col] = FILTER_6TAP(s, col, src_dy);

          int j = FILTER_6TAP(cache, col, 1);
          j = im_clip(0, clipmax, (j + 512)>>10);

          unsigned int val;
          if (xFrac == 2)     // j
            val = j;
          else if(xFrac == 1) // i
          {
            unsigned int h = im_clip(0, clipmax, (cache[col] + 16)>>5);
            val = (h+j+1) >> 1;
          }
          else // xFrac == 3  // k
          {
            unsigned int m = im_clip(0, clipmax, (cache[col+1] + 16)>>5);
            val = (j+m+1) >> 1;
          }
          dst[col] = val;
          col++;
        }
      }
      dst += dst_dy;
      src += src_dy;
      row++;
    }
  }
}
// xFrac = 2, yFrac = 1.2.3  j vert: fjq samples
void luma_sample_interpolation_fjq(int partWidth4, int partHeight4, int xFrac, int yFrac,
    const pixel_t* src, int src_dy, pixel_t* dst, int dst_dy, int clipmax)
{
  int row, col;
  int r0, r1, c0, c1;
  int cachebuffer[22]; // at least 2+partHeight+3
  int* cache = cachebuffer+2;

  for (c0 = 0, col = 0; c0 < partWidth4; c0++)
  {
    for (c1 = 0; c1 < 4; c1++)
    {
      const pixel_t* s = src+col;
      pixel_t* d = dst+col;

      // derive left vert half samples (aa, bb, b, s, gg) the 6th sample (hh) is derived at each iteration bellow
      for (row = -2; row < 3; ++row)
        cache[row] = FILTER_6TAP(s, row*src_dy, 1);

      for (r0 = 0, row = 0; r0 < partHeight4; r0++)
      {
        for (r1 = 0; r1 < 4; r1++)
        {
          // derive hh sample
          cache[3+row] = FILTER_6TAP(s, 3*src_dy, 1);

          int j = FILTER_6TAP(cache, row, 1);
          j = im_clip(0, clipmax, (j + 512)>>10);

          unsigned int val;

          if (yFrac == 2)     // j
            val = j;
          else if(yFrac == 1) // f
          {
            unsigned int b = im_clip(0, clipmax, (cache[row] + 16)>>5);
            val = (b+j+1) >> 1;
          }
          else // yFrac == 3  // q
          {
            unsigned int s = im_clip(0, clipmax, (cache[row+1] + 16)>>5);
            val = (j+s+1) >> 1;
          }
          d[0] = val;

          d += dst_dy;
          s += src_dy;
          row++;
        }
      }
      col++;
    }
  }
}
// xFrac = 2, yFrac = 2  j sample
void luma_sample_interpolation_j(int partWidth4, int partHeight4, int xFrac, int yFrac,
    const pixel_t* src, int src_dy, pixel_t* dst, int dst_dy, int clipmax)
{
  if (partWidth4>partHeight4)
    luma_sample_interpolation_ijk(partWidth4, partHeight4, xFrac, yFrac, src, src_dy, dst, dst_dy, clipmax);
  else
    luma_sample_interpolation_fjq(partWidth4, partHeight4, xFrac, yFrac, src, src_dy, dst, dst_dy, clipmax);
}

// xFrac = 1 or 3, yFrac=1 or 3 egpr samples
// Those sample derivation needs to maintain both horiz and vert cache !!!
void luma_sample_interpolation_egpr(int partWidth4, int partHeight4, int xFrac, int yFrac,
    const pixel_t* src, int src_dy, pixel_t* dst, int dst_dy, int clipmax)
{
  int row, col;
  int r0, r1, c0, c1;

  for (r0 = 0, row = 0; r0 < partHeight4; r0++)
  {
    for (r1 = 0; r1 < 4; r1++)
    {
      for (c0 = 0, col = 0; c0 < partWidth4; c0++)
      {
        for (c1 = 0; c1 < 4; c1++)
        {
          int v1;
          int v2;

          if (xFrac == 1)
            v1 = FILTER_6TAP(src, col, src_dy); // h sample
          else // xFrac == 3
            v1 = FILTER_6TAP(src, col+1, src_dy); // m sample
          v1 = im_clip(0, clipmax, (v1 + 16)>>5);

          if (yFrac == 1)
            v2 = FILTER_6TAP(src, col, 1); // b sample
          else // yFrac == 3
            v2 = FILTER_6TAP(src, col+src_dy, 1); // s sample
          v2 = im_clip(0, clipmax, (v2 + 16)>>5);

          dst[col] = (v1 + v2 + 1) >> 1;
          col++;
        }
      }
      dst += dst_dy;
      src += src_dy;
      row++;
    }
  }
}

#define INTERPOLATION_FUNC(newname, funcname, xfrac, yfrac)                                                 \
  static void newname##xfrac##yfrac##_4(int partHeight4, const pixel_t* src, unsigned int src_dy,           \
    pixel_t* dst, unsigned int dst_dy, int clipmax)                                                         \
  {                                                                                                         \
    funcname(1, partHeight4, xfrac, yfrac, src, src_dy, dst, dst_dy, clipmax);                              \
  }                                                                                                         \
  static void newname##xfrac##yfrac##_8(int partHeight4, const pixel_t* src, unsigned int src_dy,           \
    pixel_t* dst, unsigned int dst_dy, int clipmax)                                                         \
  {                                                                                                         \
    funcname(2, partHeight4, xfrac, yfrac, src, src_dy, dst, dst_dy, clipmax);                              \
  }                                                                                                         \
  static void newname##xfrac##yfrac##_16(int partHeight4, const pixel_t* src, unsigned int src_dy,          \
    pixel_t* dst, unsigned int dst_dy, int clipmax)                                                         \
  {                                                                                                         \
    funcname(4, partHeight4, xfrac, yfrac, src, src_dy, dst, dst_dy, clipmax);                              \
  }                                                                                                         \


INTERPOLATION_FUNC(interfunc, luma_sample_interpolation_G,   0, 0)
INTERPOLATION_FUNC(interfunc, luma_sample_interpolation_abc, 1, 0)
INTERPOLATION_FUNC(interfunc, luma_sample_interpolation_abc, 2, 0)
INTERPOLATION_FUNC(interfunc, luma_sample_interpolation_abc, 3, 0)
INTERPOLATION_FUNC(interfunc, luma_sample_interpolation_dhn, 0, 1)
INTERPOLATION_FUNC(interfunc, luma_sample_interpolation_egpr,1, 1)
INTERPOLATION_FUNC(interfunc, luma_sample_interpolation_fjq, 2, 1)
INTERPOLATION_FUNC(interfunc, luma_sample_interpolation_egpr,3, 1)
INTERPOLATION_FUNC(interfunc, luma_sample_interpolation_dhn, 0, 2)
INTERPOLATION_FUNC(interfunc, luma_sample_interpolation_ijk, 1, 2)
INTERPOLATION_FUNC(interfunc, luma_sample_interpolation_j,   2, 2)
INTERPOLATION_FUNC(interfunc, luma_sample_interpolation_ijk, 3, 2)
INTERPOLATION_FUNC(interfunc, luma_sample_interpolation_dhn, 0, 3)
INTERPOLATION_FUNC(interfunc, luma_sample_interpolation_egpr,1, 3)
INTERPOLATION_FUNC(interfunc, luma_sample_interpolation_fjq, 2, 3)
INTERPOLATION_FUNC(interfunc, luma_sample_interpolation_egpr,3, 3)


#define QPELSET(w)                                                        \
{                                                                         \
  {interfunc00_##w, interfunc01_##w, interfunc02_##w, interfunc03_##w},   \
  {interfunc10_##w, interfunc11_##w, interfunc12_##w, interfunc13_##w},   \
  {interfunc20_##w, interfunc21_##w, interfunc22_##w, interfunc23_##w},   \
  {interfunc30_##w, interfunc31_##w, interfunc32_##w, interfunc33_##w},   \
}


const qpel_func_t qpel_func_c[4][4][4] = // [partWidth4][xFrac][yFrac]
{
  QPELSET(4),
  QPELSET(8),
  QPELSET(8),
  QPELSET(16)
};


void chroma_sample_interpolation(unsigned int partWidth4, unsigned int partHeight4, unsigned int xfn, unsigned int yfn, unsigned int xFrac, unsigned int yFrac,
    const pixel_t* src, unsigned int src_dy, pixel_t* dst, unsigned int dst_dy, unsigned int cfidc)
{

  int col;
  unsigned int r0, r1, c0, c1;
  unsigned int w = MbWidthCdiv4[cfidc];
  unsigned int h = MbHeightCdiv4[cfidc];

  for (r0 = 0; r0 < partHeight4; r0++)
  {
    for (r1 = 0; r1 < h; r1++)
    {
      for (c0 = 0, col = 0; c0 < partWidth4; c0++)
      {
        for (c1 = 0; c1 < w; c1++)
        {
          if (xfn && yfn)
            dst[col] = ( (8-xFrac) * (8-yFrac)*src[col]
                         + xFrac * (8-yFrac) * src[col+1]
                         + (8-xFrac) * yFrac * src[col+src_dy]
                         + xFrac * yFrac * src[col+1+src_dy]
                         + 32) >> 6;
          else if (!xfn && yfn) // xFrac == 0
            dst[col] = ( 8 * (8-yFrac)*src[col]
                         + 8 * yFrac * src[col+src_dy]
                         + 32) >> 6;
          else if (xfn) // yFrac == 0
            dst[col] = ( (8-xFrac) * 8 * src[col]
                         + xFrac * 8 * src[col+1]
                         + 32) >> 6;
          else // xFrac == 0 and yFrac == 0
            dst[col] = src[col];


          col++;
        }
      }
      dst += dst_dy;
      src += src_dy;
    }
  }
}

#define chromainter(xfn, yfn, cfidc)                                                                                  \
  static void chromainter_##xfn##yfn##_cfidc##cfidc##_1(unsigned int partHeight4,                                     \
  unsigned int xFrac, unsigned int yFrac, const pixel_t* src, unsigned int src_dy, pixel_t* dst, unsigned int dst_dy) \
  {                                                                                                                   \
    chroma_sample_interpolation(1, partHeight4, xfn, yfn, xFrac, yFrac, src, src_dy, dst, dst_dy, cfidc);             \
  }                                                                                                                   \
  static void chromainter_##xfn##yfn##_cfidc##cfidc##_2(unsigned int partHeight4,                                     \
  unsigned int xFrac, unsigned int yFrac, const pixel_t* src, unsigned int src_dy, pixel_t* dst, unsigned int dst_dy) \
  {                                                                                                                   \
    chroma_sample_interpolation(2, partHeight4, xfn, yfn, xFrac, yFrac, src, src_dy, dst, dst_dy, cfidc);             \
  }                                                                                                                   \
  static void chromainter_##xfn##yfn##_cfidc##cfidc##_4(unsigned int partHeight4,                                     \
  unsigned int xFrac, unsigned int yFrac, const pixel_t* src, unsigned int src_dy, pixel_t* dst, unsigned int dst_dy) \
  {                                                                                                                   \
    chroma_sample_interpolation(4, partHeight4, xfn, yfn, xFrac, yFrac, src, src_dy, dst, dst_dy, cfidc);             \
  }

#define chromaintercfidc(cfidc) \
  chromainter(0, 0, cfidc)      \
  chromainter(1, 0, cfidc)      \
  chromainter(0, 1, cfidc)      \
  chromainter(1, 1, cfidc)      \


chromaintercfidc(1)
chromaintercfidc(2)
chromaintercfidc(3)


const hpel_func_t hpel_func_c[3][4][2][2] = // [cfidc-1]
  {
    {
      { { chromainter_00_cfidc1_1, chromainter_01_cfidc1_1 }, { chromainter_10_cfidc1_1, chromainter_11_cfidc1_1 } },
      { { chromainter_00_cfidc1_2, chromainter_01_cfidc1_2 }, { chromainter_10_cfidc1_2, chromainter_11_cfidc1_2 } },
      { { 0, 0 }, { 0, 0 } },
      { { chromainter_00_cfidc1_4, chromainter_01_cfidc1_4 }, { chromainter_10_cfidc1_4, chromainter_11_cfidc1_4 } },
    },
    {
      { { chromainter_00_cfidc2_1, chromainter_01_cfidc2_1 }, { chromainter_10_cfidc2_1, chromainter_11_cfidc2_1 } },
      { { chromainter_00_cfidc2_2, chromainter_01_cfidc2_2 }, { chromainter_10_cfidc2_2, chromainter_11_cfidc2_2 } },
      { { 0, 0 }, { 0, 0 } },
      { { chromainter_00_cfidc2_4, chromainter_01_cfidc2_4 }, { chromainter_10_cfidc2_4, chromainter_11_cfidc2_4 } },
    },
    {
      { { chromainter_00_cfidc3_1, chromainter_01_cfidc3_1 }, { chromainter_10_cfidc3_1, chromainter_11_cfidc3_1 } },
      { { chromainter_00_cfidc3_2, chromainter_01_cfidc3_2 }, { chromainter_10_cfidc3_2, chromainter_11_cfidc3_2 } },
      { { 0, 0 }, { 0, 0 } },
      { { chromainter_00_cfidc3_4, chromainter_01_cfidc3_4 }, { chromainter_10_cfidc3_4, chromainter_11_cfidc3_4 } },
    }
  };








void weighted_interpolation_monopred(int width, int height,
    const pixel_t src[], unsigned int src_dy, pixel_t dst[], unsigned int dst_dy, int clip,
    int o, int w, int logWD)
{
  int r, c;

  for (r = 0; r < height; ++r)
  {
    for (c = 0; c < width; ++c)
    {
      if (logWD >= 1)
        dst[c] = im_clip(0, clip, ((src[c]*w + (1<<(logWD-1))) >> logWD) + o );
      else
        dst[c] = im_clip(0, clip, src[c]*w + o );
    }
    dst += dst_dy;
    src += src_dy;
  }
}

void weighted_interpolation_bipred(int width, int height,
    const pixel_t src0[], const pixel_t src1[], unsigned int src_dy, pixel_t dst[], unsigned int dst_dy, int clip,
    int o0, int w0, int o1, int w1, int logWD)
{
  int r, c;

  for (r = 0; r < height; ++r)
  {
    for (c = 0; c < width; ++c)
    {
      dst[c] = im_clip(0, clip, ((src0[c]*w0 + src1[c]*w1 + (1<<logWD)) >> (logWD+1)) + ((o0+o1+1)>>1) );
    }
    dst += dst_dy;
    src0 += src_dy;
    src1 += src_dy;
  }
}

#define WIMP(width, height, logWDNonNull)                                                     \
static void wimp_##width##x##height##_##logWDNonNull                                          \
  (const pixel_t src[], unsigned int src_dy, pixel_t dst[], unsigned int dst_dy, int clip,    \
   int o, int w, int logWD)                                                                   \
{                                                                                             \
    weighted_interpolation_monopred(width, height, src, src_dy, dst, dst_dy, clip, o, w,      \
        logWD * logWDNonNull);                                                                \
}

#define WIBP(width, height)                                                                   \
static void wibp_##width##x##height                                                           \
  (const pixel_t src0[], const pixel_t src1[], unsigned int src_dy, pixel_t dst[],            \
   unsigned int dst_dy, int clip, int o0, int w0, int o1, int w1, int logWD)                  \
{                                                                                             \
  weighted_interpolation_bipred(width, height, src0, src1, src_dy, dst, dst_dy, clip,         \
      o0, w0, o1, w1, logWD);                                                                 \
}

#define WIBPI(width, height)                                                                  \
/*static*/ void wibpi_##width##x##height                                                          \
  (const pixel_t src0[], const pixel_t src1[], unsigned int src_dy, pixel_t dst[],            \
   unsigned int dst_dy, int clip, int w0, int w1)                                             \
{                                                                                             \
  weighted_interpolation_bipred(width, height, src0, src1, src_dy, dst, dst_dy, clip,         \
      0, w0, 0, w1, 5);                                                                       \
}

WIMP(2, 2, 0)
WIMP(2, 2, 1)
WIMP(2, 4, 0)
WIMP(2, 4, 1)
WIMP(4, 2, 0)
WIMP(4, 2, 1)
WIMP(4, 4, 0)
WIMP(4, 4, 1)
WIMP(4, 8, 0)
WIMP(4, 8, 1)
WIMP(8, 4, 0)
WIMP(8, 4, 1)
WIMP(8, 8, 0)
WIMP(8, 8, 1)
WIMP(8, 16, 0)
WIMP(8, 16, 1)
WIMP(16, 8, 0)
WIMP(16, 8, 1)
WIMP(16, 16, 0)
WIMP(16, 16, 1)
WIMP(2, 8, 0)
WIMP(2, 8, 1)
WIMP(4, 16, 0)
WIMP(4, 16, 1)
WIBP(2, 2)
WIBP(2, 4)
WIBP(4, 2)
WIBP(4, 4)
WIBP(4, 8)
WIBP(8, 4)
WIBP(8, 8)
WIBP(8, 16)
WIBP(16, 8)
WIBP(16, 16)
WIBP(2, 8)
WIBP(4, 16)
WIBPI(2, 2)
WIBPI(2, 4)
WIBPI(4, 2)
WIBPI(4, 4)
WIBPI(4, 8)
WIBPI(8, 4)
WIBPI(8, 8)
WIBPI(8, 16)
WIBPI(16, 8)
WIBPI(16, 16)
WIBPI(2, 8)
WIBPI(4, 16)

const wimp_func wimp[12][2] =
{
    {wimp_2x2_0, wimp_2x2_1},
    {wimp_2x4_0, wimp_2x4_1},
    {wimp_4x2_0, wimp_4x2_1},
    {wimp_4x4_0, wimp_4x4_1},
    {wimp_4x8_0, wimp_4x8_1},
    {wimp_8x4_0, wimp_8x4_1},
    {wimp_8x8_0, wimp_8x8_1},
    {wimp_8x16_0, wimp_8x16_1},
    {wimp_16x8_0, wimp_16x8_1},
    {wimp_16x16_0, wimp_16x16_1},
    {wimp_2x8_0, wimp_2x8_1},
    {wimp_4x16_0, wimp_4x16_1}
};

const wibp_func wibp[12] =
{
    wibp_2x2,
    wibp_2x4,
    wibp_4x2,
    wibp_4x4,
    wibp_4x8,
    wibp_8x4,
    wibp_8x8,
    wibp_8x16,
    wibp_16x8,
    wibp_16x16,
    wibp_2x8,
    wibp_4x16
};

extern const void wibpi_4x4_sse(const pixel_t src0[], const pixel_t src1[], unsigned int src_dy, pixel_t dst[],
unsigned int dst_dy, int clip, int w0, int w1);
const wibpi_func wibpi[12] =
{
    wibpi_2x2,
    wibpi_2x4,
    wibpi_4x2,
    wibpi_4x4,
    //wibpi_4x4_sse,
    wibpi_4x8,
    wibpi_8x4,
    wibpi_8x8,
    wibpi_8x16,
    wibpi_16x8,
    wibpi_16x16,
    wibpi_2x8,
    wibpi_4x16
};



void default_weighted_sample_prediction(int width, int height,
    const pixel_t* src0, const pixel_t* src1, unsigned int src_dy, pixel_t* dst, unsigned int dst_dy)
{
  int r, c;

  for (r = 0; r < height; r++)
  {
    for (c = 0; c < width; c++)
    {
      dst[c] = (src0[c] + src1[c] + 1) >> 1;
    }
    dst += dst_dy;
    src0 += src_dy;
    src1 += src_dy;
  }
}

#define DWSP(width, height)                                                                 \
static void dwsp_##width##x##height                                                         \
  (const pixel_t src0[], const pixel_t src1[], unsigned int src_dy, pixel_t dst[],          \
   unsigned int dst_dy)                                                                     \
{                                                                                           \
  default_weighted_sample_prediction(width, height, src0, src1, src_dy, dst, dst_dy);       \
}

DWSP(2, 2)
DWSP(2, 4)
DWSP(4, 2)
DWSP(4, 4)
DWSP(4, 8)
DWSP(8, 4)
DWSP(8, 8)
DWSP(8, 16)
DWSP(16, 8)
DWSP(16, 16)
DWSP(2, 8)
DWSP(4, 16)

const dwsp_func dwsp[12] =
{
    dwsp_2x2,
    dwsp_2x4,
    dwsp_4x2,
    dwsp_4x4,
    dwsp_4x8,
    dwsp_8x4,
    dwsp_8x8,
    dwsp_8x16,
    dwsp_16x8,
    dwsp_16x16,
    dwsp_2x8,
    dwsp_4x16
};
