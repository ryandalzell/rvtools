/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/



#include <string.h>

#include "common.h"
#include "decode.h"
#include "defaulttables.h"
#include "intmath.h"
#include "decode_slice_data.h"
#include "system.h"
#include "inverse_transforms.h"
#include "motion_vector.h"

#include "decode_image_intra.h"
#include "decode_image_inter.h"



void print_16x16_data_block(int16_t* data, int dy, char* s)
{
  int i, j;
  int16_t* row = data+dy;

  printf("16x16 block: %s\n", s);
  for (j=0; j< 16; j++)
  {
    for (i=0; i< 16; i++)
    {
      printf("%3d ", *data++);
    }
    data = row;
    row+=dy;
    printf("\n");
  }
}
void print_8x8_data_block(int16_t* data, int dy, char* s)
{
  int i, j;
  int16_t* row = data+dy;

  printf("8x8 block: %s\n", s);
  for (j=0; j< 8; j++)
  {
    for (i=0; i< 8; i++)
    {
      printf("%3d ", *data++);
    }
    data = row;
    row+=dy;
    printf("\n");
  }
}

void print_4x4_data_block(int16_t* data, int dy, char* s)
{
  int i, j;
  int16_t* row = data+dy;

  printf("4x4 block: %s\n", s);
  for (j=0; j< 4; j++)
  {
    for (i=0; i< 4; i++)
    {
      printf("%3d ", *data++);
    }
    data = row;
    row+=dy;
    printf("\n");
  }
}


static void decode_mb(struct rv264macro* curr_mb_attr, int16_t* mb_data, unsigned int mb_row, unsigned int mb_col, unsigned int mb_idx,
                             int PicWidthInMbs, unsigned int PicWidthY, unsigned int PicWidthC, unsigned int PicHeightY, unsigned int PicHeightC,
                             pixel_t* mb_Y_samples, pixel_t* mb_C_samples_[2], unsigned int mbc_height, unsigned int mbc_width,
                             unsigned int MbaffFrameFlag, unsigned int cfidc,
                             int clipY, int clipC, int meanY, int meanC,
                             unsigned int strideY, unsigned int strideC,
                             RefListEntry* RefPicList[2], int32_t FieldOrderCnt[2], int32_t PicOrderCnt,
                             unsigned int direct_spatial_mv_pred_flag, unsigned int direct_8x8_inference_flag, unsigned int field_pic_flag,
                             unsigned int bottom_field_flag,
                             slice_type_t slice_type_modulo5, unsigned int weighted_pred_flag, unsigned int weighted_bipred_idc,
                             pred_weight_table_t* wt, unsigned int constrained_intra_pred_flag)
{
  mb_type_t mb_type = curr_mb_attr->mb_type;
  unsigned int mb_field_decoding_flag = curr_mb_attr->mb_field_decoding_flag;
  pixel_t* mb_C_samples[2];

  mb_C_samples[0] = mb_C_samples_[0];
  mb_C_samples[1] = mb_C_samples_[1];

  if (MbaffFrameFlag && mb_field_decoding_flag)
  {
    if (mb_row & 0x1) // bottom MB
    {
      mb_Y_samples -= PicWidthY * 15;
      mb_C_samples[0] -= PicWidthC * (mbc_height-1);
      mb_C_samples[1] -= PicWidthC * (mbc_height-1);
    }
    strideY <<= 1;
    strideC <<= 1;
  }

  if (mb_type <= SI) // Intra mb
    decode_intra_mb(curr_mb_attr, mb_type, mb_data, strideY, strideC, mb_Y_samples, mb_C_samples[0], mb_C_samples[1],
        mbc_height, mbc_width, cfidc, clipY, clipC, meanY, meanC, mb_field_decoding_flag, mb_row&1, PicWidthInMbs, MbaffFrameFlag,
        constrained_intra_pred_flag);

  else  // Inter MB
  {
    derive_motion_vectors_internal(curr_mb_attr, mb_type, mb_idx, mb_row,
        mb_col, PicWidthInMbs, mb_field_decoding_flag, MbaffFrameFlag,
        direct_spatial_mv_pred_flag, direct_8x8_inference_flag,
        RefPicList, field_pic_flag, bottom_field_flag,
        FieldOrderCnt, PicOrderCnt);

    decode_inter_mb(curr_mb_attr, mb_type, mb_data, mb_row, mb_col, mb_field_decoding_flag, MbaffFrameFlag,
        RefPicList, field_pic_flag, bottom_field_flag, FieldOrderCnt,
        slice_type_modulo5, weighted_pred_flag, weighted_bipred_idc,
        mb_Y_samples, mb_C_samples, PicWidthY, PicWidthC,
        strideY, strideC, PicHeightY, PicHeightC, clipY, clipC,
        cfidc, mbc_width, mbc_height, wt);
  }
}

void print_mb_samples(struct rv264macro* curr_mb_attr, unsigned int mb_col, unsigned int mb_row,
    pixel_t* Y_samples, pixel_t* Cb_samples, pixel_t* Cr_samples, int PicWidthY, int PicWidthC,
    unsigned int MbaffFrameFlag, unsigned int mb_field_decoding_flag, unsigned int cfidc)
{
  unsigned int i, j;
  pixel_t* row;
  pixel_t* Y;
  pixel_t* Cb;
  pixel_t* Cr;
  unsigned int mbc_height = MbHeightC[cfidc];
  unsigned int mbc_width = MbWidthC[cfidc];
  if (MbaffFrameFlag && mb_field_decoding_flag)
  {
    if (mb_row & 0x1) // bottom MB
    {
      Y_samples -= PicWidthY * 15;
      Cb_samples -= PicWidthC * (mbc_height-1);
      Cr_samples -= PicWidthC * (mbc_height-1);
    }
    PicWidthY <<= 1;
    PicWidthC <<= 1;

  }
  row = Y_samples;
  printf("MB (%d,%d) - mb_type:%d\n", mb_col, mb_row, curr_mb_attr->mb_type);
  printf("Luma\n");
  for (j=0; j<16; j++)
  {
    Y = row;
    for (i=0; i<16; i++)
    {
      printf("%3d ", *Y++);
    }
    printf("\n");
    row += PicWidthY;
  }
  row = Cb_samples;
  printf("Chroma Cb\n");
  for (j=0; j<mbc_height; j++)
  {
    Cb = row;
    for (i=0; i<mbc_width; i++)
    {
      printf("%3d ", *Cb++);
    }
    printf("\n");
    row += PicWidthC;
  }
  row = Cr_samples;
  printf("Chroma Cr\n");
  for (j=0; j<mbc_height; j++)
  {
    Cr = row;
    for (i=0; i<mbc_width; i++)
    {
      printf("%3d ", *Cr++);
    }
    printf("\n");
    row += PicWidthC;
  }


}
void print_mb_data(PictureDecoderData* pdd, struct rv264macro* curr_mb_attr, unsigned int mb_col, unsigned int mb_row, unsigned int mb_idx, unsigned int cfidc)
{
  int i, j;
  PictureData* p = &pdd->pic->field_data[pdd->prev_sh->bottom_field_flag];
  int16_t* data = p->data + mb_idx * (MbSizeC[cfidc]*2+256);

  printf("MB (%d,%d) - mb_type:%d\n", mb_col, mb_row, curr_mb_attr->mb_type);
  printf("Luma data\n");
  for (j=0; j<16; j++)
  {
    for (i=0; i<16; i++)
    {
      printf("%3d ", *data++);
    }
    printf("\n");
  }
  if (cfidc > 0)
  {
    printf("Chroma Cb data\n");
    for (j=0; j<MbHeightC[cfidc]; j++)
    {
      for (i=0; i<MbWidthC[cfidc]; i++)
      {
        printf("%3d ", *data++);
      }
      printf("\n");
    }
    printf("Chroma Cr data\n");
    for (j=0; j<MbHeightC[cfidc]; j++)
    {
      for (i=0; i<MbWidthC[cfidc]; i++)
      {
        printf("%3d ", *data++);
      }
      printf("\n");
    }
  }
}


#define CALC_MB_COORDS(currmb)                                        \
if (MbaffFrameFlag)                                                   \
{                                                                     \
  int top = (currmb)%2;                                               \
  int idx = (currmb)/2;                                               \
  mb_row = (idx / PicWidthInMbs)*2 + top;                             \
  mb_col = idx % PicWidthInMbs;                                       \
  mb_idx = mb_row * PicWidthInMbs + mb_col;                           \
  curr_mb_attr = &mb_attr[mb_idx];                                    \
  curr_mb_data = mb_data + mb_idx * mb_data_size;                     \
  y = Y + mb_col*16 + mb_row*strideY*16;                              \
  c[0] = C[0] + mb_col*mbc_width + mb_row*strideC*mbc_height;         \
  c[1] = C[1] + mb_col*mbc_width + mb_row*strideC*mbc_height;         \
}                                                                     \
else                                                                  \
{                                                                     \
  mb_row = (currmb) / PicWidthInMbs;                                  \
  mb_col = (currmb) % PicWidthInMbs;                                  \
  mb_idx = (currmb);                                                  \
  curr_mb_attr = &mb_attr[mb_idx];                                    \
  curr_mb_data = mb_data + mb_idx * mb_data_size;                     \
  y = Y + mb_col*16 + mb_row*strideY*16;                              \
  c[0] = C[0] + mb_col*mbc_width + mb_row*strideC*mbc_height;         \
  c[1] = C[1] + mb_col*mbc_width + mb_row*strideC*mbc_height;         \
}


static void decode_image_internal(struct rv264sequence *seq, struct rv264picture* pic, unsigned int bottom_field_flag, unsigned int cfidc, unsigned int BitDepthY, unsigned int BitDepthC, unsigned int MbaffFrameFlag)
{
  unsigned int mb_row, mb_col, mb_idx, i, j;

  struct slice_header* slice0 = pic->field_sh[bottom_field_flag][0]; // get a slice header. It is used for variables that are the same for the whole picture
  struct rv264seq_parameter_set* sps = slice0->sps;
  struct rv264pic_parameter_set* pps = slice0->pps;
  int PicWidthInMbs = sps->PicWidthInMbs;
  unsigned int PicWidthY = PicWidthInMbs * 16;
  unsigned int PicHeightInMbs = slice0->PicHeightInMbs;
  unsigned int PicSizeInMbs = PicWidthInMbs*PicHeightInMbs;
  int CurrMbAddr;
  struct rv264macro* mb_attr = pic->field_data[bottom_field_flag].mb_attr;
  struct rv264macro* curr_mb_attr;
  unsigned int mbc_width = MbWidthC[cfidc];
  unsigned int mbc_height = MbHeightC[cfidc];
//  unsigned int mbc_size = mbc_width*mbc_height;
  unsigned int PicWidthC = PicWidthInMbs * mbc_width;
  int clipY = (1<<sps->BitDepthY)-1;
  int meanY = 1<<(sps->BitDepthY-1);
  int clipC = (1<<sps->BitDepthC)-1;
  int meanC = 1<<(sps->BitDepthC-1);
  int mb_data_size = (256+2*MbSizeC[cfidc]);
  int16_t* mb_data = pic->field_data[bottom_field_flag].data;
  int16_t* curr_mb_data;
  int32_t* FieldOrderCnt = pic->FieldOrderCnt;
  int32_t PicOrderCnt = pic->PicOrderCnt;
  unsigned int direct_8x8_inference_flag = sps->direct_8x8_inference_flag;
  unsigned int field_pic_flag = slice0->field_pic_flag;
  unsigned int weighted_pred_flag = pps->weighted_pred_flag;
  unsigned int weighted_bipred_idc = pps->weighted_bipred_idc;
  unsigned int strideY = PicWidthY << field_pic_flag;
  unsigned int strideC = PicWidthC << field_pic_flag;
  struct slice_header* sh;
  unsigned int constrained_intra_pred_flag = pps->constrained_intra_pred_flag;

  pixel_t* Y;
  pixel_t* C[2];
  pixel_t* y;
  pixel_t* c[2];


  Y = pic->Y + (bottom_field_flag!=0)*PicWidthY;
  C[0] = pic->C[0]+ (bottom_field_flag!=0)*PicWidthC;
  C[1] = pic->C[1]+ (bottom_field_flag!=0)*PicWidthC;

  for (j = 0; j<=pic->slice_num[bottom_field_flag]; j++)
  {
    unsigned int PicHeightY;
    unsigned int PicHeightC = 0; // prevents compil warning
    unsigned int direct_spatial_mv_pred_flag;

    sh = pic->field_sh[bottom_field_flag][j];
    direct_spatial_mv_pred_flag = sh->direct_spatial_mv_pred_flag; // Only necessary for B Slices !! Indeed The field is not defined (nor used) in other slices
    CurrMbAddr = sh->first_mb_in_slice * (1 + MbaffFrameFlag);
    PicHeightY = PicHeightInMbs * 16;
    if (cfidc)
      PicHeightC = PicHeightY / SubHeightC[cfidc];
    for (i = 0; i<sh->mb_nb; i++)
    {
      CALC_MB_COORDS(CurrMbAddr); // TODO: if that macro takes too much cpu, find something else in order to calculate the coords (i.e. calculate the offsets instead of abs values)

      decode_mb(curr_mb_attr, curr_mb_data, mb_row, mb_col, mb_idx, PicWidthInMbs, PicWidthY, PicWidthC, PicHeightY, PicHeightC,
          y, c, mbc_height, mbc_width, MbaffFrameFlag, cfidc, clipY, clipC, meanY, meanC, strideY, strideC,
          sh->RefPicList, FieldOrderCnt, PicOrderCnt, direct_spatial_mv_pred_flag, direct_8x8_inference_flag, field_pic_flag,
          bottom_field_flag, sh->slice_type_modulo5, weighted_pred_flag, weighted_bipred_idc,
          sh->pred_weight_table, constrained_intra_pred_flag);

      CurrMbAddr = NextMbAddress(sh->MbToSliceGroupMap, CurrMbAddr, PicSizeInMbs, pps->num_slice_groups_minus1);
    }
  }

  // Release the ref picture it was using
  release_picture_refpics(pic, bottom_field_flag); // it is OK even when the pic is a frame because both field points to the same lists

  filter_image(pic, bottom_field_flag);

  // Output the picture !
  add_image_to_dpb(&seq->dpb, pic);
}

void decode_image(struct rv264sequence *seq, struct rv264picture* pic, unsigned int bottom_field_flag)
{
  struct slice_header* slice0 = pic->field_sh[bottom_field_flag][0]; // get a slice header. It is used for variables that are the same for the whole picture
  struct rv264seq_parameter_set* sps = slice0->sps;
  //struct rv264pic_parameter_set* pps = slice0->pps;
  unsigned int cfidc = sps->chroma_format_idc;
  unsigned int BitDepthY = sps->BitDepthY;
  unsigned int BitDepthC = sps->BitDepthC;
  unsigned int MbaffFrameFlag = slice0->MbaffFrameFlag;

  unsigned int j;
  for (j = 0; j<=pic->slice_num[bottom_field_flag]; j++)
  {
    struct slice_header* sh = pic->field_sh[bottom_field_flag][j];
    decode_slice_data(sh);
  }
  if (sps->residual_colour_transform_flag != 0) // not implemented yet
    rvexit("residual_colour_transform_flag not implemented yet");
  if (sps->BitDepthY != 8) // Not tested yet
    rvexit("BitDepthY greater than 8 not tested yet");
  if (sps->BitDepthC != 8) // Not tested yet
    rvexit("BitDepthY greater than 8 not tested yet");

  if (MbaffFrameFlag == 0 && cfidc == 1 && BitDepthY == 8 && BitDepthC == 8)
      decode_image_internal(seq, pic, bottom_field_flag, 1, 8, 8, 0);
  else
      decode_image_internal(seq, pic, bottom_field_flag, cfidc, BitDepthY, BitDepthC, MbaffFrameFlag);
}



// No specific initialization to be done for mono threaded decoding
void decode_image_init()
{

}

void decode_image_destroy()
{

}
void decode_image_end()
{

}


