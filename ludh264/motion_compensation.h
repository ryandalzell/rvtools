/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/


#ifndef MOTION_COMPENSATION_H_
#define MOTION_COMPENSATION_H_

#include "common.h"
#include "decode.h"
#include "motion_compensation_types.h"

void prefetch_unsafe_borders(const pixel_t* src, unsigned int src_dy, pixel_t* dst, unsigned int dst_dy,
    int partWidth, int partHeight, int x, int y, int picWidth, int picHeight);
void luma_samples_interpolation_process(int x, int y, int mvx, int mvy, int partWidth4, int partHeight4,
    const pixel_t* src, unsigned int src_dy, pixel_t* dst, unsigned int dst_dy,
    unsigned int picWidth, unsigned int picHeight, unsigned int clipmax);
void chroma_samples_interpolation_process(int x, int y, int mvx, int mvy, unsigned int partWidth4, unsigned int partHeight4,
    const pixel_t* src[2], unsigned int src_dy, pixel_t* dst[2], unsigned int dst_dy, unsigned int picWidth, unsigned int picHeight, unsigned int cfidc);
void default_weighted_sample_prediction_process(int partSize,
    const pixel_t* src0, const pixel_t* src1, unsigned int src_dy, pixel_t* dst, unsigned int dst_dy);
void explicit_weigthed_sample_prediction_process_monopred(int partSize,
    const pixel_t* src, unsigned int src_dy, pixel_t* dst, unsigned int dst_dy, unsigned int clip,
    unsigned int log2_weight_denom,
    int refIdxLXWP, int8_t* weight_lX, int16_t* offset_lX);
void explicit_weigthed_sample_prediction_process_bipred(int partSize,
    const pixel_t* src0, const pixel_t* src1, unsigned int src_dy, pixel_t* dst, unsigned int dst_dy, unsigned int clip,
    unsigned int log2_weight_denom, int refIdxL0WP, int refIdxL1WP,
    int8_t* weight_l0, int8_t* weight_l1, int16_t* offset_l0, int16_t* offset_l1);
void implicit_weigthed_sample_prediction_process_compute_wx(
    unsigned int mb_field_decoding_flag, unsigned int bottom_field_flag, unsigned int field_pic_flag, unsigned int mb_row,
    RefListEntry* RefPicList[2], int FieldOrderCnt[2], int refIdxL0, int refIdxL1, int* w0_, int* w1_);
void implicit_weigthed_sample_prediction_process_apply(int partSize,
    const pixel_t* src0, const pixel_t* src1, unsigned int src_dy, pixel_t* dst, unsigned int dst_dy,
    unsigned int clip, int w0, int w1);

#endif
