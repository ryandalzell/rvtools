/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/



#include <string.h>

#include "system.h"
#include "common.h"
#include "syntax.h"
#include "bitstream.h"
#include "defaulttables.h"
#include "trace.h"

#define Extended_SAR      255

int more_rbsp_data_lud(nal_unit_t* nalu, struct bitbuf *bs)
{
  return nalu->data_length_in_bits > numbits(bs);
}

void release_nalu(nal_unit_t* nalu)
{
  if (nalu)
  {
    if (nalu->ref_count == 0) {
      if (nalu->bs)
        freebits(nalu->bs);
      rvfree(nalu);
    }
    else
      nalu->ref_count--;
  }
}

// The buffer must not contain start codes
// The data are copied into a fresh buffer, the original data are not altered
RetCode parse_nal_unit(const uint8_t* buffer, uint32_t length, nal_unit_t** nalu_)
{
  int bufsize = length+8; // +8 because some function may read more ahead: prevent uninitialized reads...
  nal_unit_t *nalu = (nal_unit_t *)rvalloc(NULL, sizeof(nal_unit_t) + bufsize, 1);
  if (!nalu) {
    release_nalu(nalu);
    return LUDH264_ERR_MEMORY_ALLOC_FAILED;
  }

  OBJECT_HEADER_INST(nalu);

  const uint8_t* src = buffer;
  uint8_t* dst = (uint8_t*) (nalu+1);


  unsigned int i = 0; // src index
  int j = 0; // dst index



  // Remove emulation 3bytes and go until the end of the buffer
  while (i < length-1)
  {
    if(src[i])
    {
      *(uint16_t*)(dst+j) = *(uint16_t*)(src+i); // Can be unaligned 16 bits copy !
      i += 2; j += 2;
      continue;
    }
    if(src[i-1] == 0) {i--;j--;}
    if(src[i+1] == 0 && (i+3 < length) && src[i+2] == 3 && src[i+3] <= 3) //0x000003 detected - this is "emulation_prevention_three_byte". see H264 spec
    {
      *(uint16_t*)(dst+j) = 0; // Can be unaligned 16 bits copy !
      j += 2; i += 3;
    }
    else //false detection
    {
      *(uint16_t*)(dst+j) = *(uint16_t*)(src+i); // Can be unaligned 16 bits copy !
      i += 2; j += 2;
    }
  }
  LUD_DEBUG_ASSERT(i>=length-1);
  if (i<length)
  {
    dst[j] = src[i];  // add the possible remaining byte from the input buffer
    j++;
  }

  // Set all 0 in the buffer remaining
  for (; j < bufsize; ++j)
    dst[j] = 0;

  nalu->rbsp_byte = dst;
  nalu->NumBytesInRBSP = length;


  // the NALU RBSP is cleaned from its start code and any 'emulation prevention bytes'

  // parse the nal unit header.
  uint8_t nal_unit_header = nalu->rbsp_byte[0];
  int forbidden_zero_bit = nal_unit_header>>7;                      // u(1)
  if (forbidden_zero_bit!=0)
    rvmessage("forbidden_zero_bit is not zero");
  nalu->nal_ref_idc = (nal_unit_header>>5) & 0x3;                   // u(2)
  nalu->nal_unit_type = (nal_unit_type_t)(nal_unit_header & 0x1f);  // u(5)

  // move past the nal unit header.
  nalu->rbsp_byte++;
  nalu->NumBytesInRBSP--;

  // Init Bitstream context for subsequent parsing functions...
  //init_bitstream(bs, nalu->rbsp_byte);
  nalu->bs = initbits_memory(nalu->rbsp_byte, nalu->NumBytesInRBSP);

  // Remove trailing bytes if any. This would include cabac_zero_word. (see parse_rbsp_slice_trailing_bits function)
  while(nalu->NumBytesInRBSP > 1 && !nalu->rbsp_byte[nalu->NumBytesInRBSP-1])
    nalu->NumBytesInRBSP--;

  // Count trailing bits (including stop bit)
  {
    int r = 0;
    if (nalu->NumBytesInRBSP)
    {
      int v = nalu->rbsp_byte[nalu->NumBytesInRBSP-1];
      for(r=1; r<9; r++)
      {
        if(v&1) break;
        v>>=1;
      }
    }
    nalu->trailing_bits = (uint8_t)r;
  }

  nalu->data_length_in_bits = nalu->NumBytesInRBSP * 8 - nalu->trailing_bits;

  LUD_TRACE(TRACE_INFO, "nal unit: type=%s length=%d NumBytesInRBSP=%d", nal_unit_type_name[nalu->nal_unit_type], length, nalu->NumBytesInRBSP);

  *nalu_ = nalu;
  return LUDH264_SUCCESS;
}

static RetCode parse_hrd_parameters(struct bitbuf* bs, struct hrd_parameters* hrd)
{
  int SchedSelIdx;
  uint8_t cpb_cnt_minus1;

  // The 1st parameter is necessary in order to calculate the size of the structure
  cpb_cnt_minus1 = uexpbits(bs); // 0 ue(v)

  hrd->cpb_cnt_minus1 = cpb_cnt_minus1;
  hrd->bit_rate_scale = getbits(bs, 4); // 0 u(4)
  hrd->cpb_size_scale = getbits(bs, 4); // 0 u(4)

  for(SchedSelIdx = 0; SchedSelIdx <= hrd->cpb_cnt_minus1; SchedSelIdx++ )
  {
    hrd->bit_rate_value_minus1[ SchedSelIdx ] = uexpbits(bs); // 0 ue(v)
    hrd->cpb_size_value_minus1[ SchedSelIdx ] = uexpbits(bs); // 0 ue(v)
    hrd->cbr_flag[ SchedSelIdx ] = getbit(bs); // 0 u(1)
  }
  hrd->initial_cpb_removal_delay_length_minus1 = getbits(bs, 5); // 0 u(5)
  hrd->cpb_removal_delay_length_minus1 = getbits(bs, 5); // 0 u(5)
  hrd->dpb_output_delay_length_minus1 = getbits(bs, 5); // 0 u(5)
  hrd->time_offset_length = getbits(bs, 5); // 0 u(5)

  return LUDH264_SUCCESS;
}

static RetCode parse_vui_parameters(struct bitbuf* bs, struct vui_parameters* vui)
{
  RetCode r;

  // Initialize default values and pointers
  vui->aspect_ratio_idc = 0;
  vui->video_format = 5;
  vui->video_full_range_flag = 0;
  vui->colour_primaries = 2;
  vui->transfer_characteristics = 2;
  vui->matrix_coefficients = 2;
  vui->chroma_sample_loc_type_top_field = 0;
  vui->chroma_sample_loc_type_bottom_field = 0;
  vui->motion_vectors_over_pic_boundaries_flag = 1;
  vui->max_bytes_per_pic_denom = 2;
  vui->max_bits_per_mb_denom = 1;
  vui->log2_max_mv_length_horizontal = 16;
  vui->log2_max_mv_length_vertical = 16;
  vui->max_dec_frame_buffering = 16; //MaxDpbSize TODO: need to calculate MaxDpbSize. 16 is the max but not the good value. See ITU-T Rec. H.264 (03/2005) page 323.
  vui->num_reorder_frames = vui->max_dec_frame_buffering;

  vui->aspect_ratio_info_present_flag = getbit(bs); // 0    u(1)
  if( vui->aspect_ratio_info_present_flag )
  {
    vui->aspect_ratio_idc = getbits(bs, 8); // 0    u(8)
    if( vui->aspect_ratio_idc == Extended_SAR )
    {
      vui->sar_width = getbits(bs, 16); // 0    u(16)
      vui->sar_height = getbits(bs, 16); // 0    u(16)
    }
  }
  vui->overscan_info_present_flag = getbit(bs); // 0    u(1)
  if( vui->overscan_info_present_flag )
    vui->overscan_appropriate_flag = getbit(bs); // 0    u(1)
  vui->video_signal_type_present_flag = getbit(bs); // 0    u(1)
  if( vui->video_signal_type_present_flag )
  {
    vui->video_format = getbits(bs, 3); // 0    u(3)
    vui->video_full_range_flag = getbit(bs); // 0    u(1)
    vui->colour_description_present_flag = getbit(bs); // 0    u(1)
    if( vui->colour_description_present_flag )
    {
      vui->colour_primaries = getbits(bs, 8); // 0    u(8)
      vui->transfer_characteristics = getbits(bs, 8); // 0    u(8)
      vui->matrix_coefficients = getbits(bs, 8); // 0    u(8)
    }
  }
  vui->chroma_loc_info_present_flag = getbit(bs); // 0    u(1)
  if( vui->chroma_loc_info_present_flag )
  {
    vui->chroma_sample_loc_type_top_field = uexpbits(bs); // 0    ue(v)
    vui->chroma_sample_loc_type_bottom_field = uexpbits(bs); // 0  ue(v)
  }
  vui->timing_info_present_flag = getbit(bs); // 0  u(1)
  if( vui->timing_info_present_flag )
  {
    vui->num_units_in_tick = getbits32(bs); // 0  u(32)
    vui->time_scale = getbits32(bs); // 0  u(32)
    vui->fixed_frame_rate_flag = getbit(bs); // 0  u(1)
  }
  vui->nal_hrd_parameters_present_flag = getbit(bs); // 0  u(1)
  if( vui->nal_hrd_parameters_present_flag )
  {
    if (LUDH264_SUCCESS != (r=parse_hrd_parameters(bs, &vui->nal_hrd_parameters)))
      return r;
  }
  vui->vcl_hrd_parameters_present_flag = getbit(bs); // 0  u(1)
  if( vui->vcl_hrd_parameters_present_flag )
  {
    if (LUDH264_SUCCESS != (r=parse_hrd_parameters(bs, &vui->vcl_hrd_parameters)))
      return r;
  }
  if( vui->nal_hrd_parameters_present_flag || vui->vcl_hrd_parameters_present_flag )
    vui->low_delay_hrd_flag = getbit(bs); // 0  u(1)
  vui->pic_struct_present_flag = getbit(bs); // 0  u(1)
  vui->bitstream_restriction_flag = getbit(bs); // 0  u(1)
  if( vui->bitstream_restriction_flag )
  {
    vui->motion_vectors_over_pic_boundaries_flag = getbit(bs); // 0  u(1)
    vui->max_bytes_per_pic_denom = uexpbits(bs); // 0  ue(v)
    vui->max_bits_per_mb_denom = uexpbits(bs); // 0  ue(v)
    vui->log2_max_mv_length_horizontal = uexpbits(bs); // 0  ue(v)
    vui->log2_max_mv_length_vertical = uexpbits(bs); // 0  ue(v)
    vui->num_reorder_frames = uexpbits(bs); // 0  ue(v)
    vui->max_dec_frame_buffering = uexpbits(bs); // 0  ue(v)
   }

  return LUDH264_SUCCESS;
}

// This is very useful for error checking the syntax parsing.
RetCode parse_rbsp_trailing_bits_lud(nal_unit_t* nalu, struct bitbuf* bs, unsigned int entropy_coding_mode_flag)
{
  if (entropy_coding_mode_flag) // when doing CABAC decoding the rbsp_stop_one_bit is the last bit read before decoding end_of_slice_flag
    unflushbits(bs, 1);                // rewind 1 bit to check that it was indeed equal to 1 (cf H264(03/2005) 9.3.3.2.4, p242)

  int rbsp_stop_one_bit = getbit(bs);
  if (rbsp_stop_one_bit!=1)
    rvmessage("rbsp_trailing_bits: rbsp_stop_one_bit is not one");

  if (numbits(bs) != nalu->data_length_in_bits+1)
  {
    rvmessage("trailing bits error: used bits: %zd, total bits: %d", numbits(bs), nalu->data_length_in_bits+1);
    int nb_rbsp_alignment_zero_bit = (nalu->NumBytesInRBSP*8 - numbits(bs)) % 8;
    int rbsp_alignment_zero_bits = nb_rbsp_alignment_zero_bit ? getbits(bs, nb_rbsp_alignment_zero_bit) : 0;
    if (rbsp_alignment_zero_bits!=0)
      rvmessage("rbsp_trailing_bits: rbsp_alignment_zero_bits is not zero");
  }
  else
  {
    int nb_rbsp_alignment_zero_bit = (nalu->NumBytesInRBSP*8 - numbits(bs)) % 8;
    int rbsp_alignment_zero_bits = nb_rbsp_alignment_zero_bit ? getbits(bs, nb_rbsp_alignment_zero_bit) : 0;
    if (rbsp_alignment_zero_bits!=0)
      rvmessage("rbsp_trailing_bits: rbsp_alignment_zero_bits is not zero");
  }
/*
  LUD_DEBUG_ASSERT(bs->index == nalu->data_length_in_bits+1); // number of rbsp bits + rbsp_stop_one_bit
  int nb_rbsp_alignment_zero_bit = nalu->NumBytesInRBSP*8 - bs->index;
  LUD_DEBUG_ASSERT(nb_rbsp_alignment_zero_bit>=0);

  int rbsp_alignment_zero_bit = getbits(bs, nb_rbsp_alignment_zero_bit);
  LUD_DEBUG_ASSERT(rbsp_alignment_zero_bit == 0);
*/

  return LUDH264_SUCCESS;
}

RetCode parse_rbsp_slice_trailing_bits(nal_unit_t* nalu, unsigned int entropy_coding_mode_flag)
{
  struct bitbuf* bs = nalu->bs;
  return parse_rbsp_trailing_bits_lud(nalu, bs, entropy_coding_mode_flag); // All
  // the cabac_zero_word have been removed when parsing the nal unit
  //if( entropy_coding_mode_flag )
  //  while( more_rbsp_trailing_data( ) )
  //    cabac_zero_word /* equal to 0x0000 */ All f(16)
}

static unsigned int parse_scaling_list(struct bitbuf* bs, uint8_t* scalingList, int sizeOfScalingList)
{
  int lastScale = 8;
  int nextScale = 8;
  int delta_scale;
  int j;
  unsigned int useDefaultScalingMatrixFlag = 0;
  for( j = 0; j < sizeOfScalingList; j++ )
  {
    if( nextScale != 0 )
    {
      delta_scale = sexpbits(bs); // 0|1 se(v)
      nextScale = ( lastScale + delta_scale + 256 ) % 256;
      useDefaultScalingMatrixFlag = ( j == 0 && nextScale == 0 );
    }
    scalingList[ j ] = ( nextScale == 0 ) ? lastScale : nextScale;
    lastScale = scalingList[ j ];
  }
  return useDefaultScalingMatrixFlag;
}





void release_sps(struct rv264seq_parameter_set* sps)
{
  if (sps)
    rvfree(sps);
}

RetCode parse_sps(nal_unit_t* nalu, struct rv264seq_parameter_set** sps_)
{
  RetCode r;

  int i;
  struct bitbuf* bs = nalu->bs;


  // Allocate struct rv264seq_parameter_set structure
  struct rv264seq_parameter_set *sps = (struct rv264seq_parameter_set *)rvalloc(NULL, sizeof(struct rv264seq_parameter_set), 0);
  if (!sps) {
    release_sps(sps);
    return LUDH264_ERR_MEMORY_ALLOC_FAILED;
  }

  *sps_ = sps;

  // Init dynamic allocation pointer and default values
  sps->occupied = 0;
  sps->chroma_format_idc = 1;
  sps->residual_colour_transform_flag = 0;
  sps->bit_depth_luma_minus8 = 0;
  sps->bit_depth_chroma_minus8 = 0;
  sps->qpprime_y_zero_transform_bypass_flag = 0;
  sps->seq_scaling_matrix_present_flag = 0;
  sps->mb_adaptive_frame_field_flag = 0;
  sps->frame_crop_left_offset = 0;
  sps->frame_crop_right_offset = 0;
  sps->frame_crop_top_offset = 0;
  sps->frame_crop_bottom_offset = 0;
  sps->delta_pic_order_always_zero_flag = 0;

  // Parse the SPS
  sps->profile_idc = getbits(bs, 8);    // 0 u(8)
  sps->constraint_set0_flag = getbit(bs);    // 0 u(1)
  sps->constraint_set1_flag = getbit(bs);    // 0 u(1)
  sps->constraint_set2_flag = getbit(bs);    // 0 u(1)
  sps->constraint_set3_flag = getbit(bs);    // 0 u(1)
  int reserved_zero_4bits = getbits(bs, 4);    // /* equal to 0 */ 0 u(4)
  if (reserved_zero_4bits!=0)
    rvmessage("reserved_zero_4bits is not zero");
  sps->level_idc = getbits(bs, 8);    // 0 u(8)
  sps->seq_parameter_set_id = uexpbits(bs);    // 0 ue(v)
  if (sps->seq_parameter_set_id>31) {
    release_sps(sps);
    return LUDH264_ERR_PARSED_VALUE_OUT_OF_RANGE;
  }

  if (sps->profile_idc == 100 || sps->profile_idc == 110 ||
      sps->profile_idc == 122 || sps->profile_idc == 144)
  {
    sps->chroma_format_idc = uexpbits(bs);    // 0 ue(v)
    if (sps->chroma_format_idc == 3)
      sps->residual_colour_transform_flag = getbit(bs);    // 0 u(1)
    sps->bit_depth_luma_minus8 = uexpbits(bs);    // 0 ue(v)
    sps->bit_depth_chroma_minus8 = uexpbits(bs);    // 0 ue(v)
    sps->qpprime_y_zero_transform_bypass_flag = getbit(bs);    // 0 u(1)
    sps->seq_scaling_matrix_present_flag = getbit(bs);    // 0 u(1)
    if (sps->seq_scaling_matrix_present_flag)
      for (i = 0; i < 8; i++)
      {
        sps->seq_scaling_list_present_flag[i] = getbit(bs);    //  0 u(1)
        if (sps->seq_scaling_list_present_flag[i])
        {
          if (i < 6)   // 4x4 lists
          {
            int UseDefaultScalingMatrix4x4Flag;
            UseDefaultScalingMatrix4x4Flag = parse_scaling_list(bs, sps->ScalingList4x4[i], 16);
            if (UseDefaultScalingMatrix4x4Flag)
            {
              memcpy(sps->ScalingList4x4[i], ScalingList_Default_4x4[(i+1)>>2], 16);
            }
          }
          else // 8x8 lists
          {
            int UseDefaultScalingMatrix8x8Flag;
            UseDefaultScalingMatrix8x8Flag = parse_scaling_list(bs, sps->ScalingList8x8[i-6], 64);
            if (UseDefaultScalingMatrix8x8Flag)
            {
              memcpy(sps->ScalingList8x8[i-6], ScalingList_Default_8x8[i-6], 64);
            }
          }
        }
        else // sps->seq_scaling_list_present_flag[i] is false
        {
          switch (i)   // implements table 7.2 Scaling list fall-back rule set A of the standard
          {
            case 0: memcpy(sps->ScalingList4x4[0], ScalingList_Default_4x4[0] , 16); break;
            case 1: memcpy(sps->ScalingList4x4[1], sps->ScalingList4x4[0]     , 16); break;
            case 2: memcpy(sps->ScalingList4x4[2], sps->ScalingList4x4[1]     , 16); break;
            case 3: memcpy(sps->ScalingList4x4[3], ScalingList_Default_4x4[1] , 16); break;
            case 4: memcpy(sps->ScalingList4x4[4], sps->ScalingList4x4[3]     , 16); break;
            case 5: memcpy(sps->ScalingList4x4[5], sps->ScalingList4x4[4]     , 16); break;
            case 6: memcpy(sps->ScalingList8x8[0], ScalingList_Default_8x8[0] , 64); break;
            case 7: memcpy(sps->ScalingList8x8[1], ScalingList_Default_8x8[1] , 64); break;
            default:
              LUD_DEBUG_ASSERT(0);    // cannot happen, i = 0..7
              break;
          }
        }
      }
  }
  sps->log2_max_frame_num_minus4 = uexpbits(bs);    // 0 ue(v)
  sps->pic_order_cnt_type = uexpbits(bs);    // 0 ue(v)
  if (sps->pic_order_cnt_type == 0)
    sps->log2_max_pic_order_cnt_lsb_minus4 = uexpbits(bs);    // 0 ue(v)
  else if (sps->pic_order_cnt_type == 1)
  {
    sps->delta_pic_order_always_zero_flag = getbit(bs);    // 0 u(1)
    sps->offset_for_non_ref_pic = sexpbits(bs);    // 0 se(v)
    sps->offset_for_top_to_bottom_field = sexpbits(bs);    // 0 se(v)
    sps->num_ref_frames_in_pic_order_cnt_cycle = uexpbits(bs);    // 0 ue(v)
    for (i = 0; i < sps->num_ref_frames_in_pic_order_cnt_cycle; i++)
      sps->offset_for_ref_frame[i] = sexpbits(bs);    // 0 se(v)
  }
  sps->max_num_ref_frames = uexpbits(bs);    // 0 ue(v)
  sps->gaps_in_frame_num_value_allowed_flag = getbit(bs);    // 0 u(1)
  sps->pic_width_in_mbs_minus1 = uexpbits(bs);    // 0 ue(v)
  sps->pic_height_in_map_units_minus1 = uexpbits(bs);    // 0 ue(v)
  sps->frame_mbs_only_flag = getbit(bs);    // 0 u(1)
  if (!sps->frame_mbs_only_flag)
    sps->mb_adaptive_frame_field_flag = getbit(bs);    // 0 u(1)
  sps->direct_8x8_inference_flag = getbit(bs);    // 0 u(1)
  sps->frame_cropping_flag = getbit(bs);    // 0 u(1)
  if (sps->frame_cropping_flag)
  {
    sps->frame_crop_left_offset = uexpbits(bs);    // 0 ue(v)
    sps->frame_crop_right_offset = uexpbits(bs);    // 0 ue(v)
    sps->frame_crop_top_offset = uexpbits(bs);    // 0 ue(v)
    sps->frame_crop_bottom_offset = uexpbits(bs);    // 0 ue(v)
  }
  sps->vui_parameters_present_flag = getbit(bs);    // 0 u(1)
  if (sps->vui_parameters_present_flag)
  {
    if (LUDH264_SUCCESS != (r = parse_vui_parameters(bs, &sps->vui))) {
      release_sps(sps);
      return r;
    }
  }

  LUD_DEBUG_ASSERT(parse_rbsp_trailing_bits_lud(nalu, bs, 0) == LUDH264_SUCCESS);

  // Default inferred variables
  if (!sps->seq_scaling_matrix_present_flag)
  {
    // set Flat_4x4_16 and Flat_8x8_16 into the scaling lists
    memset(sps->ScalingList4x4, 16, 6*16);
    memset(sps->ScalingList8x8, 16, 2*64);
  }

  sps->PicWidthInMbs = sps->pic_width_in_mbs_minus1 + 1;
  sps->PicHeightInMapUnits = sps->pic_height_in_map_units_minus1 + 1;
  sps->FrameHeightInMbs = sps->frame_mbs_only_flag ? sps->PicHeightInMapUnits : 2 * sps->PicHeightInMapUnits;
  sps->PicSizeInMapUnits = sps->PicWidthInMbs * sps->PicHeightInMapUnits;
  sps->BitDepthY = 8 + sps->bit_depth_luma_minus8;
  sps->QpBdOffsetY = 6 * sps->bit_depth_luma_minus8;
  sps->BitDepthC = 8 + sps->bit_depth_chroma_minus8;
  sps->QpBdOffsetC = 6 * ( sps->bit_depth_chroma_minus8 + sps->residual_colour_transform_flag );
  sps->MaxFrameNum = 1 << (sps->log2_max_frame_num_minus4+4);

  sps->occupied = 1;
  return LUDH264_SUCCESS;
}


void release_pps(struct rv264pic_parameter_set* pps)
{
  if (pps)
  {
    rvfree(pps->run_length_minus1);
    rvfree(pps->slice_group_id);
    rvfree(pps->top_left);
    rvfree(pps);
  }
}


RetCode parse_pps(struct rv264sequence *seq, nal_unit_t* nalu, struct rv264pic_parameter_set** pps_)
{
  int iGroup, i;
  struct bitbuf* bs = nalu->bs;


  // Allocate struct rv264pic_parameter_set structure
  struct rv264pic_parameter_set *pps = (struct rv264pic_parameter_set *)rvalloc(NULL, sizeof(struct rv264pic_parameter_set), 0);
  if (!pps) {
    release_pps(pps);
    return LUDH264_ERR_MEMORY_ALLOC_FAILED;
  }
  *pps_ = pps;

  // init pointers and default values
  pps->occupied = 0;
  pps->run_length_minus1 = 0;
  pps->top_left = 0;
  pps->bottom_right = 0;
  pps->slice_group_id = 0;
  pps->transform_8x8_mode_flag = 0;
  pps->pic_scaling_matrix_present_flag = 0;

  pps->pic_parameter_set_id = uexpbits(bs); // 1   ue(v)
  pps->seq_parameter_set_id = uexpbits(bs); // 1   ue(v)
  if (pps->seq_parameter_set_id>31) { //  || pps->pic_parameter_set_id>255 => always false since it is stored into a byte...
    release_pps(pps);
    return LUDH264_ERR_PARSED_VALUE_OUT_OF_RANGE;
  }
  struct rv264seq_parameter_set *sps = seq->sps[pps->seq_parameter_set_id];
  if (!sps) {
    release_pps(pps);
    rvmessage("ppd id %d: no sps id: %d", pps->pic_parameter_set_id, pps->seq_parameter_set_id);
    return LUDH264_ERR_REFERING_NON_EXISTING_SPS;
  }
  pps->entropy_coding_mode_flag = getbit(bs); // 1   u(1)
  pps->pic_order_present_flag = getbit(bs); // 1   u(1)
  pps->num_slice_groups_minus1 = uexpbits(bs); // 1   ue(v)
  LUD_DEBUG_ASSERT(pps->num_slice_groups_minus1 < 8);
  if( pps->num_slice_groups_minus1 > 0 )
  {
    pps->slice_group_map_type = uexpbits(bs); // 1   ue(v)
    if( pps->slice_group_map_type == 0 )
    {
      pps->run_length_minus1 = (uint16_t *)rvalloc(NULL, (pps->num_slice_groups_minus1+1)*sizeof(*pps->run_length_minus1), 0);
      if (!pps->run_length_minus1) {
        release_pps(pps);
        return LUDH264_ERR_MEMORY_ALLOC_FAILED;
      }
      for( iGroup = 0; iGroup <= pps->num_slice_groups_minus1; iGroup++ )
        pps->run_length_minus1[ iGroup ] = uexpbits(bs); // 1   ue(v)
    }
    else if( pps->slice_group_map_type == 2 )
    {
      unsigned int size = (pps->num_slice_groups_minus1)*(sizeof(*pps->top_left)+sizeof(*pps->bottom_right));
      pps->top_left = (uint16_t *)rvalloc(NULL, size, 0);
      if (!pps->top_left) {
        release_pps(pps);
        return LUDH264_ERR_MEMORY_ALLOC_FAILED;
      }
      pps->bottom_right = pps->top_left + pps->num_slice_groups_minus1;
      for( iGroup = 0; iGroup < pps->num_slice_groups_minus1; iGroup++ )
      {
        pps->top_left[ iGroup ] = uexpbits(bs); // 1   ue(v)
        pps->bottom_right[ iGroup ] = uexpbits(bs); // 1   ue(v)
      }
    }
    else if( pps->slice_group_map_type == 3 ||
             pps->slice_group_map_type == 4 ||
             pps->slice_group_map_type == 5 )
    {
      pps->slice_group_change_direction_flag = getbit(bs); // 1   u(1)
      pps->slice_group_change_rate_minus1 = uexpbits(bs); // 1   ue(v)
    }
    else if( pps->slice_group_map_type == 6 )
    {
      int len;
      pps->pic_size_in_map_units_minus1 = uexpbits(bs); // 1   ue(v)
      pps->slice_group_id = (uint8_t *)rvalloc(NULL, (pps->pic_size_in_map_units_minus1+1)*sizeof(*pps->slice_group_id), 0);
      if (!pps->slice_group_id) {
        release_pps(pps);
        return LUDH264_ERR_MEMORY_ALLOC_FAILED;
      }
      if (pps->num_slice_groups_minus1+1 >4)
        len = 3;
      else if (pps->num_slice_groups_minus1+1 > 2)
        len = 2;
      else
        len = 1;
      for( i = 0; i <= (int)pps->pic_size_in_map_units_minus1; i++ )
        pps->slice_group_id[i] = getbits(bs, len); // 1   u(v)
    }
  }
  pps->num_ref_idx_active[0] = uexpbits(bs)+1; // 1   ue(v)
  if (pps->num_ref_idx_active[0] > NB_OF_REF_PICS)
  {
    LUD_DEBUG_ASSERT(0); // num_ref_idx_active_minus1 overflow
    LUD_TRACE(TRACE_ERROR, "PPS num_ref_idx_active_minus1 L0 overflow, set to max number of ref minus 1");
    pps->num_ref_idx_active[0] = NB_OF_REF_PICS;
  }
  pps->num_ref_idx_active[1] = uexpbits(bs)+1; // 1   ue(v)
  if (pps->num_ref_idx_active[1] > NB_OF_REF_PICS)
  {
    LUD_DEBUG_ASSERT(0); // num_ref_idx_active_minus1 overflow
    LUD_TRACE(TRACE_ERROR, "PPS num_ref_idx_active_minus1 L0 overflow, set to max number of ref minus 1");
    pps->num_ref_idx_active[1] = NB_OF_REF_PICS;
  }
  pps->weighted_pred_flag = getbit(bs); // 1   u(1)
  pps->weighted_bipred_idc = getbits(bs, 2); // 1   u(2)
  pps->pic_init_qp_minus26 = sexpbits(bs); // 1   se(v)
  pps->pic_init_qs_minus26 = sexpbits(bs); // 1   se(v)
  pps->chroma_qp_index_offset = sexpbits(bs); // 1   se(v)
  pps->second_chroma_qp_index_offset = pps->chroma_qp_index_offset; // This is a fallback value in case it is not defined afterwards
  pps->deblocking_filter_control_present_flag = getbit(bs); // 1   u(1)
  pps->constrained_intra_pred_flag = getbit(bs); // 1   u(1)
  pps->redundant_pic_cnt_present_flag = getbit(bs); // 1   u(1)

  if( more_rbsp_data_lud(nalu, bs) )
  {
    pps->transform_8x8_mode_flag = getbit(bs); // 1   u(1)
    pps->pic_scaling_matrix_present_flag = getbit(bs); // 1   u(1)
    if( pps->pic_scaling_matrix_present_flag )
      for( i = 0; i < 6 + 2 * pps->transform_8x8_mode_flag; i++ )
      {
        pps->pic_scaling_list_present_flag[i] = getbit(bs); // 1   u(1)
        if( pps->pic_scaling_list_present_flag[i] )
        {
          if( i < 6 ) // 4x4 lists
          {
            unsigned int UseDefaultScalingMatrix4x4Flag;
            UseDefaultScalingMatrix4x4Flag = parse_scaling_list(bs, pps->ScalingList4x4[i], 16);
            if (UseDefaultScalingMatrix4x4Flag)
            {
              memcpy(pps->ScalingList4x4[i], ScalingList_Default_4x4[(i+1)>>2], 16);
            }
          }
          else  // 8x8 lists
          {
            unsigned int UseDefaultScalingMatrix8x8Flag;
            UseDefaultScalingMatrix8x8Flag = parse_scaling_list(bs, pps->ScalingList8x8[i-6], 64);
            if (UseDefaultScalingMatrix8x8Flag)
            {
              memcpy(pps->ScalingList8x8[i-6], ScalingList_Default_8x8[i-6], 64);
            }
          }
        }
        else // pps->UseDefaultScalingMatrix4x4Flag[i] is false
        {
          if(sps->seq_scaling_matrix_present_flag)
          {
            switch (i)   // implements table 7.2 Scaling list fall-back rule set B of the standard
            {
              case 0: memcpy(pps->ScalingList4x4[0], sps->ScalingList4x4[0]     , 16); break;
              case 1: memcpy(pps->ScalingList4x4[1], pps->ScalingList4x4[0]     , 16); break;
              case 2: memcpy(pps->ScalingList4x4[2], pps->ScalingList4x4[1]     , 16); break;
              case 3: memcpy(pps->ScalingList4x4[3], sps->ScalingList4x4[3]     , 16); break;
              case 4: memcpy(pps->ScalingList4x4[4], pps->ScalingList4x4[3]     , 16); break;
              case 5: memcpy(pps->ScalingList4x4[5], pps->ScalingList4x4[4]     , 16); break;
              case 6: memcpy(pps->ScalingList8x8[0], sps->ScalingList8x8[0]     , 64); break;
              case 7: memcpy(pps->ScalingList8x8[1], sps->ScalingList8x8[1]     , 64); break;
              default:
                LUD_DEBUG_ASSERT(0);    // cannot happen, i = 0..7
                break;
            }
          }
          else
          {
            switch (i)   // implements table 7.2 Scaling list fall-back rule set A of the standard
            {
              case 0: memcpy(pps->ScalingList4x4[0], ScalingList_Default_4x4[0] , 16); break;
              case 1: memcpy(pps->ScalingList4x4[1], pps->ScalingList4x4[0]     , 16); break;
              case 2: memcpy(pps->ScalingList4x4[2], pps->ScalingList4x4[1]     , 16); break;
              case 3: memcpy(pps->ScalingList4x4[3], ScalingList_Default_4x4[1] , 16); break;
              case 4: memcpy(pps->ScalingList4x4[4], pps->ScalingList4x4[3]     , 16); break;
              case 5: memcpy(pps->ScalingList4x4[5], pps->ScalingList4x4[4]     , 16); break;
              case 6: memcpy(pps->ScalingList8x8[0], ScalingList_Default_8x8[0] , 64); break;
              case 7: memcpy(pps->ScalingList8x8[1], ScalingList_Default_8x8[1] , 64); break;
              default:
                LUD_DEBUG_ASSERT(0);    // cannot happen, i = 0..7
                break;
            }
          }
        }
      } //for
    pps->second_chroma_qp_index_offset = sexpbits(bs); // 1 se(v)
  }

  LUD_DEBUG_ASSERT(parse_rbsp_trailing_bits_lud(nalu, bs, 0) == LUDH264_SUCCESS);

  // Fills in dome default values
  if (!pps->pic_scaling_matrix_present_flag)
  {
    // copy the scaling lists from the sps
    memcpy(pps->ScalingList4x4, sps->ScalingList4x4, 6*16);
    memcpy(pps->ScalingList8x8, sps->ScalingList8x8, 2*64);

  }

  {
    int l, i, j, k, m;
    pps->SliceGroupChangeRate = pps->slice_group_change_rate_minus1 + 1;

    // Derive LevelScale(m, i, j)
    for( l=0; l<6; l++)
    {
      uint8_t* weightScale = pps->ScalingList4x4[l];
      for( m=0; m<6; m++)
      {
        uint16_t* LevelScale4x4 = pps->LevelScale4x4[l][m];

        for(k=0; k<16; k++)
        {
          int idx = inverse_4x4zigzag_scan[k];
          i = idx >> 2;
          j = idx & 3;
          LevelScale4x4[idx] = weightScale[k] * normAdjust4x4[m][(i&1)+(j&1)];
        }
      }
    }
    // Derive LevelScale8x8(m, i, j)
    for( l=0; l<2; l++)
    {
      uint8_t* weightScale = pps->ScalingList8x8[l];
      for( m=0; m<6; m++)
      {
        uint16_t* LevelScale8x8 = pps->LevelScale8x8[l][m];

        for(k=0; k<64; k++)
        {
          int idx = inverse_8x8zigzag_scan[k];
          i = idx >> 3;
          j = idx & 7;
          LevelScale8x8[idx] = weightScale[k] * normAdjust8x8[m][(i&3)+(j&3)*4];
        }
      }
    }
  }

  pps->occupied = 1;
  return LUDH264_SUCCESS;
}

RetCode parse_filler_data(nal_unit_t* nalu)
{
  struct bitbuf* bs = nalu->bs;
  int fillbytes = 0;

  while (showbits(bs, 8)==0xff) {
    flushbits(bs, 8);
    fillbytes++;
  }
  LUD_DEBUG_ASSERT(parse_rbsp_trailing_bits_lud(nalu, bs, 0) == LUDH264_SUCCESS);

  LUD_TRACE(TRACE_INFO, "filler nalu had %d bytes", fillbytes);

  return LUDH264_SUCCESS;
}

static void free_ref_pic_list_reordering(ref_pic_list_reordering_t* rplr)
{
  rvfree(rplr->pic_list_reordering_commands[0]);
  rvfree(rplr->pic_list_reordering_commands[1]);
}

// this parsing does not do allocation. Caller must allocate the structure
static RetCode parse_ref_pic_list_reordering(struct slice_header* sh, struct bitbuf* bs, ref_pic_list_reordering_t* rplr)
{
  int idx;
  int reordering_of_pic_nums_idc;




  rplr->pic_list_reordering_commands[0] = NULL;
  rplr->pic_list_reordering_commands[1] = NULL;

  if (sh->slice_type_modulo5 != SLICE_I && sh->slice_type_modulo5 != SLICE_SI)
  {
    rplr->ref_pic_list_reordering_flag[0] = getbit(bs); // 2 u(1)
    if (rplr->ref_pic_list_reordering_flag[0])
    {
      idx = 0;
      rplr->pic_list_reordering_commands[0] = (uint32_t *)rvalloc(NULL, (sh->num_ref_idx_active[0]+1)*sizeof(*rplr->pic_list_reordering_commands[0]), 0);
      if (!rplr->pic_list_reordering_commands[0]) {
        free_ref_pic_list_reordering(rplr);
        return LUDH264_ERR_MEMORY_ALLOC_FAILED;
      }
      do
      {
        reordering_of_pic_nums_idc = uexpbits(bs); // 2 ue(v)
        LUD_DEBUG_ASSERT(reordering_of_pic_nums_idc <= 3);
        LUD_DEBUG_ASSERT(idx <= sh->num_ref_idx_active[0]);
        if (reordering_of_pic_nums_idc == 0 || reordering_of_pic_nums_idc == 1)
          rplr->pic_list_reordering_commands[0][idx] = uexpbits(bs); // 2 ue(v) abs_diff_pic_num_minus1
        else if (reordering_of_pic_nums_idc == 2)
          rplr->pic_list_reordering_commands[0][idx] = uexpbits(bs); // 2 ue(v) long_term_pic_num
        else
          rplr->pic_list_reordering_commands[0][idx] = 0;

        rplr->pic_list_reordering_commands[0][idx++] |= reordering_of_pic_nums_idc << 30;
      } while (reordering_of_pic_nums_idc != 3);
    }
  }
  if (sh->slice_type_modulo5 == SLICE_B)
  {
    rplr->ref_pic_list_reordering_flag[1] = getbit(bs); // 2 u(1)
    if (rplr->ref_pic_list_reordering_flag[1])
    {
      idx = 0;
      rplr->pic_list_reordering_commands[1] = (uint32_t *)rvalloc(NULL, (sh->num_ref_idx_active[1]+1)*sizeof(*rplr->pic_list_reordering_commands[1]), 0);
      if (!rplr->pic_list_reordering_commands[1]) {
        free_ref_pic_list_reordering(rplr);
        return LUDH264_ERR_MEMORY_ALLOC_FAILED;
      }
      do
      {
        reordering_of_pic_nums_idc = uexpbits(bs); // 2 ue(v)
        LUD_DEBUG_ASSERT(reordering_of_pic_nums_idc <= 3);
        LUD_DEBUG_ASSERT(idx <= sh->num_ref_idx_active[1]);
        if (reordering_of_pic_nums_idc == 0 || reordering_of_pic_nums_idc == 1)
          rplr->pic_list_reordering_commands[1][idx] = uexpbits(bs); // 2 ue(v) abs_diff_pic_num_minus1
        else if (reordering_of_pic_nums_idc == 2)
          rplr->pic_list_reordering_commands[1][idx] = uexpbits(bs); // 2 ue(v) long_term_pic_num
        else
          rplr->pic_list_reordering_commands[1][idx] = 0;

        rplr->pic_list_reordering_commands[1][idx++] |= reordering_of_pic_nums_idc << 30;
      } while (reordering_of_pic_nums_idc != 3);
    }
  }
  return LUDH264_SUCCESS;
}


static void free_pred_weight_table(pred_weight_table_t* pwt)
{
  rvfree(pwt);
}

#define ALIGN(p, a) (((unsigned long)(p) + (a) -1) & ~((a) -1))
static RetCode parse_pred_weight_table(struct rv264seq_parameter_set* sps, struct slice_header* sh, struct bitbuf* bs, pred_weight_table_t** pwt_)
{
  uint8_t luma_weight_l0_flag;
  uint8_t luma_weight_l1_flag;
  uint8_t chroma_weight_l0_flag;
  uint8_t chroma_weight_l1_flag;
  int i;
  int factorY = (1<<(sps->BitDepthY-8));
  int factorC = (1<<(sps->BitDepthC-8));


  // Allocate pred_weight_table_t structure
  unsigned int pes = sps->chroma_format_idc == 0 ? 3 : 9;
  unsigned int size = sizeof(pred_weight_table_t) + (sh->num_ref_idx_active[0])*pes + (sh->num_ref_idx_active[1])*pes +8; // +8 in order to ensure alignment of offset sub arrays
  pred_weight_table_t *pwt = (pred_weight_table_t *)rvalloc(NULL, size, 0);
  if (!pwt) {
    free_pred_weight_table(pwt);
    return LUDH264_ERR_MEMORY_ALLOC_FAILED;
  }

  *pwt_ = pwt;

  pwt->luma_log2_weight_denom = uexpbits(bs); // 2 ue(v)
  if (sps->chroma_format_idc != 0)
    pwt->chroma_log2_weight_denom = uexpbits(bs); // 2 ue(v)
  pwt->luma_weight_l[0] = (int8_t*) pwt + sizeof(*pwt);
  pwt->luma_offset_l[0] = (int16_t*) ALIGN(pwt->luma_weight_l[0] + (sh->num_ref_idx_active[0]), 2);
  if (sps->chroma_format_idc != 0)
  {
    pwt->chroma_weight_l[0][0] = (int8_t*) (pwt->luma_offset_l[0] + sh->num_ref_idx_active[0]);
    pwt->chroma_weight_l[1][0] = pwt->chroma_weight_l[0][0] + sh->num_ref_idx_active[0];
    pwt->chroma_offset_l[0][0] = (int16_t*) ALIGN(pwt->chroma_weight_l[1][0] + sh->num_ref_idx_active[0], 2);
    pwt->chroma_offset_l[1][0] = pwt->chroma_offset_l[0][0] + sh->num_ref_idx_active[0];
  }
  for (i = 0; i < sh->num_ref_idx_active[0]; i++)
  {
    luma_weight_l0_flag = getbit(bs); // 2 u(1)
    if (luma_weight_l0_flag)
    {
      pwt->luma_weight_l[0][i] = sexpbits(bs); // 2 se(v)
      pwt->luma_offset_l[0][i] = sexpbits(bs); // 2 se(v)
      pwt->luma_offset_l[0][i] *= factorY;
    }
    else // luma_weight_l0_flag is false
    {
      pwt->luma_weight_l[0][i] = 1 << pwt->luma_log2_weight_denom;
      pwt->luma_offset_l[0][i] = 0;
    }
    if (sps->chroma_format_idc != 0)
    {
      chroma_weight_l0_flag = getbit(bs); // 2 u(1)
      if (chroma_weight_l0_flag)
      {
        pwt->chroma_weight_l[0][0][i] = sexpbits(bs); // 2 se(v)
        pwt->chroma_offset_l[0][0][i] = sexpbits(bs); // 2 se(v)
        pwt->chroma_offset_l[0][0][i] *= factorC;
        pwt->chroma_weight_l[1][0][i] = sexpbits(bs); // 2 se(v)
        pwt->chroma_offset_l[1][0][i] = sexpbits(bs); // 2 se(v)
        pwt->chroma_offset_l[1][0][i] *= factorC;
      }
      else // chroma_weight_l0_flag is false
      {
        pwt->chroma_weight_l[0][0][i] = 1 << pwt->chroma_log2_weight_denom;
        pwt->chroma_offset_l[0][0][i] = 0;
        pwt->chroma_weight_l[1][0][i] = 1 << pwt->chroma_log2_weight_denom;
        pwt->chroma_offset_l[1][0][i] = 0;
      }
    }
  } // for
  if (sh->slice_type_modulo5 == SLICE_B)
  {
    pwt->luma_weight_l[1] = (int8_t*) pwt + sizeof(*pwt) + pes * sh->num_ref_idx_active[0] + 4;
    pwt->luma_offset_l[1] = (int16_t*) ALIGN(pwt->luma_weight_l[1] + (sh->num_ref_idx_active[1]), 2);
    if (sps->chroma_format_idc != 0)
    {
      pwt->chroma_weight_l[0][1] = (int8_t*) (pwt->luma_offset_l[1] + sh->num_ref_idx_active[1]);
      pwt->chroma_weight_l[1][1] = pwt->chroma_weight_l[0][1] + sh->num_ref_idx_active[1];
      pwt->chroma_offset_l[0][1] = (int16_t*) ALIGN(pwt->chroma_weight_l[1][1] + sh->num_ref_idx_active[1], 2);
      pwt->chroma_offset_l[1][1] = pwt->chroma_offset_l[0][1] + sh->num_ref_idx_active[1];
    }
    for (i = 0; i < sh->num_ref_idx_active[1]; i++)
    {
      luma_weight_l1_flag = getbit(bs); // 2 u(1)
      if (luma_weight_l1_flag)
      {
        pwt->luma_weight_l[1][i] = sexpbits(bs); // 2 se(v)
        pwt->luma_offset_l[1][i] = sexpbits(bs); // 2 se(v)
        pwt->luma_offset_l[1][i] *= factorY;
      }
      else // luma_weight_l1_flag is false
      {
        pwt->luma_weight_l[1][i] = 1 << pwt->luma_log2_weight_denom;
        pwt->luma_offset_l[1][i] = 0;
      }
      if (sps->chroma_format_idc != 0)
      {
        chroma_weight_l1_flag = getbit(bs); // 2 u(1)
        if (chroma_weight_l1_flag)
        {
          pwt->chroma_weight_l[0][1][i] = sexpbits(bs); // 2 se(v)
          pwt->chroma_offset_l[0][1][i] = sexpbits(bs); // 2 se(v)
          pwt->chroma_offset_l[0][1][i] *= factorC;
          pwt->chroma_weight_l[1][1][i] = sexpbits(bs); // 2 se(v)
          pwt->chroma_offset_l[1][1][i] = sexpbits(bs); // 2 se(v)
          pwt->chroma_offset_l[1][1][i] *= factorC;
        }
        else // chroma_weight_l0_flag is false
        {
          pwt->chroma_weight_l[0][1][i] = 1 << pwt->chroma_log2_weight_denom;
          pwt->chroma_offset_l[0][1][i] = 0;
          pwt->chroma_weight_l[1][1][i] = 1 << pwt->chroma_log2_weight_denom;
          pwt->chroma_offset_l[1][1][i] = 0;
        }
      }
    } // for
  }
  return LUDH264_SUCCESS;
}

static RetCode parse_dec_ref_pic_marking(nal_unit_t* nalu, struct slice_header* sh, struct bitbuf* bs, dec_ref_pic_marking_t* drpm)
{
  uint8_t memory_management_control_operation;

  drpm->nb_of_mmco_cmd = 0;

  if( nalu->nal_unit_type == 5 )
  {
    drpm->no_output_of_prior_pics_flag = getbit(bs); // 2|5 u(1)
    drpm->long_term_reference_flag = getbit(bs); // 2|5 u(1)
    drpm->adaptive_ref_pic_marking_mode_flag = 0; // default value
  }
  else
  {
    drpm->no_output_of_prior_pics_flag = 0; // default value
    drpm->long_term_reference_flag = 0;     // default value
    drpm->adaptive_ref_pic_marking_mode_flag = getbit(bs); // 2|5 u(1)
    if( drpm->adaptive_ref_pic_marking_mode_flag )
    {
      int idx = 0;
      uint8_t* commands = drpm->mmco_commands;
      do
      {
        memory_management_control_operation = uexpbits(bs); // 2|5 ue(v)

        switch (memory_management_control_operation)
        {
          case 4: // 2|5 ue(v) max_long_term_frame_idx_plus1, bits 0..28
          case 6: // 2|5 ue(v) long_term_frame_idx, bits 0..28
          case 1: // 2|5 ue(v) difference_of_pic_nums_minus1 bits, 0..2
          case 2: // 2|5 ue(v) long_term_pic_num, bits 0..28
            commands[idx] = uexpbits(bs);
            break;
          case 3:
            commands[idx] = uexpbits(bs); // 2|5 ue(v) difference_of_pic_nums_minus1, bits 0..22 (23 bits is enough)
            commands[idx] |= uexpbits(bs) << 23; // 2|5 ue(v) long_term_frame_idx, bits 23..28 (6 bits is enough)
            break;
          case 5:
            sh->has_mmco5 = 1;
          case 0:
            commands[idx] = 0;
            break;
          default:
            rvexit("parse_dec_ref_pic_marking: unknown mmco command: %d", memory_management_control_operation);
            break;
        }

        commands[idx] |= memory_management_control_operation << 29; // the command is stored into the bits 29..31 (3 bits is enough for 7 commands from 0 to 6)

        idx++;
      } while (memory_management_control_operation != 0 && idx<MAX_NB_OF_MMCO_COMMANDS);

      LUD_DEBUG_ASSERT(idx < MAX_NB_OF_MMCO_COMMANDS); // there are move mmco commands than we can store ?
      if (idx == MAX_NB_OF_MMCO_COMMANDS)
        commands[--idx] = 0; // close the command list (prevent overflow in subsequent code...)

      drpm->nb_of_mmco_cmd = idx;
    }
  }
  return LUDH264_SUCCESS;
}

void release_slice_header(struct slice_header* sh)
{
  if (sh)
  {
    if (sh->ref_count == 0)
    {
      free_ref_pic_list_reordering(&sh->ref_pic_list_reordering);
      free_pred_weight_table(sh->pred_weight_table);
      if (sh->RefPicList[0]) // list 1 is on the same buffer
      {
/* We do not recursively release picture here ! Pictures from these lists are released at the end of the decoding process of the curr picture.
        int i;
        for (i=0; i<sh->num_ref_idx_active[0]; i++)
          if (sh->field_pic_flag)
            release_picture_field(sh->RefPicList[0][i].ref_pic, sh->RefPicList[0][i].parity);
          else
            release_picture(sh->RefPicList[0][i].ref_pic);


        if (sh->RefPicList[1])
          for (i=0; i<sh->num_ref_idx_active[1]; i++)
            if (sh->field_pic_flag)
              release_picture_field(sh->RefPicList[1][i].ref_pic, sh->RefPicList[1][i].parity);
            else
              release_picture(sh->RefPicList[1][i].ref_pic);
*/

        rvfree(sh->RefPicList[0]);
      }
      rvfree(sh);
    }
    else
      sh->ref_count--;
  }
}

RetCode parse_slice_header_lud(struct rv264sequence *seq, nal_unit_t* nalu, struct slice_header** sh_)
{
  RetCode r;
  struct rv264seq_parameter_set* sps;
  struct rv264pic_parameter_set* pps;
  struct bitbuf* bs = nalu->bs;

  // Allocate slice header structure
  struct slice_header *sh = (struct slice_header *)rvalloc(NULL, sizeof(struct slice_header), 0);
  if (!sh) {
    release_slice_header(sh);
    return LUDH264_ERR_MEMORY_ALLOC_FAILED;
  }

  *sh_ = sh;
  OBJECT_HEADER_INST(sh);

  // Default values
  sh->pred_weight_table = 0;
  sh->field_pic_flag = 0;
  sh->bottom_field_flag = 0;
  sh->delta_pic_order_cnt[ 0 ] = sh->delta_pic_order_cnt[ 1 ] = 0;
  sh->delta_pic_order_cnt_bottom = 0;
  sh->has_mmco5 = 0;
  sh->disable_deblocking_filter_idc = 0;
  sh->slice_alpha_c0_offset_div2 = 0;
  sh->slice_beta_offset_div2 = 0;
  sh->MbToSliceGroupMap = 0;



  sh->first_mb_in_slice = uexpbits(bs); // 2    ue(v)
  sh->slice_type = slice_type_t(uexpbits(bs)); // 2    ue(v)
  sh->slice_type_modulo5 = slice_type_t(sh->slice_type < 5 ? sh->slice_type : sh->slice_type - 5);
  sh->pic_parameter_set_id = uexpbits(bs); // 2    ue(v)
  pps = seq->pps[sh->pic_parameter_set_id];
  if (!pps || !pps->occupied)
    return LUDH264_ERR_REFERING_NON_EXISTING_PPS;
  sps = seq->sps[pps->seq_parameter_set_id];
  if (!sps || !sps->occupied)
    return LUDH264_ERR_REFERING_NON_EXISTING_SPS;
  sh->frame_num = getbits(bs, sps->log2_max_frame_num_minus4+4 ); // 2    u(v)
  sh->original_frame_num = sh->frame_num;
  if( !sps->frame_mbs_only_flag )
  {
    sh->field_pic_flag = getbit(bs); // 2    u(1)
    if( sh->field_pic_flag )
      sh->bottom_field_flag = getbit(bs); // 2    u(1)
  }
  if( nalu->nal_unit_type == 5 )
    sh->idr_pic_id = uexpbits(bs); // 2    ue(v)
  if( sps->pic_order_cnt_type == 0 )
  {
    sh->pic_order_cnt_lsb = getbits(bs, sps->log2_max_pic_order_cnt_lsb_minus4+4); // 2    u(v)
    if( pps->pic_order_present_flag && !sh->field_pic_flag )
      sh->delta_pic_order_cnt_bottom = sexpbits(bs); // 2    se(v)
  }
  if( sps->pic_order_cnt_type == 1 && !sps->delta_pic_order_always_zero_flag )
  {
    sh->delta_pic_order_cnt[ 0 ] = sexpbits(bs); // 2    se(v)
    if( pps->pic_order_present_flag && !sh->field_pic_flag )
      sh->delta_pic_order_cnt[ 1 ] = sexpbits(bs); // 2    se(v)
  }
  if( pps->redundant_pic_cnt_present_flag )
    sh->redundant_pic_cnt = uexpbits(bs); // 2    ue(v)
  if( sh->slice_type_modulo5 == SLICE_B )
    sh->direct_spatial_mv_pred_flag = getbit(bs); // 2    u(1)
  sh->num_ref_idx_active[0] = pps->num_ref_idx_active[0]; // default value
  sh->num_ref_idx_active[1] = pps->num_ref_idx_active[1]; // default value
  if( sh->slice_type_modulo5 == SLICE_P || sh->slice_type_modulo5 == SLICE_SP || sh->slice_type_modulo5 == SLICE_B )
  {
    sh->num_ref_idx_active_override_flag = getbit(bs); // 2    u(1)
    if( sh->num_ref_idx_active_override_flag )
    {
      sh->num_ref_idx_active[0] = uexpbits(bs)+1; // 2    ue(v)
      if (sh->num_ref_idx_active[0] > NB_OF_REF_PICS)
      {
        LUD_DEBUG_ASSERT(0); // num_ref_idx_active_minus1 overflow
        LUD_TRACE(TRACE_ERROR, "num_ref_idx_active_minus1 L0 overflow, set to max number of ref minus 1");
        sh->num_ref_idx_active[0] = NB_OF_REF_PICS;
      }
      if( sh->slice_type_modulo5 == SLICE_B )
      {
        sh->num_ref_idx_active[1] = uexpbits(bs)+1; // 2    ue(v)
        if (sh->num_ref_idx_active[1] > NB_OF_REF_PICS)
        {
          LUD_DEBUG_ASSERT(0); // num_ref_idx_active_minus1 overflow
          LUD_TRACE(TRACE_ERROR, "num_ref_idx_active_minus1 L1 overflow, set to max number of ref minus 1");
          sh->num_ref_idx_active[1] = NB_OF_REF_PICS;
        }
      }
    }
  }

  // the structure itself is allocated into the slice header structure ! However the function allocates tables...
  if (LUDH264_SUCCESS != (r = parse_ref_pic_list_reordering(sh, bs, &sh->ref_pic_list_reordering))) {
    release_slice_header(sh);
    return r;
  }

  if( ( pps->weighted_pred_flag && ( sh->slice_type_modulo5  == SLICE_P || sh->slice_type_modulo5 == SLICE_SP ) ) ||
      ( pps->weighted_bipred_idc == 1 && sh->slice_type_modulo5 == SLICE_B ) )
  {
    if (LUDH264_SUCCESS != (r = parse_pred_weight_table(sps, sh, bs, &sh->pred_weight_table))) {
      release_slice_header(sh);
      return r;
    }
  }

  if( nalu->nal_ref_idc != 0 )
  {
    if (LUDH264_SUCCESS != (r = parse_dec_ref_pic_marking(nalu, sh, bs, &sh->dec_ref_pic_marking))) {
      release_slice_header(sh);
      return r;
    }
  }

  if( pps->entropy_coding_mode_flag && sh->slice_type_modulo5 != SLICE_I && sh->slice_type_modulo5 != SLICE_SI )
    sh->cabac_init_idc = uexpbits(bs); // 2    ue(v)
  sh->slice_qp_delta = sexpbits(bs); // 2    se(v)
  if( sh->slice_type_modulo5 == SLICE_SP || sh->slice_type_modulo5 == SLICE_SI )
  {
    if( sh->slice_type_modulo5 == SLICE_SP )
      sh->sp_for_switch_flag = getbit(bs); // 2    u(1)
    sh->slice_qs_delta = sexpbits(bs); // 2 se(v)
  }
  if( pps->deblocking_filter_control_present_flag )
  {
    sh->disable_deblocking_filter_idc = uexpbits(bs); // 2 ue(v)
    if( sh->disable_deblocking_filter_idc != 1 )
    {
      sh->slice_alpha_c0_offset_div2 = sexpbits(bs); // 2 se(v)
      sh->slice_beta_offset_div2 = sexpbits(bs); // 2 se(v)
    }
  }
  if( pps->num_slice_groups_minus1 > 0 && pps->slice_group_map_type >= 3 && pps->slice_group_map_type <= 5)
    sh->slice_group_change_cycle = getbits(bs, im_ceillog2( sps->PicSizeInMapUnits / pps->SliceGroupChangeRate + 1 )); // 2 u(v)

  // compute derived variables
  {
    sh->pps = pps;
    sh->sps = sps;
    sh->MbaffFrameFlag = ( sps->mb_adaptive_frame_field_flag && !sh->field_pic_flag );
    sh->PicHeightInMbs = sps->FrameHeightInMbs >> sh->field_pic_flag;
    sh->PicSizeInMbs = sps->PicWidthInMbs * sh->PicHeightInMbs;
    if (sh->field_pic_flag)
    {
      sh->MaxPicNum =  sps->MaxFrameNum * 2;
      sh->CurrPicNum = sh->frame_num * 2 + 1;
    }
    else
    {
      sh->MaxPicNum =  sps->MaxFrameNum;
      sh->CurrPicNum = sh->frame_num;
    }
    sh->SliceQPY = 26 + pps->pic_init_qp_minus26 + sh->slice_qp_delta;

    // TODO FIXME remove non necessary variables from the struct!:  slice_alpha_c0_offset_div2 slice_beta_offset_div2
    sh->FilterOffsetA = sh->slice_alpha_c0_offset_div2 << 1;
    sh->FilterOffsetB = sh->slice_beta_offset_div2 << 1;

    sh->nal_ref_idc = nalu->nal_ref_idc;
    sh->nal_unit_type = nalu->nal_unit_type;
    sh->RefPicList[0] = sh->RefPicList[1] = 0;
  }

  return LUDH264_SUCCESS;
}




