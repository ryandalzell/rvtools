/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/

#include "decode.h"
#include "defaulttables.h"
#include "inverse_transforms.h"
#include "intmath.h"
#include "decode_slice_data.h"

unsigned int Intra16x16PredMode(mb_type_t mb_type)
{
  LUD_DEBUG_ASSERT(mb_type >= I_16x16_0_0_0 && mb_type <= I_16x16_3_2_1);
  return Intra16x16PredMode_NumMbPart_array[mb_type];
}

/*
 * Intrat 4x4 prediction
 */

void copy4samples(pixel_t* dst, const pixel_t* src)
{
#ifdef PIXEL_T_IS_8_BITS
  LUD_DEBUG_ASSERT(((unsigned long)dst&3) == 0 && ((unsigned long)src&3) == 0);
  *(uint32_t*)dst = *(uint32_t*)src; // could be unaligned 32/64 bits copy
#else // => PIXEL_T_IS_16_BITS
  LUD_DEBUG_ASSERT(((unsigned long)dst&7) == 0 && ((unsigned long)src&7) == 0);
  *(uint64_t*)dst = *(uint64_t*)src; // could be unaligned 32/64 bits copy
#endif
}
void duplicate1to4samples(pixel_t* dst, const pixel_t v)
{
#ifdef PIXEL_T_IS_8_BITS
  LUD_DEBUG_ASSERT(((unsigned long)dst&3) == 0);
  *(uint32_t*)dst = (uint32_t)v * (uint32_t)0x01010101;
#else // => PIXEL_T_IS_16_BITS
# ifndef ARCH_X86_64
  LUD_DEBUG_ASSERT(((unsigned long)dst&3) == 0);
  ((uint32_t*)dst)[0] = ((uint32_t*)dst)[1] =  v * 0x00010001; // 64bit multiply is not a good idea on 32 bits architecture
# else
  LUD_DEBUG_ASSERT(((unsigned long)dst&7) == 0);
  *(uint64_t*)dst = (uint64_t)v * (uint64_t)0x0001000100010001;
# endif
#endif
}


#define PRED4x4_LOAD_LEFT_TOP_SAMPLE\
    const int lt= dst[-1-1*dy];

#define PRED4x4_LOAD_TOP_RIGHT_EDGE   \
    int unused_param t4, t5, t6, t7;  \
    if (topright_is_avail)            \
{                                 \
      t4 = dst[-dy+4];                \
      t5 = dst[-dy+5];                \
      t6 = dst[-dy+6];                \
      t7 = dst[-dy+7];                \
}                                 \
    else                              \
      t4 = t5 = t6 = t7 = dst[-dy+3];

#define PRED4x4_LOAD_LEFT_EDGE\
    const int unused_param l0= dst[-1+0*dy];\
    const int unused_param l1= dst[-1+1*dy];\
    const int unused_param l2= dst[-1+2*dy];\
    const int unused_param l3= dst[-1+3*dy];

#define PRED4x4_LOAD_TOP_EDGE\
    const int unused_param t0= dst[ 0-1*dy];\
    const int unused_param t1= dst[ 1-1*dy];\
    const int unused_param t2= dst[ 2-1*dy];\
    const int unused_param t3= dst[ 3-1*dy];


void pred_intra_4x4_vertical(int dy, pixel_t* dst)
{
  pixel_t* d = dst;
  const pixel_t* src = dst - dy;
  copy4samples(d, src); d += dy;
  copy4samples(d, src); d += dy;
  copy4samples(d, src); d += dy;
  copy4samples(d, src);
}
void pred_intra_4x4_horizontal(int dy, pixel_t* dst)
{
  const pixel_t* s = dst-1;
  pixel_t* d = dst;
  duplicate1to4samples(d, *s); s += dy; d += dy;
  duplicate1to4samples(d, *s); s += dy; d += dy;
  duplicate1to4samples(d, *s); s += dy; d += dy;
  duplicate1to4samples(d, *s);
}
// mode: 0=avr of left and top samples, 1: avr of left, 2: avr of top, 3: default value zero
void pred_intra_4x4_dc(int dy, pixel_t* dst, int mode, pixel_t zero)
{
  int dc;
  if (mode == 0)
  {
    int a, b;
    const pixel_t* sa = dst-dy;
    const pixel_t* sb = dst-1;
    a =  *sa++;
    b =  *sb; sb += dy;
    a += *sa++;
    b += *sb; sb += dy;
    a += *sa++;
    b += *sb; sb += dy;
    a += *sa;
    b += *sb;
    dc = (a + b + 4) >> 3;
  }
  else if (mode == 1)
  {
    int b;
    const pixel_t* sb = dst-1;
    b =  *sb; sb += dy;
    b += *sb; sb += dy;
    b += *sb; sb += dy;
    b += *sb;
    dc = (b + 2) >> 2;
  }
  else if (mode == 2)
  {
    int a;
    const pixel_t* sa = dst-dy;
    a =  *sa++;
    a += *sa++;
    a += *sa++;
    a += *sa;
    dc = (a + 2) >> 2;
  }
  else // mode = 3
    dc = zero;
  {
    pixel_t* d = dst + dy;
    const pixel_t* src = dst;
    duplicate1to4samples(dst, (pixel_t)dc);
    copy4samples(d, src); d += dy;
    copy4samples(d, src); d += dy;
    copy4samples(d, src);
  }
}

void pred_intra_4x4_diagonal_down_left(int dy, pixel_t* dst, unsigned int topright_is_avail)
{
  PRED4x4_LOAD_TOP_RIGHT_EDGE;
  PRED4x4_LOAD_TOP_EDGE;

  dst[0+0*dy] = (t0 + t2 + 2 * t1 + 2) >> 2;
  dst[1+0*dy] =
    dst[0+1*dy] = (t1 + t3 + 2 * t2 + 2) >> 2;
  dst[2+0*dy] =
    dst[1+1*dy] =
      dst[0+2*dy] = (t2 + t4 + 2 * t3 + 2) >> 2;
  dst[3+0*dy] =
    dst[2+1*dy] =
      dst[1+2*dy] =
        dst[0+3*dy] = (t3 + t5 + 2 * t4 + 2) >> 2;
  dst[3+1*dy] =
    dst[2+2*dy] =
      dst[1+3*dy] = (t4 + t6 + 2 * t5 + 2) >> 2;
  dst[3+2*dy] =
    dst[2+3*dy] = (t5 + t7 + 2 * t6 + 2) >> 2;
  dst[3+3*dy] = (t6 + 3 * t7 + 2) >> 2;
}
void pred_intra_4x4_diagonal_down_right(int dy, pixel_t* dst)
{
  PRED4x4_LOAD_LEFT_TOP_SAMPLE;
  PRED4x4_LOAD_TOP_EDGE;
  PRED4x4_LOAD_LEFT_EDGE;

  dst[0+3*dy] = (l3 + 2 * l2 + l1 + 2) >> 2;
  dst[0+2*dy] =
    dst[1+3*dy] = (l2 + 2 * l1 + l0 + 2) >> 2;
  dst[0+1*dy] =
    dst[1+2*dy] =
      dst[2+3*dy] = (l1 + 2 * l0 + lt + 2) >> 2;
  dst[0+0*dy] =
    dst[1+1*dy] =
      dst[2+2*dy] =
        dst[3+3*dy] = (l0 + 2 * lt + t0 + 2) >> 2;
  dst[1+0*dy] =
    dst[2+1*dy] =
      dst[3+2*dy] = (lt + 2 * t0 + t1 + 2) >> 2;
  dst[2+0*dy] =
    dst[3+1*dy] = (t0 + 2 * t1 + t2 + 2) >> 2;
  dst[3+0*dy] = (t1 + 2 * t2 + t3 + 2) >> 2;
}
void pred_intra_4x4_vertical_right(int dy, pixel_t* dst)
{
  PRED4x4_LOAD_LEFT_TOP_SAMPLE;
  PRED4x4_LOAD_TOP_EDGE;
  PRED4x4_LOAD_LEFT_EDGE;

  dst[0+0*dy] =
    dst[1+2*dy] = (lt + t0 + 1) >> 1;
  dst[1+0*dy] =
    dst[2+2*dy] = (t0 + t1 + 1) >> 1;
  dst[2+0*dy] =
    dst[3+2*dy] = (t1 + t2 + 1) >> 1;
  dst[3+0*dy] = (t2 + t3 + 1) >> 1;
  dst[0+1*dy] =
    dst[1+3*dy] = (l0 + 2 * lt + t0 + 2) >> 2;
  dst[1+1*dy] =
    dst[2+3*dy] = (lt + 2 * t0 + t1 + 2) >> 2;
  dst[2+1*dy] =
    dst[3+3*dy] = (t0 + 2 * t1 + t2 + 2) >> 2;
  dst[3+1*dy] = (t1 + 2 * t2 + t3 + 2) >> 2;
  dst[0+2*dy] = (lt + 2 * l0 + l1 + 2) >> 2;
  dst[0+3*dy] = (l0 + 2 * l1 + l2 + 2) >> 2;
}
void pred_intra_4x4_horinzontal_down(int dy, pixel_t* dst)
{
  PRED4x4_LOAD_LEFT_TOP_SAMPLE;
  PRED4x4_LOAD_TOP_EDGE;
  PRED4x4_LOAD_LEFT_EDGE;

  dst[0+0*dy] =
    dst[2+1*dy] = (lt + l0 + 1) >> 1;
  dst[1+0*dy] =
    dst[3+1*dy] = (l0 + 2 * lt + t0 + 2) >> 2;
  dst[2+0*dy] = (lt + 2 * t0 + t1 + 2) >> 2;
  dst[3+0*dy] = (t0 + 2 * t1 + t2 + 2) >> 2;
  dst[0+1*dy] =
    dst[2+2*dy] = (l0 + l1 + 1) >> 1;
  dst[1+1*dy] =
    dst[3+2*dy] = (lt + 2 * l0 + l1 + 2) >> 2;
  dst[0+2*dy] =
    dst[2+3*dy] = (l1 + l2 + 1) >> 1;
  dst[1+2*dy] =
    dst[3+3*dy] = (l0 + 2 * l1 + l2 + 2) >> 2;
  dst[0+3*dy] = (l2 + l3 + 1) >> 1;
  dst[1+3*dy] = (l1 + 2 * l2 + l3 + 2) >> 2;
}
void pred_intra_4x4_vertical_left(int dy, pixel_t* dst, unsigned int topright_is_avail)
{
  PRED4x4_LOAD_TOP_EDGE;
  PRED4x4_LOAD_TOP_RIGHT_EDGE;

  dst[0+0*dy] = (t0 + t1 + 1) >> 1;
  dst[1+0*dy] =
    dst[0+2*dy] = (t1 + t2 + 1) >> 1;
  dst[2+0*dy] =
    dst[1+2*dy] = (t2 + t3 + 1) >> 1;
  dst[3+0*dy] =
    dst[2+2*dy] = (t3 + t4 + 1) >> 1;
  dst[3+2*dy] = (t4 + t5 + 1) >> 1;
  dst[0+1*dy] = (t0 + 2 * t1 + t2 + 2) >> 2;
  dst[1+1*dy] =
    dst[0+3*dy] = (t1 + 2 * t2 + t3 + 2) >> 2;
  dst[2+1*dy] =
    dst[1+3*dy] = (t2 + 2 * t3 + t4 + 2) >> 2;
  dst[3+1*dy] =
    dst[2+3*dy] = (t3 + 2 * t4 + t5 + 2) >> 2;
  dst[3+3*dy] = (t4 + 2 * t5 + t6 + 2) >> 2;
}
void pred_intra_4x4_horizontal_up(int dy, pixel_t* dst)
{
  PRED4x4_LOAD_LEFT_EDGE;

  dst[0+0*dy] = (l0 + l1 + 1) >> 1;
  dst[1+0*dy] = (l0 + 2 * l1 + l2 + 2) >> 2;
  dst[2+0*dy] =
    dst[0+1*dy] = (l1 + l2 + 1) >> 1;
  dst[3+0*dy] =
    dst[1+1*dy] = (l1 + 2 * l2 + l3 + 2) >> 2;
  dst[2+1*dy] =
    dst[0+2*dy] = (l2 + l3 + 1) >> 1;
  dst[3+1*dy] =
    dst[1+2*dy] = (l2 + 2 * l3 + l3 + 2) >> 2;
  dst[3+2*dy] =
    dst[1+3*dy] =
    dst[0+3*dy] =
    dst[2+2*dy] =
    dst[2+3*dy] =
    dst[3+3*dy] = l3;
}




/*
 * Intra 8x8 prediction
 */

#define DST(x,y) dst[(x)+(y)*dy]
#define SRC(x,y) src[(x)+(y)*dy]
#define PL(y)    const int l##y = (SRC(-1,y-1) + 2*SRC(-1,y) + SRC(-1,y+1) + 2) >> 2;
#define PRED8x8_LOAD_LEFT_EDGE                                          \
    const int l0 = ((topleft_is_avail ? SRC(-1,-1) : SRC(-1,0))         \
                     + 2*SRC(-1,0) + SRC(-1,1) + 2) >> 2;               \
    PL(1) PL(2) PL(3) PL(4) PL(5) PL(6)                                 \
    const int unused_param l7 = (SRC(-1,6) + 3*SRC(-1,7) + 2) >> 2

#define PT(x)   const int t##x = (SRC(x-1,-1) + 2*SRC(x,-1) + SRC(x+1,-1) + 2) >> 2;
#define PRED8x8_LOAD_TOP_EDGE                                           \
    const int t0 = ((topleft_is_avail ? SRC(-1,-1) : SRC(0,-1))         \
                     + 2*SRC(0,-1) + SRC(1,-1) + 2) >> 2;               \
    PT(1) PT(2) PT(3) PT(4) PT(5) PT(6)                                 \
    const int unused_param t7 = ((topright_is_avail ? SRC(8,-1) : SRC(7,-1)) \
                     + 2*SRC(7,-1) + SRC(6,-1) + 2) >> 2

#define PTR(x)  t##x = (SRC(x-1,-1) + 2*SRC(x,-1) + SRC(x+1,-1) + 2) >> 2;
#define PRED8x8_LOAD_TOPRIGHT_EDGE                                      \
    int t8, t9, t10, t11, t12, t13, t14, t15;                           \
    if(topright_is_avail)                                               \
    {                                                                   \
        PTR(8) PTR(9) PTR(10) PTR(11) PTR(12) PTR(13) PTR(14)           \
        t15 = (SRC(14,-1) + 3*SRC(15,-1) + 2) >> 2;                     \
    }                                                                   \
    else                                                                \
      t8=t9=t10=t11=t12=t13=t14=t15= SRC(7,-1);

#define PRED8x8_LOAD_TOPLEFT_SAMPLE \
    const int lt = (SRC(-1,0) + 2*SRC(-1,-1) + SRC(0,-1) + 2) >> 2


void copy8samples(pixel_t* dst, const pixel_t* src)
{
  LUD_DEBUG_ASSERT(((unsigned long)dst&7) == 0 && ((unsigned long)src&7) == 0);
#ifdef PIXEL_T_IS_8_BITS
  *(uint64_t*)dst = *(uint64_t*)src; // could be unaligned 32/64 bits copy
#else // => PIXEL_T_IS_16_BITS
  ((uint64_t*)dst)[0] = ((uint64_t*)src)[0]; // could be unaligned 32/64 bits copy
  ((uint64_t*)dst)[1] = ((uint64_t*)src)[1]; // could be unaligned 32/64 bits copy
#endif
}
void duplicate1to8samples(pixel_t* dst, const pixel_t v)
{
#ifndef ARCH_X86_64
  LUD_DEBUG_ASSERT(((unsigned long)dst&3) == 0);

# ifdef PIXEL_T_IS_8_BITS
  ((uint32_t*)dst)[0] = ((uint32_t*)dst)[1] = (uint32_t)v * (uint32_t)0x01010101;
# else // => PIXEL_T_IS_16_BITS
  ((uint32_t*)dst)[0] = ((uint32_t*)dst)[1] = ((uint32_t*)dst)[2] = ((uint32_t*)dst)[3] = (uint32_t)v * (uint32_t)0x00010001;
# endif

#else // ARCH_X86_64
  LUD_DEBUG_ASSERT(((unsigned long)dst&7) == 0);

# ifdef PIXEL_T_IS_8_BITS
  ((uint64_t*)dst)[0] = (uint64_t)v * (uint64_t)0x0101010101010101;
# else // => PIXEL_T_IS_16_BITS
  ((uint64_t*)dst)[0] = ((uint64_t*)dst)[1] = (uint64_t)v * (uint64_t)0x0001000100010001;
# endif
#endif // ARCH_X86_64

}


void pred_intra_8x8_vertical(int dy, pixel_t* dst, unsigned int topleft_is_avail, unsigned int topright_is_avail)
{
  pixel_t* d = dst + dy;
  const pixel_t* src = dst;
  PRED8x8_LOAD_TOP_EDGE;

  dst[0] = t0; dst[1] = t1; dst[2] = t2; dst[3] = t3; dst[4] = t4; dst[5] = t5; dst[6] = t6; dst[7] = t7;
  copy8samples(d, src); d += dy;
  copy8samples(d, src); d += dy;
  copy8samples(d, src); d += dy;
  copy8samples(d, src); d += dy;
  copy8samples(d, src); d += dy;
  copy8samples(d, src); d += dy;
  copy8samples(d, src);
}
void pred_intra_8x8_horizontal(int dy, pixel_t* dst, unsigned int topleft_is_avail)
{
  pixel_t* d = dst;
  const pixel_t* src = dst;
  PRED8x8_LOAD_LEFT_EDGE;

  duplicate1to8samples(d, l0); d += dy;
  duplicate1to8samples(d, l1); d += dy;
  duplicate1to8samples(d, l2); d += dy;
  duplicate1to8samples(d, l3); d += dy;
  duplicate1to8samples(d, l4); d += dy;
  duplicate1to8samples(d, l5); d += dy;
  duplicate1to8samples(d, l6); d += dy;
  duplicate1to8samples(d, l7);
}
// mode: 0=avr of left and top samples, 1: avr of left, 2: avr of top, 3: default value zero
void pred_intra_8x8_dc(int dy, pixel_t* dst, int mode, pixel_t zero, unsigned int topleft_is_avail, unsigned int topright_is_avail)
{
  int dc;
  const pixel_t* src = dst;

  if (mode == 0)
  {
    int a, b;
    PRED8x8_LOAD_LEFT_EDGE;
    PRED8x8_LOAD_TOP_EDGE;
    a =  t0 + t1;
    b =  l0 + l1;
    a += t2 + t3;
    b += l2 + l3;
    a += t4 + t5;
    b += l4 + l5;
    a += t6 + t7;
    b += l6 + l7;
    dc = (a + b + 8) >> 4;
  }
  else if (mode == 1)
  {
    int b;
    PRED8x8_LOAD_LEFT_EDGE;
    b =  l0 + l1;
    b += l2 + l3;
    b += l4 + l5;
    b += l6 + l7;
    dc = (b + 4) >> 3;
  }
  else if (mode == 2)
  {
    int a;
    PRED8x8_LOAD_TOP_EDGE;
    a =  t0 + t1;
    a += t2 + t3;
    a += t4 + t5;
    a += t6 + t7;
    dc = (a + 4) >> 3;
  }
  else // mode = 3
    dc = zero;
  {
    pixel_t* d = dst + dy;
    const pixel_t* src;
    duplicate1to8samples(dst, (pixel_t)dc); src = dst;
    copy8samples(d, src); d += dy;
    copy8samples(d, src); d += dy;
    copy8samples(d, src); d += dy;
    copy8samples(d, src); d += dy;
    copy8samples(d, src); d += dy;
    copy8samples(d, src); d += dy;
    copy8samples(d, src);
  }
}

void pred_intra_8x8_diagonal_down_left(int dy, pixel_t* dst, unsigned int topleft_is_avail, unsigned int topright_is_avail)
{
  const pixel_t* src = dst;
  PRED8x8_LOAD_TOP_EDGE;
  PRED8x8_LOAD_TOPRIGHT_EDGE;

  DST(0,0)= (t0 + 2*t1 + t2 + 2) >> 2;
  DST(0,1)=DST(1,0)= (t1 + 2*t2 + t3 + 2) >> 2;
  DST(0,2)=DST(1,1)=DST(2,0)= (t2 + 2*t3 + t4 + 2) >> 2;
  DST(0,3)=DST(1,2)=DST(2,1)=DST(3,0)= (t3 + 2*t4 + t5 + 2) >> 2;
  DST(0,4)=DST(1,3)=DST(2,2)=DST(3,1)=DST(4,0)= (t4 + 2*t5 + t6 + 2) >> 2;
  DST(0,5)=DST(1,4)=DST(2,3)=DST(3,2)=DST(4,1)=DST(5,0)= (t5 + 2*t6 + t7 + 2) >> 2;
  DST(0,6)=DST(1,5)=DST(2,4)=DST(3,3)=DST(4,2)=DST(5,1)=DST(6,0)= (t6 + 2*t7 + t8 + 2) >> 2;
  DST(0,7)=DST(1,6)=DST(2,5)=DST(3,4)=DST(4,3)=DST(5,2)=DST(6,1)=DST(7,0)= (t7 + 2*t8 + t9 + 2) >> 2;
  DST(1,7)=DST(2,6)=DST(3,5)=DST(4,4)=DST(5,3)=DST(6,2)=DST(7,1)= (t8 + 2*t9 + t10 + 2) >> 2;
  DST(2,7)=DST(3,6)=DST(4,5)=DST(5,4)=DST(6,3)=DST(7,2)= (t9 + 2*t10 + t11 + 2) >> 2;
  DST(3,7)=DST(4,6)=DST(5,5)=DST(6,4)=DST(7,3)= (t10 + 2*t11 + t12 + 2) >> 2;
  DST(4,7)=DST(5,6)=DST(6,5)=DST(7,4)= (t11 + 2*t12 + t13 + 2) >> 2;
  DST(5,7)=DST(6,6)=DST(7,5)= (t12 + 2*t13 + t14 + 2) >> 2;
  DST(6,7)=DST(7,6)= (t13 + 2*t14 + t15 + 2) >> 2;
  DST(7,7)= (t14 + 3*t15 + 2) >> 2;
}
void pred_intra_8x8_diagonal_down_right(int dy, pixel_t* dst, unsigned int topleft_is_avail, unsigned int topright_is_avail)
{
  const pixel_t* src = dst;
  PRED8x8_LOAD_TOP_EDGE;
  PRED8x8_LOAD_LEFT_EDGE;
  PRED8x8_LOAD_TOPLEFT_SAMPLE;

  DST(0,7)= (l7 + 2*l6 + l5 + 2) >> 2;
  DST(0,6)=DST(1,7)= (l6 + 2*l5 + l4 + 2) >> 2;
  DST(0,5)=DST(1,6)=DST(2,7)= (l5 + 2*l4 + l3 + 2) >> 2;
  DST(0,4)=DST(1,5)=DST(2,6)=DST(3,7)= (l4 + 2*l3 + l2 + 2) >> 2;
  DST(0,3)=DST(1,4)=DST(2,5)=DST(3,6)=DST(4,7)= (l3 + 2*l2 + l1 + 2) >> 2;
  DST(0,2)=DST(1,3)=DST(2,4)=DST(3,5)=DST(4,6)=DST(5,7)= (l2 + 2*l1 + l0 + 2) >> 2;
  DST(0,1)=DST(1,2)=DST(2,3)=DST(3,4)=DST(4,5)=DST(5,6)=DST(6,7)= (l1 + 2*l0 + lt + 2) >> 2;
  DST(0,0)=DST(1,1)=DST(2,2)=DST(3,3)=DST(4,4)=DST(5,5)=DST(6,6)=DST(7,7)= (l0 + 2*lt + t0 + 2) >> 2;
  DST(1,0)=DST(2,1)=DST(3,2)=DST(4,3)=DST(5,4)=DST(6,5)=DST(7,6)= (lt + 2*t0 + t1 + 2) >> 2;
  DST(2,0)=DST(3,1)=DST(4,2)=DST(5,3)=DST(6,4)=DST(7,5)= (t0 + 2*t1 + t2 + 2) >> 2;
  DST(3,0)=DST(4,1)=DST(5,2)=DST(6,3)=DST(7,4)= (t1 + 2*t2 + t3 + 2) >> 2;
  DST(4,0)=DST(5,1)=DST(6,2)=DST(7,3)= (t2 + 2*t3 + t4 + 2) >> 2;
  DST(5,0)=DST(6,1)=DST(7,2)= (t3 + 2*t4 + t5 + 2) >> 2;
  DST(6,0)=DST(7,1)= (t4 + 2*t5 + t6 + 2) >> 2;
  DST(7,0)= (t5 + 2*t6 + t7 + 2) >> 2;
}
void pred_intra_8x8_vertical_right(int dy, pixel_t* dst, unsigned int topleft_is_avail, unsigned int topright_is_avail)
{
  const pixel_t* src = dst;
  PRED8x8_LOAD_TOP_EDGE;
  PRED8x8_LOAD_LEFT_EDGE;
  PRED8x8_LOAD_TOPLEFT_SAMPLE;

  DST(0,6)= (l5 + 2*l4 + l3 + 2) >> 2;
  DST(0,7)= (l6 + 2*l5 + l4 + 2) >> 2;
  DST(0,4)=DST(1,6)= (l3 + 2*l2 + l1 + 2) >> 2;
  DST(0,5)=DST(1,7)= (l4 + 2*l3 + l2 + 2) >> 2;
  DST(0,2)=DST(1,4)=DST(2,6)= (l1 + 2*l0 + lt + 2) >> 2;
  DST(0,3)=DST(1,5)=DST(2,7)= (l2 + 2*l1 + l0 + 2) >> 2;
  DST(0,1)=DST(1,3)=DST(2,5)=DST(3,7)= (l0 + 2*lt + t0 + 2) >> 2;
  DST(0,0)=DST(1,2)=DST(2,4)=DST(3,6)= (lt + t0 + 1) >> 1;
  DST(1,1)=DST(2,3)=DST(3,5)=DST(4,7)= (lt + 2*t0 + t1 + 2) >> 2;
  DST(1,0)=DST(2,2)=DST(3,4)=DST(4,6)= (t0 + t1 + 1) >> 1;
  DST(2,1)=DST(3,3)=DST(4,5)=DST(5,7)= (t0 + 2*t1 + t2 + 2) >> 2;
  DST(2,0)=DST(3,2)=DST(4,4)=DST(5,6)= (t1 + t2 + 1) >> 1;
  DST(3,1)=DST(4,3)=DST(5,5)=DST(6,7)= (t1 + 2*t2 + t3 + 2) >> 2;
  DST(3,0)=DST(4,2)=DST(5,4)=DST(6,6)= (t2 + t3 + 1) >> 1;
  DST(4,1)=DST(5,3)=DST(6,5)=DST(7,7)= (t2 + 2*t3 + t4 + 2) >> 2;
  DST(4,0)=DST(5,2)=DST(6,4)=DST(7,6)= (t3 + t4 + 1) >> 1;
  DST(5,1)=DST(6,3)=DST(7,5)= (t3 + 2*t4 + t5 + 2) >> 2;
  DST(5,0)=DST(6,2)=DST(7,4)= (t4 + t5 + 1) >> 1;
  DST(6,1)=DST(7,3)= (t4 + 2*t5 + t6 + 2) >> 2;
  DST(6,0)=DST(7,2)= (t5 + t6 + 1) >> 1;
  DST(7,1)= (t5 + 2*t6 + t7 + 2) >> 2;
  DST(7,0)= (t6 + t7 + 1) >> 1;
}
void pred_intra_8x8_horinzontal_down(int dy, pixel_t* dst, unsigned int topleft_is_avail, unsigned int topright_is_avail)
{
  const pixel_t* src = dst;
  PRED8x8_LOAD_TOP_EDGE;
  PRED8x8_LOAD_LEFT_EDGE;
  PRED8x8_LOAD_TOPLEFT_SAMPLE;

  DST(0,7)= (l6 + l7 + 1) >> 1;
  DST(1,7)= (l5 + 2*l6 + l7 + 2) >> 2;
  DST(0,6)=DST(2,7)= (l5 + l6 + 1) >> 1;
  DST(1,6)=DST(3,7)= (l4 + 2*l5 + l6 + 2) >> 2;
  DST(0,5)=DST(2,6)=DST(4,7)= (l4 + l5 + 1) >> 1;
  DST(1,5)=DST(3,6)=DST(5,7)= (l3 + 2*l4 + l5 + 2) >> 2;
  DST(0,4)=DST(2,5)=DST(4,6)=DST(6,7)= (l3 + l4 + 1) >> 1;
  DST(1,4)=DST(3,5)=DST(5,6)=DST(7,7)= (l2 + 2*l3 + l4 + 2) >> 2;
  DST(0,3)=DST(2,4)=DST(4,5)=DST(6,6)= (l2 + l3 + 1) >> 1;
  DST(1,3)=DST(3,4)=DST(5,5)=DST(7,6)= (l1 + 2*l2 + l3 + 2) >> 2;
  DST(0,2)=DST(2,3)=DST(4,4)=DST(6,5)= (l1 + l2 + 1) >> 1;
  DST(1,2)=DST(3,3)=DST(5,4)=DST(7,5)= (l0 + 2*l1 + l2 + 2) >> 2;
  DST(0,1)=DST(2,2)=DST(4,3)=DST(6,4)= (l0 + l1 + 1) >> 1;
  DST(1,1)=DST(3,2)=DST(5,3)=DST(7,4)= (lt + 2*l0 + l1 + 2) >> 2;
  DST(0,0)=DST(2,1)=DST(4,2)=DST(6,3)= (lt + l0 + 1) >> 1;
  DST(1,0)=DST(3,1)=DST(5,2)=DST(7,3)= (l0 + 2*lt + t0 + 2) >> 2;
  DST(2,0)=DST(4,1)=DST(6,2)= (t1 + 2*t0 + lt + 2) >> 2;
  DST(3,0)=DST(5,1)=DST(7,2)= (t2 + 2*t1 + t0 + 2) >> 2;
  DST(4,0)=DST(6,1)= (t3 + 2*t2 + t1 + 2) >> 2;
  DST(5,0)=DST(7,1)= (t4 + 2*t3 + t2 + 2) >> 2;
  DST(6,0)= (t5 + 2*t4 + t3 + 2) >> 2;
  DST(7,0)= (t6 + 2*t5 + t4 + 2) >> 2;
}
void pred_intra_8x8_vertical_left(int dy, pixel_t* dst, unsigned int topleft_is_avail, unsigned int topright_is_avail)
{
  const pixel_t* src = dst;
  PRED8x8_LOAD_TOP_EDGE;
  PRED8x8_LOAD_TOPRIGHT_EDGE;

  DST(0,0)= (t0 + t1 + 1) >> 1;
  DST(0,1)= (t0 + 2*t1 + t2 + 2) >> 2;
  DST(0,2)=DST(1,0)= (t1 + t2 + 1) >> 1;
  DST(0,3)=DST(1,1)= (t1 + 2*t2 + t3 + 2) >> 2;
  DST(0,4)=DST(1,2)=DST(2,0)= (t2 + t3 + 1) >> 1;
  DST(0,5)=DST(1,3)=DST(2,1)= (t2 + 2*t3 + t4 + 2) >> 2;
  DST(0,6)=DST(1,4)=DST(2,2)=DST(3,0)= (t3 + t4 + 1) >> 1;
  DST(0,7)=DST(1,5)=DST(2,3)=DST(3,1)= (t3 + 2*t4 + t5 + 2) >> 2;
  DST(1,6)=DST(2,4)=DST(3,2)=DST(4,0)= (t4 + t5 + 1) >> 1;
  DST(1,7)=DST(2,5)=DST(3,3)=DST(4,1)= (t4 + 2*t5 + t6 + 2) >> 2;
  DST(2,6)=DST(3,4)=DST(4,2)=DST(5,0)= (t5 + t6 + 1) >> 1;
  DST(2,7)=DST(3,5)=DST(4,3)=DST(5,1)= (t5 + 2*t6 + t7 + 2) >> 2;
  DST(3,6)=DST(4,4)=DST(5,2)=DST(6,0)= (t6 + t7 + 1) >> 1;
  DST(3,7)=DST(4,5)=DST(5,3)=DST(6,1)= (t6 + 2*t7 + t8 + 2) >> 2;
  DST(4,6)=DST(5,4)=DST(6,2)=DST(7,0)= (t7 + t8 + 1) >> 1;
  DST(4,7)=DST(5,5)=DST(6,3)=DST(7,1)= (t7 + 2*t8 + t9 + 2) >> 2;
  DST(5,6)=DST(6,4)=DST(7,2)= (t8 + t9 + 1) >> 1;
  DST(5,7)=DST(6,5)=DST(7,3)= (t8 + 2*t9 + t10 + 2) >> 2;
  DST(6,6)=DST(7,4)= (t9 + t10 + 1) >> 1;
  DST(6,7)=DST(7,5)= (t9 + 2*t10 + t11 + 2) >> 2;
  DST(7,6)= (t10 + t11 + 1) >> 1;
  DST(7,7)= (t10 + 2*t11 + t12 + 2) >> 2;
}
void pred_intra_8x8_horizontal_up(int dy, pixel_t* dst, unsigned int topleft_is_avail)
{
  const pixel_t* src = dst;
  PRED8x8_LOAD_LEFT_EDGE;
  DST(0,0)= (l0 + l1 + 1) >> 1;
  DST(1,0)= (l0 + 2*l1 + l2 + 2) >> 2;
  DST(0,1)=DST(2,0)= (l1 + l2 + 1) >> 1;
  DST(1,1)=DST(3,0)= (l1 + 2*l2 + l3 + 2) >> 2;
  DST(0,2)=DST(2,1)=DST(4,0)= (l2 + l3 + 1) >> 1;
  DST(1,2)=DST(3,1)=DST(5,0)= (l2 + 2*l3 + l4 + 2) >> 2;
  DST(0,3)=DST(2,2)=DST(4,1)=DST(6,0)= (l3 + l4 + 1) >> 1;
  DST(1,3)=DST(3,2)=DST(5,1)=DST(7,0)= (l3 + 2*l4 + l5 + 2) >> 2;
  DST(0,4)=DST(2,3)=DST(4,2)=DST(6,1)= (l4 + l5 + 1) >> 1;
  DST(1,4)=DST(3,3)=DST(5,2)=DST(7,1)= (l4 + 2*l5 + l6 + 2) >> 2;
  DST(0,5)=DST(2,4)=DST(4,3)=DST(6,2)= (l5 + l6 + 1) >> 1;
  DST(1,5)=DST(3,4)=DST(5,3)=DST(7,2)= (l5 + 2*l6 + l7 + 2) >> 2;
  DST(0,6)=DST(2,5)=DST(4,4)=DST(6,3)= (l6 + l7 + 1) >> 1;
  DST(1,6)=DST(3,5)=DST(5,4)=DST(7,3)= (l6 + 3*l7 + 2) >> 2;
  DST(0,7)=DST(1,7)=DST(2,6)=DST(2,7)=DST(3,6)=
      DST(3,7)=DST(4,5)=DST(4,6)=DST(4,7)=DST(5,5)=
      DST(5,6)=DST(5,7)=DST(6,4)=DST(6,5)=DST(6,6)=
      DST(6,7)=DST(7,4)=DST(7,5)=DST(7,6)=DST(7,7)= l7;
}



/*
 * Intra 16x16 prediction
 */
void copy16samples(pixel_t* dst, const pixel_t* src)
{
  LUD_DEBUG_ASSERT(((unsigned long)dst&7) == 0 && ((unsigned long)src&7) == 0);
#ifdef PIXEL_T_IS_8_BITS
  *((uint64_t*)dst) = ((uint64_t*)src)[0]; // could be unaligned 32/64 bits copy
  *((uint64_t*)dst+1) = ((uint64_t*)src)[1]; // could be unaligned 32/64 bits copy
#else // => PIXEL_T_IS_16_BITS
  ((uint64_t*)dst)[0] = ((uint64_t*)src)[0]; // could be unaligned 32/64 bits copy
  ((uint64_t*)dst)[1] = ((uint64_t*)src)[1]; // could be unaligned 32/64 bits copy
  ((uint64_t*)dst)[2] = ((uint64_t*)src)[2]; // could be unaligned 32/64 bits copy
  ((uint64_t*)dst)[3] = ((uint64_t*)src)[3]; // could be unaligned 32/64 bits copy
#endif
}
void duplicate1to16samples(pixel_t* dst, const pixel_t v)
{

#ifndef ARCH_X86_64
  LUD_DEBUG_ASSERT(((unsigned long)dst&3) == 0);
# ifdef PIXEL_T_IS_8_BITS
  ((uint32_t*)dst)[0] = ((uint32_t*)dst)[1] = ((uint32_t*)dst)[2] = ((uint32_t*)dst)[3] = (uint32_t)v * (uint32_t)0x01010101;
# else // => PIXEL_T_IS_16_BITS
  ((uint32_t*)dst)[0] = ((uint32_t*)dst)[1] = ((uint32_t*)dst)[2] = ((uint32_t*)dst)[3] =
      ((uint32_t*)dst)[4] = ((uint32_t*)dst)[5] = ((uint32_t*)dst)[6] = ((uint32_t*)dst)[7] = (uint32_t)v * (uint32_t)0x00010001;
# endif

#else // ARCH_X86_64

# ifdef PIXEL_T_IS_8_BITS
  LUD_DEBUG_ASSERT(((unsigned long)dst&7) == 0);
  ((uint64_t*)dst)[0] = ((uint64_t*)dst)[1] = (uint64_t)v * (uint64_t)0x0101010101010101;
# else // => PIXEL_T_IS_16_BITS
  ((uint64_t*)dst)[0] = ((uint64_t*)dst)[1] = ((uint64_t*)dst)[2] = ((uint64_t*)dst)[3] = (uint64_t)v * (uint64_t)0x0001000100010001;
# endif

#endif // ARCH_X86_64

}


void pred_intra_16x16_vertical(int dy, pixel_t* dst)
{
  pixel_t* d = dst;
  const pixel_t* src = dst-dy;

  copy16samples(d, src); d += dy;
  copy16samples(d, src); d += dy;
  copy16samples(d, src); d += dy;
  copy16samples(d, src); d += dy;
  copy16samples(d, src); d += dy;
  copy16samples(d, src); d += dy;
  copy16samples(d, src); d += dy;
  copy16samples(d, src); d += dy;
  copy16samples(d, src); d += dy;
  copy16samples(d, src); d += dy;
  copy16samples(d, src); d += dy;
  copy16samples(d, src); d += dy;
  copy16samples(d, src); d += dy;
  copy16samples(d, src); d += dy;
  copy16samples(d, src); d += dy;
  copy16samples(d, src);
}
void pred_intra_16x16_horizontal(int dy, pixel_t* dst)
{
  const pixel_t* s = dst-1;
  pixel_t* d = dst;
  duplicate1to16samples(d, *s); s += dy; d += dy;
  duplicate1to16samples(d, *s); s += dy; d += dy;
  duplicate1to16samples(d, *s); s += dy; d += dy;
  duplicate1to16samples(d, *s); s += dy; d += dy;
  duplicate1to16samples(d, *s); s += dy; d += dy;
  duplicate1to16samples(d, *s); s += dy; d += dy;
  duplicate1to16samples(d, *s); s += dy; d += dy;
  duplicate1to16samples(d, *s); s += dy; d += dy;
  duplicate1to16samples(d, *s); s += dy; d += dy;
  duplicate1to16samples(d, *s); s += dy; d += dy;
  duplicate1to16samples(d, *s); s += dy; d += dy;
  duplicate1to16samples(d, *s); s += dy; d += dy;
  duplicate1to16samples(d, *s); s += dy; d += dy;
  duplicate1to16samples(d, *s); s += dy; d += dy;
  duplicate1to16samples(d, *s); s += dy; d += dy;
  duplicate1to16samples(d, *s);
}

// mode: 0=avr of left and top samples, 1: avr of left, 2: avr of top, 3: default value zero
void pred_intra_16x16_dc(int dy, pixel_t* dst, int mode, pixel_t zero)
{
  int dc;
  if (mode == 0)
  {
    int a, b;
    const pixel_t* sa = dst-dy;
    const pixel_t* sb = dst-1;
    a =  *sa++; b =  *sb; sb += dy;
    a += *sa++; b += *sb; sb += dy;
    a += *sa++; b += *sb; sb += dy;
    a += *sa++; b += *sb; sb += dy;
    a += *sa++; b += *sb; sb += dy;
    a += *sa++; b += *sb; sb += dy;
    a += *sa++; b += *sb; sb += dy;
    a += *sa++; b += *sb; sb += dy;
    a += *sa++; b += *sb; sb += dy;
    a += *sa++; b += *sb; sb += dy;
    a += *sa++; b += *sb; sb += dy;
    a += *sa++; b += *sb; sb += dy;
    a += *sa++; b += *sb; sb += dy;
    a += *sa++; b += *sb; sb += dy;
    a += *sa++; b += *sb; sb += dy;
    a += *sa;   b += *sb;
    dc = (a + b + 16) >> 5;
  }
  else if (mode == 1)
  {
    int b;
    const pixel_t* sb = dst-1;
    b =  *sb; sb += dy;
    b += *sb; sb += dy;
    b += *sb; sb += dy;
    b += *sb; sb += dy;
    b += *sb; sb += dy;
    b += *sb; sb += dy;
    b += *sb; sb += dy;
    b += *sb; sb += dy;
    b += *sb; sb += dy;
    b += *sb; sb += dy;
    b += *sb; sb += dy;
    b += *sb; sb += dy;
    b += *sb; sb += dy;
    b += *sb; sb += dy;
    b += *sb; sb += dy;
    b += *sb;
    dc = (b + 8) >> 4;
  }
  else if (mode == 2)
  {
    int a;
    const pixel_t* sa = dst-dy;
    a =  *sa++;
    a += *sa++;
    a += *sa++;
    a += *sa++;
    a += *sa++;
    a += *sa++;
    a += *sa++;
    a += *sa++;
    a += *sa++;
    a += *sa++;
    a += *sa++;
    a += *sa++;
    a += *sa++;
    a += *sa++;
    a += *sa++;
    a += *sa;
    dc = (a + 8) >> 4;
  }
  else // mode = 3
    dc = zero;
  {
    pixel_t* d = dst + dy;
    const pixel_t* src = dst;
    duplicate1to16samples(dst, (pixel_t)dc);
    //duplicate1to16samples(dst, (pixel_t)1);
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src);
  }
}

void pred_intra_16x16_plane(int dy, pixel_t* dst, int max)
{
  const pixel_t* src = dst;
  int a, b, c;
  int H, V;
  int y;
  int constant;

  H =  SRC(8, -1) - SRC(6, -1);
  V =  SRC(-1, 8) - SRC(-1, 6);
  H += 2*(SRC(9, -1)  - SRC(5, -1) );
  V += 2*(SRC(-1, 9)  - SRC(-1, 5) );
  H += 3*(SRC(10, -1) - SRC(4, -1) );
  V += 3*(SRC(-1, 10) - SRC(-1, 4) );
  H += 4*(SRC(11, -1) - SRC(3, -1) );
  V += 4*(SRC(-1, 11) - SRC(-1, 3) );
  H += 5*(SRC(12, -1) - SRC(2, -1) );
  V += 5*(SRC(-1, 12) - SRC(-1, 2) );
  H += 6*(SRC(13, -1) - SRC(1, -1) );
  V += 6*(SRC(-1, 13) - SRC(-1, 1) );
  H += 7*(SRC(14, -1) - SRC(0, -1) );
  V += 7*(SRC(-1, 14) - SRC(-1, 0) );
  H += 8*(SRC(15, -1) - SRC(-1, -1));
  V += 8*(SRC(-1, 15) - SRC(-1, -1));


  a = 16 * (SRC(-1, 15) + SRC(15, -1));
  b = (5 * H + 32) >> 6;
  c = (5 * V + 32) >> 6;

  constant = a + (c+b)*-7 + 16;
  for (y=0; y<16 ; y++)
  {
    int v = constant;
    DST(0, y)  = im_clip(0, max, (v)>>5); v+= b;
    DST(1, y)  = im_clip(0, max, (v)>>5); v+= b;
    DST(2, y)  = im_clip(0, max, (v)>>5); v+= b;
    DST(3, y)  = im_clip(0, max, (v)>>5); v+= b;
    DST(4, y)  = im_clip(0, max, (v)>>5); v+= b;
    DST(5, y)  = im_clip(0, max, (v)>>5); v+= b;
    DST(6, y)  = im_clip(0, max, (v)>>5); v+= b;
    DST(7, y)  = im_clip(0, max, (v)>>5); v+= b;
    DST(8, y)  = im_clip(0, max, (v)>>5); v+= b;
    DST(9, y)  = im_clip(0, max, (v)>>5); v+= b;
    DST(10, y) = im_clip(0, max, (v)>>5); v+= b;
    DST(11, y) = im_clip(0, max, (v)>>5); v+= b;
    DST(12, y) = im_clip(0, max, (v)>>5); v+= b;
    DST(13, y) = im_clip(0, max, (v)>>5); v+= b;
    DST(14, y) = im_clip(0, max, (v)>>5); v+= b;
    DST(15, y) = im_clip(0, max, (v)>>5);
    constant += c;
  }

}


/*
 * Intra Chroma pred
 */
int add4vertsamples(const pixel_t* src, const int dy)
{
  int a = src[0+0*dy] + src[0+1*dy];
  int b = src[0+2*dy] + src[0+3*dy];
  return a+b;
}
int add4horizsamples(const pixel_t* src)
{
  int a = src[0] + src[1];
  int b = src[2] + src[3];
  return a+b;
}
void fill_4x4_block(pixel_t* dst, const int dy, const pixel_t v)
{
  pixel_t* d = dst + dy;
  const pixel_t* src;
  duplicate1to4samples(dst, v); src = dst;
  copy4samples(d, src); d += dy;
  copy4samples(d, src); d += dy;
  copy4samples(d, src);
}
void fill_8x4_block(pixel_t* dst, const int dy, const pixel_t v)
{
  pixel_t* d = dst + dy;
  const pixel_t* src;
  duplicate1to8samples(dst, v); src = dst;
  copy8samples(d, src); d += dy;
  copy8samples(d, src); d += dy;
  copy8samples(d, src);
}
void fill_8x8_block(pixel_t* dst, const int dy, const pixel_t v)
{
  pixel_t* d = dst + dy;
  const pixel_t* src;
  duplicate1to8samples(dst, v); src = dst;
  copy8samples(d, src); d += dy;
  copy8samples(d, src); d += dy;
  copy8samples(d, src); d += dy;
  copy8samples(d, src); d += dy;
  copy8samples(d, src); d += dy;
  copy8samples(d, src); d += dy;
  copy8samples(d, src);
}
void fill_4x8_block(pixel_t* dst, const int dy, const pixel_t v)
{
  pixel_t* d = dst + dy;
  const pixel_t* src;
  duplicate1to4samples(dst, v); src = dst;
  copy4samples(d, src); d += dy;
  copy4samples(d, src); d += dy;
  copy4samples(d, src); d += dy;
  copy4samples(d, src); d += dy;
  copy4samples(d, src); d += dy;
  copy4samples(d, src); d += dy;
  copy4samples(d, src);
}
void fill_4x16_block(pixel_t* dst, const int dy, const pixel_t v)
{
  pixel_t* d = dst + dy;
  const pixel_t* src;
  duplicate1to4samples(dst, v); src = dst;
  copy4samples(d, src); d += dy;
  copy4samples(d, src); d += dy;
  copy4samples(d, src); d += dy;
  copy4samples(d, src); d += dy;
  copy4samples(d, src); d += dy;
  copy4samples(d, src); d += dy;
  copy4samples(d, src); d += dy;
  copy4samples(d, src); d += dy;
  copy4samples(d, src); d += dy;
  copy4samples(d, src); d += dy;
  copy4samples(d, src); d += dy;
  copy4samples(d, src); d += dy;
  copy4samples(d, src); d += dy;
  copy4samples(d, src); d += dy;
  copy4samples(d, src);
}
void fill_16x8_block(pixel_t* dst, const int dy, const pixel_t v)
{
  pixel_t* d = dst + dy;
  const pixel_t* src;
  duplicate1to16samples(dst, v); src = dst;
  copy16samples(d, src); d += dy;
  copy16samples(d, src); d += dy;
  copy16samples(d, src); d += dy;
  copy16samples(d, src); d += dy;
  copy16samples(d, src); d += dy;
  copy16samples(d, src); d += dy;
  copy16samples(d, src);
}


#define LOAD_TOP_4SAMPLES_SUM(idx) \
  const int ts##idx = add4horizsamples((src)-(dy)+4*(idx));
#define LOAD_LEFT_4SAMPLES_SUM(idx) \
  const int ls##idx = add4vertsamples((src)-1+4*(idx)*(dy), dy);

void pred_intra_chroma_dc_full(int cfidc, int dy, pixel_t* dst, unsigned int left_is_avail, unsigned int top_is_avail, pixel_t zero)
{
  const pixel_t* src = dst;
  pixel_t* d = dst;
  int mode = 0;

  if (!top_is_avail) // top not available
    mode += 1;
  if (!left_is_avail) // left not available
    mode += 2;

  if (mode == 0) // Both left and top edge are available
  {
    LOAD_TOP_4SAMPLES_SUM(0);
    LOAD_TOP_4SAMPLES_SUM(1);
    LOAD_LEFT_4SAMPLES_SUM(0);
    LOAD_LEFT_4SAMPLES_SUM(1);

    fill_4x4_block(d, dy, (ts0 + ls0 + 4) >> 3);
    fill_4x4_block(d+4, dy, (ts1 + 2) >> 2);
    d += 4*dy;
    fill_4x4_block(d, dy, (ls1 + 2) >> 2);
    fill_4x4_block(d+4, dy, (ts1 + ls1 + 4) >> 3);

    if (cfidc >= 2)
    {
      LOAD_LEFT_4SAMPLES_SUM(2);
      LOAD_LEFT_4SAMPLES_SUM(3);

      d += 4*dy;
      fill_4x4_block(d, dy, (ls2 + 2) >> 2);
      fill_4x4_block(d+4, dy, (ts1 + ls2 + 4) >> 3);
      d += 4*dy;
      fill_4x4_block(d, dy, (ls3 + 2) >> 2);
      fill_4x4_block(d+4, dy, (ts1 + ls3 + 4) >> 3);

      if (cfidc == 3)
      {
        LOAD_TOP_4SAMPLES_SUM(2);
        LOAD_TOP_4SAMPLES_SUM(3);

        d = dst + 8;
        fill_4x4_block(d, dy, (ts2 + 2) >> 2);
        fill_4x4_block(d+4, dy, (ts3 + 2) >> 2);
        d += 4*dy;
        fill_4x4_block(d, dy, (ts2 + ls1 + 4) >> 3);
        fill_4x4_block(d+4, dy, (ts3 + ls1 + 4) >> 3);
        d += 4*dy;
        fill_4x4_block(d, dy, (ts2 + ls2 + 4) >> 3);
        fill_4x4_block(d+4, dy, (ts3 + ls2 + 4) >> 3);
        d += 4*dy;
        fill_4x4_block(d, dy, (ts2 + ls3 + 4) >> 3);
        fill_4x4_block(d+4, dy, (ts3 + ls3 + 4) >> 3);
      }
    }
  }
  else if (mode == 1) // Only left edge is available
  {
    LOAD_LEFT_4SAMPLES_SUM(0);
    LOAD_LEFT_4SAMPLES_SUM(1);
    const int dc0 = (ls0 + 2) >> 2;
    const int dc1 = (ls1 + 2) >> 2;

    fill_8x4_block(d, dy, dc0);
    d += 4*dy;
    fill_8x4_block(d, dy, dc1);

    if (cfidc >= 2)
    {
      LOAD_LEFT_4SAMPLES_SUM(2);
      LOAD_LEFT_4SAMPLES_SUM(3);
      const int dc2 = (ls2 + 2) >> 2;
      const int dc3 = (ls3 + 2) >> 2;

      d += 4*dy;
      fill_8x4_block(d, dy, dc2);
      d += 4*dy;
      fill_8x4_block(d, dy, dc3);

      if (cfidc == 3)
      {
        d = dst + 8;
        fill_8x4_block(d, dy, dc0);
        d += 4*dy;
        fill_8x4_block(d, dy, dc1);
        d += 4*dy;
        fill_8x4_block(d, dy, dc2);
        d += 4*dy;
        fill_8x4_block(d, dy, dc3);
      }
    }
  }
  else if (mode == 2) // Only top edge is available
  {
    LOAD_TOP_4SAMPLES_SUM(0);
    LOAD_TOP_4SAMPLES_SUM(1);
    const int dc0 = (ts0 + 2) >> 2;
    const int dc1 = (ts1 + 2) >> 2;

    fill_4x8_block(d, dy, dc0);
    d += 4;
    fill_4x8_block(d, dy, dc1);

    if (cfidc >= 2)
    {
      d = dst + 8*dy;
      fill_4x8_block(d, dy, dc0);
      d += 4;
      fill_4x8_block(d, dy, dc1);

      if (cfidc == 3)
      {
        LOAD_TOP_4SAMPLES_SUM(2);
        LOAD_TOP_4SAMPLES_SUM(3);
        const int dc2 = (ts2 + 2) >> 2;
        const int dc3 = (ts3 + 2) >> 2;

        d = dst + 8;
        fill_4x16_block(d, dy, dc2);
        d += 4;
        fill_4x16_block(d, dy, dc3);
      }
    }
  }
  else // mode = 3 // no edge is available
  {
    pixel_t* d = dst + dy;
    if (cfidc <= 2)
    {
      duplicate1to8samples(dst, zero); src = dst;
      copy8samples(d, src); d += dy;
      copy8samples(d, src); d += dy;
      copy8samples(d, src); d += dy;
      copy8samples(d, src); d += dy;
      copy8samples(d, src); d += dy;
      copy8samples(d, src); d += dy;
      copy8samples(d, src);
      if (cfidc == 2)
      {
        d += dy;
        copy8samples(d, src); d += dy;
        copy8samples(d, src); d += dy;
        copy8samples(d, src); d += dy;
        copy8samples(d, src); d += dy;
        copy8samples(d, src); d += dy;
        copy8samples(d, src); d += dy;
        copy8samples(d, src); d += dy;
        copy8samples(d, src);
      }
    }

    else // cfidc == 3
    {
      LUD_DEBUG_ASSERT(cfidc==3);
      duplicate1to16samples(dst, zero); src = dst;
      copy16samples(d, src); d += dy;
      copy16samples(d, src); d += dy;
      copy16samples(d, src); d += dy;
      copy16samples(d, src); d += dy;
      copy16samples(d, src); d += dy;
      copy16samples(d, src); d += dy;
      copy16samples(d, src); d += dy;
      copy16samples(d, src); d += dy;
      copy16samples(d, src); d += dy;
      copy16samples(d, src); d += dy;
      copy16samples(d, src); d += dy;
      copy16samples(d, src); d += dy;
      copy16samples(d, src); d += dy;
      copy16samples(d, src); d += dy;
      copy16samples(d, src);
    }
  }
}

/*
void pred_intra_chroma_dc_cfidc1(unsigned int cfidc, int dy, pixel_t* dst, unsigned int left_is_avail_top, unsigned int left_is_avail_bot,
    unsigned int top_is_avail, pixel_t zero)
{
  const pixel_t* src = dst;

  LOAD_TOP_4SAMPLES_SUM(0);
  LOAD_TOP_4SAMPLES_SUM(1);
  LOAD_LEFT_4SAMPLES_SUM(0);
  LOAD_LEFT_4SAMPLES_SUM(1);

  if (top_is_avail)
  {
    if (left_is_avail_top)
      fill_4x4_block(dst, dy, (ts0+ls0+4)>>3); // B0
    else
      fill_4x4_block(dst, dy, (ts0+2)>>2); //B0

    fill_4x4_block(dst+4, dy, (ts1+2)>>2); //B1

    if (left_is_avail_bot)
    {
      fill_4x4_block(dst+4*dy, dy, (ls1+2)>>2); //B2
      fill_4x4_block(dst+4*dy+4, dy, (ts1+ls1+4)>>3); //B3
    }
    else
    {
      fill_4x4_block(dst+4*dy, dy, (ts0+2)>>2); //B2
      fill_4x4_block(dst+4*dy+4, dy, (ts1+2)>>2); //B3
    }
  }
  else // top_is_avail == 0
  {
    if (left_is_avail_top)
    {
      fill_4x4_block(dst, dy, (ls0+2)>>2); //B0
      fill_4x4_block(dst+4, dy, (ls0+2)>>2); //B1
    }
    else
    {
      fill_4x4_block(dst, dy, zero); //B0
      fill_4x4_block(dst+4, dy, zero); //B1
    }

    if (left_is_avail_bot)
    {
      fill_4x4_block(dst+4*dy, dy, (ls1+2)>>2); //B2
      fill_4x4_block(dst+4*dy+4, dy, (ls1+2)>>2); //B3
    }
    else
    {
      fill_4x4_block(dst+4*dy, dy, zero); //B2
      fill_4x4_block(dst+4*dy+4, dy, zero); //B3
    }
  }
}

*/
void pred_intra_chroma_dc(unsigned int cfidc, int dy, pixel_t* dst_, unsigned int left_is_avail_top, unsigned int left_is_avail_bot,
    unsigned int top_is_avail, pixel_t zero)
{
  pixel_t* dst = dst_;

  unsigned int ts[4];
  unsigned int ls[2];

  ts[0] = add4horizsamples(dst-dy);
  ts[1] = add4horizsamples(dst-dy+4);
  ls[0] = add4vertsamples(dst-1, dy);
  if (cfidc>1)
    ls[1] = add4vertsamples(dst-1+4*dy, dy);
  else
    ls[1] = 0;

  if (cfidc==3)
  {
    ts[2] = add4horizsamples(dst-dy+8);
    ts[3] = add4horizsamples(dst-dy+12);
  } else
    ts[2] = ts[3] = 0;

  if (top_is_avail && left_is_avail_top)
  {
    fill_4x4_block(dst, dy, (ts[0]+ls[0]+4)>>3);
    fill_4x4_block(dst+4, dy, (ts[1]+2)>>2);
    if (cfidc > 1)
    {
      fill_4x4_block(dst+4*dy, dy, (ls[1]+2)>>2);
      fill_4x4_block(dst+4*dy+4, dy, (ts[1]+ls[1]+4)>>3);
    }
    if (cfidc == 3)
    {
      fill_4x4_block(dst+8, dy, (ts[2]+2)>>2);
      fill_4x4_block(dst+12, dy, (ts[3]+2)>>2);
      fill_4x4_block(dst+4*dy+8, dy, (ls[1]+ts[2]+4)>>3);
      fill_4x4_block(dst+4*dy+12, dy, (ls[1]+ts[3]+4)>>3);
    }
  }
  else if (top_is_avail)
  {
    fill_4x4_block(dst, dy, (ts[0]+2)>>2);
    fill_4x4_block(dst+4, dy, (ts[1]+2)>>2);
    if (cfidc > 1)
    {
      fill_4x4_block(dst+4*dy, dy, (ts[0]+2)>>2);
      fill_4x4_block(dst+4*dy+4, dy, (ts[1]+2)>>2);
    }
    if (cfidc == 3)
    {
      fill_4x8_block(dst+8, dy, (ts[2]+2)>>2);
      fill_4x8_block(dst+12, dy, (ts[3]+2)>>2);
    }
  }
  else if (left_is_avail_top)
  {
    fill_8x4_block(dst, dy, (ls[0]+2)>>2);
    if (cfidc > 1)
    {
      fill_8x4_block(dst+4*dy, dy, (ls[1]+2)>>2);
    }
    if (cfidc == 3)
    {
      fill_8x4_block(dst+8, dy, (ls[0]+2)>>2);
      fill_8x4_block(dst+4*dy+8, dy, (ls[1]+2)>>2);
    }
  }
  else
  {
    if (cfidc==1)
      fill_8x4_block(dst, dy, zero);
    else if (cfidc == 2)
      fill_8x8_block(dst, dy, zero);
    else // cfidc == 3
      fill_16x8_block(dst, dy, zero);
  }


  dst = cfidc == 1 ? dst_+4*dy : dst_+8*dy;

  ls[0] = add4vertsamples(dst-1, dy);
  if (cfidc>1)
    ls[1] = add4vertsamples(dst-1+4*dy, dy);

  if (top_is_avail && left_is_avail_bot)
  {
    fill_4x4_block(dst, dy, (ls[0]+2)>>2);
    fill_4x4_block(dst+4, dy, (ts[1]+ls[0]+4)>>3);
    if (cfidc > 1)
    {
      fill_4x4_block(dst+4*dy, dy, (ls[1]+2)>>2);
      fill_4x4_block(dst+4*dy+4, dy, (ts[1]+ls[1]+4)>>3);
    }
    if (cfidc == 3)
    {
      fill_4x4_block(dst+8, dy, (ls[0]+ts[2]+4)>>3);
      fill_4x4_block(dst+12, dy, (ls[0]+ts[2]+4)>>3);
      fill_4x4_block(dst+4*dy+8, dy, (ls[1]+ts[2]+4)>>3);
      fill_4x4_block(dst+4*dy+12, dy, (ls[1]+ts[3]+4)>>3);
    }
  }
  else if (top_is_avail)
  {
    fill_4x4_block(dst, dy, (ts[0]+2)>>2);
    fill_4x4_block(dst+4, dy, (ts[1]+2)>>2);
    if (cfidc > 1)
    {
      fill_4x4_block(dst+4*dy, dy, (ts[0]+2)>>2);
      fill_4x4_block(dst+4*dy+4, dy, (ts[1]+2)>>2);
    }
    if (cfidc == 3)
    {
      fill_4x8_block(dst+8, dy, (ts[2]+2)>>2);
      fill_4x8_block(dst+12, dy, (ts[3]+2)>>2);
    }
  }
  else if (left_is_avail_bot)
  {
    fill_8x4_block(dst, dy, (ls[0]+2)>>2);
    if (cfidc > 1)
    {
      fill_8x4_block(dst+4*dy, dy, (ls[1]+2)>>2);
    }
    if (cfidc == 3)
    {
      fill_8x4_block(dst+8, dy, (ls[0]+2)>>2);
      fill_8x4_block(dst+4*dy+8, dy, (ls[1]+2)>>2);
    }
  }
  else
  {
    if (cfidc==1)
      fill_8x4_block(dst, dy, zero);
    else if (cfidc == 2)
      fill_8x8_block(dst, dy, zero);
    else // cfidc == 3
      fill_16x8_block(dst, dy, zero);
  }
}



void pred_intra_chroma_horizontal(int cfidc, int dy, pixel_t* dst)
{
  const pixel_t* s = dst-1;
  pixel_t* d = dst;

  if (cfidc <= 2 )
  {
    duplicate1to8samples(d, *s); s += dy; d += dy;
    duplicate1to8samples(d, *s); s += dy; d += dy;
    duplicate1to8samples(d, *s); s += dy; d += dy;
    duplicate1to8samples(d, *s); s += dy; d += dy;
    duplicate1to8samples(d, *s); s += dy; d += dy;
    duplicate1to8samples(d, *s); s += dy; d += dy;
    duplicate1to8samples(d, *s); s += dy; d += dy;
    duplicate1to8samples(d, *s);
    if (cfidc == 2)
    {
      s += dy; d += dy;
      duplicate1to8samples(d, *s); s += dy; d += dy;
      duplicate1to8samples(d, *s); s += dy; d += dy;
      duplicate1to8samples(d, *s); s += dy; d += dy;
      duplicate1to8samples(d, *s); s += dy; d += dy;
      duplicate1to8samples(d, *s); s += dy; d += dy;
      duplicate1to8samples(d, *s); s += dy; d += dy;
      duplicate1to8samples(d, *s); s += dy; d += dy;
      duplicate1to8samples(d, *s);
    }
  }
  else // cfidc == 3
  {
    LUD_DEBUG_ASSERT(cfidc==3);
    duplicate1to16samples(d, *s); s += dy; d += dy;
    duplicate1to16samples(d, *s); s += dy; d += dy;
    duplicate1to16samples(d, *s); s += dy; d += dy;
    duplicate1to16samples(d, *s); s += dy; d += dy;
    duplicate1to16samples(d, *s); s += dy; d += dy;
    duplicate1to16samples(d, *s); s += dy; d += dy;
    duplicate1to16samples(d, *s); s += dy; d += dy;
    duplicate1to16samples(d, *s); s += dy; d += dy;
    duplicate1to16samples(d, *s); s += dy; d += dy;
    duplicate1to16samples(d, *s); s += dy; d += dy;
    duplicate1to16samples(d, *s); s += dy; d += dy;
    duplicate1to16samples(d, *s); s += dy; d += dy;
    duplicate1to16samples(d, *s); s += dy; d += dy;
    duplicate1to16samples(d, *s); s += dy; d += dy;
    duplicate1to16samples(d, *s); s += dy; d += dy;
    duplicate1to16samples(d, *s);
  }

}

void pred_intra_chroma_vertical(int cfidc, int dy, pixel_t* dst)
{
  pixel_t* d = dst;
  const pixel_t* src = dst-dy;

  if (cfidc <= 2 )
  {
    copy8samples(d, src); d += dy;
    copy8samples(d, src); d += dy;
    copy8samples(d, src); d += dy;
    copy8samples(d, src); d += dy;
    copy8samples(d, src); d += dy;
    copy8samples(d, src); d += dy;
    copy8samples(d, src); d += dy;
    copy8samples(d, src);

    if (cfidc == 2)
    {
      d += dy;
      copy8samples(d, src); d += dy;
      copy8samples(d, src); d += dy;
      copy8samples(d, src); d += dy;
      copy8samples(d, src); d += dy;
      copy8samples(d, src); d += dy;
      copy8samples(d, src); d += dy;
      copy8samples(d, src); d += dy;
      copy8samples(d, src);
    }
  }
  else // cfidc == 3
  {
    LUD_DEBUG_ASSERT(cfidc==3);
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src); d += dy;
    copy16samples(d, src);
  }
}




void pred_intra_chroma_plane(int cfidc, int dy, pixel_t* dst, int max)
{
  const pixel_t* src = dst;
  int a, b, c;
  int H=0, V=0; // initialization not necessary, but prevent a warning...
  int y;
  int constant;
  int height;
  int width;

  if (cfidc <= 2)
  {
    width = 7;
    H =     SRC(4, -1) - SRC(2, -1);
    H += 2*(SRC(5, -1) - SRC(1, -1));
    H += 3*(SRC(6, -1) - SRC(0, -1));
    H += 4*(SRC(7, -1) - SRC(-1, -1));

    if (cfidc == 1)
    {
      height = 7;
      V =     SRC(-1, 4) - SRC(-1, 2);
      V += 2*(SRC(-1, 5) - SRC(-1, 1));
      V += 3*(SRC(-1, 6) - SRC(-1, 0));
      V += 4*(SRC(-1, 7) - SRC(-1, -1));
    }
  }

  if (cfidc >= 2)
  {
    height = 15;
    if (cfidc == 3)
    {
      width = 15;
      H =  SRC(8, -1) - SRC(6, -1);
      H += 2*(SRC(9, -1)  - SRC(5, -1) );
      H += 3*(SRC(10, -1) - SRC(4, -1) );
      H += 4*(SRC(11, -1) - SRC(3, -1) );
      H += 5*(SRC(12, -1) - SRC(2, -1) );
      H += 6*(SRC(13, -1) - SRC(1, -1) );
      H += 7*(SRC(14, -1) - SRC(0, -1) );
      H += 8*(SRC(15, -1) - SRC(-1, -1));
    }

    V =  SRC(-1, 8) - SRC(-1, 6);
    V += 2*(SRC(-1, 9)  - SRC(-1, 5) );
    V += 3*(SRC(-1, 10) - SRC(-1, 4) );
    V += 4*(SRC(-1, 11) - SRC(-1, 3) );
    V += 5*(SRC(-1, 12) - SRC(-1, 2) );
    V += 6*(SRC(-1, 13) - SRC(-1, 1) );
    V += 7*(SRC(-1, 14) - SRC(-1, 0) );
    V += 8*(SRC(-1, 15) - SRC(-1, -1));
  }


  a = 16 * (SRC(-1, height) + SRC(width, -1));
  b = ((34-29*(cfidc == 3)) * H + 32) >> 6;
  c = ((34-29*(cfidc != 1)) * V + 32) >> 6;

  if (cfidc == 1)
  {
    constant = a + (c+b)*-3 + 16;
    for (y=0; y<8 ; y++)
    {
      int v = constant;
      DST(0, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(1, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(2, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(3, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(4, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(5, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(6, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(7, y)  = im_clip(0, max, (v)>>5);
      constant += c;
    }
  }
  else if (cfidc == 2)
  {
    constant = a + c*-7 + b*-3 + 16;
    for (y=0; y<16 ; y++)
    {
      int v = constant;
      DST(0, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(1, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(2, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(3, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(4, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(5, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(6, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(7, y)  = im_clip(0, max, (v)>>5);
      constant += c;
    }
  }
  else // cfidc == 3
  {
    LUD_DEBUG_ASSERT(cfidc==3);
    constant = a + (c+b)*-7 + 16;
    for (y=0; y<16 ; y++)
    {
      int v = constant;
      DST(0, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(1, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(2, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(3, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(4, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(5, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(6, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(7, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(8, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(9, y)  = im_clip(0, max, (v)>>5); v+= b;
      DST(10, y) = im_clip(0, max, (v)>>5); v+= b;
      DST(11, y) = im_clip(0, max, (v)>>5); v+= b;
      DST(12, y) = im_clip(0, max, (v)>>5); v+= b;
      DST(13, y) = im_clip(0, max, (v)>>5); v+= b;
      DST(14, y) = im_clip(0, max, (v)>>5); v+= b;
      DST(15, y) = im_clip(0, max, (v)>>5);
      constant += c;
    }
  }
}



void pred_intra4x4(int meanY, intra_4x4_pred_mode_t Intra4x4PredMode, pixel_t* dst, int dy,
                                 unsigned int left_is_avail, unsigned int top_is_avail, unsigned int topright_is_avail)
{
  switch (Intra4x4PredMode)
  {
    case Intra_4x4_Vertical:
      LUD_DEBUG_ASSERT(top_is_avail); // top samples must be available
      pred_intra_4x4_vertical(dy, dst);
      break;
    case Intra_4x4_Horizontal:
      LUD_DEBUG_ASSERT(left_is_avail); // left samples must be available
      pred_intra_4x4_horizontal(dy, dst);
      break;
    case Intra_4x4_DC:
    {
      int mode = 0;
      pixel_t zero = meanY;
      if (!top_is_avail) // top not available
        mode += 1;
      if (!left_is_avail) // left not available
        mode += 2;
      pred_intra_4x4_dc(dy, dst, mode, zero);
      break;
    }
    case Intra_4x4_Diagonal_Down_Left:
      LUD_DEBUG_ASSERT(top_is_avail); // top samples must be available
      pred_intra_4x4_diagonal_down_left(dy, dst, topright_is_avail);
      break;
    case Intra_4x4_Diagonal_Down_Right:
      LUD_DEBUG_ASSERT(top_is_avail && left_is_avail); // top and left edge must be available
      pred_intra_4x4_diagonal_down_right(dy, dst);
      break;
    case Intra_4x4_Vertical_Right:
      LUD_DEBUG_ASSERT(top_is_avail && left_is_avail); // top and left edge must be available
      pred_intra_4x4_vertical_right(dy, dst);
      break;
    case Intra_4x4_Horizontal_Down:
      LUD_DEBUG_ASSERT(top_is_avail && left_is_avail); // top and left edge must be available
      pred_intra_4x4_horinzontal_down(dy, dst);
      break;
    case Intra_4x4_Vertical_Left:
      LUD_DEBUG_ASSERT(top_is_avail); // top samples must be available
      pred_intra_4x4_vertical_left(dy, dst, topright_is_avail);
      break;
    case Intra_4x4_Horizontal_Up:
      LUD_DEBUG_ASSERT(left_is_avail); // left samples must be available
      pred_intra_4x4_horizontal_up(dy, dst);
      break;
    default:
      LUD_DEBUG_ASSERT(0); // non-existing Intra4x4PredMode !!
      break;
  }
}


void pred_intra8x8(int meanY, intra_4x4_pred_mode_t Intra8x8PredMode, pixel_t* dst, int dy,
                                 unsigned int left_is_avail, unsigned int topleft_is_avail, unsigned int top_is_avail, unsigned int topright_is_avail)
{
  switch (Intra8x8PredMode)
  {
    case Intra_4x4_Vertical:
      LUD_DEBUG_ASSERT(top_is_avail); // top samples must be available
      pred_intra_8x8_vertical(dy, dst, topleft_is_avail, topright_is_avail);
      break;
    case Intra_4x4_Horizontal:
      LUD_DEBUG_ASSERT(left_is_avail); // left samples must be available
      pred_intra_8x8_horizontal(dy, dst, topleft_is_avail);
      break;
    case Intra_4x4_DC:
    {
      int mode = 0;
      pixel_t zero = meanY;
      if (!top_is_avail) // top not available
        mode += 1;
      if (!left_is_avail) // left not available
        mode += 2;
      pred_intra_8x8_dc(dy, dst, mode, zero, topleft_is_avail, topright_is_avail);
      break;
    }
    case Intra_4x4_Diagonal_Down_Left:
      LUD_DEBUG_ASSERT(top_is_avail); // top samples must be available
      pred_intra_8x8_diagonal_down_left(dy, dst, topleft_is_avail, topright_is_avail);
      break;
    case Intra_4x4_Diagonal_Down_Right:
      LUD_DEBUG_ASSERT(top_is_avail && left_is_avail && topleft_is_avail); // top and left edge must be available
      pred_intra_8x8_diagonal_down_right(dy, dst, topleft_is_avail, topright_is_avail);
      break;
    case Intra_4x4_Vertical_Right:
      LUD_DEBUG_ASSERT(top_is_avail && left_is_avail && topleft_is_avail); // top and left edge must be available
      pred_intra_8x8_vertical_right(dy, dst, topleft_is_avail, topright_is_avail);
      break;
    case Intra_4x4_Horizontal_Down:
      LUD_DEBUG_ASSERT(top_is_avail && left_is_avail && topleft_is_avail); // top and left edge must be available
      pred_intra_8x8_horinzontal_down(dy, dst, topleft_is_avail, topright_is_avail);
      break;
    case Intra_4x4_Vertical_Left:
      LUD_DEBUG_ASSERT(top_is_avail); // top samples must be available
      pred_intra_8x8_vertical_left(dy, dst, topleft_is_avail, topright_is_avail);
      break;
    case Intra_4x4_Horizontal_Up:
      LUD_DEBUG_ASSERT(left_is_avail); // left samples must be available
      pred_intra_8x8_horizontal_up(dy, dst, topleft_is_avail);
      break;
    default:
      LUD_DEBUG_ASSERT(0); // non-existing Intra8x8PredMode !!
      break;
  }
}



void pred_intra16x16(int meanY, int clipY, int Intra16x16PredMode, pixel_t* dst, int dy,
                                   unsigned int left_is_avail, unsigned int top_is_avail)
{
  switch (Intra16x16PredMode)
  {
    case 0:
      LUD_DEBUG_ASSERT(top_is_avail); // top samples must be available
      pred_intra_16x16_vertical(dy, dst);
      break;
    case 1:
      LUD_DEBUG_ASSERT(left_is_avail); // left samples must be available
      pred_intra_16x16_horizontal(dy, dst);
      break;
    case 2:
    {
      int mode = 0;
      pixel_t zero = meanY;
      if (!top_is_avail) // top not available
        mode += 1;
      if (!left_is_avail) // left not available
        mode += 2;
      pred_intra_16x16_dc(dy, dst, mode, zero);
      break;
    }
    case 3:
      LUD_DEBUG_ASSERT(top_is_avail && left_is_avail); // top and left edge must be available
      pred_intra_16x16_plane(dy, dst, clipY);
      break;

    default:
      LUD_DEBUG_ASSERT(0); // non-existing Intra16x16PredMode !!
      break;
  }
}


void pred_intra_chroma(int meanC, int clipC, int cfidc, int intra_chroma_pred_mode, pixel_t* dst, int dy,
    unsigned int left_is_avail_top, unsigned int left_is_avail_bot, unsigned int top_is_avail)
{
  switch (intra_chroma_pred_mode)
  {
    case 0:
    {
      pixel_t zero = meanC;
      if (left_is_avail_top == left_is_avail_bot)
        pred_intra_chroma_dc_full(cfidc, dy, dst, left_is_avail_top, top_is_avail, zero);
      else
        pred_intra_chroma_dc(cfidc, dy, dst, left_is_avail_top, left_is_avail_bot, top_is_avail, zero);
      break;
    }
    case 1:
      LUD_DEBUG_ASSERT(left_is_avail_top && left_is_avail_bot); // left samples must be available
      pred_intra_chroma_horizontal(cfidc, dy, dst);
      break;
    case 2:
      LUD_DEBUG_ASSERT(top_is_avail); // top samples must be available
      pred_intra_chroma_vertical(cfidc, dy, dst);
      break;
    case 3:
      LUD_DEBUG_ASSERT(top_is_avail && left_is_avail_top && left_is_avail_bot); // top and left edge must be available
      pred_intra_chroma_plane(cfidc, dy, dst, clipC);
      break;

    default:
      LUD_DEBUG_ASSERT(0); // non-existing Intra16x16PredMode !!
      break;
  }
}

void compute_left_edge_availability(struct rv264macro* curr_mb_attr, mb_type_t mb_type, unsigned int mb_field_decoding_flag,
    unsigned int curr_is_bot_mb,unsigned int PicWidthInMbs, unsigned int MbaffFrameFlag, unsigned int constrained_intra_pred_flag,
    unsigned int* left_is_avail_top, unsigned int* left_is_avail_bot)
{
  if (constrained_intra_pred_flag && curr_mb_attr->left_mb_is_available)
  {
    if (!MbaffFrameFlag || !(curr_mb_attr->left_mb_is_field ^ mb_field_decoding_flag))
    {
      unsigned int avail;
      struct rv264macro* left = curr_mb_attr-1;
      avail = left->mb_type < SI || (left->mb_type == mb_type); // The second test is =SI because mb_type is a Intra Mb and the first test failed
      *left_is_avail_top = *left_is_avail_bot = avail;
    }
    else if (mb_field_decoding_flag) // Only one of the left MB must satisfy the constraint
    {
      struct rv264macro* left0 = curr_mb_attr -1 -PicWidthInMbs * curr_is_bot_mb;
      struct rv264macro* left1 = left0 + PicWidthInMbs;

      *left_is_avail_top = left0->mb_type < SI || (left0->mb_type == mb_type);
      *left_is_avail_bot = left1->mb_type < SI || (left1->mb_type == mb_type);
    }
    else // Both top and bot MB must satisfy the constraint
    {
      unsigned int avail;
      struct rv264macro* left0 = curr_mb_attr -1 -PicWidthInMbs * curr_is_bot_mb;
      struct rv264macro* left1 = left0 + PicWidthInMbs;

      avail =  (left0->mb_type < SI || (left0->mb_type == mb_type)) && (left1->mb_type < SI || (left1->mb_type == mb_type));
      *left_is_avail_top = *left_is_avail_bot = avail;
    }
  }
  else
  {
    *left_is_avail_top = *left_is_avail_bot = curr_mb_attr->left_mb_is_available;
  }
}
unsigned int get_up_edge_availability(struct rv264macro* curr_mb_attr, mb_type_t mb_type, unsigned int mb_field_decoding_flag,
    unsigned int curr_is_bot_mb,unsigned int PicWidthInMbs, unsigned int MbaffFrameFlag)
{
  if (!MbaffFrameFlag)
  {
    struct rv264macro* up = curr_mb_attr-PicWidthInMbs;
    return up->mb_type < SI || (up->mb_type == mb_type); // The second test is =SI because mb_type is a Intra Mb and the first test failed
  }
  else
  {
    struct rv264macro* up = get_up_mbaff_mb(curr_mb_attr, PicWidthInMbs, mb_field_decoding_flag, curr_mb_attr->up_mb_is_field, curr_is_bot_mb);
    return up->mb_type < SI || (up->mb_type == mb_type); // The second test is =SI because mb_type is a Intra Mb and the first test failed
  }
}
unsigned int get_upleft_edge_availability(struct rv264macro* curr_mb_attr, mb_type_t mb_type, unsigned int mb_field_decoding_flag,
    unsigned int curr_is_bot_mb,unsigned int PicWidthInMbs, unsigned int MbaffFrameFlag)
{
  if (!MbaffFrameFlag)
  {
    struct rv264macro* upleft = curr_mb_attr-PicWidthInMbs-1;
    return upleft->mb_type < SI || (upleft->mb_type == mb_type); // The second test is =SI because mb_type is a Intra Mb and the first test failed
  }
  else
  {
    struct rv264macro* upleft = get_upleft_mbaff_mb(curr_mb_attr, PicWidthInMbs, mb_field_decoding_flag, curr_mb_attr->upleft_mb_is_field, curr_is_bot_mb);
    return upleft->mb_type < SI || (upleft->mb_type == mb_type); // The second test is =SI because mb_type is a Intra Mb and the first test failed
  }
}
unsigned int get_upright_edge_availability(struct rv264macro* curr_mb_attr, mb_type_t mb_type, unsigned int mb_field_decoding_flag,
    unsigned int curr_is_bot_mb,unsigned int PicWidthInMbs, unsigned int MbaffFrameFlag)
{
  if (!MbaffFrameFlag)
  {
    struct rv264macro* upright = curr_mb_attr-PicWidthInMbs+1;
    return upright->mb_type < SI || (upright->mb_type == mb_type); // The second test is =SI because mb_type is a Intra Mb and the first test failed
  }
  else
  {
    struct rv264macro* upright = get_upright_mbaff_mb(curr_mb_attr, PicWidthInMbs, mb_field_decoding_flag, curr_mb_attr->upright_mb_is_field, curr_is_bot_mb);
    return upright->mb_type < SI || (upright->mb_type == mb_type); // The second test is =SI because mb_type is a Intra Mb and the first test failed
  }
}


void decode_intra_mb(struct rv264macro* curr_mb_attr, mb_type_t mb_type, int16_t* mb_data, unsigned int PicWidthY,
    unsigned int PicWidthC, pixel_t* mb_Y_samples, pixel_t* mb_Cb_samples, pixel_t* mb_Cr_samples,
    unsigned int mbc_height, unsigned int mbc_width, unsigned int cfidc, int clipY, int clipC, int meanY, int meanC,
    unsigned int mb_field_decoding_flag, unsigned int curr_is_bot_mb, unsigned int PicWidthInMbs, unsigned int MbaffFrameFlag,
    unsigned int constrained_intra_pred_flag)
{
 if (mb_type == I_PCM)
 {
   // Fills in the pcm samples that are store in the mb data
   // TODO, little optimization: PCM samples could be directly filled in during the slice_data decoding instead of being stored and copied again here
   // But anyway PCM is not used much...
   unsigned int i, j;
   int16_t* pcm_samples = mb_data;

   for( j=0; j<16; j++)
   {
     for( i=0; i<16; i++)
     {
       mb_Y_samples[i] = *pcm_samples++;
     }
     mb_Y_samples += PicWidthY;
   }
   for( j=0; j<mbc_height; j++)
   {
     for( i=0; i<mbc_width; i++)
     {
       mb_Cb_samples[i] = pcm_samples[0];
       mb_Cr_samples[i] = pcm_samples[mbc_height*mbc_width];
       pcm_samples++;
     }
     mb_Cb_samples += PicWidthC;
     mb_Cr_samples += PicWidthC;
   }
 }
 else
 {
   unsigned int left_is_avail_top, left_is_avail_bot;
   compute_left_edge_availability(curr_mb_attr, mb_type, mb_field_decoding_flag, curr_is_bot_mb, PicWidthInMbs, MbaffFrameFlag,
       constrained_intra_pred_flag, &left_is_avail_top, &left_is_avail_bot);
   unsigned int top_is_avail = (constrained_intra_pred_flag && curr_mb_attr->up_mb_is_available) ?
       get_up_edge_availability(curr_mb_attr, mb_type, mb_field_decoding_flag, curr_is_bot_mb, PicWidthInMbs, MbaffFrameFlag) :
       curr_mb_attr->up_mb_is_available;

   if (mb_type == I_NxN || mb_type == SI)
   {
     if (curr_mb_attr->transform_size_8x8_flag)
     { // luma 8x8 pred
       pixel_t* block_samples = mb_Y_samples;
       intra_4x4_pred_mode_t* Intra8x8PredMode = curr_mb_attr->Intra4x4PredMode;
       int16_t* data = mb_data;
       unsigned int topleft_is_avail = (constrained_intra_pred_flag && curr_mb_attr->upleft_mb_is_available) ?
           get_upleft_edge_availability(curr_mb_attr, mb_type, mb_field_decoding_flag, curr_is_bot_mb, PicWidthInMbs, MbaffFrameFlag) :
           curr_mb_attr->upleft_mb_is_available;
       unsigned int topright_is_avail= (constrained_intra_pred_flag && curr_mb_attr->upright_mb_is_available) ?
           get_upright_edge_availability(curr_mb_attr, mb_type, mb_field_decoding_flag, curr_is_bot_mb, PicWidthInMbs, MbaffFrameFlag) :
           curr_mb_attr->upright_mb_is_available;
       unsigned int total_coeffs;

       //            left_is_avail,        topleft_is_avail,       top_is_avail,       topright_is_avail
       pred_intra8x8(meanY, Intra8x8PredMode[0] , block_samples, PicWidthY,
                     left_is_avail_top,    topleft_is_avail,       top_is_avail,       top_is_avail);
       total_coeffs = curr_mb_attr->TotalCoeffLuma[0] + curr_mb_attr->TotalCoeffLuma[1]
                    + curr_mb_attr->TotalCoeffLuma[4] + curr_mb_attr->TotalCoeffLuma[5];
       inverse_transform(IDCT8x8, total_coeffs, curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthY, clipY);

       block_samples+= 8; data+= 64;
       pred_intra8x8(meanY, Intra8x8PredMode[2] , block_samples, PicWidthY,
                     1,                    top_is_avail,           top_is_avail,       topright_is_avail);
       total_coeffs = curr_mb_attr->TotalCoeffLuma[2] + curr_mb_attr->TotalCoeffLuma[3]
                    + curr_mb_attr->TotalCoeffLuma[6] + curr_mb_attr->TotalCoeffLuma[7];
       inverse_transform(IDCT8x8, total_coeffs, curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthY, clipY);


       block_samples = mb_Y_samples + 8*PicWidthY; data+= 64;
       pred_intra8x8(meanY, Intra8x8PredMode[8] , block_samples, PicWidthY,
                     left_is_avail_bot,    left_is_avail_top,      1,                  1);
       total_coeffs = curr_mb_attr->TotalCoeffLuma[8] + curr_mb_attr->TotalCoeffLuma[9]
                    + curr_mb_attr->TotalCoeffLuma[12] + curr_mb_attr->TotalCoeffLuma[13];
       inverse_transform(IDCT8x8, total_coeffs, curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthY, clipY);

       block_samples+= 8; data+= 64;
       pred_intra8x8(meanY, Intra8x8PredMode[10], block_samples, PicWidthY,
                     1,                    1,                      1,                  0);
       total_coeffs = curr_mb_attr->TotalCoeffLuma[10] + curr_mb_attr->TotalCoeffLuma[11]
                    + curr_mb_attr->TotalCoeffLuma[14] + curr_mb_attr->TotalCoeffLuma[15];
       inverse_transform(IDCT8x8, total_coeffs, curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthY, clipY);
     }
     else
     { // luma 4x4 pred
       pixel_t* row_samples = mb_Y_samples;
       pixel_t* block_samples = row_samples; row_samples += 4*PicWidthY;
       intra_4x4_pred_mode_t* Intra4x4PredMode = curr_mb_attr->Intra4x4PredMode;
       int16_t* data = mb_data;

       unsigned int topright_is_avail= (constrained_intra_pred_flag && curr_mb_attr->upright_mb_is_available) ?
           get_upright_edge_availability(curr_mb_attr, mb_type, mb_field_decoding_flag, curr_is_bot_mb, PicWidthInMbs, MbaffFrameFlag) :
           curr_mb_attr->upright_mb_is_available;



       pred_intra4x4(meanY, Intra4x4PredMode[0],  block_samples, PicWidthY, left_is_avail_top,    top_is_avail,      top_is_avail );
       inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffLuma[0], curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthY, clipY);

       block_samples+= 4; data+= 16;
       pred_intra4x4(meanY, Intra4x4PredMode[1],  block_samples, PicWidthY, 1,                    top_is_avail,      top_is_avail );
       inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffLuma[1], curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthY, clipY);

       block_samples+= 4; data += 16;
       pred_intra4x4(meanY, Intra4x4PredMode[2],  block_samples, PicWidthY, 1,                    top_is_avail,      top_is_avail );
       inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffLuma[2], curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthY, clipY);

       block_samples+= 4; data+= 16;
       pred_intra4x4(meanY, Intra4x4PredMode[3],  block_samples, PicWidthY, 1,                    top_is_avail,      topright_is_avail);
       inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffLuma[3], curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthY, clipY);

       block_samples = row_samples; row_samples += 4*PicWidthY; data+= 16;
       pred_intra4x4(meanY, Intra4x4PredMode[4],  block_samples, PicWidthY, left_is_avail_top,    1,                  1                  );
       inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffLuma[4], curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthY, clipY);

       block_samples+= 4; data+= 16;
       pred_intra4x4(meanY, Intra4x4PredMode[5],  block_samples, PicWidthY, 1,                    1,                  0                  );
       inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffLuma[5], curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthY, clipY);

       block_samples+= 4; data+= 16;
       pred_intra4x4(meanY, Intra4x4PredMode[6],  block_samples, PicWidthY, 1,                    1,                  1                  );
       inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffLuma[6], curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthY, clipY);

       block_samples+= 4; data+= 16;
       pred_intra4x4(meanY, Intra4x4PredMode[7],  block_samples, PicWidthY, 1,                    1,                  0                  );
       inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffLuma[7], curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthY, clipY);

       block_samples = row_samples; row_samples += 4*PicWidthY; data+= 16;
       pred_intra4x4(meanY, Intra4x4PredMode[8],  block_samples, PicWidthY, left_is_avail_bot,    1,                  1                  );
       inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffLuma[8], curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthY, clipY);

       block_samples+= 4; data+= 16;
       pred_intra4x4(meanY, Intra4x4PredMode[9],  block_samples, PicWidthY, 1,                    1,                  1                  );
       inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffLuma[9], curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthY, clipY);

       block_samples+= 4; data+= 16;
       pred_intra4x4(meanY, Intra4x4PredMode[10], block_samples, PicWidthY, 1,                    1,                  1                  );
       inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffLuma[10], curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthY, clipY);

       block_samples+= 4; data+= 16;
       pred_intra4x4(meanY, Intra4x4PredMode[11], block_samples, PicWidthY, 1,                    1,                  0                  );
       inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffLuma[11], curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthY, clipY);

       block_samples = row_samples; row_samples += 4*PicWidthY; data+= 16;
       pred_intra4x4(meanY, Intra4x4PredMode[12], block_samples, PicWidthY, left_is_avail_bot,    1,                  1                  );
       inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffLuma[12], curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthY, clipY);

       block_samples+= 4; data+= 16;
       pred_intra4x4(meanY, Intra4x4PredMode[13], block_samples, PicWidthY, 1,                    1,                  0                  );
       inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffLuma[13], curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthY, clipY);

       block_samples+= 4; data+= 16;
       pred_intra4x4(meanY, Intra4x4PredMode[14], block_samples, PicWidthY, 1,                    1,                  1                  );
       inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffLuma[14], curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthY, clipY);

       block_samples+= 4; data+= 16;
       pred_intra4x4(meanY, Intra4x4PredMode[15], block_samples, PicWidthY, 1,                    1,                  0                  );
       inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffLuma[15], curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthY, clipY);
     }
   }
   else
   {
     // luma 16x16 pred
     int pred_mode = Intra16x16PredMode(mb_type);
     int i;
     int16_t temp;
     pixel_t* block_samples;
     int16_t* data = mb_data;

     pred_intra16x16(meanY, clipY, pred_mode, mb_Y_samples, PicWidthY, left_is_avail_top && left_is_avail_bot, top_is_avail);

     // The transform the 16 DC coeffs is done in the decode_slice process
     data += 15;
     temp = data[0]; // store the 16th DC coeff
     // Transform the 16*15 AC coeffs. a DC coeff is taken from the previous list
     for (i=0; i<15; i++)
     {
       data[0] = mb_data[i];
       block_samples = mb_Y_samples + ((i>>2)*PicWidthY + (i&3))*4;
       inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffLuma[i]+(data[0]!=0), curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthY, clipY);
       data+= 15;
     }
     data[0] = temp;
     block_samples = mb_Y_samples + ((i>>2)*PicWidthY + (i&3))*4;
     inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffLuma[i]+(data[0]!=0), curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthY, clipY);
   }

   // chroma pred
   if (cfidc == 0)
   { // No chroma component, nothing to do
   }
   else
   {
     unsigned int i, r, c;
     int16_t temp;
     pixel_t* block_samples;
     int16_t* data;
     unsigned int num_blocks;

     // r is used for shifting and c is used as a mask

     mb_data+=256; // go to chroma data
     if (cfidc == 1)
     {
       num_blocks = 4;
       r = 1;
       c = 1;
     }
     else if (cfidc == 2)
     {
       num_blocks = 8;
       r = 1;
       c = 1;
     }
     else // cfidc == 3
     {
       LUD_DEBUG_ASSERT(cfidc==3);
       num_blocks = 16;
       r = 2;
       c = 3; // is a mask
     }

     // Cb
     data = mb_data;
     pred_intra_chroma(meanC, clipC, cfidc, curr_mb_attr->intra_chroma_pred_mode, mb_Cb_samples, PicWidthC, left_is_avail_top, left_is_avail_bot, top_is_avail);
     data += num_blocks-1;
     temp = data[0]; // store the 16th DC coeff
     // Transform the 16*15 AC coeffs. a DC coeff is taken from the previous list
     for (i=0; i<num_blocks-1; i++)
     {
       data[0] = mb_data[i];
       block_samples =  mb_Cb_samples + ((i>>r)*PicWidthC + (i&c))*4;
       inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffChroma[0][i]+(data[0]!=0), curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthC, clipC);
       data+= 15;
     }
     data[0] = temp;
     block_samples =  mb_Cb_samples + ((i>>r)*PicWidthC + (i&c))*4;
     inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffChroma[0][i]+(data[0]!=0), curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthC, clipC);

     // Cr
     mb_data += num_blocks*16;
     data = mb_data;
     pred_intra_chroma(meanC, clipC, cfidc, curr_mb_attr->intra_chroma_pred_mode, mb_Cr_samples, PicWidthC, left_is_avail_top, left_is_avail_bot, top_is_avail);
     data += num_blocks-1;
     temp = data[0]; // store the 16th DC coeff
     // Transform the 16*15 AC coeffs. a DC coeff is taken from the previous list
     for (i=0; i<num_blocks-1; i++)
     {
       data[0] = mb_data[i];
       block_samples =  mb_Cr_samples + ((i>>r)*PicWidthC + (i&c))*4;
       inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffChroma[1][i]+(data[0]!=0), curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthC, clipC);
       data+= 15;
     }
     data[0] = temp;
     block_samples =  mb_Cr_samples + ((i>>r)*PicWidthC + (i&c))*4;
     inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffChroma[1][i]+(data[0]!=0), curr_mb_attr->TransformBypassModeFlag, block_samples, data, PicWidthC, clipC);
   }
 }
}
