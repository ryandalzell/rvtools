/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/



#ifndef __INVERSE_TRANSFORMS_H__
#define __INVERSE_TRANSFORMS_H__

#include "common.h"

typedef enum
{
  IDCT4x4,
  IDCT8x8,
  IHDM2x2,
  IHDM2x4,
  IHDM4x4,
} TRANSFORM_TYPE;

void inverse_transform(TRANSFORM_TYPE type, unsigned int total_coeffs, unsigned int bypass_transform, pixel_t *dst, int16_t *block, int dy, int clip_val);

#endif //__INVERSE_TRANSFORMS_H__
