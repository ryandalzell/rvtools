/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/


#include <string.h>
#include "common.h"
#include "syntax.h"
#include "bitstream.h"
#include "defaulttables.h"
#include "intmath.h"
#include "inverse_transforms.h"
#include "decode_slice_data.h"
#include "cabac.h"
#include "trace.h"


// This enum is also set equal to ctxBlockCat
typedef enum
{
  LUMA_DC_LEVEL   = 0,
  LUMA_AC_LEVEL   = 1,
  LUMA_LEVEL      = 2,
  CHROMA_DC_LEVEL = 3,
  CHROMA_AC_LEVEL = 4,
  LUMA8x8_LEVEL   = 5,
} RESIDUAL_BLOCK_TYPE;

const intra_4x4_pred_mode_t Intra_4x4_Invalid = intra_4x4_pred_mode_t(-1);
static const intra_4x4_pred_mode_t dc_val[2] = {Intra_4x4_DC, Intra_4x4_Invalid};

unsigned int NextMbAddress(uint8_t* MbToSliceGroupMap, unsigned int n, unsigned int PicSizeInMbs, unsigned int num_slice_groups_minus1)
{
  unsigned int i = n+1;

  if (!num_slice_groups_minus1) // easy optimisation...
    return i;

  while( i < PicSizeInMbs && MbToSliceGroupMap[ i ] != MbToSliceGroupMap[ n ] )
    i++;

  return i;
}


// can_be_I_mb must be set to a constant value when calling this function => enable simplification when we know we are dealing with P/B mb only
part_type_t MbPartPredMode( mb_type_t mb_type, unsigned int mbPartIdx, unsigned int transform_size_8x8_flag, unsigned int can_be_I_mb)
{
  LUD_DEBUG_ASSERT(mb_type < NB_OF_MB_TYPE && mbPartIdx < 2);
  if (!can_be_I_mb || mb_type > 0)
  {
    LUD_DEBUG_ASSERT(mbPartIdx == 0 || mb_type >= P_L0_16x16 );
    return MbPartPredMode_array[mbPartIdx][mb_type];
  }
  else
  {
    LUD_DEBUG_ASSERT(mbPartIdx == 0);
    if (transform_size_8x8_flag)
      return Intra_8x8;
    else
      return Intra_4x4;
  }
}

unsigned int NumMbPart(mb_type_t mb_type)
{
  LUD_DEBUG_ASSERT(mb_type >= P_L0_16x16 && mb_type < NB_OF_MB_TYPE);
  return Intra16x16PredMode_NumMbPart_array[mb_type];
}

part_type_t SubMbPredMode(sub_mb_type_t sub_mb_type )
{
  LUD_DEBUG_ASSERT(sub_mb_type < NB_OF_SUB_MB_TYPE);
  return SubMbPredMode_array[sub_mb_type];
}

unsigned int NumSubMbPart(sub_mb_type_t sub_mb_type)
{
  LUD_DEBUG_ASSERT(sub_mb_type < NB_OF_SUB_MB_TYPE);
  return NumSubMbPart_array[sub_mb_type];
}

unsigned int MbPartWidth(mb_type_t mb_type)
{
  LUD_DEBUG_ASSERT(mb_type >= P_L0_16x16 && mb_type < NB_OF_MB_TYPE);
  return CodedBlockPatternChroma_MbPartWidth_array[mb_type];
}

unsigned int MbPartHeight(mb_type_t mb_type)
{
  LUD_DEBUG_ASSERT(mb_type >= P_L0_16x16 && mb_type < NB_OF_MB_TYPE);
  return CodedBlockPatternLuma_MbPartHeight_array[mb_type];
}

unsigned int SubMbPartWidth(sub_mb_type_t sub_mb_type)
{
  LUD_DEBUG_ASSERT(sub_mb_type < NB_OF_SUB_MB_TYPE);
  return SubMbPartWidth_array[sub_mb_type];
}

unsigned int SubMbPartHeight(sub_mb_type_t sub_mb_type)
{
  LUD_DEBUG_ASSERT(sub_mb_type < NB_OF_SUB_MB_TYPE);
  return SubMbPartHeight_array[sub_mb_type];
}

unsigned int is_IntraMb(mb_type_t mb_type)
{
  return mb_type<=SI;
}

unsigned int is_InterMb(mb_type_t mb_type)
{
  return mb_type>=P_L0_16x16;
}

// 4x4 or 8x8 pred type. SI macroblocks are 4x4 pred type
unsigned int is_NxNMb(mb_type_t mb_type)
{
  return (mb_type == I_NxN || mb_type == SI);
}

unsigned int is_data_partitioning_slice(nal_unit_type_t nal_unit_type)
{
  return nal_unit_type>=NAL_CODED_SLICE_PARTITION_A && nal_unit_type<=NAL_CODED_SLICE_PARTITION_C;
}

struct rv264macro* get_left_mbaff_mb(struct rv264macro* curr_mb, unsigned int PicWidthInMbs,
    unsigned int curr_is_field, unsigned int left_is_field, unsigned int curr_is_bot, unsigned int block_row)
{
  unsigned int left_mb_topbot = left_mb_pos[curr_is_field][left_is_field][curr_is_bot][block_row];
  return curr_mb -1 -(PicWidthInMbs << curr_is_bot) + (PicWidthInMbs << left_mb_topbot);
}
unsigned int get_left_mbaff_4x4block(unsigned int curr_is_field, unsigned int left_is_field,
    unsigned int curr_is_bot, unsigned int block_row)
{
  return left_4x4block_pos[curr_is_field][left_is_field][curr_is_bot][block_row];
}

struct rv264macro* get_up_mbaff_mb(struct rv264macro* curr_mb, unsigned int PicWidthInMbs,
    unsigned int curr_is_field, unsigned int up_is_field, unsigned int curr_is_bot)
{
  return curr_mb - (PicWidthInMbs << up_4x4block_pos[curr_is_field][up_is_field][curr_is_bot]);
}

struct rv264macro* get_upleft_mbaff_mb(struct rv264macro* curr_mb, unsigned int PicWidthInMbs,
    unsigned int curr_is_field, unsigned int upleft_is_field, unsigned int curr_is_bot)
{
  unsigned int bit47 = upleft_4x4block_pos[curr_is_field][upleft_is_field][curr_is_bot] >> 4;
  return curr_mb - 1 - PicWidthInMbs * bit47;
}
unsigned int get_upleft_mbaff_4x4block(unsigned int curr_is_field, unsigned int upleft_is_field,
    unsigned int curr_is_bot)
{
  unsigned int bit03 = upleft_4x4block_pos[curr_is_field][upleft_is_field][curr_is_bot] & 0xF;
  return bit03;
}

struct rv264macro* get_upright_mbaff_mb(struct rv264macro* curr_mb, unsigned int PicWidthInMbs,
    unsigned int curr_is_field, unsigned int upright_is_field, unsigned int curr_is_bot)
{
  return curr_mb + 1 - (PicWidthInMbs << upright_4x4block_pos[curr_is_field][upright_is_field][curr_is_bot]);
}

void fills_in_intra4x4_pred_cache(struct rv264macro* mb_attr, unsigned int curr_is_bot, unsigned int mb_field_decoding_flag,
    int PicWidthInMbs, intra_4x4_pred_mode_t intra_pred_cache[25], unsigned int MbaffFrameFlag, unsigned int constrained_intra_pred_flag)
{
  int dcPredModePredictedFlag;

  if (!MbaffFrameFlag)
  {
    // Fills in the top line
    if (!mb_attr->up_mb_is_available)
      intra_pred_cache[1] = intra_pred_cache[2] = intra_pred_cache[3] = intra_pred_cache[4] = Intra_4x4_Invalid;
    else
    {
      struct rv264macro* upper_mb_attr = mb_attr - PicWidthInMbs; // will only be used if mb_row>0
      if (!IS_NxN(upper_mb_attr))
      { // upper mb is not available
        dcPredModePredictedFlag = (IS_INTER(upper_mb_attr) && constrained_intra_pred_flag);
        intra_pred_cache[1] = intra_pred_cache[2] = intra_pred_cache[3] = intra_pred_cache[4] = dc_val[dcPredModePredictedFlag];
      }
      else
      {
        intra_pred_cache[1] = upper_mb_attr->Intra4x4PredMode[12];
        intra_pred_cache[2] = upper_mb_attr->Intra4x4PredMode[13];
        intra_pred_cache[3] = upper_mb_attr->Intra4x4PredMode[14];
        intra_pred_cache[4] = upper_mb_attr->Intra4x4PredMode[15];
      }
    }

    // Fills in the left line
    if (!mb_attr->left_mb_is_available)
      intra_pred_cache[5] = intra_pred_cache[10] = intra_pred_cache[15] = intra_pred_cache[20] = Intra_4x4_Invalid;
    else
    {
      struct rv264macro* left_mb_attr = mb_attr - 1; // will only be used if mb_col>0
      if (!IS_NxN(left_mb_attr))
      { // left mb is not available
        dcPredModePredictedFlag = (IS_INTER(left_mb_attr) && constrained_intra_pred_flag);
        intra_pred_cache[5] = intra_pred_cache[10] = intra_pred_cache[15] = intra_pred_cache[20] = dc_val[dcPredModePredictedFlag];
      }
      else
      {
        intra_pred_cache[5]  = left_mb_attr->Intra4x4PredMode[3];
        intra_pred_cache[10] = left_mb_attr->Intra4x4PredMode[7];
        intra_pred_cache[15] = left_mb_attr->Intra4x4PredMode[11];
        intra_pred_cache[20] = left_mb_attr->Intra4x4PredMode[15];
      }
    }
  }
  else // MbaffFrame pictures
  {
    unsigned int curr_is_field = mb_field_decoding_flag;

    // Fills in the top line
    if (!mb_attr->up_mb_is_available)
      intra_pred_cache[1] = intra_pred_cache[2] = intra_pred_cache[3] = intra_pred_cache[4] = Intra_4x4_Invalid;
    else
    {
      struct rv264macro* upper_mb_attr = get_up_mbaff_mb(mb_attr, PicWidthInMbs, curr_is_field, mb_attr->up_mb_is_field, curr_is_bot);
      if (!IS_NxN(upper_mb_attr))
      {
        dcPredModePredictedFlag = IS_INTER(upper_mb_attr) && constrained_intra_pred_flag;
        intra_pred_cache[1] = intra_pred_cache[2] = intra_pred_cache[3] = intra_pred_cache[4] = dc_val[dcPredModePredictedFlag];
      }
      else
      {
        intra_pred_cache[1] = upper_mb_attr->Intra4x4PredMode[12];
        intra_pred_cache[2] = upper_mb_attr->Intra4x4PredMode[13];
        intra_pred_cache[3] = upper_mb_attr->Intra4x4PredMode[14];
        intra_pred_cache[4] = upper_mb_attr->Intra4x4PredMode[15];
      }
    }

    // Fills in the left line
    if (!mb_attr->left_mb_is_available)
      intra_pred_cache[5] = intra_pred_cache[10] = intra_pred_cache[15] = intra_pred_cache[20] = Intra_4x4_Invalid;
    else
    {
      unsigned int left_is_field = mb_attr->left_mb_is_field;
      int i;
      struct rv264macro* temp_mb_attr;
      struct rv264macro* left_mb_attr;
      const uint8_t* mb_pos = left_mb_pos[curr_is_field][left_is_field][curr_is_bot];
      const uint8_t* block_pos = left_4x4block_pos[curr_is_field][left_is_field][curr_is_bot];
      temp_mb_attr = mb_attr -1 -(PicWidthInMbs << curr_is_bot);

      for(i=0; i<4; i++)
      {
        left_mb_attr = temp_mb_attr + (PicWidthInMbs << mb_pos[i]);
        if (!IS_NxN(left_mb_attr))
        {
          dcPredModePredictedFlag = IS_INTER(left_mb_attr) && constrained_intra_pred_flag;
          intra_pred_cache[5+i*5] = dc_val[dcPredModePredictedFlag];
        }
        else
          intra_pred_cache[5+i*5]  = left_mb_attr->Intra4x4PredMode[block_pos[i]];
      }
    }
  }
}


void fills_in_intra8x8_pred_cache(struct rv264macro* mb_attr, unsigned int curr_is_bot, unsigned int mb_field_decoding_flag,
    int PicWidthInMbs, int8_t intra_pred_cache[9], unsigned int MbaffFrameFlag, unsigned int constrained_intra_pred_flag)
{
  int dcPredModePredictedFlag;


  if (!MbaffFrameFlag)
  {
    // Fills in the top line
    if (!mb_attr->up_mb_is_available)
      intra_pred_cache[1] = intra_pred_cache[2] = -1;
    else
    {
      struct rv264macro* upper_mb_attr = mb_attr - PicWidthInMbs;
      if (!IS_NxN(upper_mb_attr))
      { // upper mb is not available
        dcPredModePredictedFlag = (IS_INTER(upper_mb_attr) && constrained_intra_pred_flag);
        intra_pred_cache[1] = intra_pred_cache[2] = dc_val[dcPredModePredictedFlag];
      }
      else
      {
        intra_pred_cache[1] = upper_mb_attr->Intra4x4PredMode[12];
        intra_pred_cache[2] = upper_mb_attr->Intra4x4PredMode[14];
      }
    }

    // Fills in the left line
    if (!mb_attr->left_mb_is_available)
      intra_pred_cache[3] = intra_pred_cache[6] = -1;
    else
    {
      struct rv264macro* left_mb_attr = mb_attr - 1;
      if (!IS_NxN(left_mb_attr))
      { // left mb is not available
        dcPredModePredictedFlag = (IS_INTER(left_mb_attr) && constrained_intra_pred_flag);
        intra_pred_cache[3] = intra_pred_cache[6] = dc_val[dcPredModePredictedFlag];
      }
      else
      {
        intra_pred_cache[3] = left_mb_attr->Intra4x4PredMode[3];
        intra_pred_cache[6] = left_mb_attr->Intra4x4PredMode[11];
      }
    }
  }
  else // MbaffFrame pictures
  {
    unsigned int curr_is_field = mb_field_decoding_flag;

    // Fills in the top line
    if (!mb_attr->up_mb_is_available)
      intra_pred_cache[1] = intra_pred_cache[2] = -1;
    else
    {
      struct rv264macro* upper_mb_attr = get_up_mbaff_mb(mb_attr, PicWidthInMbs, curr_is_field, mb_attr->up_mb_is_field, curr_is_bot);
      if (!IS_NxN(upper_mb_attr))
      {
        dcPredModePredictedFlag = IS_INTER(upper_mb_attr) && constrained_intra_pred_flag;
        intra_pred_cache[1] = intra_pred_cache[2] = dc_val[dcPredModePredictedFlag];
      }
      else
      {
        intra_pred_cache[1] = upper_mb_attr->Intra4x4PredMode[12];
        intra_pred_cache[2] = upper_mb_attr->Intra4x4PredMode[14];
      }
    }

    // Fills in the left line
    if (!mb_attr->left_mb_is_available)
      intra_pred_cache[3] = intra_pred_cache[6] = -1;
    else
    {
      unsigned int left_is_field = mb_attr->left_mb_is_field;
      int i;
      struct rv264macro* temp_mb_attr;
      struct rv264macro* left_mb_attr;
      const uint8_t* mb_pos = left_mb_pos[curr_is_field][left_is_field][curr_is_bot];
      const uint8_t* block_pos = left_4x4block_pos[curr_is_field][left_is_field][curr_is_bot];
      temp_mb_attr = mb_attr -1 -(PicWidthInMbs << curr_is_bot);

      for(i=0; i<2; i++)
      {
        left_mb_attr = temp_mb_attr + (PicWidthInMbs << mb_pos[i*2]);
        if (!IS_NxN(left_mb_attr))
        {
          dcPredModePredictedFlag = IS_INTER(left_mb_attr) && constrained_intra_pred_flag;
          intra_pred_cache[3+i*3] = dc_val[dcPredModePredictedFlag];
        }
        else
          intra_pred_cache[3+i*3]  = left_mb_attr->Intra4x4PredMode[block_pos[i*2]];
      }
    }
  }
}

void reset_line(int16_t mv_cache[2][25][2], int8_t refIdx_cache[2][25], unsigned int idx, int stride,
    unsigned int need_L0, unsigned int need_L1, unsigned int simple)
{
  int i;
  int nb = simple ? 1 : 4;
  for (i = 0; i < nb; ++i)
  {
    if (need_L0)
    {
      refIdx_cache[0][idx+i*stride] = 0;
      mv_cache[0][idx+i*stride][0] = mv_cache[0][idx+i*stride][1] = 0;
    }
    if (need_L1)
    {
      refIdx_cache[1][idx+i*stride]=0;
      mv_cache[1][idx+i*stride][0] = mv_cache[1][idx+i*stride][1] = 0;
    }
  }
}


void set_line(int16_t mv_cache[2][25][2], int8_t refIdx_cache[2][25], struct rv264macro* mb_attr, unsigned int horiz,
    unsigned int need_L0, unsigned int need_L1, unsigned int need_sub_parts, unsigned int scaler_biased)
{
  unsigned int partstruct_type = mb_partstruct_type[mb_attr->mb_type];
  unsigned int idx = horiz ? 1 : 5;
  int stride = idx;
  int i;

  LUD_DEBUG_ASSERT(need_L0 || need_L1); // should not be called if nothing is needed !!

  if (partstruct_type == 4)
    reset_line(mv_cache, refIdx_cache, idx, stride, need_L0, need_L1, 0);
  else if (partstruct_type != 3) // no sub partition
  {
    for ( i = 0;  i < 2; i++)
    {
      unsigned int partIdx = partstruct_def[partstruct_type][i+2*horiz];
      if (need_L0)
      {
        refIdx_cache[0][idx+2*stride*i] = mb_attr->ref_idx_l0[partIdx] - (scaler_biased==1);
        mv_cache[0][idx+2*stride*i][0] = mb_attr->mvd[0][partIdx][0][0];
        mv_cache[0][idx+2*stride*i][1] = mb_attr->mvd[0][partIdx][0][1];
        mv_cache[0][idx+2*stride*i][1] = im_scale2_biased(mv_cache[0][idx+2*stride*i][1], scaler_biased);
        if (need_sub_parts)
        {
          mv_cache[0][idx+2*stride*i+stride][0] = mv_cache[0][idx+2*stride*i][0];
          mv_cache[0][idx+2*stride*i+stride][1] = mv_cache[0][idx+2*stride*i][1];
        }
      }
      if (need_L1)
      {
        refIdx_cache[1][idx+2*stride*i] = mb_attr->ref_idx_l1[partIdx] - (scaler_biased==1);
        mv_cache[1][idx+2*stride*i][0] = mb_attr->mvd[1][partIdx][0][0];
        mv_cache[1][idx+2*stride*i][1] = mb_attr->mvd[1][partIdx][0][1];
        mv_cache[1][idx+2*stride*i][1] = im_scale2_biased(mv_cache[1][idx+2*stride*i][1], scaler_biased);
        if (need_sub_parts)
        {
          mv_cache[1][idx+2*stride*i+stride][0] = mv_cache[1][idx+2*stride*i][0];
          mv_cache[1][idx+2*stride*i+stride][1] = mv_cache[1][idx+2*stride*i][1];
        }
      }
    }
  }
  else  // with sub partitions
  {
    for ( i = 0;  i < 2; i++)
    {
      unsigned int partIdx = partstruct_def[partstruct_type][i+2*horiz];
      unsigned int subPartIdx;
      sub_mb_type_t sub_mb_type = (sub_mb_type_t) mb_attr->sub_mb_type[partIdx];

      unsigned int subpartstruct_type = submb_partstruct_type[sub_mb_type];
      if (need_L0)
      {
        refIdx_cache[0][idx+2*stride*i] = mb_attr->ref_idx_l0[partIdx] - (scaler_biased==1);
        subPartIdx = partstruct_def[subpartstruct_type][0+2*horiz];
        mv_cache[0][idx+2*stride*i][0] = mb_attr->mvd[0][partIdx][subPartIdx][0];
        mv_cache[0][idx+2*stride*i][1] = mb_attr->mvd[0][partIdx][subPartIdx][1];
        mv_cache[0][idx+2*stride*i][1] = im_scale2_biased(mv_cache[0][idx+2*stride*i][1], scaler_biased);
        if (need_sub_parts)
        {
          subPartIdx = partstruct_def[subpartstruct_type][1+2*horiz];
          mv_cache[0][idx+2*stride*i+stride][0] = mb_attr->mvd[0][partIdx][subPartIdx][0];
          mv_cache[0][idx+2*stride*i+stride][1] = mb_attr->mvd[0][partIdx][subPartIdx][1];
          mv_cache[0][idx+2*stride*i+stride][1] = im_scale2_biased(mv_cache[0][idx+2*stride*i+stride][1], scaler_biased);
        }
      }
      if (need_L1)
      {
        refIdx_cache[1][idx+2*stride*i] = mb_attr->ref_idx_l1[partIdx] - (scaler_biased==1);
        subPartIdx = partstruct_def[subpartstruct_type][0+2*horiz];
        mv_cache[1][idx+2*stride*i][0] = mb_attr->mvd[1][partIdx][subPartIdx][0];
        mv_cache[1][idx+2*stride*i][1] = mb_attr->mvd[1][partIdx][subPartIdx][1];
        mv_cache[1][idx+2*stride*i][1] = im_scale2_biased(mv_cache[1][idx+2*stride*i][1], scaler_biased);
        if (need_sub_parts)
        {
          subPartIdx = partstruct_def[subpartstruct_type][1+2*horiz];
          mv_cache[1][idx+2*stride*i+stride][0] = mb_attr->mvd[1][partIdx][subPartIdx][0];
          mv_cache[1][idx+2*stride*i+stride][1] = mb_attr->mvd[1][partIdx][subPartIdx][1];
          mv_cache[1][idx+2*stride*i+stride][1] = im_scale2_biased(mv_cache[1][idx+2*stride*i+stride][1], scaler_biased);
        }
      }
    }
  }
}


void set_one_vert_entry_mbaff(int16_t mv_cache[2][25][2], int8_t refIdx_cache[2][25], struct rv264macro* mb_attr,
    unsigned int idx, unsigned int srcrow,
    unsigned int need_L0, unsigned int need_L1, unsigned int scaler_biased)
{
  unsigned int partstruct_type = mb_partstruct_type[mb_attr->mb_type];

  LUD_DEBUG_ASSERT(need_L0 || need_L1); // should not be called if nothing is needed !!

  if (partstruct_type == 4)
    reset_line(mv_cache, refIdx_cache, idx, 0, need_L0, need_L1, 1);
  else if (partstruct_type != 3) // no sub partition
  {
    unsigned int partIdx = partstruct_def[partstruct_type][srcrow>>1];
    if (need_L0)
    {
      if (idx == 5 || idx == 15)
        refIdx_cache[0][idx] = mb_attr->ref_idx_l0[partIdx] - (scaler_biased==1);
      mv_cache[0][idx][0] = mb_attr->mvd[0][partIdx][0][0];
      mv_cache[0][idx][1] = mb_attr->mvd[0][partIdx][0][1];
      mv_cache[0][idx][1] = im_scale2_biased(mv_cache[0][idx][1], scaler_biased);
    }
    if (need_L1)
    {
      if (idx == 5 || idx == 15)
        refIdx_cache[1][idx] = mb_attr->ref_idx_l1[partIdx] - (scaler_biased==1);
      mv_cache[1][idx][0] = mb_attr->mvd[1][partIdx][0][0];
      mv_cache[1][idx][1] = mb_attr->mvd[1][partIdx][0][1];
      mv_cache[1][idx][1] = im_scale2_biased(mv_cache[1][idx][1], scaler_biased);
    }
  }
  else  // with sub partitions
  {
    unsigned int partIdx = partstruct_def[partstruct_type][srcrow>>1];
    unsigned int subPartIdx;
    sub_mb_type_t sub_mb_type = (sub_mb_type_t) mb_attr->sub_mb_type[partIdx];
    unsigned int subpartstruct_type = submb_partstruct_type[sub_mb_type];
    if (need_L0)
    {
      if (idx == 5 || idx == 15)
        refIdx_cache[0][idx] = mb_attr->ref_idx_l0[partIdx] - (scaler_biased==1);
      subPartIdx = partstruct_def[subpartstruct_type][srcrow&1];
      mv_cache[0][idx][0] = mb_attr->mvd[0][partIdx][subPartIdx][0];
      mv_cache[0][idx][1] = mb_attr->mvd[0][partIdx][subPartIdx][1];
      mv_cache[0][idx][1] = im_scale2_biased(mv_cache[0][idx][1], scaler_biased);
    }
    if (need_L1)
    {
      if (idx == 5 || idx == 15)
        refIdx_cache[1][idx] = mb_attr->ref_idx_l1[partIdx] - (scaler_biased==1);
      subPartIdx = partstruct_def[subpartstruct_type][srcrow&1];
      mv_cache[1][idx][0] = mb_attr->mvd[1][partIdx][subPartIdx][0];
      mv_cache[1][idx][1] = mb_attr->mvd[1][partIdx][subPartIdx][1];
      mv_cache[1][idx][1] = im_scale2_biased(mv_cache[1][idx][1], scaler_biased);
    }
  }
}


void fills_in_interpred_cache(struct rv264macro* curr_mb_attr, unsigned int PicWidthInMbs,
    int16_t mv_cache[2][25][2], int8_t refIdx_cache[2][25], unsigned int need_L0, unsigned int need_L1, unsigned int has_sub_parts)
{
    // Fills in 1st row
    if (!curr_mb_attr->up_mb_is_available)
      reset_line(mv_cache, refIdx_cache, 1, 1, need_L0, need_L1, 0);
    else
    {
      struct rv264macro* upper_mb = curr_mb_attr - PicWidthInMbs;
      set_line(mv_cache, refIdx_cache, upper_mb, 1, need_L0, need_L1, has_sub_parts, 0);
    }

    // Fills in left column
    if (!curr_mb_attr->left_mb_is_available)
    {
      reset_line(mv_cache, refIdx_cache, 5, 5, need_L0, need_L1, 0);
    }
    else
    {
      struct rv264macro* left_mb = curr_mb_attr - 1;
      set_line(mv_cache, refIdx_cache, left_mb, 0, need_L0, need_L1, has_sub_parts, 0);
    }
}
void fills_in_interpred_cache_mbaff(struct rv264macro* curr_mb_attr, unsigned int curr_is_field, unsigned int curr_is_bot,
    unsigned int PicWidthInMbs, int16_t mv_cache[2][25][2], int8_t refIdx_cache[2][25], unsigned int need_L0, unsigned int need_L1,
    unsigned int need_sub_parts)
{
  // Fills in 1st row
  if (!curr_mb_attr->up_mb_is_available)
    reset_line(mv_cache, refIdx_cache, 1, 1, need_L0, need_L1, 0);
  else
  {
    unsigned int up_mb_is_field = curr_mb_attr->up_mb_is_field;
    struct rv264macro* upper_mb = get_up_mbaff_mb(curr_mb_attr, PicWidthInMbs, curr_is_field, up_mb_is_field, curr_is_bot);
    set_line(mv_cache, refIdx_cache, upper_mb, 1, need_L0, need_L1, need_sub_parts, (curr_is_field<<1)+up_mb_is_field);
  }

  // Fills in left column
  if (!curr_mb_attr->left_mb_is_available)
  {
    reset_line(mv_cache, refIdx_cache, 5, 5, need_L0, need_L1, 0);
  }
  else
  {
    unsigned int left_mb_is_field = curr_mb_attr->left_mb_is_field;
    struct rv264macro* left_mb;
    if (left_mb_is_field == curr_is_field)
    {
      left_mb = curr_mb_attr - 1;
      set_line(mv_cache, refIdx_cache, left_mb, 0, need_L0, need_L1, need_sub_parts, 0);
    }
    else if (curr_is_field)
    {
      left_mb = curr_mb_attr - 1 - PicWidthInMbs*curr_is_bot;
      set_one_vert_entry_mbaff(mv_cache, refIdx_cache, left_mb,  5, 0, need_L0, need_L1, 2);
      if (need_sub_parts)
        set_one_vert_entry_mbaff(mv_cache, refIdx_cache, left_mb, 10, 2, need_L0, need_L1, 2);
      left_mb += PicWidthInMbs;
      set_one_vert_entry_mbaff(mv_cache, refIdx_cache, left_mb, 15, 0, need_L0, need_L1, 2);
      if (need_sub_parts)
        set_one_vert_entry_mbaff(mv_cache, refIdx_cache, left_mb, 20, 2, need_L0, need_L1, 2);
    }
    else // left_is_field
    {
      left_mb = curr_mb_attr - 1 - PicWidthInMbs*curr_is_bot;
      set_one_vert_entry_mbaff(mv_cache, refIdx_cache, left_mb,  5, curr_is_bot*2+0, need_L0, need_L1, 1);
      if (need_sub_parts)
        set_one_vert_entry_mbaff(mv_cache, refIdx_cache, left_mb, 10, curr_is_bot*2+0, need_L0, need_L1, 1);
      set_one_vert_entry_mbaff(mv_cache, refIdx_cache, left_mb, 15, curr_is_bot*2+1, need_L0, need_L1, 1);
      if (need_sub_parts)
        set_one_vert_entry_mbaff(mv_cache, refIdx_cache, left_mb, 20, curr_is_bot*2+1, need_L0, need_L1, 1);
    }
  }
}



void sub_mb_pred_cavlc(struct bitbuf* bs, mb_type_t mb_type, sub_mb_type_t* sub_mb_type,
                                     unsigned int MbaffFrameFlag, unsigned int mb_field_decoding_flag,
                                     uint8_t num_ref_idx_active[2], struct rv264macro* curr_mb_attr)
{
  unsigned int mbPartIdx, subMbPartIdx;
  unsigned int range_l0;
  unsigned int range_l1;
  int8_t* ref_idx_l0 = &curr_mb_attr->ref_idx_l0[0];
  int8_t* ref_idx_l1 = &curr_mb_attr->ref_idx_l1[0];
  int16_t* mvd_l0 = &curr_mb_attr->mvd[0][0][0][0];
  int16_t* mvd_l1 = &curr_mb_attr->mvd[1][0][0][0];


  range_l0 = ((MbaffFrameFlag && mb_field_decoding_flag)+1) * num_ref_idx_active[0] -1;
  range_l1 = ((MbaffFrameFlag && mb_field_decoding_flag)+1) * num_ref_idx_active[1] -1;

  if (mb_type < B_Direct_16x16) // we have a type P macroblock
    for( mbPartIdx = 0; mbPartIdx < 4; mbPartIdx++ )
    {
      sub_mb_type[ mbPartIdx ] = (sub_mb_type_t)uexpbits_ludh264(bs, "sub_mb_type"); // 2 ue(v) | ae(v)
      LUD_DEBUG_ASSERT(sub_mb_type[ mbPartIdx ] < NB_OF_SUB_MB_TYPE);
    }
  else // we have a type B macroblock
    for( mbPartIdx = 0; mbPartIdx < 4; mbPartIdx++ )
    {
      sub_mb_type[ mbPartIdx ] = (sub_mb_type_t)(uexpbits(bs) + (unsigned int)B_Direct_8x8); // 2 ue(v) | ae(v)
    }

  if( ( num_ref_idx_active[0] > 1 || (MbaffFrameFlag == 1 && mb_field_decoding_flag) ) && mb_type != P_8x8ref0 )
  {
    for( mbPartIdx = 0; mbPartIdx < 4; mbPartIdx++ )
      if( sub_mb_type[ mbPartIdx ] != B_Direct_8x8 &&
          SubMbPredMode( sub_mb_type[ mbPartIdx ] ) != Pred_L1 )
        ref_idx_l0[ mbPartIdx ] = texpbits(bs, range_l0); // 2 te(v) | ae(v)
  }
  else
    ref_idx_l0[0] = ref_idx_l0[1] = ref_idx_l0[2] = ref_idx_l0[3] = 0;

  if( (num_ref_idx_active[1] > 1 || (MbaffFrameFlag == 1 && mb_field_decoding_flag) ) )
  {
    for( mbPartIdx = 0; mbPartIdx < 4; mbPartIdx++ )
      if( sub_mb_type[ mbPartIdx ] != B_Direct_8x8 &&
          SubMbPredMode( sub_mb_type[ mbPartIdx ] ) != Pred_L0 )
        ref_idx_l1[ mbPartIdx ] = texpbits(bs, range_l1); // 2 te(v) | ae(v)
  }
  else
    ref_idx_l1[0] = ref_idx_l1[1] = ref_idx_l1[2] = ref_idx_l1[3] = 0;


  for( mbPartIdx = 0; mbPartIdx < 4; mbPartIdx++)
  {
    unsigned int index = mbPartIdx * 4 * 2;
    if( sub_mb_type[ mbPartIdx ] != B_Direct_8x8 && SubMbPredMode( sub_mb_type[ mbPartIdx ] ) != Pred_L1 )
    {
      for( subMbPartIdx = 0; subMbPartIdx < NumSubMbPart( sub_mb_type[ mbPartIdx ] ); subMbPartIdx++, index+=2)
      {
        mvd_l0[ index ] = sexpbits_ludh264(bs, "mvd_l0"); // 2 se(v) | ae(v)
        mvd_l0[ index +1 ] = sexpbits_ludh264(bs, "mvd_l0"); // 2 se(v) | ae(v)
      }
    }
  }

  for( mbPartIdx = 0; mbPartIdx < 4; mbPartIdx++)
  {
    unsigned int index = mbPartIdx * 4 * 2;
    if( sub_mb_type[ mbPartIdx ] != B_Direct_8x8 && SubMbPredMode( sub_mb_type[ mbPartIdx ] ) != Pred_L0 )
    {
      for( subMbPartIdx = 0; subMbPartIdx < NumSubMbPart( sub_mb_type[ mbPartIdx ] ); subMbPartIdx++, index+=2)
      {
        mvd_l1[ index ] = sexpbits(bs); // 2 se(v) | ae(v)
        mvd_l1[ index +1 ] = sexpbits(bs); // 2 se(v) | ae(v)
      }
    }
  }
}


void sub_mb_pred_cabac(CABACContext* cabac_ctx, struct bitbuf* bs, mb_type_t mb_type, sub_mb_type_t* sub_mb_type, unsigned int curr_is_bot,
                                     unsigned int MbaffFrameFlag, unsigned int mb_field_decoding_flag, unsigned int PicWidthInMbs,
                                     uint8_t num_ref_idx_active[2], struct rv264macro* curr_mb_attr)
{
  unsigned int mbPartIdx, subMbPartIdx;
  int16_t mv_cache[2][25][2] declare_aligned(4);
  int8_t refIdx_cache[2][25];
  int r5x5idx;
  int8_t* ref_idx_l0 = &curr_mb_attr->ref_idx_l0[0];
  int8_t* ref_idx_l1 = &curr_mb_attr->ref_idx_l1[0];
  int16_t* mvd_l0 = &curr_mb_attr->mvd[0][0][0][0];
  int16_t* mvd_l1 = &curr_mb_attr->mvd[1][0][0][0];

  // TODO FIXME Optimization: better check on need_LX (maybe only based on slice type) + hardcode the calls with fixed values in fills_in functions
  unsigned int need_L0 = num_ref_idx_active[0] > 0;
  unsigned int need_L1 = num_ref_idx_active[1] > 0;

  unsigned int partstruct_type = 3; // this mb has sub partitions so its struct type is 3 !
  const uint8_t* xoff = leftpartoffset[partstruct_type];
  const uint8_t* yoff = uppartoffset[partstruct_type];
  const uint8_t* r5x5 = partr5x5idx[partstruct_type];

  //memset(&curr_mb_attr->RefIdxL[0][0], 0, sizeof(curr_mb_attr->RefIdxL));
  //memset(&curr_mb_attr->MvL[0][0][0], 0, sizeof(curr_mb_attr->MvL));
  //memset(mv_cache, 0, sizeof(mv_cache));
  //memset(refIdx_cache, 0, sizeof(refIdx_cache));

  if (!MbaffFrameFlag)
    fills_in_interpred_cache(curr_mb_attr, PicWidthInMbs, mv_cache, refIdx_cache, need_L0, need_L1, 1);
  else
    fills_in_interpred_cache_mbaff(curr_mb_attr, mb_field_decoding_flag,curr_is_bot,
        PicWidthInMbs, mv_cache, refIdx_cache, need_L0, need_L1, 1);


  for( mbPartIdx = 0; mbPartIdx < 4; mbPartIdx++ )
    sub_mb_type[ mbPartIdx ] = cabac_decode_sub_mb_type(cabac_ctx, bs, mb_type >= B_Direct_16x16); // 2 ue(v) | ae(v)


  if(num_ref_idx_active[0] > 1 || (MbaffFrameFlag == 1 && mb_field_decoding_flag))
  {
    for( mbPartIdx = 0; mbPartIdx < 4; mbPartIdx++ )
    {
      r5x5idx = r5x5[mbPartIdx];
      if( sub_mb_type[ mbPartIdx ] != B_Direct_8x8 && SubMbPredMode( sub_mb_type[ mbPartIdx ] ) != Pred_L1 )
        refIdx_cache[0][r5x5idx] = ref_idx_l0[ mbPartIdx ] = cabac_decode_ref_idx_lX(cabac_ctx, bs, refIdx_cache[0], r5x5idx, xoff[mbPartIdx], yoff[mbPartIdx]); // 2 te(v) | ae(v)
      else
        refIdx_cache[0][r5x5idx] = ref_idx_l0[ mbPartIdx ] = 0;
    }
  }
  else
    memset(ref_idx_l0, 0, 4);


  if(num_ref_idx_active[1] > 1 || (MbaffFrameFlag == 1 && mb_field_decoding_flag))
  {
    for( mbPartIdx = 0; mbPartIdx < 4; mbPartIdx++ )
    {
      r5x5idx = r5x5[mbPartIdx];
      if( sub_mb_type[ mbPartIdx ] != B_Direct_8x8 && SubMbPredMode( sub_mb_type[ mbPartIdx ] ) != Pred_L0 )
        refIdx_cache[1][r5x5idx] = ref_idx_l1[ mbPartIdx ] = cabac_decode_ref_idx_lX(cabac_ctx, bs, refIdx_cache[1], r5x5idx, xoff[mbPartIdx], yoff[mbPartIdx]); // 2 te(v) | ae(v)
      else
        refIdx_cache[1][r5x5idx] = ref_idx_l1[ mbPartIdx ] = 0;
    }
  }
  else
    memset(ref_idx_l1, 0, 4);



  for( mbPartIdx = 0; mbPartIdx < 4; mbPartIdx++)
  {
    unsigned int index = mbPartIdx * 4 * 2;
    sub_mb_type_t smb_type =  sub_mb_type[ mbPartIdx ];
    if( smb_type != B_Direct_8x8 && SubMbPredMode( smb_type ) != Pred_L1 )
    {
      unsigned int subpartstruct_type = submb_partstruct_type[smb_type];
      unsigned int pr5x5idx = r5x5[mbPartIdx];
      for( subMbPartIdx = 0; subMbPartIdx < NumSubMbPart( smb_type ); subMbPartIdx++, index+=2)
      {
        r5x5idx = pr5x5idx + partr5x5idxoffset[subpartstruct_type][subMbPartIdx];
        mv_cache[0][r5x5idx][0] = mvd_l0[ index ] = cabac_decode_mvd_lX(cabac_ctx, bs, mv_cache[0], r5x5idx, 0, 1, 5); // 2 se(v) | ae(v)
        mv_cache[0][r5x5idx][1] = mvd_l0[ index +1 ] = cabac_decode_mvd_lX(cabac_ctx, bs, mv_cache[0], r5x5idx, 1, 1, 5); // 2 se(v) | ae(v)
        if (SubMbPartWidth(smb_type) > 1)
        {
          mv_cache[0][r5x5idx+1][0] = mv_cache[0][r5x5idx][0];
          mv_cache[0][r5x5idx+1][1] = mv_cache[0][r5x5idx][1];
        }
        if (SubMbPartHeight(smb_type) > 1)
        {
          mv_cache[0][r5x5idx+5][0] = mv_cache[0][r5x5idx][0];
          mv_cache[0][r5x5idx+5][1] = mv_cache[0][r5x5idx][1];
        }
        if (SubMbPartWidth(smb_type) > 1 && SubMbPartHeight(smb_type) > 1)
        {
          mv_cache[0][r5x5idx+6][0] = mv_cache[0][r5x5idx][0];
          mv_cache[0][r5x5idx+6][1] = mv_cache[0][r5x5idx][1];
        }
      }
    }
    else
    {
      unsigned int r5x5idx = r5x5[mbPartIdx];
      mv_cache[0][r5x5idx+0][0] = mv_cache[0][r5x5idx+0][1] = 0;
      mv_cache[0][r5x5idx+1][0] = mv_cache[0][r5x5idx+1][1] = 0;
      mv_cache[0][r5x5idx+5][0] = mv_cache[0][r5x5idx+5][1] = 0;
      mv_cache[0][r5x5idx+6][0] = mv_cache[0][r5x5idx+6][1] = 0;
      mvd_l0[index] = mvd_l0[index+1] = mvd_l0[index+2] = mvd_l0[index+3] = mvd_l0[index+4] = mvd_l0[index+5] = mvd_l0[index+6] = mvd_l0[index+7] = 0;
    }
  }

  for( mbPartIdx = 0; mbPartIdx < 4; mbPartIdx++)
  {
    unsigned int index = mbPartIdx * 4 * 2;
    sub_mb_type_t smb_type =  sub_mb_type[ mbPartIdx ];
    if( smb_type != B_Direct_8x8 && SubMbPredMode( smb_type ) != Pred_L0 )
    {
      unsigned int subpartstruct_type = submb_partstruct_type[smb_type];
      unsigned int pr5x5idx = r5x5[mbPartIdx];
      for( subMbPartIdx = 0; subMbPartIdx < NumSubMbPart( smb_type ); subMbPartIdx++, index+=2)
      {
        r5x5idx = pr5x5idx + partr5x5idxoffset[subpartstruct_type][subMbPartIdx];
        mv_cache[1][r5x5idx][0] = mvd_l1[ index ] = cabac_decode_mvd_lX(cabac_ctx, bs, mv_cache[1], r5x5idx, 0, 1, 5); // 2 se(v) | ae(v)
        mv_cache[1][r5x5idx][1] = mvd_l1[ index +1 ] = cabac_decode_mvd_lX(cabac_ctx, bs, mv_cache[1], r5x5idx, 1, 1, 5); // 2 se(v) | ae(v)
        if (SubMbPartWidth(smb_type) > 1)
        {
          mv_cache[1][r5x5idx+1][0] = mv_cache[1][r5x5idx][0];
          mv_cache[1][r5x5idx+1][1] = mv_cache[1][r5x5idx][1];
        }
        if (SubMbPartHeight(smb_type) > 1)
        {
          mv_cache[1][r5x5idx+5][0] = mv_cache[1][r5x5idx][0];
          mv_cache[1][r5x5idx+5][1] = mv_cache[1][r5x5idx][1];
        }
        if (SubMbPartWidth(smb_type) > 1 && SubMbPartHeight(smb_type) > 1)
        {
          mv_cache[1][r5x5idx+6][0] = mv_cache[1][r5x5idx][0];
          mv_cache[1][r5x5idx+6][1] = mv_cache[1][r5x5idx][1];
        }
      }
    }
    else
    {
      unsigned int r5x5idx = r5x5[mbPartIdx];
      mv_cache[1][r5x5idx+0][0] = mv_cache[1][r5x5idx+0][1] = 0;
      mv_cache[1][r5x5idx+1][0] = mv_cache[1][r5x5idx+1][1] = 0;
      mv_cache[1][r5x5idx+5][0] = mv_cache[1][r5x5idx+5][1] = 0;
      mv_cache[1][r5x5idx+6][0] = mv_cache[1][r5x5idx+6][1] = 0;
      mvd_l1[index] = mvd_l1[index+1] = mvd_l1[index+2] = mvd_l1[index+3] = mvd_l1[index+4] = mvd_l1[index+5] = mvd_l1[index+6] = mvd_l1[index+7] = 0;

    }
  }
}



intra_4x4_pred_mode_t intra4x4_pred_mode(intra_4x4_pred_mode_t intra_pred_cache[25], int lumaBlkIdx)
{
  unsigned int index;
  unsigned int pred_mode;
  unsigned int v;

  index = scan4x4toraster5x5[lumaBlkIdx];
  v = im_min(intra_pred_cache[index-1], intra_pred_cache[index-5])+1;
  LUD_DEBUG_ASSERT(v <= 9);
  pred_mode = pred_mode_conv[v];
  return (intra_4x4_pred_mode_t)pred_mode;
}

intra_4x4_pred_mode_t intra8x8_pred_mode(int8_t intra_pred_cache[9], int lumaBlkIdx)
{
  unsigned int index;
  unsigned int pred_mode;
  unsigned int v;

  index = raster2x4toraster3x5[lumaBlkIdx];
  v = im_min(intra_pred_cache[index-1], intra_pred_cache[index-3])+1;
  LUD_DEBUG_ASSERT(v <= 9);
  pred_mode = pred_mode_conv[v];
  return (intra_4x4_pred_mode_t)pred_mode;
}

unsigned int decode_prev_intra_pred_mode_flag(CABACContext* cabac_ctx, struct bitbuf* bs, unsigned int entropy_coding_mode_flag)
{
  if (!entropy_coding_mode_flag)
    return getbit(bs);
  else
    return cabac_decode_prev_intra_pred_mode_flag(cabac_ctx, bs);
}

intra_4x4_pred_mode_t decode_rem_intra_pred_mode(CABACContext* cabac_ctx, struct bitbuf* bs, unsigned int entropy_coding_mode_flag)
{
  if (!entropy_coding_mode_flag)
    return intra_4x4_pred_mode_t(getbits(bs, 3));
  else
    return cabac_decode_rem_intra_pred_mode(cabac_ctx, bs);
}

intra_chroma_pred_mode_t decode_intra_chroma_pred_mode(CABACContext* cabac_ctx, struct bitbuf* bs, unsigned int entropy_coding_mode_flag,
    struct rv264macro* curr)
{
  if (!entropy_coding_mode_flag)
    return intra_chroma_pred_mode_t(uexpbits(bs));
  else
    return cabac_decode_intra_chroma_pred_mode(cabac_ctx, bs, curr);
}

void mb_pred_cavlc(struct bitbuf* bs, mb_type_t mb_type, struct rv264macro* curr_mb_attr,
    unsigned int MbaffFrameFlag, unsigned int mb_field_decoding_flag, uint8_t num_ref_idx_active[2])
{
  unsigned int range_l0;
  unsigned int range_l1;
  unsigned int index;
  unsigned int mbPartIdx;
  int8_t* ref_idx_l0 = &curr_mb_attr->ref_idx_l0[0];
  int8_t* ref_idx_l1 = &curr_mb_attr->ref_idx_l1[0];
  int16_t* mvd_l0 = &curr_mb_attr->mvd[0][0][0][0];
  int16_t* mvd_l1 = &curr_mb_attr->mvd[1][0][0][0];


  range_l0 = ((MbaffFrameFlag && mb_field_decoding_flag)+1) * num_ref_idx_active[0] -1;
  range_l1 = ((MbaffFrameFlag && mb_field_decoding_flag)+1) * num_ref_idx_active[1] -1;

  if( ( num_ref_idx_active[0] > 1 || (MbaffFrameFlag && mb_field_decoding_flag) ) )
  {
    for( mbPartIdx = 0; mbPartIdx < NumMbPart( mb_type ); mbPartIdx++)
      if( MbPartPredMode( mb_type, mbPartIdx, 0, 0) != Pred_L1 )
        ref_idx_l0[ mbPartIdx ] = texpbits(bs, range_l0); // 2 te(v) | ae(v)
  }
  else
    ref_idx_l0[0] = ref_idx_l0[1] = ref_idx_l0[2] = ref_idx_l0[3] = 0;

  if( ( num_ref_idx_active[1] > 1 || (MbaffFrameFlag && mb_field_decoding_flag) ) )
  {
    for( mbPartIdx = 0; mbPartIdx < NumMbPart( mb_type ); mbPartIdx++)
      if( MbPartPredMode( mb_type, mbPartIdx, 0, 0) != Pred_L0 )
        ref_idx_l1[ mbPartIdx ] = texpbits(bs, range_l1); // 2 te(v) | ae(v)
  }
  else
    ref_idx_l1[0] = ref_idx_l1[1] = ref_idx_l1[2] = ref_idx_l1[3] = 0;


  for( mbPartIdx = 0, index = 0; mbPartIdx < NumMbPart( mb_type ); mbPartIdx++, index += 8)
  {
    if( MbPartPredMode ( mb_type, mbPartIdx , 0, 0) != Pred_L1 )
    {
      mvd_l0[ index ] = sexpbits(bs); // 2 se(v) | ae(v)
      mvd_l0[ index+1 ] = sexpbits(bs); // 2 se(v) | ae(v)
    }
  }
  for( mbPartIdx = 0, index = 0; mbPartIdx < NumMbPart( mb_type ); mbPartIdx++, index += 8)
  {
    if( MbPartPredMode( mb_type, mbPartIdx , 0, 0) != Pred_L0 )
    {
      mvd_l1[ index ] = sexpbits(bs); // 2 se(v) | ae(v)
      mvd_l1[ index+1 ] = sexpbits(bs); // 2 se(v) | ae(v)
    }
  }
}

void mb_pred_cabac(CABACContext* cabac_ctx, struct bitbuf* bs, struct rv264macro* curr_mb_attr, mb_type_t mb_type, unsigned int curr_is_bot,
    unsigned int MbaffFrameFlag, unsigned int mb_field_decoding_flag, uint8_t num_ref_idx_active[2], unsigned int PicWidthInMbs)
{
  int r5x5idx;
  unsigned int mbPartIdx;
  unsigned int index;
  int16_t mv_cache[2][25][2] declare_aligned(4);
  int8_t refIdx_cache[2][25];
  int8_t* ref_idx_l0 = &curr_mb_attr->ref_idx_l0[0];
  int8_t* ref_idx_l1 = &curr_mb_attr->ref_idx_l1[0];
  int16_t* mvd_l0 = &curr_mb_attr->mvd[0][0][0][0];
  int16_t* mvd_l1 = &curr_mb_attr->mvd[1][0][0][0];

  // TODO FIXME Optimization: better check on need_LX (maybe only based on slice type) + hardcode the calls with fixed values in fills_in functions
  unsigned int need_L0 = num_ref_idx_active[0] > 0;
  unsigned int need_L1 = num_ref_idx_active[1] > 0;

  unsigned int partstruct_type = mb_partstruct_type[mb_type];
  const uint8_t* xoff = leftpartoffset[partstruct_type];
  const uint8_t* yoff = uppartoffset[partstruct_type];
  const uint8_t* r5x5 = partr5x5idx[partstruct_type];


  if (!MbaffFrameFlag)
    fills_in_interpred_cache(curr_mb_attr, PicWidthInMbs, mv_cache, refIdx_cache, need_L0, need_L1, 0);
  else
    fills_in_interpred_cache_mbaff(curr_mb_attr, mb_field_decoding_flag, curr_is_bot,
        PicWidthInMbs, mv_cache, refIdx_cache, need_L0, need_L1, 0);


  if( ( num_ref_idx_active[0] > 1 || (MbaffFrameFlag && mb_field_decoding_flag) ) )
  {
    for( mbPartIdx = 0; mbPartIdx < NumMbPart( mb_type ); mbPartIdx++)
    {
      r5x5idx = r5x5[mbPartIdx];
      if( MbPartPredMode( mb_type, mbPartIdx, 0, 0) != Pred_L1 )
        refIdx_cache[0][r5x5idx] = ref_idx_l0[ mbPartIdx ] = cabac_decode_ref_idx_lX(cabac_ctx, bs, refIdx_cache[0], r5x5idx, xoff[mbPartIdx], yoff[mbPartIdx]); // 2 te(v) | ae(v)
      else
        refIdx_cache[0][r5x5idx] = ref_idx_l0[ mbPartIdx ] = 0;
    }
  }
  else
    memset(ref_idx_l0, 0, 4);


  if( ( num_ref_idx_active[1] > 1 || (MbaffFrameFlag && mb_field_decoding_flag) ) )
  {
    for( mbPartIdx = 0; mbPartIdx < NumMbPart( mb_type ); mbPartIdx++)
    {
      r5x5idx = r5x5[mbPartIdx];
      if( MbPartPredMode( mb_type, mbPartIdx, 0, 0) != Pred_L0 )
        refIdx_cache[1][r5x5idx] = ref_idx_l1[ mbPartIdx ] = cabac_decode_ref_idx_lX(cabac_ctx, bs, refIdx_cache[1], r5x5idx, xoff[mbPartIdx], yoff[mbPartIdx]); // 2 te(v) | ae(v)
      else
        refIdx_cache[1][r5x5idx] = ref_idx_l1[ mbPartIdx ] = 0;
    }
  }
  else
    memset(ref_idx_l1, 0, 4);


  for( mbPartIdx = 0, index = 0; mbPartIdx < NumMbPart( mb_type ); mbPartIdx++, index += 8)
  {
    if( MbPartPredMode ( mb_type, mbPartIdx , 0, 0) != Pred_L1 )
    {
      r5x5idx = r5x5[mbPartIdx];
      mv_cache[0][r5x5idx][0] = mvd_l0[ index ] = cabac_decode_mvd_lX(cabac_ctx, bs, mv_cache[0], r5x5idx, 0, xoff[mbPartIdx], yoff[mbPartIdx]); // 2 se(v) | ae(v)
      mv_cache[0][r5x5idx][1] = mvd_l0[ index+1 ] = cabac_decode_mvd_lX(cabac_ctx, bs, mv_cache[0], r5x5idx, 1, xoff[mbPartIdx], yoff[mbPartIdx]); // 2 se(v) | ae(v)
    }
    else
    {
      unsigned int r5x5idx = r5x5[mbPartIdx];
      mv_cache[0][r5x5idx][0] = mv_cache[0][r5x5idx][1] = 0;
      mvd_l0[index] = mvd_l0[index+1] = 0;
    }
  }
  for( mbPartIdx = 0, index = 0; mbPartIdx < NumMbPart( mb_type ); mbPartIdx++, index += 8)
  {
    if( MbPartPredMode( mb_type, mbPartIdx , 0, 0) != Pred_L0 )
    {
      r5x5idx = r5x5[mbPartIdx];
      mv_cache[1][r5x5idx][0] = mvd_l1[ index ] = cabac_decode_mvd_lX(cabac_ctx, bs, mv_cache[1], r5x5idx, 0, xoff[mbPartIdx], yoff[mbPartIdx]); // 2 se(v) | ae(v)
      mv_cache[1][r5x5idx][1] = mvd_l1[ index+1 ] = cabac_decode_mvd_lX(cabac_ctx, bs, mv_cache[1], r5x5idx, 1, xoff[mbPartIdx], yoff[mbPartIdx]); // 2 se(v) | ae(v)
    }
    else
    {
      unsigned int r5x5idx = r5x5[mbPartIdx];
      mv_cache[1][r5x5idx][0] = mv_cache[1][r5x5idx][1] = 0;
      mvd_l1[index] = mvd_l1[index+1] = 0;
    }
  }

}



void mb_pred(CABACContext* cabac_ctx, struct bitbuf* bs, unsigned int entropy_coding_mode_flag, mb_type_t mb_type,
                           part_type_t MbPartPredMode0, unsigned int transform_size_8x8_flag,
                           unsigned int cfidc, unsigned int MbaffFrameFlag, unsigned int mb_field_decoding_flag, unsigned int constrained_intra_pred_flag,
                           uint8_t num_ref_idx_active[2], struct rv264macro* curr_mb_attr, unsigned int curr_is_bot, int PicWidthInMbs
                           )
{
  if( MbPartPredMode0 <= Intra_16x16 ) // Intra_4x4 Intra_8x8 or Intra_16x16
  {
    if( MbPartPredMode0 == Intra_4x4 )
    {
      unsigned int prev_intra4x4_pred_mode_flag;
      unsigned int luma4x4BlkIdx;
      intra_4x4_pred_mode_t intra_pred_cache[25];
      fills_in_intra4x4_pred_cache(curr_mb_attr, curr_is_bot, mb_field_decoding_flag, PicWidthInMbs, intra_pred_cache, MbaffFrameFlag, constrained_intra_pred_flag);
      for( luma4x4BlkIdx=0; luma4x4BlkIdx<16; luma4x4BlkIdx++ )
      {
        intra_4x4_pred_mode_t pred_mode = intra4x4_pred_mode(intra_pred_cache, luma4x4BlkIdx);
        prev_intra4x4_pred_mode_flag = decode_prev_intra_pred_mode_flag(cabac_ctx, bs, entropy_coding_mode_flag); // 2 u(1) | ae(v)
        if (!prev_intra4x4_pred_mode_flag)
        {
          curr_mb_attr->rem_intra_pred_mode[luma4x4BlkIdx] = decode_rem_intra_pred_mode(cabac_ctx, bs, entropy_coding_mode_flag); // 2 u(3) | ae(v)
          pred_mode  = intra_4x4_pred_mode_t(curr_mb_attr->rem_intra_pred_mode[luma4x4BlkIdx] + (curr_mb_attr->rem_intra_pred_mode[luma4x4BlkIdx] >= pred_mode));
        }
        else
          curr_mb_attr->rem_intra_pred_mode[luma4x4BlkIdx] = intra_4x4_pred_mode_t(-1);
        intra_pred_cache[scan4x4toraster5x5[luma4x4BlkIdx]] =
            curr_mb_attr->Intra4x4PredMode[scan4x4[luma4x4BlkIdx]] = pred_mode;
      }
    }
    else if( MbPartPredMode0 == Intra_8x8 )
    {
      unsigned int prev_intra8x8_pred_mode_flag;
      unsigned int luma8x8BlkIdx, idx;
      int8_t intra_pred_cache[9];
      fills_in_intra8x8_pred_cache(curr_mb_attr, curr_is_bot, mb_field_decoding_flag, PicWidthInMbs, intra_pred_cache, MbaffFrameFlag, constrained_intra_pred_flag);
      for( luma8x8BlkIdx=0; luma8x8BlkIdx<4; luma8x8BlkIdx++)
      {
        intra_4x4_pred_mode_t pred_mode = intra8x8_pred_mode(intra_pred_cache, luma8x8BlkIdx);
        prev_intra8x8_pred_mode_flag = decode_prev_intra_pred_mode_flag(cabac_ctx, bs, entropy_coding_mode_flag); // 2 u(1) | ae(v)
        if( !prev_intra8x8_pred_mode_flag )
        {
          curr_mb_attr->rem_intra_pred_mode[luma8x8BlkIdx] = decode_rem_intra_pred_mode(cabac_ctx, bs, entropy_coding_mode_flag); // 2 u(3) | ae(v)
          pred_mode = intra_4x4_pred_mode_t(curr_mb_attr->rem_intra_pred_mode[luma8x8BlkIdx] + (curr_mb_attr->rem_intra_pred_mode[luma8x8BlkIdx] >= pred_mode));
        }
        else
          curr_mb_attr->rem_intra_pred_mode[luma8x8BlkIdx] = intra_4x4_pred_mode_t(-1);
        intra_pred_cache[raster2x4toraster3x5[luma8x8BlkIdx]] = pred_mode;
        idx = col_scan4x4[luma8x8BlkIdx];
        curr_mb_attr->Intra4x4PredMode[idx]   = pred_mode;
        curr_mb_attr->Intra4x4PredMode[idx+1] = pred_mode;
        curr_mb_attr->Intra4x4PredMode[idx+4] = pred_mode;
        curr_mb_attr->Intra4x4PredMode[idx+5] = pred_mode;
      }
    }
    if( cfidc != 0 )
      curr_mb_attr->intra_chroma_pred_mode = decode_intra_chroma_pred_mode(cabac_ctx, bs, entropy_coding_mode_flag, curr_mb_attr); // 2 ue(v) | ae(v)
  }
  else if( MbPartPredMode0 != Direct )
  {

    if (!entropy_coding_mode_flag)
      mb_pred_cavlc(bs, mb_type, curr_mb_attr, MbaffFrameFlag, mb_field_decoding_flag, num_ref_idx_active);
    else
      mb_pred_cabac(cabac_ctx, bs, curr_mb_attr, mb_type, curr_is_bot, MbaffFrameFlag, mb_field_decoding_flag, num_ref_idx_active, PicWidthInMbs);
  }
}


// nC_cache is a 5x5 or 5x3 array that contains nC values for neighboring blocks
// table_flag==0 specify that we are in 5x5 neighboring or else that we have 5x3 neighboring
int derive_nC(uint8_t* nC_cache, unsigned int lc4x4blockIdx, unsigned int table_flag)
{
  unsigned int nC;
  int index;

  if (table_flag)
    index = raster2x4toraster5x5[lc4x4blockIdx];
  else
    index = scan4x4toraster5x5[lc4x4blockIdx];

  nC = nC_cache[index-1] + nC_cache[index-5];
  LUD_DEBUG_ASSERT(nC_cache[index-1] <= 128 && nC_cache[index-5] <= 128 ); // FIXME DEBUG
  if (nC<128)
    nC = (nC+1) >> 1;
  return nC & 63;
}


void fills_in_nC_cache_for_nonmbaff_slice(
    struct rv264macro* curr_mb_attr, int PicWidthInMbs,
    uint8_t luma_nC_cache[28], uint8_t chroma_nC_cache[2][28],
    unsigned int cfidc, mb_type_t mb_type, unsigned int constrained_intra_pred_flag, unsigned int data_partitioning_slice_flag)
{
  unsigned int common_test =  constrained_intra_pred_flag &&
                              is_IntraMb(mb_type) &&
                              data_partitioning_slice_flag;

  unsigned int iCbCr;

  // Fills in 1st row
  if (!curr_mb_attr->up_mb_is_available)
  {
    // Luma
    luma_nC_cache[1] = luma_nC_cache[2] = luma_nC_cache[3] = luma_nC_cache[4] = 128; // mean 0 and not_available_flag is set to 1
    // Chroma
    if (cfidc == 1 || cfidc == 2)
      chroma_nC_cache[0][1] = chroma_nC_cache[0][2] =
          chroma_nC_cache[1][1] = chroma_nC_cache[1][2] = 128;
    else if (cfidc == 3)
      chroma_nC_cache[0][1] = chroma_nC_cache[0][2] = chroma_nC_cache[0][3] = chroma_nC_cache[0][4] =
          chroma_nC_cache[1][1] = chroma_nC_cache[1][2] = chroma_nC_cache[1][3] = chroma_nC_cache[1][4] = 128;
  }
  else
  {
    struct rv264macro* up_mb =  curr_mb_attr - PicWidthInMbs;
    if ( !common_test || !IS_INTER(up_mb) )
    {
      // Luma
      luma_nC_cache[1] = up_mb->TotalCoeffLuma[12];
      luma_nC_cache[2] = up_mb->TotalCoeffLuma[13];
      luma_nC_cache[3] = up_mb->TotalCoeffLuma[14];
      luma_nC_cache[4] = up_mb->TotalCoeffLuma[15];
      // Chroma
      if (cfidc == 1)
        for (iCbCr = 0; iCbCr < 2; iCbCr++)
        {
          chroma_nC_cache[iCbCr][1] = up_mb->TotalCoeffChroma[iCbCr][2];
          chroma_nC_cache[iCbCr][2] = up_mb->TotalCoeffChroma[iCbCr][3];
        }
      else if (cfidc == 2)
      {
        for (iCbCr = 0; iCbCr < 2; iCbCr++)
        {
          chroma_nC_cache[iCbCr][1] = up_mb->TotalCoeffChroma[iCbCr][6];
          chroma_nC_cache[iCbCr][2] = up_mb->TotalCoeffChroma[iCbCr][7];
        }
      }
      else if (cfidc == 3)
        for (iCbCr = 0; iCbCr < 2; iCbCr++)
        {
          chroma_nC_cache[iCbCr][1] = up_mb->TotalCoeffChroma[iCbCr][12];
          chroma_nC_cache[iCbCr][2] = up_mb->TotalCoeffChroma[iCbCr][13];
          chroma_nC_cache[iCbCr][3] = up_mb->TotalCoeffChroma[iCbCr][14];
          chroma_nC_cache[iCbCr][4] = up_mb->TotalCoeffChroma[iCbCr][15];
        }
    }
    else
    {
      // Luma
      luma_nC_cache[1] = luma_nC_cache[2] = luma_nC_cache[3] = luma_nC_cache[4] = 0;
      // Chroma
      if (cfidc == 1 || cfidc == 2)
        chroma_nC_cache[0][1] = chroma_nC_cache[0][2] = chroma_nC_cache[1][1] = chroma_nC_cache[1][2] = 0;
      else if (cfidc == 3)
        chroma_nC_cache[0][1] = chroma_nC_cache[0][2] = chroma_nC_cache[0][3] = chroma_nC_cache[0][4] =
            chroma_nC_cache[1][1] = chroma_nC_cache[1][2] = chroma_nC_cache[1][3] = chroma_nC_cache[1][4] = 0;

    }
  }

  // Fills in 1st column
  if (!curr_mb_attr->left_mb_is_available)
  {
    // Luma
    luma_nC_cache[5] = luma_nC_cache[10] = luma_nC_cache[15] = luma_nC_cache[20] = 128; // mean 0 and not_available_flag is set to 1
    // Chroma
    if (cfidc == 1)
      chroma_nC_cache[0][5] = chroma_nC_cache[0][10] =
          chroma_nC_cache[1][5] = chroma_nC_cache[1][10] = 128;
    else if (cfidc == 2 || cfidc == 3)
      chroma_nC_cache[0][5] = chroma_nC_cache[0][10] = chroma_nC_cache[0][15] = chroma_nC_cache[0][20] =
          chroma_nC_cache[1][5] = chroma_nC_cache[1][10] = chroma_nC_cache[1][15] = chroma_nC_cache[1][20] = 128;
  }
  else
  {
    struct rv264macro* left_mb =  curr_mb_attr - 1;
    if ( !common_test || !IS_INTER(left_mb) )
    {
      // Luma
      luma_nC_cache[5]  = left_mb->TotalCoeffLuma[3];
      luma_nC_cache[10] = left_mb->TotalCoeffLuma[7];
      luma_nC_cache[15] = left_mb->TotalCoeffLuma[11];
      luma_nC_cache[20] = left_mb->TotalCoeffLuma[15];
      // Chroma
      if (cfidc == 1)
        for (iCbCr = 0; iCbCr < 2; iCbCr++)
        {
          chroma_nC_cache[iCbCr][5]  = left_mb->TotalCoeffChroma[iCbCr][1];
          chroma_nC_cache[iCbCr][10] = left_mb->TotalCoeffChroma[iCbCr][3];
        }
      else if (cfidc == 2)
        for (iCbCr = 0; iCbCr < 2; iCbCr++)
        {
          chroma_nC_cache[iCbCr][5]  = left_mb->TotalCoeffChroma[iCbCr][1];
          chroma_nC_cache[iCbCr][10] = left_mb->TotalCoeffChroma[iCbCr][3];
          chroma_nC_cache[iCbCr][15] = left_mb->TotalCoeffChroma[iCbCr][5];
          chroma_nC_cache[iCbCr][20] = left_mb->TotalCoeffChroma[iCbCr][7];
        }
      else if (cfidc == 3)
        for (iCbCr = 0; iCbCr < 2; iCbCr++)
        {
          chroma_nC_cache[iCbCr][5]  = left_mb->TotalCoeffChroma[iCbCr][3];
          chroma_nC_cache[iCbCr][10] = left_mb->TotalCoeffChroma[iCbCr][7];
          chroma_nC_cache[iCbCr][15] = left_mb->TotalCoeffChroma[iCbCr][11];
          chroma_nC_cache[iCbCr][20] = left_mb->TotalCoeffChroma[iCbCr][15];
        }
    }
    else
    {
      // Luma
      luma_nC_cache[5] = luma_nC_cache[10] = luma_nC_cache[15] = luma_nC_cache[20] = 0;
      // Chroma
      if (cfidc == 1)
        chroma_nC_cache[0][5] = chroma_nC_cache[0][10] =
            chroma_nC_cache[1][5] = chroma_nC_cache[1][10] = 0;
      else if (cfidc == 2 || cfidc == 3)
        chroma_nC_cache[0][5] = chroma_nC_cache[0][10] = chroma_nC_cache[0][15] = chroma_nC_cache[0][20] =
            chroma_nC_cache[1][5] = chroma_nC_cache[1][10] = chroma_nC_cache[1][15] = chroma_nC_cache[1][20] = 0;
    }
  }
}


// This is the same function as above, but is is called when MbaffFrameFlag==1
void fills_in_nC_cache_for_mbaff_slice(
    struct rv264macro* curr_mb_attr, unsigned int curr_is_bot, unsigned int curr_is_field, int PicWidthInMbs,
    uint8_t luma_nC_cache[28], uint8_t chroma_nC_cache[2][28],
    unsigned int cfidc, mb_type_t mb_type, unsigned int constrained_intra_pred_flag, unsigned int data_partitioning_slice_flag)
{
  unsigned int common_test =  constrained_intra_pred_flag &&
                              is_IntraMb(mb_type) &&
                              data_partitioning_slice_flag;

  unsigned int iCbCr;

  // fills in the 1st row
  if (!curr_mb_attr->up_mb_is_available)
  {
    // Luma
    luma_nC_cache[1] = luma_nC_cache[2] = luma_nC_cache[3] = luma_nC_cache[4] = 128; // mean 0 and not_available_flag is set to 1
    // Chroma
    if (cfidc == 1 || cfidc == 2)
      chroma_nC_cache[0][1] = chroma_nC_cache[0][2] =
          chroma_nC_cache[1][1] = chroma_nC_cache[1][2] = 128;
    else if (cfidc == 3)
      chroma_nC_cache[0][1] = chroma_nC_cache[0][2] = chroma_nC_cache[0][3] = chroma_nC_cache[0][4] =
          chroma_nC_cache[1][1] = chroma_nC_cache[1][2] = chroma_nC_cache[1][3] = chroma_nC_cache[1][4] = 128;
  }
  else
  {
    struct rv264macro* upper_mb_attr = get_up_mbaff_mb(curr_mb_attr, PicWidthInMbs, curr_is_field, curr_mb_attr->up_mb_is_field, curr_is_bot);
    if (!common_test || !IS_INTER(upper_mb_attr))
    {
      // Luma
      luma_nC_cache[1] = upper_mb_attr->TotalCoeffLuma[12];
      luma_nC_cache[2] = upper_mb_attr->TotalCoeffLuma[13];
      luma_nC_cache[3] = upper_mb_attr->TotalCoeffLuma[14];
      luma_nC_cache[4] = upper_mb_attr->TotalCoeffLuma[15];
      // Chroma
      if (cfidc == 1)
        for (iCbCr = 0; iCbCr < 2; iCbCr++)
        {
          chroma_nC_cache[iCbCr][1] = upper_mb_attr->TotalCoeffChroma[iCbCr][2];
          chroma_nC_cache[iCbCr][2] = upper_mb_attr->TotalCoeffChroma[iCbCr][3];
        }
      else if (cfidc == 2)
        for (iCbCr = 0; iCbCr < 2; iCbCr++)
        {
          chroma_nC_cache[iCbCr][1] = upper_mb_attr->TotalCoeffChroma[iCbCr][6];
          chroma_nC_cache[iCbCr][2] = upper_mb_attr->TotalCoeffChroma[iCbCr][7];
        }
      else if (cfidc == 3)
        for (iCbCr = 0; iCbCr < 2; iCbCr++)
        {
          chroma_nC_cache[iCbCr][1] = upper_mb_attr->TotalCoeffChroma[iCbCr][12];
          chroma_nC_cache[iCbCr][2] = upper_mb_attr->TotalCoeffChroma[iCbCr][13];
          chroma_nC_cache[iCbCr][3] = upper_mb_attr->TotalCoeffChroma[iCbCr][14];
          chroma_nC_cache[iCbCr][4] = upper_mb_attr->TotalCoeffChroma[iCbCr][15];
        }
    }
    else
    {
      // Luma
      luma_nC_cache[1] = luma_nC_cache[2] = luma_nC_cache[3] = luma_nC_cache[4] = 0;
      // Chroma
      if (cfidc == 1 || cfidc == 2)
        chroma_nC_cache[0][1] = chroma_nC_cache[0][2] = chroma_nC_cache[1][1] = chroma_nC_cache[1][2] = 0;
      else if (cfidc == 3)
        chroma_nC_cache[0][1] = chroma_nC_cache[0][2] = chroma_nC_cache[0][3] = chroma_nC_cache[0][4] =
                                  chroma_nC_cache[1][1] = chroma_nC_cache[1][2] = chroma_nC_cache[1][3] = chroma_nC_cache[1][4] = 0;
    }
  }

  // fills in the 1st column
  if (!curr_mb_attr->left_mb_is_available)
  {
    // Luma
    luma_nC_cache[5] = luma_nC_cache[10] = luma_nC_cache[15] = luma_nC_cache[20] = 128; // mean 0 and not_available_flag is set to 1
    // Chroma
    if (cfidc == 1)
      chroma_nC_cache[0][5] = chroma_nC_cache[0][10] =
                                chroma_nC_cache[1][5] = chroma_nC_cache[1][10] = 128;
    else if (cfidc == 2 || cfidc == 3)
      chroma_nC_cache[0][5] = chroma_nC_cache[0][10] = chroma_nC_cache[0][15] = chroma_nC_cache[0][20] =
                                chroma_nC_cache[1][5] = chroma_nC_cache[1][10] = chroma_nC_cache[1][15] = chroma_nC_cache[1][20] = 128;
  }
  else
  {
    int i;
    struct rv264macro* temp_mb_attr;
    struct rv264macro* left_mb_attr;
    unsigned int left_is_field = curr_mb_attr->left_mb_is_field;
    const uint8_t* mb_pos = left_mb_pos[curr_is_field][left_is_field][curr_is_bot];
    const uint8_t* block_pos = left_4x4block_pos[curr_is_field][left_is_field][curr_is_bot];
    temp_mb_attr = curr_mb_attr -1 -(PicWidthInMbs << curr_is_bot);

    for(i=0; i<4; i++)
    {
      left_mb_attr = temp_mb_attr + (PicWidthInMbs << mb_pos[i]);
      if (!common_test || !IS_INTER(left_mb_attr))
      {
        luma_nC_cache[5+i*5]  = left_mb_attr->TotalCoeffLuma[block_pos[i]];
        if (cfidc == 1 && (i%2) == 0)
          for (iCbCr = 0; iCbCr < 2; iCbCr++)
            chroma_nC_cache[iCbCr][5+(i/2)*5] = left_mb_attr->TotalCoeffChroma[iCbCr][(block_pos[i]>=8)*2+1];
        else if (cfidc == 2)
          for (iCbCr = 0; iCbCr < 2; iCbCr++)
            chroma_nC_cache[iCbCr][5+i*5] = left_mb_attr->TotalCoeffChroma[iCbCr][(block_pos[i]>>1)];
        else if (cfidc == 3)
          for (iCbCr = 0; iCbCr < 2; iCbCr++)
            chroma_nC_cache[iCbCr][5+i*5] = left_mb_attr->TotalCoeffChroma[iCbCr][block_pos[i]];
      }
      else
      {
        luma_nC_cache[5+i*5] = 0;
        if (cfidc == 1 && (i%2) == 0)
          chroma_nC_cache[0][5+(i/2)*5] = chroma_nC_cache[1][5+(i/2)*5] = 0;
        else if (cfidc == 2 || cfidc == 3)
          chroma_nC_cache[0][5+i*5] = chroma_nC_cache[1][5+i*5] = 0;
      }
    }
  }
}



void fills_in_nC_cache(struct rv264macro* curr_mb_attr, unsigned int curr_is_bot, unsigned int curr_is_field,
    int PicWidthInMbs, uint8_t luma_nC_cache[28], uint8_t chroma_nC_cache[2][28], unsigned int cfidc, mb_type_t mb_type,
    unsigned int constrained_intra_pred_flag, unsigned int data_partitioning_slice_flag, unsigned int MbaffFrameFlag)
{

  if (MbaffFrameFlag)
  {
    fills_in_nC_cache_for_mbaff_slice(curr_mb_attr, curr_is_bot, curr_is_field, PicWidthInMbs,
                                             luma_nC_cache, chroma_nC_cache,
                                             cfidc, mb_type, constrained_intra_pred_flag, data_partitioning_slice_flag);
    return;
  }
  else
  {
    fills_in_nC_cache_for_nonmbaff_slice(curr_mb_attr, PicWidthInMbs,
                                                luma_nC_cache, chroma_nC_cache,
                                                cfidc, mb_type, constrained_intra_pred_flag, data_partitioning_slice_flag);
    return;
  }
}


void reset_one_nC_cache_line(uint8_t luma_nC_cache[28], uint8_t chroma_nC_cache[2][28], unsigned int is_horiz,
    unsigned int v, unsigned int cfidc)
{
  unsigned int i, iCbCr;
  unsigned int stride = is_horiz ? 1 : 5;

  for (i = 0; i < 4; ++i)
    luma_nC_cache[stride+i*stride] = v;

  // used for luma DC coeffs: 26: mbA, 27:mbB
  luma_nC_cache[26+is_horiz] = v;

  if (cfidc > 0)
  {
    unsigned int nb = (is_horiz ? MbWidthCdiv4 : MbHeightCdiv4)[cfidc];
    for (iCbCr = 0; iCbCr < 2; ++iCbCr)
    {
      for (i = 0; i < nb; ++i)
        chroma_nC_cache[iCbCr][stride+i*stride] = v;

      // used for chroma DC coeffs: 26: mbA, 27:mbB
      chroma_nC_cache[iCbCr][26+is_horiz] = v;
    }
  }
}

void set_one_nC_cache_line(uint8_t luma_nC_cache[28], uint8_t chroma_nC_cache[2][28], unsigned int test, unsigned int is_horiz,
    struct rv264macro* mb_attr, unsigned int cfidc)
{

  if (test)
  {
    reset_one_nC_cache_line(luma_nC_cache, chroma_nC_cache, is_horiz, 0, cfidc);
    return;
  }

  unsigned int i, iCbCr;
  unsigned int stride = is_horiz ? 1 : 5;
  unsigned int srcstride = is_horiz ? 1 : 4;
  unsigned int srcoffset = is_horiz ? 12 : 3;

  for (i = 0; i < 4; ++i)
    luma_nC_cache[stride+i*stride] = mb_attr->TotalCoeffLuma[i*srcstride+srcoffset];

  // used for luma DC coeffs: 26: mbA, 27:mbB
  luma_nC_cache[26+is_horiz] = mb_attr->luma_dc_coeffs_non_null;

  if (cfidc > 0)
  {
    unsigned int nb = (is_horiz ? MbWidthCdiv4 : MbHeightCdiv4)[cfidc];
    srcstride = is_horiz ? 1 : MbWidthCdiv4[cfidc];
    srcoffset = is_horiz ? MbSizeCdiv16[cfidc]-MbWidthCdiv4[cfidc] : MbWidthCdiv4[cfidc]-1;
    for (iCbCr = 0; iCbCr < 2; ++iCbCr)
    {
      for (i = 0; i < nb; ++i)
        chroma_nC_cache[iCbCr][stride+i*stride] = mb_attr->TotalCoeffChroma[iCbCr][i*srcstride+srcoffset];

      // used for chroma DC coeffs: 26: mbA, 27:mbB
      chroma_nC_cache[iCbCr][26+is_horiz] = mb_attr->chroma_dc_coeffs_non_null & (1<<iCbCr);
    }
  }
}

void reset_one_nC_cache_vert_mbaff(uint8_t luma_nC_cache[28], uint8_t chroma_nC_cache[2][28],
    unsigned int dstrow, unsigned int cfidc)
{
  luma_nC_cache[5+5*dstrow] = 0;
  if (dstrow==0)
    luma_nC_cache[26] = 0;

  if (cfidc>0 && ((dstrow & (cfidc==1)) == 0))
  {
    unsigned int cdrow = dstrow >> (cfidc==1);
    unsigned int iCbCr;
    for (iCbCr = 0; iCbCr < 2; ++iCbCr)
    {
      chroma_nC_cache[iCbCr][5+5*cdrow] = 0;

      // used for chroma DC coeffs: 26: mbA, 27:mbB
      if (dstrow==0)
        chroma_nC_cache[iCbCr][26] = 0;
    }
  }
}

void set_one_nC_cache_vert_mbaff(uint8_t luma_nC_cache[28], uint8_t chroma_nC_cache[2][28],
    unsigned int test, unsigned int dstrow, int srcrow, struct rv264macro* mb_attr, unsigned int cfidc)
{
  unsigned int iCbCr;

  if (test)
  {
    reset_one_nC_cache_vert_mbaff(luma_nC_cache, chroma_nC_cache, dstrow, cfidc);
    return;
  }

  luma_nC_cache[5+5*dstrow] = mb_attr->TotalCoeffLuma[3+srcrow*4];
  if (dstrow==0)
    luma_nC_cache[26] = mb_attr->luma_dc_coeffs_non_null;

  if (cfidc>0 && ((dstrow & (cfidc==1)) == 0))
  {
    unsigned int stride = MbWidthCdiv4[cfidc];
    unsigned int cdrow = dstrow >> (cfidc==1);
    unsigned int csrow = srcrow >> (cfidc==1);
    for (iCbCr = 0; iCbCr < 2; ++iCbCr)
    {
      chroma_nC_cache[iCbCr][5+5*cdrow] = mb_attr->TotalCoeffChroma[iCbCr][stride-1 + csrow*stride];

      // used for chroma DC coeffs: 26: mbA, 27:mbB
      if (dstrow==0)
        chroma_nC_cache[iCbCr][26] = mb_attr->chroma_dc_coeffs_non_null & (1<<iCbCr);
    }
  }
}

void fills_in_nC_cache_cabac(struct rv264macro* curr_mb_attr, unsigned int curr_is_bot, unsigned int curr_is_field,
    int PicWidthInMbs, uint8_t luma_nC_cache[28], uint8_t chroma_nC_cache[2][28], unsigned int cfidc, mb_type_t mb_type,
    unsigned int constrained_intra_pred_flag, unsigned int data_partitioning_slice_flag, unsigned int MbaffFrameFlag)
{

  unsigned int common = constrained_intra_pred_flag && is_IntraMb(mb_type) && data_partitioning_slice_flag;

  if (!MbaffFrameFlag)
  {
    // upper row
    if (!curr_mb_attr->up_mb_is_available)
      reset_one_nC_cache_line(luma_nC_cache, chroma_nC_cache, 1, is_IntraMb(mb_type), cfidc);
    else
    {
      struct rv264macro* up = curr_mb_attr-PicWidthInMbs;
      set_one_nC_cache_line(luma_nC_cache, chroma_nC_cache, common && IS_INTER(up), 1, up, cfidc);
    }

    // left column
    if (!curr_mb_attr->left_mb_is_available)
      reset_one_nC_cache_line(luma_nC_cache, chroma_nC_cache, 0, is_IntraMb(mb_type), cfidc);
    else
    {
      struct rv264macro* left = curr_mb_attr-1;
      set_one_nC_cache_line(luma_nC_cache, chroma_nC_cache, common && IS_INTER(left), 0, left, cfidc);
    }
  }

  else // MbaffFrameFlag==1
  {
    // upper row
    if (!curr_mb_attr->up_mb_is_available)
      reset_one_nC_cache_line(luma_nC_cache, chroma_nC_cache, 1, is_IntraMb(mb_type), cfidc);
    else
    {
      unsigned int up_is_field = curr_mb_attr->up_mb_is_field;
      struct rv264macro* up = get_up_mbaff_mb(curr_mb_attr, PicWidthInMbs, curr_is_field, up_is_field, curr_is_bot);
      set_one_nC_cache_line(luma_nC_cache, chroma_nC_cache, common && IS_INTER(up), 1, up, cfidc);
    }

    // left column
    if (!curr_mb_attr->left_mb_is_available)
      reset_one_nC_cache_line(luma_nC_cache, chroma_nC_cache, 0, is_IntraMb(mb_type), cfidc);
    else
    {
      unsigned int left_is_field = curr_mb_attr->left_mb_is_field;
      struct rv264macro* left;
      if (left_is_field == curr_is_field)
      {
        left = curr_mb_attr - 1;
        set_one_nC_cache_line(luma_nC_cache, chroma_nC_cache, common && IS_INTER(left), 0, left, cfidc);
      }
      else if (curr_is_field)
      {
        left = curr_mb_attr - 1 - PicWidthInMbs*curr_is_bot;
        set_one_nC_cache_vert_mbaff(luma_nC_cache, chroma_nC_cache, common && IS_INTER(left), 0, 0, left, cfidc);
        set_one_nC_cache_vert_mbaff(luma_nC_cache, chroma_nC_cache, common && IS_INTER(left), 1, 2, left, cfidc);
        left += PicWidthInMbs;
        set_one_nC_cache_vert_mbaff(luma_nC_cache, chroma_nC_cache, common && IS_INTER(left), 2, 0, left, cfidc);
        set_one_nC_cache_vert_mbaff(luma_nC_cache, chroma_nC_cache, common && IS_INTER(left), 3, 2, left, cfidc);
      }
      else // left_is_field
      {
        left = curr_mb_attr - 1 - PicWidthInMbs*curr_is_bot;
        set_one_nC_cache_vert_mbaff(luma_nC_cache, chroma_nC_cache, common && IS_INTER(left), 0, curr_is_bot*2+0, left, cfidc);
        set_one_nC_cache_vert_mbaff(luma_nC_cache, chroma_nC_cache, common && IS_INTER(left), 1, curr_is_bot*2+0, left, cfidc);
        set_one_nC_cache_vert_mbaff(luma_nC_cache, chroma_nC_cache, common && IS_INTER(left), 2, curr_is_bot*2+1, left, cfidc);
        set_one_nC_cache_vert_mbaff(luma_nC_cache, chroma_nC_cache, common && IS_INTER(left), 3, curr_is_bot*2+1, left, cfidc);
      }
    }
  }
}


void writes_back_nC_cache(struct rv264macro* mb_attr,
                                        uint8_t luma_nC_cache[28], uint8_t chroma_nC_cache[2][28],
                                        unsigned int cfidc)
{
  unsigned int iCbCr;
  int i;

  // LUMA
  for( i=0; i<16; i++)
    mb_attr->TotalCoeffLuma[i] = luma_nC_cache[raster4x4toraster5x5[i]];

  // CHROMA
  if (cfidc == 1)
  {
    for (iCbCr = 0; iCbCr < 2; iCbCr++)
      for( i=0; i<4; i++)
        mb_attr->TotalCoeffChroma[iCbCr][i] = chroma_nC_cache[iCbCr][raster2x4toraster5x5[i]];
  }
  else if (cfidc == 2)
  {
    for (iCbCr = 0; iCbCr < 2; iCbCr++)
      for( i=0; i<8; i++)
        mb_attr->TotalCoeffChroma[iCbCr][i] = chroma_nC_cache[iCbCr][raster2x4toraster5x5[i]];
  }
  else if (cfidc == 3)
  {
    for (iCbCr = 0; iCbCr < 2; iCbCr++)
      for( i=0; i<16; i++)
        mb_attr->TotalCoeffChroma[iCbCr][i] = chroma_nC_cache[iCbCr][raster4x4toraster5x5[i]];
  }
}

unsigned int get_level_prefix(struct bitbuf* bs)
{
  uint32_t buf;
  unsigned int l;

  buf = showbits(bs, 25); // TODO - FIXME: check that 25 bits is enough for level_prefix !
  LUD_DEBUG_ASSERT(buf != 0); // If buf=0 then we did not read enough bits !
  l = 25-im_log2(buf);
  flushbits(bs, l);
  return l-1;
}

static void scale_coeff(int16_t* coeffLevel, const uint8_t* scan_table, int coeffNum, int level,
                               const uint16_t* LevelScale, int two_pow_qp_div6, RESIDUAL_BLOCK_TYPE block_type, int bypass)
{
  int i = scan_table[coeffNum];

  if (bypass)
  {
    coeffLevel[i] = (int16_t) level;
  }
  else if (block_type == CHROMA_DC_LEVEL)
  {
    // cfidc == 1 = >coeffLevel[i] = (int16_t) ((level * (int)LevelScale[0] * two_pow_qp_div6)>>5); // the multiplication must be done AFTER the hadamart transform
    //else => coeffLevel[i] = (int16_t) ((level * (int)LevelScale[0] * two_pow_qp_div6 + (1<<5))>>6); // the multiplication must be done AFTER the hadamart transform
    coeffLevel[i] = (int16_t) level;
  }
  else if (block_type == LUMA_DC_LEVEL)
  {
    // coeffLevel[i] = (int16_t) ((level * (int)LevelScale[0] * two_pow_qp_div6 + (1<<5))>>6);  // the multiplication must be done AFTER the hadamart transform
    coeffLevel[i] = (int16_t) level;
  }
  else if (block_type == LUMA8x8_LEVEL)
  {
    coeffLevel[i] = (int16_t) ((level * (int)LevelScale[i] * two_pow_qp_div6 + (1<<5))>>6);
  }
  else // (block_type == INTRA_AC_LEVEL or LUMA_LEVEL or CHROMA_AC_LEVEL)
  {
    coeffLevel[i] = (int16_t) ((level * (int)LevelScale[i] * two_pow_qp_div6 + (1<<3))>>4);
  }
}

// decode residual with CAVLC method. It means that this function should be called only when entropy_coding_mode_flag==0
static unsigned int residual_block_cavlc(struct bitbuf* bs, unsigned int lc4x4blockIdx, int16_t* coeffLevel, unsigned int maxNumCoeff,
                                        RESIDUAL_BLOCK_TYPE block_type, uint8_t* nC_cache, unsigned int cfidc, const uint8_t* scan_table,
                                        const uint16_t* LevelScale, int two_pow_qp_div6, int bypass)
{
  static const uint8_t coeff_token_table_index[10]= {4, 3, 0, 0, 1, 1, 2, 2, 2, 2};
  static const int8_t cfidc_to_nc[4] = {0, -1, -2, 0};
  int nC;
  unsigned int i;
  unsigned int coeff_token, TotalCoeff, TrailingOnes, total_zeros, zerosLeft, run_before;
  unsigned int level_prefix, level_suffix, suffixLength;
  int level[16], levelCode;
  int coeffNum;


  if (block_type == CHROMA_DC_LEVEL)
  {
    nC = cfidc_to_nc[cfidc];
  }
  else if (block_type == LUMA_DC_LEVEL)
  {
    nC = derive_nC(nC_cache, 0, 0);
  }
  else if (block_type == CHROMA_AC_LEVEL)
  {
    nC = derive_nC(nC_cache, lc4x4blockIdx, cfidc<3);
  }
  else // (block_type == LUMA_AC_LEVEL or LUMA_LEVEL or LUMA8x8_LEVEL)
  {
    nC = derive_nC(nC_cache, lc4x4blockIdx, 0);
  }

  if (8 <= nC) // use the fixed length in order to read coeff_token
  {
    coeff_token = coeff_token_fixed[getbits(bs, 6)];
  }
  else // use VLC table to read coeff_token
  {
    coeff_token = bs_read_vlc_ludh264(bs, &gdd.coeff_token_vlc_tables[coeff_token_table_index[nC+2]], "Luma # c & tr.1s");
  }
  TotalCoeff = coeff_token>>2;
  TrailingOnes = coeff_token&3;

  if (block_type != CHROMA_DC_LEVEL && block_type != LUMA_DC_LEVEL)
  {
    if (block_type == CHROMA_AC_LEVEL && cfidc<3)
      nC_cache[raster2x4toraster5x5[lc4x4blockIdx]] = TotalCoeff;
    else
      nC_cache[scan4x4toraster5x5[lc4x4blockIdx]] = TotalCoeff;
  }

  if( TotalCoeff > 0 )
  {
    if( TotalCoeff > 10 && TrailingOnes < 3 )
      suffixLength = 1;
    else
      suffixLength = 0;
    for( i = 0; i < TotalCoeff; i++ )
    {
      if( i < TrailingOnes )
      {
        //trailing_ones_sign_flag = getbit(bs); // 3|4 u(1)
        level[ i ] = 1 - 2 * getbit(bs);
      }
      else
      {
        level_prefix = get_level_prefix(bs); // 3|4 ce(v)
        levelCode =  im_min( 15, level_prefix ) << suffixLength ;
        if( suffixLength > 0 || level_prefix >= 14 )
        {
          unsigned int levelSuffixSize = suffixLength;
          if (level_prefix == 14 && suffixLength==0)
            levelSuffixSize = 4;
          else if (level_prefix >= 15)
            levelSuffixSize = level_prefix - 3;
          level_suffix = getbits(bs, levelSuffixSize); // 3|4 u(v)
          levelCode += level_suffix;
        }
        if( level_prefix >= 15 && suffixLength == 0 )
          levelCode += 15;
        if( level_prefix >= 16 )
          levelCode += ( 1 << ( level_prefix - 3 ) ) - 4096;
        if( i == TrailingOnes && TrailingOnes < 3 )
          levelCode += 2;
        if( levelCode % 2 == 0 )
          level[ i ] = ( levelCode + 2 ) >> 1;
        else
          level[ i ] = ( -levelCode - 1 ) >> 1;
        if( suffixLength == 0 )
          suffixLength = 1;
        if( im_abs( level[ i ] ) > ( 3 << ( suffixLength - 1 ) ) && suffixLength < 6 )
          suffixLength++;
      }
    }
    if( TotalCoeff < maxNumCoeff )
    {
      if (maxNumCoeff == 4)
        total_zeros = bs_read_vlc_ludh264(bs, &gdd.total_zeros_2x2_vlc_tables[TotalCoeff-1], "Luma totalrun");
      else if (maxNumCoeff == 8)
        total_zeros = bs_read_vlc_ludh264(bs, &gdd.total_zeros_2x4_vlc_tables[TotalCoeff-1], "Luma totalrun");
      else
        total_zeros = bs_read_vlc_ludh264(bs, &gdd.total_zeros_4x4_vlc_tables[TotalCoeff-1], "Luma totalrun");
      zerosLeft = total_zeros;
    }
    else
      zerosLeft = 0;

    // index of the last non null coeff
    coeffNum = TotalCoeff + zerosLeft - 1;
    scale_coeff(coeffLevel, scan_table, coeffNum, level[0],
                     LevelScale, two_pow_qp_div6, block_type, bypass);
    for( i = 1; i < TotalCoeff ; i++ )
    {
      if( zerosLeft > 0 )
      {
        if (zerosLeft <= 6)
          run_before =  bs_read_vlc_ludh264(bs, &gdd.run_before_vlc_tables[zerosLeft-1], "Luma run");
        else
          run_before =  bs_read_vlc_ludh264(bs, &gdd.run_before_vlc_tables[6], "Luma run");
      }
      else
        run_before = 0;
      zerosLeft -= run_before;
      coeffNum -= 1 + run_before;
      scale_coeff(coeffLevel, scan_table, coeffNum, level[i],
                  LevelScale, two_pow_qp_div6, block_type, bypass);
    }
  }

  return TotalCoeff;
}


static unsigned int residual_block_cabac(CABACContext* cabac_ctx, struct bitbuf* bs,
    unsigned int lc4x4blockIdx, int16_t* coeffLevel, unsigned int maxNumCoeff, RESIDUAL_BLOCK_TYPE block_type,
    uint8_t* nC_cache, unsigned int cfidc, const uint8_t* scan_table, const uint16_t* LevelScale, int two_pow_qp_div6,
    int bypass, unsigned int mb_field_decoding_flag)
{
  unsigned int ctxBlockCat = block_type;
  unsigned int numCoeff = 0;
  uint8_t significant_coeff_flag[64];
  int i;
  unsigned int coded_block_flag;
  unsigned int numDecodAbsLevelGt1 = 0;
  unsigned int numDecodAbsLevelEq1 = 0;

  if( maxNumCoeff == 64 )
    coded_block_flag = 1;
  else
    coded_block_flag = cabac_decode_coded_block_flag(cabac_ctx, bs, ctxBlockCat, nC_cache, cfidc, lc4x4blockIdx);

  if( coded_block_flag )
  {
    int coeff_abs_level_minus1;
    unsigned int coeff_sign_flag;
    numCoeff = maxNumCoeff;
    i=0;
    do
    {
      significant_coeff_flag[i] = cabac_decode_significant_coeff_flag(cabac_ctx, bs, ctxBlockCat, i, mb_field_decoding_flag, cfidc);
      if( significant_coeff_flag[i] )
      {
        unsigned int last_significant_coeff_flag = cabac_decode_last_significant_coeff_flag(cabac_ctx, bs,
            ctxBlockCat, i, mb_field_decoding_flag, cfidc);
        if( last_significant_coeff_flag )
          numCoeff = i + 1; // not necessary to 0 all the remaining coeffs since they have been initialized to 0 already
      }
      i++;
    } while( i < numCoeff - 1 );

    int level;
    unsigned int count = 1;
    coeff_abs_level_minus1 = cabac_decode_coeff_abs_level_minus1(cabac_ctx, bs, numDecodAbsLevelEq1, numDecodAbsLevelGt1, ctxBlockCat);
    numDecodAbsLevelGt1 += coeff_abs_level_minus1 > 0;
    numDecodAbsLevelEq1 += coeff_abs_level_minus1 == 0;
    coeff_sign_flag = cabac_decode_coeff_sign_flag(cabac_ctx, bs);
    level = ( coeff_abs_level_minus1 + 1 ) *  ( 1 - 2 * coeff_sign_flag );
    scale_coeff(coeffLevel, scan_table, numCoeff - 1, level, LevelScale, two_pow_qp_div6, block_type, bypass);


    for( i = numCoeff - 2; i >= 0; i-- )
      if( significant_coeff_flag[i] )
      {
        coeff_abs_level_minus1 = cabac_decode_coeff_abs_level_minus1(cabac_ctx, bs, numDecodAbsLevelEq1, numDecodAbsLevelGt1, ctxBlockCat);
        numDecodAbsLevelGt1 += coeff_abs_level_minus1 > 0;
        numDecodAbsLevelEq1 += coeff_abs_level_minus1 == 0;
        coeff_sign_flag = cabac_decode_coeff_sign_flag(cabac_ctx, bs);
        //coeffLevel[ i ] = ( coeff_abs_level_minus1 + 1 ) * ( 1 - 2 * coeff_sign_flag );
        level = ( coeff_abs_level_minus1 + 1 ) *  ( 1 - 2 * coeff_sign_flag );
        scale_coeff(coeffLevel, scan_table, i, level, LevelScale, two_pow_qp_div6, block_type, bypass);
        count++;
      }
    numCoeff = count;
  }

  if (block_type != CHROMA_DC_LEVEL && block_type != LUMA_DC_LEVEL)
  {
    if (block_type == CHROMA_AC_LEVEL && cfidc<3)
      nC_cache[raster2x4toraster5x5[lc4x4blockIdx]] = numCoeff != 0;
    else if (block_type != LUMA8x8_LEVEL)
      nC_cache[scan4x4toraster5x5[lc4x4blockIdx]] = numCoeff;
    else
    {
      // TODO: Optimize this hack: numCoeff should be the number of coeff per 4x4 block. However when CABAC is used it can decode 8x8 block at a time
      //       so numCoeff is actually the number of coeff for the whole 8x8 block. Moreover, for CABAC decoding nC_cache is just used as a flag in
      //       order to derive coded_block_flag. => need to repeat the numCoeff value on each nC entries of the 8x8 block.
      //       However, the nC value is also used in intra/inter decoding in order to optimize the residual transform.
      //       The conclusion is: if numCoeff(8x8) = 1 => 1 is filled in the 4 nC, => residual decoding re-add the nC => give 4 instead of the original 1
      //          => suboptimize the residual transform.
      unsigned int idx = scan4x4toraster5x5[lc4x4blockIdx];
      numCoeff = (numCoeff+3) / 4; // this hacky divide preserve the non nulity of numCoeff, but keep it in the range of 0-16
      nC_cache[idx] = numCoeff;
      nC_cache[idx+1] = numCoeff;
      nC_cache[idx+5] = numCoeff;
      nC_cache[idx+6] = numCoeff;
    }
  }

  return numCoeff;
}



static unsigned int residual_block(CABACContext* cabac_ctx, struct bitbuf* bs,
    unsigned int lc4x4blockIdx, int16_t* coeffLevel, unsigned int maxNumCoeff, RESIDUAL_BLOCK_TYPE block_type,
    uint8_t* nC_cache, unsigned int cfidc, const uint8_t* scan_table, const uint16_t* LevelScale, int two_pow_qp_div6,
    int bypass, unsigned int mb_field_decoding_flag, unsigned int entropy_coding_mode_flag)
{

  if (entropy_coding_mode_flag)
    return residual_block_cabac(cabac_ctx, bs, lc4x4blockIdx, coeffLevel, maxNumCoeff, block_type, nC_cache, cfidc,
        scan_table, LevelScale, two_pow_qp_div6, bypass, mb_field_decoding_flag);
  else
    return residual_block_cavlc(bs, lc4x4blockIdx, coeffLevel, maxNumCoeff, block_type, nC_cache, cfidc,
        scan_table, LevelScale, two_pow_qp_div6, bypass);

}
// LumaLevel stands for either Intra16x16ACLevel, LumaLevel or LumaLevel8x8 according to context (transform_size_8x8_flag, MbPartPredMode)...
// Intra16x16DCLevel can be set to NULL if not processing an Intra_16x16 macroblock
void residual(CABACContext* cabac_ctx, struct bitbuf* bs, part_type_t MbPartPredMode0, struct rv264macro* curr_mb_attr, mb_type_t mb_type,
                            unsigned int CodedBlockPatternLuma, unsigned int CodedBlockPatternChroma, unsigned int curr_is_bot,
                            unsigned int entropy_coding_mode_flag, unsigned int transform_size_8x8_flag, unsigned int cfidc,
                            int QPprimeY, int QPprimeC[2], int16_t* mb_data, unsigned int mb_field_decoding_flag, unsigned int PicWidthInMbs,
                            uint16_t LevelScale4x4[6][6][16], uint16_t LevelScale8x8[2][6][64], unsigned int qpprime_y_zero_transform_bypass_flag,
                            unsigned int constrained_intra_pred_flag, unsigned int data_partitioning_slice_flag, unsigned int MbaffFrameFlag)
{
  // 5x5 arrays + 2 for cabac decoding
  uint8_t luma_nC_cache[28];
  uint8_t chroma_nC_cache[2][28];

  unsigned int i8x8, i4x4;
  const uint8_t* scan_table;
  int bypass = QPprimeY == 0 && qpprime_y_zero_transform_bypass_flag != 0;
  int QPprimeY_div6 = QPprimeY/6;
  int QPprimeY_mod6 = QPprimeY%6;

  curr_mb_attr->TransformBypassModeFlag = bypass;

  if (entropy_coding_mode_flag)
    fills_in_nC_cache_cabac(curr_mb_attr, curr_is_bot, mb_field_decoding_flag, PicWidthInMbs, luma_nC_cache,
        chroma_nC_cache, cfidc, mb_type, constrained_intra_pred_flag, data_partitioning_slice_flag, MbaffFrameFlag);
  else
    fills_in_nC_cache(curr_mb_attr, curr_is_bot, mb_field_decoding_flag, PicWidthInMbs, luma_nC_cache, chroma_nC_cache,
        cfidc, mb_type, constrained_intra_pred_flag, data_partitioning_slice_flag, MbaffFrameFlag);


  // Intra16x16
  if( MbPartPredMode0 == Intra_16x16 )
  {
    scan_table =  mb_field_decoding_flag ? inverse_4x4field_scan : inverse_4x4zigzag_scan;
    int TotalCoeff;
    int i;
    int16_t* data = mb_data;
    int DCLevelScale = LevelScale4x4[0][QPprimeY_mod6][0];
    int two_pow_qp_div6 = 1<<QPprimeY_div6;

    TotalCoeff = residual_block(cabac_ctx, bs, 0, mb_data, 16, LUMA_DC_LEVEL, luma_nC_cache, cfidc, scan_table, 0, 0, bypass,
        mb_field_decoding_flag, entropy_coding_mode_flag);

    inverse_transform(IHDM4x4, TotalCoeff, bypass, 0, data, 0, 0);
    for( i=0; i<16; i++, data++)
      *data = (int16_t) ((*data * DCLevelScale * two_pow_qp_div6 + (1<<5))>>6);
    curr_mb_attr->luma_dc_coeffs_non_null = TotalCoeff!=0;

    for( i8x8 = 0; i8x8 < 4; i8x8++ ) /* each luma 8x8 block */
    {
      if( CodedBlockPatternLuma & ( 1 << i8x8 ) )
      {
        for( i4x4 = 0; i4x4 < 4; i4x4++ )
        { /* each 4x4 sub-block of block */
          residual_block(cabac_ctx, bs, i8x8 * 4 + i4x4, mb_data + 16 - 1 + scan4x4[i8x8 * 4 + i4x4] * 15, 15, LUMA_AC_LEVEL,
              luma_nC_cache, cfidc, scan_table + 1, LevelScale4x4[0][QPprimeY_mod6], 1 << QPprimeY_div6, bypass,
              mb_field_decoding_flag, entropy_coding_mode_flag);
        }
      }
      else
      {
        luma_nC_cache[scan4x4toraster5x5[i8x8 * 4 + 0]] = 0;
        luma_nC_cache[scan4x4toraster5x5[i8x8 * 4 + 1]] = 0;
        luma_nC_cache[scan4x4toraster5x5[i8x8 * 4 + 2]] = 0;
        luma_nC_cache[scan4x4toraster5x5[i8x8 * 4 + 3]] = 0;
      }
    }
  }
  else // intra/inter 8x8 or 4x4 blocks
  {
    curr_mb_attr->luma_dc_coeffs_non_null = 0;
    if (!transform_size_8x8_flag) // 4x4 cavlc or cabac
    {
      unsigned int intra_inter_mb = (mb_type > SI) * 3;

      scan_table =  mb_field_decoding_flag ? inverse_4x4field_scan : inverse_4x4zigzag_scan;
      for( i8x8 = 0; i8x8 < 4; i8x8++ ) /* each luma 8x8 block */
      {
        if( CodedBlockPatternLuma & ( 1 << i8x8 ) )
        {
          for( i4x4 = 0; i4x4 < 4; i4x4++ ) /* each 4x4 sub-block of block */
          {
            residual_block(cabac_ctx, bs, i8x8 * 4 + i4x4, mb_data + scan4x4[i8x8 * 4 + i4x4] * 16, 16, LUMA_LEVEL, luma_nC_cache,
                cfidc, scan_table, LevelScale4x4[intra_inter_mb][QPprimeY_mod6], 1 << QPprimeY_div6, bypass,
                mb_field_decoding_flag, entropy_coding_mode_flag);
          }
        }
        else
        {
          luma_nC_cache[scan4x4toraster5x5[i8x8 * 4 + 0]] = 0;
          luma_nC_cache[scan4x4toraster5x5[i8x8 * 4 + 1]] = 0;
          luma_nC_cache[scan4x4toraster5x5[i8x8 * 4 + 2]] = 0;
          luma_nC_cache[scan4x4toraster5x5[i8x8 * 4 + 3]] = 0;
        }
      }
    }
    else if (!entropy_coding_mode_flag) // intra/inter 8x8 cavlc
    {
      unsigned int intra_inter_mb = (mb_type > SI);

      scan_table = mb_field_decoding_flag ? inverse_8x8field_scan_cavlc : inverse_8x8zigzag_scan_cavlc;
      for( i8x8 = 0; i8x8 < 4; i8x8++ ) /* each luma 8x8 block */
      {
        if( CodedBlockPatternLuma & ( 1 << i8x8 ) )
        {
          for( i4x4 = 0; i4x4 < 4; i4x4++ ) /* each 4x4 sub-block of block */
          {
            residual_block(0, bs, i8x8 * 4 + i4x4, mb_data + (i8x8 * 64), 16, LUMA8x8_LEVEL, luma_nC_cache, cfidc,
                scan_table + i4x4 * 16, LevelScale8x8[intra_inter_mb][QPprimeY_mod6], 1 << QPprimeY_div6, bypass,
                mb_field_decoding_flag, 0);
          }
        }
        else
        {
          luma_nC_cache[scan4x4toraster5x5[i8x8 * 4 + 0]] = 0;
          luma_nC_cache[scan4x4toraster5x5[i8x8 * 4 + 1]] = 0;
          luma_nC_cache[scan4x4toraster5x5[i8x8 * 4 + 2]] = 0;
          luma_nC_cache[scan4x4toraster5x5[i8x8 * 4 + 3]] = 0;
        }
      }
    }
    else // 8x8 cabac
    {
      unsigned int intra_inter_mb = (mb_type > SI);
      scan_table = mb_field_decoding_flag ? inverse_8x8field_scan : inverse_8x8zigzag_scan;
      for( i8x8 = 0; i8x8 < 4; i8x8++ ) /* each luma 8x8 block */
      {
        if (CodedBlockPatternLuma & (1<<i8x8))
        {
          residual_block(cabac_ctx, bs, i8x8 * 4, mb_data + (i8x8 * 64), 64, LUMA8x8_LEVEL, luma_nC_cache, cfidc,
              scan_table, LevelScale8x8[intra_inter_mb][QPprimeY_mod6], 1 << QPprimeY_div6, bypass,
              mb_field_decoding_flag, entropy_coding_mode_flag);
        }
        else
        {
          luma_nC_cache[scan4x4toraster5x5[i8x8 * 4 + 0]] = 0;
          luma_nC_cache[scan4x4toraster5x5[i8x8 * 4 + 1]] = 0;
          luma_nC_cache[scan4x4toraster5x5[i8x8 * 4 + 2]] = 0;
          luma_nC_cache[scan4x4toraster5x5[i8x8 * 4 + 3]] = 0;
        }
      }
    }
  }


  // CHROMA residual
  if (cfidc != 0)
  {
    unsigned int intra_inter_mb = (mb_type > SI) * 3;
    int QPprimeC_div6[2];
    int QPprimeC_mod6[2];
    unsigned int NumC8x8, iCbCr;
    const uint8_t* nC_cache_scan_table = cfidc == 3 ? scan4x4toraster5x5 : raster2x4toraster5x5;
    const uint8_t* chroma_scan_table = cfidc == 3 ? scan4x4 : scan2x4;
    unsigned int TotalCoeff[2] = {0, 0};
    int i, DCLevelScale, two_pow_qp_div6;

    mb_data += 256; // go to the chroma location

    scan_table =  mb_field_decoding_flag ? inverse_4x4field_scan : inverse_4x4zigzag_scan;

    // Deals with DC coeffs
    if (cfidc == 1)
    {
      NumC8x8 = 1;
      for (iCbCr = 0; iCbCr < 2; iCbCr++)
      {
        QPprimeC_div6[iCbCr] = QPprimeC[iCbCr] / 6;
        QPprimeC_mod6[iCbCr] = QPprimeC[iCbCr] % 6;
        if (CodedBlockPatternChroma & 3)  /* chroma DC residual present */
        {
          int16_t* data = mb_data+64*iCbCr;

          TotalCoeff[iCbCr] = residual_block(cabac_ctx, bs, 0, data, 4, CHROMA_DC_LEVEL, chroma_nC_cache[iCbCr], 1,
              chroma_inverse_dc_scan1, 0, 0, bypass, mb_field_decoding_flag, entropy_coding_mode_flag);
          inverse_transform(IHDM2x2, TotalCoeff[iCbCr], bypass, 0, data, 0, 0);
          DCLevelScale = LevelScale4x4[intra_inter_mb+iCbCr+1][QPprimeC_mod6[iCbCr]][0];
          two_pow_qp_div6 = 1<<(QPprimeC_div6[iCbCr]);
          for( i=0; i<4; i++, data++)
            *data = (int16_t) ((*data * DCLevelScale * two_pow_qp_div6)>>5);
        }
      }
    }
    else if (cfidc == 2)
    {
      NumC8x8 = 2;
      int QPprimeC_DC_div6;
      int QPprimeC_DC_mod6;
      for (iCbCr = 0; iCbCr < 2; iCbCr++)
      {
        QPprimeC_div6[iCbCr] = QPprimeC[iCbCr] / 6; // used for AC coeffs
        QPprimeC_mod6[iCbCr] = QPprimeC[iCbCr] % 6; // used for AC coeffs
        QPprimeC_DC_div6 = (QPprimeC[iCbCr]+3) / 6;
        QPprimeC_DC_mod6 = (QPprimeC[iCbCr]+3) % 6;
        if (CodedBlockPatternChroma & 3)  /* chroma DC residual present */
        {
          int16_t* data = mb_data+128*iCbCr;

          TotalCoeff[iCbCr] = residual_block(cabac_ctx, bs, 0, data, 8, CHROMA_DC_LEVEL, chroma_nC_cache[iCbCr], 2,
              chroma_inverse_dc_scan2, 0, 0, bypass, mb_field_decoding_flag, entropy_coding_mode_flag);
          inverse_transform(IHDM2x4, TotalCoeff[iCbCr], bypass, 0, data, 0, 0);
          DCLevelScale = LevelScale4x4[intra_inter_mb+iCbCr+1][QPprimeC_DC_mod6][0];
          two_pow_qp_div6 = 1<<(QPprimeC_DC_div6);
          for( i=0; i<8; i++, data++)
            *data = (int16_t) ((*data * DCLevelScale * two_pow_qp_div6 + (1<<5))>>6);
        }
      }
    }
    else // cfidc == 3
    {
      NumC8x8 = 4;
      for (iCbCr = 0; iCbCr < 2; iCbCr++)
      {
        QPprimeC_div6[iCbCr] = QPprimeC[iCbCr] / 6;
        QPprimeC_mod6[iCbCr] = QPprimeC[iCbCr] % 6;
        if (CodedBlockPatternChroma & 3)  /* chroma DC residual present */
        {
          int16_t* data = mb_data+256*iCbCr;

          TotalCoeff[iCbCr] = residual_block(cabac_ctx, bs, 0, data, 16, CHROMA_DC_LEVEL, chroma_nC_cache[iCbCr], 3, scan_table,
              0, 0, bypass, mb_field_decoding_flag, entropy_coding_mode_flag);
          inverse_transform(IHDM4x4, TotalCoeff[iCbCr], bypass, 0, data, 0, 0);
          DCLevelScale = LevelScale4x4[intra_inter_mb+iCbCr+1][QPprimeC_mod6[iCbCr]][0];
          two_pow_qp_div6 = 1<<(QPprimeC_div6[iCbCr]);
          for( i=0; i<16; i++, data++)
            *data = (int16_t) ((*data * DCLevelScale * two_pow_qp_div6 + (1<<5))>>6);
        }
      }
    }
    curr_mb_attr->chroma_dc_coeffs_non_null = (TotalCoeff[0] != 0) + ((TotalCoeff[1] != 0)<<1);

    // Deals with AC coeffs
    for (iCbCr = 0; iCbCr < 2; iCbCr++)
      for (i8x8 = 0; i8x8 < NumC8x8; i8x8++)
        for (i4x4 = 0; i4x4 < 4; i4x4++)
          if (CodedBlockPatternChroma & 2)
          {
            /* chroma AC residual present */
            residual_block(cabac_ctx, bs, i8x8 * 4 + i4x4, mb_data + NumC8x8 * 64 * iCbCr + NumC8x8 * 4 - 1
                + chroma_scan_table[i8x8 * 4 + i4x4] * 15, 15, CHROMA_AC_LEVEL, chroma_nC_cache[iCbCr], cfidc,
                scan_table + 1, LevelScale4x4[intra_inter_mb + iCbCr + 1][QPprimeC_mod6[iCbCr]], 1
                    << (QPprimeC_div6[iCbCr]), bypass, mb_field_decoding_flag, entropy_coding_mode_flag);
          }
          else
          {
            chroma_nC_cache[iCbCr][nC_cache_scan_table[i8x8 * 4 + i4x4]] = 0;
          }
  }

  writes_back_nC_cache(curr_mb_attr, luma_nC_cache, chroma_nC_cache, cfidc);
}


mb_type_t decode_mb_type(CABACContext* cabac_ctx, struct bitbuf* bs, unsigned int entropy_coding_mode_flag,
    struct rv264macro* curr, slice_type_t slice_type_modulo5)
{
  if (!entropy_coding_mode_flag)
  {
    int mb_type;

    mb_type = uexpbits_ludh264(bs, "mb_type");
    // When decoding CAVLC shift mb_type if not an I Slice . See ITU-T Rec. H.264 (03/2005) page 86 and after
    if (!entropy_coding_mode_flag)
    {
      if (slice_type_modulo5 == SLICE_SI)
      {
        if  (mb_type > 0)
          mb_type--;
        else
          mb_type = SI;
      }
      else if (slice_type_modulo5 == SLICE_P || slice_type_modulo5 == SLICE_SP)
      {
        if (mb_type > 4)
          mb_type -= 5;
        else
          mb_type += P_L0_16x16;
      }
      else if (slice_type_modulo5 == SLICE_B)
      {
        if (mb_type > 22)
          mb_type -= 23;
        else
          mb_type += B_Direct_16x16;
      }
    }
    return (mb_type_t)mb_type;
  }
  else
    return cabac_decode_mb_type(cabac_ctx, bs, curr, slice_type_modulo5);
}

unsigned int decode_transform_size_8x8_flag(CABACContext* cabac_ctx, struct bitbuf* bs, unsigned int entropy_coding_mode_flag,
    struct rv264macro* curr_mb_attr)
{
  if (!entropy_coding_mode_flag)
    return getbit(bs);
  else
    return cabac_decode_transform_size_8x8_flag(cabac_ctx, bs, curr_mb_attr);
}

void decode_coded_block_pattern(unsigned int* CodedBlockPatternLuma, unsigned int* CodedBlockPatternChroma,
    CABACContext* cabac_ctx, struct bitbuf* bs, unsigned int entropy_coding_mode_flag, part_type_t prediction_mode,
    unsigned int cfidc, struct rv264macro* curr)
{
  if (!entropy_coding_mode_flag)
  {
    curr->coded_block_pattern = bs_read_me(bs, prediction_mode, cfidc); // 2  me(v) | ae(v)
    *CodedBlockPatternLuma = curr->coded_block_pattern & 15;
    *CodedBlockPatternChroma = curr->coded_block_pattern >> 4;
  }
  else
  {
    cabac_decode_coded_block_pattern(CodedBlockPatternLuma, CodedBlockPatternChroma, cabac_ctx, bs, curr, cfidc);
  }
}

int decode_mb_qp_delta(CABACContext* cabac_ctx, struct bitbuf* bs, unsigned int entropy_coding_mode_flag, int prevMbAddr_mb_qp_delta)
{
  if(!entropy_coding_mode_flag)
    return sexpbits_ludh264(bs, "mb_qp_delta");
  else
    return cabac_decode_mb_qp_delta(cabac_ctx, bs, prevMbAddr_mb_qp_delta);
}

int derive_QPY(int QPY_PREV, int mb_qp_delta, int QpBdOffsetY)
{
  return ( ( QPY_PREV + mb_qp_delta + 52 + 2 * QpBdOffsetY ) % ( 52 + QpBdOffsetY ) ) - QpBdOffsetY;
}

// TODO readd the CABAC mode specific code (following the unsigned int entropy_coding_mode_flag, renabling)
RetCode decode_macroblock_layer(CABACContext* cabac_ctx, struct bitbuf* bs,
                                              struct rv264macro* curr_mb_attr, unsigned int mb_row, unsigned int mb_col, unsigned int mb_idx, int PicWidthInMbs,
                                              slice_type_t slice_type_modulo5, unsigned int cfidc,
                                              unsigned int direct_8x8_inference_flag, unsigned int transform_8x8_mode_flag, unsigned int entropy_coding_mode_flag,
                                              unsigned int MbaffFrameFlag, unsigned int mb_field_decoding_flag,
                                              uint8_t num_ref_idx_active[2],
                                              int *QPY, int QPC[2], int BitDepthY, int BitDepthC, int QpBdOffsetY, int QpBdOffsetC,
                                              int chroma_qp_index_offset, int second_chroma_qp_index_offset, unsigned int qpprime_y_zero_transform_bypass_flag,
                                              uint16_t LevelScale4x4[6][6][16], uint16_t LevelScale8x8[2][6][64],
                                              unsigned int constrained_intra_pred_flag, unsigned int data_partitioning_slice_flag,
                                              int16_t* mb_data, unsigned int mb_data_size, int* prevMbAddr_mb_qp_delta)
{
  mb_type_t mb_type; //uint8_t
  int transform_size_8x8_flag = 0; // uint8_t
  int noSubMbPartSizeLessThan8x8Flag; // uint8_t
  unsigned int CodedBlockPatternLuma; // uint8_t
  unsigned int CodedBlockPatternChroma = 0; // uint8_t
  part_type_t MbPartPredMode0;
  sub_mb_type_t* sub_mb_type;

  mb_type = decode_mb_type(cabac_ctx, bs, entropy_coding_mode_flag, curr_mb_attr, slice_type_modulo5); // 2  ue(v) | ae(v)

  curr_mb_attr->mb_type = mb_type;

  if (mb_type == I_PCM) // Copy pcm samples into the residual buffer. They will be used latter on during picture reconstruction
  {
    int qPi;
    int i;
    int16_t* pcm_samples;
    int alignlen = (8 - (numbits(bs) & 0x7)) & 0x7;
    int pcm_alignment_zero_bit = alignlen ? getbits(bs, alignlen) : 0;
    if (pcm_alignment_zero_bit!=0)
      rvmessage("pcm_alignment_zero_bit not zero");

    pcm_samples = mb_data;
    for( i=0; i<256; i++)
      *pcm_samples++ =  getbits(bs, BitDepthY); // 2  u(v)
    for( i=0; i<MbSizeC[cfidc]*2; i++)
      *pcm_samples++ =  getbits(bs, BitDepthC); // 2  u(v)

    // Fills in the total coeffs that may be used for nC_cache
    memset(&curr_mb_attr->TotalCoeffLuma[0], 16, sizeof(curr_mb_attr->TotalCoeffLuma));
    memset(&curr_mb_attr->TotalCoeffChroma[0][0], 16, sizeof(curr_mb_attr->TotalCoeffChroma));

    *prevMbAddr_mb_qp_delta = 0;
    curr_mb_attr->intra_chroma_pred_mode = Intra_Chroma_DC;
    curr_mb_attr->QPy = 0;
    qPi = im_clip(-QpBdOffsetC, 51, chroma_qp_index_offset)-30;
    curr_mb_attr->QPc[0] = (qPi<0) ? qPi+30 : qPi_to_QPC[qPi]; // qPi if <30 or conv[qPi] if >=30
    qPi = im_clip(-QpBdOffsetC, 51, second_chroma_qp_index_offset)-30;
    curr_mb_attr->QPc[1] = (qPi<0) ? qPi+30 : qPi_to_QPC[qPi]; // qPi if <30 or conv[qPi] if >=30

    if (entropy_coding_mode_flag)
    {
      init_cabac_decoding_engine(cabac_ctx, bs);
      curr_mb_attr->CodedBlockPatternLuma = 0xf; // help to save one test when decoding coded_block_pattern SE
      curr_mb_attr->CodedBlockPatternChroma = 2;// "
      curr_mb_attr->luma_dc_coeffs_non_null = 1;
      curr_mb_attr->chroma_dc_coeffs_non_null = 3;

    }
  }
  else
  {
    noSubMbPartSizeLessThan8x8Flag = 1;
    MbPartPredMode0 = MbPartPredMode(mb_type, 0, 0, 0); // for the next test we know mb_type != I_NxN, so this is correct
    if( mb_type != I_NxN && (MbPartPredMode0 != Intra_16x16) && NumMbPart( mb_type ) == 4 )
    {
      int mbPartIdx;
      sub_mb_type = curr_mb_attr->sub_mb_type;

      if (!entropy_coding_mode_flag)
        sub_mb_pred_cavlc( bs, mb_type, sub_mb_type, MbaffFrameFlag, mb_field_decoding_flag, num_ref_idx_active, curr_mb_attr);
      else
        sub_mb_pred_cabac(cabac_ctx, bs, mb_type, sub_mb_type, mb_row&1, MbaffFrameFlag, mb_field_decoding_flag, PicWidthInMbs, num_ref_idx_active, curr_mb_attr);

      for( mbPartIdx = 0; mbPartIdx < 4; mbPartIdx++ )
        if( sub_mb_type[ mbPartIdx ] != B_Direct_8x8 )
        {
          if( NumSubMbPart( sub_mb_type[ mbPartIdx ] ) > 1 )
          {
            noSubMbPartSizeLessThan8x8Flag = 0;
            break;
          }
        }
        else if( !direct_8x8_inference_flag )
        {
          noSubMbPartSizeLessThan8x8Flag = 0;
          break;
        }
    }
    else
    {
      if (mb_type == I_NxN)
      {
        if( transform_8x8_mode_flag)
          transform_size_8x8_flag = decode_transform_size_8x8_flag( cabac_ctx, bs, entropy_coding_mode_flag, curr_mb_attr); // 2  u(1) | ae(v)
        MbPartPredMode0 = MbPartPredMode(mb_type, 0, transform_size_8x8_flag, 1); // We need to re set it because now we know the transform_size_8x8_flag
      }


      mb_pred(cabac_ctx, bs, entropy_coding_mode_flag, mb_type, MbPartPredMode0, transform_size_8x8_flag,
              cfidc, MbaffFrameFlag, mb_field_decoding_flag, constrained_intra_pred_flag,
              num_ref_idx_active,
              curr_mb_attr, mb_row&1, PicWidthInMbs);
    }
    if( MbPartPredMode0 != Intra_16x16 )
    {
      decode_coded_block_pattern(&CodedBlockPatternLuma, &CodedBlockPatternChroma,
          cabac_ctx, bs, entropy_coding_mode_flag, MbPartPredMode0, cfidc, curr_mb_attr);  // 2  me(v) | ae(v)
      curr_mb_attr->CodedBlockPatternLuma = CodedBlockPatternLuma;
      curr_mb_attr->CodedBlockPatternChroma = CodedBlockPatternChroma;
      if( CodedBlockPatternLuma > 0 && transform_8x8_mode_flag && mb_type != I_NxN && noSubMbPartSizeLessThan8x8Flag &&
        ( mb_type != B_Direct_16x16 || direct_8x8_inference_flag ) )
      {
        transform_size_8x8_flag = decode_transform_size_8x8_flag( cabac_ctx, bs, entropy_coding_mode_flag, curr_mb_attr); // 2  u(1) | ae(v)
        // not needed, the initial value of MbPartPredMode0 is still correct since mb_type != I_NxN  //MbPartPredMode0 = MbPartPredMode(mb_type, 0 , transform_size_8x8_flag); // it will be needed afterwards...
      }
    }
    else // MbPartPredMode0 == Intra_16x16
    {
      // derive CodedBlockPatternLuma and CodedBlockPatternChroma according to table 7.11 (cf ITU-T Rec. H.264 (03/2005) 87)
      curr_mb_attr->CodedBlockPatternLuma = CodedBlockPatternLuma = CodedBlockPatternLuma_MbPartHeight_array[mb_type];
      curr_mb_attr->CodedBlockPatternChroma = CodedBlockPatternChroma = cfidc == 0 ? 0 : CodedBlockPatternChroma_MbPartWidth_array[mb_type];
    }
    if( CodedBlockPatternLuma > 0 || CodedBlockPatternChroma > 0 || MbPartPredMode0 == Intra_16x16 )
    {
      int QPprimeY;
      int QPprimeC[2];
      int qPi;

      int16_t* curr_mb_data = mb_data;

      // Set all coeffs to zero. Needed because only non null coeffs are written in the subsequent code...
      memset(curr_mb_data, 0, mb_data_size*sizeof(*curr_mb_data)); // could move this to the picture data allocation ? that would make one call for the whole picture.
      //Would it make big overhead (for instance for skip mb, the data are not necessary)

      *prevMbAddr_mb_qp_delta = curr_mb_attr->mb_qp_delta = decode_mb_qp_delta(cabac_ctx, bs, entropy_coding_mode_flag, *prevMbAddr_mb_qp_delta); // 2  se(v) | ae(v)

      *QPY = derive_QPY(*QPY, curr_mb_attr->mb_qp_delta, QpBdOffsetY);
      QPprimeY = *QPY + QpBdOffsetY;

      qPi = im_clip(-QpBdOffsetC, 51, *QPY+chroma_qp_index_offset)-30;
      QPC[0] = (qPi<0) ? qPi+30 : qPi_to_QPC[qPi]; // qPi if <30 or conv[qPi] if >=30
      qPi = im_clip(-QpBdOffsetC, 51, *QPY+second_chroma_qp_index_offset)-30;
      QPC[1] = (qPi<0) ? qPi+30 : qPi_to_QPC[qPi]; // qPi if <30 or conv[qPi] if >=30
      QPprimeC[0] = QPC[0] + QpBdOffsetC;
      QPprimeC[1] = QPC[1] + QpBdOffsetC;

      residual(cabac_ctx, bs, MbPartPredMode0, curr_mb_attr, mb_type,
                CodedBlockPatternLuma, CodedBlockPatternChroma, mb_row&1,
                entropy_coding_mode_flag, transform_size_8x8_flag, cfidc,
                QPprimeY, QPprimeC, curr_mb_data, mb_field_decoding_flag, PicWidthInMbs,
                LevelScale4x4, LevelScale8x8, qpprime_y_zero_transform_bypass_flag,
                constrained_intra_pred_flag, data_partitioning_slice_flag, MbaffFrameFlag
              );
    }
    else // All coeffs are null
    {
      int16_t* curr_mb_data = mb_data;

      // Directly fills in the data that may be used for nC_cache
      memset(&curr_mb_attr->TotalCoeffLuma[0], 0, sizeof(curr_mb_attr->TotalCoeffLuma));
      memset(&curr_mb_attr->TotalCoeffChroma[0][0], 0, sizeof(curr_mb_attr->TotalCoeffChroma));
      curr_mb_attr->luma_dc_coeffs_non_null = 0;
      curr_mb_attr->chroma_dc_coeffs_non_null = 0;
      memset(curr_mb_data, 0, mb_data_size*sizeof(*curr_mb_data)); // could move this to the picture data allocation ? that would make one call for the whole picture.
      *prevMbAddr_mb_qp_delta = 0;
    }

    curr_mb_attr->QPy = *QPY;
    curr_mb_attr->QPc[0] = QPC[0];
    curr_mb_attr->QPc[1] = QPC[1];

  }
  curr_mb_attr->transform_size_8x8_flag = transform_size_8x8_flag;


  return LUDH264_SUCCESS;
}


// Derive neighbors mb flags (for MbaffFrameFlag==1) and neighbor availability
void derive_neighbors_flags(struct rv264macro* curr, unsigned int slice_num, unsigned int MbaffFrameFlag, unsigned int PicWidthInMbs,
    unsigned int curr_is_field, unsigned mb_col, unsigned mb_row)
{
  if (!MbaffFrameFlag)
  {
    curr->left_mb_is_available = mb_col > 0 && (curr-1)->slice_num == slice_num;
    curr->up_mb_is_available = mb_row > 0 && (curr-PicWidthInMbs)->slice_num == slice_num;
    curr->upleft_mb_is_available = mb_row > 0 && mb_col > 0 && (curr-1-PicWidthInMbs)->slice_num == slice_num;
    curr->upright_mb_is_available = mb_row > 0 &&  mb_col < (PicWidthInMbs-1) && (curr+1-PicWidthInMbs)->slice_num == slice_num;
  }
  else
  {
    // left mb
    struct rv264macro* left = curr - 1;
    curr->left_mb_is_available = mb_col > 0 && left->slice_num == slice_num;
    if (curr->left_mb_is_available) // TODO: not doing this test may provoke read outside the allocated block, to check how this work
      curr->left_mb_is_field = left->mb_field_decoding_flag;

    // up mb
    unsigned int is_bot_mb = mb_row & 1;
    struct rv264macro* up = curr - (PicWidthInMbs << (is_bot_mb && curr_is_field));
    curr->up_mb_is_available = mb_row > curr_is_field  && up->slice_num == slice_num;
    if (curr->up_mb_is_available) // TODO: not doing this test may provoke read outside the allocated block, to check how this work
      curr->up_mb_is_field = up->mb_field_decoding_flag;

    // up left mb
    struct rv264macro* upleft = curr -1 -(PicWidthInMbs << (is_bot_mb && curr_is_field));
    curr->upleft_mb_is_available = mb_row > curr_is_field && mb_col > 0 && upleft->slice_num == slice_num;
    if (curr->upleft_mb_is_available) // TODO: not doing this test may provoke read outside the allocated block, to check how this work
      curr->upleft_mb_is_field = upleft->mb_field_decoding_flag;

    // up right mb
    struct rv264macro* upright = curr +1 -(PicWidthInMbs << (is_bot_mb && curr_is_field));
    curr->upright_mb_is_available = mb_row > 1 && mb_col < (PicWidthInMbs-1) && upright->slice_num == slice_num;
    if (curr->upright_mb_is_available) // TODO: not doing this test may provoke read outside the allocated block, to check how this work
      curr->upright_mb_is_field = upright->mb_field_decoding_flag;

  }
}


void decode_skip_mb(struct rv264macro* curr_mb_attr, unsigned int slice_type_modulo5, int QPY, int QPC[2], unsigned int slice_num)
{
  // Emulate TotalCoeff for skip blocks. there are derived as 0
  memset(&curr_mb_attr->TotalCoeffLuma[0], 0, sizeof(curr_mb_attr->TotalCoeffLuma));
  memset(&curr_mb_attr->TotalCoeffChroma[0][0], 0, sizeof(curr_mb_attr->TotalCoeffChroma));

  curr_mb_attr->mb_skip_flag = 1;
  curr_mb_attr->slice_num = slice_num;
  curr_mb_attr->mb_type = slice_type_modulo5 == SLICE_B ?  B_Skip : P_Skip;
  curr_mb_attr->transform_size_8x8_flag = 0;
  curr_mb_attr->TransformBypassModeFlag = 0;
  curr_mb_attr->luma_dc_coeffs_non_null = 0;
  curr_mb_attr->chroma_dc_coeffs_non_null = 0;
  curr_mb_attr->QPy = QPY;
  curr_mb_attr->QPc[0] = QPC[0];
  curr_mb_attr->QPc[1] = QPC[1];
  curr_mb_attr->CodedBlockPatternLuma = 0; // help to save one test when decoding cabac coded_block_pattern SE
  curr_mb_attr->CodedBlockPatternChroma = 0; // "

}


int derive_mb_field_decoding_flag(struct rv264macro* curr_mb_attr, unsigned int mb_row, unsigned int mb_col, int PicWidthInMbs, unsigned int slice_num)
{
  // This is a pair macroblock (by definition)
  if (mb_row % 2 == 1)   // Check the other macroblock if available (i.e. this is the second of the pair)
  {
    LUD_DEBUG_ASSERT(curr_mb_attr[-PicWidthInMbs].slice_num == slice_num); // macroblock pair should be in the same slice !
    return curr_mb_attr[-PicWidthInMbs].mb_field_decoding_flag;
  }
  else if (mb_col>0 && curr_mb_attr[-1].slice_num == slice_num) // Check for the left macroblock pair
    return curr_mb_attr[-1].mb_field_decoding_flag;
  else if (mb_row>0 && curr_mb_attr[-PicWidthInMbs].slice_num == slice_num) // Check for the top macroblock pair
    return curr_mb_attr[-PicWidthInMbs].mb_field_decoding_flag;
  else
    return 0;
}

unsigned int decode_mb_field_decoding_flag(CABACContext* cabac_ctx, struct bitbuf* bs, unsigned int entropy_coding_mode_flag,
    struct rv264macro* curr_mb_attr, unsigned int mb_row, unsigned int PicWidthInMbs, unsigned int slice_num)
{
  if (!entropy_coding_mode_flag)
    return getbit(bs);
  else
    return cabac_decode_mb_field_decoding_flag(cabac_ctx, bs, curr_mb_attr, mb_row, PicWidthInMbs, slice_num);
}

#define CALC_MB_COORDS(currmb, mbidx, mbrow, mbcol, mbflag, picwidth) \
if (mbflag)                                                           \
{                                                                     \
  int top = (currmb)%2;                                               \
  int idx = (currmb)/2;                                               \
  (mbrow) = ((idx) / (picwidth))*2 + top;                             \
  (mbcol) = (idx) % (picwidth);                                       \
  (mbidx) = (mbrow) * (picwidth) + (mbcol);                           \
}                                                                     \
else                                                                  \
{                                                                     \
  (mbrow) = (currmb) / (picwidth);                                    \
  (mbcol) = (currmb) % (picwidth);                                    \
  (mbidx) = (currmb);                                                 \
}

#define CALC_MB_IDX(currmb, mbidx, mbflag, picwidth)                  \
if (mbflag)                                                           \
{                                                                     \
  int top = (currmb)%2;                                               \
  int idx = (currmb)/2;                                               \
  int mbrow = ( (idx) / (picwidth) )*2 + top;                         \
  int mbcol = (idx ) % (picwidth);                                    \
  (mbidx) = (mbrow) * (picwidth) + (mbcol);                           \
}                                                                     \
else                                                                  \
{                                                                     \
  (mbidx) = (currmb);                                                 \
}


RetCode decode_slice_data(struct slice_header* sh)
{
  struct rv264seq_parameter_set* sps = sh->sps;
  struct rv264pic_parameter_set* pps = sh->pps;
  nal_unit_t* nalu = sh->nalu;
  struct bitbuf* bs = nalu->bs;
  unsigned int cfidc = sps->chroma_format_idc;
  unsigned int BitDepthY = sps->BitDepthY;
  unsigned int BitDepthC = sps->BitDepthC;
  unsigned int QpBdOffsetY = sps->QpBdOffsetY;
           int QpBdOffsetC = sps->QpBdOffsetC;
  unsigned int MbaffFrameFlag = sh->MbaffFrameFlag;
  unsigned int entropy_coding_mode_flag = pps->entropy_coding_mode_flag;
  int i;

  int mb_nb = 0;
  unsigned int PicWidthInMbs = sps->PicWidthInMbs;
  int CurrMbAddr = sh->first_mb_in_slice * (1 + MbaffFrameFlag);
  int moreDataFlag = 1;
  int prevMbSkipped = 0;
  int mb_skip_run;
  unsigned int mb_skip_flag = 0;
  uint8_t mb_field_decoding_flag = sh->field_pic_flag;
  int mb_row, mb_col, mb_idx; // Those numbers are computed so that it is real raster scan even when MbaffFrameFlag is set.
  unsigned int bottom_field_flag = sh->bottom_field_flag;
  unsigned int slice_num = sh->slice_num;
  struct rv264picture* pic = sh->pic;
  struct rv264macro* mb_attr = pic->field_data[bottom_field_flag].mb_attr;
  int16_t* data = pic->field_data[bottom_field_flag].data;
  int16_t* mb_data;
  unsigned int mb_data_size = 256+2*MbSizeC[cfidc];
  int QPY = sh->SliceQPY;
  int QPC[2];
  CABACContext cabac_ctx;
  slice_type_t slice_type_modulo5 = sh->slice_type_modulo5;
  int prevMbAddr_mb_qp_delta = 0;
  unsigned int data_partitioning_slice_flag = is_data_partitioning_slice(nalu->nal_unit_type);

  {     // not really sure this is necessary ?! But safer anyway, in case first mb of the slice is not Intra16x16 and cbp==0, QPC will still have a default value...
    int qPi;
    qPi = im_clip(-QpBdOffsetC, 51, QPY + pps->chroma_qp_index_offset)-30;
    QPC[0] = (qPi<0) ? qPi+30 : qPi_to_QPC[qPi]; // qPi if <30 or conv[qPi] if >=30
    qPi = im_clip(-QpBdOffsetC, 51, QPY + pps->second_chroma_qp_index_offset)-30;
    QPC[1] = (qPi<0) ? qPi+30 : qPi_to_QPC[qPi]; // qPi if <30 or conv[qPi] if >=30
  }


  if (entropy_coding_mode_flag)
  {
    // Align on 8-bits boundary
    int alignlen = (8 - (numbits(bs) & 0x7)) & 0x7;
    int cabac_alignment_one_bits = alignlen ? getbits(bs, alignlen) : 0;
    if (cabac_alignment_one_bits != (1<<alignlen)-1)
      rvmessage("cabac_alignment_one_bits not all ones");

    // Initialize cabac decoding context and engine
    init_cabac_context_variables(&cabac_ctx, QPY, sh->cabac_init_idc, slice_type_modulo5);
    init_cabac_decoding_engine(&cabac_ctx, bs);

  }

  do
  {
    /* get pointer to current macroblock */
    CALC_MB_COORDS(CurrMbAddr, mb_idx, mb_row, mb_col, MbaffFrameFlag, PicWidthInMbs);
    if (mb_idx >= sh->PicSizeInMbs)
    {
      LUD_TRACE(TRACE_ERROR, "too many macroblocks in picture: %d", mb_idx);
      break;
    }
    struct rv264macro *curr_mb_attr = mb_attr + mb_idx;

    if (slice_type_modulo5 != SLICE_I && slice_type_modulo5 != SLICE_SI)
    {
      if (!entropy_coding_mode_flag)
      {
        mb_skip_run = uexpbits_ludh264(bs, "mb_skip_run"); // 2   ue(v)
        prevMbSkipped = (mb_skip_run > 0);
        for (i = 0; i < mb_skip_run; i++)
        {
          // Generate a Skip macroblock...
          decode_skip_mb(curr_mb_attr, slice_type_modulo5, QPY, QPC, slice_num);

          mb_field_decoding_flag = curr_mb_attr->mb_field_decoding_flag =
              (MbaffFrameFlag == 0) ? sh->field_pic_flag : derive_mb_field_decoding_flag(curr_mb_attr, mb_row, mb_col, PicWidthInMbs, slice_num);
          derive_neighbors_flags(curr_mb_attr, slice_num, MbaffFrameFlag, PicWidthInMbs, mb_field_decoding_flag, mb_col, mb_row);

          // Next macroblock
          CurrMbAddr = NextMbAddress(sh->MbToSliceGroupMap, CurrMbAddr, sh->PicSizeInMbs, pps->num_slice_groups_minus1);
          CALC_MB_COORDS(CurrMbAddr, mb_idx, mb_row, mb_col, MbaffFrameFlag, PicWidthInMbs);
          LUD_DEBUG_ASSERT(mb_idx < sh->PicSizeInMbs || !more_rbsp_data_lud(nalu, bs));
          curr_mb_attr = mb_attr + mb_idx;
        }
        mb_nb += mb_skip_run;
        moreDataFlag = more_rbsp_data_lud(nalu, bs);
        if (!moreDataFlag)
          break;
        // prevMbAddr_mb_qp_delta = 0; only necessary for cabac decoding !
        mb_field_decoding_flag = (MbaffFrameFlag == 0) ? sh->field_pic_flag : derive_mb_field_decoding_flag(curr_mb_attr, mb_row, mb_col, PicWidthInMbs, slice_num);
        derive_neighbors_flags(curr_mb_attr, slice_num, MbaffFrameFlag, PicWidthInMbs, mb_field_decoding_flag, mb_col, mb_row);
      }
      else // entropy_coding_mode_flag
      {
        mb_field_decoding_flag =
          (MbaffFrameFlag == 0) ? sh->field_pic_flag : derive_mb_field_decoding_flag(curr_mb_attr, mb_row, mb_col, PicWidthInMbs, slice_num);

        derive_neighbors_flags(curr_mb_attr, slice_num, MbaffFrameFlag, PicWidthInMbs, mb_field_decoding_flag, mb_col, mb_row);
        cabac_derive_mb_neighbors(&cabac_ctx, curr_mb_attr, mb_row&1, mb_field_decoding_flag, PicWidthInMbs, MbaffFrameFlag);
        mb_skip_flag = cabac_decode_mb_skip_flag(&cabac_ctx, bs, curr_mb_attr, slice_type_modulo5);

        moreDataFlag = !mb_skip_flag;

        if (mb_skip_flag)
        {
          // Generate a Skip macroblock...
          prevMbAddr_mb_qp_delta = 0;
          decode_skip_mb(curr_mb_attr, slice_type_modulo5, QPY, QPC, slice_num);
          curr_mb_attr->mb_field_decoding_flag = mb_field_decoding_flag;
          mb_nb++;
        }
      }
    }
    else // I or SI  slice
    {
      derive_neighbors_flags(curr_mb_attr, slice_num, MbaffFrameFlag, PicWidthInMbs, mb_field_decoding_flag, mb_col, mb_row);
      if (entropy_coding_mode_flag)
        cabac_derive_mb_neighbors(&cabac_ctx, curr_mb_attr, mb_row&1, mb_field_decoding_flag, PicWidthInMbs, MbaffFrameFlag);
    }

    if (moreDataFlag)
    {
      if (MbaffFrameFlag)
      {
        unsigned int prev = mb_field_decoding_flag;
        if (CurrMbAddr % 2 == 0) // this is a top MB, read the flag
          mb_field_decoding_flag = decode_mb_field_decoding_flag(&cabac_ctx, bs, entropy_coding_mode_flag,
              curr_mb_attr, mb_row, PicWidthInMbs, slice_num); //  2   u(1) | ae(v)
        else if (prevMbSkipped) // this is a bottom MB, and the top MB was skipped, read the flag and set it for the top MB as well
        {
          mb_field_decoding_flag = decode_mb_field_decoding_flag(&cabac_ctx, bs, entropy_coding_mode_flag,
                        curr_mb_attr, mb_row, PicWidthInMbs, slice_num); //  2   u(1) | ae(v)
          curr_mb_attr[-(int)PicWidthInMbs].mb_field_decoding_flag = mb_field_decoding_flag;
          // re-derive neighbors flags since it may have changed between field / frame for the previous mb !
          derive_neighbors_flags(curr_mb_attr-PicWidthInMbs, slice_num, 1, PicWidthInMbs, mb_field_decoding_flag, mb_col, mb_row-1);
        }
        else if (!entropy_coding_mode_flag) // when doing cabac, this flag was already derived just above
          mb_field_decoding_flag = derive_mb_field_decoding_flag(curr_mb_attr, mb_row, mb_col, PicWidthInMbs, slice_num);

        // We need to recalculate the neighbors mb if mb_field_decoding_flag changed from what was guessed above
        if (prev != mb_field_decoding_flag)
        {
          derive_neighbors_flags(curr_mb_attr, slice_num, 1, PicWidthInMbs, mb_field_decoding_flag, mb_col, mb_row);
          if (entropy_coding_mode_flag)
            cabac_derive_mb_neighbors(&cabac_ctx, curr_mb_attr, mb_row&1, mb_field_decoding_flag, PicWidthInMbs, 1);
        }
      }

      /* prepare the macroblock attributes */
      curr_mb_attr->occupied = 1;
      curr_mb_attr->slice_num = slice_num;
      curr_mb_attr->sh = sh;
      curr_mb_attr->mb_skip_flag = 0;
      curr_mb_attr->mb_field_decoding_flag = mb_field_decoding_flag;
      mb_data = data + mb_idx * mb_data_size;
      decode_macroblock_layer(&cabac_ctx, bs,
                              curr_mb_attr, mb_row, mb_col, mb_idx, PicWidthInMbs,
                              slice_type_modulo5, cfidc,
                              sps->direct_8x8_inference_flag, pps->transform_8x8_mode_flag, entropy_coding_mode_flag,
                              MbaffFrameFlag, mb_field_decoding_flag,
                              sh->num_ref_idx_active,
                              &QPY, QPC, BitDepthY, BitDepthC, QpBdOffsetY, QpBdOffsetC,
                              pps->chroma_qp_index_offset, pps->second_chroma_qp_index_offset, sps->qpprime_y_zero_transform_bypass_flag,
                              pps->LevelScale4x4, pps->LevelScale8x8,
                              pps->constrained_intra_pred_flag, data_partitioning_slice_flag,
                              mb_data, mb_data_size, &prevMbAddr_mb_qp_delta);


      mb_nb++;
    }

    if (!entropy_coding_mode_flag)
      moreDataFlag = more_rbsp_data_lud(nalu, bs);
    else
    {
      if (slice_type_modulo5 != SLICE_I && slice_type_modulo5 != SLICE_SI)
        prevMbSkipped = mb_skip_flag;
      if (MbaffFrameFlag && CurrMbAddr % 2 == 0)
        moreDataFlag = 1;
      else
      {
        unsigned int end_of_slice_flag = cabac_decode_end_of_slice_flag(&cabac_ctx, bs); // 2 ae(v)
        moreDataFlag = !end_of_slice_flag;
      }
    }

    CurrMbAddr = NextMbAddress(sh->MbToSliceGroupMap, CurrMbAddr, sh->PicSizeInMbs, pps->num_slice_groups_minus1);
  }
  while (moreDataFlag);

  pic->field_sh[bottom_field_flag][slice_num]->mb_nb = mb_nb;

  //printf("decoded %d mb\n", mb_idx);
  if (parse_rbsp_slice_trailing_bits(nalu, entropy_coding_mode_flag)!=LUDH264_SUCCESS)
    rvmessage("failed to parse rbsp slice trailing bits");

  release_nalu(nalu); // do not need the nal unit data anymore !

  return LUDH264_SUCCESS;
}
