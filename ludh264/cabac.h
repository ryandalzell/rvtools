/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/

#ifndef CABAC_H_
#define CABAC_H_

#include "decode_slice_data.h"

void cabac_init_data();

uint8_t cabac_ctx_vars(int preCtxState);
void init_cabac_context_variables(CABACContext* cabac_ctx, int SliceQPY, unsigned int cabac_init_idc, slice_type_t slice_type_modulo5);
void init_cabac_decoding_engine(CABACContext* cabac_ctx, struct bitbuf* bs);
void refill_buffer(CABACContext* cabac_ctx, struct bitbuf* bs);
void refill_buffer_aligned(CABACContext* cabac_ctx, struct bitbuf* bs);
void renormalization_process(CABACContext* cabac_ctx, struct bitbuf* bs);
void renormalization_process_once(CABACContext* cabac_ctx, struct bitbuf* bs);
unsigned int decode_cabac_1bit(CABACContext* cabac_ctx, struct bitbuf* bs, uint8_t* state_vars);
unsigned int decode_cabac_bypass_bit(CABACContext* cabac_ctx, struct bitbuf* bs);
unsigned int decode_cabac_termination_bit(CABACContext* cabac_ctx, struct bitbuf* bs);
void cabac_derive_mb_neighbors(CABACContext* cabac_ctx, struct rv264macro* curr_mb_attr,
    unsigned int curr_is_bot, unsigned int mb_field_decoding_flag, unsigned int PicWidthInMbs,
    unsigned int MbaffFrameFlag);
unsigned int cabac_decode_mb_skip_flag(CABACContext* cabac_ctx, struct bitbuf* bs,
    struct rv264macro* curr_mb_attr, slice_type_t slice_type_modulo5);
unsigned int cabac_decode_end_of_slice_flag(CABACContext* cabac_ctx, struct bitbuf* bs);
unsigned int cabac_decode_mb_field_decoding_flag(CABACContext* cabac_ctx, struct bitbuf* bs, struct rv264macro* curr_mb_attr,
    unsigned int mb_row, unsigned int PicWidthInMbs, unsigned int slice_num);
mb_type_t cabac_decode_I_mb_type(CABACContext* cabac_ctx, struct bitbuf* bs, struct rv264macro* curr,
    unsigned int ctxIdxOffset, unsigned int is_intra_slice);
mb_type_t cabac_decode_mb_type(CABACContext* cabac_ctx, struct bitbuf* bs, struct rv264macro* curr, slice_type_t slice_type_modulo5);
sub_mb_type_t cabac_decode_sub_mb_type(CABACContext* cabac_ctx, struct bitbuf* bs, unsigned int is_B_mb);
unsigned int cabac_decode_transform_size_8x8_flag(CABACContext* cabac_ctx, struct bitbuf* bs, struct rv264macro* curr_mb_attr);
void cabac_decode_coded_block_pattern(unsigned int* CodedBlockPatternLuma, unsigned int* CodedBlockPatternChroma,
    CABACContext* cabac_ctx, struct bitbuf* bs, struct rv264macro* curr, unsigned int cfidc);
int cabac_decode_mb_qp_delta(CABACContext* cabac_ctx, struct bitbuf* bs, int prevMbAddr_mb_qp_delta);
unsigned int cabac_decode_prev_intra_pred_mode_flag(CABACContext* cabac_ctx, struct bitbuf* bs);
intra_4x4_pred_mode_t cabac_decode_rem_intra_pred_mode(CABACContext* cabac_ctx, struct bitbuf* bs);
intra_chroma_pred_mode_t cabac_decode_intra_chroma_pred_mode(CABACContext* cabac_ctx, struct bitbuf* bs, struct rv264macro* curr);
int8_t cabac_decode_ref_idx_lX(CABACContext* cabac_ctx, struct bitbuf* bs, int8_t refIdx_cache[25],
    int r5x5idx, int xoff, int yoff);
int16_t cabac_decode_mvd_lX(CABACContext* cabac_ctx, struct bitbuf* bs, int16_t mv_cache[25][2],
    int r5x5idx, unsigned int compIdx, int xoff, int yoff);
unsigned int cabac_decode_coded_block_flag(CABACContext* cabac_ctx, struct bitbuf* bs,
    unsigned int ctxBlockCat, uint8_t* nC_cache, unsigned int cfidc, unsigned int lc4x4blockIdx);
unsigned int cabac_decode_significant_coeff_flag(CABACContext* cabac_ctx, struct bitbuf* bs,
    unsigned int ctxBlockCat, unsigned int levelListIdx, unsigned int mb_field_decoding_flag, unsigned int cfidc);
unsigned int cabac_decode_last_significant_coeff_flag(CABACContext* cabac_ctx, struct bitbuf* bs,
    unsigned int ctxBlockCat, unsigned int levelListIdx, unsigned int mb_field_decoding_flag, unsigned int cfidc);
unsigned int cabac_decode_coeff_abs_level_minus1(CABACContext* cabac_ctx, struct bitbuf* bs,
    unsigned int numDecodAbsLevelEq1, unsigned int numDecodAbsLevelGt1, unsigned int ctxBlockCat);
unsigned int cabac_decode_coeff_sign_flag(CABACContext* cabac_ctx, struct bitbuf* bs);

#endif /* CABAC_H_ */
