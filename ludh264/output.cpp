/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/


#include <stdio.h>
#include <string.h>

#include "decode.h"
#include "system.h"
#include "defaulttables.h"
#include "../rvh264.h"


static void add_pic(DecodedPictureBuffer *ob, struct rv264picture* pic)
{
  OUTPICLIST* p = (OUTPICLIST *)rvalloc(NULL, sizeof(*p), 0); // could use a preallocated table so prevent malloc calls
  p->pic = pic;
  p->next = 0;
  ob->tail->next = p;
  ob->tail = p;
}

struct rv264picture* get_pic(DecodedPictureBuffer *ob)
{
  if (ob->head == 0)
    return 0;

  OUTPICLIST* p = ob->head;
  struct rv264picture* pic = p->pic;
  ob->head = p->next;
  rvfree(p);

  if (!ob->head)
    ob->tail = (OUTPICLIST*) &ob->head;

  return pic;
}

// Output buffer
/*#define OUTPUT_BUFFER_SIZE 64
static struct rv264picture* pic_buffer[OUTPUT_BUFFER_SIZE];
unsigned int read_idx=0, write_idx=0, num=0;

static void add_pic(struct rv264picture* pic)
{
  LUD_DEBUG_ASSERT(num<OUTPUT_BUFFER_SIZE);
  pic_buffer[write_idx] = pic;
  write_idx = (write_idx+1)%OUTPUT_BUFFER_SIZE;
  num++;
}
struct rv264picture* get_pic()
{
  if (num == 0)
    return 0;
  struct rv264picture* pic = pic_buffer[read_idx];
  read_idx = (read_idx+1)%OUTPUT_BUFFER_SIZE;
  num--;

  return pic;
}*/



// Decoded Picture Buffer

#define NEXT(a) ((a+1) & 0xF)
#define PREV(a) ((a-1) & 0xF)


static void compute_image_size(struct rv264picture* image)
{
  struct slice_header* sh = image->field_sh[0][0];
  struct rv264seq_parameter_set* sps = sh->sps;
  unsigned int FrameHeightInMbs = sps->FrameHeightInMbs;
  unsigned int cfidc = sps->chroma_format_idc;
  unsigned int PicWidthInMbs = sps->PicWidthInMbs;

  image->width = PicWidthInMbs*16;
  image->height = FrameHeightInMbs*16;
  image->image_buffer_size = PicWidthInMbs*FrameHeightInMbs*(256+2*MbSizeC[cfidc])*sizeof(pixel_t);
}

static void output_image(DecodedPictureBuffer *ob, struct rv264picture* image)
{
  add_pic(ob, image);
}

static void flush_images(DecodedPictureBuffer *ob)
{
  int i;

  for(i=0; i<ob->nb_of_images; i++)
  {
    output_image(ob, ob->images[ob->first]);
    ob->first = NEXT(ob->first);
  }
  ob->nb_of_images = 0;
}

static void insert_image(DecodedPictureBuffer *ob, struct rv264picture* image)
{
  // If this is an IDR (or mmco5), flush it and then add the images into the output buffer.
  // IDR and mmco5 have poc==0
  if (image->PicOrderCnt == 0)
  {
    flush_images(ob);
    //printf("inserting IDR pic !\n");
    ob->images[ob->last] = image;
    ob->last = NEXT(ob->last);
    ob->nb_of_images++;
  }
  else // Add the image in the buffer, sort it by its poc number
  {
    if (ob->nb_of_images >= 15)
    {
      // The buffer is full, need to output an image
      output_image(ob, ob->images[ob->first]);
      ob->first = NEXT(ob->first);
      ob->nb_of_images--;
    }

    unsigned int idx = PREV(ob->last);
    while (idx != PREV(ob->first) && ob->images[idx]->PicOrderCnt > image->PicOrderCnt)
    {
      ob->images[NEXT(idx)] = ob->images[idx]; // shift by one the image in order to let space for inserting the new image
      idx = PREV(idx);
    }
    ob->images[NEXT(idx)] = image;
    ob->last = NEXT(ob->last);
    ob->nb_of_images++;
  }
}


void init_dpb(DecodedPictureBuffer *ob)
{
  ob->first = 0;
  ob->last = 0;
  ob->nb_of_images = 0;
  ob->last_added_image = 0;
}

void add_image_to_dpb(DecodedPictureBuffer *ob, struct rv264picture* image)
{
  // Detect if this is the second field of a pair
  if (image == ob->last_added_image)
  {
    // If so, nothing to do !
    return;
  }

  compute_image_size(image);

  // Insert the last picture in the buffer
  if (ob->last_added_image)
    insert_image(ob, ob->last_added_image);
    //output_image(&ob->last_added_image);


  // Store the last image data
  ob->last_added_image = image;

  // Hold the picture so it wont be free'd elsewhere
  use_picture_dec_data(image);
  use_picture(image);

  return;
}


void flush_dpb(DecodedPictureBuffer *ob)
{
  if (ob->last_added_image)
    insert_image(ob, ob->last_added_image);
  ob->last_added_image = 0;
  flush_images(ob);


}

void close_dpb(DecodedPictureBuffer *ob)
{
}
