/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/

#include "motion_compensation.h"
#include "motion_vector.h"
#include "defaulttables.h"
#include "inverse_transforms.h"
#include "intmath.h"


// SET / GET the part size (width or height) in the macroblock partWidth/Height 'array' (a uint32_t in fact)
#define GET_PART_SIZE(array, idx) (((array)>>(idx<<1)) &  3)


void refrence_picture_selection_process(int refIdxLX, RefListEntry* RefPicListX, unsigned int field_pic_flag,
    unsigned int mb_field_decoding_flag, unsigned int mb_row,
    RefListEntry** refPic, unsigned int* field)
{

  if (field_pic_flag || !mb_field_decoding_flag)
  {
    *refPic = &RefPicListX[refIdxLX];
    *field = RefPicListX[refIdxLX].parity;
  }
  else
  {
    *refPic = &RefPicListX[refIdxLX>>1];
    *field = (mb_row % 2) ^ (refIdxLX % 2);
  }
}
static const uint8_t NextSubPart4x4Idx[3][3][16] = // partWidth, partHeight, curr4x4Idx
  {
    // w=0 (4)
    {
      {  1,  4,  3,  6,  5,  2,  7,  8,  9, 12, 11, 14, 13, 10, 15, 16}, //h=0 (4)
      {  1,  2,  3,  8,N_A,N_A,N_A,N_A,  9, 10, 11, 16,N_A,N_A,N_A,N_A}, //h=1 (8)
      {N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A}, //h=2 (16) (not possible when width is 4)
    },
    // w=1 (8)
    {
      {  4,N_A,  6,N_A,  2,N_A,  8,N_A, 12,N_A, 14,N_A, 10,N_A, 16,N_A}, //h=0 (4)
      {  2,N_A,  8,N_A,N_A,N_A,N_A,N_A, 10,N_A, 16,N_A,N_A,N_A,N_A,N_A}, //h=1 (8)
      {  2,N_A, 16,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A}, //h=2 (16)
    },
    // w=2 (16)
    {
      {N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A}, //h=0 (4) (not possible when width is 16)
      {  8,N_A,N_A,N_A,N_A,N_A,N_A,N_A, 16,N_A,N_A,N_A,N_A,N_A,N_A,N_A}, //h=1 (8)
      { 16,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A,N_A}, //h=2 (16)
    },
  };

static const uint8_t partSizeId[3] = {1, 2, 4};

static const uint8_t partSizeConv[3][3][3] = // [cfidc-1][partWidth][partHeight]
{
    {{0, 1, N_A}, {2, 3, 4}, {N_A, 5, 6}}, // cfidc = 1
    {{1, 10, N_A}, {3, 4, 11}, {N_A, 6, 7}}, // cfidc = 2
    {{3, 4, N_A}, {5, 6, 7}, {N_A, 8, 9}}, // cfidc = 3 or luma
};

void decoding_process_for_inter_prediction_samples(struct rv264macro* curr_mb_attr, slice_type_t slice_type_modulo5,
    unsigned int weighted_pred_flag, unsigned int weighted_bipred_idc, unsigned int mb_row, unsigned int mb_col, int FieldOrderCnt[2],
    RefListEntry* RefPicList[2], unsigned int field_pic_flag, unsigned int bottom_field_flag, unsigned int mb_field_decoding_flag,
    unsigned int MbaffFrameFlag, pixel_t* mb_Y_samples_, pixel_t* mb_C_samples_[2], pred_weight_table_t* wt,
    unsigned int PicWidthY, unsigned int PicWidthC, unsigned int strideY, unsigned int strideC, unsigned int PicHeightY, unsigned int PicHeightC,
    unsigned int clipY, unsigned int clipC,
    unsigned int cfidc, unsigned int mbc_width, unsigned int mbc_height)
{
  // need partIdx, partWidth, partHeight, pos of the upper left
  unsigned int partSizeIdx = 0;
  unsigned int partWidth4;
  unsigned int partHeight4;
  unsigned int idx = 0; // raster 4x4 block idx
  unsigned int x, y;
  unsigned int pos_x;
  unsigned int pos_y;
  pixel_t* mb_Y_samples;
  pixel_t* mb_C_samples[2];
  unsigned int is_mbaff_and_field = (MbaffFrameFlag && mb_field_decoding_flag);

  pos_x = mb_col*16;
  pos_y = (mb_row>>is_mbaff_and_field)*16;
  PicHeightY >>= is_mbaff_and_field;
  PicHeightC >>= is_mbaff_and_field;


  if (slice_type_modulo5 != SLICE_B) // We have a P or SP slice
  {
    while (idx != 16)
    {
      RefListEntry* refPic;
      unsigned int field;
      unsigned int nextIdx;
      const pixel_t* srcY;
      const pixel_t* srcC[2];
      partWidth4 = GET_PART_SIZE(curr_mb_attr->partWidths, partSizeIdx);
      partHeight4 = GET_PART_SIZE(curr_mb_attr->partHeights, partSizeIdx);
      LUD_DEBUG_ASSERT(partWidth4 < 3 && partHeight4 < 3);
      partSizeIdx++;
      nextIdx = NextSubPart4x4Idx[partWidth4][partHeight4][idx];
      LUD_DEBUG_ASSERT(nextIdx != (uint8_t)N_A);
      int partSizeY = partSizeConv[2][partWidth4][partHeight4];
      int partSizeC = N_A;
      if (cfidc)
        partSizeC = partSizeConv[cfidc-1][partWidth4][partHeight4];
      partWidth4 = partSizeId[partWidth4];
      partHeight4 = partSizeId[partHeight4];

      int mvxY, mvyY;
      int mvyC_offset;
      int refIdxL0 = curr_mb_attr->RefIdxL[0][idx];
      LUD_DEBUG_ASSERT(refIdxL0 >= 0);
      mvxY = curr_mb_attr->mv[0][idx][0];
      mvyY = curr_mb_attr->mv[0][idx][1];


      x = ((idx & 3) << 2);
      y = (idx & 0xC);

      mb_Y_samples = mb_Y_samples_ + x + y * strideY;
      if (cfidc)
      {
        mb_C_samples[0] = mb_C_samples_[0] + x/SubWidthC[cfidc] + y * strideC / SubHeightC[cfidc];
        mb_C_samples[1] = mb_C_samples_[1] + x/SubWidthC[cfidc] + y * strideC / SubHeightC[cfidc];
      }

      x += pos_x;
      y += pos_y;

      refrence_picture_selection_process(refIdxL0, RefPicList[0], field_pic_flag,
          mb_field_decoding_flag, mb_row, &refPic, &field);
      LUD_DEBUG_ASSERT(refPic->ref_pic!=NULL);

      // derive the offset of the chroma motion vector
      if ((cfidc != 1) || !mb_field_decoding_flag || (field_pic_flag && bottom_field_flag == field) || (!field_pic_flag && (mb_row%2) == field))
        mvyC_offset = 0;
      else
        mvyC_offset = field ? -2 : 2;



      srcY = refPic->ref_pic->Y+(field!=0)*PicWidthY;
      srcC[0] = refPic->ref_pic->C[0]+(field!=0)*PicWidthC;
      srcC[1] = refPic->ref_pic->C[1]+(field!=0)*PicWidthC;


      if (!weighted_pred_flag)
      {
        // Just interpolate the sample from the ref pic, no weighted pred. The result is directly put into the dst picture
        luma_samples_interpolation_process(x, y, mvxY, mvyY, partWidth4, partHeight4,
            srcY, strideY, mb_Y_samples, strideY, PicWidthY, PicHeightY, clipY);
        if (cfidc)
          chroma_samples_interpolation_process(x, y, mvxY, mvyY+mvyC_offset, partWidth4, partHeight4,
            srcC, strideC, mb_C_samples, strideC, PicWidthC, PicHeightC, cfidc);
      }
      else // weighted_pred_flag==1
      {
        int refIdxLXWP = refIdxL0 >> (MbaffFrameFlag && mb_field_decoding_flag);

        // Do the interpolation (put the result into the dst picture, the weighted pred will be done in-place)
        luma_samples_interpolation_process(x, y, mvxY, mvyY, partWidth4, partHeight4,
            srcY, strideY, mb_Y_samples, strideY, PicWidthY, PicHeightY, clipY);
        explicit_weigthed_sample_prediction_process_monopred(partSizeY,
            (const pixel_t*)mb_Y_samples, strideY, mb_Y_samples, strideY, clipY,
            wt->luma_log2_weight_denom,
            refIdxLXWP, wt->luma_weight_l[0], wt->luma_offset_l[0]);

        if (cfidc)
        {
          chroma_samples_interpolation_process(x, y, mvxY, mvyY+mvyC_offset, partWidth4, partHeight4,
              srcC, strideC, mb_C_samples, strideC, PicWidthC, PicHeightC, cfidc);

          explicit_weigthed_sample_prediction_process_monopred(partSizeC,
              (const pixel_t*)mb_C_samples[0], strideC, mb_C_samples[0], strideC, clipC,
              wt->chroma_log2_weight_denom,
              refIdxLXWP, wt->chroma_weight_l[0][0], wt->chroma_offset_l[0][0]);
          explicit_weigthed_sample_prediction_process_monopred(partSizeC,
              (const pixel_t*)mb_C_samples[1], strideC, mb_C_samples[1], strideC, clipC,
              wt->chroma_log2_weight_denom,
              refIdxLXWP, wt->chroma_weight_l[1][0], wt->chroma_offset_l[1][0]);

        }
      }

      idx = nextIdx;
    }

  }
  else // We have a B Slice
  {
    while (idx != 16)
    {
      RefListEntry* refPic;
      unsigned int field;
      unsigned int nextIdx;
      const pixel_t* srcY;
      const pixel_t* srcC[2];
      int i;
      int refIdxL[2];

      partWidth4 = GET_PART_SIZE(curr_mb_attr->partWidths, partSizeIdx);
      partHeight4 = GET_PART_SIZE(curr_mb_attr->partHeights, partSizeIdx);
      LUD_DEBUG_ASSERT(partWidth4 < 3 && partHeight4 < 3);
      partSizeIdx++;
      nextIdx = NextSubPart4x4Idx[partWidth4][partHeight4][idx];
      LUD_DEBUG_ASSERT(nextIdx != (unsigned int)N_A);
      int partSizeY = partSizeConv[2][partWidth4][partHeight4];
      int partSizeC = N_A;
      if (cfidc)
        partSizeC = partSizeConv[cfidc-1][partWidth4][partHeight4];
      partWidth4 = partSizeId[partWidth4];
      partHeight4 = partSizeId[partHeight4];

      x = ((idx & 3) << 2);
      y = (idx & 0xC);

      mb_Y_samples = mb_Y_samples_ + x + y * strideY;
      if (cfidc)
      {
        mb_C_samples[0] = mb_C_samples_[0] + x/SubWidthC[cfidc] + y * strideC / SubHeightC[cfidc];
        mb_C_samples[1] = mb_C_samples_[1] + x/SubWidthC[cfidc] + y * strideC / SubHeightC[cfidc];
      }

      x += pos_x;
      y += pos_y;

      refIdxL[0] = curr_mb_attr->RefIdxL[0][idx];
      refIdxL[1] = curr_mb_attr->RefIdxL[1][idx];


      if (refIdxL[0] < 0 && refIdxL[1] < 0 )
      {
        // Bad stream, a B Mb must be at least one reference !
        //LUD_DEBUG_ASSERT(refIdxL[0] >= 0 || refIdxL[1] >= 0 );
        LUD_TRACE(TRACE_ERROR, "B MB partition without reference!");
        return;
      }

      i = refIdxL[0] < 0; // get the index of the reference list (0 or 1)

      if (i || refIdxL[1] < 0) // Only one reference picture. Work in-place
      {
        int mvxY, mvyY;
        int mvyC_offset;


        mvxY = curr_mb_attr->mv[i][idx][0];
        mvyY = curr_mb_attr->mv[i][idx][1];

        refrence_picture_selection_process(refIdxL[i], RefPicList[i], field_pic_flag,
            mb_field_decoding_flag, mb_row, &refPic, &field);

        // derive the offset of the chroma motion vector
        if ((cfidc != 1) || !mb_field_decoding_flag || (field_pic_flag && bottom_field_flag == field) || (!field_pic_flag && (mb_row%2) == field))
          mvyC_offset = 0;
        else
          mvyC_offset = field ? -2 : 2;


        srcY = refPic->ref_pic->Y+(field!=0)*PicWidthY;
        srcC[0] = refPic->ref_pic->C[0]+(field!=0)*PicWidthC;
        srcC[1] = refPic->ref_pic->C[1]+(field!=0)*PicWidthC;


        if (weighted_bipred_idc != 1) // weighted_bipred_idc == 0 or 2
        {
          // Just interpolate the sample from the ref pic, no weighted pred. The result is directly put into the dst picture
          luma_samples_interpolation_process(x, y, mvxY, mvyY, partWidth4, partHeight4,
              srcY, strideY, mb_Y_samples, strideY, PicWidthY, PicHeightY, clipY);
          if (cfidc)
            chroma_samples_interpolation_process(x, y, mvxY, mvyY+mvyC_offset, partWidth4, partHeight4,
                srcC, strideC, mb_C_samples, strideC, PicWidthC, PicHeightC, cfidc);
        }
        else // weighted_bipred_idc==1
        {
          int refIdxLWP[2];

          refIdxLWP[0] = refIdxL[0] >> (MbaffFrameFlag && mb_field_decoding_flag);
          refIdxLWP[1] = refIdxL[1] >> (MbaffFrameFlag && mb_field_decoding_flag);

          // Do the interpolation (put the result into the dst picture, the weighted pred will be done in-place)
          luma_samples_interpolation_process(x, y, mvxY, mvyY, partWidth4, partHeight4,
              srcY, strideY, mb_Y_samples, strideY, PicWidthY, PicHeightY, clipY);

          explicit_weigthed_sample_prediction_process_monopred(partSizeY,
              (const pixel_t*)mb_Y_samples, strideY, mb_Y_samples, strideY, clipY,
              wt->luma_log2_weight_denom,
              refIdxLWP[i], wt->luma_weight_l[i], wt->luma_offset_l[i]);

          if (cfidc)
          {
            chroma_samples_interpolation_process(x, y, mvxY, mvyY+mvyC_offset, partWidth4, partHeight4,
                srcC, strideC, mb_C_samples, strideC, PicWidthC, PicHeightC, cfidc);
            explicit_weigthed_sample_prediction_process_monopred(partSizeC,
                (const pixel_t*)mb_C_samples[0], strideC, mb_C_samples[0], strideC, clipC,
                wt->chroma_log2_weight_denom,
                refIdxLWP[i], wt->chroma_weight_l[0][i], wt->chroma_offset_l[0][i]);
            explicit_weigthed_sample_prediction_process_monopred(partSizeC,
                (const pixel_t*)mb_C_samples[1], strideC, mb_C_samples[1], strideC, clipC,
                wt->chroma_log2_weight_denom,
                refIdxLWP[i], wt->chroma_weight_l[1][i], wt->chroma_offset_l[1][i]);

          }
        }
      }
      else // two reference pictures, need to work with cache
      {
        pixel_t luma_cached[2][16*16];
        pixel_t chroma_cached[2][2][16*16];
        pixel_t* chroma_cache[2][2] = {{chroma_cached[0][0], chroma_cached[0][1]}, {chroma_cached[1][0], chroma_cached[1][1]}};

        // derive the motion vectors reference list interpolations
        for (i = 0; i < 2; ++i)
        {
          int mvxY, mvyY;
          int mvyC_offset;

          mvxY = curr_mb_attr->mv[i][idx][0];
          mvyY = curr_mb_attr->mv[i][idx][1];

          refrence_picture_selection_process(refIdxL[i], RefPicList[i], field_pic_flag,
              mb_field_decoding_flag, mb_row, &refPic, &field);

          // derive the offset of the chroma motion vector
          if ((cfidc != 1) || !mb_field_decoding_flag || (field_pic_flag && bottom_field_flag == field) || (!field_pic_flag && (mb_row%2) == field))
            mvyC_offset = 0;
          else
            mvyC_offset = field ? -2 : 2;


          srcY = refPic->ref_pic->Y+(field!=0)*PicWidthY;
          srcC[0] = refPic->ref_pic->C[0]+(field!=0)*PicWidthC;
          srcC[1] = refPic->ref_pic->C[1]+(field!=0)*PicWidthC;

          luma_samples_interpolation_process(x, y, mvxY, mvyY, partWidth4, partHeight4,
              srcY, strideY, luma_cached[i], 16, PicWidthY, PicHeightY, clipY);
          if (cfidc)
            chroma_samples_interpolation_process(x, y, mvxY, mvyY+mvyC_offset, partWidth4, partHeight4,
                srcC, strideC, chroma_cache[i], 16, PicWidthC, PicHeightC, cfidc);
        }

        if (!weighted_bipred_idc)
        {
          default_weighted_sample_prediction_process(partSizeY,
              (const pixel_t*)luma_cached[0], (const pixel_t*)luma_cached[1], 16, mb_Y_samples, strideY);
          if (cfidc)
          {
            default_weighted_sample_prediction_process(partSizeC,
                (const pixel_t*)chroma_cached[0][0], (const pixel_t*)chroma_cached[1][0], 16, mb_C_samples[0], strideC);
            default_weighted_sample_prediction_process(partSizeC,
                (const pixel_t*)chroma_cached[0][1], (const pixel_t*)chroma_cached[1][1], 16, mb_C_samples[1], strideC);
          }
        }
        else if (weighted_bipred_idc==1) // weighted_bipred_idc==1
        {
          // explicit weighted prediction
          int refIdxLWP[2];

          if (MbaffFrameFlag && mb_field_decoding_flag)
          {
            refIdxLWP[0] = refIdxL[0] >> 1;
            refIdxLWP[1] = refIdxL[1] >> 1;
          }
          else
          {
            refIdxLWP[0] = refIdxL[0];
            refIdxLWP[1] = refIdxL[1];
          }

          explicit_weigthed_sample_prediction_process_bipred(partSizeY,
              (const pixel_t*)luma_cached[0], (const pixel_t*)luma_cached[1], 16, mb_Y_samples, strideY, clipY,
              wt->luma_log2_weight_denom, refIdxLWP[0], refIdxLWP[1],
              wt->luma_weight_l[0], wt->luma_weight_l[1], wt->luma_offset_l[0], wt->luma_offset_l[1]);

          if (cfidc)
          {
            explicit_weigthed_sample_prediction_process_bipred(partSizeC,
                (const pixel_t*)chroma_cached[0][0], (const pixel_t*)chroma_cached[1][0], 16, mb_C_samples[0], strideC, clipC,
                wt->chroma_log2_weight_denom, refIdxLWP[0], refIdxLWP[1],
                wt->chroma_weight_l[0][0], wt->chroma_weight_l[0][1], wt->chroma_offset_l[0][0], wt->chroma_offset_l[0][1]);
            explicit_weigthed_sample_prediction_process_bipred(partSizeC,
                (const pixel_t*)chroma_cached[0][1], (const pixel_t*)chroma_cached[1][1], 16, mb_C_samples[1], strideC, clipC,
                wt->chroma_log2_weight_denom, refIdxLWP[0], refIdxLWP[1],
                wt->chroma_weight_l[1][0], wt->chroma_weight_l[1][1], wt->chroma_offset_l[1][0], wt->chroma_offset_l[1][1]);
          }
        }
        else // weighted_bipred_idc==2
        {
          // implicit weighted prediction
          int w0, w1;
          implicit_weigthed_sample_prediction_process_compute_wx(
              mb_field_decoding_flag, bottom_field_flag, field_pic_flag, mb_row,
              RefPicList, FieldOrderCnt, refIdxL[0], refIdxL[1], &w0, &w1);

          implicit_weigthed_sample_prediction_process_apply(partSizeY,
              (const pixel_t*)luma_cached[0], (const pixel_t*)luma_cached[1], 16, mb_Y_samples, strideY,
              clipY, w0, w1);

          if (cfidc)
          {
            implicit_weigthed_sample_prediction_process_apply(partSizeC,
                (const pixel_t*)chroma_cached[0][0], (const pixel_t*)chroma_cached[1][0], 16, mb_C_samples[0], strideC,
                clipC, w0, w1);
            implicit_weigthed_sample_prediction_process_apply(partSizeC,
                (const pixel_t*)chroma_cached[0][1], (const pixel_t*)chroma_cached[1][1], 16, mb_C_samples[1], strideC,
                clipC, w0, w1);
          }
        }
      }

      idx = nextIdx;
    }

  }
}

void inter_mb_inverse_transform(struct rv264macro* curr_mb_attr, mb_type_t mb_type, int16_t* mb_data,
    pixel_t* mb_Y_samples, pixel_t* mb_C_samples[2],
    unsigned int cfidc, int clipY, int clipC, unsigned int strideY, unsigned int strideC)
{
  int r, c;

  if (mb_type == P_Skip || mb_type == B_Skip)
  {
    // all transform coefficients are inferred as zeros
    // nothing to do
  }
  else
  {
    if (curr_mb_attr->transform_size_8x8_flag)
    { // luma 8x8 inv transform

      int16_t* data = mb_data;
      unsigned int total_coeffs;


      for (r = 0; r < 2; ++r)
      {
        pixel_t* block_samples = mb_Y_samples+r*8*strideY;

        for (c = 0; c < 2; ++c)
        {
          int k = c*2 + r*8;
          total_coeffs = curr_mb_attr->TotalCoeffLuma[k+0] + curr_mb_attr->TotalCoeffLuma[k+1]
                       + curr_mb_attr->TotalCoeffLuma[k+4] + curr_mb_attr->TotalCoeffLuma[k+5];
          // This is a little hacky. We actually write coeff values into TotalCoeffLuma so that it will be used for the filtering process.
          // In that way we do not need to re-add the 4 coeffs when calculating the bS values for this mb
          curr_mb_attr->TotalCoeffLuma[k+0] = curr_mb_attr->TotalCoeffLuma[k+1]
            = curr_mb_attr->TotalCoeffLuma[k+4] = curr_mb_attr->TotalCoeffLuma[k+5] = total_coeffs;

          inverse_transform(IDCT8x8, total_coeffs, curr_mb_attr->TransformBypassModeFlag, block_samples, data, strideY, clipY);

          block_samples+= 8;
          data+= 64;
        }
      }
    }

    else
    { // luma 4x4 inv transform
      int16_t* data = mb_data;
      unsigned int idx = 0;

      for (r = 0; r < 4; ++r)
      {
        pixel_t* block_samples = mb_Y_samples+r*4*strideY;

        for (c = 0; c < 4; ++c)
        {
          inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffLuma[idx], curr_mb_attr->TransformBypassModeFlag, block_samples, data, strideY, clipY);

          block_samples+= 4;
          data+= 16;
          idx++;
        }
      }
    }

    // chroma inverse transform
    if (cfidc == 0)
    { // No chroma component, nothing to do
    }
    else
    {
      unsigned int i, r, c;
      unsigned int iCbCr;
      int16_t temp;
      pixel_t* block_samples;
      int16_t* data;
      unsigned int num_blocks;

      mb_data+=256; // go to chroma data
      if (cfidc == 1)
      {
        num_blocks = 4;
        r = 1;
        c = 1;
      }
      else if (cfidc == 2)
      {
        num_blocks = 8;
        r = 1;
        c = 1;
      }
      else // cfidc == 3
      {
        LUD_DEBUG_ASSERT(cfidc==3);
        num_blocks = 16;
        r = 2;
        c = 3;
      }


      for (iCbCr = 0; iCbCr < 2; ++iCbCr)
      {
        data = mb_data;
        data += num_blocks-1;
        temp = data[0]; // store the 16th DC coeff
        // Transform the 16*15 AC coeffs. a DC coeff is taken from the previous list
        for (i=0; i<num_blocks-1; i++)
        {
          data[0] = mb_data[i];
          block_samples =  mb_C_samples[iCbCr] + ((i>>r)*strideC + (i&c))*4;
          inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffChroma[iCbCr][i]+(data[0]!=0), curr_mb_attr->TransformBypassModeFlag, block_samples, data, strideC, clipC);
          data+= 15;
        }
        data[0] = temp;
        block_samples =  mb_C_samples[iCbCr] + ((i>>r)*strideC + (i&c))*4;
        inverse_transform(IDCT4x4, curr_mb_attr->TotalCoeffChroma[iCbCr][i]+(data[0]!=0), curr_mb_attr->TransformBypassModeFlag, block_samples, data, strideC, clipC);

        mb_data += num_blocks*16;
      }
    }
  }
}

void decode_inter_mb(struct rv264macro* curr_mb_attr, mb_type_t mb_type, int16_t* mb_data, unsigned int mb_row,
    unsigned int mb_col, unsigned int mb_field_decoding_flag, unsigned int MbaffFrameFlag,
    RefListEntry* RefPicList[2], unsigned int field_pic_flag, unsigned int bottom_field_flag, int FieldOrderCnt[2],
    slice_type_t slice_type_modulo5, unsigned int weighted_pred_flag, unsigned int weighted_bipred_idc,
    pixel_t* mb_Y_samples, pixel_t* mb_C_samples[2], unsigned int PicWidthY, unsigned int PicWidthC,
    unsigned int strideY, unsigned int strideC, unsigned int PicHeightY, unsigned int PicHeightC, unsigned int clipY, unsigned int clipC,
    unsigned int cfidc, unsigned int mbc_width, unsigned int mbc_height, pred_weight_table_t* wt)
{

  decoding_process_for_inter_prediction_samples(curr_mb_attr, slice_type_modulo5,
      weighted_pred_flag, weighted_bipred_idc, mb_row, mb_col, FieldOrderCnt,
      RefPicList, field_pic_flag, bottom_field_flag, mb_field_decoding_flag,
      MbaffFrameFlag, mb_Y_samples, mb_C_samples, wt,
      PicWidthY, PicWidthC,
      strideY, strideC, PicHeightY, PicHeightC, clipY, clipC,
      cfidc, mbc_width, mbc_height);

/*  if (mb_col == 5 && mb_row == 0)
  {
    print_mb_mv_pred(curr_mb_attr, mb_type, mb_col, mb_row, mb_col);
    print_mb_samples(curr_mb_attr, mb_col, mb_row, mb_Y_samples, mb_C_samples[0], mb_C_samples[1], strideY, strideC,
        MbaffFrameFlag, curr_mb_attr->mb_field_decoding_flag, cfidc);
  }*/



  inter_mb_inverse_transform(curr_mb_attr, mb_type, mb_data,
      mb_Y_samples, mb_C_samples,
      cfidc, clipY, clipC, strideY, strideC);



}
