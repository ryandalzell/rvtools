/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/


#ifndef __DECODE_SLICE_H__
#define __DECODE_SLICE_H__

#include <stdint.h>

#include "decode.h"
#include "bitstream_types.h"

#if defined(ARCH_X86_64)
typedef uint64_t cabac_reg;
#else // ARCH_X86_64
typedef uint32_t cabac_reg;
#endif // ARCH_X86_64

typedef struct
{
  cabac_reg     codIRange;
  cabac_reg     codIOffset;
  int           buffbits;
  uint8_t*      stream;
  uint8_t       cabac_ctx_vars[460]; // for each ctxIdx: = pStateIdx*2+valMPS => valMPS = cabac_ctx_vars&1, pStateIdx=cabac_ctx_vars>>1;
  struct rv264macro* mbA; // used to derive mb_skip_flag and mb_type
  struct rv264macro* mbB; // they are put here so to be derived only once !
  uint8_t       cbpA; // used when MbaffFrameFlag==1 and curr and left mb are not the same kind (field/frame).
                      // see cabac_decode_coded_block_pattern().
} CABACContext;


RetCode decode_slice_data(struct slice_header* sh);



unsigned int NextMbAddress(uint8_t* MbToSliceGroupMap, unsigned int n, unsigned int PicSizeInMbs, unsigned int num_slice_groups_minus1);
part_type_t MbPartPredMode( mb_type_t mb_type, unsigned int mbPartIdx, unsigned int transform_size_8x8_flag, unsigned int can_be_I_mb);
unsigned int NumMbPart(mb_type_t mb_type);
part_type_t SubMbPredMode(sub_mb_type_t sub_mb_type );
unsigned int NumSubMbPart(sub_mb_type_t sub_mb_type);
unsigned int MbPartWidth(mb_type_t mb_type);
unsigned int MbPartHeight(mb_type_t mb_type);
unsigned int SubMbPartWidth(sub_mb_type_t sub_mb_type);
unsigned int SubMbPartHeight(sub_mb_type_t sub_mb_type);
unsigned int is_IntraMb(mb_type_t mb_type);
unsigned int is_InterMb(mb_type_t mb_type);
unsigned int is_NxNMb(mb_type_t mb_type);
unsigned int is_data_partitioning_slice(nal_unit_type_t nal_unit_type);
struct rv264macro* get_left_mbaff_mb(struct rv264macro* curr_mb, unsigned int PicWidthInMbs,
    unsigned int curr_is_field, unsigned int left_is_field, unsigned int curr_is_bot, unsigned int block_row);
unsigned int get_left_mbaff_4x4block(unsigned int curr_is_field, unsigned int left_is_field,
    unsigned int curr_is_bot, unsigned int block_row);
struct rv264macro* get_up_mbaff_mb(struct rv264macro* curr_mb, unsigned int PicWidthInMbs,
    unsigned int curr_is_field, unsigned int up_is_field, unsigned int curr_is_bot);
struct rv264macro* get_upleft_mbaff_mb(struct rv264macro* curr_mb, unsigned int PicWidthInMbs,
    unsigned int curr_is_field, unsigned int upleft_is_field, unsigned int curr_is_bot);
unsigned int get_upleft_mbaff_4x4block(unsigned int curr_is_field, unsigned int upleft_is_field,
    unsigned int curr_is_bot);
struct rv264macro* get_upright_mbaff_mb(struct rv264macro* curr_mb, unsigned int PicWidthInMbs,
    unsigned int curr_is_field, unsigned int upright_is_field, unsigned int curr_is_bot);

#define IS_INTRA(mb) (is_IntraMb((mb)->mb_type))
#define IS_INTER(mb) (is_InterMb((mb)->mb_type))
#define IS_NxN(mb) (is_NxNMb((mb)->mb_type))

#endif //__DECODE_SLICE_H__
