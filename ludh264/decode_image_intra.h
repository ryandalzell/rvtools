/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/

#ifndef DECODE_IMAGE_INTRA_H_
#define DECODE_IMAGE_INTRA_H_

#include "decode.h"

unsigned int Intra16x16PredMode(mb_type_t mb_type);
void copy4samples(pixel_t* dst, const pixel_t* src);
void duplicate1to4samples(pixel_t* dst, const pixel_t v);
void pred_intra_4x4_vertical(int dy, pixel_t* dst);
void pred_intra_4x4_horizontal(int dy, pixel_t* dst);
void pred_intra_4x4_dc(int dy, pixel_t* dst, int mode, pixel_t zero);
void pred_intra_4x4_diagonal_down_left(int dy, pixel_t* dst, unsigned int topright_is_avail);
void pred_intra_4x4_diagonal_down_right(int dy, pixel_t* dst);
void pred_intra_4x4_vertical_right(int dy, pixel_t* dst);
void pred_intra_4x4_horinzontal_down(int dy, pixel_t* dst);
void pred_intra_4x4_vertical_left(int dy, pixel_t* dst, unsigned int topright_is_avail);
void pred_intra_4x4_horizontal_up(int dy, pixel_t* dst);
void copy8samples(pixel_t* dst, const pixel_t* src);
void duplicate1to8samples(pixel_t* dst, const pixel_t v);
void pred_intra_8x8_vertical(int dy, pixel_t* dst, unsigned int topleft_is_avail, unsigned int topright_is_avail);
void pred_intra_8x8_horizontal(int dy, pixel_t* dst, unsigned int topleft_is_avail);
void pred_intra_8x8_dc(int dy, pixel_t* dst, int mode, pixel_t zero, unsigned int topleft_is_avail, unsigned int topright_is_avail);
void pred_intra_8x8_diagonal_down_left(int dy, pixel_t* dst, unsigned int topleft_is_avail, unsigned int topright_is_avail);
void pred_intra_8x8_diagonal_down_right(int dy, pixel_t* dst, unsigned int topleft_is_avail, unsigned int topright_is_avail);
void pred_intra_8x8_vertical_right(int dy, pixel_t* dst, unsigned int topleft_is_avail, unsigned int topright_is_avail);
void pred_intra_8x8_horinzontal_down(int dy, pixel_t* dst, unsigned int topleft_is_avail, unsigned int topright_is_avail);
void pred_intra_8x8_vertical_left(int dy, pixel_t* dst, unsigned int topleft_is_avail, unsigned int topright_is_avail);
void pred_intra_8x8_horizontal_up(int dy, pixel_t* dst, unsigned int topleft_is_avail);
void copy16samples(pixel_t* dst, const pixel_t* src);
void duplicate1to16samples(pixel_t* dst, const pixel_t v);
void pred_intra_16x16_vertical(int dy, pixel_t* dst);
void pred_intra_16x16_horizontal(int dy, pixel_t* dst);
void pred_intra_16x16_dc(int dy, pixel_t* dst, int mode, pixel_t zero);
void pred_intra_16x16_plane(int dy, pixel_t* dst, int max);
int add4vertsamples(const pixel_t* src, const int dy);
int add4horizsamples(const pixel_t* src);
void fill_4x4_block(pixel_t* dst, const int dy, const pixel_t v);
void fill_8x4_block(pixel_t* dst, const int dy, const pixel_t v);
void fill_8x8_block(pixel_t* dst, const int dy, const pixel_t v);
void fill_4x8_block(pixel_t* dst, const int dy, const pixel_t v);
void fill_4x16_block(pixel_t* dst, const int dy, const pixel_t v);
void fill_16x8_block(pixel_t* dst, const int dy, const pixel_t v);
void pred_intra_chroma_dc_full(int cfidc, int dy, pixel_t* dst, unsigned int left_is_avail, unsigned int top_is_avail, pixel_t zero);
void pred_intra_chroma_dc(unsigned int cfidc, int dy, pixel_t* dst_, unsigned int left_is_avail_top, unsigned int left_is_avail_bot,
    unsigned int top_is_avail, pixel_t zero);
void pred_intra_chroma_horizontal(int cfidc, int dy, pixel_t* dst);
void pred_intra_chroma_vertical(int cfidc, int dy, pixel_t* dst);
void pred_intra_chroma_plane(int cfidc, int dy, pixel_t* dst, int max);
void pred_intra4x4(int meanY, int Intra4x4PredMode, pixel_t* dst, int dy,
                                 unsigned int left_is_avail, unsigned int top_is_avail, unsigned int topright_is_avail);
void pred_intra8x8(int meanY, int Intra8x8PredMode, pixel_t* dst, int dy,
                                 unsigned int left_is_avail, unsigned int topleft_is_avail, unsigned int top_is_avail, unsigned int topright_is_avail);
void pred_intra16x16(int meanY, int clipY, int Intra16x16PredMode, pixel_t* dst, int dy,
                                   unsigned int left_is_avail, unsigned int top_is_avail);
void pred_intra_chroma(int meanC, int clipC, int cfidc, int intra_chroma_pred_mode, pixel_t* dst, int dy,
    unsigned int left_is_avail_top, unsigned int left_is_avail_bot, unsigned int top_is_avail);
void compute_left_edge_availability(struct rv264macro* curr_mb_attr, mb_type_t mb_type, unsigned int mb_field_decoding_flag,
    unsigned int curr_is_bot_mb,unsigned int PicWidthInMbs, unsigned int MbaffFrameFlag, unsigned int constrained_intra_pred_flag,
    unsigned int* left_is_avail_top, unsigned int* left_is_avail_bot);
unsigned int get_up_edge_availability(struct rv264macro* curr_mb_attr, mb_type_t mb_type, unsigned int mb_field_decoding_flag,
    unsigned int curr_is_bot_mb,unsigned int PicWidthInMbs, unsigned int MbaffFrameFlag);
unsigned int get_upleft_edge_availability(struct rv264macro* curr_mb_attr, mb_type_t mb_type, unsigned int mb_field_decoding_flag,
    unsigned int curr_is_bot_mb,unsigned int PicWidthInMbs, unsigned int MbaffFrameFlag);
unsigned int get_upright_edge_availability(struct rv264macro* curr_mb_attr, mb_type_t mb_type, unsigned int mb_field_decoding_flag,
    unsigned int curr_is_bot_mb,unsigned int PicWidthInMbs, unsigned int MbaffFrameFlag);
void decode_intra_mb(struct rv264macro* curr_mb_attr, mb_type_t mb_type, int16_t* mb_data, unsigned int PicWidthY,
    unsigned int PicWidthC, pixel_t* mb_Y_samples, pixel_t* mb_Cb_samples, pixel_t* mb_Cr_samples,
    unsigned int mbc_height, unsigned int mbc_width, unsigned int cfidc, int clipY, int clipC, int meanY, int meanC,
    unsigned int mb_field_decoding_flag, unsigned int curr_is_bot_mb, unsigned int PicWidthInMbs, unsigned int MbaffFrameFlag,
    unsigned int constrained_intra_pred_flag);

#endif
