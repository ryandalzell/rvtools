/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/

#include <string.h>

#include "common.h"
#include "bitstream_types.h"

/* pretty print syntax types */
const char *nal_unit_type_name[20] = {
  "NALU_TYPE_INVALID",
  "NALU_TYPE_SLICE",
  "NALU_TYPE_DPA",
  "NALU_TYPE_DPB",
  "NALU_TYPE_DPC",
  "NALU_TYPE_IDR",
  "NALU_TYPE_SEI",
  "NALU_TYPE_SPS",
  "NALU_TYPE_PPS",
  "NALU_TYPE_AUD",
  "NALU_TYPE_EOSEQ",
  "NALU_TYPE_EOS",
  "NALU_TYPE_FILL",
  "NALU_TYPE_SPS_EXT",
  "NALU_TYPE_RESERVED",
  "NALU_TYPE_AUX",
};

/* return next unsigned Exp-Golomb coded word, with debug trace */
unsigned int uexpbits_trace(struct bitbuf *bb, const char *name)
{
    char s[24] = "";
    int pos = tellbits(bb);
    int b, zerobits = -1;
    for (b=0; !b; zerobits++) {
        b = getbit(bb);
        strncat(s, b? "1" : "0", sizeof(s)-1);
    }
    unsigned int codeword = getbits(bb, zerobits);
    if (zerobits)
        for (unsigned int i=(1<<(zerobits-1)); i>0; i>>=1)
            strncat(s, (codeword & i) == i? "1" : "0", sizeof(s)-1);
    unsigned int code = (1<<zerobits) - 1 + codeword;
    fprintf(stderr, "@%-5d %-40s %23s (%3d) \n", pos, name, s, code);
    return code;
}

/* return next signed Exp-Golomb coded word, with debug trace */
signed int sexpbits_trace(struct bitbuf *bb, const char *name)
{
    char s[24] = "";
    int pos = tellbits(bb);
    int b, zerobits = -1;
    for (b=0; !b; zerobits++) {
        b = getbit(bb);
        strncat(s, b? "1" : "0", sizeof(s)-1);
    }
    unsigned int codeword = getbits(bb, zerobits);
    if (zerobits)
        for (unsigned int i=(1<<(zerobits-1)); i>0; i>>=1)
            strncat(s, (codeword & i) == i? "1" : "0", sizeof(s)-1);
    int code = (1<<zerobits) - 1 + codeword;
    if (code&1)
        code = (code >> 1) + 1;
    else
        code = -(code >> 1);
    fprintf(stderr, "@%-5d %-40s %23s (%3d) \n", pos, name, s, code);
    return code;
}

uint8_t bs_read_vlc_trace(struct bitbuf *bs, VLCReader* vlc, const char *name)
{
  unsigned int buf;
  unsigned int symbol;
  int length;

  char s[24] = "";
  int pos = tellbits(bs);
  buf = showbits(bs, vlc->bits);
  symbol  = vlc->table.code_symbol[buf];
  length  = vlc->table.code_length[buf];
  if (length < 0)
  {
    uint8_t*  st;
    int8_t*   lt;
    LUD_DEBUG_ASSERT(symbol < vlc->nb_of_subtables); // otherwise we are going to read garbage (overflow)
    st = vlc->subtable[symbol].code_symbol;
    lt = vlc->subtable[symbol].code_length;
    for (unsigned i=(1<<(vlc->bits-1)); i>0; i>>=1)
      strncat(s, (buf & i) == i? "1" : "0", sizeof(s)-1);
    flushbits(bs, vlc->bits);
    buf = showbits(bs, -length);
    symbol  = st[buf];
    length  = lt[buf];
    for (unsigned i=(1<<(length-1)); i>0; i>>=1)
      strncat(s, (buf & i) == i? "1" : "0", sizeof(s)-1);
  } else {
    for (unsigned i=(1<<(length-1)); i>0; i>>=1)
      strncat(s, (buf & i) == i? "1" : "0", sizeof(s)-1);
  }
  LUD_DEBUG_ASSERT(length>0); // we only allow one sub-table jump ! (i.e. a vlc code of maximal length 2*vlc->bits)
  flushbits(bs, length);

  char t[41];
  snprintf(t, sizeof(t), "%s #c=%d #t1=%d", name, symbol>>2, symbol&3);
  fprintf(stderr, "@%-5d %-40s %23s (%3d) \n", pos, t, s, buf);

  return symbol;
}
