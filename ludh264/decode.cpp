/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/

#include "syntax.h"
#include "decode.h"
#include <string.h>
#include "defaulttables.h"
#include "bitstream.h"
#include "decode_slice_data.h"
#include "system.h"
#include "cabac.h"
#include "../rvh264.h"

GlobalDecoderData gdd;




static unsigned int is_new_picture(struct slice_header* prev_sh,
                                          struct slice_header* curr_sh, struct rv264seq_parameter_set* curr_sps)
{
  struct rv264seq_parameter_set* prev_sps = prev_sh->sps;

  if (!prev_sps)
    return 1;

  return
      prev_sh->original_frame_num != curr_sh->original_frame_num ||
      prev_sh->pic_parameter_set_id != curr_sh->pic_parameter_set_id ||
      prev_sh->field_pic_flag != curr_sh->field_pic_flag  ||
      prev_sh->bottom_field_flag != curr_sh->bottom_field_flag ||
      ((prev_sh->nal_ref_idc == 0 || curr_sh->nal_ref_idc == 0) && prev_sh->nal_ref_idc != curr_sh->nal_ref_idc) ||
      (prev_sps->pic_order_cnt_type==0 && curr_sps->pic_order_cnt_type==0 &&
          (prev_sh->pic_order_cnt_lsb != curr_sh->pic_order_cnt_lsb || prev_sh->delta_pic_order_cnt_bottom != curr_sh->delta_pic_order_cnt_bottom )) ||
      (prev_sps->pic_order_cnt_type==1 && curr_sps->pic_order_cnt_type==1 &&
          (prev_sh->delta_pic_order_cnt[ 0 ] != curr_sh->delta_pic_order_cnt[ 0 ] || prev_sh->delta_pic_order_cnt[ 1 ] != curr_sh->delta_pic_order_cnt[ 1 ])) ||
      ((prev_sh->nal_unit_type == 5 || curr_sh->nal_unit_type == 5) && prev_sh->nal_unit_type != curr_sh->nal_unit_type ) ||
      (prev_sh->nal_unit_type == 5 && curr_sh->nal_unit_type == 5 && prev_sh->idr_pic_id != curr_sh->idr_pic_id )
      ;
}

// This function is only used when gaps_in_frame_num_value_allowed_flag==1 and there is a gap in frame_num...
static void decode_picture_order_count_for_gaps_in_frame_num(struct rv264seq_parameter_set* sps, int frame_num, int pic_order_cnt_type,
    int32_t* PicOrderCnt, int32_t* TopFieldOrderCnt, int32_t* BottomFieldOrderCnt, uint16_t* prevFrameNum, int32_t* prevFrameNumOffset)
{
  if (pic_order_cnt_type == 0)
  {
    // Nothing to do, just set poc to its default value:0
    *PicOrderCnt = 0;
    *TopFieldOrderCnt = 0;
    *BottomFieldOrderCnt = 0;
  }

  else if (pic_order_cnt_type == 1)
  {
    int i, absFrameNum, expectedDeltaPerPicOrderCntCycle, expectedPicOrderCnt, FrameNumOffset;

    if( *prevFrameNum > frame_num )
      FrameNumOffset = *prevFrameNumOffset + sps->MaxFrameNum;
    else
      FrameNumOffset = *prevFrameNumOffset;

    if (sps->num_ref_frames_in_pic_order_cnt_cycle != 0)
      absFrameNum = FrameNumOffset + frame_num;
    else
      absFrameNum = 0;

    expectedDeltaPerPicOrderCntCycle = 0;
    for (i = 0; i < sps->num_ref_frames_in_pic_order_cnt_cycle; i++)
      expectedDeltaPerPicOrderCntCycle += sps->offset_for_ref_frame[ i ];

    if (absFrameNum > 0)
    {
      int picOrderCntCycleCnt = (absFrameNum - 1) / sps->num_ref_frames_in_pic_order_cnt_cycle;
      int frameNumInPicOrderCntCycle = (absFrameNum - 1) % sps->num_ref_frames_in_pic_order_cnt_cycle;
      expectedPicOrderCnt = picOrderCntCycleCnt * expectedDeltaPerPicOrderCntCycle;
      for (i = 0; i <= frameNumInPicOrderCntCycle; i++)
        expectedPicOrderCnt = expectedPicOrderCnt + sps->offset_for_ref_frame[ i ];
    }
    else
      expectedPicOrderCnt = 0;

    *TopFieldOrderCnt = expectedPicOrderCnt;
    *BottomFieldOrderCnt = *TopFieldOrderCnt + sps->offset_for_top_to_bottom_field;
    // Calculate the PicOrderCnt according to Top and BottomFieldOrderCnt
    *PicOrderCnt = im_min( *TopFieldOrderCnt, *BottomFieldOrderCnt );

    // set prev values for next POC process
    *prevFrameNumOffset = FrameNumOffset;
    *prevFrameNum = frame_num;
  }

  else // (sps-> pic_order_cnt_type == 2
  {
    LUD_DEBUG_ASSERT(sps->pic_order_cnt_type == 2);
    int tempPicOrderCnt, FrameNumOffset;

    if( *prevFrameNum > frame_num )
      FrameNumOffset = *prevFrameNumOffset + sps->MaxFrameNum;
    else
      FrameNumOffset = *prevFrameNumOffset;

    tempPicOrderCnt = 2 * (FrameNumOffset + frame_num);

    *PicOrderCnt = tempPicOrderCnt;
    *TopFieldOrderCnt = tempPicOrderCnt;
    *BottomFieldOrderCnt = tempPicOrderCnt;

    // set prev values for next POC process
    *prevFrameNumOffset = FrameNumOffset;
    *prevFrameNum = frame_num;
  }

}

void decode_picture_order_count(PictureDecoderData* pdd, struct rv264picture* pic, struct slice_header* sh, struct rv264seq_parameter_set* sps)
{
  if (sps->pic_order_cnt_type == 0)
  {
    int MaxPicOrderCntLsb = 1<<(sps->log2_max_pic_order_cnt_lsb_minus4+4);

    if (sh->nal_unit_type == NAL_CODED_SLICE_IDR)
      pdd->prevPicOrderCntMsb = pdd->prevPicOrderCntLsb = 0;

    if( ( sh->pic_order_cnt_lsb < pdd->prevPicOrderCntLsb ) &&
          ( ( pdd->prevPicOrderCntLsb - sh->pic_order_cnt_lsb ) >= ( MaxPicOrderCntLsb / 2 ) ) )
      pdd->PicOrderCntMsb = pdd->prevPicOrderCntMsb + MaxPicOrderCntLsb;
    else if( ( sh->pic_order_cnt_lsb > pdd->prevPicOrderCntLsb ) &&
               ( ( sh->pic_order_cnt_lsb - pdd->prevPicOrderCntLsb ) > ( MaxPicOrderCntLsb / 2 ) ) )
      pdd->PicOrderCntMsb = pdd->prevPicOrderCntMsb - MaxPicOrderCntLsb;
    else
      pdd->PicOrderCntMsb = pdd->prevPicOrderCntMsb;

    if( !sh->field_pic_flag || !sh->bottom_field_flag )
      pic->FieldOrderCnt[0] = pdd->PicOrderCntMsb + sh->pic_order_cnt_lsb;
    if( !sh->field_pic_flag )
      pic->FieldOrderCnt[1] = pic->FieldOrderCnt[0] + sh->delta_pic_order_cnt_bottom;
    else if( sh->bottom_field_flag )
      pic->FieldOrderCnt[1] = pdd->PicOrderCntMsb + sh->pic_order_cnt_lsb;

  }

  else if (sps->pic_order_cnt_type == 1)
  {
    int i, absFrameNum, picOrderCntCycleCnt = 0, frameNumInPicOrderCntCycle = 0, expectedDeltaPerPicOrderCntCycle, expectedPicOrderCnt; // initialization not necessary, but prevent a warning...


    if (sh->nal_unit_type == 5 /*NAL_CODED_SLICE_IDR*/)
      pdd->FrameNumOffset = 0;
    else if( pdd->prevFrameNum > sh->frame_num )
      pdd->FrameNumOffset = pdd->prevFrameNumOffset + sps->MaxFrameNum;
    else
      pdd->FrameNumOffset = pdd->prevFrameNumOffset;

    if (sps->num_ref_frames_in_pic_order_cnt_cycle != 0)
      absFrameNum = pdd->FrameNumOffset + sh->frame_num;
    else
      absFrameNum = 0;
    if (sh->nal_ref_idc == 0 && absFrameNum > 0)
      absFrameNum = absFrameNum - 1;

    if (absFrameNum > 0)
    {
      picOrderCntCycleCnt = (absFrameNum - 1) / sps->num_ref_frames_in_pic_order_cnt_cycle;
      frameNumInPicOrderCntCycle = (absFrameNum - 1) % sps->num_ref_frames_in_pic_order_cnt_cycle;
    }

    expectedDeltaPerPicOrderCntCycle = 0;
    for (i = 0; i < sps->num_ref_frames_in_pic_order_cnt_cycle; i++)
      expectedDeltaPerPicOrderCntCycle += sps->offset_for_ref_frame[ i ];

    if (absFrameNum > 0)
    {
      expectedPicOrderCnt = picOrderCntCycleCnt * expectedDeltaPerPicOrderCntCycle;
      for (i = 0; i <= frameNumInPicOrderCntCycle; i++)
        expectedPicOrderCnt = expectedPicOrderCnt + sps->offset_for_ref_frame[ i ];
    }
    else
      expectedPicOrderCnt = 0;
    if (sh->nal_ref_idc == 0)
      expectedPicOrderCnt = expectedPicOrderCnt + sps->offset_for_non_ref_pic;

    if (!sh->field_pic_flag)
    {
      pic->FieldOrderCnt[0] = expectedPicOrderCnt + sh->delta_pic_order_cnt[ 0 ];
      pic->FieldOrderCnt[1] = pic->FieldOrderCnt[0] + sps->offset_for_top_to_bottom_field + sh->delta_pic_order_cnt[ 1 ];
    }
    else if (!sh->bottom_field_flag)
      pic->FieldOrderCnt[0] = expectedPicOrderCnt + sh->delta_pic_order_cnt[ 0 ];
    else
      pic->FieldOrderCnt[1] = expectedPicOrderCnt + sps->offset_for_top_to_bottom_field + sh->delta_pic_order_cnt[ 0 ];
  }

  else // (sps-> pic_order_cnt_type == 2
  {
    LUD_DEBUG_ASSERT(sps->pic_order_cnt_type == 2);
    int tempPicOrderCnt;

    if (sh->nal_unit_type == 5 /*NAL_CODED_SLICE_IDR*/)
      pdd->FrameNumOffset = 0;
    else if( pdd->prevFrameNum > sh->frame_num )
      pdd->FrameNumOffset = pdd->prevFrameNumOffset + sps->MaxFrameNum;
    else
      pdd->FrameNumOffset = pdd->prevFrameNumOffset;

    if (sh->nal_unit_type == 5)
      tempPicOrderCnt = 0;
    else if (sh->nal_ref_idc == 0)
      tempPicOrderCnt = 2 * (pdd->FrameNumOffset + sh->frame_num) - 1;
    else
      tempPicOrderCnt = 2 * (pdd->FrameNumOffset + sh->frame_num);

    if (!sh->field_pic_flag)
    {
      pic->FieldOrderCnt[0] = tempPicOrderCnt;
      pic->FieldOrderCnt[1] = tempPicOrderCnt;
    }
    else if (sh->bottom_field_flag)
      pic->FieldOrderCnt[1] = tempPicOrderCnt;
    else
      pic->FieldOrderCnt[0] = tempPicOrderCnt;
  }

  // Calculate the PicOrderCnt according to Top and BottomFieldOrderCnt
  if (!sh->field_pic_flag || pdd->is_second_field_of_a_pair)
    pic->PicOrderCnt = im_min( pic->FieldOrderCnt[0], pic->FieldOrderCnt[1] );
  else if (!sh->bottom_field_flag)
    pic->PicOrderCnt = pic->FieldOrderCnt[0];
  else
    pic->PicOrderCnt = pic->FieldOrderCnt[1];

  // Copy the calculated PicOrderCnt to the slice header
  sh->PicOrderCnt = pic->PicOrderCnt;

}

// This function is called when a new pic is detected when the current pic is not an idr
static void store_prev_values_for_poc(PictureDecoderData* pdd, struct rv264picture* pic, struct slice_header* sh)
{
  struct rv264seq_parameter_set* sps = sh->sps;

  if (sps->pic_order_cnt_type == 0)
  {
    // set prev values for next POC process
    if (sh->nal_ref_idc)
    {
      if (sh->has_mmco5)
      {
        if (!sh->bottom_field_flag)
        {
          pdd->prevPicOrderCntMsb = 0;
          pdd->prevPicOrderCntLsb = pic->FieldOrderCnt[0];
        }
        else
          pdd->prevPicOrderCntMsb = pdd->prevPicOrderCntLsb = 0;
      }
      else // no mmco==5
      {
        pdd->prevPicOrderCntLsb = sh->pic_order_cnt_lsb;
        pdd->prevPicOrderCntMsb = pdd->PicOrderCntMsb;
      }
    }
  }

  else if (sps->pic_order_cnt_type == 1)
  {
    // set prev values for next POC process
    if (sh->has_mmco5)
    {
      pdd->prevFrameNumOffset = 0;
      pdd->prevFrameNum = 0; // set to 0 because a pic hax mmco==5, frame_num is set to 0 (after the ref pic marking process)
    }
    else
    {
      pdd->prevFrameNumOffset = pdd->FrameNumOffset;
      pdd->prevFrameNum = sh->frame_num;
    }
  }

  else // (sps-> pic_order_cnt_type == 2
  {
    LUD_DEBUG_ASSERT(sps->pic_order_cnt_type == 2);

    // set prev values for next POC process
    if (sh->has_mmco5)
    {
      pdd->prevFrameNumOffset = 0;
      pdd->prevFrameNum = 0; // set to 0 because a pic hax mmco==5, frame_num is set to 0 (after the ref pic marking process)
    }
    else
    {
      pdd->prevFrameNumOffset = pdd->FrameNumOffset;
      pdd->prevFrameNum = sh->frame_num;
    }
  }
}

// This function does not allocate it's working space (uint8_t* MbToSliceGroupMap). caller must do it !
static void derive_MbToSliceGroupMap(struct rv264seq_parameter_set* sps,  struct rv264pic_parameter_set* pps, struct slice_header* sh, uint8_t* MbToSliceGroupMap)
{
  uint8_t* mapUnitToSliceGroupMap = MbToSliceGroupMap;
  int i;

  switch(pps->slice_group_map_type)
  {
    case 0:
    {
      i = 0;
      int iGroup, j;
      do
      {
        LUD_DEBUG_ASSERT(pps->run_length_minus1);
        for( iGroup = 0; iGroup <= pps->num_slice_groups_minus1 && i < sps->PicSizeInMapUnits; i += pps->run_length_minus1[ iGroup++ ] + 1 )
          for( j = 0; j <= pps->run_length_minus1[ iGroup ] && i + j < sps->PicSizeInMapUnits; j++ )
            mapUnitToSliceGroupMap[ i + j ] = iGroup;
      } while( i < sps->PicSizeInMapUnits );
      break;
    }
    case 1:
    {
      for( i = 0; i < sps->PicSizeInMapUnits; i++ )
        mapUnitToSliceGroupMap[ i ] = ( ( i % sps->PicWidthInMbs ) + ( ( ( i / sps->PicWidthInMbs ) * ( pps->num_slice_groups_minus1 + 1 ) ) / 2 ) )
            % ( pps->num_slice_groups_minus1 + 1 );
      break;
    }
    case 2:
    {
      int iGroup;
      for( i = 0; i < sps->PicSizeInMapUnits; i++ )
        mapUnitToSliceGroupMap[ i ] = pps->num_slice_groups_minus1;
      for( iGroup = pps->num_slice_groups_minus1 - 1; iGroup >= 0; iGroup-- )
      {
        int x, y;
        int yTopLeft = pps->top_left[ iGroup ] / sps->PicWidthInMbs;
        int xTopLeft = pps->top_left[ iGroup ] % sps->PicWidthInMbs;
        int yBottomRight = pps->bottom_right[ iGroup ] / sps->PicWidthInMbs;
        int xBottomRight = pps->bottom_right[ iGroup ] % sps->PicWidthInMbs;
        for( y = yTopLeft; y <= yBottomRight; y++ )
          for( x = xTopLeft; x <= xBottomRight; x++ )
            mapUnitToSliceGroupMap[ y * sps->PicWidthInMbs + x ] = iGroup;
      }
      break;
    }
    case 3:
    {
      int x, y, xDir, yDir, k;
      int rightBound, bottomBound, leftBound, topBound;
      int mapUnitVacant; // need not to be inited. It is assigned into the 2nd following for loop...
      int MapUnitsInSliceGroup0 = im_min( sh->slice_group_change_cycle * pps->SliceGroupChangeRate, sps->PicSizeInMapUnits );
      for( i = 0; i < sps->PicSizeInMapUnits; i++ )
        mapUnitToSliceGroupMap[ i ] = 1;
      x = ( sps->PicWidthInMbs - pps->slice_group_change_direction_flag ) / 2;
      y = ( sps->PicHeightInMapUnits - pps->slice_group_change_direction_flag ) / 2;
      rightBound = leftBound = x;
      bottomBound = topBound = y;
      yDir = pps->slice_group_change_direction_flag;
      xDir = pps->slice_group_change_direction_flag - 1;
      for( k = 0; k < MapUnitsInSliceGroup0; k += mapUnitVacant )
      {
        mapUnitVacant = ( mapUnitToSliceGroupMap[ y * sps->PicWidthInMbs + x ] == 1 );
        if( mapUnitVacant )
          mapUnitToSliceGroupMap[ y * sps->PicWidthInMbs + x ] = 0;
        if( xDir == -1 && x == leftBound )
        {
          leftBound = im_max( leftBound - 1, 0 );
          x = leftBound;
          xDir = 0;
          yDir = 2 * pps->slice_group_change_direction_flag - 1;
        }
        else if( xDir == 1 && x == rightBound )
        {
          rightBound = im_min( rightBound + 1, sps->PicWidthInMbs - 1 );
          x = rightBound;
          xDir = 0;
          yDir = 1 - 2 * pps->slice_group_change_direction_flag;
        }
        else if( yDir == -1 && y == topBound )
        {
          topBound = im_max( topBound - 1, 0 );
          y = topBound;
          xDir = 1 - 2 * pps->slice_group_change_direction_flag;
          yDir =  0;
        }
        else if( yDir == 1 && y == bottomBound )
        {
          bottomBound = im_min( bottomBound + 1, sps->PicHeightInMapUnits - 1 );
          y = bottomBound;
          xDir = 2 * pps->slice_group_change_direction_flag - 1;
          yDir = 0;
        }
        else
        {
          x = x + xDir;
          y = y + yDir;
        }
      }

      break;
    }
    case 4:
    {
      int MapUnitsInSliceGroup0 = im_min( sh->slice_group_change_cycle * pps->SliceGroupChangeRate, sps->PicSizeInMapUnits );
      int sizeOfUpperLeftGroup = ( pps->slice_group_change_direction_flag ? ( sps->PicSizeInMapUnits - MapUnitsInSliceGroup0 ) : MapUnitsInSliceGroup0 );
      for( i = 0; i < sizeOfUpperLeftGroup; i++ )
        mapUnitToSliceGroupMap[ i ] = pps->slice_group_change_direction_flag;
      for(; i < sps->PicSizeInMapUnits; i++ )
        mapUnitToSliceGroupMap[ i ] = 1 - pps->slice_group_change_direction_flag;
      break;
    }
    case 5:
    {
      int j;
      int k = 0;
      int MapUnitsInSliceGroup0 = im_min( sh->slice_group_change_cycle * pps->SliceGroupChangeRate, sps->PicSizeInMapUnits );
      int sizeOfUpperLeftGroup = ( pps->slice_group_change_direction_flag ? ( sps->PicSizeInMapUnits - MapUnitsInSliceGroup0 ) : MapUnitsInSliceGroup0 );
      for( j = 0; j < sps->PicWidthInMbs; j++ )
        for( i = 0; i < sps->PicHeightInMapUnits; i++ )
          if( k++ < sizeOfUpperLeftGroup )
            mapUnitToSliceGroupMap[ i * sps->PicWidthInMbs + j ] = pps->slice_group_change_direction_flag;
      else
        mapUnitToSliceGroupMap[ i * sps->PicWidthInMbs + j ] = 1 - pps->slice_group_change_direction_flag;
      break;
    }
    case 6:
    {
      mapUnitToSliceGroupMap = pps->slice_group_id;
      break;
    }
    default:
    {
      LUD_DEBUG_ASSERT(0); // Should never happen !! Is slice_group_map_type corrupted ?
      break;
    }
  }

  // Reassign MbToSliceGroupMap
  if ( sps->frame_mbs_only_flag || sh->field_pic_flag)
  {
    if (pps->slice_group_map_type == 6) // in other case (pps->slice_group_map_type != 6) we already have: MbToSliceGroupMap equals to mapUnitToSliceGroupMap (pointer)
      for (i=0; i < sh->PicSizeInMbs; i++)
        MbToSliceGroupMap[i] = mapUnitToSliceGroupMap[i];
  }
  else if (sps->mb_adaptive_frame_field_flag  &&  (!sh->field_pic_flag))
  {
    for (i=sh->PicSizeInMbs-1; i>=0; i--)
      MbToSliceGroupMap[i] = mapUnitToSliceGroupMap[i/2];
  }
  else
  {
    for (i=sh->PicSizeInMbs-1; i>=0; i--)
      MbToSliceGroupMap[i] = mapUnitToSliceGroupMap[(i/(2*sps->PicWidthInMbs))*sps->PicWidthInMbs+(i%sps->PicWidthInMbs)];
  }
}

static void derive_PrevRefFrameNum(PictureDecoderData* pdd, struct slice_header* sh)
{
  if (sh->nal_unit_type == NAL_CODED_SLICE_IDR)
    pdd->PrevRefFrameNum = 0;
  else if (pdd->prev_sh->nal_ref_idc)
  {
    pdd->PrevRefFrameNum = pdd->prev_sh->frame_num;
  }
}


static int ref_pic_sliding_window_process(PictureDecoderData* pdd, struct rv264seq_parameter_set* sps, struct slice_header* sh);
static void decoding_process_for_gaps_in_frame_num(PictureDecoderData* pdd, struct slice_header* sh, struct rv264seq_parameter_set* sps)
{
  int UnusedShortTermFrameNum;
  int MaxFrameNum = sps->MaxFrameNum;
  int frame_num = sh->frame_num;
  int pic_order_cnt_type = sps->pic_order_cnt_type;
  int32_t PicOrderCnt;
  int32_t TopFieldOrderCnt;
  int32_t BottomFieldOrderCnt;
  uint16_t prevFrameNum = pdd->prevFrameNum;
  int32_t prevFrameNumOffset = pdd->prevFrameNumOffset;

  if (!sps->gaps_in_frame_num_value_allowed_flag) // Unintentional loss of frames...
    return;


  UnusedShortTermFrameNum = ( pdd->PrevRefFrameNum + 1 ) & (MaxFrameNum-1); // modulo MaxFrameNum which is a power of 2
  while( UnusedShortTermFrameNum != frame_num )
  {
    // call the modified poc decoding process in order to calculate poc values
    // when pic_order_cnt_type == 0 poc need not to be calculated, set it to 0 by default
    // the decode_picture_order_count_for_gaps_in_frame_num does not modify/use pdd context
    decode_picture_order_count_for_gaps_in_frame_num(sps, UnusedShortTermFrameNum, pic_order_cnt_type,
        &PicOrderCnt, &TopFieldOrderCnt, &BottomFieldOrderCnt, &prevFrameNum, &prevFrameNumOffset);

    ref_pic_sliding_window_process(pdd, sps, sh); // will remove a short term frame if necessary

    // shift all elements of the list, and add the pic at idx 0 (newest pic)
    memmove(&pdd->ref_list_st[1], &pdd->ref_list_st[0], sizeof(pdd->ref_list_st[0]) * pdd->ref_list_nb_of_st);
    pdd->ref_list_st[0] = allocate_and_set_non_existing_ref_pic(PicOrderCnt, TopFieldOrderCnt, BottomFieldOrderCnt, UnusedShortTermFrameNum, 7, sh->bottom_field_flag);
    pdd->ref_list_nb_of_st++;

    UnusedShortTermFrameNum = ( UnusedShortTermFrameNum + 1 ) & (MaxFrameNum-1);
  }

  //pdd->prev_ref_pic_type = SHORT_TERM_REF;
  //pdd->prev_ref_pic = pdd->ref_list_st[0];

  pdd->PrevRefFrameNum = UnusedShortTermFrameNum;
}


/*
Prob with reference picture when field_pic_flag == 1 => the second field of the pair can point to the first one (i.e. the same picture !)
=> increase ref_count => the picture is never released
=> fix: 1) either to not increment ref_count when a pic point to itself => but prb: when freeing the pic => double free occurs
        2) find another solution !
*/

// Remove non reference fields and alternate top/bottom fields
static int alternate_field_parity(RefListEntry** dst, int dst_size, struct rv264picture** src, int src_nb_of_pics, int parity, ref_pic_type_t ref_pic_type)
{
  int same_parity_idx = 0;
  int opp_parity_idx = 0;
  int i;
  int same_parity = 1;
  int opp_parity = 1 - parity;
  int parity_mask = parity+1;           // +1 so it match top/bottom bits in ref_struct field
  int opp_parity_mask = opp_parity+1;


  for(i = 0; i < dst_size; )
  {
    if (same_parity && same_parity_idx < src_nb_of_pics) // add a same parity field
    {
      if ((ref_pic_type==SHORT_TERM_REF || src[same_parity_idx]) && (src[same_parity_idx]->ref_struct & parity_mask))
        // why (is_short_term || src[same_parity_idx]) instead of only (src[same_parity_idx]) ?
        // This is a cheap optimization since the function is inlined, and is_short_term is hardcoded by the caller,
        //        and short term ref list does not have "holes" => one test that should be removed at compilation time !
      {
        use_picture_field(src[same_parity_idx], parity);
        dst[i]->ref_pic = src[same_parity_idx];
        dst[i]->ref_pic_type = ref_pic_type;
        dst[i]->parity = parity;
        i++;
        same_parity = !same_parity;
      }
      same_parity_idx++;
    }
    else if (opp_parity_idx < src_nb_of_pics) // else, add a opposed parity field. Will also add opposed parity fields when their is only this kind of field remaining
    {
      if ((ref_pic_type==SHORT_TERM_REF || src[opp_parity_idx]) && (src[opp_parity_idx]->ref_struct & opp_parity_mask))
      {
        use_picture_field(src[opp_parity_idx], opp_parity);
        dst[i]->ref_pic = src[opp_parity_idx];
        dst[i]->ref_pic_type = ref_pic_type;
        dst[i]->parity = opp_parity;
        i++;
        same_parity = !same_parity;
      }
      opp_parity_idx++;
    }
    else if (same_parity_idx < src_nb_of_pics) // When it only remains same parity fields
    {
      if ((ref_pic_type==SHORT_TERM_REF || src[same_parity_idx]) && (src[same_parity_idx]->ref_struct & parity_mask))
      {
        use_picture_field(src[same_parity_idx], parity);
        dst[i]->ref_pic = src[same_parity_idx];
        dst[i]->ref_pic_type = ref_pic_type;
        dst[i]->parity = parity;
        i++;
        same_parity = !same_parity;
      }
      same_parity_idx++;
    }

    else // no more fields are available
      break;
  }

  return i;
}


static void init_ref_pic_list(PictureDecoderData* pdd, struct rv264picture* pic, struct slice_header* sh, struct rv264seq_parameter_set* sps, RefListEntry* RefPicList_copy[2][NB_OF_REF_PICS])
{
  int i;
  int nb_of_st_ref_pics = pdd->ref_list_nb_of_st;
  int nb_of_lt_ref_pics = pdd->ref_list_nb_of_lt;
  struct rv264picture** ref_list_st = pdd->ref_list_st;
  struct rv264picture** ref_list_lt = pdd->ref_list_lt;
  int pic_order_cnt_type = sps->pic_order_cnt_type;


  if (sh->slice_type_modulo5 == SLICE_B)
  {
    RefListEntry** RefPicList[2];
    RefPicList[0] = RefPicList_copy[0];
    RefPicList[1] = RefPicList_copy[1];
    struct rv264picture* sorted_ref_list[NB_OF_REF_PICS];
    int currPicOrderCnt = pic->PicOrderCnt;
    int pivot_idx, idx; //of_smallest_poc_bigger_than_curr_poc
    int num_ref_idx_active0 = sh->num_ref_idx_active[0];
    int num_ref_idx_active1 = sh->num_ref_idx_active[1];
    int size0, size1;
    int max_num_ref_pic = (nb_of_st_ref_pics+nb_of_lt_ref_pics) << (sh->field_pic_flag);
    int nb_of_sorted_pics = 0;

    LUD_DEBUG_ASSERT(num_ref_idx_active0 <= NB_OF_REF_PICS && num_ref_idx_active1 <= NB_OF_REF_PICS);
    if (max_num_ref_pic > NB_OF_REF_PICS)
      max_num_ref_pic = NB_OF_REF_PICS;

    // Set up table pointers
    for( idx=0; idx < NB_OF_REF_PICS; idx++ )
    {
      RefPicList[0][idx] = &pdd->RefPicList_d[0][idx];
      RefPicList[1][idx] = &pdd->RefPicList_d[1][idx];
    }

    // Sort short term ref picture in ascending order of POC
    for (i=0; i<nb_of_st_ref_pics; i++)
    {
      int poc;

      if (!pic_order_cnt_type && ref_list_st[i]->non_existing_flag) // do not use non-existing frames when pic_order_cnt_type==0
        continue;
      poc = ref_list_st[i]->PicOrderCnt;
      idx = 0;
      while(idx<i && sorted_ref_list[idx]->PicOrderCnt < poc)
        idx++;
      if (idx<i) // insert this refpic before the bigger refpic poc
        memmove(&sorted_ref_list[idx+1], &sorted_ref_list[idx], sizeof(sorted_ref_list[0])*(i-idx));
      sorted_ref_list[idx] = ref_list_st[i];
      nb_of_sorted_pics++;
    }
    nb_of_st_ref_pics = nb_of_sorted_pics;

    // Get the index of the RefPic with the smallest POC bigger than or equal to the curr pic POC
    pivot_idx = 0;
    while((pivot_idx < nb_of_st_ref_pics) && (sorted_ref_list[pivot_idx]->PicOrderCnt <= currPicOrderCnt))
      pivot_idx++;
    // sorted_ref_list[k] poc, with k <  pivot_idx are smaller than or equal to curr pic poc
    // sorted_ref_list[k] poc, with k >= pivot_idx are bigger (strictly) than curr pic poc;

    if (max_num_ref_pic>1 && num_ref_idx_active1 == 1 && (pivot_idx==0 || pivot_idx==nb_of_st_ref_pics))
      num_ref_idx_active1 = 2; // deals with 2 pictures because they will be swapped at the end of the ref pic list init process

    // Fills in RefPicList0 and RefPicList1
    if (sh->field_pic_flag) // curr pic is a coded field
    {
      struct rv264picture* reordered_ref_list[2][NB_OF_REF_PICS];

      // Short term ref pic
      for (i=0; i<pivot_idx; i++)
        reordered_ref_list[1][nb_of_st_ref_pics-pivot_idx+i] = reordered_ref_list[0][i] = sorted_ref_list[pivot_idx-i-1];
      for (; i<nb_of_st_ref_pics; i++)
        reordered_ref_list[1][i-pivot_idx] = reordered_ref_list[0][i] = sorted_ref_list[i];

      size0 = alternate_field_parity(RefPicList[0], num_ref_idx_active0,
                             reordered_ref_list[0], nb_of_st_ref_pics,
                             sh->bottom_field_flag, SHORT_TERM_REF);
      size1 = alternate_field_parity(RefPicList[1], num_ref_idx_active1,
                             reordered_ref_list[1], nb_of_st_ref_pics,
                             sh->bottom_field_flag, SHORT_TERM_REF);

      // Long term ref pic
      if (nb_of_lt_ref_pics)
      {
        // long term list is the same for both lists ! However they may have different length
        size0 += alternate_field_parity(&RefPicList[0][size0], num_ref_idx_active0 - size0,
                                         ref_list_lt, 16,
                                         sh->bottom_field_flag, LONG_TERM_REF);
        size1 += alternate_field_parity(&RefPicList[1][size1], num_ref_idx_active1 - size1,
                                         ref_list_lt, 16,
                                         sh->bottom_field_flag, LONG_TERM_REF);
      }
    }
    else // Curr Pic is a coded frame
    {
      // Deals with L0 short term pics
      for (size0=0, i=pivot_idx-1; i>=0 && size0<num_ref_idx_active0; i--)
      {
        if ((sorted_ref_list[i]->ref_struct & 3) == 3) // only store frames and compl field pairs
        {
          use_picture(sorted_ref_list[i]);
          RefPicList[0][size0]->ref_pic = sorted_ref_list[i];
          RefPicList[0][size0]->ref_pic_type = SHORT_TERM_REF;
          RefPicList[0][size0]->parity = 0;
          size0++;
        }
      }
      for (i=pivot_idx; i<nb_of_st_ref_pics && size0<num_ref_idx_active0; i++)
      {
        if ((sorted_ref_list[i]->ref_struct & 3) == 3) // only store frames and compl field pairs
        {
          use_picture(sorted_ref_list[i]);
          RefPicList[0][size0]->ref_pic = sorted_ref_list[i];
          RefPicList[0][size0]->ref_pic_type = SHORT_TERM_REF;
          RefPicList[0][size0]->parity = 0;
          size0++;
        }
      }

      // Deals with L1 short term pics
      for (size1=0, i=pivot_idx; i<nb_of_st_ref_pics && size1<num_ref_idx_active1; i++)
      {
        if ((sorted_ref_list[i]->ref_struct & 3) == 3) // only store frames and compl field pairs
        {
          use_picture(sorted_ref_list[i]);
          RefPicList[1][size1]->ref_pic = sorted_ref_list[i];
          RefPicList[1][size1]->ref_pic_type = SHORT_TERM_REF;
          RefPicList[1][size1]->parity = 0;
          size1++;
        }
      }
      for (i=pivot_idx-1; i>=0 && size1<num_ref_idx_active1; i--)
      {
        if ((sorted_ref_list[i]->ref_struct & 3) == 3) // only store frames and compl field pairs
        {
          use_picture(sorted_ref_list[i]);
          RefPicList[1][size1]->ref_pic = sorted_ref_list[i];
          RefPicList[1][size1]->ref_pic_type = SHORT_TERM_REF;
          RefPicList[1][size1]->parity = 0;
          size1++;
        }
      }

      // Long term ref picture;
      if (nb_of_lt_ref_pics)
      {
        // L0
        for (i=0; i<16 && size0<num_ref_idx_active0; i++)
          if (ref_list_lt[i] && (ref_list_lt[i]->ref_struct & 3) == 3) // Frame or complementary field pair
          {
            use_picture(ref_list_lt[i]);
            RefPicList[0][size0]->ref_pic = ref_list_lt[i];
            RefPicList[0][size0]->ref_pic_type = LONG_TERM_REF;
            RefPicList[0][size0]->parity = 0;
            size0++;
          }
        // L1
        for (i=0; i<16 && size1<num_ref_idx_active1; i++)
          if (ref_list_lt[i] && (ref_list_lt[i]->ref_struct & 3) == 3) // Frame or complementary field pair
          {
            use_picture(ref_list_lt[i]);
            RefPicList[1][size1]->ref_pic = ref_list_lt[i];
            RefPicList[1][size1]->ref_pic_type = LONG_TERM_REF;
            RefPicList[1][size1]->parity = 0;
            size1++;
          }

      }
    }


    // Check that RefPicList0 and RefPicList1 are different. If not, swap the 2 first elements of RefPicList1
    // The list are identical if either currPOC is < than all POC or currPOC is > than all POC
    if ((pivot_idx==0 || pivot_idx==nb_of_st_ref_pics) && max_num_ref_pic > 1)
    {
      RefListEntry* tmp;
      tmp = RefPicList[1][0];
      RefPicList[1][0] = RefPicList[1][1];
      RefPicList[1][1] = tmp;

      // In some cases we need to release an extra picture (when ==1 but we set it to 2 in order to swap it afterward...
      if (num_ref_idx_active1 != sh->num_ref_idx_active[1])
      {
        LUD_DEBUG_ASSERT(num_ref_idx_active1 == 2); // only possible case where they can be different ! (see code above...)
        if (sh->field_pic_flag)
          release_picture_field(tmp->ref_pic, tmp->parity);
        else
          release_picture(tmp->ref_pic);
      }
    }

    // If the lists are shorter than num_ref_idx_active_minus1, fills in the list with unused for ref pics
    for (; size0 < num_ref_idx_active0; size0++)
    {
      RefPicList[0][size0]->ref_pic_type = UNUSED_FOR_REF;
      RefPicList[0][size0]->ref_pic = 0;
    }
    for (; size1 < num_ref_idx_active1; size1++)
    {
      RefPicList[1][size1]->ref_pic_type = UNUSED_FOR_REF;
      RefPicList[1][size1]->ref_pic = 0;
    }

  }
  else // P and SP Slice only !
  {
    RefListEntry** RefPicList = RefPicList_copy[0];
    LUD_DEBUG_ASSERT(sh->slice_type_modulo5 == SLICE_P || sh->slice_type_modulo5 == SLICE_SP);
    int num_ref_idx_active = sh->num_ref_idx_active[0];
    struct rv264picture* filtered_ref_list[NB_OF_REF_PICS];
    int nb_of_filtered_pics = 0;
    int size;

    // Set up table pointers
    for (i=0; i < num_ref_idx_active; i++)
      RefPicList[i] = &pdd->RefPicList_d[0][i];

    // When pic_order_cnt_type == 0, remove non-existing frames from the list ! There are non-existing frames only when gaps_in_frame_num_value_allowed_flag==1
    if (sps->gaps_in_frame_num_value_allowed_flag && pic_order_cnt_type == 0)
    {
      for (i=0; i<nb_of_st_ref_pics; i++)
      {
        if (ref_list_st[i]->non_existing_flag) // do not use non-existing frames when pic_order_cnt_type==0
          continue;
        filtered_ref_list[nb_of_filtered_pics++] = ref_list_st[i];
      }
      nb_of_st_ref_pics = nb_of_filtered_pics;
      ref_list_st = filtered_ref_list;
    }


    if (sh->field_pic_flag) // curr pic is a coded field
    {
      // short term list
      // Alternate field parity and remove non reference fields
      size = alternate_field_parity(
          RefPicList, num_ref_idx_active,
          ref_list_st, nb_of_st_ref_pics,
          sh->bottom_field_flag, SHORT_TERM_REF);

      // long term list
      if (nb_of_lt_ref_pics)
        // Alternate field parity and remove non reference fields
        size += alternate_field_parity(
            &RefPicList[size], num_ref_idx_active - size,
            ref_list_lt, 16,
            sh->bottom_field_flag, LONG_TERM_REF);
    }
    else // Curr Pic is a coded frame
    {
      // short term list
      for (i=size=0; i<nb_of_st_ref_pics && size<num_ref_idx_active; i++)
      {
        LUD_DEBUG_ASSERT(size<NB_OF_REF_PICS);
        if ((ref_list_st[i]->ref_struct & 3) == 3) // Frame or complementary field pair
        {
          use_picture(ref_list_st[i]);
          RefPicList[size]->ref_pic = ref_list_st[i];
          RefPicList[size]->ref_pic_type = SHORT_TERM_REF;
          RefPicList[size]->parity = 0;
          size++;
        }
      }
      // long term list
      if (nb_of_lt_ref_pics)
        for (i=0; i < 16 && size<num_ref_idx_active; i++)
        {
          LUD_DEBUG_ASSERT(size<NB_OF_REF_PICS);
          if (ref_list_lt[i] && (ref_list_lt[i]->ref_struct & 3) == 3) // Frame or complementary field pair
          {
            use_picture(ref_list_lt[i]);
            RefPicList[size]->ref_pic = ref_list_lt[i];
            RefPicList[size]->ref_pic_type = LONG_TERM_REF;
            RefPicList[size]->parity = 0;
            size++;
          }
        }
    }

    // If the list is shorter than num_ref_idx_active_minus1, fills in the list with unused for ref pics
    for (; size < num_ref_idx_active; size++)
    {
      RefPicList[size]->ref_pic_type = UNUSED_FOR_REF;
      RefPicList[size]->ref_pic = 0;
    }

  }
}


// Given a PicNum it return the corresponding id (frame_num or LongTermIdx), and the ref_struct of the pic designated by pic num
// picnum: PicNum to decode
// *ref_struct: in: reference structure of the current pic, out: structure of the decoded PicNum
// field_pic_flag, field flag of the current picture
// parity: parity of the current pic
static int extract_from_picnum(int picnum, int* ref_struct, int field_pic_flag, int parity)
{
  if (field_pic_flag)
  {
    *ref_struct = ((picnum & 1) == 0) ? (1-parity)+1: parity+1;  // if the picnum is even, this is an opposite parity field
    picnum >>= 1;
  }
  else
    *ref_struct = 3; // the current pic is a frame, only frame ref pic are identified

  return picnum;
}

static int find_short_term_ref_pic(struct rv264picture** ref_pic, int nb, int frame_num, int ref_struct)
{
  int i;
  for ( i=0; i<nb; i++)
  {
    if (ref_pic[i]->frame_num == frame_num) // there is only one pic with the same frame_num. If this is a frame or a comp field pair => they are in the same ref_pic
    {
      if ((ref_pic[i]->ref_struct & ref_struct) == ref_struct)
        return i;
      else
        return nb;
    }
  }
  return nb;
}

// TODO: check that : This function works on a private copy of ref pic lists since the initial list can be used for all the slices of the same pic
// => answer to that question is on page 75
//  => need to adapt according to slice_type: work on the original list when slice_type < 5, make a copy for the whole picture when slice_type >=5
// After re reading this I am not sure anymore: is the init_ref_pic_list the same for all slices of a picture is slice_type >=5 ??
// => this optimization has been removed for now...
static void list_reordering_process(PictureDecoderData* pdd, struct slice_header* sh, RefListEntry* RefPicList_copy[2][NB_OF_REF_PICS])
{
  int reordering_of_pic_nums_idc;
  int list;
  int nb_of_lists = (sh->slice_type_modulo5 == SLICE_B) ? 2 : 1;

  for (list=0; list<nb_of_lists; list++)
  {
    if (sh->ref_pic_list_reordering.ref_pic_list_reordering_flag[list])
    {
      uint32_t* commands = sh->ref_pic_list_reordering.pic_list_reordering_commands[list];
      int refIdxLX = 0;
      int CurrPicNum = sh->CurrPicNum;
      int MaxPicNum = sh->MaxPicNum;
      int picNumLXPred = CurrPicNum;
      RefListEntry** RefPicList = RefPicList_copy[list];
      int num_ref_idx_active = sh->num_ref_idx_active[list];
      int field_pic_flag = sh->field_pic_flag;
      int bottom_field_flag = sh->bottom_field_flag;
      struct rv264picture** ref_list_st = pdd->ref_list_st;
      struct rv264picture** ref_list_lt = pdd->ref_list_lt;
      int st_nb = pdd->ref_list_nb_of_st;

      do
      {
        struct rv264picture* designated_ref_pic = 0;
        ref_pic_type_t pic_ref_type;
        int pic_parity;

        LUD_DEBUG_ASSERT(refIdxLX < num_ref_idx_active);
        reordering_of_pic_nums_idc = commands[refIdxLX] >> 30; // 2 ue(v)
        LUD_DEBUG_ASSERT(reordering_of_pic_nums_idc <= 3);

        if (reordering_of_pic_nums_idc == 0 || reordering_of_pic_nums_idc == 1)
        {
          int abs_diff_pic_num = (commands[refIdxLX] & 0x3FFFFFFF) + 1;
          int picNumLXNoWrap;
          int frame_num;
          int pic_idx, ref_struct;

          if (reordering_of_pic_nums_idc == 0)
            picNumLXNoWrap = picNumLXPred - abs_diff_pic_num;
          else
            picNumLXNoWrap = picNumLXPred + abs_diff_pic_num;
          picNumLXNoWrap &= (MaxPicNum-1); // modulo MaxPicNum, optimize because MaxPicNum is a power of 2 !

          picNumLXPred = picNumLXNoWrap;

          frame_num = extract_from_picnum(picNumLXNoWrap, &ref_struct, field_pic_flag, bottom_field_flag);
          pic_ref_type = SHORT_TERM_REF;
          pic_parity = 1-(ref_struct&1); // true, even if it is a frame
          pic_idx = find_short_term_ref_pic(ref_list_st, st_nb, frame_num, ref_struct);
          if (pic_idx < st_nb)
            designated_ref_pic = ref_list_st[pic_idx];
          else
          {
            LUD_TRACE(TRACE_ERROR, "Missing Short Term reference picture, ignoring reordering command...");
          }
        }

        else if (reordering_of_pic_nums_idc == 2)
        {
          int long_term_pic_num = commands[refIdxLX] & 0x3FFFFFFF;
          int pic_idx;
          int ref_struct;

          pic_idx = extract_from_picnum(long_term_pic_num, &ref_struct, field_pic_flag, bottom_field_flag);
          pic_ref_type = LONG_TERM_REF;
          pic_parity = 1-(ref_struct&1); // true, even if it is a frame
          if (pic_idx <= pdd->MaxLongTermFrameIdx && ref_list_lt[pic_idx] && (ref_list_lt[pic_idx]->ref_struct & ref_struct) == ref_struct)
            designated_ref_pic = ref_list_lt[pic_idx];
          else
          {
            LUD_DEBUG_ASSERT(0);
            LUD_TRACE(TRACE_ERROR, "Missing Long Term reference picture, ignoring reordering command...");
          }
        }

        else // this is the exit command !
        {
          LUD_DEBUG_ASSERT(reordering_of_pic_nums_idc == 3);
          break;
        }

        if (designated_ref_pic)
        {
          int i;
          RefListEntry* tmp;
          // Find the designated pic in RefPicList, if present
          // We need to split in two cases if field_pic_flag, we could have a frame or comp field pair (same designated_ref_pic pointer) but with different parity
          if (field_pic_flag)
          {
            for (i = refIdxLX; i<num_ref_idx_active-1; i++)
              if (RefPicList[i]->ref_pic == designated_ref_pic && RefPicList[i]->parity == pic_parity)
                break;
            tmp = RefPicList[i]; // pic at index i will be overwritten
            release_picture_field(tmp->ref_pic, tmp->parity);  // this picture is not used anymore (it is overwritten just bellow)
            use_picture_field(designated_ref_pic, pic_parity); // this picture is used one more time
          }
          else //field_pic_flag==0
          {
            for (i = refIdxLX; i<num_ref_idx_active-1; i++)
              if (RefPicList[i]->ref_pic == designated_ref_pic)
                break;
            tmp = RefPicList[i]; // pic at index i will be overwritten
            release_picture(tmp->ref_pic);  // this picture is not used anymore (it is overwritten just bellow)
            use_picture(designated_ref_pic); // this picture is used one more time
          }

          // Shift the elements of the list
          for ( ; i>refIdxLX; i--)
            RefPicList[i] = RefPicList[i-1];

          RefPicList[refIdxLX] = tmp;
          RefPicList[refIdxLX]->ref_pic = designated_ref_pic;
          RefPicList[refIdxLX]->ref_pic_type = pic_ref_type;
          RefPicList[refIdxLX]->parity = pic_parity; // when the pic is a frame, the parity is equal to 0
        }

        refIdxLX++;
      } while (refIdxLX < num_ref_idx_active);
    }
  }
}

// This function is invoked at the end of the decoding of a picture
// sh, sps are taken from pdd context
static int ref_pic_sliding_window_process(PictureDecoderData* pdd, struct rv264seq_parameter_set* sps, struct slice_header* sh)
{
  int current_pic_is_marked;

  if (pdd->is_second_field_of_a_pair/* && pdd->prev_ref_pic_type == SHORT_TERM_REF / *the first field has been marked as short term ref*/)
  {
    // This is the second field of the pair !
    // mark this field as short term
    // Remark: if the first field of the pair has been marked for long term (by long_term_reference_flag of mmco==6)
    //         the second must be (will be) marked as long term as well by a mmco=6. See spec page 84

    // Nothing special to do, the field is part of a pair, and the first field has already been marked, so was the pair...
    use_picture_field(pdd->pic, sh->bottom_field_flag);
    current_pic_is_marked = 1;
  }
  else
  {
    current_pic_is_marked = 0;
    if (pdd->ref_list_nb_of_st + pdd->ref_list_nb_of_lt == im_max(1, sps->max_num_ref_frames))
    { // need to discard one pic
      LUD_DEBUG_ASSERT(pdd->ref_list_nb_of_st>0);

      // drop the pic with the lowest FrameNumWrap => oldest pic => last pic in the short term array
      // when it is a frame or a field pair, both its fields are dropped
      pdd->ref_list_nb_of_st--;
      release_picture_field_struct(pdd->ref_list_st[pdd->ref_list_nb_of_st], pdd->ref_list_st[pdd->ref_list_nb_of_st]->ref_struct);
      pdd->ref_list_st[pdd->ref_list_nb_of_st] = 0; // not really necessary, but help when debugging
    }
  }

  return current_pic_is_marked;
}

// This function is theoretically invoked at the end of the decoding of a picture
//    => called at the beginning of the decoding of the next picture (because of the end of picture detection mechanism)
//    => sh pointer/structure is not valid, all data must be taken from pdd of sps/pps pointers directly
static int ref_pic_mmco_process(PictureDecoderData* pdd, struct slice_header* sh)
{
  int idx = 0;
  unsigned int command;
  int memory_management_control_operation;
  int i;
  struct rv264picture** ref_list_st = pdd->ref_list_st;
  struct rv264picture** ref_list_lt = pdd->ref_list_lt;
  int st_nb = pdd->ref_list_nb_of_st;
  int lt_nb = pdd->ref_list_nb_of_lt;
  int current_pic_is_marked = 0;
  int field_pic_flag = sh->field_pic_flag;
  int bottom_field_flag = sh->bottom_field_flag;
  int MaxPicNum = sh->MaxPicNum;

  do
  {
    command = sh->dec_ref_pic_marking.mmco_commands[idx];
    memory_management_control_operation = command >> 29;

    switch (memory_management_control_operation)
    {
      case 0: // nothing to do : end command.
        break;

        // Marking process of a short-term reference picture as “unused for reference”
      case 1: // 2|5 ue(v) difference_of_pic_nums_minus1 bits, 0..28
        {
          int difference_of_pic_nums_minus1 = command & 0x1FFFFFFF;
          int picNumXNoWrap = (sh->CurrPicNum - (difference_of_pic_nums_minus1 + 1)) & (MaxPicNum-1);
          int frame_num;
          int ref_struct;
          struct rv264picture* pic;

          frame_num = extract_from_picnum(picNumXNoWrap, &ref_struct, field_pic_flag, bottom_field_flag);
          i = find_short_term_ref_pic(ref_list_st, st_nb, frame_num, ref_struct);
          if (i >= st_nb)
          {
            //LUD_DEBUG_ASSERT(0); // We should find the pic !
            LUD_TRACE(TRACE_ERROR, "Missing Short Term reference picture, ignoring mmco command...");
            break; // end of case 1
          }
          // Mark the field as unused for reference
          pic = ref_list_st[i];
          ref_list_st[i]->ref_struct &= ~ref_struct;
          if (!(ref_list_st[i]->ref_struct & 3)) // if there are no ref field left, remove the ref_pic from the list
          {
            st_nb--;
            memmove(&ref_list_st[i], &ref_list_st[i+1], sizeof(ref_list_st[0])*(st_nb-i));
          }
          release_picture_field_struct(pic, ref_struct);
        }
        break;

        // Marking process of a long-term reference picture as “unused for reference”
      case 2: // 2|5 ue(v) long_term_pic_num, bits 0..28
        {
          int long_term_pic_num = command & 0x1FFFFFFF;
          int ref_struct;
          struct rv264picture* pic;

          i = extract_from_picnum(long_term_pic_num, &ref_struct, field_pic_flag, bottom_field_flag);
          if (i > pdd->MaxLongTermFrameIdx)
          {
            //LUD_DEBUG_ASSERT(0); // i should be smaller or equal to MaxLongTermFrameIdx
            LUD_TRACE(TRACE_ERROR, "Out of range LongTermFrameIdx, ignoring mmco command...");
            break; // end of case 2
          }
          if (!ref_list_st[i])
          {
            //LUD_DEBUG_ASSERT(0); // the long term ref pic should be existing !
            LUD_TRACE(TRACE_ERROR, "Missing Long Term reference picture, ignoring mmco command...");
            break; // end of case 2
          }
          // Mark the field as unused for reference
          pic = ref_list_lt[i];
          ref_list_lt[i]->ref_struct &= ~ref_struct;
          if (!(ref_list_lt[i]->ref_struct & 3)) // if there are no ref field left, remove the ref_pic from the list
          {
            lt_nb--;
            ref_list_lt[i] = 0;
          }
          release_picture_field_struct(pic, ref_struct);
        }
        break;

        // Assignment process of a LongTermFrameIdx to a short-term reference picture
      case 3:
        {
          int difference_of_pic_nums_minus1 = command & 0x03FFFFF;  // 2|5 ue(v) difference_of_pic_nums_minus1, bits 0..22 (23 bits is enough)
          int long_term_frame_idx = (command>>23) & 0x3F;  // 2|5 ue(v) long_term_frame_idx, bits 23..28 (6 bits is enough)
          int picNumXNoWrap = (sh->CurrPicNum - (difference_of_pic_nums_minus1 + 1)) & (MaxPicNum-1);
          int frame_num, ref_struct;

          if (long_term_frame_idx > pdd->MaxLongTermFrameIdx)
          {
            //LUD_DEBUG_ASSERT(0); // long_term_frame_idx should be smaller or equal to MaxLongTermFrameIdx
            LUD_TRACE(TRACE_ERROR, "Out of range LongTermFrameIdx, ignoring mmco command...");
            break; // end of case 1
          }

          frame_num = extract_from_picnum(picNumXNoWrap, &ref_struct, field_pic_flag, bottom_field_flag);
          // With the following code, if the field pertain to a ref frame or a comp ref field pair, both field are going to be marked as long term ref.
          // However this is not a prb since according to page 84 of ITU-T Rec. H.264 (03/2005), a second command must follow
          // to assign the same long term idx to the other field. In other word, if one field of a frame/comp field pair is marked as
          // long term, the other is marked as long term as well
          if (field_pic_flag && ref_list_lt[long_term_frame_idx] &&
              ref_list_lt[long_term_frame_idx]->frame_num == frame_num)
            break; // Nothing to do, we have already moved the frame or comp field pair

          i = find_short_term_ref_pic(ref_list_st, st_nb, frame_num, ref_struct);
          if (i >= st_nb)
          {
            //LUD_DEBUG_ASSERT(0); // We should find the pic !
            LUD_TRACE(TRACE_ERROR, "Missing Short Term reference picture, ignoring mmco command...");
            break; // end of case 3
          }

          // If a previous long term ref pic is present at that index, we need to release it (the spec says it is marked as unused for ref)
          if (ref_list_lt[long_term_frame_idx])
          {
            release_picture_field_struct(ref_list_lt[long_term_frame_idx], ref_list_lt[long_term_frame_idx]->ref_struct);
            lt_nb--;
          }

          ref_list_lt[long_term_frame_idx] = ref_list_st[i];
          lt_nb++;
          st_nb--;
          memmove(&ref_list_st[i], &ref_list_st[i+1], sizeof(ref_list_st[0])*(st_nb-i));
        }
        break;

        // Decoding process for MaxLongTermFrameIdx
      case 4: // 2|5 ue(v) max_long_term_frame_idx_plus1, bits 0..28
        {
          int max_long_term_frame_idx_plus1 = (command & 0x1FFFFFFF);
          if (max_long_term_frame_idx_plus1 > 16)
          {
            //LUD_DEBUG_ASSERT(0);
            LUD_TRACE(TRACE_ERROR, "MaxLongTermFrameIdx out of range, ignoring mmco command...");
            break; // end of case 4
          }
          for (i = max_long_term_frame_idx_plus1; i < 16 && lt_nb>0; i++)
            if (ref_list_lt[i])
            {
              release_picture_field_struct(ref_list_lt[i], ref_list_lt[i]->ref_struct);
              ref_list_lt[i] = 0;
              lt_nb--;
            }
          pdd->MaxLongTermFrameIdx = max_long_term_frame_idx_plus1-1;
        }
        break;

        // Marking process of all reference pictures as “unused for reference” and setting
        // MaxLongTermFrameIdx to “no long-term frame indices”
      case 5:
        {
          // Remove all the short term and long term ref pics
          for (i = 0; i < st_nb; i++)
            if (ref_list_st[i])
            {
              release_picture_field_struct(ref_list_st[i], ref_list_st[i]->ref_struct);
              ref_list_st[i] = 0;
            }
          if (lt_nb) // only need to process the long term list if there are some !
            for (i = 0; i < 16; i++)
              if (ref_list_lt[i])
              {
                release_picture_field_struct(ref_list_lt[i], ref_list_lt[i]->ref_struct);
                ref_list_lt[i] = 0;
              }
          st_nb = lt_nb = 0;
          pdd->MaxLongTermFrameIdx = -1;
        }
        break;

        // Process for assigning a long-term frame index to the current picture
      case 6: // 2|5 ue(v) long_term_frame_idx, bits 0..28
        {
          int long_term_frame_idx = command & 0x1FFFFFFF;
          if (long_term_frame_idx > pdd->MaxLongTermFrameIdx)
          {
            //LUD_DEBUG_ASSERT(0); // long_term_frame_idx should be smaller or equal to MaxLongTermFrameIdx
            LUD_TRACE(TRACE_ERROR, "Out of range LongTermFrameIdx, ignoring mmco command...");
            break; // end of case 6
          }
          LUD_DEBUG_ASSERT(current_pic_is_marked == 0);

          current_pic_is_marked = 1;

          // This may be the second field of a comp ref field pair. In such case the comp ref field pair has already been marked as long term ref pic
          // and this field is added to the comp ref field pair, so there is nothing to do here.
          if (field_pic_flag && pdd->is_second_field_of_a_pair &&
              ref_list_lt[long_term_frame_idx] && ref_list_lt[long_term_frame_idx]->frame_num == sh->frame_num)
          {
            use_picture_field(ref_list_lt[long_term_frame_idx], sh->bottom_field_flag);
            break; // Nothing to do, we have already moved the frame or comp field pair
          }


          // If a previous long term ref pic is present at that index, we need to release it (the spec says it is marked as unused for ref)
          if (ref_list_lt[long_term_frame_idx])
          {
            release_picture_field_struct(ref_list_lt[long_term_frame_idx], ref_list_lt[long_term_frame_idx]->ref_struct);
            lt_nb--;
          }

          ref_list_lt[long_term_frame_idx] = set_ref_pic(pdd, sh);
          lt_nb++;
        }
        break;

      default:
        //LUD_DEBUG_ASSERT(0); // unknown command !!!
        LUD_TRACE(TRACE_ERROR, "Unknown mmco command");
        break;
    }
    idx++;
  } while (memory_management_control_operation != 0);

  // Re assign ref pic nb
  pdd->ref_list_nb_of_st = st_nb;
  pdd->ref_list_nb_of_lt = lt_nb;

  return current_pic_is_marked;
}

// Only call this function when the picture is a reference picture !
// This function is theoretically invoked at the end of the decoding of a picture
//    => called at the beginning of the decoding of the next picture (because of the end of picture detection mechanism)
//    => sh pointer/structure is not valid, all data must be taken from pdd of sps/pps pointers directly
static void ref_pic_marking_process(PictureDecoderData* pdd, struct slice_header* sh)
{
  int i;
  struct rv264picture** ref_list_st = pdd->ref_list_st;
  struct rv264picture** ref_list_lt = pdd->ref_list_lt;

  if (sh->nal_unit_type == NAL_CODED_SLICE_IDR)
  {
    int st_nb = pdd->ref_list_nb_of_st;
    int lt_nb = pdd->ref_list_nb_of_lt;

    // Remove all reference pictures from the list
    for (i=0; i<st_nb; i++)
      release_picture_field_struct(ref_list_st[i], ref_list_st[i]->ref_struct);

    if (lt_nb)
      for ( i=0; i<16; i++)
        if (ref_list_lt[i])
        {
          release_picture_field_struct(ref_list_lt[i], ref_list_lt[i]->ref_struct);
          ref_list_lt[i] = 0;
        }

    if (sh->dec_ref_pic_marking.long_term_reference_flag)
    { // The picture is marked as long term
      ref_list_lt[0] = set_ref_pic(pdd, sh);
      pdd->MaxLongTermFrameIdx = 0;
      pdd->ref_list_nb_of_lt = 1;
      pdd->ref_list_nb_of_st = 0;
    }
    else
    { // the pic is marked as short term, the list is empty add it at idx 0 !
      ref_list_st[0] = set_ref_pic(pdd, sh);
      pdd->MaxLongTermFrameIdx = -1;
      pdd->ref_list_nb_of_st = 1;
      pdd->ref_list_nb_of_lt = 0;
    }
  }

  else // this is not an IDR picture
  {
    int current_pic_is_marked;

    if (sh->dec_ref_pic_marking.adaptive_ref_pic_marking_mode_flag)
    { // Do the mmco process
      current_pic_is_marked = ref_pic_mmco_process(pdd, sh);
    }
    else
    { // Do the sliding window process
      current_pic_is_marked = ref_pic_sliding_window_process(pdd, sh->sps, sh);
    }

    if (!current_pic_is_marked) // The current pic was not marked, so mark it as used for short term
    {
      if (pdd->ref_list_nb_of_st >= NB_OF_REF_PICS)
      {
        LUD_DEBUG_ASSERT(0); // Too many ref pics in the list ! should never happen, there must be a bug either in the stream or in the list management...
        // free the oldest pic... in order to prevent buffer overflow...
        pdd->ref_list_nb_of_st--;
        release_picture_field_struct(ref_list_st[pdd->ref_list_nb_of_st], ref_list_st[pdd->ref_list_nb_of_st]->ref_struct);
      }
      // If this is the second field of a pair, nothing to do, the pair has already been marked !
      if (pdd->is_second_field_of_a_pair)
      {
        // In that case the first field must be a short term ref as well !
        use_picture_field(pdd->pic, sh->bottom_field_flag);
      }
      else
      {
        // shift all elements of the list, and add the pic at idx 0 (newest pic)
        memmove(&ref_list_st[1], &ref_list_st[0], sizeof(ref_list_st[0]) * pdd->ref_list_nb_of_st);
        ref_list_st[0] = set_ref_pic(pdd, sh);
        pdd->ref_list_nb_of_st++;
      }
    }
  }
}


/* debug functions */
void print_RefPicList(PictureDecoderData* pdd, struct slice_header* sh)
{
  int list, i;
  const char* ref_name[4] = {"U", "S", "L", "N"};
  int nb_of_lists = (sh->slice_type_modulo5 == SLICE_B) ? 2 : 1;;

  for (list=0; list<nb_of_lists; list++)
  {
    RefListEntry** RefPicList = pdd->RefPicList[list];
    printf("L%d:  %d pics | ", list, sh->num_ref_idx_active[list]);
    for (i=0; i < sh->num_ref_idx_active[list]; i++)
    {
      if (RefPicList[i]->ref_pic_type)
        if (RefPicList[i]->ref_pic->non_existing_flag)
          printf("%s:FN %d, POC %d | ", "N", RefPicList[i]->ref_pic->frame_num, RefPicList[i]->ref_pic->PicOrderCnt);
        else
          printf("%s:FN %d, POC %d | ", ref_name[RefPicList[i]->ref_pic_type], RefPicList[i]->ref_pic->frame_num, RefPicList[i]->ref_pic->PicOrderCnt);
      else
        printf("%s | ", ref_name[RefPicList[i]->ref_pic_type]);
    }
    printf("\n");
  }
}

void print_ref_pic_lists(PictureDecoderData* pdd)
{
  int i;
  printf("ref list: %d ST - ", pdd->ref_list_nb_of_st);
  for ( i=0; i<pdd->ref_list_nb_of_st; i++)
    printf("FN %d ", pdd->ref_list_st[i]->frame_num);
  printf("| %d LT - ", pdd->ref_list_nb_of_lt);
  for ( i=0; i<16; i++)
    if (pdd->ref_list_lt[i])
      printf("IDX%d ", i);
  printf("\n");
}


static void storeRefPicList(PictureDecoderData* pdd, struct slice_header* sh)
{
  int i;


  sh->RefPicList[0] = (RefListEntry_s *)rvalloc(NULL, sizeof(RefListEntry) * (sh->num_ref_idx_active[0]+sh->num_ref_idx_active[1]), 0);

  for (i=0; i<sh->num_ref_idx_active[0]; i++)
    sh->RefPicList[0][i] = *pdd->RefPicList[0][i];


  if (sh->slice_type_modulo5 == SLICE_B)
  {
    sh->RefPicList[1] = sh->RefPicList[0] + sh->num_ref_idx_active[0];
    for (i=0; i<sh->num_ref_idx_active[1]; i++)
      sh->RefPicList[1][i] = *pdd->RefPicList[1][i];
  }
}


// Invoked when the current picture parsing is finished
static void end_picture(struct rv264sequence *seq, PictureDecoderData* pdd)
{
  struct slice_header* sh = pdd->prev_sh;
  struct rv264picture* pic = pdd->pic;

  // If the picture is already finished, just return...
  if (!pdd->pic_not_finished)
    return;

  // Do things for the previous picture First !

  // Store previous values for POC decoding, not needed when curr pic is an IDR because poc does not use prev values in that case !
  store_prev_values_for_poc(pdd, pic, sh);

  // If the previous(current) picture is a ref pic and the second of a pair, the RefPic holding the first field of the pair have to be updated (to hold the 2 fields!)
  if (pdd->is_second_field_of_a_pair && sh->nal_ref_idc)
  {
    // The corresponding RefPic is the last of the list (newest RefPic)
    LUD_DEBUG_ASSERT(pic->frame_num == sh->frame_num);
    pic->ref_struct |= sh->bottom_field_flag+1;
  }

  // decode image samples
  decode_image(seq, pic, sh->bottom_field_flag);


  // Do the marking process if the picture is a reference picture !
  if (sh->nal_ref_idc)
    ref_pic_marking_process(pdd, sh);

    // FIXME DEBUG
    //print_ref_pic_lists(pdd);

  // If the picture had a mmco==5,
  if (sh->has_mmco5)
  {
    // its frame_num has to be set to 0
    pic->frame_num = sh->frame_num = 0;
    // its poc is recalculated
    pic->FieldOrderCnt[0] -= pic->PicOrderCnt;
    pic->FieldOrderCnt[1] -= pic->PicOrderCnt;
    pic->PicOrderCnt = 0;
    if (sh->sps->pic_order_cnt_type == 0)
      store_prev_values_for_poc(pdd, pic, sh); // this function is called again when pic_order_cnt_type == 0
                                      // in order to correctly set prevPicOrderCntMsb and prevPicOrderCntLsb
  }


  // The picture has finished to decode !
  pdd->pic_not_finished = 0;
}

// Invoked when there is no more data in the input stream, so we can finish and tide up the current decoding session
static void end_decoding(struct rv264sequence *seq)
{
  PictureDecoderData* pdd = &seq->pdd;

  // Protect from multiple call to this function
  if (pdd->decoding_finished)
    return;
  pdd->decoding_finished = 1;

  end_picture(seq, pdd);

  // We can now release the last picture !
  if (pdd->prev_sh->field_pic_flag)
    release_picture_field(pdd->pic, pdd->prev_sh->bottom_field_flag);
  else
    release_picture(pdd->pic);


  // Finish the decoding of the last picture (if necessary)
  decode_image_end();

  // Release the reference pictures from the pdd context
  struct rv264picture** ref_list_st = pdd->ref_list_st;
  struct rv264picture** ref_list_lt = pdd->ref_list_lt;
  int st_nb = pdd->ref_list_nb_of_st;
  int lt_nb = pdd->ref_list_nb_of_lt;
  int i;

  // Remove all reference pictures from the list
  for (i=0; i<st_nb; i++)
    release_picture_field_struct(ref_list_st[i], ref_list_st[i]->ref_struct);

  if (lt_nb)
    for ( i=0; i<16; i++)
    if (ref_list_lt[i])
    {
      release_picture_field_struct(ref_list_lt[i], ref_list_st[i]->ref_struct);
      ref_list_lt[i] = 0;
    }

  // Empty the ouput buffer !
  flush_dpb(&seq->dpb);

  // Close the output buffer module
  close_dpb(&seq->dpb);
}


// Invoked when a new picture is detected;
static void start_picture(PictureDecoderData* pdd, struct slice_header* sh, struct rv264seq_parameter_set* sps, struct rv264pic_parameter_set* pps)
{
  struct slice_header* prev_sh = pdd->prev_sh;

  LUD_DEBUG_ASSERT(!pdd->pic_not_finished);
  pdd->pic_not_finished = 1;

  // Start to assign picture number (in decoding order) to the current picture
  pdd->dec_num++;

  // Detect if this is the second field of a complementary field pair, the process is different if this is a reference picture or not
  if (sh->field_pic_flag && prev_sh->field_pic_flag)
  {
    if(sh->nal_ref_idc) // reference pics
    {
      if (prev_sh->nal_ref_idc && prev_sh->frame_num == sh->frame_num &&
        sh->nal_unit_type != NAL_CODED_SLICE_IDR && !sh->has_mmco5)
        pdd->is_second_field_of_a_pair = 1; // complementary reference field pair
      else
        pdd->is_second_field_of_a_pair = 0;
    }
    else // non reference pics
    {
      if (!prev_sh->nal_ref_idc &&
          prev_sh->bottom_field_flag != sh->bottom_field_flag && !pdd->is_second_field_of_a_pair)
        pdd->is_second_field_of_a_pair = 1; // complementary non-reference field pair
      else
        pdd->is_second_field_of_a_pair = 0;
    }
  }
  else
    pdd->is_second_field_of_a_pair = 0;


  struct rv264picture* prev_pic = pdd->pic;
  if (!pdd->is_second_field_of_a_pair)
  {
    // Allocate memory for that picture
    pdd->pic = alloc_picture(pdd, sh);
  }
  else
  { // second field of a pair !
    // update structure flag
    LUD_DEBUG_ASSERT(pdd->pic);
    pdd->pic->structure = 3;
    // allocate the second field sh array
    unsigned int bottom_field_flag = sh->bottom_field_flag;
    pdd->pic->field_sh[bottom_field_flag] = (struct slice_header **)rvalloc(NULL, sizeof(struct slice_header *) * pdd->max_num_of_slices, 0);
    use_picture_field(pdd->pic, bottom_field_flag);
  }

  pdd->pic->dec_num[sh->bottom_field_flag] = pdd->dec_num;

  // Decode POC
  decode_picture_order_count(pdd, pdd->pic, sh, sps);

  // Derive PrevRefFrameNum
  derive_PrevRefFrameNum(pdd, sh);

  // We can now release the previous picture !
  if (prev_sh->field_pic_flag)
    release_picture_field(prev_pic, prev_sh->bottom_field_flag);
  else
    release_picture(prev_pic);


  // Process the Slice group map
  if (pps->num_slice_groups_minus1) // if num_slice_groups_minus1 is 0, we do not need to derive MbToSliceGroupMap, see NextMbAddress function
  {
    sh->MbToSliceGroupMap = (unsigned char *)rvalloc(NULL, (sh->PicSizeInMbs)*sizeof(*sh->MbToSliceGroupMap), 0);
    derive_MbToSliceGroupMap(sps, pps, sh, sh->MbToSliceGroupMap);
  }

}

RetCode decode_slice(struct rv264sequence *seq, PictureDecoderData* pdd, nal_unit_t* nalu)
{
  RetCode r;
  struct slice_header* sh;
  struct rv264seq_parameter_set* sps;
  struct rv264pic_parameter_set* pps;
  struct rv264picture* pic;
  unsigned int bottom_field_flag;

  if (LUDH264_SUCCESS != (r=parse_slice_header_lud(seq, nalu, &sh)))
    return r;

  sps = sh->sps;
  pps = sh->pps;
  bottom_field_flag = sh->bottom_field_flag;

  if (is_new_picture(pdd->prev_sh, sh, sps))
  {
    // This slice will begin a new picture !
    //printf("New  detected\n");


    // Execute actions that finish the picture decoding
    end_picture(seq, pdd);

    // Execute actions for starting a new picture decoding
    start_picture(pdd, sh, sps, pps);
    pic = pdd->pic;
  }
  else // new slice but same picture
  {
    pic = pdd->pic;
    pic->slice_num[bottom_field_flag]++;
    if (pic->slice_num[bottom_field_flag] >= pdd->max_num_of_slices) // check that the slice params array is big enough in order to store params for each slice
    {
      pdd->max_num_of_slices *= 2;
      pic->field_sh[bottom_field_flag] = (struct slice_header **)rvalloc(pic->field_sh[bottom_field_flag], pdd->max_num_of_slices * sizeof(struct slice_header *), 0);
      if (!sh->field_pic_flag)
        pic->field_sh[1-bottom_field_flag] = pic->field_sh[bottom_field_flag]; // keep the pointers identical
    }

    //MbToSliceGroupMap pointer is copied for the following slices of the same picture (by spec it is identical for all slices of a picture)
    sh->MbToSliceGroupMap = pdd->prev_sh->MbToSliceGroupMap;
  }
  pic->field_sh[bottom_field_flag][pic->slice_num[bottom_field_flag]] = sh;

  if (verbose>=1)
    LUD_TRACE(TRACE_INFO, "picture: number %d, slice_type %d, slice_num %d, frame_num %d, POC %d, field_pic_flag %d, size: %dx%d",
         pdd->dec_num, sh->slice_type, pic->slice_num[bottom_field_flag], sh->frame_num, pic->PicOrderCnt, sh->field_pic_flag, sps->PicWidthInMbs*16, sh->PicHeightInMbs*16);

  // decoding process for gaps in frame_num
  if (sh->frame_num != pdd->PrevRefFrameNum && sh->frame_num != ((pdd->PrevRefFrameNum+1) & (sps->MaxFrameNum-1))) // modulo sps->MaxFrameNum which is a power of 2
    decoding_process_for_gaps_in_frame_num(pdd, sh, sps);


  // When processing B P or SP slice, generate reference list and do reordering when necessary
  if (sh->slice_type_modulo5 == SLICE_B || sh->slice_type_modulo5 == SLICE_P || sh->slice_type_modulo5 == SLICE_SP)
  {
    init_ref_pic_list(pdd, pic, sh, sps, pdd->RefPicList);

    list_reordering_process(pdd, sh, pdd->RefPicList);
    /* FIXME DEBUG Print Ref Lists */
    //print_RefPicList(pdd, sh);

    // Save the RefPicLists in the slice header parameters for later use
    storeRefPicList(pdd, sh);
  }

  sh->slice_num = pic->slice_num[bottom_field_flag];
  sh->pic = pic;



  // Store the slice header to compare it with the next slice
  pdd->prev_sh = sh;

  // Copy any SEI messages present before this nalu
  memcpy(&pic->sei_message, &seq->sei_message, sizeof(struct rv264sei_message));

  use_object(nalu); // increment the ref counter. the naly will be released when the slice data decoding is done
  sh->nalu = nalu; // the slice data decoding is done once the full picture (field) is parsed (all slices)
  //decode_slice_data(pic, sh);
  return LUDH264_SUCCESS;
}


static struct slice_header dummy_sh; // a all 0-filled structure that will prevent from testing if prev_sh pointer is non null each time it is accessed !
RetCode decoder_init(struct rv264sequence *seq)
{

  int i;

  // Set default values of gdd and pdd when necessary
  memset(&gdd, 0, sizeof(gdd));

  seq->pdd.max_num_of_slices = 1;
  seq->pdd.decoding_finished = 0;

  memset(&dummy_sh, 0, sizeof(dummy_sh)); // all 0 should make a good "before-first" slice
  seq->pdd.prev_sh = &dummy_sh; // needed so that there is no need to test if the pointer is non null

  // Init VLC tables
  for (i=0; i<3; i++)
  {
    init_vlc(&gdd.coeff_token_vlc_tables[i], 62, coeff_token_values, coeff_token[i], coeff_token_len[i], 9);
  }
  init_vlc(&gdd.coeff_token_vlc_tables[3], 14, coeff_token_values, coeff_token_cdc0, coeff_token_cdc0_len, 9);
  init_vlc(&gdd.coeff_token_vlc_tables[4], 30, coeff_token_values, coeff_token_cdc1, coeff_token_cdc1_len, 9);
  for( i=0; i<15; i++)
  {
    init_vlc(&gdd.total_zeros_4x4_vlc_tables[i], 16-i, total_zeros_values, total_zeros_for4x4blocks[i], total_zeros_for4x4blocks_len[i], 8);
  }
  for( i=0; i<3; i++)
  {
    init_vlc(&gdd.total_zeros_2x2_vlc_tables[i], 4-i, total_zeros_values, total_zeros_for2x2blocks[i], total_zeros_for2x2blocks_len[i], 3);
  }
  for( i=0; i<7; i++)
  {
    init_vlc(&gdd.total_zeros_2x4_vlc_tables[i], 8-i, total_zeros_values, total_zeros_for2x4blocks[i], total_zeros_for2x4blocks_len[i], 5);
  }
  for( i=0; i<7; i++)
  {
    uint8_t size[7] = { 2, 3, 4, 5, 6, 7, 15 };
    init_vlc(&gdd.run_before_vlc_tables[i], size[i], total_zeros_values, run_before_vlc[i], run_before_vlc_len[i], 6);
  }

  // Init CABAC tables
  cabac_init_data();

  init_dpb(&seq->dpb);

  // Init decoder
  decode_image_init();

  return LUDH264_SUCCESS;
}

RetCode decoder_reset(struct rv264sequence *seq)
{
  end_decoding(seq);
  seq->pdd.pic = 0;
  seq->pdd.prev_sh = &dummy_sh;
  seq->pdd.prevPicOrderCntMsb = 0;
  seq->pdd.prevPicOrderCntLsb = 0;
  seq->pdd.PicOrderCntMsb = 0;
  seq->pdd.FrameNumOffset = 0;
  seq->pdd.prevFrameNumOffset = 0;
  seq->pdd.prevFrameNum = 0;
  seq->pdd.PrevRefFrameNum = 0;
  seq->pdd.dec_num = 0;
  seq->pdd.pic_not_finished = 0;
  seq->pdd.decoding_finished = 0;
  memset(seq->pdd.ref_list_st, 0, sizeof(seq->pdd.ref_list_st));
  seq->pdd.ref_list_nb_of_st = 0;
  memset(seq->pdd.ref_list_lt, 0, sizeof(seq->pdd.ref_list_lt));
  seq->pdd.ref_list_nb_of_lt = 0;
  memset(seq->pdd.RefPicList_d, 0, sizeof(seq->pdd.RefPicList_d));
  memset(seq->pdd.RefPicList, 0, sizeof(seq->pdd.RefPicList));
  seq->pdd.MaxLongTermFrameIdx = 0;
  seq->pdd.is_second_field_of_a_pair = 0;

  return LUDH264_SUCCESS;
}

RetCode decoder_destroy(struct rv264sequence *seq)
{
  int i;

  // Stop image decoder
  decode_image_destroy();

  // Free VLC tables
  for (i=0; i<5; i++)
  {
    destroy_vlc(&gdd.coeff_token_vlc_tables[i]);
  }
  for( i=0; i<15; i++)
  {
    destroy_vlc(&gdd.total_zeros_4x4_vlc_tables[i]);
  }
  for( i=0; i<3; i++)
  {
    destroy_vlc(&gdd.total_zeros_2x2_vlc_tables[i]);
  }
  for( i=0; i<7; i++)
  {
    destroy_vlc(&gdd.total_zeros_2x4_vlc_tables[i]);
  }
  for( i=0; i<7; i++)
  {
    destroy_vlc(&gdd.run_before_vlc_tables[i]);
  }
  return LUDH264_SUCCESS;
}


// Decode the next nal unit from the buffer. The buffer must start with a start code. length is: in(input buffer size)/out(consumed bytes into the input buffer)
RetCode decode_nalu(struct rv264sequence *seq, uint8_t* data, uint32_t length)
{
  PictureDecoderData* pdd = &seq->pdd;

  nal_unit_t* nalu;
  RetCode r;

  if (!data || !length) // the caller mean the end of file(/stream), so we can finish and tide up the current decoding session
  {
    end_decoding(seq);
    return LUDH264_SUCCESS;
  }

  r = parse_nal_unit(data, length, &nalu);

  if (r != LUDH264_SUCCESS)
  {
    rvmessage("failed to parse nal unit");
    return r;
  }

  switch (nalu->nal_unit_type)
  {
    case NAL_CODED_SLICE_NON_IDR:
    {
      LUD_TRACE(TRACE_DETAIL, "Parsing a Slice");
      r = decode_slice(seq, pdd, nalu);
      if (r!=LUDH264_SUCCESS)
        rvmessage("failed to decode slice");
      break;
    }
    case NAL_CODED_SLICE_PARTITION_A:
    {
      LUD_TRACE(TRACE_INFO, "Parsing a DPA");
      break;
    }
    case NAL_CODED_SLICE_PARTITION_B:
    {
      LUD_TRACE(TRACE_INFO, "Parsing a DPB");
      break;
    }
    case NAL_CODED_SLICE_PARTITION_C:
    {
      LUD_TRACE(TRACE_INFO, "Parsing a DPC");
      break;
    }
    case NAL_CODED_SLICE_IDR:
    {
      LUD_TRACE(TRACE_DETAIL, "Parsing an IDR");
      r = decode_slice(seq, pdd, nalu);
      if (r!=LUDH264_SUCCESS)
        rvmessage("failed to decode idr slice");
      break;
    }
    case NAL_SEI:
    {
      LUD_TRACE(TRACE_DETAIL, "Parsing an SEI");
      parse_sei(nalu->bs, seq, verbose);
      break;
    }
    case NAL_SPS:
    {
      // Finish the previous pic decoding
      end_picture(seq, pdd);
      LUD_TRACE(TRACE_DETAIL, "Parsing SPS");
      struct rv264seq_parameter_set* sps;
      r = parse_sps(nalu, &sps);
      if (LUDH264_SUCCESS != r)
      {
        LUD_TRACE(TRACE_ERROR, "Error %d while parsing SPS, nalu dropped...", r);
        break;
      }
      if (seq->sps[sps->seq_parameter_set_id]!=0)
        release_sps(seq->sps[sps->seq_parameter_set_id]);
      seq->sps[sps->seq_parameter_set_id] = sps;
      break;
    }
    case NAL_PPS:
    {
      // Finish the previous pic decoding
      end_picture(seq, pdd);
      LUD_TRACE(TRACE_DETAIL, "Parsing PPS");
      struct rv264pic_parameter_set* pps;
      r = parse_pps(seq, nalu, &pps);
      if (LUDH264_SUCCESS != r)
      {
        LUD_TRACE(TRACE_ERROR, "Error %d while parsing PPS, nalu dropped...", r);
        break;
      }
      if (seq->pps[pps->pic_parameter_set_id]!=0)
        release_pps(seq->pps[pps->pic_parameter_set_id]);
      seq->pps[pps->pic_parameter_set_id] = pps;
      break;
    }
    case NAL_AUD:
      //LUD_TRACE(TRACE_INFO, "AUD NALU - not implemented yet...");
      break;
    case NAL_EOQ:
      LUD_TRACE(TRACE_INFO, "EOSEQ NALU - not implemented yet...");
      break;
    case NAL_EOS:
      LUD_TRACE(TRACE_INFO, "EOSTREAM NALU - not implemented yet...");
      break;
    case NAL_FILLER:
      parse_filler_data(nalu);
      break;
    case NAL_SPS_EXT:
      LUD_TRACE(TRACE_INFO, "SPS_EXT NALU - not implemented yet...");
      break;
    case NAL_CODED_AUX:
      LUD_TRACE(TRACE_INFO, "AUX NALU - not implemented yet...");
      break;
    default:
      LUD_TRACE(TRACE_ERROR, "NALU type %d is unknown", nalu->nal_unit_type);
      break;
  }

  release_nalu(nalu);
  return LUDH264_SUCCESS;
}

