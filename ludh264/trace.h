#ifndef _TRACE_H_
#define _TRACE_H_


extern const char *nal_unit_type_name[20];

#ifdef TRACE
#define uexpbits_ludh264(b,n) uexpbits_trace(b,n)
#define sexpbits_ludh264(b,n) sexpbits_trace(b,n)
#define bs_read_vlc_ludh264(b,v,n) bs_read_vlc_trace(b,v,n)
#else
#define uexpbits_ludh264(b,n) uexpbits(b)
#define sexpbits_ludh264(b,n) sexpbits(b)
#define bs_read_vlc_ludh264(b,v,n) bs_read_vlc(b,v)
#endif

unsigned int uexpbits_trace(struct bitbuf *bb, const char *name);
signed int sexpbits_trace(struct bitbuf *bb, const char *name);
uint8_t bs_read_vlc_trace(struct bitbuf *bs, VLCReader* vlc, const char *name);


#endif
