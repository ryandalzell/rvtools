/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/


#include <inttypes.h>
#include <string.h>
#include "common.h"
#include "system.h"
#include "decode.h"
#include "defaulttables.h"

struct rv264picture* set_ref_pic(PictureDecoderData* pdd, struct slice_header* sh)
{
  struct rv264picture* pic = pdd->pic;

  if (sh->field_pic_flag)
    use_picture_field(pic, sh->bottom_field_flag);
  else
    use_picture(pic);

  pic->ref_struct = pic->structure;

  return pic;
}

struct rv264picture* allocate_and_set_non_existing_ref_pic(int PicOrderCnt, int TopFieldOrderCnt, int BottomFieldOrderCnt, int frame_num, int ref_struct, unsigned int bottom_field_flag)
{
  struct rv264picture* pic;
  pic = (struct rv264picture *)rvalloc(NULL, sizeof(*pic), 1);

  pic->field_use_count[bottom_field_flag] = 0;
  pic->field_use_count[1-bottom_field_flag] = -1;
  pic->dec_data_use_count = -1;

  pic->PicOrderCnt = PicOrderCnt;
  pic->FieldOrderCnt[0] = TopFieldOrderCnt;
  pic->FieldOrderCnt[1] = BottomFieldOrderCnt;
  pic->frame_num = frame_num;
  pic->ref_struct = ref_struct;
  pic->non_existing_flag = 1;

  return pic;
}


void init_mb_attr(struct rv264macro* mb_attr, unsigned int nb)
{
  unsigned int i;
  while (nb > 15)
  {
    for (i = 0; i < 16; ++i)
    {
      mb_attr->slice_num = -1;
      mb_attr++;
    }
    nb -= 16;
  }
  for (i = 0; i < nb; ++i)
  {
    mb_attr->slice_num = -1;
    mb_attr++;
  }
}

// Allocate necessary data for a picture
struct rv264picture* alloc_picture(PictureDecoderData* pdd, struct slice_header* sh)
{
  struct rv264picture* pic;

  unsigned int PicSizeInMbs = sh->PicSizeInMbs;
  unsigned int PicWidthInMbs = sh->sps->PicWidthInMbs;
  unsigned int field_pic_flag = sh->field_pic_flag;
  unsigned int FrameSizeInMbs = PicSizeInMbs << (field_pic_flag!=0);
  unsigned int cfidc = sh->sps->chroma_format_idc;
  unsigned int chroma_mb_size = MbSizeC[cfidc];
  unsigned int bottom_field_flag = sh->bottom_field_flag;


  // Pic structure alloc
  pic = (struct rv264picture *)rvalloc(NULL, sizeof(*pic), 1);

  // Set ref counter to -1 (unsused). When set to 0 (memset above) it means it used 1 time.
  if (field_pic_flag)
    pic->field_use_count[1-bottom_field_flag] = -1;
  pic->dec_data_use_count = -1;

  // Pic decoded data alloc
  pic->Y = (pixel_t *)rvalloc(NULL, ((256+chroma_mb_size*2)*FrameSizeInMbs + PicWidthInMbs*16 )*sizeof(pixel_t), 0); // I allocate 1 more line here (+ PicWidthInMbs*16) because some pixel transforms function process 2 lines at a time (when using SIMD instructions)
  pic->C[0] = pic->Y + 256 * FrameSizeInMbs;
  pic->C[1] = pic->C[0] + chroma_mb_size * FrameSizeInMbs;

  // Pic mb data alloc
  pic->field_data[0].mb_attr = pic->field_data[1].mb_attr = (struct rv264macro *)
    rvalloc(NULL, ((256+chroma_mb_size*2)*sizeof(*pic->field_data[0].data)+sizeof(*pic->field_data[0].mb_attr))*FrameSizeInMbs, 0);
  pic->field_data[0].data = pic->field_data[1].data =
    (int16_t*)((uint8_t*)pic->field_data[0].mb_attr + FrameSizeInMbs*sizeof(*pic->field_data[0].mb_attr));

  init_mb_attr(pic->field_data[0].mb_attr, FrameSizeInMbs);


  // Slice pointers alloc
  // We need to do 2 separate mallocs because if the array is too small it can be reallocated while decoding...
  // The second malloc is done when we have a field picture (see bellow)
  pic->field_sh[0] = pic->field_sh[1] = (struct slice_header **)rvalloc(NULL, sizeof(struct slice_header *) * pdd->max_num_of_slices, 0);

  // When dealing with fields, just need to add an offset for the second field
  if (field_pic_flag)
  {
    pic->field_data[1].mb_attr += PicSizeInMbs;
    pic->field_data[1].data += PicSizeInMbs*(256+chroma_mb_size*2);
    // the second array of slices headers will be allocated somewhere else...
    pic->structure = sh->bottom_field_flag+1; // the structure may be updated if decoding the second field of a pair
  }
  else
  {
    pic->structure = 7;
  }

  pic->frame_num = sh->frame_num;

  return pic;
}

// This function releases reference pictures of the given field (pictures that are in RefPicList[0..1]
// It may looks dangerous to release (potentially free) the pictures in the list without actually freeing the list itself.
// Well it is OK because: after the decoding process of the current picture those lists are only used for co-located pic (see
// inter prediction chapter in the spec), and what is used in the list is the pointer of the pic (see MapColToList0 function)
// in order to identify a picture uniquely. The pointed picture is not actually accessed. Moreover the pointed picture must also
// be present in the currently decoded picture, so it cannot have been freed.
void release_picture_refpics(struct rv264picture* pic, unsigned int bottom_field_flag)
{
  struct slice_header** s = pic->field_sh[bottom_field_flag];
  unsigned int i, j;

  for (i = 0; i <= pic->slice_num[bottom_field_flag]; ++i)
    if (s[i]->RefPicList[0]) // list 1 is on the same buffer
    {
      for (j=0; j<s[i]->num_ref_idx_active[0]; j++)
        if (s[i]->field_pic_flag)
          release_picture_field(s[i]->RefPicList[0][j].ref_pic, s[i]->RefPicList[0][j].parity);
        else
          release_picture(s[i]->RefPicList[0][j].ref_pic);


      if (s[i]->RefPicList[1])
        for (j=0; j<s[i]->num_ref_idx_active[1]; j++)
        {
          if (s[i]->field_pic_flag)
            release_picture_field(s[i]->RefPicList[1][j].ref_pic, s[i]->RefPicList[1][j].parity);
          else
            release_picture(s[i]->RefPicList[1][j].ref_pic);
        }
    }
}

// Check if some part (or whole) of the picture can be free'd and do it !
void check_free_picture(struct rv264picture* pic)
{
  unsigned int i;
  struct slice_header** sh;

  LUD_DEBUG_ASSERT(pic->dec_data_use_count>=-1);
  // This is a bit tricky: we actually increment this counter so that we are sure that the picture structure will NOT be free'd in the middle of the process
  //  it could indeed happen otherwise: all field counter are set to -1 when the second field was referencing the first one of the same pic !
  pic->dec_data_use_count++;

  // if 2 different mallocs for each field data
  if (pic->field_sh[0] != pic->field_sh[1])
  {
    if (pic->field_sh[0] && pic->field_use_count[0] < 0)
    {
      sh = pic->field_sh[0];
      rvfree(sh[0]->MbToSliceGroupMap);
      pic->field_sh[0] = 0; // set to 0 so that recursion will not double free...
      for (i = 0; i <= pic->slice_num[0]; ++i)
        release_slice_header(sh[i]);
      rvfree(sh);
    }
    if (pic->field_sh[1] && pic->field_use_count[1] < 0)
    {
      sh = pic->field_sh[1];
      rvfree(sh[0]->MbToSliceGroupMap);
      pic->field_sh[1] = 0; // set to 0 so that recursion will not double free...
      for (i = 0; i <= pic->slice_num[1]; ++i)
        release_slice_header(sh[i]);
      rvfree(sh);
    }
  }
  else
  {
    if (pic->field_sh[0] && pic->field_use_count[0] < 0 && pic->field_use_count[1] < 0)
    {
      sh = pic->field_sh[0];
      rvfree(sh[0]->MbToSliceGroupMap);
      pic->field_sh[0] = pic->field_sh[1] = 0; // set to 0 so that recursion will not double free...
      for (i = 0; i <= pic->slice_num[0]; ++i)
        release_slice_header(sh[i]);
      rvfree(sh);
    }
  }

  pic->dec_data_use_count--; // see comment at the beginning of this function (increment of the same variable)

  if (pic->field_use_count[0] < 0 && pic->field_use_count[1] < 0 && pic->dec_data_use_count < 0)
  {
    rvfree(pic->Y);
    rvfree(pic->field_data[0].mb_attr);

    rvfree(pic);
  }
}

void use_picture(struct rv264picture* pic)
{
  pic->field_use_count[0]++;
  pic->field_use_count[1]++;
}

void use_picture_field(struct rv264picture* pic, unsigned int bottom_field_flag)
{
  pic->field_use_count[bottom_field_flag]++;
}

void use_picture_dec_data(struct rv264picture* pic)
{
  pic->dec_data_use_count++;
}

void release_picture(struct rv264picture* pic)
{
  if (pic)
  {
    //LUD_DEBUG_ASSERT(pic->field_use_count[0]>=0 && pic->field_use_count[1]>=0);
    pic->field_use_count[0]--;
    pic->field_use_count[1]--;
    check_free_picture(pic);
  }
}

void release_picture_field(struct rv264picture* pic, unsigned int bottom_field_flag)
{
  if (pic)
  {
    //LUD_DEBUG_ASSERT(pic->field_use_count[bottom_field_flag]>=0);
    pic->field_use_count[bottom_field_flag]--;
    check_free_picture(pic);
  }
}

void release_picture_field_struct(struct rv264picture* pic, unsigned int field_struct)
{
  if (pic)
  {
    if (field_struct&1)
    {
      //LUD_DEBUG_ASSERT(pic->field_use_count[0]>=0);
      pic->field_use_count[0]--;
    }
    if (field_struct&2)
    {
      //LUD_DEBUG_ASSERT(pic->field_use_count[1]>=0);
      pic->field_use_count[1]--;
    }

    check_free_picture(pic);
  }
}

void release_picture_dec_data(struct rv264picture* pic)
{
  if (pic)
  {
    //LUD_DEBUG_ASSERT(pic->dec_data_use_count>=0);
    pic->dec_data_use_count--;
    check_free_picture(pic);
  }
}
