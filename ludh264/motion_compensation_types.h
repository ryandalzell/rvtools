/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/


#ifndef MOTION_COMPENSATION_TYPES_H_
#define MOTION_COMPENSATION_TYPES_H_


typedef void (*qpel_func_t)(int partHeight4, const pixel_t* src, unsigned int src_dy, pixel_t* dst, unsigned int dst_dy, int clipmax);
extern const qpel_func_t qpel_func_c[4][4][4]; // [partWidth4-1][xFrac][yFrac]
extern const qpel_func_t qpel_func_sse2[4][4][4]; // [partWidth4-1][xFrac][yFrac]

typedef void (*hpel_func_t)(unsigned int partHeight4, unsigned int xFrac, unsigned int yFrac,
    const pixel_t* src, unsigned int src_dy, pixel_t* dst, unsigned int dst_dy);
extern const hpel_func_t hpel_func_c[3][4][2][2]; // [cfidc-1][partWidth4-1][xFrac!=0][yFrac!=0]
extern const hpel_func_t hpel_func_sse2[3][4][2][2]; // [cfidc-1][partWidth4-1][xFrac!=0][yFrac!=0]

typedef void (*wimp_func)(const pixel_t src[], unsigned int src_dy, pixel_t dst[], unsigned int dst_dy,
    int clip, int o, int w, int logWD);
extern const wimp_func wimp[12][2]; // [partSize][logWDNonNull]

typedef void (*wibp_func)(const pixel_t src0[], const pixel_t src1[], unsigned int src_dy, pixel_t dst[],
    unsigned int dst_dy, int clip, int o0, int w0, int o1, int w1, int logWD);
extern const wibp_func wibp[12]; // [partSize]

typedef void (*wibpi_func)(const pixel_t src0[], const pixel_t src1[], unsigned int src_dy, pixel_t dst[],
    unsigned int dst_dy, int clip, int w0, int w1);
extern const wibpi_func wibpi[12]; // [partSize]

typedef void (*dwsp_func)(const pixel_t src0[], const pixel_t src1[], unsigned int src_dy, pixel_t dst[],
    unsigned int dst_dy);

extern const dwsp_func dwsp[12];



#endif /* MOTION_COMPENSATION_TYPES_H_ */
