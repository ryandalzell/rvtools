/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/


#ifndef MOTION_VECTOR_H_
#define MOTION_VECTOR_H_

#include "common.h"

typedef enum
{
  One_To_One, Frm_To_Fld, Fld_To_Frm,
} VERT_MV_SCALE_TYPE;

void reset_one_mv_cache_entry(int16_t mv_cache[2][30][2], int8_t refIdx_cache[2][30], unsigned int idx,
    int repeat_mv, unsigned int need_L1, int refIdxVal);
void fills_in_one_mv_cache_entry(int16_t mv_cache[2][30][2], int8_t refIdx_cache[2][30],
    unsigned int idx, struct rv264macro* mb_attr, unsigned int src_idx, int repeat_mv,
    unsigned int need_L1, unsigned int scaler_biased);
void fills_in_mv_cache_for_nonmbaff_slice(struct rv264macro* curr_mb_attr, unsigned int PicWidthInMbs,
    int16_t mv_cache[2][30][2], int8_t refIdx_cache[2][30], unsigned int need_L1, unsigned int simple);
void fills_in_mv_cache_for_mbaff_slice(struct rv264macro* curr_mb_attr, unsigned int curr_is_bot, unsigned int curr_is_field,
    unsigned int PicWidthInMbs, int16_t mv_cache[2][30][2], int8_t refIdx_cache[2][30], unsigned int need_L1,
    unsigned int simple);
void fills_in_mv_cache(struct rv264macro* curr_mb_attr, unsigned int curr_is_bot, unsigned int curr_is_field,
    unsigned int PicWidthInMbs, int16_t mv_cache[2][30][2], int8_t refIdx_cache[2][30], unsigned int need_L1,
    unsigned int simple, unsigned int MbaffFrameFlag);
void mv_cache_write_back(struct rv264macro* curr_mb_attr, unsigned int need_L1, int16_t mv_cache[2][30][2],
    int8_t refIdx_cache[2][30]);
void mv_cache_fill_const(struct rv264macro* curr_mb_attr, unsigned int LX_mask, int refIdxLX, int16_t mv[2]);
void derive_mbAddrD_mv_cache(int16_t mvLXD[2], int8_t* refIdxLXD, struct rv264macro* mb_attr,
    unsigned int src_idx, unsigned int LX, unsigned int scaler_biased);
void fills_in_mbAddrD_mv_cache(struct rv264macro* curr_mb_attr, unsigned int mb_row, unsigned int PicWidthInMbs,
    unsigned int r6x5idx, unsigned int LX, int16_t mvLXD[2], int8_t* refIdxLXD);
void median_luma_motion_vector_pred(unsigned int partWidth, unsigned int partHeight,
    unsigned int mbPartIdx, unsigned int r6x5idx, unsigned int LX, unsigned int MbaffFrameFlag, struct rv264macro* curr_mb_attr,
    unsigned int mb_row, unsigned int PicWidthInMbs, int16_t mvLXA[2], int16_t mvLXB[2], int16_t mvLXC_orig[2],
    int16_t mvLXD[2], int8_t refIdxLXA, int8_t refIdxLXB, int8_t refIdxLXC, int8_t refIdxLXD, int8_t refIdxLX,
    int16_t mvpLX[2]);
void median_luma_motion_vector_pred_simple(int16_t mvLXA[2], int16_t mvLXB[2], int16_t mvLXC[2],
    int16_t mvLXD[2], int8_t refIdxLXA, int8_t refIdxLXB, int8_t refIdxLXC, int8_t refIdxLXD, int8_t refIdxLX,
    int16_t mvpLX[2]);
int MinPositive(int a, int b, int c);
void derive_colPic(RefListEntry* RefPicList1, int PicOrderCnt, unsigned int mb_row,
    unsigned int field_pic_flag, unsigned int mb_field_decoding_flag, unsigned int MbaffFrameFlag,
    struct rv264picture** colPic_, unsigned int* field, unsigned int* topAbsDiffPOC, unsigned int* bottomAbsDiffPOC,
    unsigned int* CurrPic_PicCodingStruct, unsigned int* ColPic_PicCodingStruct);
void derive_colocated_4x4_submbpart(struct rv264picture* colPic, unsigned int field,
    unsigned int CurrPic_PicCodingStruct, unsigned int ColPic_PicCodingStruct, unsigned int topAbsDiffPOC,
    unsigned int bottomAbsDiffPOC, unsigned int block_row, unsigned int block_col, unsigned int mb_idx,
    unsigned int mb_row, unsigned int mb_col, unsigned int PicWidthInMbs, unsigned int mb_field_decoding_flag,
    unsigned int bottom_field_flag, unsigned int* mbIdxCol, unsigned int* block_rowCol,
    VERT_MV_SCALE_TYPE* vertMvScale, int16_t mvCol[2], int* refIdxCol, RefListEntry** refPicCol, unsigned int* refPicCol_field);
int MapColToList0(struct rv264picture* colPic, RefListEntry* refPicCol, RefListEntry* RefPicList0, int refIdxCol,
    VERT_MV_SCALE_TYPE vertMvScale, unsigned int field_pic_flag, unsigned int bottom_field_flag,
    unsigned int mb_field_decoding_flag, unsigned int mb_parity, unsigned int refPicCol_field, int16_t mvCol[2]);
void mv_pred_P_Skip_mb(int16_t mv_cache[2][30][2], int8_t refIdx_cache[2][30]);
int mv_pred_B_SKIP_B_Direct_init_mechanism(int16_t mv_cache[2][30][2], int8_t refIdx_cache[2][30],
    unsigned int mb_row, unsigned int field_pic_flag, unsigned int mb_field_decoding_flag, int PicOrderCnt,
    unsigned int MbaffFrameFlag, unsigned int direct_spatial_mv_pred_flag,
    RefListEntry* RefPicList[2], struct rv264picture** colPic, unsigned int* field, unsigned int* topAbsDiffPOC,
    unsigned int* bottomAbsDiffPOC, unsigned int* CurrPic_PicCodingStruct, unsigned int* ColPic_PicCodingStruct,
    int refIdxL[2], int16_t mvLX[2][2]);
void mv_pred_B_SKIP_B_Direct_iterate(unsigned int direct_spatial_mv_pred_flag,
    unsigned int direct_8x8_inference_flag, int16_t mv_cache[2][30][2], int8_t refIdx_cache[2][30],
    unsigned int scan4x4idx, unsigned int mb_idx, unsigned int mb_row, unsigned int mb_col,
    unsigned int mb_field_decoding_flag, unsigned int bottom_field_flag, unsigned int field_pic_flag,
    unsigned int PicWidthInMbs, RefListEntry* RefPicList[2], int FieldOrderCnt[2],
    struct rv264picture* colPic, unsigned int field, unsigned int topAbsDiffPOC, unsigned int bottomAbsDiffPOC,
    unsigned int CurrPic_PicCodingStruct, unsigned int ColPic_PicCodingStruct, int refIdxL[2], int16_t mvLX[2][2]);
void mv_pred_B_Skip_B_Direct_16x16_mb(int16_t mv_cache[2][30][2], int8_t refIdx_cache[2][30],
    unsigned int mb_idx, unsigned int mb_row, unsigned int mb_col, unsigned int direct_spatial_mv_pred_flag,
    unsigned int direct_8x8_inference_flag, RefListEntry* RefPicList[2], unsigned int field_pic_flag,
    unsigned int bottom_field_flag, unsigned int mb_field_decoding_flag, unsigned int MbaffFrameFlag,
    int FieldOrderCnt[2], int PicOrderCnt, unsigned int PicWidthInMbs,
    uint32_t* partWidths, uint32_t* partHeights);
void mv_pred_P_B_mb(struct rv264macro* curr_mb_attr, mb_type_t mb_type, unsigned int numMbPart,
    unsigned int has_sub_part, unsigned int is_B_mb, unsigned int PicWidthInMbs, unsigned int MbaffFrameFlag,
    int16_t mv_cache[2][30][2], int8_t refIdx_cache[2][30], unsigned int mb_idx, unsigned int mb_row,
    unsigned int mb_col, unsigned int field_pic_flag, unsigned int mb_field_decoding_flag,
    unsigned int direct_spatial_mv_pred_flag,
    unsigned int bottom_field_flag, unsigned int direct_8x8_inference_flag,
    RefListEntry* RefPicList[2], int PicOrderCnt, int FieldOrderCnt[2],
    uint32_t* partWidths, uint32_t* partHeights);
void mv_pred_P_B_mb_simple(struct rv264macro* curr_mb_attr, mb_type_t mb_type, unsigned int numMbPart,
    unsigned int has_sub_part, unsigned int is_B_mb, int16_t mv_cache[2][30][2], int8_t refIdx_cache[2][30],
    unsigned int MbaffFrameFlag, unsigned int mb_row, unsigned int PicWidthInMbs,
    uint32_t* partWidths, uint32_t* partHeights);
void print_mb_mv_pred(struct rv264macro* curr_mb_attr, mb_type_t mb_type, unsigned int mb_idx, unsigned int mb_row,
    unsigned int mb_col);
void derive_motion_vectors_internal(struct rv264macro* curr_mb_attr, mb_type_t mb_type, unsigned int mb_idx, unsigned int mb_row,
    unsigned int mb_col, unsigned int PicWidthInMbs, unsigned int mb_field_decoding_flag, unsigned int MbaffFrameFlag,
    unsigned int direct_spatial_mv_pred_flag, unsigned int direct_8x8_inference_flag,
    RefListEntry* RefPicList[2], unsigned int field_pic_flag, unsigned int bottom_field_flag,
    int FieldOrderCnt[2], int PicOrderCnt);
void derive_motion_vectors(PictureDecoderData* pdd);

#endif /* MOTION_VECTOR_H_ */
