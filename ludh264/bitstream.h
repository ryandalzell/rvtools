/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/

#ifndef __BITSTREAM_H__
#define __BITSTREAM_H__

#include "intmath.h"
#include "bitstream_types.h"

int init_vlc(VLCReader* vlc, unsigned int nb_of_symbols, const uint8_t* symbol_table, const uint8_t* code_table, const uint8_t* length_table, unsigned int nb_of_bits);
int destroy_vlc(VLCReader* vlc);

uint8_t bs_read_me(struct bitbuf *bs, part_type_t prediction_mode, unsigned int chroma_format_idc);
uint8_t bs_read_vlc(struct bitbuf *bs, VLCReader* vlc);

#endif //__BITSTREAM_H__
