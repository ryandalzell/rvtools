/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/


#ifndef __DECODE_H__
#define __DECODE_H__

#include "rvh264.h"
#include "common.h"
#include "syntax.h"
#include "bitstream_types.h"


typedef struct
{
  /* coeff_token VLC tables
   * 0 coeff_token 0<=nC<2
   * 1 coeff_token 2<=nC<4
   * 2 coeff_token 4<=nC<8
   * 3 coeff_token nC==-1
   * 4 coeff_token nC==-2
   * NOTE: coeff_token for 8 <= nC is not a VLC table, it is fixed length (6bits)
   */
  VLCReader coeff_token_vlc_tables[5];

  // total_zeros VLC tables
  VLCReader total_zeros_4x4_vlc_tables[15];
  VLCReader total_zeros_2x2_vlc_tables[3];
  VLCReader total_zeros_2x4_vlc_tables[7];

  // run_before VLC tables
  VLCReader run_before_vlc_tables[7];
}
GlobalDecoderData;

extern GlobalDecoderData gdd;




RetCode decoder_init(struct rv264sequence *seq);
RetCode decoder_reset(struct rv264sequence *seq);
RetCode decoder_destroy(struct rv264sequence *seq);
RetCode decode_nalu(struct rv264sequence *seq, uint8_t* data, uint32_t length);
struct rv264picture* get_pic(DecodedPictureBuffer *ob);
void decode_image_init();
void decode_image(struct rv264sequence *seq, struct rv264picture* pic, unsigned int bottom_field_flag);
void decode_image_end();
void decode_image_destroy();
void filter_image(struct rv264picture* pic, unsigned int bottom_field_flag);
void init_dpb(DecodedPictureBuffer *ob);
void add_image_to_dpb(DecodedPictureBuffer *ob, struct rv264picture* image);
void flush_dpb(DecodedPictureBuffer *ob);
void close_dpb(DecodedPictureBuffer *ob);


#endif //__DECODE_H__
