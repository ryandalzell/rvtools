/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/




#include "common.h"
#include "decode.h"
#include "defaulttables.h"
#include "intmath.h"

#define IS_INTRA_MB(m)        ((m)->mb_type <= SI)
#define IS_INTER_MB(m)        ((m)->mb_type >  SI)
#define IS_SI_SP_SLICE_MB(m)  (sh[(m)->slice_num]->slice_type_modulo5 >= SLICE_SP)




static const uint8_t indexA_to_alpha[52] =
{
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 5, 6, 7, 8, 9, 10, 12, 13,
  15, 17, 20, 22, 25, 28, 32, 36, 40, 45, 50, 56, 63, 71, 80, 90, 101, 113, 127, 144, 162, 182, 203, 226, 255, 255
};


static const uint8_t indexB_to_beta[52] =
{
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4,
  6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18
};


static const uint8_t bS_indexA_to_tc0[3][52] =
{
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 6, 6, 7, 8, 9, 10, 11, 13 },
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 4, 4, 5, 5, 6, 7, 8, 8, 10, 11, 12, 13, 15, 17 },
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 6, 6, 7, 8, 9, 10, 11, 13, 14, 16, 18, 20, 23, 25 }
};



#define comp(a, b) (refPic_p[a] == refPic_q[b] && field_p[a] == field_q[b])
// i and j are set to 0 or 1 (num of the list used). They are derived so that RefPic(i) = RefPic(j)
static int same_refpics(int8_t* refIdxLX_p,  int8_t* refIdxLX_q,
    RefListEntry* RefPicList_p[2], RefListEntry* RefPicList_q[2],
    unsigned int MbaffFrameFlag, unsigned int mb_field_decoding_flag,
    unsigned int* i, unsigned int* j, unsigned int* sameRefPic)
{
  struct rv264picture* refPic_p[2] = {0, 0};
  struct rv264picture* refPic_q[2] = {0, 0};
  uint8_t field_p[2] = {0, 0};
  uint8_t field_q[2] = {0, 0};
  int k;

  if (!MbaffFrameFlag || !mb_field_decoding_flag)
  {
    for (k = 0; k < 2; ++k)
    {
      if (*refIdxLX_p >= 0)
      {
        refPic_p[k] = RefPicList_p[k][*refIdxLX_p].ref_pic;
        field_p[k] = RefPicList_p[k][*refIdxLX_p].parity;
      }
      refIdxLX_p += 16;
    }
    for (k = 0; k < 2; ++k)
    {
      if (*refIdxLX_q >= 0)
      {
        refPic_q[k] = RefPicList_q[k][*refIdxLX_q].ref_pic;
        field_q[k] = RefPicList_q[k][*refIdxLX_q].parity;
      }
      refIdxLX_q += 16;
    }
  }
  else
  {
    for (k = 0; k < 2; ++k)
    {
      if (*refIdxLX_p >= 0)
      {
        refPic_p[k] = RefPicList_p[k][(*refIdxLX_p)>>1].ref_pic;
        field_p[k] = (*refIdxLX_p) % 2;
      }
      refIdxLX_p += 16;
    }
    for (k = 0; k < 2; ++k)
    {
      if (*refIdxLX_q >= 0)
      {
        refPic_q[k] = RefPicList_q[k][(*refIdxLX_q)>>1].ref_pic;
        field_q[k] = (*refIdxLX_q) % 2;
      }
      refIdxLX_q += 16;
    }
  }


  if (comp(0, 0) && comp(1, 1))
  {
    *i = refPic_p[0] == 0;
    *j = *i;
    *sameRefPic = refPic_p[0] == refPic_p[1] && field_p[0] == field_p[1];
    return 1;
  }
  else if (comp(0, 1) && comp(1, 0))
  {
    *i = refPic_p[0] == 0;
    *j = 1-*i;
    *sameRefPic = 0; // ref pics cannot be the same for both list, otherwise we would have gone through the previous if block !
    return 1;
  }

  return 0;

}

// test if Inter mb satisfy condition on page 189 of H264.2005/03 standard
static int inter_bS_test(unsigned int mixedModeEdgeFlag, int8_t* refIdxLX_p,  int8_t* refIdxLX_q,
    RefListEntry* RefPicList_p[2], RefListEntry* RefPicList_q[2],
    int16_t* mvL_p, int16_t* mvL_q, unsigned int MbaffFrameFlag, unsigned int mb_field_decoding_flag)
{
  if (mixedModeEdgeFlag)
    return 1;

  unsigned int nbOfMv_p = ((*refIdxLX_p) >= 0) + ((*(refIdxLX_p+16)) >= 0);
  unsigned int nbOfMv_q = ((*refIdxLX_q) >= 0) + ((*(refIdxLX_q+16)) >= 0);

  //LUD_DEBUG_ASSERT(nbOfMv_p>0 || nbOfMv_q>0);

  if (nbOfMv_p != nbOfMv_q)
    return 1;

  unsigned int i, j, sameRefPic;

  if (!same_refpics(refIdxLX_p, refIdxLX_q,
      RefPicList_p, RefPicList_q,
      MbaffFrameFlag, mb_field_decoding_flag,
      &i, &j, &sameRefPic))
    return 1;

  if (nbOfMv_p == 1)
  {
    mvL_p += 32 *i;
    mvL_q += 32 *j;
    if (im_abs(mvL_p[0]-mvL_q[0]) >= 4 || im_abs(mvL_p[1]-mvL_q[1]) >= (4>>mb_field_decoding_flag) )
      return 1;
    else
      return 0;
  }

  // 2 motion vectors are used to predict both (sub) partitions
  if (!sameRefPic)
  {
    int16_t* mv_p;
    int16_t* mv_q;
    mv_p = mvL_p + 32 *i;
    mv_q = mvL_q + 32 *j;
    // This part is not really clear: however it seems that it is a "OR" (the absolute diff is >=4 for one couple of vectors, not necessarily both)
    if (im_abs(mv_p[0]-mv_q[0]) >= 4 || im_abs(mv_p[1]-mv_q[1]) >= (4>>mb_field_decoding_flag) )
      return 1;
    else
    {
      mv_p = mvL_p + 32 *(1-i);
      mv_q = mvL_q + 32 *(1-j);
      if (im_abs(mv_p[0]-mv_q[0]) >= 4 || im_abs(mv_p[1]-mv_q[1]) >= (4>>mb_field_decoding_flag) )
        return 1;
      else
        return 0;
    }
  }

  // 2 motion vectors but the same reference picture is used for both vectors (and for both (sub) partitions)
  if (im_abs(mvL_p[0]-mvL_q[0]) >= 4 || im_abs(mvL_p[1]-mvL_q[1]) >= (4>>mb_field_decoding_flag)
  || (im_abs(mvL_p[0+32]-mvL_q[0+32]) >= 4 || im_abs(mvL_p[1+32]-mvL_q[1+32]) >= (4>>mb_field_decoding_flag)))
  {
    if (im_abs(mvL_p[0]-mvL_q[0+32]) >= 4 || im_abs(mvL_p[1]-mvL_q[1+32]) >= (4>>mb_field_decoding_flag)
    || (im_abs(mvL_p[0+32]-mvL_q[0]) >= 4 || im_abs(mvL_p[1+32]-mvL_q[1]) >= (4>>mb_field_decoding_flag)))
      return 1;
  }

  return 0;
}

static void fills_in_bS_row(uint8_t bSr[4], unsigned int either_or_both_is_intra, unsigned int either_or_both_is_SI_SP,
                                   uint8_t* upper_coeffs, uint8_t* curr_coeffs, int8_t* refIdxLX_p, int8_t* refIdxLX_q,
                                   RefListEntry* RefPicList_p[2], RefListEntry* RefPicList_q[2],
                                   int16_t* mvL_p, int16_t* mvL_q, unsigned int mb_field_decoding_flag, unsigned int mixedModeEdgeFlag,
                                   unsigned int MbaffFrameFlag)
{
  int i;

  if(either_or_both_is_intra || either_or_both_is_SI_SP)
  {
    bSr[0] = bSr[1] = bSr[2] = bSr[3] = 3;
  }
  else // we have 2 inter macroblocks
  {
    for (i=0; i<4; i++)
    {
      if (curr_coeffs[i] || upper_coeffs[i])
      {
        bSr[i] = 2;
      }
      else if (inter_bS_test(mixedModeEdgeFlag, refIdxLX_p+i, refIdxLX_q+i, RefPicList_p, RefPicList_q, mvL_p+i*2, mvL_q+i*2, MbaffFrameFlag, mb_field_decoding_flag))
      {
        bSr[i] = 1;
      }
      else
      {
        bSr[i] = 0;
      }
    }
  }
}

static void fills_in_bS_col(uint8_t bSr[4], unsigned int either_or_both_is_intra, unsigned int either_or_both_is_SI_SP,
                                   uint8_t* left_coeffs, uint8_t* curr_coeffs, int8_t* refIdxLX_p, int8_t* refIdxLX_q,
                                   RefListEntry* RefPicList_p[2], RefListEntry* RefPicList_q[2],
                                   int16_t* mvL_p, int16_t* mvL_q, unsigned int mb_field_decoding_flag, unsigned int MbaffFrameFlag)
{
  int i;

  if (either_or_both_is_intra || either_or_both_is_SI_SP)
  {
    bSr[0] = bSr[1] = bSr[2] = bSr[3] = 3;
  }
  else
  {
    for (i=0; i<4; i++)
    {
      if (curr_coeffs[i*4] || left_coeffs[i*4])
      {
        bSr[i] = 2;
      }
      else if (inter_bS_test(0, refIdxLX_p+i*4, refIdxLX_q+i*4, RefPicList_p, RefPicList_q, mvL_p+i*8, mvL_q+i*8, MbaffFrameFlag, mb_field_decoding_flag))
      {
        bSr[i] = 1;
      }
      else
      {
        bSr[i] = 0;
      }
    }
  }
}

// this function is used when curr_is_field ^ left_is_field == 0 == mixedModeEdgeFlag
static void fills_in_bS_col_mbaff2(uint8_t bSr[8], unsigned int either_or_both_is_intra, unsigned int either_or_both_is_SI_SP,
                                   uint8_t* left_coeffs, uint8_t* curr_coeffs, int8_t* refIdxLX_p, int8_t* refIdxLX_q,
                                   RefListEntry* RefPicList_p[2], RefListEntry* RefPicList_q[2],
                                   int16_t* mvL_p, int16_t* mvL_q, unsigned int mb_field_decoding_flag)
{
  int i;

  if (either_or_both_is_intra || either_or_both_is_SI_SP)
  {
    bSr[0] = bSr[1] = bSr[2] = bSr[3] = bSr[4] = bSr[5] = bSr[6] = bSr[7] = 3;
  }
  else
  {
    for (i=0; i<4; i++)
    {
      if (curr_coeffs[i*4] || left_coeffs[i*4])
      {
        bSr[2*i] = bSr[2*i+1] = 2;
      }
      else if (inter_bS_test(0, refIdxLX_p+i*4, refIdxLX_q+i*4, RefPicList_p, RefPicList_q, mvL_p+i*8, mvL_q+i*8, 1, mb_field_decoding_flag))
      {
        bSr[2*i] = bSr[2*i+1] = 1;
      }
      else
      {
        bSr[2*i] = bSr[2*i+1] = 0;
      }
    }
  }
}

// this function is used when curr_is_field && !left_is_field => mixedModeEdgeFlag = 1
static void fills_in_bS_col_mbaff0(uint8_t bSr[4],
                                         uint8_t* left_coeffs, uint8_t* curr_coeffs, int8_t* refIdxLX_p, int8_t* refIdxLX_q,
                                         RefListEntry* RefPicList_p[2], RefListEntry* RefPicList_q[2],
                                         int16_t* mvL_p, int16_t* mvL_q)
{
  int i;

  for (i=0; i<4; i++)
  {
    if (curr_coeffs[(i>>1)*4] || left_coeffs[i*4])
    {
      bSr[i] = 2;
    }
    else if (inter_bS_test(1, refIdxLX_p+i*4, refIdxLX_q+(i>>1)*4, RefPicList_p, RefPicList_q, mvL_p+i*8, mvL_q+(i>>1)*8, 1, 0))
    {
      bSr[i] = 1;
    }
    else
    {
      bSr[i] = 0;
    }
  }
}

// this function is used when !curr_is_field && left_is_field => mixedModeEdgeFlag = 1
// left_offset is equal to 0 (left top mb) or 1 (left bottom mb)
static void fills_in_bS_col_mbaff1(uint8_t bSr[8], unsigned int left_offset,
                                          uint8_t* left_coeffs, uint8_t* curr_coeffs, int8_t* refIdxLX_p, int8_t* refIdxLX_q,
                                          RefListEntry* RefPicList_p[2], RefListEntry* RefPicList_q[2],
                                          int16_t* mvL_p, int16_t* mvL_q)
{
  int i;

  for (i=0; i<4; i++)
  {
    if (curr_coeffs[i*4] || left_coeffs[(i>>1)*4])
    {
      bSr[(i<<1)+left_offset] = 2;
    }
    else if (inter_bS_test(1, refIdxLX_p+(i>>1)*4, refIdxLX_q+i*4, RefPicList_p, RefPicList_q, mvL_p+(i>>1)*8, mvL_q+i*8, 1, 0))
    {
      bSr[(i<<1)+left_offset] = 1;
    }
    else
    {
      bSr[(i<<1)+left_offset] = 0;
    }
  }
}



// vertical edges:   bS[0][col][row]
// horizontal edges: bS[1][row][col]
// bS array should be aligned on a 4 bytes bondary
static void fills_in_bS_nonmbaff(uint8_t bS[2][4][4], int8_t qPav[3][3], struct rv264macro* curr_mb_attr, unsigned int mb_row, unsigned int mb_col,
                                        int PicWidthInMbs, unsigned int disable_deblocking_filter_idc, struct slice_header** sh,
                                        unsigned int slice_num, RefListEntry* RefPicList[2], unsigned int field_pic_flag,
                                        unsigned int transform_size_8x8_flag, unsigned int cfidc)
{
  uint8_t* bSr;
  unsigned int either_or_both_is_intra;
  unsigned int either_or_both_is_SI_SP;
  struct rv264macro* upper_mb_attr;
  struct rv264macro* left_mb_attr;

  qPav[0][0] = curr_mb_attr->QPy;
  qPav[0][1] = curr_mb_attr->QPc[0];
  qPav[0][2] = curr_mb_attr->QPc[1];


  // fills in horizontal edges
  // 1st row
  bSr = bS[1][0];
  upper_mb_attr = curr_mb_attr-PicWidthInMbs;
  if (mb_row > 0 && (!disable_deblocking_filter_idc || upper_mb_attr->slice_num == slice_num))
  {
    qPav[2][0] = (upper_mb_attr->QPy + curr_mb_attr->QPy + 1) >> 1; // Y
    qPav[2][1] = (upper_mb_attr->QPc[0] + curr_mb_attr->QPc[0] + 1) >> 1; // Cb
    qPav[2][2] = (upper_mb_attr->QPc[1] + curr_mb_attr->QPc[1] + 1) >> 1; // Cr
    either_or_both_is_intra = IS_INTRA_MB(curr_mb_attr) || IS_INTRA_MB(upper_mb_attr);
    either_or_both_is_SI_SP = IS_SI_SP_SLICE_MB(curr_mb_attr) || IS_SI_SP_SLICE_MB(upper_mb_attr);
    if ( (!field_pic_flag && (either_or_both_is_intra || either_or_both_is_SI_SP )))
    {
      bSr[0] = bSr[1] = bSr[2] = bSr[3] = 4;
    }
    else
    {
      fills_in_bS_row(bSr, either_or_both_is_intra, either_or_both_is_SI_SP,
                      upper_mb_attr->TotalCoeffLuma+12, curr_mb_attr->TotalCoeffLuma,
                      &upper_mb_attr->RefIdxL[0][12], &curr_mb_attr->RefIdxL[0][0],
                      sh[upper_mb_attr->slice_num]->RefPicList, RefPicList,
                      upper_mb_attr->mv[0][12], curr_mb_attr->mv[0][0],
                      field_pic_flag, 0, 0);
    }
  }
  else // no filtering of 1st row
    bSr[0] = bSr[1] = bSr[2] = bSr[3] = 0;

  // 2nd row
  bSr += 4;
  either_or_both_is_intra = IS_INTRA_MB(curr_mb_attr);
  either_or_both_is_SI_SP = IS_SI_SP_SLICE_MB(curr_mb_attr);
  if (!transform_size_8x8_flag || cfidc > 1)
  {
    fills_in_bS_row(bSr, either_or_both_is_intra, either_or_both_is_SI_SP,
                    curr_mb_attr->TotalCoeffLuma, curr_mb_attr->TotalCoeffLuma+4,
                    &curr_mb_attr->RefIdxL[0][0], &curr_mb_attr->RefIdxL[0][4],
                    RefPicList, RefPicList,
                    curr_mb_attr->mv[0][0], curr_mb_attr->mv[0][4],
                    field_pic_flag, 0, 0);
  }
  // 3rd row
  bSr += 4;
  fills_in_bS_row(bSr, either_or_both_is_intra, either_or_both_is_SI_SP,
                  curr_mb_attr->TotalCoeffLuma+4, curr_mb_attr->TotalCoeffLuma+8,
                  &curr_mb_attr->RefIdxL[0][4], &curr_mb_attr->RefIdxL[0][8],
                  RefPicList, RefPicList,
                  curr_mb_attr->mv[0][4], curr_mb_attr->mv[0][8],
                  field_pic_flag, 0, 0);
  // 4th row
  bSr += 4;
  if (!transform_size_8x8_flag || cfidc > 1)
  {
    fills_in_bS_row(bSr, either_or_both_is_intra, either_or_both_is_SI_SP,
                    curr_mb_attr->TotalCoeffLuma+8, curr_mb_attr->TotalCoeffLuma+12,
                    &curr_mb_attr->RefIdxL[0][8], &curr_mb_attr->RefIdxL[0][12],
                    RefPicList, RefPicList,
                    curr_mb_attr->mv[0][8], curr_mb_attr->mv[0][12],
                    field_pic_flag, 0, 0);
  }

  // fills in vertical edges
  left_mb_attr = curr_mb_attr-1;
  bSr = bS[0][0];
  // 1st col
  if (mb_col>0 && (!disable_deblocking_filter_idc || left_mb_attr->slice_num == slice_num))
  {
    qPav[1][0] = (left_mb_attr->QPy + curr_mb_attr->QPy + 1) >> 1;
    qPav[1][1] = (left_mb_attr->QPc[0] + curr_mb_attr->QPc[0] + 1) >> 1;
    qPav[1][2] = (left_mb_attr->QPc[1] + curr_mb_attr->QPc[1] + 1) >> 1;
    either_or_both_is_intra = IS_INTRA_MB(curr_mb_attr) || IS_INTRA_MB(left_mb_attr);
    either_or_both_is_SI_SP = IS_SI_SP_SLICE_MB(curr_mb_attr) || IS_SI_SP_SLICE_MB(left_mb_attr);
    if ( (either_or_both_is_intra || either_or_both_is_SI_SP) )
    {
      bSr[0] = bSr[1] = bSr[2] = bSr[3] = 4;
    }
    else
    {
      fills_in_bS_col(bSr, either_or_both_is_intra, either_or_both_is_SI_SP,
                      left_mb_attr->TotalCoeffLuma+3, curr_mb_attr->TotalCoeffLuma,
                      &left_mb_attr->RefIdxL[0][3], &curr_mb_attr->RefIdxL[0][0],
                      sh[left_mb_attr->slice_num]->RefPicList, RefPicList,
                      left_mb_attr->mv[0][3], curr_mb_attr->mv[0][0],
                      field_pic_flag, 0);
    }
  }
  else // no filtering for the first col
  {
    bSr[0] = bSr[1] = bSr[2] = bSr[3] = 0;
  }

  // 2nd col
  bSr += 4;
  either_or_both_is_intra = IS_INTRA_MB(curr_mb_attr);
  either_or_both_is_SI_SP = IS_SI_SP_SLICE_MB(curr_mb_attr);
  if (!transform_size_8x8_flag || cfidc > 2)
  {
    fills_in_bS_col(bSr, either_or_both_is_intra, either_or_both_is_SI_SP,
                    curr_mb_attr->TotalCoeffLuma, curr_mb_attr->TotalCoeffLuma+1,
                    &curr_mb_attr->RefIdxL[0][0], &curr_mb_attr->RefIdxL[0][1],
                    RefPicList, RefPicList,
                    curr_mb_attr->mv[0][0], curr_mb_attr->mv[0][1],
                    field_pic_flag, 0);
  }

  // 3rd col
  bSr += 4;
  fills_in_bS_col(bSr, either_or_both_is_intra, either_or_both_is_SI_SP,
                  curr_mb_attr->TotalCoeffLuma+1, curr_mb_attr->TotalCoeffLuma+2,
                  &curr_mb_attr->RefIdxL[0][1], &curr_mb_attr->RefIdxL[0][2],
                  RefPicList, RefPicList,
                  curr_mb_attr->mv[0][1], curr_mb_attr->mv[0][2],
                  field_pic_flag, 0);

  // 4th col
  bSr += 4;
  if (!transform_size_8x8_flag || cfidc > 2)
  {
    fills_in_bS_col(bSr, either_or_both_is_intra, either_or_both_is_SI_SP,
                    curr_mb_attr->TotalCoeffLuma+2, curr_mb_attr->TotalCoeffLuma+3,
                    &curr_mb_attr->RefIdxL[0][2], &curr_mb_attr->RefIdxL[0][3],
                    RefPicList, RefPicList,
                    curr_mb_attr->mv[0][2], curr_mb_attr->mv[0][3],
                    field_pic_flag, 0);
  }
}



// this function is called with curr_mb_attr as a top mb only !
// vertical edges:   bS[0][col][row]
// horizontal edges: bS[1][row][col]
// inner_bS array should be aligned on a 8 bytes boundary !
// upper_bS array should be aligned on a 8 bytes boundary.
//  upper_bS[idx][block], idx: 1->1st row of top mb, 2->1st row of bot mb, 0->special second 1st row of top mb (It is used when upper_is_field && !curr_is_field)
// left_bS <= aligned on 8 bytes boundary. It is used only when (curr_is_field ^ left_is_field) == 1
//    int8_t left_qPav[3][16]; // [component][left mb for row]:   component: 0-Y, 1-Cb, 2-Cr; row : = actual row/2
//    int8_t upper_qPav[3][3]; // [component][upper mb for row]:   component: 0-Y, 1-Cb, 2-Cr; row : = 0-1: two 1st rows of top mb, 2: 1st row of bot mb (see upper_bS idx)
//    int8_t inner_qPav[3][2]; //  [component][top/bot] component: 0-Y, 1-Cb, 2-Cr, top/bot: 0->top, 1->bot
static void fills_in_bS_mbaff(uint8_t inner_bS[2][24], uint8_t upper_bS[3][4], uint8_t left_bS[16],
                                     int8_t inner_qPav[3][2], int8_t upper_qPav[3][3], int8_t left_qPav[3][16], struct rv264macro* curr_mb_attr,
                                     unsigned int mb_row, unsigned int mb_col, int PicWidthInMbs, unsigned int disable_deblocking_filter_idc,
                                     struct slice_header** sh, unsigned int slice_num, RefListEntry* RefPicList[2],
                                     unsigned int transform_size_8x8_flag[2], unsigned int cfidc)

{
  uint8_t* bSr;
  unsigned int either_or_both_is_intra;
  unsigned int either_or_both_is_SI_SP;
  struct rv264macro* upper_mb_attr;
  struct rv264macro* left_mb_attr;
  struct rv264macro* left_mb_attr0 = curr_mb_attr - 1;
  struct rv264macro* left_mb_attr1 = left_mb_attr0 + PicWidthInMbs;
  struct rv264macro* curr_mb_attr0 = curr_mb_attr; // top mb of the curr pair
  struct rv264macro* curr_mb_attr1 = curr_mb_attr + PicWidthInMbs; // bot mb of the curr pair
  unsigned int curr_is_field = curr_mb_attr->mb_field_decoding_flag;
  unsigned int upper_is_field = 0;  // initialization not necessary, but prevent a warning...
  unsigned int left_is_field;


  inner_qPav[0][0] = curr_mb_attr0->QPy;
  inner_qPav[1][0] = curr_mb_attr0->QPc[0];
  inner_qPav[2][0] = curr_mb_attr0->QPc[1];
  inner_qPav[0][1] = curr_mb_attr1->QPy;
  inner_qPav[1][1] = curr_mb_attr1->QPc[0];
  inner_qPav[2][1] = curr_mb_attr1->QPc[1];

  // fills in horizontal edges
  // Top Macroblock
  // 1st row
  upper_mb_attr = curr_mb_attr - PicWidthInMbs;
  if (mb_row > 0 && (!disable_deblocking_filter_idc || upper_mb_attr->slice_num == slice_num))
  {
    upper_is_field = upper_mb_attr->mb_field_decoding_flag;
    if (upper_is_field)
    {
      if (!curr_is_field)
      {
        // special case for top edge filtering => there is 2 top edge filtering, one with yEk = 0 and one with yEk = 1
        // start special case (for yEk = 1)
        either_or_both_is_intra = IS_INTRA_MB(curr_mb_attr) || IS_INTRA_MB(upper_mb_attr);
        either_or_both_is_SI_SP = IS_SI_SP_SLICE_MB(curr_mb_attr) || IS_SI_SP_SLICE_MB(upper_mb_attr);
        fills_in_bS_row(upper_bS[0], either_or_both_is_intra, either_or_both_is_SI_SP,
                        upper_mb_attr->TotalCoeffLuma+12, curr_mb_attr->TotalCoeffLuma,
                        &upper_mb_attr->RefIdxL[0][12], &curr_mb_attr->RefIdxL[0][0],
                        sh[upper_mb_attr->slice_num]->RefPicList, RefPicList,
                        upper_mb_attr->mv[0][12], curr_mb_attr->mv[0][0],
                        0, 1, 1);
        upper_qPav[0][0] = (upper_mb_attr->QPy + curr_mb_attr->QPy + 1) >> 1;
        upper_qPav[1][0] = (upper_mb_attr->QPc[0] + curr_mb_attr->QPc[0] + 1) >> 1;
        upper_qPav[2][0] = (upper_mb_attr->QPc[1] + curr_mb_attr->QPc[1] + 1) >> 1;
        // end special case
      }
      else
        upper_bS[0][0] = upper_bS[0][1] = upper_bS[0][2] = upper_bS[0][3] = 0;

      upper_mb_attr -= PicWidthInMbs;
    }

    either_or_both_is_intra = IS_INTRA_MB(curr_mb_attr) || IS_INTRA_MB(upper_mb_attr);
    either_or_both_is_SI_SP = IS_SI_SP_SLICE_MB(curr_mb_attr) || IS_SI_SP_SLICE_MB(upper_mb_attr);
    if ( !curr_is_field && !upper_is_field && (either_or_both_is_intra || either_or_both_is_SI_SP) )
    {
      upper_bS[1][0] = upper_bS[1][1] = upper_bS[1][2] = upper_bS[1][3] = 0x04;
    }
    else
    {
        fills_in_bS_row(upper_bS[1], either_or_both_is_intra, either_or_both_is_SI_SP,
                        upper_mb_attr->TotalCoeffLuma+12, curr_mb_attr->TotalCoeffLuma,
                        &upper_mb_attr->RefIdxL[0][12], &curr_mb_attr->RefIdxL[0][0],
                        sh[upper_mb_attr->slice_num]->RefPicList, RefPicList,
                        upper_mb_attr->mv[0][12], curr_mb_attr->mv[0][0],
                        curr_is_field, curr_is_field^upper_is_field, 1);
    }
    upper_qPav[0][1] = (upper_mb_attr->QPy + curr_mb_attr->QPy + 1) >> 1;
    upper_qPav[1][1] = (upper_mb_attr->QPc[0] + curr_mb_attr->QPc[0] + 1) >> 1;
    upper_qPav[2][1] = (upper_mb_attr->QPc[1] + curr_mb_attr->QPc[1] + 1) >> 1;
  }
  else
  { // no filtering of 1st row
    upper_bS[0][0] = upper_bS[0][1] = upper_bS[0][2] = upper_bS[0][3] = 0;
  }

  // 2nd row
  bSr = &inner_bS[1][0];
  either_or_both_is_intra = IS_INTRA_MB(curr_mb_attr);
  either_or_both_is_SI_SP = IS_SI_SP_SLICE_MB(curr_mb_attr);
  if (!transform_size_8x8_flag[0] || cfidc > 1)
  {
    fills_in_bS_row(bSr, either_or_both_is_intra, either_or_both_is_SI_SP,
                    curr_mb_attr->TotalCoeffLuma, curr_mb_attr->TotalCoeffLuma+4,
                    &curr_mb_attr->RefIdxL[0][0], &curr_mb_attr->RefIdxL[0][4],
                    RefPicList, RefPicList,
                    curr_mb_attr->mv[0][0], curr_mb_attr->mv[0][4],
                    curr_is_field, 0, 1);
  }
  // 3rd row
  bSr += 4;
  fills_in_bS_row(bSr, either_or_both_is_intra, either_or_both_is_SI_SP,
                  curr_mb_attr->TotalCoeffLuma+4, curr_mb_attr->TotalCoeffLuma+8,
                  &curr_mb_attr->RefIdxL[0][4], &curr_mb_attr->RefIdxL[0][8],
                  RefPicList, RefPicList,
                  curr_mb_attr->mv[0][4], curr_mb_attr->mv[0][8],
                  curr_is_field, 0, 1);
  // 4th row
  bSr += 4;
  if (!transform_size_8x8_flag[0] || cfidc > 1)
  {
    fills_in_bS_row(bSr, either_or_both_is_intra, either_or_both_is_SI_SP,
                    curr_mb_attr->TotalCoeffLuma+8, curr_mb_attr->TotalCoeffLuma+12,
                    &curr_mb_attr->RefIdxL[0][8], &curr_mb_attr->RefIdxL[0][12],
                    RefPicList, RefPicList,
                    curr_mb_attr->mv[0][8], curr_mb_attr->mv[0][12],
                    curr_is_field, 0, 1);
  }

  // (2nd mb) Bottom Macroblock
  if (mb_row > 0 || !curr_is_field)
  {
    if (curr_is_field) // mb_row>0 protect for the first top line. In that case
    {
      upper_mb_attr = curr_mb_attr - PicWidthInMbs; // bottom mb of the upper pair
      // upper_is_field value is still correct
    }
    else // curr is a frame mb
    {
      upper_mb_attr = curr_mb_attr;
      upper_is_field = curr_is_field;
    }
    curr_mb_attr += PicWidthInMbs;
    // 5th row
    either_or_both_is_intra = IS_INTRA_MB(curr_mb_attr) || IS_INTRA_MB(upper_mb_attr);
    either_or_both_is_SI_SP = IS_SI_SP_SLICE_MB(curr_mb_attr) || IS_SI_SP_SLICE_MB(upper_mb_attr);
    if ( (!curr_is_field && !upper_is_field && (either_or_both_is_intra || either_or_both_is_SI_SP)) )
    {
      upper_bS[2][0] = upper_bS[2][1] = upper_bS[2][2] = upper_bS[2][3] = 0x04;
    }
    else
    {
      fills_in_bS_row(upper_bS[2], either_or_both_is_intra, either_or_both_is_SI_SP,
                      upper_mb_attr->TotalCoeffLuma+12, curr_mb_attr->TotalCoeffLuma,
                      &upper_mb_attr->RefIdxL[0][12], &curr_mb_attr->RefIdxL[0][0],
                      sh[upper_mb_attr->slice_num]->RefPicList, RefPicList,
                      upper_mb_attr->mv[0][12], curr_mb_attr->mv[0][0],
                      curr_is_field, curr_is_field^upper_is_field, 1);
    }
    upper_qPav[0][2] = (upper_mb_attr->QPy + curr_mb_attr->QPy + 1) >> 1;
    upper_qPav[1][2] = (upper_mb_attr->QPc[0] + curr_mb_attr->QPc[0] + 1) >> 1;
    upper_qPav[2][2] = (upper_mb_attr->QPc[1] + curr_mb_attr->QPc[1] + 1) >> 1;
  }
  else
  {
    upper_bS[2][0] = upper_bS[2][1] = upper_bS[2][2] = upper_bS[2][3] = 0;
    curr_mb_attr += PicWidthInMbs;
  }


  // 6th row
  bSr += 4;
  either_or_both_is_intra = IS_INTRA_MB(curr_mb_attr);
  either_or_both_is_SI_SP = IS_SI_SP_SLICE_MB(curr_mb_attr);
  if (!transform_size_8x8_flag[1] || cfidc > 1)
  {
    fills_in_bS_row(bSr, either_or_both_is_intra, either_or_both_is_SI_SP,
                    curr_mb_attr->TotalCoeffLuma, curr_mb_attr->TotalCoeffLuma+4,
                    &curr_mb_attr->RefIdxL[0][0], &curr_mb_attr->RefIdxL[0][4],
                    RefPicList, RefPicList,
                    curr_mb_attr->mv[0][0], curr_mb_attr->mv[0][4],
                    curr_is_field, 0, 1);
  }
  // 7th row
  bSr += 4;
  fills_in_bS_row(bSr, either_or_both_is_intra, either_or_both_is_SI_SP,
                  curr_mb_attr->TotalCoeffLuma+4, curr_mb_attr->TotalCoeffLuma+8,
                  &curr_mb_attr->RefIdxL[0][4], &curr_mb_attr->RefIdxL[0][8],
                  RefPicList, RefPicList,
                  curr_mb_attr->mv[0][4], curr_mb_attr->mv[0][8],
                  curr_is_field, 0, 1);
  // 8th row
  bSr += 4;
  if (!transform_size_8x8_flag[1] || cfidc > 1)
  {
    fills_in_bS_row(bSr, either_or_both_is_intra, either_or_both_is_SI_SP,
                    curr_mb_attr->TotalCoeffLuma+8, curr_mb_attr->TotalCoeffLuma+12,
                    &curr_mb_attr->RefIdxL[0][8], &curr_mb_attr->RefIdxL[0][12],
                    RefPicList, RefPicList,
                    curr_mb_attr->mv[0][8], curr_mb_attr->mv[0][12],
                    curr_is_field, 0, 1);
  }



  // fills in vertical edges
  curr_mb_attr = curr_mb_attr0;
  left_mb_attr = left_mb_attr0;
  // Top Macroblock
  // 1st col
  if (mb_col>0 && (!disable_deblocking_filter_idc || left_mb_attr->slice_num == slice_num))
  {
    left_is_field = left_mb_attr->mb_field_decoding_flag;
    if (!(curr_is_field ^ left_is_field))
    {  // Easy case: both mb are field or frame mb pairs
      // 1st mb of the pair
      either_or_both_is_intra = IS_INTRA_MB(curr_mb_attr) || IS_INTRA_MB(left_mb_attr);
      either_or_both_is_SI_SP = IS_SI_SP_SLICE_MB(curr_mb_attr) || IS_SI_SP_SLICE_MB(left_mb_attr);
      if ( (either_or_both_is_intra || either_or_both_is_SI_SP) )
      {
        left_bS[0] = left_bS[1] = left_bS[2] = left_bS[3] = left_bS[4] = left_bS[5] = left_bS[6] = left_bS[7] = 4;
      }
      else
      {
        fills_in_bS_col_mbaff2(left_bS, either_or_both_is_intra, either_or_both_is_SI_SP,
                               left_mb_attr->TotalCoeffLuma+3, curr_mb_attr->TotalCoeffLuma,
                               &left_mb_attr->RefIdxL[0][3], &curr_mb_attr->RefIdxL[0][0],
                               sh[left_mb_attr->slice_num]->RefPicList, RefPicList,
                               left_mb_attr->mv[0][3], curr_mb_attr->mv[0][0], curr_is_field);
      }
      *(uint32_t*)(left_qPav[0]) = *(uint32_t*)(left_qPav[0]+4) = ((left_mb_attr->QPy + curr_mb_attr->QPy + 1) >> 1) * 0x01010101;
      *(uint32_t*)(left_qPav[1]) = *(uint32_t*)(left_qPav[1]+4) = ((left_mb_attr->QPc[0] + curr_mb_attr->QPc[0] + 1) >> 1) * 0x01010101;
      *(uint32_t*)(left_qPav[2]) = *(uint32_t*)(left_qPav[2]+4) = ((left_mb_attr->QPc[1] + curr_mb_attr->QPc[1] + 1) >> 1) * 0x01010101;

      // 2nd mb of the pair
      curr_mb_attr = curr_mb_attr1;
      left_mb_attr = left_mb_attr1;
      left_bS += 8;
      either_or_both_is_intra = IS_INTRA_MB(curr_mb_attr) || IS_INTRA_MB(left_mb_attr);
      either_or_both_is_SI_SP = IS_SI_SP_SLICE_MB(curr_mb_attr) || IS_SI_SP_SLICE_MB(left_mb_attr);
      if ( (either_or_both_is_intra || either_or_both_is_SI_SP) )
      {
        left_bS[0] = left_bS[1] = left_bS[2] = left_bS[3] = left_bS[4] = left_bS[5] = left_bS[6] = left_bS[7] = 4;
      }
      else
      {
        fills_in_bS_col_mbaff2(left_bS, either_or_both_is_intra, either_or_both_is_SI_SP,
                               left_mb_attr->TotalCoeffLuma+3, curr_mb_attr->TotalCoeffLuma,
                               &left_mb_attr->RefIdxL[0][3], &curr_mb_attr->RefIdxL[0][0],
                               sh[left_mb_attr->slice_num]->RefPicList, RefPicList,
                               left_mb_attr->mv[0][3], curr_mb_attr->mv[0][0], curr_is_field);
      }
      *(uint32_t*)(left_qPav[0]+8) = *(uint32_t*)(left_qPav[0]+12) = ((left_mb_attr->QPy + curr_mb_attr->QPy + 1) >> 1) * 0x01010101;
      *(uint32_t*)(left_qPav[1]+8) = *(uint32_t*)(left_qPav[1]+12) = ((left_mb_attr->QPc[0] + curr_mb_attr->QPc[0] + 1) >> 1) * 0x01010101;
      *(uint32_t*)(left_qPav[2]+8) = *(uint32_t*)(left_qPav[2]+12) = ((left_mb_attr->QPc[1] + curr_mb_attr->QPc[1] + 1) >> 1) * 0x01010101;
    }
    else   // Tricky case: the pairs are not the same kind (field / frame): curr_is_field ^ left_is_field == 1
    {
      unsigned int curr0_is_intra_or_SI_SP = IS_INTRA_MB(curr_mb_attr0) || IS_SI_SP_SLICE_MB(curr_mb_attr0);
      unsigned int curr1_is_intra_or_SI_SP = IS_INTRA_MB(curr_mb_attr1) || IS_SI_SP_SLICE_MB(curr_mb_attr1);
      unsigned int left0_is_intra_or_SI_SP = IS_INTRA_MB(left_mb_attr0) || IS_SI_SP_SLICE_MB(left_mb_attr0);
      unsigned int left1_is_intra_or_SI_SP = IS_INTRA_MB(left_mb_attr1) || IS_SI_SP_SLICE_MB(left_mb_attr1);

      if (curr_is_field)
      {
        if (curr0_is_intra_or_SI_SP || left0_is_intra_or_SI_SP)
          left_bS[0] = left_bS[1] = left_bS[2] = left_bS[3] = 4;
        else
          fills_in_bS_col_mbaff0(left_bS,
                                  left_mb_attr0->TotalCoeffLuma+3, curr_mb_attr0->TotalCoeffLuma,
                                  &left_mb_attr0->RefIdxL[0][3], &curr_mb_attr0->RefIdxL[0][0],
                                  sh[left_mb_attr0->slice_num]->RefPicList, RefPicList,
                                  left_mb_attr0->mv[0][3], curr_mb_attr0->mv[0][0]);
        *(uint32_t*)(left_qPav[0]+0) = ((left_mb_attr0->QPy + curr_mb_attr0->QPy + 1) >> 1) * 0x01010101;
        *(uint32_t*)(left_qPav[1]+0) = ((left_mb_attr0->QPc[0] + curr_mb_attr0->QPc[0] + 1) >> 1) * 0x01010101;
        *(uint32_t*)(left_qPav[2]+0) = ((left_mb_attr0->QPc[1] + curr_mb_attr0->QPc[1] + 1) >> 1) * 0x01010101;

        left_bS += 4;
        if (curr0_is_intra_or_SI_SP || left1_is_intra_or_SI_SP)
          left_bS[0] = left_bS[1] = left_bS[2] = left_bS[3] = 4;
        else
          fills_in_bS_col_mbaff0(left_bS,
                                  left_mb_attr1->TotalCoeffLuma+3, curr_mb_attr0->TotalCoeffLuma+8,
                                  &left_mb_attr1->RefIdxL[0][3], &curr_mb_attr0->RefIdxL[0][8],
                                  sh[left_mb_attr1->slice_num]->RefPicList, RefPicList,
                                  left_mb_attr1->mv[0][3], curr_mb_attr0->mv[0][8]);
        *(uint32_t*)(left_qPav[0]+4) = ((left_mb_attr1->QPy + curr_mb_attr0->QPy + 1) >> 1) * 0x01010101;
        *(uint32_t*)(left_qPav[1]+4) = ((left_mb_attr1->QPc[0] + curr_mb_attr0->QPc[0] + 1) >> 1) * 0x01010101;
        *(uint32_t*)(left_qPav[2]+4) = ((left_mb_attr1->QPc[1] + curr_mb_attr0->QPc[1] + 1) >> 1) * 0x01010101;

        left_bS += 4;
        if (curr1_is_intra_or_SI_SP || left0_is_intra_or_SI_SP)
          left_bS[0] = left_bS[1] = left_bS[2] = left_bS[3] = 4;
        else
          fills_in_bS_col_mbaff0(left_bS,
                                  left_mb_attr0->TotalCoeffLuma+3, curr_mb_attr1->TotalCoeffLuma,
                                  &left_mb_attr0->RefIdxL[0][3], &curr_mb_attr1->RefIdxL[0][0],
                                  sh[left_mb_attr0->slice_num]->RefPicList, RefPicList,
                                  left_mb_attr0->mv[0][3], curr_mb_attr1->mv[0][0]);
        *(uint32_t*)(left_qPav[0]+8) = ((left_mb_attr0->QPy + curr_mb_attr1->QPy + 1) >> 1) * 0x01010101;
        *(uint32_t*)(left_qPav[1]+8) = ((left_mb_attr0->QPc[0] + curr_mb_attr1->QPc[0] + 1) >> 1) * 0x01010101;
        *(uint32_t*)(left_qPav[2]+8) = ((left_mb_attr0->QPc[1] + curr_mb_attr1->QPc[1] + 1) >> 1) * 0x01010101;

        left_bS += 4;
        if (curr1_is_intra_or_SI_SP || left1_is_intra_or_SI_SP)
          left_bS[0] = left_bS[1] = left_bS[2] = left_bS[3] = 4;
        else
          fills_in_bS_col_mbaff0(left_bS,
                                  left_mb_attr1->TotalCoeffLuma+3, curr_mb_attr1->TotalCoeffLuma+8,
                                  &left_mb_attr1->RefIdxL[0][3], &curr_mb_attr1->RefIdxL[0][8],
                                  sh[left_mb_attr1->slice_num]->RefPicList, RefPicList,
                                  left_mb_attr1->mv[0][3], curr_mb_attr1->mv[0][8]);
        *(uint32_t*)(left_qPav[0]+12) = ((left_mb_attr1->QPy + curr_mb_attr1->QPy + 1) >> 1) * 0x01010101;
        *(uint32_t*)(left_qPav[1]+12) = ((left_mb_attr1->QPc[0] + curr_mb_attr1->QPc[0] + 1) >> 1) * 0x01010101;
        *(uint32_t*)(left_qPav[2]+12) = ((left_mb_attr1->QPc[1] + curr_mb_attr1->QPc[1] + 1) >> 1) * 0x01010101;
      }
      else // left_is_field
      {
        if (curr0_is_intra_or_SI_SP || (left0_is_intra_or_SI_SP && left1_is_intra_or_SI_SP))
        {
          left_bS[0] = left_bS[1] = left_bS[2] = left_bS[3] = left_bS[4] = left_bS[5] = left_bS[6] = left_bS[7] = 4;
        }
        else if (left0_is_intra_or_SI_SP)
        {
          left_bS[0] = left_bS[2] = left_bS[4] = left_bS[6] = 4;
          left_bS[1] = left_bS[3] = left_bS[5] = left_bS[7] = 0;
          fills_in_bS_col_mbaff1(left_bS, 1,
                                  left_mb_attr1->TotalCoeffLuma+3, curr_mb_attr0->TotalCoeffLuma,
                                  &left_mb_attr1->RefIdxL[0][3], &curr_mb_attr0->RefIdxL[0][0],
                                  sh[left_mb_attr1->slice_num]->RefPicList, RefPicList,
                                  left_mb_attr1->mv[0][3], curr_mb_attr0->mv[0][0]);
        }
        else if (left1_is_intra_or_SI_SP)
        {
          left_bS[0] = left_bS[2] = left_bS[4] = left_bS[6] = 0;
          left_bS[1] = left_bS[3] = left_bS[5] = left_bS[7] = 4;
          fills_in_bS_col_mbaff1(left_bS, 0,
                                  left_mb_attr0->TotalCoeffLuma+3, curr_mb_attr0->TotalCoeffLuma,
                                  &left_mb_attr0->RefIdxL[0][3], &curr_mb_attr0->RefIdxL[0][0],
                                  sh[left_mb_attr0->slice_num]->RefPicList, RefPicList,
                                  left_mb_attr0->mv[0][3], curr_mb_attr0->mv[0][0]);
        }
        else
        {
          fills_in_bS_col_mbaff1(left_bS, 0,
                                  left_mb_attr0->TotalCoeffLuma+3, curr_mb_attr0->TotalCoeffLuma,
                                  &left_mb_attr0->RefIdxL[0][3], &curr_mb_attr0->RefIdxL[0][0],
                                  sh[left_mb_attr0->slice_num]->RefPicList, RefPicList,
                                  left_mb_attr0->mv[0][3], curr_mb_attr0->mv[0][0]);
          fills_in_bS_col_mbaff1(left_bS, 1,
                                  left_mb_attr1->TotalCoeffLuma+3, curr_mb_attr0->TotalCoeffLuma,
                                  &left_mb_attr1->RefIdxL[0][3], &curr_mb_attr0->RefIdxL[0][0],
                                  sh[left_mb_attr1->slice_num]->RefPicList, RefPicList,
                                  left_mb_attr1->mv[0][3], curr_mb_attr0->mv[0][0]);
        }
        left_qPav[0][0] = left_qPav[0][2] = left_qPav[0][4] = left_qPav[0][6] =
            (left_mb_attr0->QPy + curr_mb_attr0->QPy + 1) >> 1;
        left_qPav[1][0] = left_qPav[1][2] = left_qPav[1][4] = left_qPav[1][6] =
            (left_mb_attr0->QPc[0] + curr_mb_attr0->QPc[0] + 1) >> 1;
        left_qPav[2][0] = left_qPav[2][2] = left_qPav[2][4] = left_qPav[2][6] =
            (left_mb_attr0->QPc[1] + curr_mb_attr0->QPc[1] + 1) >> 1;

        left_qPav[0][1] = left_qPav[0][3] = left_qPav[0][5] = left_qPav[0][7] =
            (left_mb_attr1->QPy + curr_mb_attr0->QPy + 1) >> 1;
        left_qPav[1][1] = left_qPav[1][3] = left_qPav[1][5] = left_qPav[1][7] =
            (left_mb_attr1->QPc[0] + curr_mb_attr0->QPc[0] + 1) >> 1;
        left_qPav[2][1] = left_qPav[2][3] = left_qPav[2][5] = left_qPav[2][7] =
            (left_mb_attr1->QPc[1] + curr_mb_attr0->QPc[1] + 1) >> 1;

        left_bS += 8;
        if (curr1_is_intra_or_SI_SP || (left0_is_intra_or_SI_SP && left1_is_intra_or_SI_SP))
        {
          left_bS[0] = left_bS[1] = left_bS[2] = left_bS[3] = left_bS[4] = left_bS[5] = left_bS[6] = left_bS[7] = 4;
        }
        else if (left0_is_intra_or_SI_SP)
        {
          left_bS[0] = left_bS[2] = left_bS[4] = left_bS[6] = 4;
          left_bS[1] = left_bS[3] = left_bS[5] = left_bS[7] = 0;
          fills_in_bS_col_mbaff1(left_bS, 1,
                                  left_mb_attr1->TotalCoeffLuma+11, curr_mb_attr1->TotalCoeffLuma,
                                  &left_mb_attr1->RefIdxL[0][11], &curr_mb_attr1->RefIdxL[0][0],
                                  sh[left_mb_attr1->slice_num]->RefPicList, RefPicList,
                                  left_mb_attr1->mv[0][11], curr_mb_attr1->mv[0][0]);
        }
        else if (left1_is_intra_or_SI_SP)
        {
          left_bS[0] = left_bS[2] = left_bS[4] = left_bS[6] = 0;
          left_bS[1] = left_bS[3] = left_bS[5] = left_bS[7] = 4;
          fills_in_bS_col_mbaff1(left_bS, 0,
                                  left_mb_attr0->TotalCoeffLuma+11, curr_mb_attr1->TotalCoeffLuma,
                                  &left_mb_attr0->RefIdxL[0][11], &curr_mb_attr1->RefIdxL[0][0],
                                  sh[left_mb_attr0->slice_num]->RefPicList, RefPicList,
                                  left_mb_attr0->mv[0][11], curr_mb_attr1->mv[0][0]);
        }
        else
        {
          fills_in_bS_col_mbaff1(left_bS, 0,
                                  left_mb_attr0->TotalCoeffLuma+11, curr_mb_attr1->TotalCoeffLuma,
                                  &left_mb_attr0->RefIdxL[0][11], &curr_mb_attr1->RefIdxL[0][0],
                                  sh[left_mb_attr0->slice_num]->RefPicList, RefPicList,
                                  left_mb_attr0->mv[0][11], curr_mb_attr1->mv[0][0]);
          fills_in_bS_col_mbaff1(left_bS, 1,
                                  left_mb_attr1->TotalCoeffLuma+11, curr_mb_attr1->TotalCoeffLuma,
                                  &left_mb_attr1->RefIdxL[0][11], &curr_mb_attr1->RefIdxL[0][0],
                                  sh[left_mb_attr1->slice_num]->RefPicList, RefPicList,
                                  left_mb_attr1->mv[0][11], curr_mb_attr1->mv[0][0]);
        }
        left_qPav[0][8] = left_qPav[0][10] = left_qPav[0][12] = left_qPav[0][14] =
            (left_mb_attr0->QPy + curr_mb_attr1->QPy + 1) >> 1;
        left_qPav[1][8] = left_qPav[1][10] = left_qPav[1][12] = left_qPav[1][14] =
            (left_mb_attr0->QPc[0] + curr_mb_attr1->QPc[0] + 1) >> 1;
        left_qPav[2][8] = left_qPav[2][10] = left_qPav[2][12] = left_qPav[2][14] =
            (left_mb_attr0->QPc[1] + curr_mb_attr1->QPc[1] + 1) >> 1;

        left_qPav[0][9] = left_qPav[0][11] = left_qPav[0][13] = left_qPav[0][15] =
            (left_mb_attr1->QPy + curr_mb_attr1->QPy + 1) >> 1;
        left_qPav[1][9] = left_qPav[1][11] = left_qPav[1][13] = left_qPav[1][15] =
            (left_mb_attr1->QPc[0] + curr_mb_attr1->QPc[0] + 1) >> 1;
        left_qPav[2][9] = left_qPav[2][11] = left_qPav[2][13] = left_qPav[2][15] =
            (left_mb_attr1->QPc[1] + curr_mb_attr1->QPc[1] + 1) >> 1;

      }
    }
  }
  else // no filtering for the first col
  {
    left_bS[0] = left_bS[1] = left_bS[2] = left_bS[3] = left_bS[4] = left_bS[5] = left_bS[6] = left_bS[7] = 0;
    left_bS[8] = left_bS[9] = left_bS[10] = left_bS[11] = left_bS[12] = left_bS[13] = left_bS[14] = left_bS[15] = 0;
  }




  // 1st mb of the pair
  bSr = &inner_bS[0][0];
  curr_mb_attr = curr_mb_attr0;
  // 2nd col
  either_or_both_is_intra = IS_INTRA_MB(curr_mb_attr);
  either_or_both_is_SI_SP = IS_SI_SP_SLICE_MB(curr_mb_attr);
  if (!transform_size_8x8_flag[0] || cfidc > 2)
  {
    fills_in_bS_col(bSr, either_or_both_is_intra, either_or_both_is_SI_SP,
                    curr_mb_attr->TotalCoeffLuma, curr_mb_attr->TotalCoeffLuma+1,
                    &curr_mb_attr->RefIdxL[0][0], &curr_mb_attr->RefIdxL[0][1],
                    RefPicList, RefPicList,
                    curr_mb_attr->mv[0][0], curr_mb_attr->mv[0][1],
                    curr_is_field, 1);
  }

  // 3rd col
  bSr += 4;
  fills_in_bS_col(bSr, either_or_both_is_intra, either_or_both_is_SI_SP,
                  curr_mb_attr->TotalCoeffLuma+1, curr_mb_attr->TotalCoeffLuma+2,
                  &curr_mb_attr->RefIdxL[0][1], &curr_mb_attr->RefIdxL[0][2],
                  RefPicList, RefPicList,
                  curr_mb_attr->mv[0][1], curr_mb_attr->mv[0][2],
                  curr_is_field, 1);

  // 4th col
  bSr += 4;
  if (!transform_size_8x8_flag[0] || cfidc > 2)
  {
    fills_in_bS_col(bSr, either_or_both_is_intra, either_or_both_is_SI_SP,
                    curr_mb_attr->TotalCoeffLuma+2, curr_mb_attr->TotalCoeffLuma+3,
                    &curr_mb_attr->RefIdxL[0][2], &curr_mb_attr->RefIdxL[0][3],
                    RefPicList, RefPicList,
                    curr_mb_attr->mv[0][2], curr_mb_attr->mv[0][3],
                    curr_is_field, 1);
  }

  // 2nd mb of the pair
  curr_mb_attr = curr_mb_attr1;
  bSr += 4;
  // 2nd col
  either_or_both_is_intra = IS_INTRA_MB(curr_mb_attr);
  either_or_both_is_SI_SP = IS_SI_SP_SLICE_MB(curr_mb_attr);
  if (!transform_size_8x8_flag[1] || cfidc > 2)
  {
    fills_in_bS_col(bSr, either_or_both_is_intra, either_or_both_is_SI_SP,
                    curr_mb_attr->TotalCoeffLuma, curr_mb_attr->TotalCoeffLuma+1,
                    &curr_mb_attr->RefIdxL[0][0], &curr_mb_attr->RefIdxL[0][1],
                    RefPicList, RefPicList,
                    curr_mb_attr->mv[0][0], curr_mb_attr->mv[0][1],
                    curr_is_field, 1);
  }

  // 3rd col
  bSr += 4;
  fills_in_bS_col(bSr, either_or_both_is_intra, either_or_both_is_SI_SP,
                  curr_mb_attr->TotalCoeffLuma+1, curr_mb_attr->TotalCoeffLuma+2,
                  &curr_mb_attr->RefIdxL[0][1], &curr_mb_attr->RefIdxL[0][2],
                  RefPicList, RefPicList,
                  curr_mb_attr->mv[0][1], curr_mb_attr->mv[0][2],
                  curr_is_field, 1);

  // 4th col
  bSr += 4;
  if (!transform_size_8x8_flag[1] || cfidc > 2)
  {
    fills_in_bS_col(bSr, either_or_both_is_intra, either_or_both_is_SI_SP,
                    curr_mb_attr->TotalCoeffLuma+2, curr_mb_attr->TotalCoeffLuma+3,
                    &curr_mb_attr->RefIdxL[0][2], &curr_mb_attr->RefIdxL[0][3],
                    RefPicList, RefPicList,
                    curr_mb_attr->mv[0][2], curr_mb_attr->mv[0][3],
                    curr_is_field, 1);
  }
}




#define LOAD_8_SAMPLES(comp, stride)        \
const int q0 = (comp)[0];                   \
const int q1 = (comp)[(stride)];            \
const int q2 = (comp)[(stride)*2];          \
const int q3 = (comp)[(stride)*3];          \
const int p0 = (comp)[-(signed)(stride)];   \
const int p1 = (comp)[-(signed)(stride)*2]; \
const int p2 = (comp)[-(signed)(stride)*3]; \
const int p3 = (comp)[-(signed)(stride)*4];

#define LOAD_6F_SAMPLES(comp, stride)       \
const int q0 = (comp)[0];                   \
const int q1 = (comp)[(stride)];            \
const int q2 = (comp)[(stride)*2];          \
const int p0 = (comp)[-(signed)(stride)];   \
const int p1 = (comp)[-(signed)(stride)*2]; \
const int p2 = (comp)[-(signed)(stride)*3];


#define LOAD_4F_SAMPLES(comp, stride)       \
const int q0 = (comp)[0];                   \
const int q1 = (comp)[(stride)];            \
const int p0 = (comp)[-(signed)(stride)];   \
const int p1 = (comp)[-(signed)(stride)*2];

#define LOAD_2M_SAMPLES(comp, stride)       \
const int q2 = (comp)[(stride)*2];          \
const int p2 = (comp)[-(signed)(stride)*3];

#define LOAD_4L_SAMPLES(comp, stride)       \
const int q2 = (comp)[(stride)*2];          \
const int q3 = (comp)[(stride)*3];          \
const int p2 = (comp)[-(signed)(stride)*3]; \
const int p3 = (comp)[-(signed)(stride)*4];

int cubu_debug = 0; //cubu

static void filtering_for_bS_less_than_4(const int p0, const int p1, const int p2, const int q0, const int q1, const int q2,
                                                const int tc0, const int beta, unsigned int is_luma, pixel_t* out, int stride, int max_value)
{
  int tc;
  int delta;

  unsigned int ap_flag = im_abs(p2-p0) < beta;
  unsigned int aq_flag = im_abs(q2-q0) < beta;


  if (is_luma)
    tc = tc0 + ap_flag + aq_flag;
  else
    tc = tc0 + 1;

  delta = im_clip(-tc, tc, ((((q0-p0)<<2)+(p1-q1)+4)>>3));

  out[0] = im_clip(0, max_value, q0 - delta); // q prime 0
  out[-1*stride] = im_clip(0, max_value, p0 + delta); // p prime 0
  if (is_luma && aq_flag)
    out[1*stride] = q1 + im_clip(-tc0, tc0, (q2+((p0+q0+1)>>1)-(q1<<1))>>1); // q prime 1
  if (is_luma && ap_flag)
    out[-2*stride] = p1 + im_clip(-tc0, tc0, (p2+((p0+q0+1)>>1)-(p1<<1))>>1); // p prime 1
}


static void filtering_for_bS_equal_to_4(const int p0, const int p1, const int p2, const int p3, const int q0, const int q1, const int q2, const int q3,
                                               const int alpha, const int beta, unsigned int is_luma, pixel_t* out, int stride)
{
  unsigned int test = (im_abs(p0-q0) < ((alpha>>2)+2));
  int ap_flag = im_abs(p2-p0) < beta;
  int aq_flag = im_abs(q2-q0) < beta;

  // p prime 0..2
  if (is_luma && ap_flag && test)
  {
    out[-1*stride] = ( p2 + 2*p1 + 2*p0 + 2*q0 + q1 + 4 ) >> 3;
    out[-2*stride] = ( p2 + p1 + p0 + q0 + 2 ) >> 2;
    out[-3*stride] = ( 2*p3 + 3*p2 + p1 + p0 + q0 + 4 ) >> 3;
  }
  else
    out[-1*stride] =  ( 2*p1 + p0 + q1 + 2 ) >> 2;

  // q prime 0..2
  if (is_luma && aq_flag && test)
  {
    out[0*stride] = ( p1 + 2*p0 + 2*q0 + 2*q1 + q2 + 4 ) >> 3;
    out[1*stride] = ( p0 + q0 + q1 + q2 + 2 ) >> 2;
    out[2*stride] = ( 2*q3 + 3*q2 + q1 + q0 + p0 + 4 ) >> 3;
  }
  else
    out[0*stride] = ( 2*q1 + q0 + p1 + 2 ) >> 2;

}

// block offset is for chroma processing
static void filter_one_edge(uint8_t bSr[4], int indexA, int alpha, int beta, pixel_t* samples_,
                                   unsigned int is_horizontal_edge, unsigned int is_luma, unsigned int PicWidth, unsigned int maxV,
                                   unsigned int block_offset, unsigned int bit_depth_minus8)
{
  int bSv;
  int tc0;
  unsigned int i, k;
  int stride = is_horizontal_edge ? PicWidth : 1;
  int sample_offset = is_horizontal_edge ? 1 : PicWidth;
  int sample_offset4 = (4/block_offset)*sample_offset;
  pixel_t* samples;

  for ( i=0; i<4; i++)
  {
    if ((bSv = bSr[i]) != 0)
    {
      samples = samples_ + i*sample_offset4;
      if (bSv<4)
      {
        tc0 = bS_indexA_to_tc0[bSv-1][indexA] << bit_depth_minus8;
        for ( k=0; k<4; k+=block_offset)
        {
          LOAD_4F_SAMPLES(samples, stride);
          //if (cubu_debug)
          //  printf(" indexA %d, Alpha %d, Beta %d, bS %d |", indexA, alpha, beta, bSv);// CUBU DEBUG
          if (cubu_debug)
            printf(" %d", bSv); // CUBU DEBUG
          if (im_abs(p0-q0) < alpha && im_abs(p1-p0) < beta && im_abs(q1-q0) < beta) // then yes we will do the filtering  for those samples !
          {
            LOAD_2M_SAMPLES(samples, stride);
            if (cubu_debug)
              printf(" %d", tc0); // CUBU DEBUG
            filtering_for_bS_less_than_4(p0, p1, p2, q0, q1, q2, tc0, beta, is_luma,
                                         samples, stride, maxV);
          }
          samples += sample_offset;
        }
      }
      else // bS == 4
      {
        for ( k=0; k<4; k+=block_offset)
        {
          LOAD_4F_SAMPLES(samples, stride);
          //if (cubu_debug)
          //  printf(" indexA %d, Alpha %d, Beta %d, bS %d |", indexA, alpha, beta, bSv);// CUBU DEBUG
          if (cubu_debug)
            printf(" %d", bSv); // CUBU DEBUG
          if (im_abs(p0-q0) < alpha && im_abs(p1-p0) < beta && im_abs(q1-q0) < beta) // then yes we will do the filtering  for those samples !
          {
            LOAD_4L_SAMPLES(samples, stride);

            filtering_for_bS_equal_to_4(p0, p1, p2, p3, q0, q1, q2, q3, alpha, beta, is_luma,
                                        samples, stride);
          }
          samples += sample_offset;
        }
      }
    }
  }
}

static void filter_left_vert_mbaff_edge(uint8_t bSr[8], int8_t qPav[8], pixel_t* samples_, unsigned int FilterOffsetA, unsigned int FilterOffsetB,
                                               unsigned int is_luma, unsigned int PicWidth, unsigned int maxV, unsigned int interlaced,
                                               unsigned int block_offset, unsigned int bit_depth_minus8)
{
  int bSv;
  int tc0;
  unsigned int i, k;
  int indexA;
  int indexB;
  int alpha;
  int beta;
  pixel_t* samples;

  LUD_DEBUG_ASSERT(is_luma || !interlaced); // for chroma the bS values can not be interlaced !

  for ( i=0; i<8; i++)
  {
    indexA = im_clip(0, 51, qPav[i] + FilterOffsetA);
    alpha = indexA_to_alpha[indexA] << bit_depth_minus8;
    indexB = im_clip(0, 51, qPav[i] + FilterOffsetB);
    beta = indexB_to_beta[indexB] << bit_depth_minus8;

    bSv = bSr[i];

    if (bSv != 0)
    {
      if (!interlaced)
        samples = samples_+i*PicWidth*(1+is_luma);
      else
        samples = samples_+ ((i>>1)*4 + (i&1)) * PicWidth;

      if (bSv<4)
      {
        tc0 = bS_indexA_to_tc0[bSv-1][indexA] << bit_depth_minus8;
        for ( k=0; k<2; k += block_offset)
        {
          LOAD_4F_SAMPLES(samples, 1);
          if (cubu_debug)
            printf(" indexA %d, Alpha %d, Beta %d, bS %d |", indexA, alpha, beta, bSv);// CUBU DEBUG

          if (im_abs(p0-q0) < alpha && im_abs(p1-p0) < beta && im_abs(q1-q0) < beta) // then yes we will do the filtering  for those samples !
          {
            LOAD_2M_SAMPLES(samples, 1);
            filtering_for_bS_less_than_4(p0, p1, p2, q0, q1, q2, tc0, beta, is_luma,
                                         samples, 1, maxV);
          }
          samples += PicWidth*(1+interlaced);
        }
      }
      else // bS == 4
      {
        for ( k=0; k<2; k += block_offset)
        {
          LOAD_4F_SAMPLES(samples, 1);
          if (cubu_debug)
            printf(" indexA %d, Alpha %d, Beta %d, bS %d |", indexA, alpha, beta, bSv);// CUBU DEBUG
          if (im_abs(p0-q0) < alpha && im_abs(p1-p0) < beta && im_abs(q1-q0) < beta) // then yes we will do the filtering  for those samples !
          {
            LOAD_4L_SAMPLES(samples, 1);

            filtering_for_bS_equal_to_4(p0, p1, p2, p3, q0, q1, q2, q3, alpha, beta, is_luma,
                                        samples, 1);
          }
          samples += PicWidth*(1+interlaced);
        }
      }
    }
  }
}


static void filter_mb(struct rv264macro* curr_mb_attr, unsigned int mb_row, unsigned int mb_col,
    unsigned int disable_deblocking_filter_idc, unsigned int slice_num, int PicWidthInMbs, unsigned int field_pic_flag,
    struct slice_header** sh, unsigned int PicWidthY, unsigned int PicWidthC, pixel_t* Y, pixel_t* C[2],
    unsigned int maxY, unsigned int maxC, unsigned int mbc_sub_width, unsigned int mbc_sub_height,
    unsigned int mbc_height, unsigned int cfidc, unsigned int MbaffFrameFlag, unsigned int FilterOffsetA,
    unsigned int FilterOffsetB, unsigned int bit_depth_luma_minus8, unsigned int bit_depth_chroma_minus8, RefListEntry* RefPicList[2])
{
  pixel_t* y;
  pixel_t* c;
  int j, iCbCr;


  if (!MbaffFrameFlag)
  {
    uint8_t bS[2][4][4] declare_aligned(4);
    int8_t qPav[3][3] = {{0}}; // [border][component]:   border: 0-inner, 1-left, 2-upper; component: 0-Y, 1-Cb, 2-Cr
    unsigned int transform_size_8x8_flag = curr_mb_attr->transform_size_8x8_flag;
    int indexA, indexA_inner;
    int indexB;
    int alpha, alpha_inner;
    int beta, beta_inner;


    fills_in_bS_nonmbaff(bS, qPav, curr_mb_attr, mb_row, mb_col, PicWidthInMbs, disable_deblocking_filter_idc,
        sh, slice_num, RefPicList, field_pic_flag, transform_size_8x8_flag, cfidc);

    // FIXME DEBUG
/*    printf("MB %d\n", mb_row*PicWidthInMbs+mb_col); // CUBU DEBUG
    {
      int k,l,m;
      for (l=0; l<2; l++)
      {
        printf("Vert/Horiz bS %d\n", l);
        for (k=0; k<4; k++)
        {
          for (m=0; m<4; m++)
          {
            printf("%d, ", bS[l][k][m]);
          }
          printf("\n");
        }
      }
    }
    printf("\n");*/

    //cubu_debug = 1; // CUBU DEBUG
    //printf("MB %d\n", mb_row*PicWidthInMbs+mb_col); // CUBU DEBUG
    //printf("Horiz/Vert: %d\n", 0); // CUBU DEBUG
    //printf("edge: %d ", 0); // CUBU DEBUG

    // do Luma filtering
    // do vertical filtering
    y = Y;
    // 1st col
    if (bS[0][0][0] != 0 || bS[0][0][1] != 0 || bS[0][0][2] != 0 || bS[0][0][3] != 0)
    {
      indexA = im_clip(0, 51, qPav[1][0] + FilterOffsetA);
      alpha = indexA_to_alpha[indexA] << bit_depth_luma_minus8;
      indexB = im_clip(0, 51, qPav[1][0] + FilterOffsetB);
      beta = indexB_to_beta[indexB] << bit_depth_luma_minus8;
      //printf("indexA %d, Alpha %d, Beta %d |", indexA, alpha, beta); // CUBU DEBUG
      filter_one_edge(bS[0][0], indexA, alpha, beta, y, 0, 1, PicWidthY, maxY, 1, bit_depth_luma_minus8);
    }
    //printf("\n"); // CUBU DEBUG
    y += 4;
    // 2nd-4th col
    indexA_inner = im_clip(0, 51, qPav[0][0] + FilterOffsetA);
    alpha_inner = indexA_to_alpha[indexA_inner] << bit_depth_luma_minus8;
    indexB = im_clip(0, 51, qPav[0][0] + FilterOffsetB);
    beta_inner = indexB_to_beta[indexB] << bit_depth_luma_minus8;
    if (transform_size_8x8_flag)
    {
      y += 4;
      filter_one_edge(bS[0][2], indexA_inner, alpha_inner, beta_inner, y, 0, 1, PicWidthY, maxY, 1, bit_depth_luma_minus8);
    }
    else
    {
      for (j=1; j<4; j += 1)
      {
        //printf("edge: %d ", j); // CUBU DEBUG
        //printf("indexA %d, Alpha %d, Beta %d |", indexA_inner, alpha_inner, beta_inner); // CUBU DEBUG
        filter_one_edge(bS[0][j], indexA_inner, alpha_inner, beta_inner, y, 0, 1, PicWidthY, maxY, 1, bit_depth_luma_minus8);
        y += 4;
        //printf("\n"); // cubu
      }
    }
    //printf("\n"); // CUBU DEBUG


    //printf("Horiz/Vert: %d\n", 1); // CUBU DEBUG
    //printf("edge: %d ", 0); // CUBU DEBUG
    // do horizontal filtering
    y = Y;
    // 1st row
    if (bS[1][0][0] != 0 || bS[1][0][1] != 0 || bS[1][0][2] != 0 || bS[1][0][3] != 0)
    {
      indexA = im_clip(0, 51, qPav[2][0] + FilterOffsetA);
      alpha = indexA_to_alpha[indexA] << bit_depth_luma_minus8;
      indexB = im_clip(0, 51, qPav[2][0] + FilterOffsetB);
      beta = indexB_to_beta[indexB] << bit_depth_luma_minus8;
      //printf("indexA %d, Alpha %d, Beta %d |", indexA, alpha, beta); // CUBU DEBUG
      filter_one_edge(bS[1][0], indexA, alpha, beta, y, 1, 1, PicWidthY, maxY, 1, bit_depth_luma_minus8);
    }
    //printf("\n"); // CUBU DEBUG
    y += 4*PicWidthY;
    // 2nd-4th row
    // inner params are already calculated !
    if (transform_size_8x8_flag)
    {
      y += 4*PicWidthY;
      filter_one_edge(bS[1][2], indexA_inner, alpha_inner, beta_inner, y, 1, 1, PicWidthY, maxY, 1, bit_depth_luma_minus8);
    }
    else
    {
      for (j=1; j<4; j += 1)
      {
        //printf("edge: %d ", j); // CUBU DEBUG
        //printf("indexA %d, Alpha %d, Beta %d |", indexA_inner, alpha_inner, beta_inner); // CUBU DEBUG
        filter_one_edge(bS[1][j], indexA_inner, alpha_inner, beta_inner, y, 1, 1, PicWidthY, maxY, 1, bit_depth_luma_minus8);
        y += 4*PicWidthY;
        //printf("\n"); // cubu
      }
    }
    //printf("\n"); // CUBU DEBUG
    //cubu_debug = 0;

    if (cfidc > 0)
    {
      // do chroma filtering
      for (iCbCr = 0; iCbCr < 2; iCbCr++)
      {
        // cubu debug
//        if (iCbCr == 0)
//          cubu_debug = 1;
//        else
//          cubu_debug = 0;

        // do vertical filtering
        c = C[iCbCr];

        // 1st col
        if (bS[0][0][0] != 0 || bS[0][0][1] != 0 || bS[0][0][2] != 0 || bS[0][0][3] != 0)
        {
          indexA = im_clip(0, 51, qPav[1][iCbCr+1] + FilterOffsetA);
          alpha = indexA_to_alpha[indexA] << bit_depth_chroma_minus8;
          indexB = im_clip(0, 51, qPav[1][iCbCr+1] + FilterOffsetB);
          beta = indexB_to_beta[indexB] << bit_depth_chroma_minus8;
          filter_one_edge(bS[0][0], indexA, alpha, beta, c, 0, 0, PicWidthC, maxC, mbc_sub_height, bit_depth_chroma_minus8);
        }
        c += 4;
        // 2nd to 4th col
        indexA_inner = im_clip(0, 51, qPav[0][iCbCr+1] + FilterOffsetA);
        alpha_inner = indexA_to_alpha[indexA_inner] << bit_depth_chroma_minus8;
        indexB = im_clip(0, 51, qPav[0][iCbCr+1] + FilterOffsetB);
        beta_inner = indexB_to_beta[indexB] << bit_depth_chroma_minus8;
        for (j=mbc_sub_width; j < 4; j += mbc_sub_width)
        {
          filter_one_edge(bS[0][j], indexA_inner, alpha_inner, beta_inner, c, 0, 0, PicWidthC, maxC, mbc_sub_height, bit_depth_chroma_minus8);
          c += 4;
        }

        //printf("\n");
        // do horizontal filtering
        c = C[iCbCr];
        // 1st row
        if (bS[1][0][0] != 0 || bS[1][0][1] != 0 || bS[1][0][2] != 0 || bS[1][0][3] != 0)
        {
          indexA = im_clip(0, 51, qPav[2][iCbCr+1] + FilterOffsetA);
          alpha = indexA_to_alpha[indexA] << bit_depth_chroma_minus8;
          indexB = im_clip(0, 51, qPav[2][iCbCr+1] + FilterOffsetB);
          beta = indexB_to_beta[indexB] << bit_depth_chroma_minus8;
          filter_one_edge(bS[1][0], indexA, alpha, beta, c, 1, 0, PicWidthC, maxC, mbc_sub_width, bit_depth_chroma_minus8);
        }
        c += 4*PicWidthC;
        // 2nd to 4th row
        // inner params are already calculated !
        for (j=mbc_sub_height; j < 4; j += mbc_sub_height)
        {
          filter_one_edge(bS[1][j], indexA_inner, alpha_inner, beta_inner, c, 1, 0, PicWidthC, maxC, mbc_sub_width, bit_depth_chroma_minus8);
          c += 4*PicWidthC;
        }
      }
    }
    cubu_debug = 0;
  }

  else // MbaffFrameFlag==1 => process pair by pair instead of mb by mb
  {
    uint8_t inner_bS[2][24] declare_aligned(8);
    uint8_t left_bS[16] declare_aligned(8);
    uint8_t upper_bS[3][4] declare_aligned(4);
    int8_t left_qPav[3][16] declare_aligned(4); // [component][left mb for row]:   component: 0-Y, 1-Cb, 2-Cr; row : = actual row/2
    int8_t upper_qPav[3][3] = {{0}}; // [component][upper mb for row]:   component: 0-Y, 1-Cb, 2-Cr; row : = 0-1: two 1st rows of top mb, 2: 1st row of bor mb
    int8_t inner_qPav[3][2]; //  [component][top/bot] component: 0-Y, 1-Cb, 2-Cr, top/bot: 0->top, 1->bot
    int indexA, indexA_inner;
    int indexB;
    int alpha, alpha_inner;
    int beta, beta_inner;
    unsigned int Y_dy, C_dy;
    unsigned int curr_is_field = curr_mb_attr->mb_field_decoding_flag;
    struct rv264macro* upper_mb_attr = curr_mb_attr-PicWidthInMbs;
    struct rv264macro* left_mb_attr = curr_mb_attr-1;
    unsigned int transform_size_8x8_flag[2];
    unsigned int topbot;

    transform_size_8x8_flag[0] = curr_mb_attr->transform_size_8x8_flag;
    transform_size_8x8_flag[1] = (curr_mb_attr+PicWidthInMbs)->transform_size_8x8_flag;


    Y_dy = curr_is_field ? PicWidthY*2 : PicWidthY;
    C_dy = curr_is_field ? PicWidthC*2 : PicWidthC;

    fills_in_bS_mbaff(inner_bS, upper_bS, left_bS, inner_qPav, upper_qPav, left_qPav, curr_mb_attr,
                      mb_row, mb_col, PicWidthInMbs, disable_deblocking_filter_idc,
                      sh, slice_num, RefPicList, transform_size_8x8_flag, cfidc);



//    if (mb_col == 2 && mb_row == 0)
  //    printf("toto\n");

    // do Luma filtering
    // top and then bottom mb
    for (topbot=0; topbot<2; topbot++)
    {
      //cubu_debug = 1; // CUBU DEBUG
      //printf("MB %d\n", (mb_row>>1)*PicWidthInMbs*2+mb_col*2+topbot); // CUBU DEBUG
      //printf("Horiz/Vert: %d\n", 0); // CUBU DEBUG
      //printf("edge: %d ", 0); // CUBU DEBUG

      // do vertical filtering
      y = Y + (curr_is_field? PicWidthY : 16*PicWidthY) * topbot;

      // 1st col
      if ( mb_col>0 && *(uint64_t*)(left_bS+topbot*8) != 0)
      {
        if (!curr_is_field && left_mb_attr->mb_field_decoding_flag)
        {
          filter_left_vert_mbaff_edge(left_bS+topbot*8, left_qPav[0]+topbot*8, y, FilterOffsetA, FilterOffsetB,
                                      1, Y_dy, maxY, 1,
                                      1, bit_depth_luma_minus8);
        }
        else
        {
          filter_left_vert_mbaff_edge(left_bS+topbot*8, left_qPav[0]+topbot*8, y, FilterOffsetA, FilterOffsetB,
                                      1, Y_dy, maxY, 0,
                                      1, bit_depth_luma_minus8);
        }
      }
      //printf("\n"); // cubu

      y += 4;
      // 2nd-4th col for top and bot mb
      indexA_inner = im_clip(0, 51, inner_qPav[0][topbot] + FilterOffsetA);
      alpha_inner = indexA_to_alpha[indexA_inner] << bit_depth_luma_minus8;
      indexB = im_clip(0, 51, inner_qPav[0][topbot] + FilterOffsetB);
      beta_inner = indexB_to_beta[indexB] << bit_depth_luma_minus8;
      if (transform_size_8x8_flag[topbot])
      {
        y += 4;
        //printf("edge: %d ", 2); // CUBU DEBUG
        filter_one_edge(inner_bS[0] + 1*4 + 3*4*topbot, indexA_inner, alpha_inner, beta_inner, y, 0, 1, Y_dy, maxY, 1, bit_depth_luma_minus8);
        //printf("\n"); // cubu

      }
      else
      {
        for (j=0; j<3; j += 1)
        {
          //printf("edge: %d ", j+1); // CUBU DEBUG
          filter_one_edge(inner_bS[0] + j*4 + 3*4*topbot, indexA_inner, alpha_inner, beta_inner, y, 0, 1, Y_dy, maxY, 1, bit_depth_luma_minus8);
          //printf("\n"); // cubu
          y += 4;
        }
      }

      //printf("\n"); // CUBU DEBUG
      //printf("Horiz/Vert: %d\n", 1); // CUBU DEBUG
      // do horizontal filtering
      y = Y + (curr_is_field? PicWidthY : 16*PicWidthY) * topbot;

      // 1st row
      //printf("edge: %d ", 0); // CUBU DEBUG
      if ( mb_row >0 || (topbot && !curr_is_field))
      {
        if (!topbot && !curr_is_field && upper_mb_attr->mb_field_decoding_flag)
        {
          if (upper_bS[1][0] != 0 || upper_bS[1][1] != 0 || upper_bS[1][2] != 0 || upper_bS[1][3] != 0)
          {
            indexA = im_clip(0, 51, upper_qPav[0][1] + FilterOffsetA);
            alpha = indexA_to_alpha[indexA] << bit_depth_luma_minus8;
            indexB = im_clip(0, 51, upper_qPav[0][1] + FilterOffsetB);
            beta = indexB_to_beta[indexB] << bit_depth_luma_minus8;
            filter_one_edge(upper_bS[1], indexA, alpha, beta, y, 1, 1, PicWidthY*2, maxY, 1, bit_depth_luma_minus8);
          }
          if (upper_bS[0][0] != 0 || upper_bS[0][1] != 0 || upper_bS[0][2] != 0 || upper_bS[0][3] != 0)
          {
            indexA = im_clip(0, 51, upper_qPav[0][0] + FilterOffsetA);
            alpha = indexA_to_alpha[indexA] << bit_depth_luma_minus8;
            indexB = im_clip(0, 51, upper_qPav[0][0] + FilterOffsetB);
            beta = indexB_to_beta[indexB] << bit_depth_luma_minus8;
            filter_one_edge(upper_bS[0], indexA, alpha, beta, y+PicWidthY, 1, 1, PicWidthY*2, maxY, 1, bit_depth_luma_minus8);
          }
        }
        else if (upper_bS[1+topbot][0] != 0 || upper_bS[1+topbot][1] != 0 || upper_bS[1+topbot][2] != 0 || upper_bS[1+topbot][3] != 0)
        {
          indexA = im_clip(0, 51, upper_qPav[0][1+topbot] + FilterOffsetA);
          alpha = indexA_to_alpha[indexA] << bit_depth_luma_minus8;
          indexB = im_clip(0, 51, upper_qPav[0][1+topbot] + FilterOffsetB);
          beta = indexB_to_beta[indexB] << bit_depth_luma_minus8;
          filter_one_edge(upper_bS[1+topbot], indexA, alpha, beta, y, 1, 1, Y_dy, maxY, 1, bit_depth_luma_minus8);
        }
      }
      //printf("\n"); // cubu

      y += 4*Y_dy;
      // 2nd-4th row of top mb
      // inner params are already calculated !
      if (transform_size_8x8_flag[topbot])
      {
        y += 4*Y_dy;
        //printf("edge: %d ", 2); // CUBU DEBUG
        filter_one_edge(inner_bS[1] + 1*4 + 3*4*topbot, indexA_inner, alpha_inner, beta_inner, y, 1, 1, Y_dy, maxY, 1, bit_depth_luma_minus8);
        //printf("\n"); // cubu

      }
      else
      {
        for (j=0; j<3; j += 1)
        {
          //printf("edge: %d ", j+1); // CUBU DEBUG
          filter_one_edge(inner_bS[1] + j*4 + 3*4*topbot, indexA_inner, alpha_inner, beta_inner, y, 1, 1, Y_dy, maxY, 1, bit_depth_luma_minus8);
          //printf("\n"); // cubu

          y += 4*Y_dy;
        }
      }
      //printf("\n"); // CUBU DEBUG
    }
    cubu_debug = 0; // CUBU DEBUG


    //cubu_debug = 1; // CUBU DEBUG

    // Chroma filtering
    if (cfidc > 0)
    {
        // top and then bottom mb
      for (topbot=0; topbot<2; topbot++)
      {
        //printf("MB %d\n", (mb_row>>1)*PicWidthInMbs*2+mb_col*2+topbot); // CUBU DEBUG
        for (iCbCr = 0; iCbCr < 2; iCbCr++)
        {
          //printf("iCbCr = %d - ", iCbCr); // cubu
          // do vertical filtering
          c = C[iCbCr] + (curr_is_field? PicWidthC : mbc_height*PicWidthC) * topbot;

          // 1st col
          if ( mb_col>0 && *(uint64_t*)(left_bS+topbot*8) != 0)
          {
            //if (!curr_is_field && left_mb_attr->mb_field_decoding_flag)
            //{
              filter_left_vert_mbaff_edge(left_bS+topbot*8, left_qPav[iCbCr+1]+topbot*8, c, FilterOffsetA, FilterOffsetB,
                                          0, C_dy, maxC, 0,
                                          mbc_sub_height, bit_depth_chroma_minus8);
            //}
            //else
            //{
            //  filter_left_vert_mbaff_edge(left_bS+topbot*8, left_qPav[iCbCr+1]+topbot*8, c, FilterOffsetA, FilterOffsetB,
            //                              0, C_dy, maxC, 0,
            //                              mbc_sub_height, bit_depth_chroma_minus8);
            //}
          }

          c += 4;
          // 2nd-4th col for top and bot mb
          indexA_inner = im_clip(0, 51, inner_qPav[iCbCr+1][topbot] + FilterOffsetA);
          alpha_inner = indexA_to_alpha[indexA_inner] << bit_depth_chroma_minus8;
          indexB = im_clip(0, 51, inner_qPav[iCbCr+1][topbot] + FilterOffsetB);
          beta_inner = indexB_to_beta[indexB] << bit_depth_chroma_minus8;
          for (j=mbc_sub_width-1; j<3; j += mbc_sub_width)
          {
            filter_one_edge(inner_bS[0] + j*4 + 3*4*topbot, indexA_inner, alpha_inner, beta_inner, c, 0, 0, C_dy, maxC, mbc_sub_height, bit_depth_chroma_minus8);
            c += 4;
          }
          //printf("\n"); // cubu
        }
        for (iCbCr = 0; iCbCr < 2; iCbCr++) // cubu -> remove !
        {
          //printf("iCbCr = %d - ", iCbCr); // cubu

          // do horizontal filtering
          c = C[iCbCr] + (curr_is_field? PicWidthC : mbc_height*PicWidthC) * topbot;

          // 1st row
          if ( mb_row >0 || (topbot && !curr_is_field))
          {
            if (!topbot && !curr_is_field && upper_mb_attr->mb_field_decoding_flag)
            {
              if (upper_bS[1][0] != 0 || upper_bS[1][1] != 0 || upper_bS[1][2] != 0 || upper_bS[1][3] != 0)
              {
                indexA = im_clip(0, 51, upper_qPav[iCbCr+1][1] + FilterOffsetA);
                alpha = indexA_to_alpha[indexA] << bit_depth_chroma_minus8;
                indexB = im_clip(0, 51, upper_qPav[iCbCr+1][1] + FilterOffsetB);
                beta = indexB_to_beta[indexB] << bit_depth_chroma_minus8;
                filter_one_edge(upper_bS[1], indexA, alpha, beta, c, 1, 0, PicWidthC*2, maxC, mbc_sub_width, bit_depth_chroma_minus8);
              }
              if (upper_bS[0][0] != 0 || upper_bS[0][1] != 0 || upper_bS[0][2] != 0 || upper_bS[0][3] != 0)
              {
                indexA = im_clip(0, 51, upper_qPav[iCbCr+1][0] + FilterOffsetA);
                alpha = indexA_to_alpha[indexA] << bit_depth_chroma_minus8;
                indexB = im_clip(0, 51, upper_qPav[iCbCr+1][0] + FilterOffsetB);
                beta = indexB_to_beta[indexB] << bit_depth_chroma_minus8;
                filter_one_edge(upper_bS[0], indexA, alpha, beta, c+PicWidthC, 1, 0, PicWidthC*2, maxC, mbc_sub_width, bit_depth_chroma_minus8);
              }
            }
            else if (upper_bS[1+topbot][0] != 0 || upper_bS[1+topbot][1] != 0 || upper_bS[1+topbot][2] != 0 || upper_bS[1+topbot][3] != 0)
            {
              indexA = im_clip(0, 51, upper_qPav[iCbCr+1][1+topbot] + FilterOffsetA);
              alpha = indexA_to_alpha[indexA] << bit_depth_chroma_minus8;
              indexB = im_clip(0, 51, upper_qPav[iCbCr+1][1+topbot] + FilterOffsetB);
              beta = indexB_to_beta[indexB] << bit_depth_chroma_minus8;
              filter_one_edge(upper_bS[1+topbot], indexA, alpha, beta, c, 1, 0, C_dy, maxC, mbc_sub_width, bit_depth_chroma_minus8);
            }
          }

          c += 4*C_dy;
          // 2nd-4th row of top mb
          // inner params are already calculated !
          for (j=mbc_sub_height-1; j<3; j += mbc_sub_height)
          {
            filter_one_edge(inner_bS[1] + j*4 + 3*4*topbot, indexA_inner, alpha_inner, beta_inner, c, 1, 0, C_dy, maxC, mbc_sub_width, bit_depth_chroma_minus8);
            c += 4*C_dy;
          }
          //printf("\n"); // cubu
        }


      } // end of iCbCr
      cubu_debug = 0; // CUBU DEBUG

    }
  }
}



void print_mb_samples2(struct rv264macro* curr_mb_attr, unsigned int mb_col, unsigned int mb_row, pixel_t* Y_samples, pixel_t* Cb_samples, pixel_t* Cr_samples, int PicWidthY, int PicWidthC)
{
  int i, j;
  pixel_t* row = Y_samples;
  pixel_t* Y;
  pixel_t* Cb;
  pixel_t* Cr;
  printf("MB (%d,%d) - mb_type:%d\n", mb_col, mb_row, curr_mb_attr->mb_type);
  printf("Luma\n");
  for (j=0; j<16; j++)
  {
    Y = row;
    for (i=0; i<16; i++)
    {
      printf("%3d ", *Y++);
    }
    printf("\n");
    row += PicWidthY;
  }
  LUD_DEBUG_ASSERT(PicWidthY/2 == PicWidthC); // dirty check: cfidc==1 (actually it could be 2 also....)
  row = Cb_samples;
  printf("Chroma Cb\n");
  for (j=0; j<8; j++)
  {
    Cb = row;
    for (i=0; i<8; i++)
    {
      printf("%3d ", *Cb++);
    }
    printf("\n");
    row += PicWidthC;
  }
  row = Cr_samples;
  printf("Chroma Cr\n");
  for (j=0; j<8; j++)
  {
    Cr = row;
    for (i=0; i<8; i++)
    {
      printf("%3d ", *Cr++);
    }
    printf("\n");
    row += PicWidthC;
  }


}


// TODO: hard code some params (ex: cfidc) in order to optimize the compiled code
void filter_image(struct rv264picture* pic, unsigned int bottom_field_flag)
{
  struct slice_header* slice0 = pic->field_sh[bottom_field_flag][0]; // get a slice header. It is used for variables that are the same for the whole picture
  unsigned int mb_row, mb_col/*, mb_idx*/;
  struct rv264seq_parameter_set* sps = slice0->sps;
  unsigned int PicWidthInMbs = sps->PicWidthInMbs;
  unsigned int PicWidthY = PicWidthInMbs * 16;
  unsigned int PicHeightInMbs = slice0->PicHeightInMbs;
  unsigned int MbaffFrameFlag = slice0->MbaffFrameFlag;
  struct rv264macro* curr_mb_attr = pic->field_data[bottom_field_flag].mb_attr;
  unsigned int cfidc = sps->chroma_format_idc;
  unsigned int mbc_width = MbWidthC[cfidc];
  unsigned int mbc_height = MbHeightC[cfidc];
  unsigned int mbc_size = mbc_width*mbc_height;
  unsigned int PicWidthC = PicWidthInMbs * mbc_width;
  unsigned int clipY = (1<<sps->BitDepthY)-1;
  unsigned int clipC = (1<<sps->BitDepthC)-1;
  unsigned int bit_depth_luma_minus8 = sps->bit_depth_luma_minus8;
  unsigned int bit_depth_chroma_minus8 = sps->bit_depth_chroma_minus8;
  unsigned int mbc_sub_width = SubWidthC[cfidc];
  unsigned int mbc_sub_height = SubHeightC[cfidc];
  unsigned int field_pic_flag = slice0->field_pic_flag;
  unsigned int is_field_pic_or_mbaff = MbaffFrameFlag || field_pic_flag;


  LUD_DEBUG_ASSERT(bit_depth_luma_minus8 == 0); // Not yet supported
  LUD_DEBUG_ASSERT(bit_depth_chroma_minus8 == 0); // Not yet supported

  pixel_t* Y;
  pixel_t* C[2];
  pixel_t* y;
  pixel_t* c[2];

  Y = pic->Y + (bottom_field_flag!=0)*PicWidthY;
  C[0] = pic->C[0] + (bottom_field_flag!=0)*PicWidthC;
  C[1] = pic->C[1] + (bottom_field_flag!=0)*PicWidthC;

  if (field_pic_flag) // Hereafter, PicWidthX is actually used as image stride, and not really as the width of the image
  {
    PicWidthY <<= 1;
    PicWidthC <<= 1;
  }

  for (mb_row = 0; mb_row<PicHeightInMbs; mb_row+=(1+MbaffFrameFlag))
  {
    y = Y;
    c[0] = C[0];
    c[1] = C[1];
    for (mb_col = 0; mb_col<PicWidthInMbs; mb_col++)
    {
      unsigned int slice_num = curr_mb_attr->slice_num;
      struct slice_header** sh = pic->field_sh[bottom_field_flag];
      unsigned int disable_deblocking_filter_idc = sh[slice_num]->disable_deblocking_filter_idc;
      unsigned int FilterOffsetA = sh[slice_num]->FilterOffsetA;
      unsigned int FilterOffsetB = sh[slice_num]->FilterOffsetB;
      if ( disable_deblocking_filter_idc != 1)
      {
        filter_mb(curr_mb_attr, mb_row, mb_col, disable_deblocking_filter_idc, slice_num, PicWidthInMbs,
            field_pic_flag, sh, PicWidthY, PicWidthC, y, c, clipY, clipC, mbc_sub_width, mbc_sub_height, mbc_height,
            cfidc, MbaffFrameFlag, FilterOffsetA, FilterOffsetB, bit_depth_luma_minus8, bit_depth_chroma_minus8,
            sh[slice_num]->RefPicList);
      }
      curr_mb_attr++;
      y += 16;
      c[0] += mbc_width;
      c[1] += mbc_width;
    }
    curr_mb_attr += MbaffFrameFlag * PicWidthInMbs;
    Y += 16*16 * (1+is_field_pic_or_mbaff) * PicWidthInMbs;
    C[0] += mbc_size * (1+is_field_pic_or_mbaff) * PicWidthInMbs;
    C[1] += mbc_size * (1+is_field_pic_or_mbaff) * PicWidthInMbs;
  }
}


