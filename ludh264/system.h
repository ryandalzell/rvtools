/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/



#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#include "decode.h"


struct rv264picture* set_ref_pic(PictureDecoderData* pdd, struct slice_header* sh);
struct rv264picture* allocate_and_set_non_existing_ref_pic(int PicOrderCnt, int TopFieldOrderCnt, int BottomFieldOrderCnt, int frame_num, int ref_struct, unsigned int bottom_field_flag);
struct rv264picture* alloc_picture(PictureDecoderData* pdd, struct slice_header* sh);
void check_free_picture(struct rv264picture* pic);
void release_picture_refpics(struct rv264picture* pic, unsigned int bottom_field_flag);


void use_picture(struct rv264picture* pic);
void use_picture_field(struct rv264picture* pic, unsigned int bottom_field_flag);
void use_picture_dec_data(struct rv264picture* pic);
void release_picture(struct rv264picture* pic);
void release_picture_field(struct rv264picture* pic, unsigned int bottom_field_flag);
void release_picture_field_struct(struct rv264picture* pic, unsigned int field_struct);
void release_picture_dec_data(struct rv264picture* pic);

#endif //__SYSTEM_H__
