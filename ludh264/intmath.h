/***************************************************************************
 *                                                                         *
 *     Copyright (C) 2008  ludrao.net                                      *
 *     ludh264@ludrao.net                                                  *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 *                                                                         *
 ***************************************************************************/


#ifndef __INTMATH_H__
#define __INTMATH_H__

#include <inttypes.h>

#include "common.h"

int32_t NEG_SSR32( int32_t a, int8_t s);
uint16_t bswap_16(uint16_t x);
uint32_t bswap_32(uint32_t x);
uint64_t bswap_64(uint64_t x);
int im_log2(unsigned int v);
int im_ceillog2(unsigned int v);
int im_log2_9b(unsigned int v);
int im_scale2(int v, int s);
int im_scale2_biased(int v, unsigned int s);

// TODO: optimize min, max, abs, clip functions (different implementation on different arch)
#define im_min(a,b)  ((a)<(b) ? (a) : (b))
#define im_max(a,b)  ((a)>(b) ? (a) : (b))
#define im_abs(a) ((a)<0 ? -(a) : (a))

// Output of clip is r where i <= r <= s
//#define im_clip(i, s, x) im_max(im_min((s), (x)), (i))
#define im_clip(i, s, x) ((x) < (i) ? (i) : (x) > (s) ? (s) : (x))

#endif //__INTMATH_H__
