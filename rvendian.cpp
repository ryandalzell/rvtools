/*
 * Description: Functions to swap endianness.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2008-11-28 14:42:57 $
 * Revision   : $Revision: 1.3 $
 * Copyright  : (c) 2006 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

short swapendians(short d)
{
    short d0 = (d >>  0) & 0xff;
    short d1 = (d >>  8) & 0xff;
    return (d0 << 8) | (d1 << 0);
}

long swapendian3(long d)
{
    long d0 = (d >>  0) & 0xff;
    long d1 = (d >>  8) & 0xff;
    long d2 = (d >> 16) & 0xff;
    return (d0 << 16) | (d1 << 8) | (d2 << 0);
}

int swapendian(int d)
{
    long d0 = (d >>  0) & 0xff;
    long d1 = (d >>  8) & 0xff;
    long d2 = (d >> 16) & 0xff;
    long d3 = (d >> 24) & 0xff;
    return (d0 << 24) | (d1 << 16) | (d2 << 8) | (d3 << 0);
}

long long swapendianll(long long d)
{
    long long d0 = (d >>  0) & 0xff;
    long long d1 = (d >>  8) & 0xff;
    long long d2 = (d >> 16) & 0xff;
    long long d3 = (d >> 24) & 0xff;
    long long d4 = (d >> 32) & 0xff;
    long long d5 = (d >> 40) & 0xff;
    long long d6 = (d >> 48) & 0xff;
    long long d7 = (d >> 56) & 0xff;
    return (d0 << 56) | (d1 << 48) | (d2 << 40) | (d3 << 32) | (d4 << 24) | (d5 << 16) | (d6 << 8) | (d7 << 0);
}

