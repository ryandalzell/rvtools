/*
 * Description: Functions related to the H.264 standard.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2010-01-28 18:07:39 $
 * Revision   : $Revision: 1.29 $
 * Copyright  : (c) 2005 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "rvutil.h"
#include "rvh264.h"
#include "rvtransform.h"


/*
 * h.264 lookups.
 */
const double ChromaFormatFactor[4] = {1.0, 1.5, 2.0, 3.0};

const char *nal_start[2] = {"short", "long"};

/*
 * h.264 slice names
 */
const char *slice_type_name[] = {
    "p-slice",
    "b-slice",
    "i-slice",
    "sp-slice",
    "si-slice",
    "p-slice",
    "b-slice",
    "i-slice",
    "sp-slice",
    "si-slice"
};

/*
 * h.264 utility functions.
 */

int picture_is_field(const struct rv264picture *picture)
{
    return picture->sh[0]->field_pic_flag;
}

int picture_is_topfield(const struct rv264picture *picture)
{
    return picture->sh[0]->field_pic_flag && !picture->sh[0]->bottom_field_flag;
}

int picture_is_bottomfield(const struct rv264picture *picture)
{
    return picture->sh[0]->field_pic_flag && picture->sh[0]->bottom_field_flag;
}

int slice_is_i(slice_type_t slice_type)
{
    return (slice_type==SLICE_I || slice_type==SLICE_SI || slice_type==SLICE_CI || slice_type==SLICE_CSI);
}

int slice_is_p(slice_type_t slice_type)
{
    return (slice_type==SLICE_P || slice_type==SLICE_SP || slice_type==SLICE_CP || slice_type==SLICE_CSP);
}

int slice_is_b(slice_type_t slice_type)
{
    return (slice_type==SLICE_B || slice_type==SLICE_CB);
}

int macro_is_intra(struct rv264macro *macro)
{
    return (macro->mb_type>=I_NxN && macro->mb_type<=I_PCM);
}

int macro_is_intra4x4(struct rv264macro *macro)
{
    return (macro->mb_type==I_NxN && !macro->transform_size_8x8_flag);
}

int macro_is_intra8x8(struct rv264macro *macro)
{
    return (macro->mb_type==I_NxN && macro->transform_size_8x8_flag);
}

int macro_is_intra16x16(struct rv264macro *macro)
{
    return (macro->mb_type>I_NxN && macro->mb_type<I_PCM);
}

int macro_is_inter(struct rv264macro *macro)
{
    return (macro->mb_type>=P_L0_16x16);
}

int macro_is_skip(struct rv264macro *macro)
{
    return (macro->mb_type==P_Skip || macro->mb_type==B_Skip);
}

int ceillog2(int n)
{
    unsigned int i=0;
    n--;
    while (n) {
        n >>= 1;
        i++;
    }
    return i;
}

int median(int a, int b, int c)
{
    return mmax(mmin(a,b),mmin(mmax(a,b),c));
}

/* 5.7 mathematical functions */

#define InverseRasterScanX(a,b,c,d) (((a)%((d)/(b)))*(b))
#define InverseRasterScanY(a,b,c,d) (((a)/((d)/(b)))*(c))

#define Clip3(x,y,z) ((z)<(x)?(x):((z)>(y)?(y):(z)))

#define Clip1y(x) Clip3(0,(1<<sps->BitDepthY)-1,x)
#define Clip1c(x) Clip3(0,(1<<sps->BitDepthC)-1,x)

/*
 * h.264 housekeeping functions.
 */

struct rv264slice *alloc_264_slice(struct slice_header *sh)
{
    /* allocate slice */
    size_t size = sizeof(struct rv264slice);
    struct rv264slice *slice = (struct rv264slice *)rvalloc(NULL, size, 1);
    slice->sh = sh;
    return slice;
}

void free_264_slice(struct rv264slice *slice)
{
    if (slice) {
        rvfree(slice->sh);
        rvfree(slice->macro);
        free(slice);
    }
}

struct rv264picture *alloc_264_picture()
{
    size_t size = sizeof(struct rv264picture);
    struct rv264picture *picture = (struct rv264picture *)rvalloc(NULL, size, 1);
    /* allocate slice array */
    picture->size_slice_array = 16;
    size = sizeof(struct slice_header);
    picture->sh = (struct slice_header **)rvalloc(NULL, picture->size_slice_array*size, 1);
    return picture;
}

void realloc_264_slice_array(struct rv264picture *picture, int size)
{
    if (size > picture->size_slice_array) {
        picture->size_slice_array += 16;
        picture->sh = (struct slice_header **)rvalloc(picture->sh, picture->size_slice_array*sizeof(struct slice_header), 0);
    }
}

void add_264_slice(struct rv264picture *picture, struct slice_header *sh)
{
    realloc_264_slice_array(picture, picture->num_slices+1);
    picture->sh[picture->num_slices] = (struct slice_header *) rvalloc(NULL, sizeof(struct slice_header), 0);
    memcpy(picture->sh[picture->num_slices], sh, sizeof(struct slice_header));
    picture->num_slices++;
}

void free_264_picture(struct rv264picture *picture)
{
    if (picture) {
        int i;
        /* struct rv264pic_parameter_set *pps does not belong to this struct */
        for (i=0; i<picture->num_slices; i++)
            rvfree(picture->sh[i]);
        rvfree(picture->sh);
        rvfree(picture->macro);
        free(picture);
    }
}

struct rv264frame *alloc_264_frame()
{
    size_t size = sizeof(struct rv264frame);
    struct rv264frame *frame = (struct rv264frame *)rvalloc(NULL, size, 1);
    return frame;
}

void free_264_frame(struct rv264frame *frame)
{
    if (frame) {
        if (frame->topfield) {
            free_264_picture(frame->topfield);
            frame->topfield = NULL;
        }
        if (frame->botfield) {
            free_264_picture(frame->botfield);
            frame->botfield = NULL;
        }
    }
}

struct rv264sequence *alloc_264_sequence()
{
    size_t size = sizeof(struct rv264sequence);
    struct rv264sequence *sequence = (struct rv264sequence *)rvalloc(NULL, size, 1);
    /* allocate frame array */
    sequence->size_frame_array = 256;
    sequence->frame = (struct rv264frame *)rvalloc(NULL, sequence->size_frame_array*sizeof(struct rv264frame), 1);
    /* initialise linked list */
    sequence->dpb.head = 0;
    sequence->dpb.tail = (OUTPICLIST*) &sequence->dpb.head; // a bit tricky, it works because next is a pointer and first member of the struct
    return sequence;
}

void realloc_264_frame_array(struct rv264sequence *sequence, int size)
{
    if (size > sequence->size_frame_array) {
        sequence->size_frame_array += 256;
        sequence->frame = (struct rv264frame *)rvalloc(sequence->frame, sequence->size_frame_array*sizeof(struct rv264frame), 0);
        memset(sequence->frame + (sequence->size_frame_array-256), 0, 256*sizeof(struct rv264frame));
    }
}

void free_264_sequence(struct rv264sequence *sequence)
{
    if (sequence) {
        int i;
        for (i=0; i<32; i++)
            rvfree(sequence->sps[i]);
        for (i=0; i<256; i++)
            rvfree(sequence->pps[i]);
        for (i=0; i<sequence->size_frame_array; i++)
            free_264_frame(sequence->frame+i);
        rvfree(sequence->frame);
        rvfree(sequence);
    }
}

/*
 * h.264 inverse scanning processes
 */

/* 6.4.1 inverse macroblock scanning */
void inverse_scan_macroblock(struct rv264macro *mb, int *x, int *y)
{
    int mbAddr = mb->mb_addr;

    if (mb->sh->MbaffFrameFlag==0) {
        *x = InverseRasterScanX(mbAddr, 16, 16, mb->sh->pps->sps->PicWidthInSamplesl);
        *y = InverseRasterScanY(mbAddr, 16, 16, mb->sh->pps->sps->PicWidthInSamplesl);
    } else {
        int xO = InverseRasterScanX(mbAddr/2, 16, 32, mb->sh->pps->sps->PicWidthInSamplesl);
        int yO = InverseRasterScanY(mbAddr/2, 16, 32, mb->sh->pps->sps->PicWidthInSamplesl);

        if (mb->mb_field_decoding_flag==0) {
            /* frame macroblock */
            *x = xO;
            *y = yO + (mbAddr%2) * 16;
        } else {
            /* field macroblock */
            *x = xO;
            *y = yO + (mbAddr%2);
        }
    }
}

/* 6.4.3 inverse 4x4 luma block scanning */
void inverse_scan_luma4x4(int luma4x4BlkIdx, int *x, int *y)
{
    *x = InverseRasterScanX(luma4x4BlkIdx/4, 8, 8, 16) + InverseRasterScanX(luma4x4BlkIdx%4, 4, 4, 8);
    *y = InverseRasterScanY(luma4x4BlkIdx/4, 8, 8, 16) + InverseRasterScanY(luma4x4BlkIdx%4, 4, 4, 8);
}

/* 6.4.5 inverse 8x8 luma block scanning */
void inverse_scan_luma8x8(int luma8x8BlkIdx, int *x, int *y)
{
    *x = InverseRasterScanX(luma8x8BlkIdx, 8, 8, 16);
    *x = InverseRasterScanY(luma8x8BlkIdx, 8, 8, 16);
}

/* 6.4.7 derivation process of the availability for macroblock addresses */
int derive_macroblock_availability(struct rv264macro *macro, int mbAddr)
{
    if (mbAddr<0)
        return mbAddr;
    // NOTE commented because compiler suggests is a tautology.
    //else if (mbAddr > macro->mb_addr)
    //    return -1;
    else if (mbAddr < macro->sh->first_mb_in_slice)
        return -1;
    else
        return mbAddr;
}

/* 6.4.8 derivation process for neighbouring macroblock addresses and their availability */
void derive_neighbouring_macroblocks(struct rv264macro *macro, int *mbAddrA, int *mbAddrB, int *mbAddrC, int *mbAddrD)
{
    int CurrMbAddr = macro->mb_addr;

    *mbAddrA = derive_macroblock_availability(macro, CurrMbAddr-1);
    if (CurrMbAddr%macro->sh->pps->sps->PicWidthInMbs == 0)
        *mbAddrA = -1;

    *mbAddrB = derive_macroblock_availability(macro, CurrMbAddr-macro->sh->pps->sps->PicWidthInMbs);

    *mbAddrC = derive_macroblock_availability(macro, CurrMbAddr-macro->sh->pps->sps->PicWidthInMbs+1);
    if ((CurrMbAddr+1)%macro->sh->pps->sps->PicWidthInMbs == 0)
        *mbAddrC = -1;

    *mbAddrD = derive_macroblock_availability(macro, CurrMbAddr-macro->sh->pps->sps->PicWidthInMbs-1);
    if (CurrMbAddr%macro->sh->pps->sps->PicWidthInMbs == 0)
        *mbAddrD = -1;

}

/* 6.4.11 derivation process for neighbouring locations */
void derive_neighbouring_location(struct rv264macro *macro, int xN, int yN, chroma_t chroma, int *mbAddrN, int *xW, int *yW)
{
    int maxW = !chroma? 16 : macro->sh->pps->sps->MbWidthC;
    int maxH = !chroma? 16 : macro->sh->pps->sps->MbHeightC;
    int CurrMbAddr = macro->mb_addr;

    if (macro->sh->MbaffFrameFlag==0) {
        /* 6.4.11.1 specification for neighbouring locations in fields and non-MBAFF frames */

        int mbAddrA, mbAddrB, mbAddrC, mbAddrD;

        derive_neighbouring_macroblocks(macro, &mbAddrA, &mbAddrB, &mbAddrC, &mbAddrD);

        if (xN<0 && yN<0)
            *mbAddrN = mbAddrD;
        else if (xN<0 && yN>=0 && yN<maxH)
            *mbAddrN = mbAddrA;
        else if (xN>=0 && xN<maxW && yN<0)
            *mbAddrN = mbAddrB;
        else if (xN>=0 && xN<maxW && yN>=0 && yN<maxH)
            *mbAddrN = CurrMbAddr;
        else if (xN>=maxW && yN<0)
            *mbAddrN = mbAddrC;
        else if (xN>=maxW && yN>=0 && yN<maxH)
            *mbAddrN = -1;
        else if (yN>=maxH)
            *mbAddrN = -1;

        if (*mbAddrN<0) {
            *xW = -1;
            *yW = -1;
        } else {
            *xW = (xN+maxW) % maxW;
            *yW = (yN+maxH) % maxH;
        }
    } else {
    }
}

/*
 * h.264 macroblock parameters
 */

int NumMbPart(struct rv264macro *mb)
{
    const struct slice_header *sh = mb->sh;

    if (slice_is_p(sh->slice_type)) {
        switch (mb->mb_type) {
            case P_L0_16x16   : return 1;
            case P_L0_L0_16x8 : return 2;
            case P_L0_L0_8x16 : return 2;
            case P_8x8        : return 4;
            case P_8x8ref0    : return 4;
            case P_Skip       : return 1;
            default           : return -1;
        }
    } else {
        switch (mb->mb_type) {
            case B_Direct_16x16 : return -1;
            case B_L0_16x16     : return 1;
            case B_L1_16x16     : return 1;
            case B_Bi_16x16     : return 1;
            case B_L0_L0_16x8   : return 2;
            case B_L0_L0_8x16   : return 2;
            case B_L1_L1_16x8   : return 2;
            case B_L1_L1_8x16   : return 2;
            case B_L0_L1_16x8   : return 2;
            case B_L0_L1_8x16   : return 2;
            case B_L1_L0_16x8   : return 2;
            case B_L1_L0_8x16   : return 2;
            case B_L0_Bi_16x8   : return 2;
            case B_L0_Bi_8x16   : return 2;
            case B_L1_Bi_16x8   : return 2;
            case B_L1_Bi_8x16   : return 2;
            case B_Bi_L0_16x8   : return 2;
            case B_Bi_L0_8x16   : return 2;
            case B_Bi_L1_16x8   : return 2;
            case B_Bi_L1_8x16   : return 2;
            case B_Bi_Bi_16x8   : return 2;
            case B_Bi_Bi_8x16   : return 2;
            case B_8x8          : return 4;
            case B_Skip         : return -1;
            default             : return -1;
        }
    }
}

part_type_t MbPartPredMode(struct rv264macro *mb, int list)
{
    const struct slice_header *sh = mb->sh;

    if (slice_is_i(sh->slice_type)) {
        if (mb->mb_type==I_NxN) {
            if (mb->transform_size_8x8_flag==0)
                return Intra_4x4;
            else
                return Intra_8x8;
        } else if (mb->mb_type==I_PCM)
            return na;
        else
            return Intra_16x16;
    } else if (slice_is_p(sh->slice_type)) {
        switch (mb->mb_type) {
            case P_L0_16x16   : return list==0? Pred_L0 :    na  ;
            case P_L0_L0_16x8 : return list==0? Pred_L0 : Pred_L0;
            case P_L0_L0_8x16 : return list==0? Pred_L0 : Pred_L0;
            case P_8x8        : return list==0? na      :    na  ;
            case P_8x8ref0    : return list==0? na      :    na  ;
            case P_Skip       : return list==0? Pred_L0 :    na  ;
            default           : return na;
        }
    } else {
        switch (mb->mb_type) {
            case B_Direct_16x16 : return list==0? Direct  :  na    ;
            case B_L0_16x16     : return list==0? Pred_L0 :  na    ;
            case B_L1_16x16     : return list==0? Pred_L1 :  na    ;
            case B_Bi_16x16     : return list==0? BiPred  :  na    ;
            case B_L0_L0_16x8   : return list==0? Pred_L0 : Pred_L0;
            case B_L0_L0_8x16   : return list==0? Pred_L0 : Pred_L0;
            case B_L1_L1_16x8   : return list==0? Pred_L1 : Pred_L1;
            case B_L1_L1_8x16   : return list==0? Pred_L1 : Pred_L1;
            case B_L0_L1_16x8   : return list==0? Pred_L0 : Pred_L1;
            case B_L0_L1_8x16   : return list==0? Pred_L0 : Pred_L1;
            case B_L1_L0_16x8   : return list==0? Pred_L1 : Pred_L0;
            case B_L1_L0_8x16   : return list==0? Pred_L1 : Pred_L0;
            case B_L0_Bi_16x8   : return list==0? Pred_L0 : BiPred ;
            case B_L0_Bi_8x16   : return list==0? Pred_L0 : BiPred ;
            case B_L1_Bi_16x8   : return list==0? Pred_L1 : BiPred ;
            case B_L1_Bi_8x16   : return list==0? Pred_L1 : BiPred ;
            case B_Bi_L0_16x8   : return list==0? BiPred  : Pred_L0;
            case B_Bi_L0_8x16   : return list==0? BiPred  : Pred_L0;
            case B_Bi_L1_16x8   : return list==0? BiPred  : Pred_L1;
            case B_Bi_L1_8x16   : return list==0? BiPred  : Pred_L1;
            case B_Bi_Bi_16x8   : return list==0? BiPred  : BiPred ;
            case B_Bi_Bi_8x16   : return list==0? BiPred  : BiPred ;
            case B_8x8          : return list==0? na      :  na    ;
            case B_Skip         : return list==0? Direct  :  na    ;
            default             : return na;
        }
    }
}

int Intra16x16PredMode(struct rv264macro *mb)
{
    return (mb->mb_type-1) & 0x3;
}

int CodedBlockPatternLuma(struct rv264macro *mb)
{
    if (mb->mb_type>=I_16x16_0_0_0 && mb->mb_type<=I_16x16_3_2_1)
        return mb->mb_type<=I_16x16_3_2_0? 0 : 15;
    else
        /* equation 7-33 */
        return mb->coded_block_pattern%16;
}

int CodedBlockPatternChroma(struct rv264macro *mb)
{
    if (mb->mb_type>=I_16x16_0_0_0 && mb->mb_type<=I_16x16_3_2_1) {
        switch (mb->mb_type) {
            case I_16x16_0_0_0 : return 0;
            case I_16x16_1_0_0 : return 0;
            case I_16x16_2_0_0 : return 0;
            case I_16x16_3_0_0 : return 0;
            case I_16x16_0_1_0 : return 1;
            case I_16x16_1_1_0 : return 1;
            case I_16x16_2_1_0 : return 1;
            case I_16x16_3_1_0 : return 1;
            case I_16x16_0_2_0 : return 2;
            case I_16x16_1_2_0 : return 2;
            case I_16x16_2_2_0 : return 2;
            case I_16x16_3_2_0 : return 2;
            case I_16x16_0_0_1 : return 0;
            case I_16x16_1_0_1 : return 0;
            case I_16x16_2_0_1 : return 0;
            case I_16x16_3_0_1 : return 0;
            case I_16x16_0_1_1 : return 1;
            case I_16x16_1_1_1 : return 1;
            case I_16x16_2_1_1 : return 1;
            case I_16x16_3_1_1 : return 1;
            case I_16x16_0_2_1 : return 2;
            case I_16x16_1_2_1 : return 2;
            case I_16x16_2_2_1 : return 2;
            case I_16x16_3_2_1 : return 2;
            default : return -1;
        }
    } else
        /* equation 7-33 */
        return mb->coded_block_pattern/16;
}

part_type_t SubMbPredMode(struct rv264macro *mb, int mbPartIdx)
{
    if (slice_is_p(mb->slice_type)) {
        switch (mb->sub_mb_type[mbPartIdx]) {
            case P_L0_8x8 : return Pred_L0;
            case P_L0_8x4 : return Pred_L0;
            case P_L0_4x8 : return Pred_L0;
            case P_L0_4x4 : return Pred_L0;
            default       : return na     ;
        }
    } else if (slice_is_b(mb->slice_type)) {
        switch (mb->sub_mb_type[mbPartIdx]) {
            case B_Direct_8x8 : return  Direct;
            case B_L0_8x8     : return Pred_L0;
            case B_L1_8x8     : return Pred_L1;
            case B_Bi_8x8     : return  BiPred;
            case B_L0_8x4     : return Pred_L0;
            case B_L0_4x8     : return Pred_L0;
            case B_L1_8x4     : return Pred_L1;
            case B_L1_4x8     : return Pred_L1;
            case B_Bi_8x4     : return  BiPred;
            case B_Bi_4x8     : return  BiPred;
            case B_L0_4x4     : return Pred_L0;
            case B_L1_4x4     : return Pred_L1;
            case B_Bi_4x4     : return  BiPred;
            default           : return  Direct;
        }
    } else
        return na;
}

int NumSubMbPart(struct rv264macro *mb, int mbPartIdx)
{
    if (slice_is_p(mb->slice_type)) {
        switch (mb->sub_mb_type[mbPartIdx]) {
            case P_L0_8x8 : return 1;
            case P_L0_8x4 : return 2;
            case P_L0_4x8 : return 2;
            case P_L0_4x4 : return 4;
            default       : return -1;
        }
    } else if (slice_is_b(mb->slice_type)) {
        switch (mb->sub_mb_type[mbPartIdx]) {
            case B_Direct_8x8 : return 4;
            case B_L0_8x8     : return 1;
            case B_L1_8x8     : return 1;
            case B_Bi_8x8     : return 1;
            case B_L0_8x4     : return 2;
            case B_L0_4x8     : return 2;
            case B_L1_8x4     : return 2;
            case B_L1_4x8     : return 2;
            case B_Bi_8x4     : return 2;
            case B_Bi_4x8     : return 2;
            case B_L0_4x4     : return 4;
            case B_L1_4x4     : return 4;
            case B_Bi_4x4     : return 4;
            default           : return 4;
        }
    } else
        return na;
}

/*
 * h.264 neighbour availability
 */
void getMacroblockNeighboursAB(int mb_addr, int PicWidthInMbs, int *mbAddrA, int *mbAddrB)
{
    /* check left macroblock address */
    if (mb_addr%PicWidthInMbs==0)
        *mbAddrA = -1;
    else
        *mbAddrA = mb_addr;

    /* check top macroblock address */
    *mbAddrB = mb_addr - PicWidthInMbs;

    /* NOTE negative macroblock addresses are not available */
}

void getLuma8x8NeighboursAB(int mb_addr, int luma8x8BlkIdx, int PicWidthInMbs, int *mbAddrA, int *luma8x8BlkIdxA, int *mbAddrB, int *luma8x8BlkIdxB)
{
    /* first lookup luma8x8BlkIdx neighbours TODO use an array */
    switch (luma8x8BlkIdx) {
        case 0  : *luma8x8BlkIdxA = 1;  *luma8x8BlkIdxB = 2; break;
        case 1  : *luma8x8BlkIdxA = 0;  *luma8x8BlkIdxB = 3; break;
        case 2  : *luma8x8BlkIdxA = 3;  *luma8x8BlkIdxB = 0; break;
        case 3  : *luma8x8BlkIdxA = 2;  *luma8x8BlkIdxB = 1; break;
    }

    /* check left macroblock address */
    *mbAddrA = mb_addr;
    if (*luma8x8BlkIdxA > luma8x8BlkIdx) {
        if (mb_addr%PicWidthInMbs==0)
            *mbAddrA = -1;
        else
            (*mbAddrA)--;
    }

    /* check top macroblock address */
    *mbAddrB = mb_addr;
    if (*luma8x8BlkIdxB > luma8x8BlkIdx)
        *mbAddrB -= PicWidthInMbs;

    /* NOTE negative macroblock addresses are not available */
}

void getLuma4x4NeighboursAB(int mb_addr, int luma4x4BlkIdx, int PicWidthInMbs, int *mbAddrA, int *luma4x4BlkIdxA, int *mbAddrB, int *luma4x4BlkIdxB)
{
    /* first lookup luma4x4BlkIdx neighbours TODO use an array */
    switch (luma4x4BlkIdx) {
        case 0  : *luma4x4BlkIdxA = 5;  *luma4x4BlkIdxB = 10; break;
        case 1  : *luma4x4BlkIdxA = 0;  *luma4x4BlkIdxB = 11; break;
        case 2  : *luma4x4BlkIdxA = 7;  *luma4x4BlkIdxB = 0;  break;
        case 3  : *luma4x4BlkIdxA = 2;  *luma4x4BlkIdxB = 1;  break;
        case 4  : *luma4x4BlkIdxA = 1;  *luma4x4BlkIdxB = 14; break;
        case 5  : *luma4x4BlkIdxA = 4;  *luma4x4BlkIdxB = 15; break;
        case 6  : *luma4x4BlkIdxA = 3;  *luma4x4BlkIdxB = 4;  break;
        case 7  : *luma4x4BlkIdxA = 6;  *luma4x4BlkIdxB = 5;  break;
        case 8  : *luma4x4BlkIdxA = 13; *luma4x4BlkIdxB = 2;  break;
        case 9  : *luma4x4BlkIdxA = 8;  *luma4x4BlkIdxB = 3;  break;
        case 10 : *luma4x4BlkIdxA = 15; *luma4x4BlkIdxB = 8;  break;
        case 11 : *luma4x4BlkIdxA = 10; *luma4x4BlkIdxB = 9;  break;
        case 12 : *luma4x4BlkIdxA = 9;  *luma4x4BlkIdxB = 6;  break;
        case 13 : *luma4x4BlkIdxA = 12; *luma4x4BlkIdxB = 7;  break;
        case 14 : *luma4x4BlkIdxA = 11; *luma4x4BlkIdxB = 12; break;
        case 15 : *luma4x4BlkIdxA = 14; *luma4x4BlkIdxB = 13; break;
    }

    /* check left macroblock address */
    *mbAddrA = mb_addr;
    if (*luma4x4BlkIdxA > luma4x4BlkIdx) {
        if (mb_addr%PicWidthInMbs==0)
            *mbAddrA = -1;
        else
            (*mbAddrA)--;
    }

    /* check top macroblock address */
    *mbAddrB = mb_addr;
    if (*luma4x4BlkIdxB > luma4x4BlkIdx)
        *mbAddrB -= PicWidthInMbs;

    /* NOTE negative macroblock addresses are not available */
}

void getLuma4x4NeighboursCD(int mb_addr, int luma4x4BlkIdx, int part_width, int PicWidthInMbs, int *mbAddrC, int *luma4x4BlkIdxC, int *mbAddrD, int *luma4x4BlkIdxD)
{
    assert(part_width==4 || part_width==8 || part_width==16);

    /* first lookup luma4x4BlkIdx neighbours TODO use an array */
    switch (luma4x4BlkIdx) {
        case 0  : if (part_width==4) *luma4x4BlkIdxC = 11; else if (part_width==8) *luma4x4BlkIdxC = 13; else *luma4x4BlkIdxC = 10; *luma4x4BlkIdxD = 15; break;
        case 1  : if (part_width==4) *luma4x4BlkIdxC = 14; else if (part_width==8) *luma4x4BlkIdxC = 13; else *luma4x4BlkIdxC = 10; *luma4x4BlkIdxD = 10; break;
        case 2  : if (part_width==4) *luma4x4BlkIdxC = 1;  else if (part_width==8) *luma4x4BlkIdxC = 4;  else *luma4x4BlkIdxC = 0;  *luma4x4BlkIdxD = 5;  break;
        case 3  : if (part_width==4) *luma4x4BlkIdxC = 4;  else if (part_width==8) *luma4x4BlkIdxC = 4;  else *luma4x4BlkIdxC = 0;  *luma4x4BlkIdxD = 0;  break;
        case 4  : if (part_width==4) *luma4x4BlkIdxC = 15; else if (part_width==8) *luma4x4BlkIdxC = 10; else *luma4x4BlkIdxC = 10; *luma4x4BlkIdxD = 11; break;
        case 5  : if (part_width==4) *luma4x4BlkIdxC = 10; else if (part_width==8) *luma4x4BlkIdxC = 10; else *luma4x4BlkIdxC = 10; *luma4x4BlkIdxD = 14; break;
        case 6  : if (part_width==4) *luma4x4BlkIdxC = 5;  else if (part_width==8) *luma4x4BlkIdxC = 0;  else *luma4x4BlkIdxC = 0;  *luma4x4BlkIdxD = 1;  break;
        case 7  : if (part_width==4) *luma4x4BlkIdxC = 0;  else if (part_width==8) *luma4x4BlkIdxC = 0;  else *luma4x4BlkIdxC = 0;  *luma4x4BlkIdxD = 4;  break;
        case 8  : if (part_width==4) *luma4x4BlkIdxC = 3;  else if (part_width==8) *luma4x4BlkIdxC = 6;  else *luma4x4BlkIdxC = 2;  *luma4x4BlkIdxD = 7;  break;
        case 9  : if (part_width==4) *luma4x4BlkIdxC = 6;  else if (part_width==8) *luma4x4BlkIdxC = 6;  else *luma4x4BlkIdxC = 2;  *luma4x4BlkIdxD = 2;  break;
        case 10 : if (part_width==4) *luma4x4BlkIdxC = 9;  else if (part_width==8) *luma4x4BlkIdxC = 12; else *luma4x4BlkIdxC = 8;  *luma4x4BlkIdxD = 13; break;
        case 11 : if (part_width==4) *luma4x4BlkIdxC = 12; else if (part_width==8) *luma4x4BlkIdxC = 12; else *luma4x4BlkIdxC = 8;  *luma4x4BlkIdxD = 8;  break;
        case 12 : if (part_width==4) *luma4x4BlkIdxC = 7;  else if (part_width==8) *luma4x4BlkIdxC = 2;  else *luma4x4BlkIdxC = 2;  *luma4x4BlkIdxD = 3;  break;
        case 13 : if (part_width==4) *luma4x4BlkIdxC = 2;  else if (part_width==8) *luma4x4BlkIdxC = 2;  else *luma4x4BlkIdxC = 2;  *luma4x4BlkIdxD = 6;  break;
        case 14 : if (part_width==4) *luma4x4BlkIdxC = 13; else if (part_width==8) *luma4x4BlkIdxC = 8;  else *luma4x4BlkIdxC = 8;  *luma4x4BlkIdxD = 9;  break;
        case 15 : if (part_width==4) *luma4x4BlkIdxC = 8;  else if (part_width==8) *luma4x4BlkIdxC = 8;  else *luma4x4BlkIdxC = 8;  *luma4x4BlkIdxD = 12; break;
    }

    /* check top right macroblock address */
    *mbAddrC = mb_addr;
    if (luma4x4BlkIdx==0 || luma4x4BlkIdx==1 || luma4x4BlkIdx==4 || luma4x4BlkIdx==5)
        *mbAddrC -= PicWidthInMbs;
    if (luma4x4BlkIdx==5 || (luma4x4BlkIdx==4 && part_width==8) || (luma4x4BlkIdx==0 && part_width==16)) {
        if (mb_addr%PicWidthInMbs==PicWidthInMbs-1)
            *mbAddrC = -1;
        else
            (*mbAddrC)++;
    }
    if ((luma4x4BlkIdx==3 || luma4x4BlkIdx==7 || luma4x4BlkIdx==11 || luma4x4BlkIdx==13 || luma4x4BlkIdx==15) && part_width==4)
        *mbAddrC = -1;
    if ((luma4x4BlkIdx==6 || luma4x4BlkIdx==12 || luma4x4BlkIdx==14) && part_width==8)
        *mbAddrC = -1;
    if ((luma4x4BlkIdx==2 || luma4x4BlkIdx==8 || luma4x4BlkIdx==10) && part_width==16)
        *mbAddrC = -1;

    /* check top left macroblock address */
    *mbAddrD = mb_addr;
    if (luma4x4BlkIdx==0 || luma4x4BlkIdx==1 || luma4x4BlkIdx==4 || luma4x4BlkIdx==5)
        *mbAddrD -= PicWidthInMbs;
    if (luma4x4BlkIdx==0 || luma4x4BlkIdx==2 || luma4x4BlkIdx==8 || luma4x4BlkIdx==10) {
        if (mb_addr%PicWidthInMbs==0)
            *mbAddrD = -1;
        else
            (*mbAddrD)--;
    }

    /* NOTE negative macroblock addresses are not available */
}

void getChroma4x4NeighboursAB(int mb_addr, int chroma4x4BlkIdx, int PicWidthInMbs, int *mbAddrA, int *chroma4x4BlkIdxA, int *mbAddrB, int *chroma4x4BlkIdxB)
{
    /* TODO add support for chroma_format_idc */
    if (chroma4x4BlkIdx>4)
        rvexit("getChroma4x4NeighboursAB: time to implement 4:2:2 support");

    /* first lookup chroma4x4BlkIdx neighbours TODO use an array */
    switch (chroma4x4BlkIdx) {
        case 0  : *chroma4x4BlkIdxA = 1;  *chroma4x4BlkIdxB = 2; break;
        case 1  : *chroma4x4BlkIdxA = 0;  *chroma4x4BlkIdxB = 3; break;
        case 2  : *chroma4x4BlkIdxA = 3;  *chroma4x4BlkIdxB = 0; break;
        case 3  : *chroma4x4BlkIdxA = 2;  *chroma4x4BlkIdxB = 1; break;
    }

    /* check left macroblock address */
    *mbAddrA = mb_addr;
    if (*chroma4x4BlkIdxA > chroma4x4BlkIdx) {
        if (mb_addr%PicWidthInMbs==0)
            *mbAddrA = -1;
        else
            (*mbAddrA)--;
    }

    /* check top macroblock address */
    *mbAddrB = mb_addr;
    if (*chroma4x4BlkIdxB > chroma4x4BlkIdx)
        *mbAddrB -= PicWidthInMbs;

    /* NOTE negative macroblock addresses are not available */
}

/*
 * h.264 table lookups.
 */

/*static char *binstring(int code, int len)
{
    static char s[32];
    int i, m;

    memset(s, '0', 32);
    for (i=0, m=1<<(len-1); i<len; i++, m>>=1) {
        int bit = code & m;
        if (bit)
            s[i] = '1';
    }
    s[i] = '\0';
    return s;
}*/

struct coeff_token_t parse_coeff_token(struct bitbuf *bb, int nC)
{
    struct coeff_token_t coeff_token = {0, 0};
    switch (nC) {
        case -1:
        if (showbits(bb,  2) ==  1) /*               01 */ { flushbits(bb,  2); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=0; break; }
        if (showbits(bb,  6) ==  7) /*           000111 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=1; break; }
        if (showbits(bb,  1) ==  1) /*                1 */ { flushbits(bb,  1); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=1; break; }
        if (showbits(bb,  6) ==  4) /*           000100 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=2; break; }
        if (showbits(bb,  6) ==  6) /*           000110 */ { flushbits(bb,  6); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=2; break; }
        if (showbits(bb,  3) ==  1) /*              001 */ { flushbits(bb,  3); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=2; break; }
        if (showbits(bb,  6) ==  3) /*           000011 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  7) ==  3) /*          0000011 */ { flushbits(bb,  7); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  7) ==  2) /*          0000010 */ { flushbits(bb,  7); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  6) ==  5) /*           000101 */ { flushbits(bb,  6); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  6) ==  2) /*           000010 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb,  8) ==  3) /*         00000011 */ { flushbits(bb,  8); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb,  8) ==  2) /*         00000010 */ { flushbits(bb,  8); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb,  7) ==  0) /*          0000000 */ { flushbits(bb,  7); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=4; break; }
        break;
        case -2:
        if (showbits(bb,  1) ==  1) /*                1 */ { flushbits(bb,  1); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=0; break; }
        if (showbits(bb,  7) == 15) /*          0001111 */ { flushbits(bb,  7); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=1; break; }
        if (showbits(bb,  2) ==  1) /*               01 */ { flushbits(bb,  2); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=1; break; }
        if (showbits(bb,  7) == 14) /*          0001110 */ { flushbits(bb,  7); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=2; break; }
        if (showbits(bb,  7) == 13) /*          0001101 */ { flushbits(bb,  7); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=2; break; }
        if (showbits(bb,  3) ==  1) /*              001 */ { flushbits(bb,  3); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=2; break; }
        if (showbits(bb,  9) ==  7) /*        000000111 */ { flushbits(bb,  9); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  7) == 12) /*          0001100 */ { flushbits(bb,  7); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  7) == 11) /*          0001011 */ { flushbits(bb,  7); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  5) ==  1) /*            00001 */ { flushbits(bb,  5); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  9) ==  6) /*        000000110 */ { flushbits(bb,  9); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb,  9) ==  5) /*        000000101 */ { flushbits(bb,  9); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb,  7) == 10) /*          0001010 */ { flushbits(bb,  7); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb,  6) ==  1) /*           000001 */ { flushbits(bb,  6); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb, 10) ==  7) /*       0000000111 */ { flushbits(bb, 10); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=5; break; }
        if (showbits(bb, 10) ==  6) /*       0000000110 */ { flushbits(bb, 10); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=5; break; }
        if (showbits(bb,  9) ==  4) /*        000000100 */ { flushbits(bb,  9); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=5; break; }
        if (showbits(bb,  7) ==  9) /*          0001001 */ { flushbits(bb,  7); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=5; break; }
        if (showbits(bb, 11) ==  7) /*      00000000111 */ { flushbits(bb, 11); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=6; break; }
        if (showbits(bb, 11) ==  6) /*      00000000110 */ { flushbits(bb, 11); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=6; break; }
        if (showbits(bb, 10) ==  5) /*       0000000101 */ { flushbits(bb, 10); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=6; break; }
        if (showbits(bb,  7) ==  8) /*          0001000 */ { flushbits(bb,  7); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=6; break; }
        if (showbits(bb, 12) ==  7) /*     000000000111 */ { flushbits(bb, 12); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=7; break; }
        if (showbits(bb, 12) ==  6) /*     000000000110 */ { flushbits(bb, 12); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=7; break; }
        if (showbits(bb, 11) ==  5) /*      00000000101 */ { flushbits(bb, 11); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=7; break; }
        if (showbits(bb, 10) ==  4) /*       0000000100 */ { flushbits(bb, 10); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=7; break; }
        if (showbits(bb, 13) ==  7) /*    0000000000111 */ { flushbits(bb, 13); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=8; break; }
        if (showbits(bb, 12) ==  5) /*     000000000101 */ { flushbits(bb, 12); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=8; break; }
        if (showbits(bb, 12) ==  4) /*     000000000100 */ { flushbits(bb, 12); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=8; break; }
        if (showbits(bb, 11) ==  4) /*      00000000100 */ { flushbits(bb, 11); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=8; break; }
        break;
        case 0:
        case 1:
        if (showbits(bb,  1) ==  1) /*                1 */ { flushbits(bb,  1); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=0; break; }
        if (showbits(bb,  6) ==  5) /*           000101 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=1; break; }
        if (showbits(bb,  2) ==  1) /*               01 */ { flushbits(bb,  2); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=1; break; }
        if (showbits(bb,  8) ==  7) /*         00000111 */ { flushbits(bb,  8); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=2; break; }
        if (showbits(bb,  6) ==  4) /*           000100 */ { flushbits(bb,  6); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=2; break; }
        if (showbits(bb,  3) ==  1) /*              001 */ { flushbits(bb,  3); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=2; break; }
        if (showbits(bb,  9) ==  7) /*        000000111 */ { flushbits(bb,  9); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  8) ==  6) /*         00000110 */ { flushbits(bb,  8); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  7) ==  5) /*          0000101 */ { flushbits(bb,  7); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  5) ==  3) /*            00011 */ { flushbits(bb,  5); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb, 10) ==  7) /*       0000000111 */ { flushbits(bb, 10); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb,  9) ==  6) /*        000000110 */ { flushbits(bb,  9); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb,  8) ==  5) /*         00000101 */ { flushbits(bb,  8); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb,  6) ==  3) /*           000011 */ { flushbits(bb,  6); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb, 11) ==  7) /*      00000000111 */ { flushbits(bb, 11); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=5; break; }
        if (showbits(bb, 10) ==  6) /*       0000000110 */ { flushbits(bb, 10); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=5; break; }
        if (showbits(bb,  9) ==  5) /*        000000101 */ { flushbits(bb,  9); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=5; break; }
        if (showbits(bb,  7) ==  4) /*          0000100 */ { flushbits(bb,  7); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=5; break; }
        if (showbits(bb, 13) == 15) /*    0000000001111 */ { flushbits(bb, 13); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=6; break; }
        if (showbits(bb, 11) ==  6) /*      00000000110 */ { flushbits(bb, 11); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=6; break; }
        if (showbits(bb, 10) ==  5) /*       0000000101 */ { flushbits(bb, 10); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=6; break; }
        if (showbits(bb,  8) ==  4) /*         00000100 */ { flushbits(bb,  8); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=6; break; }
        if (showbits(bb, 13) == 11) /*    0000000001011 */ { flushbits(bb, 13); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=7; break; }
        if (showbits(bb, 13) == 14) /*    0000000001110 */ { flushbits(bb, 13); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=7; break; }
        if (showbits(bb, 11) ==  5) /*      00000000101 */ { flushbits(bb, 11); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=7; break; }
        if (showbits(bb,  9) ==  4) /*        000000100 */ { flushbits(bb,  9); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=7; break; }
        if (showbits(bb, 13) ==  8) /*    0000000001000 */ { flushbits(bb, 13); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=8; break; }
        if (showbits(bb, 13) == 10) /*    0000000001010 */ { flushbits(bb, 13); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=8; break; }
        if (showbits(bb, 13) == 13) /*    0000000001101 */ { flushbits(bb, 13); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=8; break; }
        if (showbits(bb, 10) ==  4) /*       0000000100 */ { flushbits(bb, 10); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=8; break; }
        if (showbits(bb, 14) == 15) /*   00000000001111 */ { flushbits(bb, 14); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=9; break; }
        if (showbits(bb, 14) == 14) /*   00000000001110 */ { flushbits(bb, 14); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=9; break; }
        if (showbits(bb, 13) ==  9) /*    0000000001001 */ { flushbits(bb, 13); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=9; break; }
        if (showbits(bb, 11) ==  4) /*      00000000100 */ { flushbits(bb, 11); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=9; break; }
        if (showbits(bb, 14) == 11) /*   00000000001011 */ { flushbits(bb, 14); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=10; break; }
        if (showbits(bb, 14) == 10) /*   00000000001010 */ { flushbits(bb, 14); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=10; break; }
        if (showbits(bb, 14) == 13) /*   00000000001101 */ { flushbits(bb, 14); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=10; break; }
        if (showbits(bb, 13) == 12) /*    0000000001100 */ { flushbits(bb, 13); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=10; break; }
        if (showbits(bb, 15) == 15) /*  000000000001111 */ { flushbits(bb, 15); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=11; break; }
        if (showbits(bb, 15) == 14) /*  000000000001110 */ { flushbits(bb, 15); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=11; break; }
        if (showbits(bb, 14) ==  9) /*   00000000001001 */ { flushbits(bb, 14); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=11; break; }
        if (showbits(bb, 14) == 12) /*   00000000001100 */ { flushbits(bb, 14); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=11; break; }
        if (showbits(bb, 15) == 11) /*  000000000001011 */ { flushbits(bb, 15); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=12; break; }
        if (showbits(bb, 15) == 10) /*  000000000001010 */ { flushbits(bb, 15); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=12; break; }
        if (showbits(bb, 15) == 13) /*  000000000001101 */ { flushbits(bb, 15); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=12; break; }
        if (showbits(bb, 14) ==  8) /*   00000000001000 */ { flushbits(bb, 14); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=12; break; }
        if (showbits16(bb)   == 15) /* 0000000000001111 */ { flushbits(bb, 16); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=13; break; }
        if (showbits(bb, 15) ==  1) /*  000000000000001 */ { flushbits(bb, 15); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=13; break; }
        if (showbits(bb, 15) ==  9) /*  000000000001001 */ { flushbits(bb, 15); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=13; break; }
        if (showbits(bb, 15) == 12) /*  000000000001100 */ { flushbits(bb, 15); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=13; break; }
        if (showbits16(bb)   == 11) /* 0000000000001011 */ { flushbits(bb, 16); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=14; break; }
        if (showbits16(bb)   == 14) /* 0000000000001110 */ { flushbits(bb, 16); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=14; break; }
        if (showbits16(bb)   == 13) /* 0000000000001101 */ { flushbits(bb, 16); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=14; break; }
        if (showbits(bb, 15) ==  8) /*  000000000001000 */ { flushbits(bb, 15); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=14; break; }
        if (showbits16(bb)   ==  7) /* 0000000000000111 */ { flushbits(bb, 16); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=15; break; }
        if (showbits16(bb)   == 10) /* 0000000000001010 */ { flushbits(bb, 16); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=15; break; }
        if (showbits16(bb)   ==  9) /* 0000000000001001 */ { flushbits(bb, 16); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=15; break; }
        if (showbits16(bb)   == 12) /* 0000000000001100 */ { flushbits(bb, 16); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=15; break; }
        if (showbits16(bb)   ==  4) /* 0000000000000100 */ { flushbits(bb, 16); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=16; break; }
        if (showbits16(bb)   ==  6) /* 0000000000000110 */ { flushbits(bb, 16); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=16; break; }
        if (showbits16(bb)   ==  5) /* 0000000000000101 */ { flushbits(bb, 16); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=16; break; }
        if (showbits16(bb)   ==  8) /* 0000000000001000 */ { flushbits(bb, 16); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=16; break; }
        break;
        case 2:
        case 3:
        if (showbits(bb,  2) ==  3) /*               11 */ { flushbits(bb,  2); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=0; break; }
        if (showbits(bb,  6) == 11) /*           001011 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=1; break; }
        if (showbits(bb,  2) ==  2) /*               10 */ { flushbits(bb,  2); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=1; break; }
        if (showbits(bb,  6) ==  7) /*           000111 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=2; break; }
        if (showbits(bb,  5) ==  7) /*            00111 */ { flushbits(bb,  5); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=2; break; }
        if (showbits(bb,  3) ==  3) /*              011 */ { flushbits(bb,  3); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=2; break; }
        if (showbits(bb,  7) ==  7) /*          0000111 */ { flushbits(bb,  7); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  6) == 10) /*           001010 */ { flushbits(bb,  6); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  6) ==  9) /*           001001 */ { flushbits(bb,  6); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  4) ==  5) /*             0101 */ { flushbits(bb,  4); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  8) ==  7) /*         00000111 */ { flushbits(bb,  8); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb,  6) ==  6) /*           000110 */ { flushbits(bb,  6); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb,  6) ==  5) /*           000101 */ { flushbits(bb,  6); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb,  4) ==  4) /*             0100 */ { flushbits(bb,  4); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb,  8) ==  4) /*         00000100 */ { flushbits(bb,  8); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=5; break; }
        if (showbits(bb,  7) ==  6) /*          0000110 */ { flushbits(bb,  7); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=5; break; }
        if (showbits(bb,  7) ==  5) /*          0000101 */ { flushbits(bb,  7); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=5; break; }
        if (showbits(bb,  5) ==  6) /*            00110 */ { flushbits(bb,  5); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=5; break; }
        if (showbits(bb,  9) ==  7) /*        000000111 */ { flushbits(bb,  9); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=6; break; }
        if (showbits(bb,  8) ==  6) /*         00000110 */ { flushbits(bb,  8); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=6; break; }
        if (showbits(bb,  8) ==  5) /*         00000101 */ { flushbits(bb,  8); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=6; break; }
        if (showbits(bb,  6) ==  8) /*           001000 */ { flushbits(bb,  6); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=6; break; }
        if (showbits(bb, 11) == 15) /*      00000001111 */ { flushbits(bb, 11); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=7; break; }
        if (showbits(bb,  9) ==  6) /*        000000110 */ { flushbits(bb,  9); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=7; break; }
        if (showbits(bb,  9) ==  5) /*        000000101 */ { flushbits(bb,  9); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=7; break; }
        if (showbits(bb,  6) ==  4) /*           000100 */ { flushbits(bb,  6); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=7; break; }
        if (showbits(bb, 11) == 11) /*      00000001011 */ { flushbits(bb, 11); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=8; break; }
        if (showbits(bb, 11) == 14) /*      00000001110 */ { flushbits(bb, 11); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=8; break; }
        if (showbits(bb, 11) == 13) /*      00000001101 */ { flushbits(bb, 11); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=8; break; }
        if (showbits(bb,  7) ==  4) /*          0000100 */ { flushbits(bb,  7); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=8; break; }
        if (showbits(bb, 12) == 15) /*     000000001111 */ { flushbits(bb, 12); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=9; break; }
        if (showbits(bb, 11) == 10) /*      00000001010 */ { flushbits(bb, 11); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=9; break; }
        if (showbits(bb, 11) ==  9) /*      00000001001 */ { flushbits(bb, 11); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=9; break; }
        if (showbits(bb,  9) ==  4) /*        000000100 */ { flushbits(bb,  9); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=9; break; }
        if (showbits(bb, 12) == 11) /*     000000001011 */ { flushbits(bb, 12); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=10; break; }
        if (showbits(bb, 12) == 14) /*     000000001110 */ { flushbits(bb, 12); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=10; break; }
        if (showbits(bb, 12) == 13) /*     000000001101 */ { flushbits(bb, 12); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=10; break; }
        if (showbits(bb, 11) == 12) /*      00000001100 */ { flushbits(bb, 11); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=10; break; }
        if (showbits(bb, 12) ==  8) /*     000000001000 */ { flushbits(bb, 12); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=11; break; }
        if (showbits(bb, 12) == 10) /*     000000001010 */ { flushbits(bb, 12); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=11; break; }
        if (showbits(bb, 12) ==  9) /*     000000001001 */ { flushbits(bb, 12); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=11; break; }
        if (showbits(bb, 11) ==  8) /*      00000001000 */ { flushbits(bb, 11); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=11; break; }
        if (showbits(bb, 13) == 15) /*    0000000001111 */ { flushbits(bb, 13); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=12; break; }
        if (showbits(bb, 13) == 14) /*    0000000001110 */ { flushbits(bb, 13); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=12; break; }
        if (showbits(bb, 13) == 13) /*    0000000001101 */ { flushbits(bb, 13); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=12; break; }
        if (showbits(bb, 12) == 12) /*     000000001100 */ { flushbits(bb, 12); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=12; break; }
        if (showbits(bb, 13) == 11) /*    0000000001011 */ { flushbits(bb, 13); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=13; break; }
        if (showbits(bb, 13) == 10) /*    0000000001010 */ { flushbits(bb, 13); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=13; break; }
        if (showbits(bb, 13) ==  9) /*    0000000001001 */ { flushbits(bb, 13); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=13; break; }
        if (showbits(bb, 13) == 12) /*    0000000001100 */ { flushbits(bb, 13); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=13; break; }
        if (showbits(bb, 13) ==  7) /*    0000000000111 */ { flushbits(bb, 13); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=14; break; }
        if (showbits(bb, 14) == 11) /*   00000000001011 */ { flushbits(bb, 14); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=14; break; }
        if (showbits(bb, 13) ==  6) /*    0000000000110 */ { flushbits(bb, 13); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=14; break; }
        if (showbits(bb, 13) ==  8) /*    0000000001000 */ { flushbits(bb, 13); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=14; break; }
        if (showbits(bb, 14) ==  9) /*   00000000001001 */ { flushbits(bb, 14); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=15; break; }
        if (showbits(bb, 14) ==  8) /*   00000000001000 */ { flushbits(bb, 14); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=15; break; }
        if (showbits(bb, 14) == 10) /*   00000000001010 */ { flushbits(bb, 14); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=15; break; }
        if (showbits(bb, 13) ==  1) /*    0000000000001 */ { flushbits(bb, 13); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=15; break; }
        if (showbits(bb, 14) ==  7) /*   00000000000111 */ { flushbits(bb, 14); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=16; break; }
        if (showbits(bb, 14) ==  6) /*   00000000000110 */ { flushbits(bb, 14); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=16; break; }
        if (showbits(bb, 14) ==  5) /*   00000000000101 */ { flushbits(bb, 14); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=16; break; }
        if (showbits(bb, 14) ==  4) /*   00000000000100 */ { flushbits(bb, 14); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=16; break; }
        break;
        case 4:
        case 5:
        case 6:
        case 7:
        if (showbits(bb,  4) == 15) /*             1111 */ { flushbits(bb,  4); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=0; break; }
        if (showbits(bb,  6) == 15) /*           001111 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=1; break; }
        if (showbits(bb,  4) == 14) /*             1110 */ { flushbits(bb,  4); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=1; break; }
        if (showbits(bb,  6) == 11) /*           001011 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=2; break; }
        if (showbits(bb,  5) == 15) /*            01111 */ { flushbits(bb,  5); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=2; break; }
        if (showbits(bb,  4) == 13) /*             1101 */ { flushbits(bb,  4); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=2; break; }
        if (showbits(bb,  6) ==  8) /*           001000 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  5) == 12) /*            01100 */ { flushbits(bb,  5); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  5) == 14) /*            01110 */ { flushbits(bb,  5); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  4) == 12) /*             1100 */ { flushbits(bb,  4); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  7) == 15) /*          0001111 */ { flushbits(bb,  7); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb,  5) == 10) /*            01010 */ { flushbits(bb,  5); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb,  5) == 11) /*            01011 */ { flushbits(bb,  5); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb,  4) == 11) /*             1011 */ { flushbits(bb,  4); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb,  7) == 11) /*          0001011 */ { flushbits(bb,  7); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=5; break; }
        if (showbits(bb,  5) ==  8) /*            01000 */ { flushbits(bb,  5); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=5; break; }
        if (showbits(bb,  5) ==  9) /*            01001 */ { flushbits(bb,  5); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=5; break; }
        if (showbits(bb,  4) == 10) /*             1010 */ { flushbits(bb,  4); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=5; break; }
        if (showbits(bb,  7) ==  9) /*          0001001 */ { flushbits(bb,  7); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=6; break; }
        if (showbits(bb,  6) == 14) /*           001110 */ { flushbits(bb,  6); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=6; break; }
        if (showbits(bb,  6) == 13) /*           001101 */ { flushbits(bb,  6); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=6; break; }
        if (showbits(bb,  4) ==  9) /*             1001 */ { flushbits(bb,  4); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=6; break; }
        if (showbits(bb,  7) ==  8) /*          0001000 */ { flushbits(bb,  7); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=7; break; }
        if (showbits(bb,  6) == 10) /*           001010 */ { flushbits(bb,  6); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=7; break; }
        if (showbits(bb,  6) ==  9) /*           001001 */ { flushbits(bb,  6); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=7; break; }
        if (showbits(bb,  4) ==  8) /*             1000 */ { flushbits(bb,  4); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=7; break; }
        if (showbits(bb,  8) == 15) /*         00001111 */ { flushbits(bb,  8); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=8; break; }
        if (showbits(bb,  7) == 14) /*          0001110 */ { flushbits(bb,  7); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=8; break; }
        if (showbits(bb,  7) == 13) /*          0001101 */ { flushbits(bb,  7); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=8; break; }
        if (showbits(bb,  5) == 13) /*            01101 */ { flushbits(bb,  5); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=8; break; }
        if (showbits(bb,  8) == 11) /*         00001011 */ { flushbits(bb,  8); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=9; break; }
        if (showbits(bb,  8) == 14) /*         00001110 */ { flushbits(bb,  8); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=9; break; }
        if (showbits(bb,  7) == 10) /*          0001010 */ { flushbits(bb,  7); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=9; break; }
        if (showbits(bb,  6) == 12) /*           001100 */ { flushbits(bb,  6); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=9; break; }
        if (showbits(bb,  9) == 15) /*        000001111 */ { flushbits(bb,  9); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=10; break; }
        if (showbits(bb,  8) == 10) /*         00001010 */ { flushbits(bb,  8); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=10; break; }
        if (showbits(bb,  8) == 13) /*         00001101 */ { flushbits(bb,  8); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=10; break; }
        if (showbits(bb,  7) == 12) /*          0001100 */ { flushbits(bb,  7); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=10; break; }
        if (showbits(bb,  9) == 11) /*        000001011 */ { flushbits(bb,  9); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=11; break; }
        if (showbits(bb,  9) == 14) /*        000001110 */ { flushbits(bb,  9); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=11; break; }
        if (showbits(bb,  8) ==  9) /*         00001001 */ { flushbits(bb,  8); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=11; break; }
        if (showbits(bb,  8) == 12) /*         00001100 */ { flushbits(bb,  8); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=11; break; }
        if (showbits(bb,  9) ==  8) /*        000001000 */ { flushbits(bb,  9); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=12; break; }
        if (showbits(bb,  9) == 10) /*        000001010 */ { flushbits(bb,  9); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=12; break; }
        if (showbits(bb,  9) == 13) /*        000001101 */ { flushbits(bb,  9); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=12; break; }
        if (showbits(bb,  8) ==  8) /*         00001000 */ { flushbits(bb,  8); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=12; break; }
        if (showbits(bb, 10) == 13) /*       0000001101 */ { flushbits(bb, 10); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=13; break; }
        if (showbits(bb,  9) ==  7) /*        000000111 */ { flushbits(bb,  9); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=13; break; }
        if (showbits(bb,  9) ==  9) /*        000001001 */ { flushbits(bb,  9); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=13; break; }
        if (showbits(bb,  9) == 12) /*        000001100 */ { flushbits(bb,  9); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=13; break; }
        if (showbits(bb, 10) ==  9) /*       0000001001 */ { flushbits(bb, 10); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=14; break; }
        if (showbits(bb, 10) == 12) /*       0000001100 */ { flushbits(bb, 10); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=14; break; }
        if (showbits(bb, 10) == 11) /*       0000001011 */ { flushbits(bb, 10); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=14; break; }
        if (showbits(bb, 10) == 10) /*       0000001010 */ { flushbits(bb, 10); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=14; break; }
        if (showbits(bb, 10) ==  5) /*       0000000101 */ { flushbits(bb, 10); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=15; break; }
        if (showbits(bb, 10) ==  8) /*       0000001000 */ { flushbits(bb, 10); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=15; break; }
        if (showbits(bb, 10) ==  7) /*       0000000111 */ { flushbits(bb, 10); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=15; break; }
        if (showbits(bb, 10) ==  6) /*       0000000110 */ { flushbits(bb, 10); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=15; break; }
        if (showbits(bb, 10) ==  1) /*       0000000001 */ { flushbits(bb, 10); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=16; break; }
        if (showbits(bb, 10) ==  4) /*       0000000100 */ { flushbits(bb, 10); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=16; break; }
        if (showbits(bb, 10) ==  3) /*       0000000011 */ { flushbits(bb, 10); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=16; break; }
        if (showbits(bb, 10) ==  2) /*       0000000010 */ { flushbits(bb, 10); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=16; break; }
        break;
        default:
        if (showbits(bb,  6) ==  3) /*           000011 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=0; break; }
        if (showbits(bb,  6) ==  0) /*           000000 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=1; break; }
        if (showbits(bb,  6) ==  1) /*           000001 */ { flushbits(bb,  6); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=1; break; }
        if (showbits(bb,  6) ==  4) /*           000100 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=2; break; }
        if (showbits(bb,  6) ==  5) /*           000101 */ { flushbits(bb,  6); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=2; break; }
        if (showbits(bb,  6) ==  6) /*           000110 */ { flushbits(bb,  6); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=2; break; }
        if (showbits(bb,  6) ==  8) /*           001000 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  6) ==  9) /*           001001 */ { flushbits(bb,  6); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  6) == 10) /*           001010 */ { flushbits(bb,  6); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  6) == 11) /*           001011 */ { flushbits(bb,  6); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=3; break; }
        if (showbits(bb,  6) == 12) /*           001100 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb,  6) == 13) /*           001101 */ { flushbits(bb,  6); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb,  6) == 14) /*           001110 */ { flushbits(bb,  6); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb,  6) == 15) /*           001111 */ { flushbits(bb,  6); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=4; break; }
        if (showbits(bb,  6) == 16) /*           010000 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=5; break; }
        if (showbits(bb,  6) == 17) /*           010001 */ { flushbits(bb,  6); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=5; break; }
        if (showbits(bb,  6) == 18) /*           010010 */ { flushbits(bb,  6); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=5; break; }
        if (showbits(bb,  6) == 19) /*           010011 */ { flushbits(bb,  6); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=5; break; }
        if (showbits(bb,  6) == 20) /*           010100 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=6; break; }
        if (showbits(bb,  6) == 21) /*           010101 */ { flushbits(bb,  6); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=6; break; }
        if (showbits(bb,  6) == 22) /*           010110 */ { flushbits(bb,  6); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=6; break; }
        if (showbits(bb,  6) == 23) /*           010111 */ { flushbits(bb,  6); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=6; break; }
        if (showbits(bb,  6) == 24) /*           011000 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=7; break; }
        if (showbits(bb,  6) == 25) /*           011001 */ { flushbits(bb,  6); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=7; break; }
        if (showbits(bb,  6) == 26) /*           011010 */ { flushbits(bb,  6); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=7; break; }
        if (showbits(bb,  6) == 27) /*           011011 */ { flushbits(bb,  6); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=7; break; }
        if (showbits(bb,  6) == 28) /*           011100 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=8; break; }
        if (showbits(bb,  6) == 29) /*           011101 */ { flushbits(bb,  6); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=8; break; }
        if (showbits(bb,  6) == 30) /*           011110 */ { flushbits(bb,  6); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=8; break; }
        if (showbits(bb,  6) == 31) /*           011111 */ { flushbits(bb,  6); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=8; break; }
        if (showbits(bb,  6) == 32) /*           100000 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=9; break; }
        if (showbits(bb,  6) == 33) /*           100001 */ { flushbits(bb,  6); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=9; break; }
        if (showbits(bb,  6) == 34) /*           100010 */ { flushbits(bb,  6); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=9; break; }
        if (showbits(bb,  6) == 35) /*           100011 */ { flushbits(bb,  6); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=9; break; }
        if (showbits(bb,  6) == 36) /*           100100 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=10; break; }
        if (showbits(bb,  6) == 37) /*           100101 */ { flushbits(bb,  6); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=10; break; }
        if (showbits(bb,  6) == 38) /*           100110 */ { flushbits(bb,  6); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=10; break; }
        if (showbits(bb,  6) == 39) /*           100111 */ { flushbits(bb,  6); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=10; break; }
        if (showbits(bb,  6) == 40) /*           101000 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=11; break; }
        if (showbits(bb,  6) == 41) /*           101001 */ { flushbits(bb,  6); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=11; break; }
        if (showbits(bb,  6) == 42) /*           101010 */ { flushbits(bb,  6); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=11; break; }
        if (showbits(bb,  6) == 43) /*           101011 */ { flushbits(bb,  6); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=11; break; }
        if (showbits(bb,  6) == 44) /*           101100 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=12; break; }
        if (showbits(bb,  6) == 45) /*           101101 */ { flushbits(bb,  6); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=12; break; }
        if (showbits(bb,  6) == 46) /*           101110 */ { flushbits(bb,  6); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=12; break; }
        if (showbits(bb,  6) == 47) /*           101111 */ { flushbits(bb,  6); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=12; break; }
        if (showbits(bb,  6) == 48) /*           110000 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=13; break; }
        if (showbits(bb,  6) == 49) /*           110001 */ { flushbits(bb,  6); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=13; break; }
        if (showbits(bb,  6) == 50) /*           110010 */ { flushbits(bb,  6); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=13; break; }
        if (showbits(bb,  6) == 51) /*           110011 */ { flushbits(bb,  6); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=13; break; }
        if (showbits(bb,  6) == 52) /*           110100 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=14; break; }
        if (showbits(bb,  6) == 53) /*           110101 */ { flushbits(bb,  6); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=14; break; }
        if (showbits(bb,  6) == 54) /*           110110 */ { flushbits(bb,  6); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=14; break; }
        if (showbits(bb,  6) == 55) /*           110111 */ { flushbits(bb,  6); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=14; break; }
        if (showbits(bb,  6) == 56) /*           111000 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=15; break; }
        if (showbits(bb,  6) == 57) /*           111001 */ { flushbits(bb,  6); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=15; break; }
        if (showbits(bb,  6) == 58) /*           111010 */ { flushbits(bb,  6); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=15; break; }
        if (showbits(bb,  6) == 59) /*           111011 */ { flushbits(bb,  6); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=15; break; }
        if (showbits(bb,  6) == 60) /*           111100 */ { flushbits(bb,  6); coeff_token.TrailingOnes=0; coeff_token.TotalCoeff=16; break; }
        if (showbits(bb,  6) == 61) /*           111101 */ { flushbits(bb,  6); coeff_token.TrailingOnes=1; coeff_token.TotalCoeff=16; break; }
        if (showbits(bb,  6) == 62) /*           111110 */ { flushbits(bb,  6); coeff_token.TrailingOnes=2; coeff_token.TotalCoeff=16; break; }
        if (showbits(bb,  6) == 63) /*           111111 */ { flushbits(bb,  6); coeff_token.TrailingOnes=3; coeff_token.TotalCoeff=16; break; }
        break;
    }
    return coeff_token;
}

int parse_level_prefix(struct bitbuf *bb)
{
    int b, leadingZeroBits = -1;
    for (b = 0; !b; leadingZeroBits++)
        b = getbit(bb);
    return leadingZeroBits;
}

int parse_total_zeros(struct bitbuf *bb, int maxNumCoeff, int TotalCoeff)
{
    if (maxNumCoeff==4) {
        switch (TotalCoeff) {
            case 1:
                if (showbits(bb, 1)==1) { flushbits(bb, 1); return 0; }
                if (showbits(bb, 2)==1) { flushbits(bb, 2); return 1; }
                if (showbits(bb, 3)==1) { flushbits(bb, 3); return 2; }
                if (showbits(bb, 3)==0) { flushbits(bb, 3); return 3; }
                break;
            case 2:
                if (showbits(bb, 1)==1) { flushbits(bb, 1); return 0; }
                if (showbits(bb, 2)==1) { flushbits(bb, 2); return 1; }
                if (showbits(bb, 2)==0) { flushbits(bb, 2); return 2; }
                break;
            case 3:
                if (showbits(bb, 1)==1) { flushbits(bb, 1); return 0; }
                if (showbits(bb, 1)==0) { flushbits(bb, 1); return 1; }
                break;
        }
    } else if (maxNumCoeff==8) {
        switch (TotalCoeff) {
            case 1:
                if (showbits(bb, 1)==1) { flushbits(bb, 1); return 0; }
                if (showbits(bb, 3)==2) { flushbits(bb, 3); return 1; }
                if (showbits(bb, 3)==3) { flushbits(bb, 3); return 2; }
                if (showbits(bb, 4)==2) { flushbits(bb, 4); return 3; }
                if (showbits(bb, 4)==3) { flushbits(bb, 4); return 4; }
                if (showbits(bb, 4)==1) { flushbits(bb, 4); return 5; }
                if (showbits(bb, 5)==1) { flushbits(bb, 5); return 6; }
                if (showbits(bb, 5)==0) { flushbits(bb, 5); return 7; }
                break;
            case 2:
                if (showbits(bb, 3)==0) { flushbits(bb, 3); return 0; }
                if (showbits(bb, 2)==1) { flushbits(bb, 2); return 1; }
                if (showbits(bb, 3)==1) { flushbits(bb, 3); return 2; }
                if (showbits(bb, 3)==4) { flushbits(bb, 3); return 3; }
                if (showbits(bb, 3)==5) { flushbits(bb, 3); return 4; }
                if (showbits(bb, 3)==6) { flushbits(bb, 3); return 5; }
                if (showbits(bb, 3)==7) { flushbits(bb, 3); return 6; }
                break;
            case 3:
                if (showbits(bb, 3)==0) { flushbits(bb, 3); return 0; }
                if (showbits(bb, 3)==1) { flushbits(bb, 3); return 1; }
                if (showbits(bb, 2)==1) { flushbits(bb, 2); return 2; }
                if (showbits(bb, 2)==2) { flushbits(bb, 2); return 3; }
                if (showbits(bb, 3)==6) { flushbits(bb, 3); return 4; }
                if (showbits(bb, 3)==7) { flushbits(bb, 3); return 5; }
                break;
            case 4:
                if (showbits(bb, 3)==6) { flushbits(bb, 3); return 0; }
                if (showbits(bb, 2)==0) { flushbits(bb, 2); return 1; }
                if (showbits(bb, 2)==1) { flushbits(bb, 2); return 2; }
                if (showbits(bb, 2)==2) { flushbits(bb, 2); return 3; }
                if (showbits(bb, 3)==7) { flushbits(bb, 3); return 4; }
                break;
            case 5:
                if (showbits(bb, 2)==0) { flushbits(bb, 2); return 0; }
                if (showbits(bb, 2)==1) { flushbits(bb, 2); return 1; }
                if (showbits(bb, 2)==2) { flushbits(bb, 2); return 2; }
                if (showbits(bb, 2)==3) { flushbits(bb, 2); return 3; }
                break;
            case 6:
                if (showbits(bb, 2)==0) { flushbits(bb, 2); return 0; }
                if (showbits(bb, 2)==1) { flushbits(bb, 2); return 1; }
                if (showbits(bb, 1)==1) { flushbits(bb, 1); return 2; }
                break;
            case 7:
                if (showbits(bb, 1)==0) { flushbits(bb, 1); return 0; }
                if (showbits(bb, 1)==1) { flushbits(bb, 1); return 1; }
                break;
        }
    } else {
        switch (TotalCoeff) {
            case 1:
                if (showbits(bb, 1)==1) { flushbits(bb, 1); return 0; }
                if (showbits(bb, 3)==3) { flushbits(bb, 3); return 1; }
                if (showbits(bb, 3)==2) { flushbits(bb, 3); return 2; }
                if (showbits(bb, 4)==3) { flushbits(bb, 4); return 3; }
                if (showbits(bb, 4)==2) { flushbits(bb, 4); return 4; }
                if (showbits(bb, 5)==3) { flushbits(bb, 5); return 5; }
                if (showbits(bb, 5)==2) { flushbits(bb, 5); return 6; }
                if (showbits(bb, 6)==3) { flushbits(bb, 6); return 7; }
                if (showbits(bb, 6)==2) { flushbits(bb, 6); return 8; }
                if (showbits(bb, 7)==3) { flushbits(bb, 7); return 9; }
                if (showbits(bb, 7)==2) { flushbits(bb, 7); return 10; }
                if (showbits(bb, 8)==3) { flushbits(bb, 8); return 11; }
                if (showbits(bb, 8)==2) { flushbits(bb, 8); return 12; }
                if (showbits(bb, 9)==3) { flushbits(bb, 9); return 13; }
                if (showbits(bb, 9)==2) { flushbits(bb, 9); return 14; }
                if (showbits(bb, 9)==1) { flushbits(bb, 9); return 15; }
                break;
            case 2:
                if (showbits(bb, 3)==7) { flushbits(bb, 3); return 0; }
                if (showbits(bb, 3)==6) { flushbits(bb, 3); return 1; }
                if (showbits(bb, 3)==5) { flushbits(bb, 3); return 2; }
                if (showbits(bb, 3)==4) { flushbits(bb, 3); return 3; }
                if (showbits(bb, 3)==3) { flushbits(bb, 3); return 4; }
                if (showbits(bb, 4)==5) { flushbits(bb, 4); return 5; }
                if (showbits(bb, 4)==4) { flushbits(bb, 4); return 6; }
                if (showbits(bb, 4)==3) { flushbits(bb, 4); return 7; }
                if (showbits(bb, 4)==2) { flushbits(bb, 4); return 8; }
                if (showbits(bb, 5)==3) { flushbits(bb, 5); return 9; }
                if (showbits(bb, 5)==2) { flushbits(bb, 5); return 10; }
                if (showbits(bb, 6)==3) { flushbits(bb, 6); return 11; }
                if (showbits(bb, 6)==2) { flushbits(bb, 6); return 12; }
                if (showbits(bb, 6)==1) { flushbits(bb, 6); return 13; }
                if (showbits(bb, 6)==0) { flushbits(bb, 6); return 14; }
                break;
            case 3:
                if (showbits(bb, 4)==5) { flushbits(bb, 4); return 0; }
                if (showbits(bb, 3)==7) { flushbits(bb, 3); return 1; }
                if (showbits(bb, 3)==6) { flushbits(bb, 3); return 2; }
                if (showbits(bb, 3)==5) { flushbits(bb, 3); return 3; }
                if (showbits(bb, 4)==4) { flushbits(bb, 4); return 4; }
                if (showbits(bb, 4)==3) { flushbits(bb, 4); return 5; }
                if (showbits(bb, 3)==4) { flushbits(bb, 3); return 6; }
                if (showbits(bb, 3)==3) { flushbits(bb, 3); return 7; }
                if (showbits(bb, 4)==2) { flushbits(bb, 4); return 8; }
                if (showbits(bb, 5)==3) { flushbits(bb, 5); return 9; }
                if (showbits(bb, 5)==2) { flushbits(bb, 5); return 10; }
                if (showbits(bb, 6)==1) { flushbits(bb, 6); return 11; }
                if (showbits(bb, 5)==1) { flushbits(bb, 5); return 12; }
                if (showbits(bb, 6)==0) { flushbits(bb, 6); return 13; }
                break;
            case 4:
                if (showbits(bb, 5)==3) { flushbits(bb, 5); return 0; }
                if (showbits(bb, 3)==7) { flushbits(bb, 3); return 1; }
                if (showbits(bb, 4)==5) { flushbits(bb, 4); return 2; }
                if (showbits(bb, 4)==4) { flushbits(bb, 4); return 3; }
                if (showbits(bb, 3)==6) { flushbits(bb, 3); return 4; }
                if (showbits(bb, 3)==5) { flushbits(bb, 3); return 5; }
                if (showbits(bb, 3)==4) { flushbits(bb, 3); return 6; }
                if (showbits(bb, 4)==3) { flushbits(bb, 4); return 7; }
                if (showbits(bb, 3)==3) { flushbits(bb, 3); return 8; }
                if (showbits(bb, 4)==2) { flushbits(bb, 4); return 9; }
                if (showbits(bb, 5)==2) { flushbits(bb, 5); return 10; }
                if (showbits(bb, 5)==1) { flushbits(bb, 5); return 11; }
                if (showbits(bb, 5)==0) { flushbits(bb, 5); return 12; }
                break;
            case 5:
                if (showbits(bb, 4)==5) { flushbits(bb, 4); return 0; }
                if (showbits(bb, 4)==4) { flushbits(bb, 4); return 1; }
                if (showbits(bb, 4)==3) { flushbits(bb, 4); return 2; }
                if (showbits(bb, 3)==7) { flushbits(bb, 3); return 3; }
                if (showbits(bb, 3)==6) { flushbits(bb, 3); return 4; }
                if (showbits(bb, 3)==5) { flushbits(bb, 3); return 5; }
                if (showbits(bb, 3)==4) { flushbits(bb, 3); return 6; }
                if (showbits(bb, 3)==3) { flushbits(bb, 3); return 7; }
                if (showbits(bb, 4)==2) { flushbits(bb, 4); return 8; }
                if (showbits(bb, 5)==1) { flushbits(bb, 5); return 9; }
                if (showbits(bb, 4)==1) { flushbits(bb, 4); return 10; }
                if (showbits(bb, 5)==0) { flushbits(bb, 5); return 11; }
                break;
            case 6:
                if (showbits(bb, 6)==1) { flushbits(bb, 6); return 0; }
                if (showbits(bb, 5)==1) { flushbits(bb, 5); return 1; }
                if (showbits(bb, 3)==7) { flushbits(bb, 3); return 2; }
                if (showbits(bb, 3)==6) { flushbits(bb, 3); return 3; }
                if (showbits(bb, 3)==5) { flushbits(bb, 3); return 4; }
                if (showbits(bb, 3)==4) { flushbits(bb, 3); return 5; }
                if (showbits(bb, 3)==3) { flushbits(bb, 3); return 6; }
                if (showbits(bb, 3)==2) { flushbits(bb, 3); return 7; }
                if (showbits(bb, 4)==1) { flushbits(bb, 4); return 8; }
                if (showbits(bb, 3)==1) { flushbits(bb, 3); return 9; }
                if (showbits(bb, 6)==0) { flushbits(bb, 6); return 10; }
                break;
            case 7:
                if (showbits(bb, 6)==1) { flushbits(bb, 6); return 0; }
                if (showbits(bb, 5)==1) { flushbits(bb, 5); return 1; }
                if (showbits(bb, 3)==5) { flushbits(bb, 3); return 2; }
                if (showbits(bb, 3)==4) { flushbits(bb, 3); return 3; }
                if (showbits(bb, 3)==3) { flushbits(bb, 3); return 4; }
                if (showbits(bb, 2)==3) { flushbits(bb, 2); return 5; }
                if (showbits(bb, 3)==2) { flushbits(bb, 3); return 6; }
                if (showbits(bb, 4)==1) { flushbits(bb, 4); return 7; }
                if (showbits(bb, 3)==1) { flushbits(bb, 3); return 8; }
                if (showbits(bb, 6)==0) { flushbits(bb, 6); return 9; }
                break;
            case 8:
                if (showbits(bb, 6)==1) { flushbits(bb, 6); return 0; }
                if (showbits(bb, 4)==1) { flushbits(bb, 4); return 1; }
                if (showbits(bb, 5)==1) { flushbits(bb, 5); return 2; }
                if (showbits(bb, 3)==3) { flushbits(bb, 3); return 3; }
                if (showbits(bb, 2)==3) { flushbits(bb, 2); return 4; }
                if (showbits(bb, 2)==2) { flushbits(bb, 2); return 5; }
                if (showbits(bb, 3)==2) { flushbits(bb, 3); return 6; }
                if (showbits(bb, 3)==1) { flushbits(bb, 3); return 7; }
                if (showbits(bb, 6)==0) { flushbits(bb, 6); return 8; }
                break;
            case 9:
                if (showbits(bb, 6)==1) { flushbits(bb, 6); return 0; }
                if (showbits(bb, 6)==0) { flushbits(bb, 6); return 1; }
                if (showbits(bb, 4)==1) { flushbits(bb, 4); return 2; }
                if (showbits(bb, 2)==3) { flushbits(bb, 2); return 3; }
                if (showbits(bb, 2)==2) { flushbits(bb, 2); return 4; }
                if (showbits(bb, 3)==1) { flushbits(bb, 3); return 5; }
                if (showbits(bb, 2)==1) { flushbits(bb, 2); return 6; }
                if (showbits(bb, 5)==1) { flushbits(bb, 5); return 7; }
                break;
            case 10:
                if (showbits(bb, 5)==1) { flushbits(bb, 5); return 0; }
                if (showbits(bb, 5)==0) { flushbits(bb, 5); return 1; }
                if (showbits(bb, 3)==1) { flushbits(bb, 3); return 2; }
                if (showbits(bb, 2)==3) { flushbits(bb, 2); return 3; }
                if (showbits(bb, 2)==2) { flushbits(bb, 2); return 4; }
                if (showbits(bb, 2)==1) { flushbits(bb, 2); return 5; }
                if (showbits(bb, 4)==1) { flushbits(bb, 4); return 6; }
                break;
            case 11:
                if (showbits(bb, 4)==0) { flushbits(bb, 4); return 0; }
                if (showbits(bb, 4)==1) { flushbits(bb, 4); return 1; }
                if (showbits(bb, 3)==1) { flushbits(bb, 3); return 2; }
                if (showbits(bb, 3)==2) { flushbits(bb, 3); return 3; }
                if (showbits(bb, 1)==1) { flushbits(bb, 1); return 4; }
                if (showbits(bb, 3)==3) { flushbits(bb, 3); return 5; }
                break;
            case 12:
                if (showbits(bb, 4)==0) { flushbits(bb, 4); return 0; }
                if (showbits(bb, 4)==1) { flushbits(bb, 4); return 1; }
                if (showbits(bb, 2)==1) { flushbits(bb, 2); return 2; }
                if (showbits(bb, 1)==1) { flushbits(bb, 1); return 3; }
                if (showbits(bb, 3)==1) { flushbits(bb, 3); return 4; }
                break;
            case 13:
                if (showbits(bb, 3)==0) { flushbits(bb, 3); return 0; }
                if (showbits(bb, 3)==1) { flushbits(bb, 3); return 1; }
                if (showbits(bb, 1)==1) { flushbits(bb, 1); return 2; }
                if (showbits(bb, 2)==1) { flushbits(bb, 2); return 3; }
                break;
            case 14:
                if (showbits(bb, 2)==0) { flushbits(bb, 2); return 0; }
                if (showbits(bb, 2)==1) { flushbits(bb, 2); return 1; }
                if (showbits(bb, 1)==1) { flushbits(bb, 1); return 2; }
                break;
            case 15:
                if (showbits(bb, 1)==0) { flushbits(bb, 1); return 0; }
                if (showbits(bb, 1)==1) { flushbits(bb, 1); return 1; }
                break;
        }
    }
    rvmessage("parse_total_zeros: failed to lookup");
    return -1;
}

int parse_run_before(struct bitbuf *bb, int zerosLeft)
{
    switch (zerosLeft) {
        case 1:
            if (showbits(bb, 1)==1) { flushbits(bb, 1); return 0; }
            if (showbits(bb, 1)==0) { flushbits(bb, 1); return 1; }
            break;
        case 2:
            if (showbits(bb, 1)==1) { flushbits(bb, 1); return 0; }
            if (showbits(bb, 2)==1) { flushbits(bb, 2); return 1; }
            if (showbits(bb, 2)==0) { flushbits(bb, 2); return 2; }
            break;
        case 3:
            if (showbits(bb, 2)==3) { flushbits(bb, 2); return 0; }
            if (showbits(bb, 2)==2) { flushbits(bb, 2); return 1; }
            if (showbits(bb, 2)==1) { flushbits(bb, 2); return 2; }
            if (showbits(bb, 2)==0) { flushbits(bb, 2); return 3; }
            break;
        case 4:
            if (showbits(bb, 2)==3) { flushbits(bb, 2); return 0; }
            if (showbits(bb, 2)==2) { flushbits(bb, 2); return 1; }
            if (showbits(bb, 2)==1) { flushbits(bb, 2); return 2; }
            if (showbits(bb, 3)==1) { flushbits(bb, 3); return 3; }
            if (showbits(bb, 3)==0) { flushbits(bb, 3); return 4; }
            break;
        case 5:
            if (showbits(bb, 2)==3) { flushbits(bb, 2); return 0; }
            if (showbits(bb, 2)==2) { flushbits(bb, 2); return 1; }
            if (showbits(bb, 3)==3) { flushbits(bb, 3); return 2; }
            if (showbits(bb, 3)==2) { flushbits(bb, 3); return 3; }
            if (showbits(bb, 3)==1) { flushbits(bb, 3); return 4; }
            if (showbits(bb, 3)==0) { flushbits(bb, 3); return 5; }
            break;
        case 6:
            if (showbits(bb, 2)==3) { flushbits(bb, 2); return 0; }
            if (showbits(bb, 3)==0) { flushbits(bb, 3); return 1; }
            if (showbits(bb, 3)==1) { flushbits(bb, 3); return 2; }
            if (showbits(bb, 3)==3) { flushbits(bb, 3); return 3; }
            if (showbits(bb, 3)==2) { flushbits(bb, 3); return 4; }
            if (showbits(bb, 3)==5) { flushbits(bb, 3); return 5; }
            if (showbits(bb, 3)==4) { flushbits(bb, 3); return 6; }
            break;
        default:
            if (showbits(bb, 3)==7) { flushbits(bb, 3); return 0; }
            if (showbits(bb, 3)==6) { flushbits(bb, 3); return 1; }
            if (showbits(bb, 3)==5) { flushbits(bb, 3); return 2; }
            if (showbits(bb, 3)==4) { flushbits(bb, 3); return 3; }
            if (showbits(bb, 3)==3) { flushbits(bb, 3); return 4; }
            if (showbits(bb, 3)==2) { flushbits(bb, 3); return 5; }
            if (showbits(bb, 3)==1) { flushbits(bb, 3); return 6; }
            if (showbits(bb, 4)==1) { flushbits(bb, 4); return 7; }
            if (showbits(bb, 5)==1) { flushbits(bb, 5); return 8; }
            if (showbits(bb, 6)==1) { flushbits(bb, 6); return 9; }
            if (showbits(bb, 7)==1) { flushbits(bb, 7); return 10; }
            if (showbits(bb, 8)==1) { flushbits(bb, 8); return 11; }
            if (showbits(bb, 9)==1) { flushbits(bb, 9); return 12; }
            if (showbits(bb, 10)==1) { flushbits(bb, 10); return 13; }
            if (showbits(bb, 11)==1) { flushbits(bb, 11); return 14; }
            break;
    }
    rvmessage("parse_run_before: failed to lookup");
    return -1;
}

/*
 * h.264 rbsp functions.
 */

int remove_emulation_prevention_three_byte(unsigned char *buf, int bytes)
{
    int i, j=0;
    for (i=0; i<bytes-2; i++) {
        if (buf[i]==0 && buf[i+1]==0 && buf[i+2]==3) {
            buf[i-j] = buf[i];
            buf[i-j+1] = buf[i+1];
            i += 2;
            j++;
        } else if (j)
            buf[i-j] = buf[i];
    }
    if (j)
        rvmessage("note: removed %d emulation_prevention_three_bytes's", j);
    return bytes-j;
}

struct bitbuf *extract_rbsp(struct bitbuf *bb, int verbose)
{
    /* allocate storage for rbsp */
    int rbsp_size = 64*1024;
    unsigned char *rbsp_byte = (unsigned char *)rvalloc(NULL, rbsp_size, 0);

    /* convert nal unit to a rbsp */
    int NumBytesInRBSP = 0;
    int emulation_bytes = 0;
    while (showbits24(bb)!=0x000001 && showbits32(bb)!=0x00000001 && !eofbits(bb)) {
        if (showbits24(bb)==0x000003) {
            rbsp_byte[NumBytesInRBSP++] = getbits8(bb);
            rbsp_byte[NumBytesInRBSP++] = getbits8(bb);
            flushbits(bb, 8);       /* emulation_prevention_three_byte */
            emulation_bytes++;
        } else
            rbsp_byte[NumBytesInRBSP++] = getbits8(bb);

        /* check size of rbsp */
        if (NumBytesInRBSP>=rbsp_size-1) {
            rbsp_size += 8*1024;
            rbsp_byte = (unsigned char *)rvalloc(rbsp_byte, rbsp_size, 0);
        }
    }

    if (verbose>=3 && emulation_bytes)
        rvmessage("removed %d emulation_prevention_three_byte%s from nal unit", emulation_bytes, emulation_bytes>1? "'s" : "");

    /* prepare to parse rbsp */
    struct bitbuf *rbsp = initbits_memory(rbsp_byte, NumBytesInRBSP);
    if (rbsp==NULL) {
        rverror("failed to initialise rbsp");
        rvfree(rbsp_byte);
    }

    return rbsp;
}

void free_rbsp(struct bitbuf *bb)
{
    free(bb->buffer);
    freebits(bb);
}

/*
 * h.264 nal unit types.
 */

const char *nal_unit_type_code(int nal_unit_type)
{
    switch (nal_unit_type) {
        case 1 : return "coded slice of a non-IDR picture"; break;
        case 2 : return "coded slice data partition A"; break;
        case 3 : return "coded slice data partition B"; break;
        case 4 : return "coded slice data partition C"; break;
        case 5 : return "coded slice of an IDR picture"; break;
        case 6 : return "supplemental enhancement information (SEI)"; break;
        case 7 : return "sequence parameter set"; break;
        case 8 : return "picture parameter set"; break;
        case 9 : return "access unit delimiter"; break;
        case 10: return "end of sequence"; break;
        case 11: return "end of stream"; break;
        case 12: return "filler data"; break;
        case 13: return "sequence parameter set extension"; break;
        case 14:
        case 15:
        case 17:
        case 18: return "reserved"; break;
        case 19: return "coded slice of an auxiliary coded picture without partitioning"; break;
        case 20:
        case 21:
        case 22:
        case 23: return "reserved"; break;
        default : return "unspecified";
    }
}

/*
 * h.264 picture order count decoding.
 * this function is taken from jm 10.2.
 */
int decode_poc(const struct rv264seq_parameter_set *sps, int nal_unit_type, int nal_ref_idc, int frame_num, int field_pic_flag, int bottom_field_flag, int pic_order_cnt_lsb,
               int delta_pic_order_cnt_bottom, int delta_pic_order_cnt[2])
{
    int i;
    int PicOrderCnt = 0;

    static int toppoc, bottompoc, framepoc;

    static   signed int PrevPicOrderCntMsb;
    static unsigned int PrevPicOrderCntLsb;
    static   signed int PicOrderCntMsb;

    static unsigned int AbsFrameNum;
    static   signed int ExpectedPicOrderCnt, PicOrderCntCycleCnt, FrameNumInPicOrderCntCycle;
    static unsigned int PreviousFrameNum, FrameNumOffset;
    static          int ExpectedDeltaPerPicOrderCntCycle;
    static          int PreviousFrameNumOffset;


    unsigned int MaxPicOrderCntLsb = 1<<(sps->log2_max_pic_order_cnt_lsb_minus4+4);
    unsigned int MaxFrameNum = 1<<(sps->log2_max_frame_num_minus4+4);

    switch (sps->pic_order_cnt_type)
    {
        case 0: // poc mode 0.
            // 1st
            if (nal_unit_type==5) {
                PrevPicOrderCntMsb = 0;
                PrevPicOrderCntLsb = 0;
            } else {
                /*
                if (img->last_has_mmco_5) {
                    if (img->last_pic_bottom_field) {
                        PrevPicOrderCntMsb = 0;
                        PrevPicOrderCntLsb = 0;
                    } else {
                        PrevPicOrderCntMsb = 0;
                        PrevPicOrderCntLsb = toppoc;
                    }
                }
                */
            }

            // calculate the MSBs of current picture.
            if (pic_order_cnt_lsb < PrevPicOrderCntLsb  && (PrevPicOrderCntLsb - pic_order_cnt_lsb) >= MaxPicOrderCntLsb/2)
                PicOrderCntMsb = PrevPicOrderCntMsb + MaxPicOrderCntLsb;
            else if (pic_order_cnt_lsb > PrevPicOrderCntLsb && (pic_order_cnt_lsb - PrevPicOrderCntLsb) > MaxPicOrderCntLsb/2)
                PicOrderCntMsb = PrevPicOrderCntMsb - MaxPicOrderCntLsb;
            else
                PicOrderCntMsb = PrevPicOrderCntMsb;

            // 2nd

            if (field_pic_flag==0) {
                toppoc = PicOrderCntMsb + pic_order_cnt_lsb;
                bottompoc = toppoc + delta_pic_order_cnt_bottom;
                PicOrderCnt = framepoc = (toppoc < bottompoc)? toppoc : bottompoc; // POC200301
            } else if (bottom_field_flag==0) {  //top field
                PicOrderCnt = toppoc = PicOrderCntMsb + pic_order_cnt_lsb;
            } else {  //bottom field
                PicOrderCnt = bottompoc = PicOrderCntMsb + pic_order_cnt_lsb;
            }
            framepoc = PicOrderCnt;

            if (frame_num != PreviousFrameNum)
                PreviousFrameNum = frame_num;

            if (nal_ref_idc!=0) {
                PrevPicOrderCntLsb = pic_order_cnt_lsb;
                PrevPicOrderCntMsb = PicOrderCntMsb;
            }
            break;

        case 1: // POC MODE 1
            // 1st
            if (nal_unit_type==5) {
                FrameNumOffset = 0;     //  first picture of IDRGOP,
                delta_pic_order_cnt[0] = 0;                        //ignore first delta
                if (frame_num) rverror("frame_num != 0 in idr picture");
            } else {
                /*if (last_has_mmco_5) {
                    PreviousFrameNumOffset = 0;
                    PreviousFrameNum = 0;
                }*/

                if (frame_num < PreviousFrameNum) {             //not first picture of IDRGOP
                    FrameNumOffset = PreviousFrameNumOffset + MaxFrameNum;
                } else {
                    FrameNumOffset = PreviousFrameNumOffset;
                }
            }

            // 2nd
            if (sps->num_ref_frames_in_pic_order_cnt_cycle)
                AbsFrameNum = FrameNumOffset + frame_num;
            else
                AbsFrameNum = 0;
            if(nal_ref_idc==0 && AbsFrameNum>0)
                AbsFrameNum--;

            // 3rd
            ExpectedDeltaPerPicOrderCntCycle = 0;

            if (sps->num_ref_frames_in_pic_order_cnt_cycle)
                for(i=0; i<(int)sps->num_ref_frames_in_pic_order_cnt_cycle; i++)
                    ExpectedDeltaPerPicOrderCntCycle += sps->offset_for_ref_frame[i];

            if (AbsFrameNum) {
                PicOrderCntCycleCnt = (AbsFrameNum-1)/sps->num_ref_frames_in_pic_order_cnt_cycle;
                FrameNumInPicOrderCntCycle = (AbsFrameNum-1)%sps->num_ref_frames_in_pic_order_cnt_cycle;
                ExpectedPicOrderCnt = PicOrderCntCycleCnt*ExpectedDeltaPerPicOrderCntCycle;
                for(i=0;i<=(int)FrameNumInPicOrderCntCycle;i++)
                    ExpectedPicOrderCnt += sps->offset_for_ref_frame[i];
            } else
                ExpectedPicOrderCnt=0;

            if (nal_ref_idc==0)
                ExpectedPicOrderCnt += sps->offset_for_non_ref_pic;

            if (field_pic_flag==0) {           //frame picture
                toppoc = ExpectedPicOrderCnt + delta_pic_order_cnt[0];
                bottompoc = toppoc + sps->offset_for_top_to_bottom_field + delta_pic_order_cnt[1];
                PicOrderCnt = framepoc = (toppoc < bottompoc)? toppoc : bottompoc; // POC200301
            } else if (bottom_field_flag==0) {  //top field
                PicOrderCnt = toppoc = ExpectedPicOrderCnt + delta_pic_order_cnt[0];
            } else {  //bottom field
                PicOrderCnt = bottompoc = ExpectedPicOrderCnt + sps->offset_for_top_to_bottom_field + delta_pic_order_cnt[0];
            }
            framepoc = PicOrderCnt;

            PreviousFrameNum = frame_num;
            PreviousFrameNumOffset = FrameNumOffset;
            break;

            case 2: // POC MODE 2
                if (nal_unit_type==5) { // IDR picture
                    FrameNumOffset = 0;
                    PicOrderCnt = framepoc = toppoc = bottompoc = 0;
                    if (frame_num) rverror("frame_num != 0 in idr picture");
                } else {
                    /*if (img->last_has_mmco_5) {
                        PreviousFrameNum = 0;
                        PreviousFrameNumOffset = 0;
                    }*/
                    if (frame_num < PreviousFrameNum)
                        FrameNumOffset = PreviousFrameNumOffset + MaxFrameNum;
                    else
                        FrameNumOffset = PreviousFrameNumOffset;

                    AbsFrameNum = FrameNumOffset + frame_num;
                    if (nal_ref_idc==0)
                        PicOrderCnt = (2*AbsFrameNum - 1);
                    else
                        PicOrderCnt = (2*AbsFrameNum);

                    if (field_pic_flag==0)
                        toppoc = bottompoc = framepoc = PicOrderCnt;
                    else if (bottom_field_flag==0)
                        toppoc = framepoc = PicOrderCnt;
                    else
                        bottompoc = framepoc = PicOrderCnt;
                }

                if (nal_ref_idc!=0)
                    PreviousFrameNum = frame_num;
                PreviousFrameNumOffset = FrameNumOffset;
                break;

            default:
                rvexit("unknown pic_order_cnt_type: %d", sps->pic_order_cnt_type);
                break;
    }

    return PicOrderCnt;
}

/*
 * h.264 comparison functions.
 * these are taken from JM 19.0
 */
int sps_is_equal(struct rv264seq_parameter_set *sps1, struct rv264seq_parameter_set *sps2)
{
  unsigned i;
  int equal = 1;

  if ((!sps1->occupied) || (!sps2->occupied))
    return 0;

  equal &= (sps1->profile_idc == sps2->profile_idc);
  equal &= (sps1->constraint_set0_flag == sps2->constraint_set0_flag);
  equal &= (sps1->constraint_set1_flag == sps2->constraint_set1_flag);
  equal &= (sps1->constraint_set2_flag == sps2->constraint_set2_flag);
  equal &= (sps1->level_idc == sps2->level_idc);
  equal &= (sps1->seq_parameter_set_id == sps2->seq_parameter_set_id);
  equal &= (sps1->log2_max_frame_num_minus4 == sps2->log2_max_frame_num_minus4);
  equal &= (sps1->pic_order_cnt_type == sps2->pic_order_cnt_type);

  if (!equal) return equal;

  if( sps1->pic_order_cnt_type == 0 )
  {
    equal &= (sps1->log2_max_pic_order_cnt_lsb_minus4 == sps2->log2_max_pic_order_cnt_lsb_minus4);
  }

  else if( sps1->pic_order_cnt_type == 1 )
  {
    equal &= (sps1->delta_pic_order_always_zero_flag == sps2->delta_pic_order_always_zero_flag);
    equal &= (sps1->offset_for_non_ref_pic == sps2->offset_for_non_ref_pic);
    equal &= (sps1->offset_for_top_to_bottom_field == sps2->offset_for_top_to_bottom_field);
    equal &= (sps1->num_ref_frames_in_pic_order_cnt_cycle == sps2->num_ref_frames_in_pic_order_cnt_cycle);
    if (!equal) return equal;

    for ( i = 0 ; i< sps1->num_ref_frames_in_pic_order_cnt_cycle ;i ++)
      equal &= (sps1->offset_for_ref_frame[i] == sps2->offset_for_ref_frame[i]);
  }

  equal &= (sps1->max_num_ref_frames == sps2->max_num_ref_frames);
  equal &= (sps1->gaps_in_frame_num_value_allowed_flag == sps2->gaps_in_frame_num_value_allowed_flag);
  equal &= (sps1->pic_width_in_mbs_minus1 == sps2->pic_width_in_mbs_minus1);
  equal &= (sps1->pic_height_in_map_units_minus1 == sps2->pic_height_in_map_units_minus1);
  equal &= (sps1->frame_mbs_only_flag == sps2->frame_mbs_only_flag);

  if (!equal) return equal;
  if( !sps1->frame_mbs_only_flag )
    equal &= (sps1->mb_adaptive_frame_field_flag == sps2->mb_adaptive_frame_field_flag);

  equal &= (sps1->direct_8x8_inference_flag == sps2->direct_8x8_inference_flag);
  equal &= (sps1->frame_cropping_flag == sps2->frame_cropping_flag);
  if (!equal) return equal;
  if (sps1->frame_cropping_flag)
  {
    equal &= (sps1->frame_crop_left_offset == sps2->frame_crop_left_offset);
    equal &= (sps1->frame_crop_right_offset == sps2->frame_crop_right_offset);
    equal &= (sps1->frame_crop_top_offset == sps2->frame_crop_top_offset);
    equal &= (sps1->frame_crop_bottom_offset == sps2->frame_crop_bottom_offset);
  }
  equal &= (sps1->vui_parameters_present_flag == sps2->vui_parameters_present_flag);

  return equal;
}

int pps_is_equal(struct rv264pic_parameter_set *pps1, struct rv264pic_parameter_set *pps2)
{
  unsigned i, j;
  int equal = 1;

  if ((!pps1->occupied) || (!pps2->occupied))
    return 0;

  equal &= (pps1->pic_parameter_set_id == pps2->pic_parameter_set_id);
  equal &= (pps1->seq_parameter_set_id == pps2->seq_parameter_set_id);
  equal &= (pps1->entropy_coding_mode_flag == pps2->entropy_coding_mode_flag);
  //equal &= (pps1->bottom_field_pic_order_in_frame_present_flag == pps2->bottom_field_pic_order_in_frame_present_flag);
  equal &= (pps1->num_slice_groups_minus1 == pps2->num_slice_groups_minus1);

  if (!equal) return equal;

  if (pps1->num_slice_groups_minus1>0)
  {
      equal &= (pps1->slice_group_map_type == pps2->slice_group_map_type);
      if (!equal) return equal;
      /* FIXME these are not parsed from the bitstream
      if (pps1->slice_group_map_type == 0)
      {
        for (i=0; i<=pps1->num_slice_groups_minus1; i++)
          equal &= (pps1->run_length_minus1[i] == pps2->run_length_minus1[i]);
      }
      else if( pps1->slice_group_map_type == 2 )
      {
        for (i=0; i<pps1->num_slice_groups_minus1; i++)
        {
          equal &= (pps1->top_left[i] == pps2->top_left[i]);
          equal &= (pps1->bottom_right[i] == pps2->bottom_right[i]);
        }
      }
      else if( pps1->slice_group_map_type == 3 || pps1->slice_group_map_type==4 || pps1->slice_group_map_type==5 )
      {
        equal &= (pps1->slice_group_change_direction_flag == pps2->slice_group_change_direction_flag);
        equal &= (pps1->slice_group_change_rate_minus1 == pps2->slice_group_change_rate_minus1);
      }
      else if( pps1->slice_group_map_type == 6 )
      {
        equal &= (pps1->pic_size_in_map_units_minus1 == pps2->pic_size_in_map_units_minus1);
        if (!equal) return equal;
        for (i=0; i<=pps1->pic_size_in_map_units_minus1; i++)
          equal &= (pps1->slice_group_id[i] == pps2->slice_group_id[i]);
      }*/
  }

  equal &= (pps1->num_ref_idx_l0_active_minus1 == pps2->num_ref_idx_l0_active_minus1);
  equal &= (pps1->num_ref_idx_l1_active_minus1 == pps2->num_ref_idx_l1_active_minus1);
  equal &= (pps1->weighted_pred_flag == pps2->weighted_pred_flag);
  equal &= (pps1->weighted_bipred_idc == pps2->weighted_bipred_idc);
  equal &= (pps1->pic_init_qp_minus26 == pps2->pic_init_qp_minus26);
  equal &= (pps1->pic_init_qs_minus26 == pps2->pic_init_qs_minus26);
  equal &= (pps1->chroma_qp_index_offset == pps2->chroma_qp_index_offset);
  equal &= (pps1->deblocking_filter_control_present_flag == pps2->deblocking_filter_control_present_flag);
  equal &= (pps1->constrained_intra_pred_flag == pps2->constrained_intra_pred_flag);
  equal &= (pps1->redundant_pic_cnt_present_flag == pps2->redundant_pic_cnt_present_flag);

  if (!equal) return equal;

  //Fidelity Range Extensions Stuff
  //It is initialized to zero, so should be ok to check all the time.
  equal &= (pps1->transform_8x8_mode_flag == pps2->transform_8x8_mode_flag);
  equal &= (pps1->pic_scaling_matrix_present_flag == pps2->pic_scaling_matrix_present_flag);
  if(pps1->pic_scaling_matrix_present_flag)
  {
    for(i = 0; i < (6 + ((unsigned)pps1->transform_8x8_mode_flag << 1)); i++)
    {
      equal &= (pps1->pic_scaling_list_present_flag[i] == pps2->pic_scaling_list_present_flag[i]);
      if(pps1->pic_scaling_list_present_flag[i])
      {
        if(i < 6)
        {
          for (j = 0; j < 16; j++)
            equal &= (pps1->ScalingList4x4[i][j] == pps2->ScalingList4x4[i][j]);
        }
        else
        {
          for (j = 0; j < 64; j++)
            equal &= (pps1->ScalingList8x8[i-6][j] == pps2->ScalingList8x8[i-6][j]);
        }
      }
    }
  }
  equal &= (pps1->second_chroma_qp_index_offset == pps2->second_chroma_qp_index_offset);

  return equal;
}

/*
 * h.264 parsing functions.
 */

int find_next_nal_unit(struct bitbuf *bb, int *zerobyte)
{
    alignbits(bb);
    /* look for a start code prefix */
    while (showbits24(bb)!=0x000001 && showbits32(bb)!=0x00000001) {
        /* move along by one byte */
        flushbits(bb, 8);               /* leading_zero_8bits */
        if (eofbits(bb))
            return -1;
    }
    if (showbits24(bb)!=0x000001) {
        if (zerobyte)
            *zerobyte = 1;
        return llshowbits(bb, 40) & 0x1fllu;
    } else {
        if (zerobyte)
            *zerobyte = 0;
        return showbits32(bb) & 0x1f;
    }
}

int parse_nal_unit_header(struct bitbuf *bb, int *nal_unit_type, int *nal_ref_idc)
{
    *nal_ref_idc = 0;   /* avoid compiler warning */
    if (getbit(bb)!=0) {                                                /* forbidden_zero_bit */
        rvmessage("h.264 nal unit: forbidden_zero_bit: 1");
        return -1;
    }
    *nal_ref_idc = getbits(bb, 2);                                      /* nal_ref_idc */
    *nal_unit_type = getbits(bb, 5);                                    /* nal_unit_type */
    return 0;
}

int parse_rbsp_trailing_bits(struct bitbuf *bb, const char *where)
{
    int one = getbit(bb);
    int zero = alignbits(bb);
    if (!one || zero) {
        rvmessage("%s: rbsp_trailing_bits not correct: one=%d zero=%d", where, one, zero);
        return -1;
    }
    return 0;
}

static int check_rbsp_trailing_bits(struct bitbuf *bb)
{
    /* check for the '1' bit of a rbsp_trailing_bits() */
    int one = showbits(bb, 1);
    if (one!=1)
        return -1;

    /* check for the run of '0' bits of a rbsp_trailing_bits() */
    int align = numalignbits(bb);
    if (align==0)
        align = 8;
    long long zero = 0;
    if (align>0)
        zero = llshowbits(bb, align) & ((1ll<<(align-1))-1ll);
    if (zero!=0)
        return -1;

    return align;
}

int more_rbsp_data(struct bitbuf *bb)
{
    if (numbitsleft(bb)<=0)
        return 0;
    else if (numbitsleft(bb)<=56 && check_rbsp_trailing_bits(bb)>=0)
        return 0;
    else
        return 1;
}

static int parse_slice_rbsp_trailing_bits(struct bitbuf *bb, int entropy_coding_mode_flag)
{
    parse_rbsp_trailing_bits(bb, "h.264 slice data");
    if (entropy_coding_mode_flag)
        while (more_rbsp_data(bb))
            if (getbits(bb, 16)!=0)                                     /* cabac_zero_word */
                return -1;
    return 0;
}

static int parse_scaling_list(struct bitbuf *bb, int scaling_list[], int size, int *use_default)
{
    int j;
    int lastScale = 8;
    int nextScale = 8;
    for (j=0; j<size; j++) {
        if (nextScale!=0) {
            int delta_scale = sexpbits(bb);
            nextScale = (lastScale+delta_scale+256) % 256;
            *use_default = (j==0 && nextScale==0);
        }
        scaling_list[j] = (nextScale==0) ? lastScale : nextScale;
        lastScale = scaling_list[j];
    }
    return 0;
}

static int parse_hrd_parameters(struct bitbuf *bb, struct hrd_parameters *hrd)
{
    int SchedSelIdx;
    hrd->cpb_cnt_minus1 = uexpbits(bb);                                 /* cpb_cnt_minus1 */
    hrd->bit_rate_scale = getbits(bb, 4);                               /* bit_rate_scale */
    hrd->cpb_size_scale = getbits(bb, 4);                               /* cpb_size_scale */
    for (SchedSelIdx=0; SchedSelIdx<=hrd->cpb_cnt_minus1; SchedSelIdx++) {
        hrd->bit_rate_value_minus1[SchedSelIdx] = uexpbits(bb);         /* bit_rate_value_minus1[i] */
        hrd->cpb_size_value_minus1[SchedSelIdx] = uexpbits(bb);         /* cpb_size_value_minus1[i] */
        hrd->cbr_flag[SchedSelIdx] = getbit(bb);                        /* cbr_flag[i] */
    }
    hrd->initial_cpb_removal_delay_length_minus1 = getbits(bb, 5);      /* initial_cpb_removal_delay_length_minus1 */
    hrd->cpb_removal_delay_length_minus1 = getbits(bb, 5);              /* cpb_removal_delay_length_minus1 */
    hrd->dpb_output_delay_length_minus1 = getbits(bb, 5);               /* dpb_output_delay_length_minus1 */
    hrd->time_offset_length = getbits(bb, 5);                           /* time_offset_length */
    return 0;
}

static int parse_vui_params(struct bitbuf *bb, struct vui_parameters *vui)
{
    if (getbit(bb)) {                           /* aspect_ratio_info_present_flag */
        int aspect_ratio_idc = getbits8(bb);    /* aspect_ratio_idc */
        if (aspect_ratio_idc==255) {
            flushbits(bb, 16);                  /* sar_width */
            flushbits(bb, 16);                  /* sar_height */
        }
    }
    if (getbit(bb))                             /* overscan_info_present_flag */
        flushbits(bb, 1);                       /* overscan_appropriate_flag */
    if (getbit(bb)) {                           /* video_signal_type_present_flag */
        flushbits(bb, 3);                       /* video_format */
        flushbits(bb, 1);                       /* video_full_range_flag */
        if (getbit(bb)) {                       /* colour_description_present_flag */
            flushbits(bb, 8);                   /* colour_primaries */
            flushbits(bb, 8);                   /* transfer_characteristics */
            flushbits(bb, 8);                   /* matrix_coefficients */
        }
    }
    if (getbit(bb)) {                           /* chroma_loc_info_present_flag */
        uexpbits(bb);                           /* chroma_sample_loc_type_top_field */
        uexpbits(bb);                           /* chroma_sample_loc_type_bottom_field */
    }
    vui->timing_info_present_flag = getbit(bb); /* timing_info_present_flag */
    if (vui->timing_info_present_flag) {
        vui->num_units_in_tick = getbits32(bb); /* num_units_in_tick */
        vui->time_scale = getbits32(bb);        /* time_scale */
        vui->fixed_frame_rate_flag = getbit(bb);/* fixed_frame_rate_flag */
    }
    vui->nal_hrd_parameters_present_flag = getbit(bb);
    if (vui->nal_hrd_parameters_present_flag)        /* nal_hrd_parameters_present_flag */
        parse_hrd_parameters(bb, &vui->nal_hrd_parameters);
    vui->vcl_hrd_parameters_present_flag = getbit(bb);
    if (vui->vcl_hrd_parameters_present_flag)        /* vcl_hrd_parameters_present_flag */
        parse_hrd_parameters(bb, &vui->vcl_hrd_parameters);
    if (vui->nal_hrd_parameters_present_flag || vui->vcl_hrd_parameters_present_flag)
        vui->low_delay_hrd_flag = getbit(bb);   /* low_delay_hrd_flag */
    vui->pic_struct_present_flag = getbit(bb);  /* pic_struct_present_flag */
    if (getbit(bb)) {                           /* bitstream_restriction_flag */
        flushbits(bb, 1);                       /* motion_vectors_over_pic_boundaries_flag */
        uexpbits(bb);                           /* max_bytes_per_pic_denom */
        uexpbits(bb);                           /* max_bits_per_mb_denom */
        uexpbits(bb);                           /* log2_max_mv_length_horizontal */
        uexpbits(bb);                           /* log2_max_mv_length_vertical */
        uexpbits(bb);                           /* num_reorder_frames */
        uexpbits(bb);                           /* max_dec_frame_buffering */
    }
    return 0;
}

struct rv264seq_parameter_set *parse_sequence_param(struct bitbuf *bb)
{
    const char *where = "h.264 sequence param. set";

    /* allocate a sequence parameter set */
    struct rv264seq_parameter_set *sps = (struct rv264seq_parameter_set *)rvalloc(NULL, sizeof(struct rv264seq_parameter_set), 1);
    sps->occupied = 1;

    /* provide some defaults for optional fields */
    sps->vui.nal_hrd_parameters.initial_cpb_removal_delay_length_minus1 = 23;
    sps->vui.nal_hrd_parameters.cpb_removal_delay_length_minus1 = 23;
    sps->vui.nal_hrd_parameters.dpb_output_delay_length_minus1 = 23;
    sps->vui.nal_hrd_parameters.time_offset_length = 24;
    sps->vui.vcl_hrd_parameters.initial_cpb_removal_delay_length_minus1 = 23;
    sps->vui.vcl_hrd_parameters.cpb_removal_delay_length_minus1 = 23;
    sps->vui.vcl_hrd_parameters.dpb_output_delay_length_minus1 = 23;
    sps->vui.vcl_hrd_parameters.time_offset_length = 24;

    /* parse sequence parameter set */
    sps->profile_idc = getbits8(bb);                                    /* profile_idc */
    flushbits(bb, 1);                                                   /* constraint_set0_flag */
    flushbits(bb, 1);                                                   /* constraint_set1_flag */
    flushbits(bb, 1);                                                   /* constraint_set2_flag */
    flushbits(bb, 1);                                                   /* constraint_set3_flag */
    flushbits(bb, 1);                                                   /* constraint_set4_flag */
    flushbits(bb, 1);                                                   /* constraint_set5_flag */
    int zero = getbits(bb, 2);                                          /* reserved_zero_2bits */
    if (zero)
        rvmessage("%s: reserved_zero_2bits: %d", where, zero);
    sps->level_idc = getbits8(bb);                                      /* level_idc */
    sps->seq_parameter_set_id = uexpbits(bb);                           /* seq_parameter_set_id */
    if (sps->seq_parameter_set_id>31) {
        rvmessage("%s: seq_parameter_set_id is greater than 31: %d", where, sps->seq_parameter_set_id);
        rvfree(sps);
        return NULL;
    }
    if (sps->profile_idc==100 || sps->profile_idc==110 || sps->profile_idc==122 || sps->profile_idc==244 ||
        sps->profile_idc==44  || sps->profile_idc==83  || sps->profile_idc==86  || sps->profile_idc==118 || sps->profile_idc==128) {
        sps->chroma_format_idc = uexpbits(bb);                          /* chroma_format_idc */
        if (sps->chroma_format_idc>3) {
            rvmessage("%s: chroma_format_idc is greater than 3: %d", where, sps->chroma_format_idc);
            rvfree(sps);
            return NULL;
        }
        if (sps->chroma_format_idc==3)
            sps->separate_colour_plane_flag = getbit(bb);               /* separate_colour_plane_flag */
        sps->bit_depth_luma_minus8 = uexpbits(bb);                      /* bit_depth_luma_minus8 */
        if (sps->bit_depth_luma_minus8>4) {
            rvmessage("%s: bit_depth_luma_minus8 is greater than 4: %d", where, sps->bit_depth_luma_minus8);
            rvfree(sps);
            return NULL;
        }
        sps->bit_depth_chroma_minus8 = uexpbits(bb);                    /* bit_depth_chroma_minus8 */
        if (sps->bit_depth_chroma_minus8>4) {
            rvmessage("%s: bit_depth_chroma_minus8 is greater than 4: %d", where, sps->bit_depth_chroma_minus8);
            rvfree(sps);
            return NULL;
        }
        sps->qpprime_y_zero_transform_bypass_flag = getbit(bb);         /* qpprime_y_zero_transform_bypass_flag */
        sps->seq_scaling_matrix_present_flag = getbit(bb);              /* seq_scaling_matrix_present_flag */
        if (sps->seq_scaling_matrix_present_flag) {
            int i, use_default;
            int scaling_list4x4[16];
            int scaling_list8x8[64];
            for (i=0; i<8; i++) {
                if (getbit(bb)) {                                       /* seq_scaling_list_present_flag[ i ] */
                    if (i<6)
                        parse_scaling_list(bb, scaling_list4x4, 16, &use_default);
                    else
                        parse_scaling_list(bb, scaling_list8x8, 64, &use_default);
                }
            }
        }
    } else
        sps->chroma_format_idc = 1;
    sps->log2_max_frame_num_minus4 = uexpbits(bb);                      /* log2_max_frame_num_minus4 */
    if (sps->log2_max_frame_num_minus4>12) {
        rvmessage("%s: log2_max_frame_num_minus4 is greater than 12: %d", where, sps->log2_max_frame_num_minus4);
        rvfree(sps);
        return NULL;
    }
    sps->pic_order_cnt_type = uexpbits(bb);                             /* pic_order_cnt_type */
    switch (sps->pic_order_cnt_type) {
        case 0:
            sps->log2_max_pic_order_cnt_lsb_minus4 = uexpbits(bb);      /* log2_max_pic_order_cnt_lsb_minus4 */
            break;

        case 1:
            sps->delta_pic_order_always_zero_flag = getbit(bb);         /* delta_pic_order_always_zero_flag */
            sps->offset_for_non_ref_pic = sexpbits(bb);                 /* offset_for_non_ref_pic */
            sps->offset_for_top_to_bottom_field = sexpbits(bb);         /* offset_for_top_to_bottom_field */
            sps->num_ref_frames_in_pic_order_cnt_cycle = uexpbits(bb);  /* num_ref_frames_in_pic_order_cnt_cycle */
            if (sps->num_ref_frames_in_pic_order_cnt_cycle>255)
                rvmessage("%s: num_ref_frames_in_pic_order_cnt_cycleis greater than 255: %d", where, sps->num_ref_frames_in_pic_order_cnt_cycle);
            int i;
            for (i=0; i<sps->num_ref_frames_in_pic_order_cnt_cycle; i++)
                sps->offset_for_ref_frame[i] = sexpbits(bb);            /* offset_for_ref_frame[i] */
            break;

        case 2:
            break;

        default:
            rvmessage("%s: pic_order_cnt_type is great than 2: %d", where, sps->pic_order_cnt_type);
            rvfree(sps);
            return NULL;
    }
    sps->max_num_ref_frames = uexpbits(bb);                             /* max_num_ref_frames */
    /* int MaxDpbSize = rvmin(1024*MaxDPB/(PicWidthInMbs*FrameHeightInMbs*384), 16); */
    sps->gaps_in_frame_num_value_allowed_flag = getbit(bb);             /* gaps_in_frame_num_value_allowed_flag */
    sps->pic_width_in_mbs_minus1 = uexpbits(bb);                        /* pic_width_in_mbs_minus1 */
    sps->pic_height_in_map_units_minus1 = uexpbits(bb);                 /* pic_height_in_map_units_minus1 */
    sps->frame_mbs_only_flag = getbit(bb);                              /* frame_mbs_only_flag */
    //*width = (sps->pic_width_in_mbs_minus1 + 1) * 16;
    //*height = (2 - sps->frame_mbs_only_flag) * (sps->pic_height_in_map_units_minus1 + 1) * 16;
    if (!(sps->frame_mbs_only_flag))
        sps->mb_adaptive_frame_field_flag = getbit(bb);                 /* mb_adaptive_frame_field_flag */
    else
        sps->mb_adaptive_frame_field_flag = 0;
    sps->direct_8x8_inference_flag = getbit(bb);                        /* direct_8x8_inference_flag */
    sps->frame_cropping_flag = getbit(bb);                              /* frame_cropping_flag */
    if (sps->frame_cropping_flag) {
        sps->frame_crop_left_offset   = uexpbits(bb);                   /* frame_crop_left_offset */
        sps->frame_crop_right_offset  = uexpbits(bb);                   /* frame_crop_right_offset */
        sps->frame_crop_top_offset    = uexpbits(bb);                   /* frame_crop_top_offset */
        sps->frame_crop_bottom_offset = uexpbits(bb);                   /* frame_crop_bottom_offset */
    }
    sps->vui_parameters_present_flag = getbit(bb);                      /* vui_parameters_present_flag */
    if (sps->vui_parameters_present_flag) {
        parse_vui_params(bb, &sps->vui);
    }
    parse_rbsp_trailing_bits(bb, where);

    /* calculate derived parameters */
    sps->ChromaArrayType = sps->separate_colour_plane_flag? 0 : sps->chroma_format_idc;
    sps->SubWidthC = sps->chroma_format_idc >2 ? 1 : 2;
    sps->SubHeightC = sps->chroma_format_idc>1 ? 1 : 2;
    sps->CropUnitX = sps->ChromaArrayType==0? 1                          : sps->SubWidthC;
    sps->CropUnitY = sps->ChromaArrayType==0? 2-sps->frame_mbs_only_flag : sps->SubHeightC * (2 - sps->frame_mbs_only_flag);
    sps->MbWidthC = sps->chroma_format_idc ? 16 / sps->SubWidthC : 0;
    sps->MbHeightC = sps->chroma_format_idc? 16 / sps->SubHeightC : 0;
    sps->BitDepthY = 8 + sps->bit_depth_luma_minus8;
    sps->QpBdOffsetY = 6 * sps->bit_depth_luma_minus8;
    sps->BitDepthC = 8 + sps->bit_depth_chroma_minus8;
    sps->QpBdOffsetC = 6 * sps->bit_depth_chroma_minus8;
    sps->MaxFrameNum = 1<<(sps->log2_max_frame_num_minus4+4);
    sps->PicWidthInMbs = sps->pic_width_in_mbs_minus1+1;
    sps->PicWidthInSamplesl = sps->PicWidthInMbs * 16;
    sps->PicWidthInSamplesc = sps->PicWidthInMbs * sps->MbWidthC;
    sps->FrameHeightInMbs = (2 - sps->frame_mbs_only_flag) * (sps->pic_height_in_map_units_minus1 + 1);

    /* calculate frame cropping rectangle, this is so tricky they didn't give it proper variables */
    sps->FrameCropWidthInSamples  = (sps->pic_width_in_mbs_minus1 + 1) * 16;
    sps->FrameCropHeightInSamples = (2 - sps->frame_mbs_only_flag) * (sps->pic_height_in_map_units_minus1 + 1) * 16;
    if (sps->frame_cropping_flag) {

        sps->FrameCropWidthInSamples -= (sps->frame_crop_left_offset + sps->frame_crop_right_offset) * sps->CropUnitX;
        sps->FrameCropHeightInSamples -= (sps->frame_crop_top_offset + sps->frame_crop_bottom_offset) * sps->CropUnitY;
    }
    return sps;
}

int parse_sequence_param_ext(struct bitbuf *bb)
{
    const char *where = "h.264 sequence param. set ext.";
    int seq_parameter_set_id = uexpbits(bb);                            /* seq_parameter_set_id */
    if (seq_parameter_set_id>31) {
        rvmessage("%s: seq_parameter_set_id is greater than 31: %d", where, seq_parameter_set_id);
        return -1;
    }
    int aux_format_idc = uexpbits(bb);                                  /* aux_format_idc */
    if (aux_format_idc!=0) {
        int bit_depth_aux_minus8 = uexpbits(bb);                        /* bit_depth_aux_minus8 */
        flushbits(bb, 1);                                               /* alpha_incr_flag */
        flushbits(bb, bit_depth_aux_minus8+9);                          /* alpha_opaque_value */
        flushbits(bb, bit_depth_aux_minus8+9);                          /* alpha_transparent_value */
    }
    if (getbit(bb))                                                     /* additional_extension_flag */
        return 0;
    else
        return parse_rbsp_trailing_bits(bb, where);
}

struct rv264pic_parameter_set *parse_picture_param(struct bitbuf *bb, struct rv264seq_parameter_set **spss, int verbose)
{
    const char *where = "h.264 picture param. set";

    /* allocate a picture parameter set */
    struct rv264pic_parameter_set *pps = (struct rv264pic_parameter_set *)rvalloc(NULL, sizeof(struct rv264pic_parameter_set), 1);
    pps->occupied = 1;

    pps->pic_parameter_set_id = uexpbits(bb);                           /* pic_parameter_set_id */
    if (pps->pic_parameter_set_id>255) {
        rvmessage("%s: pic_parameter_set_id is greater than 31: %d", where, pps->pic_parameter_set_id);
        rvfree(pps);
        return NULL;
    }
    pps->seq_parameter_set_id = uexpbits(bb);                           /* seq_parameter_set_id */
    if (pps->seq_parameter_set_id>31) {
        rvmessage("%s: seq_parameter_set_id is greater than 31: %d", where, pps->seq_parameter_set_id);
        rvfree(pps);
        return NULL;
    }
    pps->sps = spss[pps->seq_parameter_set_id];
    struct rv264seq_parameter_set *sps = pps->sps;
    if (!sps || !sps->occupied) {
        if (!sps) {
            rvmessage("%s: seq_parameter_set_id is not occupied: %d", where, pps->seq_parameter_set_id);
        }
        else if (!sps->occupied_warning || verbose>=1) {
            rvmessage("%s: seq_parameter_set_id is not occupied: %d", where, pps->seq_parameter_set_id);
            sps->occupied_warning = 1;
        }
        rvfree(pps);
        return NULL;
    }
    pps->entropy_coding_mode_flag = getbit(bb);                         /* entropy_coding_mode_flag */
    pps->pic_order_present_flag = getbit(bb);                           /* pic_order_present_flag */
    pps->num_slice_groups_minus1 = uexpbits(bb);                        /* num_slice_groups_minus1 */
    if (pps->num_slice_groups_minus1>0) {
        int i, pic_size_in_map_units_minus1;
        pps->slice_group_map_type = uexpbits(bb);                       /* slice_group_map_type */
        switch (pps->slice_group_map_type) {
            case 0:
                for (i=0; i<=pps->num_slice_groups_minus1; i++)
                    uexpbits(bb);                                       /* run_length_minus1[ i ] */
                break;

            case 1:
                break;

            case 2:
                for (i=0; i<pps->num_slice_groups_minus1; i++) {
                    uexpbits(bb);                                       /* top_left[ i ] */
                    uexpbits(bb);                                       /* bottom_right[ i ] */
                }
                break;

            case 3:
            case 4:
            case 5:
                flushbits(bb, 1);                                       /* slice_group_change_direction_flag */
                uexpbits(bb);                                           /* slice_group_change_rate_minus1 */
                break;

            case 6:
                pic_size_in_map_units_minus1 = uexpbits(bb);            /* pic_size_in_map_units_minus1 */
                for (i=0; i<=pic_size_in_map_units_minus1; i++)
                    flushbits(bb, ceillog2(pps->num_slice_groups_minus1+1)); /* slice_group_id[ i ] */
                break;

            default:
                rvmessage("%s: invalid value for slice_group_map_type: %d", where, pps->slice_group_map_type);
                break;
        }
    }
    pps->num_ref_idx_l0_active_minus1 = uexpbits(bb);                   /* num_ref_idx_l0_active_minus1 */
    if (pps->num_ref_idx_l0_active_minus1>31) {
        rvmessage("%s: num_ref_idx_l0_active_minus1 is greater than 31: %d", where, pps->num_ref_idx_l0_active_minus1);
        rvfree(pps);
        return NULL;
    }
    pps->num_ref_idx_l1_active_minus1 = uexpbits(bb);                   /* num_ref_idx_l1_active_minus1 */
    if (pps->num_ref_idx_l1_active_minus1>31) {
        rvmessage("%s: num_ref_idx_l1_active_minus1 is greater than 31: %d", where, pps->num_ref_idx_l1_active_minus1);
        rvfree(pps);
        return NULL;
    }
    pps->weighted_pred_flag = getbit(bb);                               /* weighted_pred_flag */
    pps->weighted_bipred_idc = getbits(bb, 2);                          /* weighted_bipred_idc */
    if (pps->weighted_bipred_idc==3) {
        rvmessage("%s: invalid value for weighted_bipred_idc: 3", where);
        rvfree(pps);
        return NULL;
    }
    pps->pic_init_qp_minus26 = sexpbits(bb);                            /* pic_init_qp_minus26  *//* relative to 26 */
    if (pps->pic_init_qp_minus26<-26 || pps->pic_init_qp_minus26>25) {
        rvmessage("%s: invalid value for pic_init_qp_minus26: %d", where, pps->pic_init_qp_minus26);
        rvfree(pps);
        return NULL;
    }
    pps->pic_init_qs_minus26 = sexpbits(bb);                            /* pic_init_qs_minus26  *//* relative to 26 */
    if (pps->pic_init_qs_minus26<-26 || pps->pic_init_qs_minus26>25) {
        rvmessage("%s: invalid value for pic_init_qs_minus26: %d", where, pps->pic_init_qs_minus26);
        rvfree(pps);
        return NULL;
    }
    pps->chroma_qp_index_offset = sexpbits(bb);                         /* chroma_qp_index_offset */
    pps->deblocking_filter_control_present_flag = getbit(bb);           /* deblocking_filter_control_present_flag */
    pps->constrained_intra_pred_flag = getbit(bb);                      /* constrained_intra_pred_flag */
    pps->redundant_pic_cnt_present_flag = getbit(bb);                   /* redundant_pic_cnt_present_flag */
    if (more_rbsp_data(bb)) {
        int i;
        pps->transform_8x8_mode_flag = getbit(bb);                      /* transform_8x8_mode_flag */
        pps->pic_scaling_matrix_present_flag = getbit(bb);              /* pic_scaling_matrix_present_flag */
        if (pps->pic_scaling_matrix_present_flag) {
            int use_default;
            int scaling_list4x4[16];
            int scaling_list8x8[64];
            for (i=0; i<6+2*pps->transform_8x8_mode_flag; i++) {
                if (getbit(bb)) {                                       /* pic_scaling_list_present_flag[ i ] */
                    if (i<6)
                        parse_scaling_list(bb, scaling_list4x4, 16, &use_default);
                    else
                        parse_scaling_list(bb, scaling_list8x8, 64, &use_default);
                }
            }
        }
        pps->second_chroma_qp_index_offset = sexpbits(bb);              /* second_chroma_qp_index_offset */
    } else {
        pps->second_chroma_qp_index_offset = pps->chroma_qp_index_offset;
    }
    parse_rbsp_trailing_bits(bb, where);
    if (pps->seq_parameter_set_id==sps->seq_parameter_set_id)
        return pps;
    rvfree(pps);
    return NULL;
}

void parse_sei_message(struct bitbuf *bb, struct rv264sequence *seq, int verbose)
{
    int payloadType = 0;

    while (showbits(bb, 8) == 0xFF) {
        flushbits(bb, 8);                                               /* ff_byte,  equal to 0xFF */
        payloadType += 255;
    }
    int last_payload_type_byte = getbits8(bb);                          /* last_payload_type_byte */
    payloadType += last_payload_type_byte;

    int payloadSize = 0;
    while (showbits(bb, 8) == 0xFF) {
        flushbits(bb, 8);                                               /* ff_byte,  equal to 0xFF */
        payloadSize += 255;
    }
    int last_payload_size_byte = getbits8(bb);                          /* last_payload_size_byte */
    payloadSize += last_payload_size_byte;

    if (verbose>=2)
        rvmessage("sei message: payloadType=%d payloadSize=%d", payloadType, payloadSize);

    /* sanity check */
    if (payloadSize==0) {
        if (verbose>=2)
            rvmessage("sei message: payload size is zero");
        return;
    }

    /* note position in bitstream */
    off_t pos = tellbits(bb);

    /* parse sei payload */
    switch (payloadType) {
        case 0: /* buffering period */
        {
            int seq_parameter_set_id = uexpbits(bb);                    /* seq_parameter_set_id */
            if (seq_parameter_set_id>=32) {
                rvmessage("sei message: value for seq_parameter_set_id is out of range: %d", seq_parameter_set_id);
                return;
            }
            struct rv264seq_parameter_set *sps = seq->sps[seq_parameter_set_id];
            if (sps && sps->occupied) {
                seq->active_sps = seq_parameter_set_id;
                int SchedSelIdx;
                if (sps->vui.nal_hrd_parameters_present_flag) {
                    hrd_parameters *hrd = &sps->vui.nal_hrd_parameters;

                    for (SchedSelIdx=0; SchedSelIdx<=hrd->cpb_cnt_minus1; SchedSelIdx++) {
                        hrd->initial_cpb_removal_delay[SchedSelIdx] = (int) llgetbits(bb, hrd->initial_cpb_removal_delay_length_minus1+1);
                        hrd->initial_cpb_removal_delay_offset[SchedSelIdx] = (int) llgetbits(bb, hrd->initial_cpb_removal_delay_length_minus1+1);

                        /* check removal delay is valid */
                        int64_t BitRate = (hrd->bit_rate_value_minus1[SchedSelIdx] + 1) * (1 << (6+hrd->bit_rate_scale));
                        int64_t CpbSize = (hrd->cpb_size_value_minus1[SchedSelIdx] + 1) * (1 << (4+hrd->cpb_size_scale));
                        int max_initial_cpb_removal_delay_offset = 90000ll*CpbSize/BitRate;
                        if (hrd->initial_cpb_removal_delay[SchedSelIdx]<0 ||
                            hrd->initial_cpb_removal_delay[SchedSelIdx]>max_initial_cpb_removal_delay_offset)
                            rvmessage("sei: buffering period: initial_cpb_removal_delay is out of range: %d", hrd->initial_cpb_removal_delay[SchedSelIdx]);
                    }
                }
                if (sps->vui.vcl_hrd_parameters_present_flag) {
                    hrd_parameters *hrd = &sps->vui.vcl_hrd_parameters;

                    for (SchedSelIdx=0; SchedSelIdx<=hrd->cpb_cnt_minus1; SchedSelIdx++) {
                        hrd->initial_cpb_removal_delay[SchedSelIdx] = (int) llgetbits(bb, hrd->initial_cpb_removal_delay_length_minus1+1);
                        hrd->initial_cpb_removal_delay_offset[SchedSelIdx] = (int) llgetbits(bb, hrd->initial_cpb_removal_delay_length_minus1+1);

                        /* check removal delay is valid */
                        int64_t BitRate = (hrd->bit_rate_value_minus1[SchedSelIdx] + 1) * (1 << (6+hrd->bit_rate_scale));
                        int64_t CpbSize = (hrd->cpb_size_value_minus1[SchedSelIdx] + 1) * (1 << (4+hrd->cpb_size_scale));
                        int max_initial_cpb_removal_delay_offset = 90000ll*CpbSize/BitRate;
                        if (hrd->initial_cpb_removal_delay[SchedSelIdx]<0 ||
                            hrd->initial_cpb_removal_delay[SchedSelIdx]>max_initial_cpb_removal_delay_offset) {
                            rvmessage("sei: buffering period: initial_cpb_removal_delay is out of range: %d", hrd->initial_cpb_removal_delay[SchedSelIdx]);
                            rvmessage("sei: CpbSize=%ld BitRate=%ld max_initial_cpb_removal_delay=%d initial_cpb_removal_delay=%d", BitRate, CpbSize, max_initial_cpb_removal_delay_offset, hrd->initial_cpb_removal_delay[SchedSelIdx]);
                        }
                    }
                }
            } else {
                if (verbose>=2)
                    rvmessage("skipping buffering period SEI message as no sps is active");
                flushbits(bb, payloadSize*8);
            }
            break;
        }

        case 1: /* pic timing */
        {
            /* NOTE 1 – The syntax of the picture timing SEI message is dependent on the content of the sequence parameter set that is active
            for the primary coded picture associated with the picture timing SEI message. However, unless the picture timing SEI message of
            an IDR access unit is preceded by a buffering period SEI message within the same access unit, the activation of the associated
            sequence parameter set (and, for IDR pictures that are not the first picture in the bitstream, the determination that the primary coded
            picture is an IDR picture) does not occur until the decoding of the first coded slice NAL unit of the primary coded picture. Since
            the coded slice NAL unit of the primary coded picture follows the picture timing SEI message in NAL unit order, there may be
            cases in which it is necessary for a decoder to store the RBSP containing the picture timing SEI message until determining the
            parameters of the sequence parameter that will be active for the primary coded picture, and then perform the parsing of the picture
            timing SEI message. */
            struct rv264seq_parameter_set *sps = seq->sps[seq->active_sps];
            struct rv264pic_timing *pti = &seq->sei_message.pic_timing;
            if (sps && sps->occupied) {
                /* determine CpbDpbDelaysPresentFlag */
                int CpbDpbDelaysPresentFlag = 0;
                if (sps->vui.nal_hrd_parameters_present_flag)
                    CpbDpbDelaysPresentFlag = 1;
                if (sps->vui.vcl_hrd_parameters_present_flag)
                    CpbDpbDelaysPresentFlag = 1;

                if (CpbDpbDelaysPresentFlag) {
                    getbits(bb, 5);                                         /* cpb_removal_delay */
                    getbits(bb, 5);                                         /* dpb_output_delay */
                }
                if (sps->vui.pic_struct_present_flag) {
                    pti->pic_struct = getbits(bb, 4);                       /* pic_struct */
                    int NumClockTS = 1;
                    switch (pti->pic_struct) {
                        case 3:
                        case 4:
                        case 7:
                            NumClockTS = 2;
                            break;
                        case 5:
                        case 6:
                        case 8:
                            NumClockTS = 3;
                            break;
                    }
                    for (int i=0; i<NumClockTS; i++) {
                        int clock_timestamp_flag = getbit(bb);
                        if (clock_timestamp_flag) {
                            flushbits(bb, 2);                               /* ct_type */
                            flushbits(bb, 1);                               /* nuit_field_based_flag */
                            flushbits(bb, 5);                               /* counting_type */
                            int full_timestamp_flag = getbit(bb);           /* full_timestamp_flag */
                            flushbits(bb, 1);                               /* discontinuity_flag */
                            flushbits(bb, 1);                               /* cnt_dropped_flag */
                            pti->n_frames = getbits(bb, 8);                 /* n_frames */
                            if (full_timestamp_flag) {
                                pti->seconds_flag = pti->minutes_flag = pti->hours_flag = 1;
                                pti->seconds_value = getbits(bb, 6);        /* seconds_value - 0..59 */
                                pti->minutes_value = getbits(bb, 6);        /* minutes_value - 0..59 */
                                pti->hours_value   = getbits(bb, 5);        /* hours_value - 0..23 */
                            } else {
                                pti->seconds_flag = getbit(bb);
                                if (pti->seconds_flag) {
                                    pti->seconds_value = getbits(bb, 6);    /* seconds_value - range 0..59 */
                                    pti->minutes_flag = getbit(bb);
                                    if (pti->minutes_flag) {
                                        pti->minutes_value = getbits(bb, 6);/* minutes_value - 0..59 */
                                        pti->hours_flag = getbit(bb);
                                        if (pti->hours_flag)
                                            pti->hours_value = getbits(bb, 5);/* hours_value - 0..23 */
                                    }
                                }
                            }
                            int time_offset_length = 24;    /* "when the time_offset_length syntax element is not present, it shall be inferred to be equal to 24" */
                            if (sps->vui.nal_hrd_parameters_present_flag)
                                if (sps->vui.nal_hrd_parameters.time_offset_length > 0)
                                    time_offset_length = sps->vui.nal_hrd_parameters.time_offset_length;
                            if (sps->vui.vcl_hrd_parameters_present_flag)
                                if (sps->vui.vcl_hrd_parameters.time_offset_length > 0)
                                    time_offset_length = sps->vui.vcl_hrd_parameters.time_offset_length;
                            flushbits(bb, time_offset_length);              /* time_offset */
                        }
                    }
                }
            } else {
                if (verbose>=2)
                    rvmessage("skipping pic timing SEI message as no sps is active");
                flushbits(bb, payloadSize*8);
            }
            break;
        }

        case 4: /* user data registered itu-t t35 */
        {
            struct rv264user_data_registered_itu_t_t35 *pud = &seq->sei_message.user_data_registered_itu_t_t35;
            int itu_t_t35_country_code = getbits(bb, 8);
            if (itu_t_t35_country_code==0xff)
                getbits(bb, 8);                                     /* itu_t_t35_country_code_extension_byte */
            int itu_t_35_provider_code = getbits(bb, 16);
            if (itu_t_t35_country_code==0xb5 && itu_t_35_provider_code==0x0031) {
                /* scte 128-1 */
                int user_identifier = getbits32(bb);
                if (user_identifier==0x47413934) {
                    /* atsc1_data */
                    int user_data_type_code = getbits(bb, 8);
                    if (user_data_type_code==0x03) {
                        /* cea-708 */
                        flushbits(bb, 3);
                        int cc_count = getbits(bb, 5);
                        flushbits(bb, 8);
                        char cc_data[2*31+1] = {0}, *ptr = cc_data;
                        for (int i=0; i<cc_count; i++) {
                            flushbits(bb, 5);
                            int cc_valid = getbit(bb);                  /* cc_valid */
                            if (cc_valid)
                                pud->cc_valid = 1;
                            getbits(bb, 2);                             /* cc_type */
                            if (cc_valid) {
                                *ptr++ = getbits(bb, 8);
                                *ptr++ = getbits(bb, 8);
                            } else
                                flushbits(bb, 16);
                        }
                        if (verbose>=4)
                            rvmessage("cc_data: %s", cc_data);
                    }
                    flushbits(bb, 8);                                   /* marker_bits */
                }
            }
            break;
        }

        default:
            flushbits(bb, payloadSize*8);
            if (verbose>=1)
                rvmessage("info: unsupported sei message payload type: %d", payloadType);
            break;
    }
    alignbits(bb);

    /* check sei message was of advertised length */
    off_t size = (tellbits(bb)-pos)/8;
    if (size != payloadSize) {
        if (verbose>=3)
            rvmessage("sei message: message size of %zd bytes does not match payload size: %d", size, payloadSize);
        if (payloadSize > size)
            flushbits(bb, (payloadSize-size)*8);
    }
}

void parse_sei(struct bitbuf *bb, struct rv264sequence *seq, int verbose)
{
    do {
        parse_sei_message(bb, seq, verbose);
    } while (more_rbsp_data(bb));
    parse_rbsp_trailing_bits(bb, "h.264 sei message");
}

static int parse_ref_pic_list_reordering(struct bitbuf *bb, slice_type_t slice_type, struct ref_pic_list_reordering_t *rplr)
{
    int reordering_of_pic_nums_idc;
    if (!slice_is_i(slice_type)) {
        rplr->ref_pic_list_reordering_flag[0] = getbit(bb);             /* ref_pic_list_reordering_flag_l0 */
        if (rplr->ref_pic_list_reordering_flag[0]) {
            do {
                reordering_of_pic_nums_idc = uexpbits(bb);              /* reordering_of_pic_nums_idc */
                if (reordering_of_pic_nums_idc==0 || reordering_of_pic_nums_idc==1)
                    uexpbits(bb);                                       /* abs_diff_pic_num_minus1 */
                else if (reordering_of_pic_nums_idc==2)
                    uexpbits(bb);                                       /* long_term_pic_num */
            } while (reordering_of_pic_nums_idc!=3);
        }
    }
    if (slice_is_b(slice_type)) {
        rplr->ref_pic_list_reordering_flag[1] = getbit(bb);             /* ref_pic_list_reordering_flag_l1 */
        if (rplr->ref_pic_list_reordering_flag[1])
            do {
                reordering_of_pic_nums_idc = uexpbits(bb);              /* reordering_of_pic_nums_idc */
                if (reordering_of_pic_nums_idc==0 || reordering_of_pic_nums_idc==1)
                    uexpbits(bb);                                       /* abs_diff_pic_num_minus1 */
                else if (reordering_of_pic_nums_idc==2)
                    uexpbits(bb);                                       /* long_term_pic_num */
            } while (reordering_of_pic_nums_idc!=3);
    }
    return 0;
}

static int parse_pred_weight_table(struct bitbuf *bb, struct slice_header *sh)
{
    uexpbits(bb);                               /* luma_log2_weight_denom */
    if (sh->pps->sps->chroma_format_idc!=0)
        uexpbits(bb);                           /* chroma_log2_weight_denom */
    int i, j;
    for (i=0; i<=sh->num_ref_idx_l0_active_minus1; i++) {
        if (getbit(bb)) {                       /* luma_weight_l0_flag */
            sexpbits(bb);                       /* luma_weight_l0[ i ] */
            sexpbits(bb);                       /* luma_offset_l0[ i ] */
        }
        if (sh->pps->sps->chroma_format_idc!=0) {
            if (getbit(bb))                     /* chroma_weight_l0_flag */
                for (j=0; j<2; j++) {
                sexpbits(bb);               /* chroma_weight_l0[ i ][ j ] */
                sexpbits(bb);               /* chroma_offset_l0[ i ][ j ] */
                }
        }
    }
    if (slice_is_b(sh->slice_type))
        for (i=0; i<=sh->num_ref_idx_l1_active_minus1; i++) {
        if (getbit(bb)) {                   /* luma_weight_l1_flag */
            sexpbits(bb);                   /* luma_weight_l1[ i ] */
            sexpbits(bb);                   /* luma_offset_l1[ i ] */
        }
        if (sh->pps->sps->chroma_format_idc!=0) {
            if (getbit(bb))                 /* chroma_weight_l1_flag */
                for (j=0; j<2; j++) {
                sexpbits(bb);               /* chroma_weight_l1_flag *//* chroma_weight_l1[ i ][ j ] */
                sexpbits(bb);               /* chroma_offset_l1[ i ][ j ] */
                }
        }
    }
    return 0;
}

static int parse_dec_ref_pic_marking(struct bitbuf *bb, int nal_unit_type, slice_header *sh)
{
    if (nal_unit_type==NAL_CODED_SLICE_IDR) {
        sh->dec_ref_pic_marking.no_output_of_prior_pics_flag = getbit(bb);     /* no_output_of_prior_pics_flag */
        sh->dec_ref_pic_marking.long_term_reference_flag = getbit(bb);         /* long_term_reference_flag */
    } else {
        sh->dec_ref_pic_marking.adaptive_ref_pic_marking_mode_flag = getbit(bb);    /* adaptive_ref_pic_marking_mode_flag */
        if (sh->dec_ref_pic_marking.adaptive_ref_pic_marking_mode_flag) {
            int i=0, memory_management_control_operation;
            do {
                memory_management_control_operation = uexpbits(bb); /* memory_management_control_operation */
                if (memory_management_control_operation==1 || memory_management_control_operation==3)
                    sh->dec_ref_pic_marking.difference_of_pic_nums_minus1[i] = uexpbits(bb);            /* difference_of_pic_nums_minus1 */
                if(memory_management_control_operation==2)
                    sh->dec_ref_pic_marking.long_term_pic_num[i] = uexpbits(bb);                        /* long_term_pic_num */
                if (memory_management_control_operation==3 || memory_management_control_operation==6)
                    sh->dec_ref_pic_marking.long_term_frame_idx[i] = uexpbits(bb);                      /* long_term_frame_idx */
                if (memory_management_control_operation==4)
                    sh->dec_ref_pic_marking.max_long_term_frame_idx_plus1[i] = uexpbits(bb);            /* max_long_term_frame_idx_plus1 */
                sh->dec_ref_pic_marking.mmco_commands[i] = memory_management_control_operation;
                i++;
            } while (memory_management_control_operation!=0);
            sh->dec_ref_pic_marking.nb_of_mmco_cmd = i;
        }
    }
    return 0;
}

struct slice_header *parse_slice_header(struct bitbuf *bb, int nal_unit_type, int nal_ref_idc, struct rv264pic_parameter_set **ppss, int verbose)
{
    const char *where = "h.264 slice header";

    /* allocate a slice header */
    struct slice_header *sh = (struct slice_header *)rvalloc(NULL, sizeof(struct slice_header), 1);

    sh->first_mb_in_slice = uexpbits(bb);                               /* first_mb_in_slice */
    sh->slice_type = (slice_type_t)uexpbits(bb);                        /* slice_type */
    if (sh->slice_type>9) {
        rvmessage("%s: invalid value for slice_type: %d", where, sh->slice_type);
        rvfree(sh);
        return NULL;
    }
    sh->pic_parameter_set_id = uexpbits(bb);                            /* pic_parameter_set_id */
    if (sh->pic_parameter_set_id>255) {
        rvmessage("%s: pic_parameter_set_id is greater than 255: %d", where, sh->pic_parameter_set_id);
        rvfree(sh);
        return NULL;
    }
    sh->pps = ppss[sh->pic_parameter_set_id];
    if (sh->pps==NULL || !sh->pps->occupied) {
        if ((sh->pps && !sh->pps->occupied_warning) || verbose>=1) {
            rvmessage("%s: pic_parameter_set_id is not occupied: %d", where, sh->pic_parameter_set_id);
            if (sh->pps)
                sh->pps->occupied_warning = 1;
        }
        rvfree(sh);
        return NULL;
    }
    if (!sh->pps->sps->occupied) {
        if (!sh->pps->sps->occupied_warning) {
            rvmessage("%s: seq_parameter_set_id is not occupied: %d", where, sh->pps->seq_parameter_set_id);
            sh->pps->sps->occupied_warning = 1;
        }
        rvfree(sh);
        return NULL;
    }
    const struct rv264pic_parameter_set *pps = sh->pps;
    const struct rv264seq_parameter_set *sps = pps->sps;
    /* TODO check first_mb_in_slice is within range */
    sh->frame_num = getbits(bb, sps->log2_max_frame_num_minus4+4);      /* frame_num */
    sh->field_pic_flag = 0;
    sh->bottom_field_flag = 0;
    if (!sps->frame_mbs_only_flag) {
        sh->field_pic_flag = getbit(bb);                                /* field_pic_flag */
        if (sh->field_pic_flag)
            sh->bottom_field_flag = getbit(bb);                         /* bottom_field_flag */
    }
    if (nal_unit_type==5)
        sh->idr_pic_id = uexpbits(bb);                                  /* idr_pic_id */
    /* parse and decode PicOrderCnt */
    sh->pic_order_cnt_lsb = 0;
    sh->delta_pic_order_cnt_bottom = 0;
    sh->delta_pic_order_cnt[0] = 0;
    sh->delta_pic_order_cnt[1] = 0;
    if (sps->pic_order_cnt_type==0) {
        sh->pic_order_cnt_lsb = getbits(bb, sps->log2_max_pic_order_cnt_lsb_minus4+4); /* pic_order_cnt_lsb */
        if (pps->pic_order_present_flag && !sh->field_pic_flag)
            sh->delta_pic_order_cnt_bottom = sexpbits(bb);              /* delta_pic_order_cnt_bottom */
    }
    if (sps->pic_order_cnt_type==1 && !sps->delta_pic_order_always_zero_flag) {
        sh->delta_pic_order_cnt[0] = sexpbits(bb);                      /* delta_pic_order_cnt[0] */
        if (pps->pic_order_present_flag && !sh->field_pic_flag)
            sh->delta_pic_order_cnt[1] = sexpbits(bb);                  /* delta_pic_order_cnt[1] */
    }
    sh->PicOrderCnt = decode_poc(sps, nal_unit_type, nal_ref_idc, sh->frame_num, sh->field_pic_flag, sh->bottom_field_flag, sh->pic_order_cnt_lsb, sh->delta_pic_order_cnt_bottom, sh->delta_pic_order_cnt);
    if (pps->redundant_pic_cnt_present_flag)
        sh->redundant_pic_cnt = uexpbits(bb);                           /* redundant_pic_cnt */
    else
       sh->redundant_pic_cnt = 0;
    if (sh->redundant_pic_cnt>127) {
        rvmessage("%s: redundant_pic_cnt is greater than 127: %d", where, sh->redundant_pic_cnt);
        rvfree(sh);
        return NULL;
    }
    if (slice_is_b(sh->slice_type))
        sh->direct_spatial_mv_pred_flag = getbit(bb);                   /* direct_spatial_mv_pred_flag */
    if (slice_is_p(sh->slice_type) || slice_is_b(sh->slice_type) ) {
        sh->num_ref_idx_l0_active_minus1 = pps->num_ref_idx_l0_active_minus1;
        sh->num_ref_idx_l1_active_minus1 = pps->num_ref_idx_l1_active_minus1;
        sh->num_ref_idx_active_override_flag = getbit(bb);              /* num_ref_idx_active_override_flag */
        if (sh->num_ref_idx_active_override_flag) {
            sh->num_ref_idx_l0_active_minus1 = uexpbits(bb);            /* num_ref_idx_l0_active_minus1 */
            if (slice_is_b(sh->slice_type))
                sh->num_ref_idx_l1_active_minus1 = uexpbits(bb);        /* num_ref_idx_l1_active_minus1 */
        }
    }
    parse_ref_pic_list_reordering(bb, sh->slice_type, &sh->ref_pic_list_reordering);
    if ((pps->weighted_pred_flag && slice_is_p(sh->slice_type)) || (pps->weighted_bipred_idc==1 && slice_is_b(sh->slice_type)))
        parse_pred_weight_table(bb, sh);
    if (nal_ref_idc!=0)
        parse_dec_ref_pic_marking(bb, nal_unit_type, sh);
    if (pps->entropy_coding_mode_flag && !(slice_is_i(sh->slice_type)))
        sh->cabac_init_idc = uexpbits(bb);                              /* cabac_init_idc */
    sh->slice_qp_delta = sexpbits(bb);                                  /* slice_qp_delta */
    if (sh->slice_type==SLICE_SP || sh->slice_type==SLICE_SI) {
        if (sh->slice_type==SLICE_SP)
            sh->sp_for_switch_flag = flushbits(bb, 1);                  /* sp_for_switch_flag */
        sh->slice_qs_delta = sexpbits(bb);                              /* slice_qs_delta */
    }
    if (pps->deblocking_filter_control_present_flag) {
        sh->disable_deblocking_filter_idc = uexpbits(bb);               /* disable_deblocking_filter_idc */
        if (sh->disable_deblocking_filter_idc!=1) {
            sh->slice_alpha_c0_offset_div2 = sexpbits(bb);              /* slice_alpha_c0_offset_div2 */
            sh->slice_beta_offset_div2 = sexpbits(bb);                  /* slice_beta_offset_div2 */
        }
    } else
        sh->disable_deblocking_filter_idc = 0;
    if (sh->disable_deblocking_filter_idc>2) {
        rvmessage("%s: invalid value for disable_deblocking_filter_idc: %d", where, sh->disable_deblocking_filter_idc);
        rvfree(sh);
        return NULL;
    }
    if (pps->num_slice_groups_minus1>0 && pps->slice_group_map_type>=3 && pps->slice_group_map_type<=5)
        ;/* slice_group_change_cycle */

    /* copy nal parameters */
    sh->nal_ref_idc = nal_ref_idc;
    sh->nal_unit_type = nal_unit_type;

    /* calculate derived parameters */
    sh->MbaffFrameFlag = sps->mb_adaptive_frame_field_flag && !sh->field_pic_flag;
    sh->PicHeightInMbs = sps->FrameHeightInMbs / (1 + sh->field_pic_flag);
    sh->PicHeightInSamplesl = sh->PicHeightInMbs * 16;
    sh->PicHeightInSamplesc = sh->PicHeightInMbs * sps->MbHeightC;
    sh->PicSizeInMbs = sps->PicWidthInMbs * sh->PicHeightInMbs;
    sh->MaxPicNum = sh->field_pic_flag==0? sps->MaxFrameNum : 2*sps->MaxFrameNum;
    sh->CurrPicNum = sh->field_pic_flag==0? sh->frame_num : 2*sh->frame_num+1;

    return sh;
}

int NextMbAddress(int n, struct slice_header *sh)
{
    const struct rv264pic_parameter_set *pps = sh->pps;

    if (pps->num_slice_groups_minus1>0)
        rvexit("TODO: FMO");

    int i = n+1;
    while ( i<sh->PicSizeInMbs && 0 /*MbToSliceGroupMap[i] != MbToSliceGroupMap[n]*/ )
        i++;
    return i;
}

int parse_slice_data(struct bitbuf *bb, struct slice_header *sh, struct rv264macro *mb)
{
    const char *where = "h.264 slice data";
    const struct rv264pic_parameter_set *pps = sh->pps;
    //const struct seq_parameter_set *sps = pps->sps;

    if (pps->entropy_coding_mode_flag)
        alignbits(bb);                                              /* cabac_alignment_one_bit */

    int CurrMbAddr = sh->first_mb_in_slice * (1 + sh->MbaffFrameFlag);
    int moreDataFlag = 1;
    int prevMbSkipped = 0;

    /* macroblock loop */
    int num_macros = 0;
    do {
        if (CurrMbAddr >= sh->PicSizeInMbs) {
            rvmessage("%s: mb_addr is larger than number of macroblocks in picture %d: %d", where, sh->PicSizeInMbs, CurrMbAddr);
            return num_macros;
        }

        if (!slice_is_i(sh->slice_type)) {
            if (!pps->entropy_coding_mode_flag) {
                int mb_skip_run = uexpbits(bb);                     /* mb_skip_run */
                if (mb_skip_run > sh->PicSizeInMbs-CurrMbAddr)
                    rvmessage("%s: mb_skip_run is larger than remaining number of macroblocks in picture: %d", where, mb_skip_run);
                prevMbSkipped = mb_skip_run>0;
                int i;
                for (i=0; i<mb_skip_run; i++) {
                    CurrMbAddr = NextMbAddress(CurrMbAddr, sh);
                    mb[CurrMbAddr].mb_addr = CurrMbAddr;
                    mb[CurrMbAddr].mb_type = slice_is_p(sh->slice_type)? P_Skip : B_Skip;
                    mb[CurrMbAddr].sh = sh;
                }
                num_macros += mb_skip_run;
                moreDataFlag = more_rbsp_data(bb);
            } else {
                /* TODO CABAC
                mb_skip_flag
                num_macros++;
                moreDataFlag = !mb_skip_flag*/
            }
        }
        if (moreDataFlag) {
            mb[CurrMbAddr].sh = sh;
            mb[CurrMbAddr].mb_addr = CurrMbAddr;
            mb[CurrMbAddr].mb_field_decoding_flag = 0;
            if (sh->MbaffFrameFlag && (CurrMbAddr%2 == 0 || (CurrMbAddr%2 == 1 && prevMbSkipped)))
                mb[CurrMbAddr].mb_field_decoding_flag = getbit(bb);/* mb_field_decoding_flag */
            parse_macroblock_layer(bb, &mb[CurrMbAddr]);
        }
        if (!pps->entropy_coding_mode_flag)
            moreDataFlag = more_rbsp_data(bb);
        else {
            /* TODO CABAC
            if (!slice_is_i(sh->slice_type))
                prevMbSkipped = mb_skip_flag;
            if (MbaffFrameFlag && CurrMbAddr%2 == 0)
                moreDataFlag = 1;
            else {
                end_of_slice_flag
                moreDataFlag = !end_of_slice_flag
            }*/
        }
        num_macros++;
        CurrMbAddr = NextMbAddress(CurrMbAddr, sh);
    } while (moreDataFlag);
    return num_macros;
}

void parse_macroblock_layer(struct bitbuf *bb, struct rv264macro *mb)
{
    const struct slice_header *sh = mb->sh;
    const struct rv264pic_parameter_set *pps = sh->pps;
    const struct rv264seq_parameter_set *sps = pps->sps;

    //printf("*********** POC: %d (I/P) MB: %d Slice: 0 Type 2 **********\n", sh->PicOrderCnt, mb->mb_addr);


    /* get "unified" macroblock type */
    int mb_type_int;
    mb_type_int = uexpbits(bb);                                     /* mb_type */
    if (slice_is_p(sh->slice_type))
        mb_type_int += P_OFFSET;
    if (slice_is_b(sh->slice_type))
        mb_type_int += B_OFFSET;
    mb->mb_type = (mb_type_t)mb_type_int;

    /* parse PCM macroblocks */
    if (mb->mb_type==I_PCM) {
        int i;
        alignbits(bb);                                              /* pcm_alignment_zero_bit */
        for( i = 0; i < 256; i++ )
            flushbits(bb, 8);                                       /* pcm_sample_luma[ i ] */
        for( i = 0; i < 2 * sps->MbWidthC * sps->MbHeightC; i++ )
            flushbits(bb, 8);                                       /* pcm_sample_chroma[ i ] */
        return;
    }

    /* parse other macroblock types */
    int noSubMbPartSizeLessThan8x8Flag = 1;
    if (mb->mb_type != I_NxN && MbPartPredMode(mb, 0) != Intra_16x16 && NumMbPart(mb) == 4) {
        int mbPartIdx;
        parse_sub_mb_pred(bb, mb);
        for (mbPartIdx = 0; mbPartIdx < 4; mbPartIdx++)
            if (mb->sub_mb_type[mbPartIdx] != B_Direct_8x8) {
                if (NumSubMbPart(mb, mbPartIdx) > 1)
                    noSubMbPartSizeLessThan8x8Flag = 0;
            } else if (!sps->direct_8x8_inference_flag)
                noSubMbPartSizeLessThan8x8Flag = 0;
    } else {
        if (pps->transform_8x8_mode_flag && mb->mb_type == I_NxN)
            mb->transform_size_8x8_flag = getbit(bb);           /* transform_size_8x8_flag */
        parse_mb_pred(bb, mb);
    }
    if (MbPartPredMode(mb, 0) != Intra_16x16 ) {
        mb->coded_block_pattern = map_coded_block_pattern(sps->chroma_format_idc, mb->mb_type, uexpbits(bb));
        if (CodedBlockPatternLuma(mb) > 0 &&
            pps->transform_8x8_mode_flag && mb->mb_type != I_NxN &&
            noSubMbPartSizeLessThan8x8Flag &&
            (mb->mb_type != B_Direct_16x16 || sps->direct_8x8_inference_flag))
            mb->transform_size_8x8_flag = getbit(bb);           /* transform_size_8x8_flag */
    }
    if (CodedBlockPatternLuma(mb) > 0 || CodedBlockPatternChroma(mb) > 0 || MbPartPredMode(mb, 0) == Intra_16x16 ) {
        mb->mb_qp_delta = sexpbits(bb);                         /* mb_qp_delta */
        parse_residual(bb, mb);
    }
    mb->occupied = 1;
}

void parse_mb_pred(struct bitbuf *bb, struct rv264macro *mb)
{
    const struct slice_header *sh = mb->sh;
    const struct rv264pic_parameter_set *pps = sh->pps;
    const struct rv264seq_parameter_set *sps = pps->sps;

    int mbPartIdx;
    int compIdx;

    if (MbPartPredMode(mb, 0) == Intra_4x4 || MbPartPredMode(mb, 0) == Intra_8x8 || MbPartPredMode(mb, 0) == Intra_16x16 ) {
        if (MbPartPredMode(mb, 0) == Intra_4x4) {
            int luma4x4BlkIdx;
            for (luma4x4BlkIdx=0; luma4x4BlkIdx<16; luma4x4BlkIdx++) {
                int prev_intra4x4_pred_mode_flag = getbit(bb);          /* prev_intra4x4_pred_mode_flag[ luma4x4BlkIdx ] */
                if (!prev_intra4x4_pred_mode_flag)
                    mb->rem_intra_pred_mode[luma4x4BlkIdx] = (intra_4x4_pred_mode_t)getbits(bb, 3);/* rem_intra4x4_pred_mode[ luma4x4BlkIdx ] */
                else
                    mb->rem_intra_pred_mode[luma4x4BlkIdx] = (intra_4x4_pred_mode_t)-1;
            }
        }
        if (MbPartPredMode(mb, 0) == Intra_8x8) {
            int luma8x8BlkIdx;
            for (luma8x8BlkIdx=0; luma8x8BlkIdx<4; luma8x8BlkIdx++) {
                int prev_intra8x8_pred_mode_flag = getbit(bb);          /*prev_intra8x8_pred_mode_flag[ luma8x8BlkIdx ] */
                if (!prev_intra8x8_pred_mode_flag)
                    mb->rem_intra_pred_mode[luma8x8BlkIdx] = (intra_4x4_pred_mode_t)getbits(bb, 3);/* rem_intra8x8_pred_mode[ luma8x8BlkIdx ] */
                else
                    mb->rem_intra_pred_mode[luma8x8BlkIdx] = (intra_4x4_pred_mode_t)-1;
            }
        }
        if (sps->chroma_format_idc!=0)
            mb->intra_chroma_pred_mode = (intra_chroma_pred_mode_t)uexpbits(bb); /* intra_chroma_pred_mode */
    } else if (MbPartPredMode(mb, 0) != Direct) {
        for (mbPartIdx = 0; mbPartIdx < NumMbPart(mb); mbPartIdx++)
            if ((sh->num_ref_idx_l0_active_minus1 > 0 || mb->mb_field_decoding_flag ) && MbPartPredMode(mb, mbPartIdx) != Pred_L1)
                mb->ref_idx_l0[mbPartIdx] = texpbits(bb, sh->num_ref_idx_l0_active_minus1);/* ref_idx_l0 */
        for (mbPartIdx = 0; mbPartIdx < NumMbPart(mb); mbPartIdx++)
            if ((sh->num_ref_idx_l1_active_minus1 > 0 || mb->mb_field_decoding_flag ) && MbPartPredMode(mb, mbPartIdx) != Pred_L0)
                mb->ref_idx_l1[mbPartIdx] = texpbits(bb, sh->num_ref_idx_l1_active_minus1);/* ref_idx_l1 */
        for (mbPartIdx = 0; mbPartIdx < NumMbPart(mb); mbPartIdx++)
            if (MbPartPredMode (mb, mbPartIdx) != Pred_L1)
                for (compIdx = 0; compIdx < 2; compIdx++)
                    mb->mvd[0][mbPartIdx][0][compIdx] = sexpbits(bb);   /* mvd_l0 */
        for (mbPartIdx = 0; mbPartIdx < NumMbPart(mb); mbPartIdx++)
            if (MbPartPredMode(mb, mbPartIdx) != Pred_L0)
                for (compIdx = 0; compIdx < 2; compIdx++)
                    mb->mvd[1][mbPartIdx][0][compIdx] = sexpbits(bb);   /* mvd_l1 */
    }
}

void parse_sub_mb_pred(struct bitbuf *bb, struct rv264macro *mb)
{
    const struct slice_header *sh = mb->sh;

    int mbPartIdx;
    int subMbPartIdx;
    int compIdx;

    for (mbPartIdx = 0; mbPartIdx < 4; mbPartIdx++)
        mb->sub_mb_type[mbPartIdx] = (sub_mb_type_t)uexpbits(bb);              /* sub_mb_type */
    for (mbPartIdx = 0; mbPartIdx < 4; mbPartIdx++)
        if ((sh->num_ref_idx_l0_active_minus1 > 0 || mb->mb_field_decoding_flag ) &&
            mb->mb_type != P_8x8ref0 &&
            mb->sub_mb_type[ mbPartIdx ] != B_Direct_8x8 &&
            SubMbPredMode(mb, mbPartIdx) != Pred_L1 )
            mb->ref_idx_l0[ mbPartIdx ] = texpbits(bb, sh->num_ref_idx_l0_active_minus1);   /* ref_idx_l0 */
    for (mbPartIdx = 0; mbPartIdx < 4; mbPartIdx++)
        if ((sh->num_ref_idx_l1_active_minus1 > 0 || mb->mb_field_decoding_flag ) &&
            mb->sub_mb_type[ mbPartIdx ] != B_Direct_8x8 &&
            SubMbPredMode(mb, mbPartIdx) != Pred_L0 )
            mb->ref_idx_l1[ mbPartIdx ] = texpbits(bb, sh->num_ref_idx_l1_active_minus1);   /* ref_idx_l1 */
    for (mbPartIdx = 0; mbPartIdx < 4; mbPartIdx++ )
        if (mb->sub_mb_type[ mbPartIdx ] != B_Direct_8x8 && SubMbPredMode(mb, mbPartIdx) != Pred_L1 )
            for (subMbPartIdx = 0; subMbPartIdx < NumSubMbPart(mb, mbPartIdx); subMbPartIdx++)
                for (compIdx = 0; compIdx < 2; compIdx++)
                    mb->mvd[0][mbPartIdx][subMbPartIdx][compIdx] = sexpbits(bb);  /* mvd_l0 */
    for (mbPartIdx = 0; mbPartIdx < 4; mbPartIdx++)
        if (mb->sub_mb_type[ mbPartIdx ] != B_Direct_8x8 && SubMbPredMode(mb, mbPartIdx) != Pred_L0 )
            for (subMbPartIdx = 0; subMbPartIdx < NumSubMbPart(mb, mbPartIdx); subMbPartIdx++)
                for (compIdx = 0; compIdx < 2; compIdx++)
                    mb->mvd[1][mbPartIdx][subMbPartIdx][compIdx] = sexpbits(bb);  /* mvd_l1 */
}

/* determine nC for luma16x16 and luma4x4 blocks */
int getLuma4x4nC(struct rv264macro *mb, int luma4x4BlkIdx)
{
    const struct slice_header *sh = mb->sh;
    const struct rv264pic_parameter_set *pps = sh->pps;
    const struct rv264seq_parameter_set *sps = pps->sps;

    /* recover macroblock array address */
    struct rv264macro *macro = mb - mb->mb_addr;

    int mbAddrA;
    int luma4x4BlkIdxA;
    int mbAddrB;
    int luma4x4BlkIdxB;
    int nA, nB;

    getLuma4x4NeighboursAB(mb->mb_addr, luma4x4BlkIdx, sps->PicWidthInMbs, &mbAddrA, &luma4x4BlkIdxA, &mbAddrB, &luma4x4BlkIdxB);

    if (mbAddrA<0)
        nA = 0;
    //else if (pps->constrained_intra_pred_flag && macro_is_inter(&macro[mbAddrA]) && nal_unit_type>=2 && nal_unit_type<=4)
    else if (macro_is_skip(&macro[mbAddrA]))
        nA = 0;
    else if ((CodedBlockPatternLuma(&macro[mbAddrA]) & (1<<luma4x4BlkIdxA/4))==0)
        nA = 0;
    else if (macro[mbAddrA].mb_type==I_PCM)
        nA = 16;
    else
        nA = macro[mbAddrA].TotalCoeffLuma[luma4x4BlkIdxA];

    if (mbAddrB<0)
        nB = 0;
    //else if (pps->constrained_intra_pred_flag && macro_is_inter(&macro[mbAddrA]) && nal_unit_type>=2 && nal_unit_type<=4)
    else if (macro_is_skip(&macro[mbAddrB]))
        nB = 0;
    else if ((CodedBlockPatternLuma(&macro[mbAddrB]) & (1<<luma4x4BlkIdxB/4))==0)
        nB = 0;
    else if (macro[mbAddrB].mb_type==I_PCM)
        nB = 16;
    else
        nB = macro[mbAddrB].TotalCoeffLuma[luma4x4BlkIdxB];

    if (mbAddrA<0 || mbAddrB<0)
        return nA + nB;
    else
        return (nA + nB + 1) >> 1;
}

/* determine nC for luma8x8 blocks */
int getLuma8x8nC(struct rv264macro *mb, int luma8x8BlkIdx)
{
    const struct slice_header *sh = mb->sh;
    const struct rv264pic_parameter_set *pps = sh->pps;
    const struct rv264seq_parameter_set *sps = pps->sps;

    /* recover macroblock array address */
    struct rv264macro *macro = mb - mb->mb_addr;

    int mbAddrA;
    int luma8x8BlkIdxA;
    int mbAddrB;
    int luma8x8BlkIdxB;
    int nA, nB;

    getLuma8x8NeighboursAB(mb->mb_addr, luma8x8BlkIdx, sps->PicWidthInMbs, &mbAddrA, &luma8x8BlkIdxA, &mbAddrB, &luma8x8BlkIdxB);

    if (mbAddrA<0)
        nA = 0;
    //else if (pps->constrained_intra_pred_flag && macro_is_inter(macro[mbAddrA]->mb_type) && nal_unit_type>=2 && nal_unit_type<=4)
    else if (macro_is_skip(&macro[mbAddrA]))
        nA = 0;
    else if ((CodedBlockPatternLuma(&macro[mbAddrA]) & (1<<luma8x8BlkIdxA))==0)
        nA = 0;
    else if (macro[mbAddrA].mb_type==I_PCM)
        nA = 16;
    else
        nA = macro[mbAddrA].TotalCoeffLuma[luma8x8BlkIdxA]; // FIXME

    if (mbAddrB<0)
        nB = 0;
    //else if (pps->constrained_intra_pred_flag && macro_is_inter(macro[mbAddrA]->mb_type) && nal_unit_type>=2 && nal_unit_type<=4)
    else if (macro_is_skip(&macro[mbAddrB]))
        nB = 0;
    else if ((CodedBlockPatternLuma(&macro[mbAddrB]) & (1<<luma8x8BlkIdxA))==0)
        nB = 0;
    else if (macro[mbAddrB].mb_type==I_PCM)
        nB = 16;
    else
        nB = macro[mbAddrB].TotalCoeffLuma[luma8x8BlkIdxB]; // FIXME

    if (mbAddrA<0 || mbAddrB<0)
        return nA + nB;
    else
        return (nA + nB + 1) >> 1;
}

/* determine nC for chroma4x4 blocks */
int getChroma4x4nC(struct rv264macro *mb, int iCbCr, int chroma4x4BlkIdx)
{
    const struct slice_header *sh = mb->sh;
    const struct rv264pic_parameter_set *pps = sh->pps;
    const struct rv264seq_parameter_set *sps = pps->sps;

    /* recover macroblock array address */
    struct rv264macro *macro = mb - mb->mb_addr;

    int mbAddrA;
    int chroma4x4BlkIdxA;
    int mbAddrB;
    int chroma4x4BlkIdxB;
    int nA, nB;

    getChroma4x4NeighboursAB(mb->mb_addr, chroma4x4BlkIdx, sps->PicWidthInMbs, &mbAddrA, &chroma4x4BlkIdxA, &mbAddrB, &chroma4x4BlkIdxB);

    if (mbAddrA<0)
        nA = 0;
    //else if (pps->constrained_intra_pred_flag && macro_is_inter(&macro[mbAddrA]) && nal_unit_type>=2 && nal_unit_type<=4)
    else if (macro_is_skip(&macro[mbAddrA]))
        nA = 0;
    else if ((CodedBlockPatternChroma(&macro[mbAddrA]) & 2)==0)
        nA = 0;
    else if (macro[mbAddrA].mb_type==I_PCM)
        nA = 16;
    else
        nA = macro[mbAddrA].TotalCoeffChroma[iCbCr][chroma4x4BlkIdxA];

    if (mbAddrB<0)
        nB = 0;
    //else if (pps->constrained_intra_pred_flag && macro_is_inter(&macro[mbAddrA]) && nal_unit_type>=2 && nal_unit_type<=4)
    else if (macro_is_skip(&macro[mbAddrB]))
        nB = 0;
    else if ((CodedBlockPatternChroma(&macro[mbAddrB]) & 2)==0)
        nB = 0;
    else if (macro[mbAddrB].mb_type==I_PCM)
        nB = 16;
    else
        nB = macro[mbAddrB].TotalCoeffChroma[iCbCr][chroma4x4BlkIdxB];

    if (mbAddrA<0 || mbAddrB<0)
        return nA + nB;
    else
        return (nA + nB + 1) >> 1;
}

static residual_block_t block_type;

void parse_residual(struct bitbuf *bb, struct rv264macro *mb)
{
    const struct slice_header *sh = mb->sh;
    const struct rv264pic_parameter_set *pps = sh->pps;
    const struct rv264seq_parameter_set *sps = pps->sps;

    int (*residual_block)(struct bitbuf *, coeff_t *, int, int);
    int i;
    int i8x8;
    int i4x4;
    int iCbCr;

    if (!pps->entropy_coding_mode_flag)
        residual_block = parse_residual_block_cavlc;
    else
        residual_block = parse_residual_block_cabac;
    if (MbPartPredMode(mb, 0) == Intra_16x16) {
        int nC = getLuma4x4nC(mb, 0);
        block_type = Intra16x16DCLevel;
        (*residual_block)(bb, mb->Intra16x16DCLevel, 16, nC);
    }
    for (i8x8 = 0; i8x8 < 4; i8x8++) { /* each luma 8x8 block */
        if (!mb->transform_size_8x8_flag || !pps->entropy_coding_mode_flag)
            for (i4x4 = 0; i4x4 < 4; i4x4++) { /* each 4x4 sub-block of block */
                int luma4x4BlkIdx = 4*i8x8+i4x4;
                int nC = getLuma4x4nC(mb, luma4x4BlkIdx);
                if (CodedBlockPatternLuma(mb) & (1<<i8x8)) {
                    if (MbPartPredMode(mb, 0) == Intra_16x16) {
                        block_type = Intra16x16ACLevel;
                        mb->TotalCoeffLuma[luma4x4BlkIdx] = (*residual_block)(bb, mb->Intra16x16ACLevel[luma4x4BlkIdx], 15, nC);
                    } else {
                        block_type = LumaLevel;
                        mb->TotalCoeffLuma[luma4x4BlkIdx] = (*residual_block)(bb, mb->LumaLevel[luma4x4BlkIdx], 16, nC);
                    }
                } else if (MbPartPredMode(mb, 0) == Intra_16x16)
                    memset(mb->Intra16x16ACLevel[luma4x4BlkIdx], 0, 15*sizeof(coeff_t));
                else
                    memset(mb->LumaLevel[luma4x4BlkIdx], 0, 16*sizeof(coeff_t));
                if (!pps->entropy_coding_mode_flag && mb->transform_size_8x8_flag)
                    for (i = 0; i < 16; i++)
                        mb->LumaLevel8x8[i8x8][4*i + i4x4] = mb->LumaLevel[luma4x4BlkIdx][i];
            }
        else if (CodedBlockPatternLuma(mb) & (1<<i8x8)) {
            int nC = getLuma8x8nC(mb, i8x8);
            /* FIXME TotalCoeff = */ (*residual_block)(bb, mb->LumaLevel8x8[i8x8], 64, nC);
        } else
            memset(mb->LumaLevel8x8[i8x8], 0, 64*sizeof(coeff_t));
    }
    if (sps->chroma_format_idc != 0) {
        int NumC8x8 = 4 / ( sps->SubWidthC * sps->SubHeightC );
        for (iCbCr = 0; iCbCr < 2; iCbCr++)
            if (CodedBlockPatternChroma(mb) & 3) { /* chroma DC residual present */
                /* determine nC */
                int nC;
                switch (sps->chroma_format_idc) {
                    case 1 : nC = -1; break;
                    case 2 : nC = -2; break;
                    default: nC = 0;  break;
                }
                block_type = ChromaDCLevel;
                (*residual_block)(bb, mb->ChromaDCLevel[iCbCr], 4 * NumC8x8, nC);
            } else
                memset(mb->ChromaDCLevel[iCbCr], 0, 4*NumC8x8*sizeof(coeff_t));
        for (iCbCr = 0; iCbCr < 2; iCbCr++)
            for (i8x8 = 0; i8x8 < NumC8x8; i8x8++)
                for (i4x4 = 0; i4x4 < 4; i4x4++) {
                    if (CodedBlockPatternChroma(mb) & 2) { /* chroma AC residual present */
                        int chroma4x4BlkIdx = i8x8*4+i4x4;
                        int nC = getChroma4x4nC(mb, iCbCr, chroma4x4BlkIdx);
                        block_type = ChromaACLevel;
                        mb->TotalCoeffChroma[iCbCr][chroma4x4BlkIdx] = (*residual_block)(bb, mb->ChromaACLevel[iCbCr][i8x8*4+i4x4], 15, nC);
                    } else
                        memset(mb->ChromaACLevel[iCbCr][i8x8*4+i4x4], 0, 16*sizeof(coeff_t));
                }
    }
}

int parse_residual_block_cavlc(struct bitbuf *bb, coeff_t *coeffLevel, int maxNumCoeff, int nC)
{
    int i;

    coeff_t level[64];
    int8_t  run[64];

    memset(coeffLevel, 0, maxNumCoeff*sizeof(coeff_t));

    struct coeff_token_t coeff_token = parse_coeff_token(bb, nC);           /* coeff_token */
    /*if (block_type==LumaLevel)
        printf("Luma # c & tr.1s vlc=%d #c=%d #t1=%d %29s (%3d) \n", nC<=0?nC:nC<2?0:nC<4?1:nC<8?2:3, TotalCoeff(coeff_token), TrailingOnes(coeff_token), binstring(tracebits(bb), tracelen(bb)), tracebits(bb));
    else if (block_type==Intra16x16DCLevel)
        printf("Lum16DC # c & tr.1s vlc=%d #c=%d #t1=%d %25s (%3d) \n", nC<=0?nC:nC<2?0:nC<4?1:nC<8?2:3, TotalCoeff(coeff_token), TrailingOnes(coeff_token), binstring(tracebits(bb), tracelen(bb)), tracebits(bb));
    else if (block_type==Intra16x16ACLevel)
        printf("Lum16AC # c & tr.1s vlc=%d #c=%d #t1=%d %25s (%3d) \n", nC<=0?nC:nC<2?0:nC<4?1:nC<8?2:3, TotalCoeff(coeff_token), TrailingOnes(coeff_token), binstring(tracebits(bb), tracelen(bb)), tracebits(bb));
    else if (block_type==ChromaACLevel)
        printf("ChrAC # c & tr.1s vlc=%d #c=%d #t1=%d %28s (%3d) \n", nC<=0?nC:nC<2?0:nC<4?1:nC<8?2:3, TotalCoeff(coeff_token), TrailingOnes(coeff_token), binstring(tracebits(bb), tracelen(bb)), tracebits(bb));
    else if (block_type==ChromaDCLevel)
        printf("ChrDC # c & tr.1s  #c=%d #t1=%d %33s (%3d)\n", TotalCoeff(coeff_token), TrailingOnes(coeff_token), binstring(tracebits(bb), tracelen(bb)), tracebits(bb));*/
    if (TotalCoeff(coeff_token) > 0) {
        int suffixLength;
        if (TotalCoeff(coeff_token) > 10 && TrailingOnes(coeff_token) < 3)
            suffixLength = 1;
        else
            suffixLength = 0;
        for (i = 0; i < TotalCoeff(coeff_token); i++)
            if (i < TrailingOnes(coeff_token)) {
                int trailing_ones_sign_flag = getbit(bb);                   /* trailing_ones_sign_flag */
                level[i] = 1 - 2 * trailing_ones_sign_flag;
            } else {
                int level_prefix = parse_level_prefix(bb);                  /* level_prefix */
                int levelCode = mmin(15, level_prefix) << suffixLength;
                if (suffixLength > 0 || level_prefix >= 14) {
                    int levelSuffixSize = suffixLength;
                    if (level_prefix==14 && suffixLength==0)
                        levelSuffixSize = 4;
                    if (level_prefix>=15)
                        levelSuffixSize = level_prefix-3;
                    int level_suffix = getbits(bb, levelSuffixSize);        /* level_suffix */
                    levelCode += level_suffix;
                }
                if (level_prefix >= 15 && suffixLength == 0)
                    levelCode += 15;
                if (level_prefix >= 16)
                    levelCode += ( 1 << (level_prefix-3) ) - 4096;
                if (i == TrailingOnes(coeff_token) && TrailingOnes(coeff_token) < 3)
                    levelCode += 2;
                if (levelCode%2 == 0)
                    level[i] = (levelCode+2) >> 1;
                else
                    level[i] = (-levelCode-1) >> 1;
                if (suffixLength == 0)
                    suffixLength = 1;
                if (abs(level[i]) > ( 3 << (suffixLength-1) ) && suffixLength < 6)
                    suffixLength++;
            }
        int zerosLeft;
        if (TotalCoeff(coeff_token) < maxNumCoeff) {
            int total_zeros = parse_total_zeros(bb, maxNumCoeff, TotalCoeff(coeff_token));/* total_zeros */
            zerosLeft = total_zeros;
        } else
            zerosLeft = 0;
        for (i = 0; i < TotalCoeff(coeff_token)-1; i++) {
            if (zerosLeft > 0) {
                int run_before = parse_run_before(bb, zerosLeft);           /* run_before */
                run[i] = run_before;
            } else
                run[i] = 0;
            zerosLeft = zerosLeft - run[i];
        }
        run[TotalCoeff(coeff_token)-1] = zerosLeft;
        int coeffNum = -1;
        for (i = TotalCoeff(coeff_token)-1; i >= 0; i--) {
            coeffNum += run[i] + 1;
            coeffLevel[coeffNum] = level[i];
        }
    }
    return TotalCoeff(coeff_token);
}

int parse_residual_block_cabac(struct bitbuf *bb, coeff_t *coeffLevel, int maxNumCoeff, int nC)
{
    rvexit("%s: todo: cabac", bb->filename);
    return 0;
}

/* find a single parameter set that might decode a sequence,
 * if multiple picture parameter sets are required, these
 * are assumed to become available later in the nal stream */
int find_complete_parameter_set(struct rv264sequence *seq)
{
    int i, j;

    if (seq==NULL)
        return -1;
    for (i=0; i<256; i++) {
        if (!seq->pps[i])
            continue;
        if (!seq->pps[i]->occupied)
            continue;
        struct rv264pic_parameter_set *pps = seq->pps[i];
        for (j=0; j<16; j++)
            if (seq->sps[j])
                if (seq->sps[j]->seq_parameter_set_id==pps->seq_parameter_set_id) {
                    /* initialise seq with a possible parameter set,
                     * this could easily be overridden by the application */
                    seq->active_sps = j;
                    seq->active_pps = i;
                    return j;
                }
    }
    return -1;
}

struct rv264sequence *parse_264_params(struct bitbuf *bb, int verbose)
{
    const char *where = "h.264 nal unit stream";

    /* allocate h.264 sequence struct */
    size_t size = sizeof(struct rv264sequence);
    struct rv264sequence *seq = (struct rv264sequence *)rvalloc(NULL, size, 1);

    /* loop over nal units until sufficient parameter nals have been parsed */
    int firstnal = 1; /* first nal in access unit */
    do {

        /* search for the next nal unit */
        int zerobyte, pos = numbits(bb);
        int nal_unit_type = find_next_nal_unit(bb, &zerobyte);
        if (verbose>=1 && numbits(bb)-pos>7+zerobyte*8)
            rvmessage("%s: %jd bits of leading garbage before nal unit", where, numbits(bb)-pos);
        if (nal_unit_type<0) {
            rvfree(seq);
            break;
        }

        /* parse nal unit header */
        if (zerobyte)
            flushbits(bb, 8);           /* zero_byte */
        flushbits(bb, 24);              /* start_code_prefix_one_3bytes */
        int nal_ref_idc;
        int ret = parse_nal_unit_header(bb, &nal_unit_type, &nal_ref_idc);
        if (ret<0)
            rvexit("%s: failed to parse nal unit header in file \"%s\"", where, bb->filename);

        /* prepare to parse rbsp */
        struct bitbuf *rbsp = extract_rbsp(bb, verbose);
        if (verbose>=1)
            rvmessage("%s: nal: %s start: length=%d ref_idc=%d unit_type=%s", where, nal_start[zerobyte], numbitsleft(rbsp)/8+1, nal_ref_idc, nal_unit_type_code(nal_unit_type));

        /* select nal unit type */
        switch (nal_unit_type) {

            case NAL_SEI: /* supplemental enhancement information */
                /* check for long start code (Annex B.1.2) */
                if (verbose>=0 && firstnal && zerobyte==0)
                    rvmessage("%s: sei nal unit which is first in access unit should have a long start code", where);

                /* parse supplemental enhancement information */
                parse_sei(rbsp, seq, verbose);
                break;

            case NAL_SPS: /* sequence parameter set */
            {
                /* check for long start code (Annex B.1.2) */
                if (verbose>=0 && zerobyte==0)
                    rvmessage("%s: sps nal unit should have a long start code", where);

                /* parse sequence parameter set */
                struct rv264seq_parameter_set *sps = parse_sequence_param(rbsp);
                if (sps==NULL)
                    rvexit("%s: failed to parse sequence parameter set header in file \"%s\"", where, bb->filename);
                if (verbose>=2)
                    rvmessage("%s: seq_parameter_set_id=%d", where, sps->seq_parameter_set_id);

                /* store sequence parameter set */
                rvfree(seq->sps[sps->seq_parameter_set_id]);
                seq->sps[sps->seq_parameter_set_id] = sps;

                break;
            }

            case NAL_PPS: /* picture parameter set */
            {
                /* check for long start code (Annex B.1.2) */
                if (verbose>=0 && zerobyte==0)
                    rvmessage("%s: pps nal unit should have a long start code", where);

                /* parse picture parameter set */
                struct rv264pic_parameter_set *pps = parse_picture_param(rbsp, seq->sps, verbose);
                if (pps==NULL) /* TODO handle this more gracefully */
                    rvexit("%s: failed to parse picture parameter set header in file \"%s\"", where, bb->filename);
                if (verbose>=2)
                    rvmessage("%s: pic_parameter_set_id=%d seq_parameter_set_id=%d", where, pps->pic_parameter_set_id, pps->seq_parameter_set_id);

                /* store sequence parameter set */
                rvfree(seq->pps[pps->pic_parameter_set_id]);
                seq->pps[pps->pic_parameter_set_id] = pps;

                break;
            }

            case NAL_EOQ: /* end of sequence */
                if (verbose>=3)
                    rvmessage("%s: found end of sequence nal", where);
                rvfree(seq);
                return NULL;

            case NAL_EOS: /* end of stream */
                if (verbose>=3)
                    rvmessage("%s: found end of stream nal", where);
                rvfree(seq);
                return NULL;

            default:
                //rvmessage("%s: skipping nal_unit_type=%d", where, nal_unit_type);
                break;

        }
        firstnal = 0;

        /* tidy up */
        free_rbsp(rbsp);

    } while (find_complete_parameter_set(seq)<0);

    /* report sequence parameters
    if (verbose>=1) {
        char *p, *l;
        describe_profile(sequence->profile_and_level, &p, &l);
        rvmessage("%s: %s@%s %dx%d@%.2ffps", get_basename(bb->filename), p, l,
                  sequence->horizontal_size_value, sequence->vertical_size_value,
                  sequence->frame_rate);
    }*/

    /* sufficient information has now been obtained from sequence to parse first picture */
    return seq;
}

int find_complete_picture(struct rv264picture *pic)
{
    int i;
    if (pic==NULL || pic->num_slices==0)
        return -1;
    for (i=0; i<pic->sh[0]->PicSizeInMbs; i++) {
        if (!pic->macro[i].occupied)
            return -1;
    }
    return i;
}

struct rv264picture *parse_264_picture(struct bitbuf *bb, struct rv264sequence *seq, int verbose)
{
    const char *where = "h.264 nal unit stream";

    /* allocate h.264 picture */
    struct rv264picture *pic = alloc_264_picture();

    /* loop over nal units until a whole picture has been parsed */
    int last_nal = 0;
    do {

        /* search for the next nal unit */
        int zerobyte, pos = numbits(bb);
        int nal_unit_type = find_next_nal_unit(bb, &zerobyte);
        if (verbose>=1 && numbits(bb)-pos>7+zerobyte*8)
            rvmessage("%s: %jd bits of leading garbage before nal unit", where, numbits(bb)-pos);
        if (nal_unit_type<0) {
            if (pic->num_slices==0) {
                rvfree(pic);
                pic = NULL;
            }
            break;
        }

        /* parse the next nal unit */
        if (zerobyte)
            flushbits(bb, 8);           /* zero_byte */
        flushbits(bb, 24);              /* start_code_prefix_one_3bytes */
        int nal_ref_idc;
        int ret = parse_nal_unit_header(bb, &nal_unit_type, &nal_ref_idc);
        if (ret<0)
            rvexit("%s: failed to parse nal unit header in file \"%s\"", where, bb->filename);

        /* prepare to parse rbsp */
        struct bitbuf *rbsp = extract_rbsp(bb, verbose);
        if (verbose>=1)
            rvmessage("%s: nal: %s start: length=%d ref_idc=%d unit_type=%s", where, nal_start[zerobyte], numbitsleft(rbsp)/8+1, nal_ref_idc, nal_unit_type_code(nal_unit_type));

        /* select nal unit type */
        switch (nal_unit_type) {

            case NAL_CODED_SLICE_NON_IDR: /* coded slice of a non-IDR picture */
            case NAL_CODED_SLICE_IDR: /* coded slice of an IDR picture */
            {
                /* parse slice header */
                struct slice_header *sh = parse_slice_header(rbsp, nal_unit_type, nal_ref_idc, seq->pps, verbose);
                if (sh==NULL)
                    rvmessage("%s: failed to parse slice header", where);
                if (verbose>=2)
                    rvmessage("%s: slice header %d: first_mb_in_slice=%d", where, pic->num_slices, sh->first_mb_in_slice);

                /* TODO check slice belongs to current picture */
                /* TODO check slice header matches current slice */

                /* set active pps */
                pic->pps = seq->pps[sh->pic_parameter_set_id];

                /* add slice header to picture */
                realloc_264_slice_array(pic, pic->num_slices+1);
                pic->sh[pic->num_slices] = sh;

                /* copy picture header data */
                if (pic->num_slices==0) {
                    pic->nal_ref_idc = nal_ref_idc;
                    pic->nal_unit_type = nal_unit_type;
                    pic->PicOrderCnt = sh->PicOrderCnt;
                    pic->frame_num = sh->frame_num;
                }

                /* allocate array of macroblocks */
                pic->macro = (struct rv264macro *)rvalloc(pic->macro, sh->PicSizeInMbs*sizeof(struct rv264macro), 1);

                int num_macros = parse_slice_data(rbsp, sh, pic->macro);
                if (num_macros<=0)
                    rvmessage("failed to parse any macroblocks in slice %d", pic->num_slices);
                if (verbose>=1)
                    rvmessage("parsed %d macroblocks in slice %d", num_macros, pic->num_slices);

                parse_slice_rbsp_trailing_bits(rbsp, sh->pps->entropy_coding_mode_flag);
                pic->num_slices++;
                break;
            }

            case NAL_SPS: /* sequence parameter set */
            {
                /* parse sequence parameter set */
                struct rv264seq_parameter_set *sps = parse_sequence_param(rbsp);
                if (sps==NULL)
                    rvexit("%s: failed to parse sequence parameter set header in file \"%s\"", where, bb->filename);
                if (verbose>=2)
                    rvmessage("%s: seq_parameter_set_id=%d", where, sps->seq_parameter_set_id);

                /* store sequence parameter set */
                rvfree(seq->sps[sps->seq_parameter_set_id]);
                seq->sps[sps->seq_parameter_set_id] = sps;

                break;
            }

            case NAL_PPS: /* picture parameter set */
            {
                /* parse picture parameter set */
                struct rv264pic_parameter_set *pps = parse_picture_param(rbsp, seq->sps, verbose);
                if (pps==NULL) /* TODO handle this more gracefully */
                    rvexit("%s: failed to parse picture parameter set header in file \"%s\"", where, bb->filename);
                if (verbose>=2)
                    rvmessage("%s: pic_parameter_set_id=%d seq_parameter_set_id=%d", where, pps->pic_parameter_set_id, pps->seq_parameter_set_id);

                /* store sequence parameter set */
                rvfree(seq->pps[pps->pic_parameter_set_id]);
                seq->pps[pps->pic_parameter_set_id] = pps;

                break;
            }

            case NAL_AUD: /* access unit delimiter */
                pic->aud_present = 1;
                pic->primary_pic_type = getbits(rbsp, 3);
                break;

            case NAL_EOQ: /* end of sequence */
                if (verbose>=3)
                    rvmessage("%s: found end of sequence nal", where);
                last_nal = 1;
                break;

            case NAL_EOS: /* end of stream */
                if (verbose>=3)
                    rvmessage("%s: found end of stream nal", where);
                last_nal = 1;
                break;

            case NAL_FILLER:
                break;

            default:
                if (verbose>=1)
                    rvmessage("%s: skipping nal_unit_type=%s", where, nal_unit_type_code(nal_unit_type));
                break;
        }

        /* tidy up */
        free_rbsp(rbsp);

    } while (find_complete_picture(pic)<0 || last_nal);

    return pic;
}

struct rv264picture *parse_264_picture_headers(struct bitbuf *bb, struct rv264sequence *seq, int verbose)
{
    const char *where = "h.264 nal unit stream";

    /* allocate h.264 picture */
    struct rv264picture *pic = alloc_264_picture();

    /* loop over nal units until a whole picture has been parsed */
    bool slice_data_seen = false, eos = false;
    do {

        int nal_unit_type, nal_ref_idc;
        struct bitbuf *rbsp;
        if (seq->park) {
            /* nal unit was parked from the last picture */
            nal_unit_type = seq->nal_unit_type;
            nal_ref_idc   = seq->nal_ref_idc;
            rbsp = seq->park;
            seq->park = NULL;
        } else {
            /* search for the next nal unit */
            int zerobyte, pos = numbits(bb);
            nal_unit_type = find_next_nal_unit(bb, &zerobyte);
            if (verbose>=1 && numbits(bb)-pos>7+zerobyte*8)
                rvmessage("%s: %jd bits of leading garbage before nal unit", where, numbits(bb)-pos);
            if (nal_unit_type<0) {
                if (pic->num_slices==0) {
                    rvfree(pic);
                    pic = NULL;
                }
                break;
            }

            /* parse the next nal unit */
            if (zerobyte)
                flushbits(bb, 8);           /* zero_byte */
            flushbits(bb, 24);              /* start_code_prefix_one_3bytes */
            int ret = parse_nal_unit_header(bb, &nal_unit_type, &nal_ref_idc);
            if (ret<0)
                rvexit("%s: failed to parse nal unit header in file \"%s\"", where, bb->filename);

            /* prepare to parse rbsp */
            rbsp = extract_rbsp(bb, verbose);
            if (verbose>=1)
                rvmessage("%s: nal: %s start: length=%d ref_idc=%d unit_type=%s", where, nal_start[zerobyte], numbitsleft(rbsp)/8+1, nal_ref_idc, nal_unit_type_code(nal_unit_type));
        }

        /* select nal unit type */
        switch (nal_unit_type) {

            case NAL_CODED_SLICE_NON_IDR:   /* coded slice of a non-IDR picture */
            case NAL_CODED_SLICE_IDR:       /* coded slice of an IDR picture */
            {
                /* parse slice header */
                struct slice_header *sh = parse_slice_header(rbsp, nal_unit_type, nal_ref_idc, seq->pps, verbose);
                if (sh==NULL) {
                    rvmessage("%s: failed to parse slice header", where);
                    eos = true;
                    break;
                }
                if (verbose>=2)
                    rvmessage("%s: slice header %d: first_mb_in_slice=%d slice_type=%s frame_num=%d", where, pic->num_slices, sh->first_mb_in_slice, slice_type_name[sh->slice_type], sh->frame_num);

                if (slice_data_seen) {
                    /* check slice belongs to current picture */
                    if (sh->frame_num != pic->frame_num ||
                        sh->pic_parameter_set_id != pic->pic_parameter_set_id ||
                        sh->field_pic_flag != pic->field_pic_flag ||
                        (sh->field_pic_flag && pic->field_pic_flag && sh->bottom_field_flag!=pic->bottom_field_flag) ||
                        (nal_ref_idc != pic->nal_ref_idc && (nal_ref_idc==0 || pic->nal_ref_idc==0))
                        /* TODO there are more conditions */
                    ) {
                        /* new access unit: park the current nal unit */
                        rewindbits(rbsp);
                        seq->nal_unit_type = nal_unit_type;
                        seq->nal_ref_idc = nal_ref_idc;
                        seq->park = rbsp;
                        return pic;
                    }

                    /* TODO check slice header matches current slice */
                }

                /* set active pps */
                pic->pps = seq->pps[sh->pic_parameter_set_id];

                /* add slice header to picture */
                realloc_264_slice_array(pic, pic->num_slices+1);
                pic->sh[pic->num_slices] = sh;

                /* copy picture header data */
                if (pic->num_slices==0) {
                    pic->nal_ref_idc = nal_ref_idc;
                    pic->nal_unit_type = nal_unit_type;
                    pic->PicOrderCnt = sh->PicOrderCnt;
                    pic->pic_parameter_set_id = sh->pic_parameter_set_id;
                    pic->frame_num = sh->frame_num;
                    pic->field_pic_flag = sh->field_pic_flag;
                    pic->bottom_field_flag = sh->bottom_field_flag;
                    pic->idr_pic_id = sh->idr_pic_id;
                    pic->pic_order_cnt_lsb = sh->pic_order_cnt_lsb;
                }

                slice_data_seen = true;

                pic->num_slices++;
                break;
            }

            case NAL_SEI: /* supplemental enhancement information */
                parse_sei(rbsp, seq, verbose);
                break;

            case NAL_SPS: /* sequence parameter set */
            {
                if (slice_data_seen) {
                    /* new access unit: park the current nal unit */
                    seq->nal_unit_type = nal_unit_type;
                    seq->nal_ref_idc = nal_ref_idc;
                    seq->park = rbsp;
                    return pic;
                }

                /* parse sequence parameter set */
                struct rv264seq_parameter_set *sps = parse_sequence_param(rbsp);
                if (sps==NULL)
                    rvexit("%s: failed to parse sequence parameter set header in file \"%s\"", where, bb->filename);
                if (verbose>=2)
                    rvmessage("%s: seq_parameter_set_id=%d", where, sps->seq_parameter_set_id);

                /* store sequence parameter set */
                if (seq->sps[sps->seq_parameter_set_id]==NULL)
                    seq->sps[sps->seq_parameter_set_id] = sps;
                else {
                    if (!sps_is_equal(sps, seq->sps[sps->seq_parameter_set_id])) {
                        if (verbose>=0)
                            rvmessage("%s: sequence parameter set changed during sequence", bb->filename);
                        /* need to copy here as replacing will invalidate the *sps pointer in existing pps's */
                        memcpy(seq->sps[sps->seq_parameter_set_id], sps, sizeof(struct rv264seq_parameter_set));
                        rvfree(sps);
                    }
                }

                break;
            }

            case NAL_PPS: /* picture parameter set */
            {
                if (slice_data_seen) {
                    /* new access unit: park the current nal unit */
                    seq->nal_unit_type = nal_unit_type;
                    seq->nal_ref_idc = nal_ref_idc;
                    seq->park = rbsp;
                    return pic;
                }

                /* parse picture parameter set */
                struct rv264pic_parameter_set *pps = parse_picture_param(rbsp, seq->sps, verbose);
                if (pps==NULL) /* TODO handle this more gracefully */
                    rvexit("%s: failed to parse picture parameter set header in file \"%s\"", where, bb->filename);
                if (verbose>=2)
                    rvmessage("%s: pic_parameter_set_id=%d seq_parameter_set_id=%d", where, pps->pic_parameter_set_id, pps->seq_parameter_set_id);

                /* store sequence parameter set */
                if (seq->pps[pps->pic_parameter_set_id]==NULL)
                    seq->pps[pps->pic_parameter_set_id] = pps;
                else {
                    if (!pps_is_equal(pps, seq->pps[pps->pic_parameter_set_id])) {
                        if (verbose>=0)
                            rvmessage("%s: picture parameter set changed during sequence", bb->filename);
                        /* need to copy here as replacing will invalidate the *sps pointer in existing pps's */
                        memcpy(seq->pps[pps->pic_parameter_set_id], pps, sizeof(struct rv264pic_parameter_set));
                        rvfree(pps);
                    }
                }

                break;
            }

            case NAL_AUD: /* access unit delimiter */
                pic->aud_present = 1;
                pic->primary_pic_type = getbits(rbsp, 3);
                if (slice_data_seen) {
                    /* new access unit: park the current nal unit */
                    seq->nal_unit_type = nal_unit_type;
                    seq->nal_ref_idc = nal_ref_idc;
                    seq->park = rbsp;
                    return pic;
                }
                break;

            case NAL_EOQ: /* end of sequence */
                if (verbose>=3)
                    rvmessage("%s: found end of sequence nal", where);
                eos = true;
                break;

            case NAL_EOS: /* end of stream */
                if (verbose>=3)
                    rvmessage("%s: found end of stream nal", where);
                eos = true;
                break;

            case NAL_FILLER:
                /* silently skip */
                break;

            default:
                if (verbose>=1)
                    rvmessage("%s: skipping nal_unit_type=%s", where, nal_unit_type_code(nal_unit_type));
                break;
        }

        /* tidy up */
        free_rbsp(rbsp);

    } while (!eos);

    rvfree(pic);
    return NULL;
}

/*
 * h.264 macroblock data.
 */

int map_coded_block_pattern(int chroma_format_idc, mb_type_t mb_type, int codenum)
{
    // gives coded_block_pattern value from codeword number.
    const unsigned char map_cbp[2][48][2] = {
        {   { 15, 0 }, { 0, 1 }, { 7, 2 }, { 11, 4 },
            { 13, 8 }, { 14, 3 }, { 3, 5 }, { 5, 10 },
            { 10, 12 }, { 12, 15 }, { 1, 7 }, { 2, 11 },
            { 4, 13 }, { 8, 14 }, { 6, 6 }, { 9, 9 },
            { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
            { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
            { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
            { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
            { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
            { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 },
            { 0, 0 }, { 0, 0 }
        },
        {   { 47, 0 }, { 31, 16 }, { 15, 1 }, { 0, 2 },
            { 23, 4 }, { 27, 8 }, { 29, 32 }, { 30, 3 },
            { 7, 5 }, { 11, 10 }, { 13, 12 }, { 14, 15 },
            { 39, 47 }, { 43, 7 }, { 45, 11 }, { 46, 13 },
            { 16, 14 }, { 3, 6 }, { 5, 9 }, { 10, 31 },
            { 12, 35 }, { 19, 37 }, { 21, 42 }, { 26, 44 },
            { 28, 33 }, { 35, 34 }, { 37, 36 }, { 42, 40 },
            { 44, 39 }, { 1, 43 }, { 2, 45 }, { 4, 46 },
            { 8, 17 }, { 17, 18 }, { 18, 20 }, { 20, 24 },
            { 24, 19 }, { 6, 21 }, { 9, 26 }, { 22, 28 },
            { 25, 23 }, { 32, 27 }, { 33, 29 }, { 34, 30 },
            { 36, 22 }, { 40, 25 }, { 38, 38 }, { 41, 41 }
        }
    };

    return (int) map_cbp[chroma_format_idc? 1 : 0][codenum][mb_type==I_NxN? 0 : 1];
}

/*
 * h.264 prediction functions.
 */


void predict_264_macroblock_luma4x4(struct rv264macro *macro, const sample_t *luma, sample_t predL[4][4])
{
}

/* 8.3.4 intra prediction process for chroma samples */
void predict_264_chroma8x8(struct rv264macro *macro, const sample_t *cSc, sample_t predC[8][8])
{
    int x, y;
    struct rv264macro *macros = macro - macro->mb_addr;

    struct slice_header *sh = macro->sh;
    struct rv264pic_parameter_set *pps = sh->pps;
    struct rv264seq_parameter_set *sps = pps->sps;

    // TODO ChromaArrayType must be 1 to call this function.

    if (sps->ChromaArrayType==3) {
    } else {
        /* create neighbouring sample arrays which can be negatively addressed */
        sampleandavail_t pl0[9], pt0[9];
        sampleandavail_t *pl = &pl0[1];
        sampleandavail_t *pt = &pt0[1];

        /* create left column sample array */
        for (x=-1, y=-1; y<8; y++) {
            int xN = x;
            int yN = y;

            int mbAddrN;
            int xW, yW;
            derive_neighbouring_location(macro, xN, yN, CHROMA, &mbAddrN, &xW, &yW);

            if (mbAddrN<0)
                pl[y] = NOT_AVAILABLE;
            else if (macro_is_inter(&macros[mbAddrN]) && macro->sh->pps->constrained_intra_pred_flag)
                pl[y] = NOT_AVAILABLE;
            else {
                int xL, yL;
                int xM, yM;

                inverse_scan_macroblock(&macros[mbAddrN], &xL, &yL);
                xM = (xL>>4)*sps->MbWidthC;
                yM = (yL>>4)*sps->MbHeightC + (yL%2);

                if (macro->sh->MbaffFrameFlag && macro->mb_field_decoding_flag)
                    pl[y] = cSc[xM+xW + (yM+2*yW)*macro->sh->pps->sps->PicWidthInSamplesc];
                else
                    pl[y] = cSc[xM+xW + (yM+yW)*macro->sh->pps->sps->PicWidthInSamplesc];
            }
        }
        /* create top row sample array */
        for (x=-1, y=-1; x<8; x++) {
            int xN = x;
            int yN = y;

            int mbAddrN;
            int xW, yW;
            derive_neighbouring_location(macro, xN, yN, CHROMA, &mbAddrN, &xW, &yW);

            if (mbAddrN<0)
                pt[x] = NOT_AVAILABLE;
            else if (macro_is_inter(&macros[mbAddrN]) && macro->sh->pps->constrained_intra_pred_flag)
                pt[x] = NOT_AVAILABLE;
            else {
                int xL, yL;
                int xM, yM;

                inverse_scan_macroblock(&macros[mbAddrN], &xL, &yL);
                xM = (xL>>4)*sps->MbWidthC;
                yM = (yL>>4)*sps->MbHeightC + (yL%2);

                if (macro->sh->MbaffFrameFlag && macro->mb_field_decoding_flag)
                    pt[x] = cSc[xM+xW + (yM+2*yW)*macro->sh->pps->sps->PicWidthInSamplesc];
                else
                    pt[x] = cSc[xM+xW + (yM+yW)*macro->sh->pps->sps->PicWidthInSamplesc];
            }
        }

        /* select intra chroma prediction mode */
        switch (macro->intra_chroma_pred_mode) {
            case Intra_Chroma_DC:
            {
                int chroma4x4BlkIdx;
                for (chroma4x4BlkIdx=0; chroma4x4BlkIdx<(1<<(sps->ChromaArrayType+1)); chroma4x4BlkIdx++) {
                    int xO = InverseRasterScanX(chroma4x4BlkIdx, 4, 4, 8);
                    int yO = InverseRasterScanY(chroma4x4BlkIdx, 4, 4, 8);

                    int top_available = 1;
                    int left_available = 1;
                    int sum_top = 0;
                    int sum_left = 0;

                    /* sum left and top predictors */
                    for (x=0+xO; x<4+xO; x++) {
                        if (!AVAILABLE(pt[x]))
                            top_available = 0;
                        sum_top += pt[x];
                    }
                    for (y=0+yO; y<4+yO; y++) {
                        if (!AVAILABLE(pl[y]))
                            left_available = 0;
                        sum_left += pl[y];
                    }

                    /* choose predictor */
                    sample_t dc_pred;
                    if ((xO==0 && yO==0) || (xO>0 && yO>0)) {
                        if (top_available && left_available)
                            dc_pred = (sample_t)((sum_top + sum_left + 4) >> 3);
                        else if (left_available)
                            dc_pred = (sample_t)((sum_left + 2) >> 2);
                        else if (top_available)
                            dc_pred = (sample_t)((sum_top + 2) >> 2);
                        else
                            dc_pred = 1<<(sps->BitDepthC-1);
                    } else if (xO>0 && yO==0) {
                        if (top_available)
                            dc_pred = (sample_t)((sum_top + 2) >> 2);
                        else if (left_available)
                            dc_pred = (sample_t)((sum_left + 2) >> 2);
                        else
                            dc_pred = 1<<(sps->BitDepthC-1);
                    } else {
                        if (left_available)
                            dc_pred = (sample_t)((sum_left + 2) >> 2);
                        else if (top_available)
                            dc_pred = (sample_t)((sum_top + 2) >> 2);
                        else
                            dc_pred = 1<<(sps->BitDepthC-1);
                    }

                    for (y=0+yO; y<4+yO; y++)
                        for (x=0+xO; x<4+xO; x++)
                            predC[y][x] = dc_pred;
                }
                break;
            }
            case Intra_Chroma_Horizontal:
                for (y=0; y<8; y++)
                    for (x=0; x<8; x++)
                        predC[y][x] = pl[y];
                break;
            case Intra_Chroma_Vertical:
                for (y=0; y<8; y++)
                    for (x=0; x<8; x++)
                        predC[y][x] = pt[x];
                break;
            case Intra_Chroma_Plane:
            {
                const int xCF = sps->ChromaArrayType==3? 4 : 0;
                const int yCF = sps->ChromaArrayType!=1? 4 : 0;

                int H = 0;
                int V = 0;

                for (x=0; x<=3+xCF; x++)
                    H += (x+1) * (pt[4+xCF+x]-pt[2+xCF-x]);
                for (y=0; y<=3+yCF; y++)
                    V += (y+1) * (pl[4+yCF+y]-pl[2+yCF-y]);

                int a = 16 * (pl[sps->MbHeightC-1] + pt[sps->MbWidthC-1]);
                int b = ((34-29*(sps->ChromaArrayType==3))*H + 32) >> 6;
                int c = ((34-29*(sps->ChromaArrayType!=1))*V + 32) >> 6;

                for (y=0; y<8; y++)
                    for (x=0; x<8; x++)
                        predC[y][x] = Clip1c((a + b*(x-3-xCF) + c*(y-3-yCF) + 16) >> 5);
                break;
            }
        }
    }
}

/* 8.3.3 intra_16x16 prediction process for luma samples */
void predict_264_intra16x16(struct rv264macro *macro, const sample_t *cSl, sample_t predL[16][16])
{
    int x, y;
    struct rv264macro *macros = macro - macro->mb_addr;

    struct slice_header *sh = macro->sh;
    struct rv264pic_parameter_set *pps = sh->pps;
    struct rv264seq_parameter_set *sps = pps->sps;

    /* create neighbouring sample arrays which can be negatively addressed */
    sampleandavail_t pl0[17], pt0[17];
    sampleandavail_t *pl = &pl0[1];
    sampleandavail_t *pt = &pt0[1];

    /* create left column sample array */
    for (x=-1, y=-1; y<16; y++) {
        int xN = x;
        int yN = y;

        int mbAddrN;
        int xW, yW;
        derive_neighbouring_location(macro, xN, yN, LUMA, &mbAddrN, &xW, &yW);

        if (mbAddrN<0)
            pl[y] = NOT_AVAILABLE;
        else if (macro_is_inter(&macros[mbAddrN]) && macro->sh->pps->constrained_intra_pred_flag)
            pl[y] = NOT_AVAILABLE;
        else {
            int xM, yM;

            inverse_scan_macroblock(&macros[mbAddrN], &xM, &yM);

            if (macro->sh->MbaffFrameFlag && macro->mb_field_decoding_flag)
                pl[y] = cSl[xM+xW + (yM+2*yW)*macro->sh->pps->sps->PicWidthInSamplesl];
            else
                pl[y] = cSl[xM+xW + (yM+yW)*macro->sh->pps->sps->PicWidthInSamplesl];
        }
    }
    /* create top row sample array */
    for (x=-1, y=-1; x<16; x++) {
        int xN = x;
        int yN = y;

        int mbAddrN;
        int xW, yW;
        derive_neighbouring_location(macro, xN, yN, LUMA, &mbAddrN, &xW, &yW);

        if (mbAddrN<0)
            pt[x] = NOT_AVAILABLE;
        else if (macro_is_inter(&macros[mbAddrN]) && macro->sh->pps->constrained_intra_pred_flag)
            pt[x] = NOT_AVAILABLE;
        else {
            int xM, yM;

            inverse_scan_macroblock(&macros[mbAddrN], &xM, &yM);

            if (macro->sh->MbaffFrameFlag && macro->mb_field_decoding_flag)
                pt[x] = cSl[xM+xW + (yM+2*yW)*macro->sh->pps->sps->PicWidthInSamplesl];
            else
                pt[x] = cSl[xM+xW + (yM+yW)*macro->sh->pps->sps->PicWidthInSamplesl];
        }
    }

    /* select intra 16x16 prediction mode */
    switch (Intra16x16PredMode(macro)) {
        case Intra_16x16_Vertical:
            for (y=0; y<16; y++)
                for (x=0; x<16; x++)
                    predL[y][x] = pt[x];
            break;
        case Intra_16x16_Horizontal:
            for (y=0; y<16; y++)
                for (x=0; x<16; x++)
                    predL[y][x] = pl[y];
            break;
        case Intra_16x16_DC:
        {
            int top_available = 1;
            int left_available = 1;
            int sum_top = 0;
            int sum_left = 0;

            /* sum left and top predictors */
            for (x=0; x<16; x++) {
                if (!AVAILABLE(pt[x]))
                    top_available = 0;
                sum_top += pt[x];
            }
            for (y=0; y<16; y++) {
                if (!AVAILABLE(pl[y]))
                    left_available = 0;
                sum_left += pl[y];
            }

            /* choose predictor */
            sample_t dc_pred;
            if (top_available && left_available)
                dc_pred = (sample_t)((sum_top + sum_left + 16) >> 5);
            else if (top_available)
                dc_pred = (sample_t)((sum_top + 8) >> 4);
            else if (left_available)
                dc_pred = (sample_t)((sum_left + 8) >> 4);
            else
                dc_pred = 1<<(sps->BitDepthY-1);

            for (y=0; y<16; y++)
                for (x=0; x<16; x++)
                    predL[y][x] = dc_pred;
            break;
        }
        case Intra_16x16_Plane:
        {
            int H = 0;
            int V = 0;

            for (x=0; x<8; x++)
                H += (x+1) * (pt[8+x]-pt[6-x]);
            for (y=0; y<8; y++)
                V += (y+1) * (pl[8+y]-pl[6-y]);

            int a = 16 * (pl[15] + pt[15]);
            int b = (5*H + 32) >> 6;
            int c = (5*V + 32) >> 6;

            for (y=0; y<16; y++)
                for (x=0; x<16; x++)
                    predL[y][x] = Clip1y((a + b*(x-7) + c*(y-7) + 16) >> 5);
            break;
        }
    }
}

/* 8.3.1.2 intra_4x4 sample prediction */
void predict_264_intra4x4(struct rv264macro *macro, int luma4x4BlkIdx, const sample_t *cSl, sample_t pred4x4l[4][4])
{
    int x, y;
    int xO, yO;
    struct rv264macro *macros = macro - macro->mb_addr;

    inverse_scan_luma4x4(luma4x4BlkIdx, &xO, &yO);

    /* create neighbouring sample arrays which can be negatively addressed */
    sampleandavail_t pl0[5], pt0[9];
    sampleandavail_t *pl = &pl0[1];
    sampleandavail_t *pt = &pt0[1];

    /* create left column sample array */
    for (x=-1, y=-1; y<4; y++) {
        int xN = xO + x;
        int yN = yO + y;

        int mbAddrN;
        int xW, yW;
        derive_neighbouring_location(macro, xN, yN, LUMA, &mbAddrN, &xW, &yW);

        if (mbAddrN<0)
            pl[y] = NOT_AVAILABLE;
        else if (macro_is_inter(&macros[mbAddrN]) && macro->sh->pps->constrained_intra_pred_flag)
            pl[y] = NOT_AVAILABLE;
        else {
            int xM, yM;

            inverse_scan_macroblock(&macros[mbAddrN], &xM, &yM);

            if (macro->sh->MbaffFrameFlag && macro->mb_field_decoding_flag)
                pl[y] = cSl[xM+xW + (yM+2*yW)*macro->sh->pps->sps->PicWidthInSamplesl];
            else
                pl[y] = cSl[xM+xW + (yM+yW)*macro->sh->pps->sps->PicWidthInSamplesl];
        }
    }
    /* create top row sample array */
    for (x=-1, y=-1; x<8; x++) {
        int xN = xO + x;
        int yN = yO + y;

        int mbAddrN;
        int xW, yW;
        derive_neighbouring_location(macro, xN, yN, LUMA, &mbAddrN, &xW, &yW);

        if (mbAddrN<0)
            pt[x] = NOT_AVAILABLE;
        else if (macro_is_inter(&macros[mbAddrN]) && macro->sh->pps->constrained_intra_pred_flag)
            pt[x] = NOT_AVAILABLE;
        else if (x>3 && (luma4x4BlkIdx==3 || luma4x4BlkIdx==11))
            pt[x] = NOT_AVAILABLE;
        else {
            int xM, yM;

            inverse_scan_macroblock(&macros[mbAddrN], &xM, &yM);

            if (macro->sh->MbaffFrameFlag && macro->mb_field_decoding_flag)
                pt[x] = cSl[xM+xW + (yM+2*yW)*macro->sh->pps->sps->PicWidthInSamplesl];
            else
                pt[x] = cSl[xM+xW + (yM+yW)*macro->sh->pps->sps->PicWidthInSamplesl];
        }
    }

    /* final special condition */
    if (!AVAILABLE(pt[4]) && !AVAILABLE(pt[5]) && !AVAILABLE(pt[6]) && !AVAILABLE(pt[7]) && AVAILABLE(pt[3]))
        pt[4] = pt[5] = pt[6] = pt[7] = pt[3];

    /* select intra 4x4 prediction mode */
    switch (macro->Intra4x4PredMode[luma4x4BlkIdx]) {
        case Intra_4x4_Vertical:
            for (y=0; y<4; y++)
                for (x=0; x<4; x++)
                    pred4x4l[y][x] = pt[x];
            break;
        case Intra_4x4_Horizontal:
            for (y=0; y<4; y++)
                for (x=0; x<4; x++)
                    pred4x4l[y][x] = pl[y];
            break;
        case Intra_4x4_DC:
        {
            sample_t p = 0;

            if (AVAILABLE(pt[0]) && AVAILABLE(pt[1]) && AVAILABLE(pt[2]) && AVAILABLE(pt[3]) && AVAILABLE(pl[0]) && AVAILABLE(pl[1]) && AVAILABLE(pl[2]) && AVAILABLE(pl[3]))
                p = (pt[0] + pt[1] + pt[2] + pt[3] + pl[0] + pl[1] + pl[2] + pl[3] + 4) >> 3;
            else if (AVAILABLE(pl[0]) && AVAILABLE(pl[1]) && AVAILABLE(pl[2]) && AVAILABLE(pl[3]))
                p = (pl[0] + pl[1] + pl[2] + pl[3] + 2) >> 2;
            else if (AVAILABLE(pt[0]) && AVAILABLE(pt[1]) && AVAILABLE(pt[2]) && AVAILABLE(pt[3]))
                p = (pt[0] + pt[1] + pt[2] + pt[3] + 2) >> 2;
            else
                p = 1 << (macro->sh->pps->sps->BitDepthY-1);
            for (y=0; y<4; y++)
                for (x=0; x<4; x++)
                    pred4x4l[y][x] = p;
            break;
        }
        case Intra_4x4_Diagonal_Down_Left:
            for (y=0; y<4; y++)
                for (x=0; x<4; x++)
                {
                    if (x==3 && y==3)
                        pred4x4l[y][x] = (pt[6] + 3*pt[7] + 2) >> 2;
                    else
                        pred4x4l[y][x] = (pt[x+y] + 2*pt[x+y+1] + pt[x+y+2] + 2) >> 2;
                }
            break;
        case Intra_4x4_Diagonal_Down_Right:
            for (y=0; y<4; y++)
                for (x=0; x<4; x++)
                {
                    if (x>y)
                        pred4x4l[y][x] = (pt[x-y-2] + 2*pt[x-y-1] + pt[x-y] + 2) >> 2;
                    else if (x<y)
                        pred4x4l[y][x] = (pl[y-x-2] + 2*pl[y-x-1] + pl[y-x] + 2) >> 2;
                    else
                        pred4x4l[y][x] = (pt[0] + 2*pl[-1] + pl[0] + 2) >> 2;
                }
            break;
        case Intra_4x4_Vertical_Right:
            for (y=0; y<4; y++)
                for (x=0; x<4; x++)
                {
                    int zVR = 2*x - y;
                    switch (zVR) {
                        case 0:
                        case 2:
                        case 4:
                        case 6:
                            pred4x4l[y][x] = (pt[x-(y>>1)-1] + pt[x-(y>>1)] + 1) >> 1;
                            break;

                        case 1:
                        case 3:
                        case 5:
                            pred4x4l[y][x] = (pt[x-(y>>1)-2] + 2*pt[x-(y>>1)-1] + pt[x-(y>>1)] + 2) >> 2;
                            break;

                        case -1:
                            pred4x4l[y][x] = (pl[0] + 2*pl[-1] + pt[0] + 2) >> 2;
                            break;

                        case -2:
                        case -3:
                            pred4x4l[y][x] = (pl[y-1] + 2*pl[y-2] + pl[y-3] + 2) >> 2;
                            break;
                    }
                }
            break;
        case Intra_4x4_Horizontal_Down:
            for (y=0; y<4; y++)
                for (x=0; x<4; x++)
                {
                    int zHD = 2*y - x;
                    switch (zHD) {
                        case 0:
                        case 2:
                        case 4:
                        case 6:
                            pred4x4l[y][x] = (pl[y-(x>>1)-1] + pl[y-(x>>1)] + 1) >> 1;
                            break;

                        case 1:
                        case 3:
                        case 5:
                            pred4x4l[y][x] = (pl[y-(x>>1)-2] + 2*pl[y-(x>>1)-1] + pl[y-(x>>1)] + 2) >> 2;
                            break;

                        case -1:
                            pred4x4l[y][x] = (pl[0] + 2*pl[-1] + pt[0] + 2) >> 2;
                            break;

                        case -2:
                        case -3:
                            pred4x4l[y][x] = (pt[x-1] + 2*pt[x-2] + pt[x-3] + 2) >> 2;
                            break;
                    }
                }
            break;
        case Intra_4x4_Vertical_Left:
            for (y=0; y<4; y++)
                for (x=0; x<4; x++)
                {
                    if (y==0 || y==2)
                        pred4x4l[y][x] = (pt[x+(y>>1)] + pt[x+(y>>1)+1] + 1) >> 1;
                    else
                        pred4x4l[y][x] = (pt[x+(y>>1)] + 2*pt[x+(y>>1)+1] + pt[x+(y>>1)+2] + 2) >> 2;
                }
            break;
        case Intra_4x4_Horizontal_Up:
            for (y=0; y<4; y++)
                for (x=0; x<4; x++)
                {
                    int zHU = x + 2*y;
                    switch (zHU) {
                        case 0:
                        case 2:
                        case 4:
                            pred4x4l[y][x] = (pl[y+(x>>1)] + pl[y+(x>>1)+1] + 1) >> 1;
                            break;

                        case 1:
                        case 3:
                            pred4x4l[y][x] = (pl[y+(x>>1)] + 2*pl[y+(x>>1)+1] + pl[y+(x>>1)+2] + 2) >> 2;
                            break;

                        case 5:
                            pred4x4l[y][x] = (pl[2] + 3*pl[3] + 2) >> 2;
                            break;

                        default:
                            pred4x4l[y][x] = pl[3];
                            break;
                    }
                }
            break;
    }

}

/* 8.3.1.1 derivation process for Intra4x4PredMode */
void predict_264_intra4x4predmode(struct rv264macro *macro, struct rv264seq_parameter_set *sps, struct rv264pic_parameter_set *pps, int luma4x4BlkIdx)
{
    int mbAddrA, mbAddrB;
    int luma4x4BlkIdxA, indexB;
    struct rv264macro *macros = macro - macro->mb_addr;

    getLuma4x4NeighboursAB(macro->mb_addr, luma4x4BlkIdx, sps->pic_width_in_mbs_minus1 + 1, &mbAddrA, &luma4x4BlkIdxA, &mbAddrB, &indexB);

    /* derived variable dcPredModePredictedFlag */
    int dcPredModePredictedFlag = 0;
    if (mbAddrA<0 || mbAddrB<0)
        dcPredModePredictedFlag = 1;
    if (mbAddrA>=0 && macro_is_inter(&macros[mbAddrA]) && pps->constrained_intra_pred_flag)
        dcPredModePredictedFlag = 1;
    if (mbAddrB>=0 && macro_is_inter(&macros[mbAddrB]) && pps->constrained_intra_pred_flag)
        dcPredModePredictedFlag = 1;

    /* derived variables intraMxMPredModeN TODO transform_8x8_mode_flag */
    intra_4x4_pred_mode_t intra4x4PredModeA, intra4x4PredModeB;
    if (dcPredModePredictedFlag || macros[mbAddrA].mb_type!=I_NxN)
        intra4x4PredModeA = Intra_4x4_DC;
    else
        intra4x4PredModeA = macros[mbAddrA].Intra4x4PredMode[luma4x4BlkIdxA];
    if (dcPredModePredictedFlag || macros[mbAddrB].mb_type!=I_NxN)
        intra4x4PredModeB = Intra_4x4_DC;
    else
        intra4x4PredModeB = macros[mbAddrB].Intra4x4PredMode[indexB];

    /* derive intra4x4PredMode[] */
    intra_4x4_pred_mode_t predIntra4x4PredMode = intra4x4PredModeA < intra4x4PredModeB? intra4x4PredModeA : intra4x4PredModeB;
    if (macro->rem_intra_pred_mode[luma4x4BlkIdx]==-1)
        macro->Intra4x4PredMode[luma4x4BlkIdx] = predIntra4x4PredMode;
    else if (macro->rem_intra_pred_mode[luma4x4BlkIdx]<predIntra4x4PredMode)
        macro->Intra4x4PredMode[luma4x4BlkIdx] = macro->rem_intra_pred_mode[luma4x4BlkIdx];
    else
        macro->Intra4x4PredMode[luma4x4BlkIdx] = intra_4x4_pred_mode_t(int(macro->rem_intra_pred_mode[luma4x4BlkIdx]) + 1);
}

/* 8.3 intra prediction */
void predict_264_macroblock_luma(struct rv264macro *macro, sample_t *luma, sample_t predL[4][4])
{
    if (macro->mb_type==I_PCM) {
    } else {
        if (macro_is_intra4x4(macro)) {
            /* macroblock is 4x4 */
            int luma4x4BlkIdx;

            /* 8.3.1 intra_4x4 prediction process for luma samples */
            for (luma4x4BlkIdx=0; luma4x4BlkIdx<16; luma4x4BlkIdx++) {
            }

        } else if (macro_is_intra8x8(macro)) {
        } else {
        }
    }
}

/*
 * h.264 decoding functions.
 */

/* forward declarations */
coeff_t LevelScale4x4(int m, int i, int j);
coeff_t LevelScale8x8(int m, int i, int j);


void decode_intra_pred_mode(int num_macros, struct rv264macro macro[], struct rv264seq_parameter_set *sps, struct rv264pic_parameter_set *pps)
{
    int x, y;
    int luma4x4BlkIdx;

    int height = sps->pic_height_in_map_units_minus1 + 1;
    int width  = sps->pic_width_in_mbs_minus1 + 1;

    for (y=0; y<height; y++) {
        for (x=0; x<width; x++) {
            for (luma4x4BlkIdx=0; luma4x4BlkIdx<16; luma4x4BlkIdx++) {
                int mbAddr = x+y*width;

                predict_264_intra4x4predmode(&macro[mbAddr], sps, pps, luma4x4BlkIdx);
            }
        }
    }
}

void decode_single_mvd(int mb_addr, int index, int part_width, int part_height, int part_index, int PicWidthInMbs, struct rv264macro macro[])
{
    int mbAddrA, mbAddrB, mbAddrC, mbAddrD;
    int indexA, indexB, indexC, indexD;
    int mvA[2], mvB[2], mvC[2];
    int ref_idxA, ref_idxB, ref_idxC;
    int mvp[2];

    /* get neighbouring 4x4 blocks */
    getLuma4x4NeighboursAB(mb_addr, index, PicWidthInMbs, &mbAddrA, &indexA, &mbAddrB, &indexB);
    getLuma4x4NeighboursCD(mb_addr, index, part_width, PicWidthInMbs, &mbAddrC, &indexC, &mbAddrD, &indexD);

    /* check availability of C */
    if (mbAddrC<0) {
        mbAddrC = mbAddrD;
        indexC   = indexD;
    }

    /* fetch motion vector predictors */
    if (mbAddrA<0 || macro_is_intra(&macro[mbAddrA])) {
        mvA[0] = mvA[1] = 0;
        ref_idxA = -1;
    } else {
        mvA[0] = macro[mbAddrA].mv[0][indexA][0];
        mvA[1] = macro[mbAddrA].mv[0][indexA][1];
        ref_idxA = macro[mbAddrA].ref_idx_l0[indexA/4];
    }
    if (mbAddrB<0 || macro_is_intra(&macro[mbAddrB])) {
        mvB[0] = mvB[1] = 0;
        ref_idxB = -1;
    } else {
        mvB[0] = macro[mbAddrB].mv[0][indexB][0];
        mvB[1] = macro[mbAddrB].mv[0][indexB][1];
        ref_idxB = macro[mbAddrB].ref_idx_l0[indexB/4];
    }
    if (mbAddrC<0 || macro_is_intra(&macro[mbAddrC])) {
        mvC[0] = mvC[1] = 0;
        ref_idxC = -1;
    } else {
        mvC[0] = macro[mbAddrC].mv[0][indexC][0];
        mvC[1] = macro[mbAddrC].mv[0][indexC][1];
        ref_idxC = macro[mbAddrC].ref_idx_l0[indexC/4];
    }

    /* check for non-median predictors */
    if (part_width==16 && part_height==8 && part_index==0 && ref_idxB==macro[mb_addr].ref_idx_l0[index/4]) {
        mvp[0] = mvB[0];
        mvp[1] = mvB[1];
    } else if (part_width==16 && part_height==8 && part_index==1 && ref_idxA==macro[mb_addr].ref_idx_l0[index/4]) {
        mvp[0] = mvA[0];
        mvp[1] = mvA[1];
    } else if (part_width==8 && part_height==16 && part_index==0 && ref_idxA==macro[mb_addr].ref_idx_l0[index/4]) {
        mvp[0] = mvA[0];
        mvp[1] = mvA[1];
    } else if (part_width==8 && part_height==16 && part_index==1 && ref_idxC==macro[mb_addr].ref_idx_l0[index/4]) {
        mvp[0] = mvC[0];
        mvp[1] = mvC[1];
    } else {
        /* check availability of B and C */
        if (mbAddrB<0 && mbAddrC<0 && mbAddrA>=0) {
            mvC[0] = mvB[0] = mvA[0];
            mvC[1] = mvB[1] = mvA[1];
            ref_idxC = ref_idxB = ref_idxA;
        }

        /* median motion vector prediction */
        if (ref_idxA==macro[mb_addr].ref_idx_l0[index/4] && ref_idxA!=ref_idxB && ref_idxA!=ref_idxC) {
        mvp[0] = mvA[0];
        mvp[1] = mvA[1];
        } else if (ref_idxB==macro[mb_addr].ref_idx_l0[index/4] && ref_idxB!=ref_idxA && ref_idxB!=ref_idxC) {
            mvp[0] = mvB[0];
            mvp[1] = mvB[1];
        } else if (ref_idxC==macro[mb_addr].ref_idx_l0[index/4] && ref_idxC!=ref_idxA && ref_idxC!=ref_idxB) {
            mvp[0] = mvC[0];
            mvp[1] = mvC[1];
        } else {
            /* median filter motion vector predictors */
            mvp[0] = median(mvA[0], mvB[0], mvC[0]);
            mvp[1] = median(mvA[1], mvB[1], mvC[1]);
        }
    }

    /* motion vector prediction */
    macro[mb_addr].mv[0][index][0] = macro[mb_addr].mvd[0][index/4][index%4][0] + mvp[0];
    macro[mb_addr].mv[0][index][1] = macro[mb_addr].mvd[0][index/4][index%4][1] + mvp[1];
}

void decode_skipped_mvd(int mb_addr, int PicWidthInMbs, struct rv264macro macro[])
{
    int mbAddrA, mbAddrB, mbAddrC, mbAddrD;
    int indexA, indexB, indexC, indexD;

    /* get neighbouring 4x4 blocks */
    getLuma4x4NeighboursAB(mb_addr, 0, PicWidthInMbs, &mbAddrA, &indexA, &mbAddrB, &indexB);
    getLuma4x4NeighboursCD(mb_addr, 0, 16, PicWidthInMbs, &mbAddrC, &indexC, &mbAddrD, &indexD);

    /* check availability of A and B */
    if (mbAddrA<0 || mbAddrA<0) {
        macro[mb_addr].mv[0][0][0] = 0;
        macro[mb_addr].mv[0][0][1] = 0;
        return;
    }

    /* check ref_idx_l0 of A and B */
    if (macro[mbAddrA].ref_idx_l0[indexA/4]==0 && macro[mbAddrA].mv[0][indexA][0]==0 && macro[mbAddrA].mv[0][indexA][1]==0) {
        macro[mb_addr].mv[0][0][0] = 0;
        macro[mb_addr].mv[0][0][1] = 0;
        return;
    }
    if (macro[mbAddrB].ref_idx_l0[indexB/4]==0 && macro[mbAddrB].mv[0][indexB][0]==0 && macro[mbAddrB].mv[0][indexB][1]==0) {
        macro[mb_addr].mv[0][0][0] = 0;
        macro[mb_addr].mv[0][0][1] = 0;
        return;
    }

    /* otherwise decode motion vector normally */
    decode_single_mvd(mb_addr, 0, 16, 16, 0, PicWidthInMbs, macro);
}

void decode_mvd(int num_macros, struct rv264macro macro[], struct rv264seq_parameter_set *sps, struct rv264pic_parameter_set *pps)
{
    int x, y;
    int index;

    int height = sps->pic_height_in_map_units_minus1 + 1;
    int width  = sps->pic_width_in_mbs_minus1 + 1;

    for (y=0; y<height; y++) {
        for (x=0; x<width; x++) {
            int mb_addr = x+y*width;
            int mb_type = macro[mb_addr].mb_type;

            switch (mb_type) {
                case P_L0_16x16:
                    /* decode one motion vector */
                    decode_single_mvd(mb_addr, 0, 16, 16, 0, width, macro);

                    /* duplicate across partition */
                    for (index=1; index<16; index++) {
                        macro[mb_addr].mv[0][index][0] = macro[mb_addr].mv[0][0][0];
                        macro[mb_addr].mv[0][index][1] = macro[mb_addr].mv[0][0][1];
                    }
                    break;

                case P_L0_L0_16x8:
                    /* decode first motion vector */
                    decode_single_mvd(mb_addr, 0, 16, 8, 0, width, macro);

                    /* duplicate across partition */
                    for (index=1; index<8; index++) {
                        macro[mb_addr].mv[0][index][0] = macro[mb_addr].mv[0][0][0];
                        macro[mb_addr].mv[0][index][1] = macro[mb_addr].mv[0][0][1];
                    }

                    /* decode second motion vector */
                    decode_single_mvd(mb_addr, 8, 16, 8, 1, width, macro);

                    /* duplicate across partition */
                    for (index=9; index<16; index++) {
                        macro[mb_addr].mv[0][index][0] = macro[mb_addr].mv[0][8][0];
                        macro[mb_addr].mv[0][index][1] = macro[mb_addr].mv[0][8][1];
                    }
                    break;

                case P_L0_L0_8x16:
                    /* decode first motion vector */
                    decode_single_mvd(mb_addr, 0, 8, 16, 0, width, macro);

                    /* duplicate across partition */
                    for (index=1; index<4; index++) {
                        macro[mb_addr].mv[0][index][0] = macro[mb_addr].mv[0][0][0];
                        macro[mb_addr].mv[0][index][1] = macro[mb_addr].mv[0][0][1];
                    }
                    for (index=8; index<12; index++) {
                        macro[mb_addr].mv[0][index][0] = macro[mb_addr].mv[0][0][0];
                        macro[mb_addr].mv[0][index][1] = macro[mb_addr].mv[0][0][1];
                    }

                    /* decode second motion vector */
                    decode_single_mvd(mb_addr, 4, 8, 16, 1, width, macro);

                    /* duplicate across partition */
                    for (index=5; index<8; index++) {
                        macro[mb_addr].mv[0][index][0] = macro[mb_addr].mv[0][4][0];
                        macro[mb_addr].mv[0][index][1] = macro[mb_addr].mv[0][4][1];
                    }
                    for (index=12; index<16; index++) {
                        macro[mb_addr].mv[0][index][0] = macro[mb_addr].mv[0][4][0];
                        macro[mb_addr].mv[0][index][1] = macro[mb_addr].mv[0][4][1];
                    }
                    break;

                case P_8x8:
                case P_8x8ref0:
                {
                    int origin;
                    for (origin=0; origin<16; origin+=4) {
                        switch (macro[mb_addr].sub_mb_type[origin/4]) {
                            case P_L0_8x8:
                                /* decode one motion vector */
                                decode_single_mvd(mb_addr, origin+0, 8, 8, 0, width, macro);

                                /* duplicate across submacroblock */
                                for (index=origin; index<origin+4; index++) {
                                    macro[mb_addr].mv[0][index][0] = macro[mb_addr].mv[0][origin][0];
                                    macro[mb_addr].mv[0][index][1] = macro[mb_addr].mv[0][origin][1];
                                }
                                break;

                            case P_L0_8x4:
                                /* decode first motion vector */
                                decode_single_mvd(mb_addr, origin+0, 8, 4, 0, width, macro);

                                /* duplicate across submacroblock */
                                macro[mb_addr].mv[0][origin+1][0] = macro[mb_addr].mv[0][origin+0][0];
                                macro[mb_addr].mv[0][origin+1][1] = macro[mb_addr].mv[0][origin+0][1];

                                /* decode second motion vector */
                                decode_single_mvd(mb_addr, origin+2, 8, 4, 1, width, macro);

                                /* duplicate across submacroblock */
                                macro[mb_addr].mv[0][origin+3][0] = macro[mb_addr].mv[0][origin+2][0];
                                macro[mb_addr].mv[0][origin+3][1] = macro[mb_addr].mv[0][origin+2][1];
                                break;

                            case P_L0_4x8:
                                /* decode first motion vector */
                                decode_single_mvd(mb_addr, origin+0, 4, 8, 0, width, macro);

                                /* duplicate across submacroblock */
                                macro[mb_addr].mv[0][origin+2][0] = macro[mb_addr].mv[0][origin+0][0];
                                macro[mb_addr].mv[0][origin+2][1] = macro[mb_addr].mv[0][origin+0][1];

                                /* decode second motion vector */
                                decode_single_mvd(mb_addr, origin+1, 4, 8, 1, width, macro);

                                /* duplicate across submacroblock */
                                macro[mb_addr].mv[0][origin+3][0] = macro[mb_addr].mv[0][origin+1][0];
                                macro[mb_addr].mv[0][origin+3][1] = macro[mb_addr].mv[0][origin+1][1];
                                break;

                            case P_L0_4x4:
                                /* decode four motion vectors */
                                decode_single_mvd(mb_addr, origin+0, 4, 4, 0, width, macro);
                                decode_single_mvd(mb_addr, origin+1, 4, 4, 1, width, macro);
                                decode_single_mvd(mb_addr, origin+2, 4, 4, 2, width, macro);
                                decode_single_mvd(mb_addr, origin+3, 4, 4, 3, width, macro);
                                break;

                            default:
                                break;
                        }
                    }
                    break;
                }

                case P_Skip:
                    /* decode one motion vector */
                    decode_skipped_mvd(mb_addr, width, macro);

                    /* duplicate across partition */
                    for (index=1; index<16; index++) {
                        macro[mb_addr].mv[0][index][0] = macro[mb_addr].mv[0][0][0];
                        macro[mb_addr].mv[0][index][1] = macro[mb_addr].mv[0][0][1];
                    }
                    break;
            }
        }
    }
}

/* 8.5.14 picture construction process prior to deblocking filter process */
void decode_264_picture_construction_luma16x16(struct rv264macro *macro, sample_t u[16][16], sample_t *luma)
{
    const int nE = 16;
    int xP, yP;
    int i, j;

    inverse_scan_macroblock(macro, &xP, &yP);

    if (macro->sh->MbaffFrameFlag && macro->mb_field_decoding_flag) {
        for (j=0; j<nE; j++)
            for (i=0; i<nE; i++)
                luma[xP+i + (yP+2*j)*macro->sh->pps->sps->PicWidthInSamplesl] = u[j][i];
    } else {
        for (j=0; j<nE; j++) {
            for (i=0; i<nE; i++) {
                //printf("%d,%d ", xP+xO+i , (yP+yO+j));
                //printf("%3d ", u[j][i]);
                luma[xP+i + (yP+j)*macro->sh->pps->sps->PicWidthInSamplesl] = u[j][i];
            }
            //printf("\n");
        }
        //printf("\n");
    }
}

void decode_264_picture_construction_luma4x4(struct rv264macro *macro, sample_t u[4][4], int luma4x4BlkIdx, sample_t *luma)
{
    const int nE = 4;
    int xP, yP;
    int xO, yO;
    int i, j;

    inverse_scan_macroblock(macro, &xP, &yP);
    inverse_scan_luma4x4(luma4x4BlkIdx, &xO, &yO);

    if (macro->sh->MbaffFrameFlag && macro->mb_field_decoding_flag) {
        for (j=0; j<nE; j++)
            for (i=0; i<nE; i++)
                luma[xP+xO+i + (yP+2*(yO+j))*macro->sh->pps->sps->PicWidthInSamplesl] = u[j][i];
    } else {
        for (j=0; j<nE; j++) {
            for (i=0; i<nE; i++) {
                //printf("%d,%d ", xP+xO+i , (yP+yO+j));
                //printf("%3d ", u[j][i]);
                luma[xP+xO+i + (yP+yO+j)*macro->sh->pps->sps->PicWidthInSamplesl] = u[j][i];
            }
            //printf("\n");
        }
        //printf("\n");
    }
}
void decode_264_picture_construction_chroma8x8(struct rv264macro *macro, sample_t u[8][8], sample_t *chroma)
{
    const int nW = macro->sh->pps->sps->MbWidthC;
    const int nH = macro->sh->pps->sps->MbHeightC;
    const int subWidthC = macro->sh->pps->sps->SubWidthC;
    const int subHeightC = macro->sh->pps->sps->SubHeightC;
    int xP, yP;
    int xO, yO;
    int i, j;

    inverse_scan_macroblock(macro, &xP, &yP);
    xO = 0;
    yO = 0;

    if (macro->sh->MbaffFrameFlag && macro->mb_field_decoding_flag) {
        for (j=0; j<nH; j++)
            for (i=0; i<nW; i++)
                chroma[(xP/subWidthC)+xO+i + (((yP+subHeightC-1)/subHeightC)+2*(yO+j))*macro->sh->pps->sps->PicWidthInSamplesl] = u[j][i];
    } else {
        for (j=0; j<nH; j++) {
            for (i=0; i<nW; i++) {
                //printf("%d,%d ", xP+xO+i , (yP+yO+j));
                //printf("%3d ", u[j][i]);
                chroma[(xP/subWidthC)+xO+i + ((yP/subHeightC)+yO+j)*macro->sh->pps->sps->PicWidthInSamplesc] = u[j][i];
            }
            //printf("\n");
        }
        //printf("\n");
    }
}

/* 8.5.12.1 scaling process for residual 4x4 blocks */
void decode_264_scaling_4x4(struct rv264macro *macro, coeff_t c[4][4], int bitDepth, int qP, chroma_t chroma, coeff_t d[4][4])
{
    int i, j;

    for (j=0; j<4; j++) {
        for (i=0; i<4; i++) {
            if (i==0 && j==0 && ((!chroma && macro_is_intra16x16(macro)) || chroma ))
                d[0][0] = c[0][0];
            else {
                if (qP>=24)
                    d[j][i] = (c[j][i] * LevelScale4x4(qP%6,i,j)) << (qP/6-4);
                else
                    d[j][i] = (c[j][i] * LevelScale4x4(qP%6,i,j) + (1<<(3-qP/6))) >> (4-qP/6);
            }
        }
    }
}

/* 8.5.12.2 transformation process for residual 4x4 blocks */
void decode_264_transformation_4x4(struct rv264macro *macro, coeff_t d[4][4], int bitDepth, coeff_t r[4][4])
{
    int i, j;
    coeff_t e[4][4];
    coeff_t f[4][4];
    coeff_t g[4][4];
    coeff_t h[4][4];

    for (i=0; i<4; i++) {
        e[0][i] = d[0][i] + d[2][i];
        e[1][i] = d[0][i] - d[2][i];
        e[2][i] = (d[1][i]>>1) - d[3][i];
        e[3][i] = d[1][i] + (d[3][i]>>1);
    }
    for (i=0; i<4; i++) {
        f[0][i] = e[0][i] + e[3][i];
        f[1][i] = e[1][i] + e[2][i];
        f[2][i] = e[1][i] - e[2][i];
        f[3][i] = e[0][i] - e[3][i];
    }
    for (j=0; j<4; j++) {
        g[j][0] = f[j][0] + f[j][2];
        g[j][1] = f[j][0] - f[j][2];
        g[j][2] = (f[j][1]>>1) - f[j][3];
        g[j][3] = f[j][1] + (f[j][3]>>1);
    }
    for (j=0; j<4; j++) {
        h[j][0] = g[j][0] + g[j][3];
        h[j][1] = g[j][1] + g[j][2];
        h[j][2] = g[j][1] - g[j][2];
        h[j][3] = g[j][0] - g[j][3];
    }

    for (j=0; j<4; j++) {
        for (i=0; i<4; i++) {
            r[j][i] = (h[j][i]+32) >> 6;
        }
    }
}

/* 8.5.12 scaling and transformation process for residual 4x4 blocks */
void decode_264_transform_4x4(struct rv264macro *macro, coeff_t c[4][4], int bitDepth, int qP, chroma_t chroma, coeff_t r[4][4])
{
    //const int sMbFlag = 0;

    if (macro->TransformBypassModeFlag) {
        int i, j;
        for (j=0; j<4; j++)
            for (i=0; i<4; i++)
                r[j][i] = c[j][i];
    } else {
        coeff_t d[4][4];

        decode_264_scaling_4x4(macro, c, bitDepth, qP, chroma, d);
        decode_264_transformation_4x4(macro, d, bitDepth, r);
    }
}

/* 8.5.11 scaling and transformation process for chroma DC transform coefficients */
void ihadamard2x2(coeff_t tblock[2][2], coeff_t block[2][2])
{
  coeff_t t0,t1,t2,t3;

  t0 = tblock[0][0] + tblock[0][1];
  t1 = tblock[0][0] - tblock[0][1];
  t2 = tblock[1][0] + tblock[1][1];
  t3 = tblock[1][0] - tblock[1][1];

  block[0][0] = (t0 + t2);
  block[0][1] = (t1 + t3);
  block[1][0] = (t0 - t2);
  block[1][1] = (t1 - t3);
}
void decode_264_transform_chroma_2x2dc(struct rv264macro *macro, coeff_t c[2][2], int bitDepth, int qP, coeff_t dcC[2][2])
{
    int i, j;

    if (macro->TransformBypassModeFlag) {
        for (j=0; j<2; j++)
            for (i=0; i<2; i++)
                dcC[j][i] = c[j][i];
    } else {
        coeff_t f[2][2];

        /* 8.5.11 transformation process for chroma DC transform coefficients 
        coeff_t p0 = c[0][0] + c[0][1];
        coeff_t p1 = c[0][0] - c[0][1];
        coeff_t p2 = c[1][0] + c[1][1];
        coeff_t p3 = c[1][0] - c[1][1];

        f[0][0] = p0 + p2;
        f[0][1] = p1 + p3;
        f[1][0] = p0 - p2;
        f[1][1] = p1 - p3;*/
        ihadamard2x2(c, f);

        /* 8.5.12 scaling process for chroma DC transform coefficients */
        for (j=0; j<2; j++)
            for (i=0; i<2; i++)
                dcC[j][i] = ((f[j][i] * LevelScale4x4(qP%6,0,0)) << (qP/6)) >> 5;
    }
}

/* 8.5.10 scaling and transformation process for DC transform coefficients for Intra_16x16 macroblock type */
void decode_264_transform_16x16dc(struct rv264macro *macro, coeff_t c[4][4], int bitDepth, int qP, coeff_t dcY[4][4])
{
    int i, j;

    /* transform */
    if (macro->TransformBypassModeFlag) {
        for (j=0; j<4; j++)
            for (i=0; i<4; i++)
                dcY[j][i] = c[j][i];
    } else {
        coeff_t f[4][4];

        //decode_264_transformation_4x4(macro, c, bitDepth, f);
        ihadamard_4x4(c, f);

        /* scaling */
        for (j=0; j<4; j++) {
            for (i=0; i<4; i++) {
                if (qP>=36)
                    dcY[j][i] = (f[j][i] * LevelScale4x4(qP%6,0,0)) << (qP/6-6);
                else
                    dcY[j][i] = (f[j][i] * LevelScale4x4(qP%6,0,0) + (1<<(5-qP/6))) >> (6-qP/6);
            }
        }
    }
}

/* 8.5.9 derivation process for scaling functions TODO scaling lists */
coeff_t LevelScale4x4(int m, int i, int j)
{
    static const coeff_t normAdjust4x4[6][3] = {
        /* NOTE columns 2,3 swapped */
        { 10, 13, 16 },
        { 11, 14, 18 },
        { 13, 16, 20 },
        { 14, 18, 23 },
        { 16, 20, 25 },
        { 18, 23, 29 },
    };

    return (coeff_t)16 * normAdjust4x4[m][(i%2)+(j%2)];
}

coeff_t LevelScale8x8(int m, int i, int j)
{
    static const coeff_t normAdjust8x8[6][6] = {
        /* NOTE  */
        { 20, 18, 32, 19, 25, 24 },
        { 22, 19, 35, 21, 28, 26 },
        { 26, 23, 42, 24, 33, 31 },
        { 28, 25, 45, 26, 35, 33 },
        { 32, 28, 51, 30, 40, 38 },
        { 36, 32, 58, 34, 46, 43 },
    };

    return (coeff_t)16 * normAdjust8x8[m][(i%4)+(j%4)]; // FIXME not as simple as this
}

/* 8.5.7 inverse scanning process for 8x8 transform coefficients and scaling lists */
void decode_264_inverse_scanning_8x8(struct rv264macro *macro, const sample_t list[64], sample_t c[8][8])
{
    static const int mapping[2][64] = {
        { 0, 1, 5, 6, 14, 15, 27, 28, 2, 4, 7, 13, 16, 26, 29, 42, 3, 8, 12, 17, 25, 30, 41, 43, 9, 11, 18, 24, 31, 40, 44, 53,
        10, 19, 23, 32, 39, 45, 52, 54, 20, 22, 33, 38, 51, 55, 60, 21, 34, 37, 47, 50, 56, 59, 61, 35, 36, 48, 49, 57, 58, 62, 63 },   /* zig-zag */
        { 0, 3, 8, 15, 22, 30, 38, 52, 1, 4, 14, 21, 29, 37, 45, 53, 2, 7, 16, 23, 31, 39, 46, 58, 5, 9, 20, 28, 36, 44, 51, 59,
        6, 13, 24, 32, 40, 47, 54, 60, 10, 17, 25, 33, 41, 48, 55, 61, 11, 18, 26, 34, 42, 49, 56, 62, 12, 19, 27, 35, 43, 50, 57, 63 } /* field */
    };

    sample_t *p = (sample_t *) c;
    int scan = macro->mb_field_decoding_flag;

    int i;
    for (i=0; i<64; i++)
        *p++ = list[mapping[scan][i]];
}

/* 8.5.6 inverse scanning process for 4x4 transform coefficients and scaling lists */
void decode_264_inverse_scanning_4x4(struct rv264macro *macro, const coeff_t list[16], coeff_t c[4][4])
{
    static const int mapping[2][16] = {
        { 0, 1, 5, 6, 2, 4, 7, 12, 3, 8, 11, 13, 9, 10, 14, 15 }, /* zig-zag */
        { 0, 2, 8, 12, 1, 5, 9, 13, 3, 6, 10, 14, 4, 7, 11, 15 }  /* field */
    };

    int scan = macro->mb_field_decoding_flag;

    int i, j, k;
    for (j=0, k=0; j<4; j++)
        for (i=0; i<4; i++, k++)
            c[j][i] = list[mapping[scan][k]];
}

/* 8.5.4 specification of transform decoding process for chroma samples */
void decode_264_macroblock_chroma(struct rv264macro *macro, int iCbCr, int QPc, sample_t *chroma)
{
    const int bitDepth = 8;
    int i, j;

    struct slice_header *sh = macro->sh;
    struct rv264pic_parameter_set *pps = sh->pps;
    struct rv264seq_parameter_set *sps = pps->sps;

    if (sps->ChromaArrayType!=3) {
        int numChroma4x4Blks = (sps->MbWidthC/4) * (sps->MbHeightC/4);

        if (sps->ChromaArrayType==1) {
            coeff_t c[2][2] = {
                { macro->ChromaDCLevel[iCbCr][0], macro->ChromaDCLevel[iCbCr][1] },
                { macro->ChromaDCLevel[iCbCr][2], macro->ChromaDCLevel[iCbCr][3] },
            };
            coeff_t dcC[2][2];

            decode_264_transform_chroma_2x2dc(macro, c, bitDepth, QPc, dcC);

            coeff_t rMb[8][8];
            int chroma4x4BlkIdx;
            for (chroma4x4BlkIdx=0; chroma4x4BlkIdx<numChroma4x4Blks; chroma4x4BlkIdx++) {
                coeff_t chromaList[16];
                coeff_t c[4][4];
                coeff_t r[4][4];

                static const int mapping[4][2] = {
                    {0,0}, {0,1}, {1,0}, {1,1},
                };

                int j0 = mapping[chroma4x4BlkIdx][0];
                int i0 = mapping[chroma4x4BlkIdx][1];
                chromaList[0] = dcC[j0][i0];
                memcpy(&chromaList[1], macro->ChromaACLevel[iCbCr][chroma4x4BlkIdx], 15*sizeof(coeff_t));

                decode_264_inverse_scanning_4x4(macro, chromaList, c);
                decode_264_transform_4x4(macro, c, bitDepth, QPc + sps->QpBdOffsetC, CHROMA, r);

                int xO = InverseRasterScanX(chroma4x4BlkIdx, 4, 4, 8);
                int yO = InverseRasterScanY(chroma4x4BlkIdx, 4, 4, 8);
                for (j=0; j<4; j++)
                    for (i=0; i<4; i++)
                        rMb[yO+j][xO+i] = r[j][i];
            }

            //if (macro->TransformBypassModeFlag && (macro_is_intra4x4(macro) || macro_is_intra8x8(macro) || macro_is_intra16x16(macro)) &&
            //    (macro->intra_chroma_pred_mode==1 || macro->intra_chroma_pred_mode==2)) {
            //}

            sample_t predC[8][8];
            predict_264_chroma8x8(macro, chroma, predC);

            sample_t u[8][8];
            for (j=0; j<8; j++) {
                for (i=0; i<8; i++) {
                    //printf("%d ", predC[j][i]);
                    //printf("%d ", rMb[j][i]);
                    u[j][i] = (sample_t) Clip1c((coeff_t)predC[j][i] + rMb[j][i]);
                    //printf("%d ", u[j][i]);
                }
                //printf("\n");
            }
            //printf("\n");

            /* copy macroblock into frame */
            decode_264_picture_construction_chroma8x8(macro, u, chroma);

        } else {
        }
    } else {
        /* 8.5.5 */
    }
}

void decode_264_macroblock_luma(struct rv264macro *macro, sample_t *luma)
{
    const int bitDepth = 8;
    int i, j;

    struct slice_header *sh = macro->sh;
    struct rv264pic_parameter_set *pps = sh->pps;
    struct rv264seq_parameter_set *sps = pps->sps;

    /* 8.5.2 specification of transform decoding process for luma samples of Intra_16x16 macroblock prediction mode */
    if (macro_is_intra16x16(macro)) {
        /* macroblock is 16x16 */
        coeff_t c[4][4];
        coeff_t dcY[4][4];

        //printf("macro %d\n", macro->mb_addr);

        decode_264_inverse_scanning_4x4(macro, macro->Intra16x16DCLevel, c);
        decode_264_transform_16x16dc(macro, c, bitDepth, macro->QPy, dcY);

        coeff_t rMb[16][16];
        int luma4x4BlkIdx;
        for (luma4x4BlkIdx=0; luma4x4BlkIdx<16; luma4x4BlkIdx++) {
            coeff_t lumaList[16];
            coeff_t r[4][4];

            static const int mapping[16][2] = {
                {0,0}, {0,1}, {1,0}, {1,1},
                {0,2}, {0,3}, {1,2}, {1,3},
                {2,0}, {2,1}, {3,0}, {3,1},
                {2,2}, {2,3}, {3,2}, {3,3},
            };

            int j0 = mapping[luma4x4BlkIdx][0];
            int i0 = mapping[luma4x4BlkIdx][1];
            lumaList[0] = dcY[j0][i0];
            memcpy(&lumaList[1], macro->Intra16x16ACLevel[luma4x4BlkIdx], 15*sizeof(coeff_t));

            decode_264_inverse_scanning_4x4(macro, lumaList, c);
            decode_264_transform_4x4(macro, c, bitDepth, macro->QPy + sps->QpBdOffsetY, LUMA, r);

            /*for (j=0; j<4; j++) {
                for (i=0; i<4; i++)
                    printf("%d ", c[j][i]);
                printf("\n");
            }
            printf("\n");*/

            int xO;
            int yO;
            inverse_scan_luma4x4(luma4x4BlkIdx, &xO, &yO);
            for (j=0; j<4; j++)
                for (i=0; i<4; i++)
                    rMb[yO+j][xO+i] = r[j][i];
        }

        //if (macro->TransformBypassModeFlag && (macro->Intra16x16PredMode==0 || macro->Intra16x16PredMode==1)) {
        //}

        sample_t predL[16][16];
        predict_264_intra16x16(macro, luma, predL);

        sample_t u[16][16];
        for (j=0; j<16; j++) {
            for (i=0; i<16; i++) {
                //printf("%d ", predL[j][i]);
                //printf("%d ", rMb[j][i]);
                u[j][i] = (sample_t) Clip1y((coeff_t)predL[j][i] + rMb[j][i]);
                //printf("%d ", u[j][i]);
            }
            //printf("\n");
        }
        //printf("\n");

        /* copy macroblock into frame */
        decode_264_picture_construction_luma16x16(macro, u, luma);
    } else if (macro->transform_size_8x8_flag==0) {
        /* macroblock is 4x4 */
        int luma4x4BlkIdx;

        /* 8.5.1 transform decoding process for 4x4 luma residual blocks */
        for (luma4x4BlkIdx=0; luma4x4BlkIdx<16; luma4x4BlkIdx++) {
            coeff_t c[4][4];
            coeff_t r[4][4];

            //printf("macro %d, index %d\n", macro->mb_addr, luma4x4BlkIdx);

            decode_264_inverse_scanning_4x4(macro, macro->LumaLevel[luma4x4BlkIdx], c);
            decode_264_transform_4x4(macro, c, bitDepth, macro->QPy + sps->QpBdOffsetY, LUMA, r);

            //if (macro->TransformBypassModeFlag && macro_is_intra4x4(macro) && (macro->Intra4x4PredMode[luma4x4BlkIdx]==0 || macro->Intra4x4PredMode[luma4x4BlkIdx]==1)) {
            //}

            sample_t predL[4][4];
            predict_264_intra4x4predmode(macro, sps, pps, luma4x4BlkIdx);
            predict_264_intra4x4(macro, luma4x4BlkIdx, luma, predL);

            sample_t u[4][4];
            for (i=0; i<4; i++) {
                for (j=0; j<4; j++) {
                    //printf("%d ", c[i][j]);
                    //printf("%d ", predL[i][j]);
                    //printf("%d ", r[i][j]);
                    u[i][j] = (sample_t) Clip1y((coeff_t)predL[i][j] + r[i][j]);
                    //printf("%d ", u[i][j]);
                }
                //printf("\n");
            }
            //printf("\n");

            /* copy macroblock into frame */
            decode_264_picture_construction_luma4x4(macro, u, luma4x4BlkIdx, luma);
        }

    } else {
        /* macroblock is 8x8 */
    }
}

sample_t *decode_264_picture(struct rv264picture *picture, struct rv264seq_parameter_set *sps, int picno)
{
    int mbx, mby;

    const int frame_width  = 16*sps->PicWidthInMbs;
    const int frame_height = 16*sps->FrameHeightInMbs;
    const int field_height = frame_height >> picture_is_field(picture);

    /* sanity check */
    if (picture==NULL || picture->sh==NULL || picture->macro==NULL)
        return NULL;

    /* swap reference pointers in P-pictures */
    /*if (picture->picture_coding_type==PCT_P)
        forward_reference = backward_reference;*/

    /* allocate picture */
    size_t size = frame_width*field_height*3/2*sizeof(sample_t);
    switch (sps->chroma_format_idc) {
        case 3 : size = frame_width*field_height*6/2*sizeof(sample_t); break;
        case 2 : size = frame_width*field_height*4/2*sizeof(sample_t); break;
        case 1 : size = frame_width*field_height*3/2*sizeof(sample_t); break;
        case 0 : size = frame_width*field_height*2/2*sizeof(sample_t); break;
        default: rverror("unknown value for chroma_format_idc: %d", sps->chroma_format_idc);
    }
    sample_t *frame = (sample_t *)rvalloc(NULL, size, 0);
    memset(frame, 0x80, size);

    /* prepare plane pointers */
    sample_t *plane[3] = {frame, 0, 0};
    switch (sps->chroma_format_idc) {
        case 3 : plane[1] = frame + frame_width*field_height; plane[2] = frame + frame_width*field_height*8/4*sizeof(sample_t); break;
        case 2 : plane[1] = frame + frame_width*field_height; plane[2] = frame + frame_width*field_height*6/4*sizeof(sample_t); break;
        case 1 : plane[1] = frame + frame_width*field_height; plane[2] = frame + frame_width*field_height*5/4*sizeof(sample_t); break;
    }

    /* initialise Qp */
    int QPy_prev = picture->macro[0].QPy;

    /* loop over macroblocks */
    for (mby=0; mby<sps->FrameHeightInMbs>>picture_is_field(picture); mby++) {
        for (mbx=0; mbx<sps->PicWidthInMbs; mbx++) {
            int mb_addr = mby*sps->PicWidthInMbs + mbx;
            struct rv264macro *macro = picture->macro + mb_addr;

            /* sanity check */

            /* check for new slice */
            if (macro->sh->first_mb_in_slice == mb_addr) {
                QPy_prev = 26 + picture->pps->pic_init_qp_minus26 + macro->sh->slice_qp_delta;
            }

            /* update Qp */
            macro->QPy = ((QPy_prev + macro->mb_qp_delta + 52 + 2*sps->QpBdOffsetY) % (52+sps->QpBdOffsetY)) - sps->QpBdOffsetY;
            if (sps->qpprime_y_zero_transform_bypass_flag==1 && macro->QPy + sps->QpBdOffsetY == 0)
                macro->TransformBypassModeFlag = 1;
            else
                macro->TransformBypassModeFlag = 0;

            /* 8.5.8 derivation process for chroma quantisation parameters */
            static const int lookup_qpc[] = {
                 0,  1,  2,  3,  4,  5,  6,  7,  8,  9,
                10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
                20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
                29, 30, 31, 32, 32, 33, 34, 34, 35, 35,
                36, 36, 37, 37, 37, 38, 38, 38, 39, 39,
                39, 39,
            };
            int qPI = Clip3(-sps->QpBdOffsetC, 51, macro->QPy + picture->pps->chroma_qp_index_offset);
            macro->QPc[0] = lookup_qpc[qPI] + sps->QpBdOffsetC;
            qPI = Clip3(-sps->QpBdOffsetC, 51, macro->QPy + picture->pps->second_chroma_qp_index_offset);
            macro->QPc[1] = lookup_qpc[qPI] + sps->QpBdOffsetC;

            /* decode macroblock */
            decode_264_macroblock_luma(macro, plane[0]);
            decode_264_macroblock_chroma(macro, 0, macro->QPc[0], plane[1]);
            decode_264_macroblock_chroma(macro, 1, macro->QPc[1], plane[2]);
            //if (mb_addr==0)
            //   return frame;

            /* motion compensate macroblock */
            //sample_t pels[8][64];
            //if (picture->picture_coding_type==PCT_P)
            //    if (!macroblock_intra(macro->type) && forward_reference!=NULL)
            //        decode_m2v_fetch(macro, 0, mbx, mby, frame_width, frame_height, bottom_field_picture(picture), sequence->chroma_format, forward_reference, 0, pels);
            //if (picture->picture_coding_type==PCT_B) {
            //    if (macroblock_motion_forward(macro->type) && forward_reference!=NULL && macroblock_motion_backward(macro->type) && backward_reference!=NULL) {
            //        decode_m2v_fetch(macro, 0, mbx, mby, frame_width, frame_height, bottom_field_picture(picture), sequence->chroma_format, forward_reference, 0, pels);
            //        decode_m2v_fetch(macro, 1, mbx, mby, frame_width, frame_height, bottom_field_picture(picture), sequence->chroma_format, backward_reference, 1, pels);
            //    }
            //    else if (macroblock_motion_forward(macro->type) && forward_reference!=NULL)
            //        decode_m2v_fetch(macro, 0, mbx, mby, frame_width, frame_height, bottom_field_picture(picture), sequence->chroma_format, forward_reference, 0, pels);
            //    else if (macroblock_motion_backward(macro->type) && backward_reference!=NULL)
            //        decode_m2v_fetch(macro, 1, mbx, mby, frame_width, frame_height, bottom_field_picture(picture), sequence->chroma_format, backward_reference, 0, pels);
            //}

            /* reconstruct macroblock */
            //if (macroblock_intra(macro->type))
            //    decode_m2v_sample(macro, sequence->chroma_format, pels);
            //else
            //    decode_m2v_recon(macro, sequence->chroma_format, pels);

        }
    }

    return frame;
}
