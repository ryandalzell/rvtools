/*
 * Description: Crop and pad video sequences.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2010-01-22 15:52:39 $
 * Revision   : $Revision: 1.18 $
 * Copyright  : (c) 2005 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <signal.h>
#include <errno.h>
#include "getopt.h"

#include "rvutil.h"
#include "rvy4m.h"
#include "rvimage.h"

const char *appname;

void usage(int exitcode)
{
    fprintf(stderr, "%s: crop and expand uncompressed video frames\n", appname);
    fprintf(stderr, "usage: %s -c <width>x<height>[+<xoff>[+<yoff>]] [options] [<file>] [<file>...]\n", appname);
    fprintf(stderr, "  -s, --size          : image size format\n");
    fprintf(stderr, "     (formats : %s)\n", rvformats);
    fprintf(stderr, "  -p, --fourcc        : image pixel format fourcc (default: I420)\n");
    fprintf(stderr, "     (formats : %s)\n", rvfourccs);
    fprintf(stderr, "  -n, --numframes     : number of frames (default: all)\n");
    fprintf(stderr, "  -o, --output        : write output to file\n");
    fprintf(stderr, "  -4, --yuv4mpeg      : write yuv4mpeg format to output (default: raw yuv format)\n");
    fprintf(stderr, "  -q, --quiet         : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "  -v, --verbose       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "  --                  : disable argument processing\n");
    fprintf(stderr, "  -h, --help, --usage : print this usage message\n");
    exit(exitcode);
}

int main(int argc, char *argv[])
{
    FILE *filein = stdin;
    FILE *fileout = stdout;
    char *filename[RV_MAXFILES] = {0};
    char *outfile = NULL;
    int fileindex = 0;
    int numfiles = 0;
    int totframes = 0;

    /* command line defaults */
    const char *frames[RV_MAXFILES];
    const char *cropping = NULL;
    int width = 0;
    int height = 0;
    const char *format = NULL;
    const char *fccode = NULL;
    int numframes = -1;
    int force_raw = 1;
    int yuv4mpeg = 0;
    int verbose = 0;

    /* input file parameters */
    int inp_width;
    int inp_height;
    int inp_raw;

    /* crop parameters */
    int crop_width = 0;
    int crop_height = 0;
    int xoff = 0;
    int yoff = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"crop-format", 1, NULL, 'c'},
            {"size",        1, NULL, 's'},
            {"fourcc",      1, NULL, 'p'},
            {"numframes",   1, NULL, 'n'},
            {"output",      1, NULL, 'o'},
            {"yuv4mpeg",    0, NULL, '4'},
            {"quiet",       0, NULL, 'q'},
            {"verbose",     0, NULL, 'v'},
            {"usage",       0, NULL, 'h'},
            {"help",        0, NULL, 'h'},
            {NULL,          0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "c:s:p:n:o:4qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 'c':
                cropping = optarg;
                break;

            case 's':
                format = optarg;
                break;

            case 'p':
                fccode = optarg;
                break;

            case '4':
                yuv4mpeg = 1;
                break;

            case 'n':
                numframes = atoi(optarg);
                if (numframes<=0)
                    rvexit("invalid value for numframes: %d", numframes);
                break;

            case 'o':
                outfile = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < RV_MAXFILES) {
            char *c = strrchr(argv[optind], ':');
            if (c==NULL)
                frames[numfiles] = "0-";
            else {
                *c = '\0';
                frames[numfiles] = c + 1;
            }
            filename[numfiles] = argv[optind++];
            numfiles++;
        } else
            rvexit("more than %d input files", numfiles);
    }

    /* sanity check frame specification */
    for (int i=0; i<numfiles; i++) {
        const char *p;
        int len = strlen(frames[i]);
        for (p=frames[i]; p<frames[i]+len; p++)
            if (!((*p>='0' && *p<='9') || *p=='-' || *p=='+' || *p==','))
                rvexit("illegal characters in frame specification \"%s\": %c", frames[i], *p);
    }

    /* parse cropping specification */
    if (!cropping) {
        rvexit("you must specify a crop format: -c <width>x<height>[+<xoff>[+<yoff>]]");
    } else {
        const char *start = cropping;
        char *end = NULL;
        crop_width = strtol(start, &end, 10);
        if (*end=='x') {
            start = ++end;
            crop_height = strtol(start, &end, 10);
            if (*end=='+' || *end=='-') {
                start = end;
                xoff = strtol(start, &end, 10);
                if (*end=='+' || *end=='-') {
                    start = end;
                    yoff = strtol(start, &end, 10);
                }
            }
        }
    }
    if (crop_width<=0 || crop_height<=0)
        rvexit("invalid crop format: %s", format);
    if (verbose>=2)
        rvmessage("cropping parameters: width=%d height=%d xoff=%d yoff=%d", crop_width, crop_height, xoff, yoff);

    /* convert fourcc string into integer */
    unsigned int fourcc = 0;
    if (fccode)
        fourcc = get_fourcc(fccode);

    /* open output file */
    if (outfile)
        fileout = fopen(outfile, "wb");
    if (fileout==NULL)
        rverror("failed to open output file \"%s\"", outfile);

    /* ignore SIGPIPE on output file */
    if (signal(SIGPIPE, SIG_IGN)==SIG_ERR)
        rverror("failed to set ignore on SIGPIPE");

    /* loop over input files */
    do {
        int frameno = 0;

        if (numfiles)
            filein = fopen(filename[fileindex], "rb");
        if (filein==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex]);

        /* parse yuv4mpeg stream header */
        inp_width = width;
        inp_height = height;
        if (force_raw==0 || divine_image_dims(&inp_width, &inp_height, NULL, NULL, NULL, format, filename[fileindex])<0) {
            /* must be yuv4mpeg format file */
            inp_raw = 0;
            if (read_y4m_stream_header(&inp_width, &inp_height, &fourcc, filein)<0)
                rverror("failed to read yuv4mpeg stream header");
        } else
            inp_raw = 1;

        /* determine input file chroma format */
        if (fourcc==0)
            fourcc = divine_pixel_format(filename[fileindex]);
        if (fourcc==0)
            fourcc = I420;
        if (fourcc!=I420 && fourcc!=I422)
            rvexit("unsupported pixel format: %s", fourccname(fourcc));

        /* get chroma subsampling from fourcc */
        int hsub, vsub;
        get_chromasub(fourcc, &hsub, &vsub);
        if (hsub==-1 || vsub==-1)
            rvexit("unknown pixel format: %s", fourccname(fourcc));

        /* initialise input buffer */
        struct rvimage inp(inp_width, inp_width, inp_height, hsub, vsub, 8);

        /* initialise output buffer*/
        struct rvimage out(crop_width, crop_width, crop_height, hsub, vsub, 8);

        /* write yuv4mpeg stream header */
        if (yuv4mpeg && fileindex==0)
            if (write_y4m_stream_header(crop_width, crop_height, get_chromaformat(fourcc), fileout)<0)
                break;

        /* main loop */
        for (frameno=0; (totframes<numframes || numframes<0) && !feof(filein) && !ferror(filein) && !ferror(fileout); frameno++)
        {
            int match = frame_match(frameno, frames[fileindex], verbose);
            /* look for early exit */
            if (match<0)
                break;

            /* read a frame */
            int read = inp.fread(filein, !inp_raw);
            if (read && match)
            {
                /* crop image */
                out.crop(inp, xoff, yoff);

                /* write cropped image */
                out.fwrite(fileout, yuv4mpeg);

                /* update statistics */
                totframes++;
            }
        }
        if (ferror(filein))
            rverror("failed to read from input");
        if (ferror(fileout) && errno!=EPIPE)
            rverror("failed to write to output");

        /* tidy up */
        if (numfiles)
            fclose(filein);

    } while (++fileindex<numfiles && !ferror(fileout));

    if (verbose>=0)
        rvmessage("cropped %d frames", totframes);

    return 0;
}
