/*
 * Description: Removes variable length codewords from a bitstream.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2008-11-20 19:36:17 $
 * Revision   : $Revision: 1.3 $
 * Copyright  : (c) 2007 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "getopt.h"

#include "rvutil.h"
#include "rvbits.h"

const char *appname;

void usage(int exitcode)
{
    fprintf(stderr, "%s: removes variable length codeword from a bitstream\n", appname);
    fprintf(stderr, "usage: %s [options] [<file>] [<file>...]\n", appname);
    fprintf(stderr, "\t-c <num> : codeword to remove (default: 0x000003)\n");
    fprintf(stderr, "\t-n <num> : size of codeword to remove in bits (default: 24)\n");
    fprintf(stderr, "\t-t <num> : number of bits to trim in codeword (default: 8)\n");
    fprintf(stderr, "\t-o <file>: write output to file\n");
    fprintf(stderr, "\t-q       : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "\t-v       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "\t--       : disable argument processing\n");
    fprintf(stderr, "\t-h       : print this usage message\n");
    exit(exitcode);
}

int main(int argc, char *argv[])
{
    char *filename[RV_MAXFILES] = {0};
    char *outfile = NULL;
    int fileindex = 0;
    int numfiles = 0;

    /* command line defaults */
    int codeword = 0x000003;
    int codesize = 24;
    int trimbits = 8;
    int verbose = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"codeword", 1, NULL, 'c'},
            {"codesize", 1, NULL, 'n'},
            {"trimbits", 1, NULL, 't'},
            {"output",   1, NULL, 'o'},
            {"quiet",    0, NULL, 'q'},
            {"verbose",  0, NULL, 'v'},
            {"usage",    0, NULL, 'h'},
            {"help",     0, NULL, 'h'},
            {NULL,       0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "c:n:t:o:qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 'c':
                // TODO parse hex arguments.
                codeword = atoi(optarg);
                break;

            case 'n':
                codesize = atoi(optarg);
                break;

            case 't':
                trimbits = atoi(optarg);
                break;

            case 'o':
                outfile = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < sizeof(filename)/sizeof(filename[0]))
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    /* open output file */
    struct bitbuf *outbb = initbits_filename(outfile, B_PUTBITS);
    if (outbb==NULL)
        rverror("failed to open output file \"%s\"", outfile);

    /* loop over input files */
    do {

        /* prepare to parse file */
        struct bitbuf *inbb = initbits_filename(filename[fileindex], B_GETBITS);
        if (inbb==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex]);

        /* main loop */
        while (!eofbits(inbb))
        {
            if (showbits(inbb, codesize)==codeword) {
                putbits(outbb, codesize-trimbits, getbits(inbb, codesize-trimbits));
                flushbits(inbb, trimbits);
                if (verbose>=1)
                    rvmessage("trimming %d bits at bit position %jd", trimbits, numbits(inbb));
            } else
                putbits(outbb, 8, getbits8(inbb)); // FIXME why 8?
        }

        freebits(inbb);

    } while (++fileindex<numfiles);

    /* tidy up */

    return 0;
}
