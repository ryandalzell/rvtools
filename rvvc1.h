
#ifndef _RVVC1_H_
#define _RVVC1_H_

#include "rvbits.h"

int parse_sequence_layer(struct bitbuf *bb, int *profile, int *level, int *width, int *height);

#endif
