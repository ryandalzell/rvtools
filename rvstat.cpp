/*
 * Description: Statistical funtions on a frame of video data.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-05-29 16:08:38 $
 * Revision   : $Revision: 1.16 $
 * Copyright  : (c) 2005,2009 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdlib.h>
#include <math.h>

#include "rvstat.h"
#include "rvutil.h"
#include "rvcodec.h"

static int skip_plane(int index, plane_t plane)
{
    if (index==0 && (plane==CHROMA_PLANE || plane==CB_PLANE || plane==CR_PLANE))
        return 1;
    if (index==1 && (plane==LUMA_PLANE || plane==CR_PLANE))
        return 1;
    if (index==2 && (plane==LUMA_PLANE || plane==CB_PLANE))
        return 1;
    return 0;
}

/*
 * This function calculates a variety of statistics of the source image
 * with respect to the reference image. The amount of computation
 * performed in given by the 'order' parameter, which in fact gives
 * the number of passes through the source image. This is based on
 * an assumption that the performance of the algorithms are dictated
 * by the data access times and not the computations themselves
 */

struct rvstats calculate_stats(struct rvimage &src, struct rvimage &ref, int order, plane_t plane)
{
    int i, x, y, samples = 0;

    /* initialise stats structure */
    struct rvstats stats = { 0, 0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };

    /* no sense in comparing an image with itself */
    if (src[0]==ref[0])
        return stats;

    /* determine smallest rectangle within both images */
    int width  = mmin(src.width, ref.width);
    int height = mmin(src.height, ref.height);

    /* calculate first order statistics */
    double sum_err = 0.0;
    double sum_sqe = 0.0;
    for (i=0; i<3; i++) {
        if (skip_plane(i, plane))
            continue;
        if (i>0 && src.hsub != ref.hsub)
            continue;
        if (i>0 && src.vsub != ref.vsub)
            continue;

        int hsub = (i==0? 1 : src.hsub);
        int vsub = (i==0? 1 : src.vsub);

        unsigned char *psrc = src[i];
        unsigned char *pref = ref[i];
        for (y=0; y<height/vsub; y++) {
            for (x=0; x<width/hsub; x++) {
                int d = psrc[x] - pref[x];
                sum_err += (double)d;
                sum_sqe += (double)(d*d);
                d = abs(d);
                if (d>stats.maxdiff) {
                    stats.maxdiff = d;
                    stats.maxdiffx = x;
                    stats.maxdiffy = y;
                }
            }
            psrc += src.pitch/hsub;
            pref += ref.pitch/hsub;
        }

        /* count total number of samples in analysis */
        samples += x*y;
    }
    stats.mse  = sum_sqe / (double)samples;
    stats.psnr = 20.0*log10(255.0/sqrt(stats.mse));
    stats.mean = sum_err / (double)samples;

    if (order<=1)
        return stats;

    /* calculate second order statistics */
    for (i=0; i<3; i++) {
        if (skip_plane(i, plane))
            continue;

        int hsub = (i==0? 1 : src.hsub);
        int vsub = (i==0? 1 : src.vsub);

        unsigned char *psrc = src[i];
        unsigned char *pref = ref[i];
        for (y=0; y<height/vsub; y++) {
            for (x=0; x<width/hsub; x++) {
                int d = psrc[x] - pref[x];
                double v = (double)d - stats.mean;
                stats.var += v*v;
            }
            psrc += src.pitch/hsub;
            pref += ref.pitch/hsub;
        }
    }
    stats.var /= (double)(samples-1);

    if (order==2)
        return stats;

    /* calculate third order statistics (complete waste of time, I know) */
    double sdc = 0.0;
    double sdev = sqrt(stats.var);
    for (i=0; i<3; i++) {
        if (skip_plane(i, plane))
            continue;

        int hsub = (i==0? 1 : src.hsub);
        int vsub = (i==0? 1 : src.vsub);

        unsigned char *psrc = src[i];
        unsigned char *pref = ref[i];
        for (y=0; y<height/vsub; y++) {
            for (x=0; x<width/hsub; x++) {
                int d = psrc[x] - pref[x];
                double s = ((double)(d) - stats.mean) / sdev;
                sdc += s*s*s;
            }
            psrc += src.pitch/hsub;
            pref += ref.pitch/hsub;
        }
    }
    stats.skew = sdc / (double)samples;

    if (order==3)
        return stats;

    /* prepare constant ssim statistics */
    const double L = pow(2.0,src.bpp)-1.0;
    const double c1 = 0.01*L*0.01*L;
    const double c2 = 0.03*L*0.03*L;

    /* calculate windowed statistics (ssim) */
    double ssim = 0.0;
    samples = 0;
    for (i=0; i<3; i++) {
        if (skip_plane(i, plane))
            continue;
        if (i>0 && src.hsub != ref.hsub)
            continue;
        if (i>0 && src.vsub != ref.vsub)
            continue;

        int hsub = (i==0? 1 : src.hsub);
        int vsub = (i==0? 1 : src.vsub);

        for (y=0; y<height/vsub-8; y++) {
            for (x=0; x<width/hsub-8; x++) {
                int j, k;

                unsigned char *psrc = src[i]+y*width/hsub+x;
                unsigned char *pref = ref[i]+y*width/hsub+x;

                /* calculate first order statistics for window */
                int sum_src = 0;
                int sum_ref = 0;
                for (j=0; j<8; j++) {
                    for (k=0; k<8; k++) {
                        sum_src += psrc[k];
                        sum_ref += pref[k];
                    }
                    psrc += src.pitch/hsub;
                    pref += ref.pitch/hsub;
                }

                double mean_src = (double)sum_src / 64.0;
                double mean_ref = (double)sum_ref / 64.0;

                psrc = src[i]+y*width/hsub+x;
                pref = ref[i]+y*width/hsub+x;

                /* calculate second order statistics */
                double var_src = 0.0;
                double var_ref = 0.0;
                double var_cor = 0.0;
                for (j=0; j<8; j++) {
                    for (k=0; k<8; k++) {
                        double vx = (double)psrc[k] - mean_src;
                        var_src += vx*vx;
                        double vy = (double)pref[k] - mean_ref;
                        var_ref += vy*vy;
                        var_cor += vx*vy;
                    }
                    psrc += src.pitch/hsub;
                    pref += ref.pitch/hsub;
                }
                var_src /= (double)(64-1);
                var_ref /= (double)(64-1);
                var_cor /= (double)(64-1);

                /* calculate universal quality index for window, as defined in:
                 * Z. Wang, and A. C. Bovik, "A universal image quality index",  IEEE Signal Processing Letters, vol. 9, no. 3, pp. 81-84, March 2002.*/
                //uqi += 4.0*var_cor*mean_src*mean_ref/(var_src + var_ref)/(mean_src*mean_src + mean_ref*mean_ref);

                /* calculate structural similarity index for window, as defined in:
                 * Z. Wang, A. C. Bovik, H. R. Sheikh and E. P. Simoncelli, "Image quality assessment: From error visibility to structural similarity," IEEE Transactions on Image Processing, vol. 13, no. 4, pp. 600-612, Apr. 2004. */
                ssim += (2.0*mean_src*mean_ref+c1)*(2.0*var_cor+c2)/(mean_src*mean_src+mean_ref*mean_ref+c1)/(var_src+var_ref+c2);
            }
        }

        /* count total number of samples in analysis */
        samples += x*y;

    }

    stats.ssim = ssim / (double)samples;

    return stats;
}

double calc_entropy(struct rvimage &src, plane_t plane)
{
    /* select which plane to analyse */
    int a, b;
    if (plane==CHROMA_PLANE) {
        a = src.width*src.height;
        b = src.width*src.height*3/2;
    } else if (plane==CB_PLANE) {
        a = src.width*src.height;
        b = src.width*src.height*5/4;
    } else if (plane==CR_PLANE) {
        a = src.width*src.height*5/4;
        b = src.width*src.height*3/2;
    } else if (plane==LUMA_PLANE) {
        a = 0;
        b = src.width*src.height;
    } else {
        a = 0;
        b = src.width*src.height*3/2;
    }

    /* do frequency binning on picture signal */
    int j;
    int count[256] = {0};
    for (j=a; j<b; j++)
        count[src[0][j]]++;

    /* calculate entropy of picture signal */
    double entropy = 0.0;
    for (j=0; j<256; j++) {
        if (count[j]) {
            double p = (double)count[j] / (double)(b-a);
            entropy += p * log(p);
        }
    }
    return -entropy / log(2);
}

double calc_mean(struct rvimage &src, plane_t plane)
{
    /* select which plane to analyse */
    int a, b;
    if (plane==CHROMA_PLANE) {
        a = src.width*src.height;
        b = src.width*src.height*3/2;
    } else if (plane==CB_PLANE) {
        a = src.width*src.height;
        b = src.width*src.height*5/4;
    } else if (plane==CR_PLANE) {
        a = src.width*src.height*5/4;
        b = src.width*src.height*3/2;
    } else { /* LUMA */
        a = 0;
        b = src.width*src.height;
    }

    /* calculate sum */
    int j;
    double sum = 0.0;
    for (j=a; j<b; j++)
        sum += src[0][j];

    return sum/(b-a);
}
