
#ifndef _RVMPEG2_H_
#define _RVMPEG2_H_

#include "rvutil.h"
#include "rvbits.h"
#include "rvcodec.h"

/* group_of_picture flags */
#define NEW_GOP 1
#define CLOSED_GOP 2
#define BROKEN_LINK 4

/* picture_coding_type */
enum pct_t {
    PCT_I = 1,
    PCT_P = 2,
    PCT_B = 3,
    PCT_D = 4
};

/* picture_structure */
enum pst_t {
    PST_TOP    = 1,
    PST_BOTTOM = 2,
    PST_FRAME  = 3
};

/* mv_format */
enum mv_format_t {
    MV_FORMAT_FRAME = 0,
    MV_FORMAT_FIELD = 1
};

/* mpeg2 picture_coding_extension */
struct rvm2vextension
{
    int f_code[2][2];
    int intra_dc_precision;
    enum pst_t picture_structure;
    int top_field_first;
    int frame_pred_frame_dct;
    int concealment_motion_vectors;
    int q_scale_type;
    int intra_vlc_format;
    int alternate_scan;
    int repeat_first_field;
    int chroma_420_type;
    int progressive_frame;
};

/* mpeg2 picture quant_matrix_extension */
struct rvm2vquantmatrix
{
    int load_intra_quantiser_matrix;
    int intra_quantiser_matrix[64];
    int load_non_intra_quantiser_matrix;
    int non_intra_quantiser_matrix[64];
    int load_chroma_intra_quantiser_matrix;
    int chroma_intra_quantiser_matrix[64];
    int load_chroma_non_intra_quantiser_matrix;
    int chroma_non_intra_quantiser_matrix[64];
};

/* mpeg2 macroblock */
struct rvm2vmacro
{
    short addr;
    char incr;
    char type;
    char frame_motion_type;
    char field_motion_type;
    char dct_type;
    char quantiser_scale_code;
    char motion_vertical_field_select[2][2];
    struct mv_t delta[2][2];
    struct mv_t vector[4][2];
    struct mv_t dmvector;
    unsigned char pattern_code;
    char skipped;
    coeff_t coeff[8][64];
    coeff_t pixel[8][64]; /* or pixel difference */
    short num_bits;
    short stuff_bits;
};

/* mpeg2 picture */
struct rvm2vpicture
{
    int decode_order;
    int temporal_reference;
    enum pct_t picture_coding_type;
    int vbv_delay;
    int full_pel_forward_vector;
    int forward_f_code;
    int full_pel_backward_vector;
    int backward_f_code;
    struct rvm2vextension extension;
    struct rvm2vquantmatrix quantmatrix;
    struct rvm2vmacro *macro;
    off_t bytes;
    off_t stuffing;
};

/* mpeg2 sequence */
struct rvm2vsequence
{
    int profile_and_level;
    int progressive_sequence;
    int chroma_format;
    int horizontal_size_value;
    int vertical_size_value;
    int aspect_ratio_information;
    int frame_rate_code;
    float frame_rate;
    int bit_rate;
    int vbv_buffer_size;
    int constrained_parameters_flag;
    int low_delay;
    int frame_rate_extension_n;
    int frame_rate_extension_d;
    int height_in_mbs;
    int width_in_mbs;
    int display_horizontal_size;
    int display_vertical_size;
    int load_intra_quantiser_matrix;
    int intra_quantiser_matrix[64];
    int load_non_intra_quantiser_matrix;
    int non_intra_quantiser_matrix[64];
    int load_chroma_intra_quantiser_matrix;
    int chroma_intra_quantiser_matrix[64];
    int load_chroma_non_intra_quantiser_matrix;
    int chroma_non_intra_quantiser_matrix[64];
    int mpeg1;
    size_t size_pic_array;
    struct rvm2vpicture **picture;
    struct rvm2vpicture **picture2;
};

/*
 * mpeg2 lookup tables
 */

extern const char *aspect_ratio_table[5];
extern const float frame_rate_table[9];

/*
 * mpeg2 function prototypes
 */

void describe_profile(int profile_and_level, const char **p, const char **l);
const char *describe_picture(struct rvm2vpicture *picture);

int frame_picture(const struct rvm2vpicture *picture);
int field_picture(const struct rvm2vpicture *picture);
int top_field_picture(const struct rvm2vpicture *picture);
int bottom_field_picture(const struct rvm2vpicture *picture);
int first_field_picture(const struct rvm2vpicture *picture, const struct rvm2vpicture *prev_picture);
int second_field_picture(const struct rvm2vpicture *picture, const struct rvm2vpicture *prev_picture);

int macroblock_intra(int macroblock_type);
int macroblock_pattern(int macroblock_type);
int macroblock_motion_backward(int macroblock_type);
int macroblock_motion_forward(int macroblock_type);
int macroblock_quant(int macroblock_type);
int macroblock_error(int macroblock_type);

int motion_vector_count(int frame_motion_type, int field_motion_type);
enum mv_format_t mv_format(int frame_motion_type, int field_motion_type);
int dmv(int frame_motion_type, int field_motion_type);

int picture_repeat(int progressive_sequence, int progressive_frame, int repeat_first_field, int top_field_first);

int max_bit_rate_profile_level(int profile, int level);
int max_vbv_buffer_size_profile_level(int profile, int level);

struct rvm2vpicture *alloc_m2v_picture();
void free_m2v_picture(struct rvm2vpicture* picture);
void free_m2v_sequence(struct rvm2vsequence* sequence);

int parse_m2v_sequence(struct bitbuf *bb, struct rvm2vsequence *sequence);
void parse_m2v_user_data(struct bitbuf *bb, int verbose);
int parse_m2v_sequence_extension(struct bitbuf *bb, struct rvm2vsequence *seq);
int parse_m2v_sequence_display_extension(struct bitbuf *bb, struct rvm2vsequence *seq);
int parse_m2v_quant_matrix_extension(struct bitbuf *bb, struct rvm2vquantmatrix *mat);
int parse_m2v_group_of_pictures(struct bitbuf *bb, int *gop_flags);
struct rvm2vpicture *parse_m2v_picture_header(struct bitbuf *bb);
int parse_m2v_picture_extension(struct bitbuf *bb, struct rvm2vextension *ext);
struct rvm2vsequence *parse_m2v_headers(struct bitbuf *bb, int verbose);
int find_m2v_picture_end(struct bitbuf *bb, struct rvm2vsequence *sequence, int picno);
int find_m2v_next_picture(struct bitbuf *bb, struct rvm2vsequence *sequence, int picno, int *gop_flags, int verbose);
void find_m2v_motion_vector_range(struct rvm2vpicture *picture, struct rvm2vsequence *sequence, int *maxx, int *minx, int *maxy, int *miny);
struct rvm2vpicture *parse_m2v_picture_header_and_extensions(struct bitbuf *bb, struct rvm2vsequence *sequence, int picno, int verbose);
struct rvm2vpicture *parse_m2v_picture(struct bitbuf *bb, struct rvm2vsequence *sequence, int picno, int verbose);
sample_t *decode_m2v(struct rvm2vpicture *picture, struct rvm2vsequence *sequence, sample_t *forward_reference, sample_t *backward_reference, int picno, int refidct);

#endif
