#ifndef _RVIMAGE_H_
#define _RVIMAGE_H_

#include <stdio.h>

#include "rvm2v.h"
#include "rvlogfile.h"
#include "rvbits.h"

/*
 * image drawing colours
 */
#define YUV(Y, U, V) ((Y)<<16) | ((U)<<8) | ((V)<<0)
#define RGB(R, G, B) YUV((77*(R)+150*(G)+29*(B)), (-38*(R)-74*(G)+112*(B)), (157*(R)-132*(G)-26*(B)))

#define DARK  0x40
#define LIGHT 0x10

#define BLACK        YUV(0x00, 0x80, 0x80)
#define GRAY         YUV(0x80, 0x80, 0x80)
#define WHITE        YUV(0xFF, 0x80, 0x80)
#define ORANGE       YUV(0x00, 0x80-DARK,  0x80+DARK)
#define LIGHT_ORANGE YUV(0x00, 0x80-LIGHT, 0x80+LIGHT)
#define RED          YUV(0xFF, 0x80,       0x80+DARK)
#define LIGHT_RED    YUV(0xFF, 0x80-DARK,  0x80+DARK) // duplicate of ORANGE
#define PURPLE       YUV(0x00, 0x80+DARK,  0x80+DARK)
#define LIGHT_PURPLE YUV(0x00, 0x80+LIGHT, 0x80+LIGHT)
#define YELLOW       YUV(0x00, 0x00,       152)
#define LIGHT_YELLOW YUV(0x00, 0x00,       152-LIGHT)
#define TEST         YUV(0x00, 0xFF,       100)
#define VIOLET       YUV(0x00, 0x80,       0x80+DARK) // duplicate of RED
#define GREEN        YUV(0x00, 0x80-DARK,  0x80-DARK)
#define LIGHT_GREEN  YUV(0x00, 0x80-LIGHT, 0x80-LIGHT)
#define CYAN         YUV(0x00, 0x80,       0x80-DARK)
#define BLUE         YUV(0x00, 0x80+DARK,  0x80-DARK)
#define LIGHT_BLUE   YUV(0x00, 0x80+LIGHT, 0x80-LIGHT)

/*
 * image resampling algorithms
 */
enum algo_t {
    NOALGO,
    NEAREST,
    BILINEAR,
    BICUBIC,
    BARTLETT,
    VONHANN,
    HAMMING,
    BLACKMAN,
    BLACKMANHARRIS,
    BLACKMANNUTTALL,
    KAISER,
};

struct rvimage {
public:
    rvimage();
    rvimage(int width, int pitch, int height, int hsub, int vsub, int bpp);
    rvimage(int width, int pitch, int height, fourcc_t fourcc);
    rvimage(const struct rvimage &image);
    ~rvimage();

    /* image initialisation (and allocation) */
    void init();
    void init(unsigned char *base, int width, int pitch, int height, int hsub, int vsub, int bpp);
    void init(int width, int pitch, int height, int hsub, int vsub, int bbp);
    void init(int width, int pitch, int height, fourcc_t fourcc);
    void init(const struct rvimage &image);
    void clear();
    void clear_chroma();

    /* image copying */
    void copy(const struct rvimage &src);
    void copy(const struct rvimage &src, int dstx, int dsty, int srcx, int srcy, int width, int height);
    void copy_luma(const struct rvimage &src, int dstx, int dsty, int srcx, int srcy, int width, int height);
    void copy_chroma(const struct rvimage &src, int dstx, int dsty, int srcx, int srcy, int width, int height);

    /* data access */
    unsigned char *data() const { return plane[0]; }

    /* type conversion */
    void operator= (unsigned char *data) { this->plane[0] = data; this->plane[1] = data + width*height; this->plane[2] = data + width*height + width*height/hsub/vsub; }
    unsigned char *operator[] (unsigned int i) { return plane[i]; };

public:
    /* file io */
    size_t fread(FILE *file, bool yuv4mpeg=0);
    size_t fwrite(FILE *file, bool yuv4mpeg=0);
    size_t readbits(struct bitbuf *bb, bool yuv4mpeg=0);
    size_t writebits(struct bitbuf *bb, bool yuv4mpeg=0);

    /* image drawing */
    void shade_image(int x, int y, int w, int h, const int colour);
    void brighten_image(int x, int y, int w, int h, const int offset);
    void draw_line(int x, int y, int dx, int dy, const int colour);
    void draw_dot(int x, int y, const int colour);
    void draw_box(int x, int y, int dx, int dy, const int colour);
    void draw_half_box(int x, int y, int dx, int dy, const int colour);
    void draw_4x4box(int x, int y, int colour);

    /* image manipulation */
    void crop(const struct rvimage &src, int xoff, int yoff);
    void scale(const struct rvimage &src);
    void scale_config(algo_t algo, int taps, int phases);
    void split(const struct rvimage &src, fourcc_t fourcc);
    void shift(const struct rvimage &src, int xshift, int yshift);

private:
    unsigned char *plane[3];    /* pointers to luma/chroma planes NOTE might be external */
    unsigned char *buf;         /* internally allocated buffer */

public: // TODO make private.
    int width;
    int pitch;
    int height;
    int hsub;
    int vsub;
    int bpp;
    size_t size;

private:
    /* image resampling parameters */
    algo_t algo;
    int phases;
    int taps;

};

/* rvimage.c */
void draw_m2v_overlay(struct rvimage &image, struct rvm2vpicture *toppicture, struct rvm2vpicture *botpicture);
void draw_m2v_qpmap(struct rvimage &image, struct rvm2vpicture *toppicture, struct rvm2vpicture *botpicture);
void draw_h264_overlay(struct rvimage &image, struct rv264macro macro[], int mbaff, int field_pic_flag, int bottom_field_flag);
void draw_265_overlay(struct rvimage &image, const struct rvhevcpicture *pic);

#endif
