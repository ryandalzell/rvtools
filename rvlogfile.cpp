/*
 * Description: Parse decoder trace files from macroblock information.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-02-05 14:25:37 $
 * Revision   : $Revision: 1.23 $
 * Copyright  : (c) 2005 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <string.h>
#include <math.h>

#include "rvutil.h"
#include "rvlogfile.h"

/* sort an array of rv264macro structs into picture then macroblock order */
int seqsort(const void *a, const void *b)
{
    struct rv264macro *pa = (struct rv264macro *)a;
    struct rv264macro *pb = (struct rv264macro *)b;

    /* first sort by idr sub-sequence number */
    if (pa->idr_num < pb->idr_num)
        return -1;
    else if (pa->idr_num > pb->idr_num)
        return 1;
    /* then sort by picture order */
    else if (pa->pic_order_cnt < pb->pic_order_cnt)
        return -1;
    else if (pa->pic_order_cnt > pb->pic_order_cnt)
        return 1;
    /* then sort by macroblock number */
    else if (pa->mb_addr < pb->mb_addr)
        return -1;
    else if (pa->mb_addr > pb->mb_addr)
        return 1;
    else
        return 0; /* should not happen */
}

/* return the value from a logfile line that has the form: <string><stuff>(<value>) */
static int matchparen(const char *line, const char *match, int *value)
{
    const char *p = strstr(line, match);
    if (p && *(p-1)==' ') {
        p = strchr(p+strlen(match), '(');
        if (sscanf(p, "( %d)", value))
            return 1;
    }
    return 0;
}

/* return the first value from a logfile line that has the form: <string><space><value> */
static int matchvalue(const char *line, const char *match, int *value)
{
    const char *p = strstr(line, match);
    if (p && *(p-1)==' ')
        if (sscanf(p+strlen(match), " %d", value))
            return 1;
    return 0;
}

/* return the last value from a logfile line that has the form: <string><stuff>[(]<value>[)] */
static int matchlast(const char *line, const char *match, int *value)
{
    const char *p = strstr(line, match);
    if (p && *(p-1)==' ') {
        /* search from end of line for numerical value */
        for (p=line+strlen(line); *p<'0' || *p>'9'; p--);
        for (; (*p>='0' && *p<='9') || *p=='-'; p--);
        if (sscanf(p+1, " %d", value))
            return 1;
    }
    return 0;
}

/* parse the sequence parameter header from a jm h.264 decoder logfile */
int parse_jmlog_header(FILE *trace, struct rv264seq_parameter_set *sps, struct rv264pic_parameter_set *pps)
{
    char line[256];

    /* first clear the parameter sets */
    memset(sps, 0, sizeof(struct rv264seq_parameter_set));
    memset(pps, 0, sizeof(struct rv264pic_parameter_set));

    /* loop over lines in trace file */
    while (fgets(line, sizeof(line), trace)) {
        int value;

        /* look for sequence parameter set syntax elements */
        if (matchparen(line, "chroma_format_idc", &value))
            sps->chroma_format_idc = value;
        if (matchparen(line, "residual_colour_transform_flag", &value))
            sps->separate_colour_plane_flag = value;
        if (matchparen(line, "bit_depth_luma_minus8", &value))
            sps->bit_depth_luma_minus8 = value;
        if (matchparen(line, "bit_depth_chroma_minus8", &value))
            sps->bit_depth_chroma_minus8 = value;
        if (matchparen(line, "qpprime_y_zero_transform_bypass_flag", &value))
            sps->qpprime_y_zero_transform_bypass_flag = value;
        if (matchparen(line, "log2_max_frame_num_minus4", &value))
            sps->log2_max_frame_num_minus4 = value;
        if (matchparen(line, "pic_order_cnt_type", &value))
            sps->pic_order_cnt_type = value;
        if (matchparen(line, "log2_max_pic_order_cnt_lsb_minus4", &value))
            sps->log2_max_pic_order_cnt_lsb_minus4 = value;
        if (matchparen(line, "delta_pic_order_always_zero_flag", &value))
            sps->delta_pic_order_always_zero_flag = value;
        if (matchparen(line, "offset_for_non_ref_pic", &value))
            sps->offset_for_non_ref_pic = value;
        if (matchparen(line, "offset_for_top_to_bottom_field", &value))
            sps->offset_for_top_to_bottom_field = value;
        if (matchparen(line, "num_ref_frames_in_pic_order_cnt_cycle", &value))
            sps->num_ref_frames_in_pic_order_cnt_cycle = value;
        if (matchparen(line, "num_ref_frames", &value))
            sps->max_num_ref_frames = value;
        if (matchparen(line, "gaps_in_frame_num_value_allowed_flag", &value))
            sps->gaps_in_frame_num_value_allowed_flag = value;
        if (matchparen(line, "pic_width_in_mbs_minus1", &value))
            sps->pic_width_in_mbs_minus1 = value;
        if (matchparen(line, "pic_height_in_map_units_minus1", &value))
            sps->pic_height_in_map_units_minus1 = value;
        if (matchparen(line, "frame_mbs_only_flag", &value))
            sps->frame_mbs_only_flag = value;
        if (matchparen(line, "mb_adaptive_frame_field_flag", &value))
            sps->mb_adaptive_frame_field_flag = value;
        if (matchparen(line, "direct_8x8_inference_flag", &value))
            sps->direct_8x8_inference_flag = value;

        /* look for picture parameter set syntax elements */
        if (matchparen(line, "entropy_coding_mode_flag", &value))
            pps->entropy_coding_mode_flag = value;
        if (matchparen(line, "pic_order_present_flag", &value))
            pps->pic_order_present_flag = value;
        if (matchparen(line, "num_slice_groups_minus1", &value))
            pps->num_slice_groups_minus1 = value;
        if (matchparen(line, "slice_group_map_type", &value))
            pps->slice_group_map_type = value;
        if (matchparen(line, "num_ref_idx_l0_active_minus1", &value))
            pps->num_ref_idx_l0_active_minus1 = value;
        if (matchparen(line, "num_ref_idx_l1_active_minus1", &value))
            pps->num_ref_idx_l1_active_minus1 = value;
        if (matchparen(line, "weighted_pred_flag", &value))
            pps->weighted_pred_flag = value;
        if (matchparen(line, "weighted_bipred_idc", &value))
            pps->weighted_bipred_idc = value;
        if (matchparen(line, "pic_init_qp_minus26", &value))
            pps->pic_init_qp_minus26 = value;
        if (matchparen(line, "pic_init_qs_minus26", &value))
            pps->pic_init_qs_minus26 = value;
        if (matchparen(line, "chroma_qp_index_offset", &value))
            pps->chroma_qp_index_offset = value;
        if (matchparen(line, "deblocking_filter_control_present_flag", &value))
            pps->deblocking_filter_control_present_flag = value;
        if (matchparen(line, "constrained_intra_pred_flag", &value))
            pps->constrained_intra_pred_flag = value;
        if (matchparen(line, "redundant_pic_cnt_present_flag", &value))
            pps->redundant_pic_cnt_present_flag = value;
        if (matchparen(line, "transform_8x8_mode_flag", &value))
            pps->transform_8x8_mode_flag = value;

        /* return when all required information has been read */
        if (matchparen(line, "first_mb_in_slice", &value)) {
            sps->occupied = 1;
            pps->occupied = 1;
            return 0;
        }
    }
    if (ferror(trace))
        rverror("failed to read from trace file");
    return -1;
}

/* parse a number of macroblocks from a jm h.264 decoder logfile */
int parse_jmlog_macros(FILE *trace, int num_macros, struct rv264macro macro[], struct rv264seq_parameter_set *sps, struct rv264pic_parameter_set *pps)
{
    char line[256];

    /* syntax elements */
    slice_type_t slice_type = SLICE_P;
    int mvd_l0[32] = {0};
    int mvd_l1[32] = {0};
    int ref_idx_l0[4] = {0};
    int ref_idx_l1[4] = {0};

    /* syntax element array indicies */
    int index_sub = 0;
    int index_ref_l0 = 0;
    int index_ref_l1 = 0;
    int index_mvd_l0 = 0;
    int index_mvd_l1 = 0;
    int index_block = 0;

    /* loop over lines in trace file */
    int n = -1;
    int idr_num = -1, prev_type = -1;
    while (fgets(line, sizeof(line), trace)) {
        int slice_type_int;

        /* slice type */
        if (matchparen(line, "slice_type", &slice_type_int))
            continue;
        slice_type = (slice_type_t)slice_type_int;

        /* start of next macroblock in frame */
        int pic_order_cnt, mb_addr, slice_num, type;
        if (sscanf(line, "*********** POC: %d (I/P) MB: %d Slice: %d Type %d **********", &pic_order_cnt, &mb_addr, &slice_num, &type)==4) {
            int dup_ref_l0 = 0;
            int dup_ref_l1 = 0;
            int dup_mvd_l0 = 0;
            int dup_mvd_l1 = 0;

            /* finish storing data into previous macroblock */

            /* arrange and duplicate motion vectors */
            if (slice_is_p(slice_type) || slice_is_b(slice_type)) {
                int p, s;

                /* duplicate across partitions */
                switch (macro[n].mb_type) {
                    case P_L0_16x16:
                    case B_L0_16x16:
                    case B_L1_16x16:
                    case B_Bi_16x16:
                        for (p=0; p<4; p++) {
                            macro[n].ref_idx_l0[p] = ref_idx_l0[0];
                            macro[n].ref_idx_l1[p] = ref_idx_l1[0];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l0[0];
                                macro[n].mvd[0][p][s][1] = mvd_l0[1];
                                macro[n].mvd[0][p][s][0] = mvd_l1[0];
                                macro[n].mvd[0][p][s][1] = mvd_l1[1];
                            }
                        }
                        break;

                    case P_L0_L0_16x8:
                    case B_L0_L0_16x8:
                        for (p=0; p<2; p++) {
                            macro[n].ref_idx_l0[p] = ref_idx_l0[0];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l0[0];
                                macro[n].mvd[0][p][s][1] = mvd_l0[1];
                            }
                        }
                        for (p=2; p<4; p++) {
                            macro[n].ref_idx_l0[p] = ref_idx_l0[1];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l0[2];
                                macro[n].mvd[0][p][s][1] = mvd_l0[3];
                            }
                        }
                        break;

                    case B_L1_L1_16x8:
                        for (p=0; p<2; p++) {
                            macro[n].ref_idx_l1[p] = ref_idx_l1[0];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l1[0];
                                macro[n].mvd[0][p][s][1] = mvd_l1[1];
                            }
                        }
                        for (p=2; p<4; p++) {
                            macro[n].ref_idx_l1[p] = ref_idx_l1[1];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l1[2];
                                macro[n].mvd[0][p][s][1] = mvd_l1[3];
                            }
                        }
                        break;

                    case B_L0_L1_16x8:
                        for (p=0; p<2; p++) {
                            macro[n].ref_idx_l0[p] = ref_idx_l0[0];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l0[0];
                                macro[n].mvd[0][p][s][1] = mvd_l0[1];
                            }
                        }
                        for (p=2; p<4; p++) {
                            macro[n].ref_idx_l1[p] = ref_idx_l1[0];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l1[0];
                                macro[n].mvd[0][p][s][1] = mvd_l1[1];
                            }
                        }
                        break;

                    case B_L1_L0_16x8:
                        for (p=0; p<2; p++) {
                            macro[n].ref_idx_l1[p] = ref_idx_l1[0];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l1[0];
                                macro[n].mvd[0][p][s][1] = mvd_l1[1];
                            }
                        }
                        for (p=2; p<4; p++) {
                            macro[n].ref_idx_l0[p] = ref_idx_l0[0];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l0[0];
                                macro[n].mvd[0][p][s][1] = mvd_l0[1];
                            }
                        }
                        break;

                    case B_L0_Bi_16x8:
                        for (p=0; p<2; p++) {
                            macro[n].ref_idx_l0[p] = ref_idx_l0[0];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l0[0];
                                macro[n].mvd[0][p][s][1] = mvd_l0[1];
                            }
                        }
                        for (p=2; p<4; p++) {
                            macro[n].ref_idx_l0[p] = ref_idx_l0[1];
                            macro[n].ref_idx_l1[p] = ref_idx_l1[0];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l0[2];
                                macro[n].mvd[0][p][s][1] = mvd_l0[3];
                                macro[n].mvd[0][p][s][0] = mvd_l1[0];
                                macro[n].mvd[0][p][s][1] = mvd_l1[1];
                            }
                        }
                        break;

                    case B_L1_Bi_16x8:
                        for (p=0; p<2; p++) {
                            macro[n].ref_idx_l1[p] = ref_idx_l1[0];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l1[0];
                                macro[n].mvd[0][p][s][1] = mvd_l1[1];
                            }
                        }
                        for (p=2; p<4; p++) {
                            macro[n].ref_idx_l0[p] = ref_idx_l0[0];
                            macro[n].ref_idx_l1[p] = ref_idx_l1[1];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l0[0];
                                macro[n].mvd[0][p][s][1] = mvd_l0[1];
                                macro[n].mvd[0][p][s][0] = mvd_l1[2];
                                macro[n].mvd[0][p][s][1] = mvd_l1[3];
                            }
                        }
                        break;

                    case B_Bi_L0_16x8:
                    case B_Bi_L1_16x8:
                    case B_Bi_Bi_16x8:
                        for (p=0; p<2; p++) {
                            macro[n].ref_idx_l0[p] = ref_idx_l0[0];
                            macro[n].ref_idx_l1[p] = ref_idx_l1[0];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l0[0];
                                macro[n].mvd[0][p][s][1] = mvd_l0[1];
                                macro[n].mvd[0][p][s][0] = mvd_l1[0];
                                macro[n].mvd[0][p][s][1] = mvd_l1[1];
                            }
                        }
                        for (p=2; p<4; p++) {
                            macro[n].ref_idx_l0[p] = ref_idx_l0[1];
                            macro[n].ref_idx_l1[p] = ref_idx_l1[1];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l0[2];
                                macro[n].mvd[0][p][s][1] = mvd_l0[3];
                                macro[n].mvd[0][p][s][0] = mvd_l1[2];
                                macro[n].mvd[0][p][s][1] = mvd_l1[3];
                            }
                        }
                        break;

                    case P_L0_L0_8x16:
                    case B_L0_L0_8x16:
                        for (p=0; p<3; p+=2) {
                            macro[n].ref_idx_l0[p] = ref_idx_l0[0];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l0[0];
                                macro[n].mvd[0][p][s][1] = mvd_l0[1];
                            }
                        }
                        for (p=1; p<4; p+=2) {
                            macro[n].ref_idx_l0[p] = ref_idx_l0[1];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l0[2];
                                macro[n].mvd[0][p][s][1] = mvd_l0[3];
                            }
                        }
                        break;

                    case B_L1_L1_8x16:
                        for (p=0; p<3; p+=2) {
                            macro[n].ref_idx_l1[p] = ref_idx_l1[0];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l1[0];
                                macro[n].mvd[0][p][s][1] = mvd_l1[1];
                            }
                        }
                        for (p=1; p<4; p+=2) {
                            macro[n].ref_idx_l1[p] = ref_idx_l1[1];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l1[2];
                                macro[n].mvd[0][p][s][1] = mvd_l1[3];
                            }
                        }
                        break;

                    case B_L0_L1_8x16:
                        for (p=0; p<3; p+=2) {
                            macro[n].ref_idx_l0[p] = ref_idx_l0[0];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l0[0];
                                macro[n].mvd[0][p][s][1] = mvd_l0[1];
                            }
                        }
                        for (p=1; p<4; p+=2) {
                            macro[n].ref_idx_l1[p] = ref_idx_l1[0];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l1[0];
                                macro[n].mvd[0][p][s][1] = mvd_l1[1];
                            }
                        }
                        break;

                    case B_L1_L0_8x16:
                        for (p=0; p<3; p+=2) {
                            macro[n].ref_idx_l1[p] = ref_idx_l1[0];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l1[0];
                                macro[n].mvd[0][p][s][1] = mvd_l1[1];
                            }
                        }
                        for (p=1; p<4; p+=2) {
                            macro[n].ref_idx_l0[p] = ref_idx_l0[0];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l0[0];
                                macro[n].mvd[0][p][s][1] = mvd_l0[1];
                            }
                        }
                        break;

                    case B_L0_Bi_8x16:
                        for (p=0; p<3; p+=2) {
                            macro[n].ref_idx_l0[p] = ref_idx_l0[0];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l0[0];
                                macro[n].mvd[0][p][s][1] = mvd_l0[1];
                            }
                        }
                        for (p=1; p<4; p+=2) {
                            macro[n].ref_idx_l0[p] = ref_idx_l0[1];
                            macro[n].ref_idx_l1[p] = ref_idx_l1[0];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l0[2];
                                macro[n].mvd[0][p][s][1] = mvd_l0[3];
                                macro[n].mvd[0][p][s][0] = mvd_l1[0];
                                macro[n].mvd[0][p][s][1] = mvd_l1[1];
                            }
                        }
                        break;

                    case B_L1_Bi_8x16:
                        for (p=0; p<3; p+=2) {
                            macro[n].ref_idx_l1[p] = ref_idx_l1[0];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l1[0];
                                macro[n].mvd[0][p][s][1] = mvd_l1[1];
                            }
                        }
                        for (p=1; p<4; p+=2) {
                            macro[n].ref_idx_l0[p] = ref_idx_l0[0];
                            macro[n].ref_idx_l1[p] = ref_idx_l1[1];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l0[0];
                                macro[n].mvd[0][p][s][1] = mvd_l0[1];
                                macro[n].mvd[0][p][s][0] = mvd_l1[2];
                                macro[n].mvd[0][p][s][1] = mvd_l1[3];
                            }
                        }
                        break;

                    case B_Bi_L0_8x16:
                    case B_Bi_L1_8x16:
                    case B_Bi_Bi_8x16:
                        for (p=0; p<3; p+=2) {
                            macro[n].ref_idx_l0[p] = ref_idx_l0[0];
                            macro[n].ref_idx_l1[p] = ref_idx_l1[0];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l0[0];
                                macro[n].mvd[0][p][s][1] = mvd_l0[1];
                                macro[n].mvd[0][p][s][0] = mvd_l1[0];
                                macro[n].mvd[0][p][s][1] = mvd_l1[1];
                            }
                        }
                        for (p=1; p<4; p+=2) {
                            macro[n].ref_idx_l0[p] = ref_idx_l0[1];
                            macro[n].ref_idx_l1[p] = ref_idx_l1[1];
                            for (s=0; s<4; s++) {
                                macro[n].mvd[0][p][s][0] = mvd_l0[2];
                                macro[n].mvd[0][p][s][1] = mvd_l0[3];
                                macro[n].mvd[0][p][s][0] = mvd_l1[2];
                                macro[n].mvd[0][p][s][1] = mvd_l1[3];
                            }
                        }
                        break;

                    case P_8x8:
                    case P_8x8ref0:
                        for (p=0; p<4; p++) {
                            if (macro[n].mb_type!=P_8x8ref0)
                                macro[n].ref_idx_l0[p] = ref_idx_l0[dup_ref_l0++];
                            switch (macro[n].sub_mb_type[p]) {
                                case P_L0_8x8:
                                    for (s=0; s<4; s++) {
                                        macro[n].mvd[0][p][s][0] = mvd_l0[dup_mvd_l0];
                                        macro[n].mvd[0][p][s][1] = mvd_l0[dup_mvd_l0+1];
                                    }
                                    dup_mvd_l0 += 2;
                                    break;

                                case P_L0_8x4:
                                    for (s=0; s<2; s++) {
                                        macro[n].mvd[0][p][s][0] = mvd_l0[dup_mvd_l0];
                                        macro[n].mvd[0][p][s][1] = mvd_l0[dup_mvd_l0+1];
                                    }
                                    dup_mvd_l0 += 2;
                                    for (s=2; s<4; s++) {
                                        macro[n].mvd[0][p][s][0] = mvd_l0[dup_mvd_l0];
                                        macro[n].mvd[0][p][s][1] = mvd_l0[dup_mvd_l0+1];
                                    }
                                    dup_mvd_l0 += 2;
                                    break;

                                case P_L0_4x8:
                                    for (s=0; s<3; s+=2) {
                                        macro[n].mvd[0][p][s][0] = mvd_l0[dup_mvd_l0];
                                        macro[n].mvd[0][p][s][1] = mvd_l0[dup_mvd_l0+1];
                                    }
                                    dup_mvd_l0 += 2;
                                    for (s=1; s<4; s+=2) {
                                        macro[n].mvd[0][p][s][0] = mvd_l0[dup_mvd_l0];
                                        macro[n].mvd[0][p][s][1] = mvd_l0[dup_mvd_l0+1];
                                    }
                                    dup_mvd_l0 += 2;
                                    break;

                                case P_L0_4x4:
                                    for (s=0; s<4; s++) {
                                        macro[n].mvd[0][p][s][0] = mvd_l0[dup_mvd_l0++];
                                        macro[n].mvd[0][p][s][1] = mvd_l0[dup_mvd_l0++];
                                    }
                                    break;

                                default:
                                    break;
                            }
                        }
                        break;

                    case B_8x8:
                        dup_mvd_l0 = 0;
                        dup_mvd_l1 = 0;
                        for (p=0; p<4; p++) {
                            switch (macro[n].sub_mb_type[p]) {
                                case B_Direct_8x8:
                                    break;

                                case B_L0_8x8:
                                    macro[n].ref_idx_l0[p] = ref_idx_l0[dup_ref_l0++];
                                    for (s=0; s<4; s++) {
                                        macro[n].mvd[0][p][s][0] = mvd_l0[dup_mvd_l0];
                                        macro[n].mvd[0][p][s][1] = mvd_l0[dup_mvd_l0+1];
                                    }
                                    dup_mvd_l0 += 2;
                                    break;

                                case B_L1_8x8:
                                    macro[n].ref_idx_l1[p] = ref_idx_l1[dup_ref_l1++];
                                    for (s=0; s<4; s++) {
                                        macro[n].mvd[0][p][s][0] = mvd_l1[dup_mvd_l1];
                                        macro[n].mvd[0][p][s][1] = mvd_l1[dup_mvd_l1+1];
                                    }
                                    dup_mvd_l1 += 2;
                                    break;

                                case B_Bi_8x8:
                                    macro[n].ref_idx_l0[p] = ref_idx_l0[dup_ref_l0++];
                                    macro[n].ref_idx_l1[p] = ref_idx_l1[dup_ref_l1++];
                                    for (s=0; s<4; s++) {
                                        macro[n].mvd[0][p][s][0] = mvd_l0[dup_mvd_l0];
                                        macro[n].mvd[0][p][s][1] = mvd_l0[dup_mvd_l0+1];
                                        macro[n].mvd[0][p][s][0] = mvd_l1[dup_mvd_l1];
                                        macro[n].mvd[0][p][s][1] = mvd_l1[dup_mvd_l1+1];
                                    }
                                    dup_mvd_l0 += 2;
                                    dup_mvd_l1 += 2;
                                    break;

                                case B_L0_8x4:
                                    macro[n].ref_idx_l0[p] = ref_idx_l0[dup_ref_l0++];
                                    for (s=0; s<2; s++) {
                                        macro[n].mvd[0][p][s][0] = mvd_l0[dup_mvd_l0];
                                        macro[n].mvd[0][p][s][1] = mvd_l0[dup_mvd_l0+1];
                                    }
                                    dup_mvd_l0 += 2;
                                    for (s=2; s<4; s++) {
                                        macro[n].mvd[0][p][s][0] = mvd_l0[dup_mvd_l0];
                                        macro[n].mvd[0][p][s][1] = mvd_l0[dup_mvd_l0+1];
                                    }
                                    dup_mvd_l0 += 2;
                                    break;

                                case B_L1_8x4:
                                    macro[n].ref_idx_l1[p] = ref_idx_l1[dup_ref_l1++];
                                    for (s=0; s<2; s++) {
                                        macro[n].mvd[0][p][s][0] = mvd_l1[dup_mvd_l1];
                                        macro[n].mvd[0][p][s][1] = mvd_l1[dup_mvd_l1+1];
                                    }
                                    dup_mvd_l1 += 2;
                                    for (s=2; s<4; s++) {
                                        macro[n].mvd[0][p][s][0] = mvd_l1[dup_mvd_l1];
                                        macro[n].mvd[0][p][s][1] = mvd_l1[dup_mvd_l1+1];
                                    }
                                    dup_mvd_l1 += 2;
                                    break;

                                case B_Bi_8x4:
                                    macro[n].ref_idx_l0[p] = ref_idx_l0[dup_ref_l0++];
                                    macro[n].ref_idx_l1[p] = ref_idx_l1[dup_ref_l1++];
                                    for (s=0; s<2; s++) {
                                        macro[n].mvd[0][p][s][0] = mvd_l0[dup_mvd_l0];
                                        macro[n].mvd[0][p][s][1] = mvd_l0[dup_mvd_l0+1];
                                        macro[n].mvd[0][p][s][0] = mvd_l1[dup_mvd_l1];
                                        macro[n].mvd[0][p][s][1] = mvd_l1[dup_mvd_l1+1];
                                    }
                                    dup_mvd_l0 += 2;
                                    dup_mvd_l1 += 2;
                                    for (s=2; s<4; s++) {
                                        macro[n].mvd[0][p][s][0] = mvd_l0[dup_mvd_l0];
                                        macro[n].mvd[0][p][s][1] = mvd_l0[dup_mvd_l0+1];
                                        macro[n].mvd[0][p][s][0] = mvd_l1[dup_mvd_l1];
                                        macro[n].mvd[0][p][s][1] = mvd_l1[dup_mvd_l1+1];
                                    }
                                    dup_mvd_l0 += 2;
                                    dup_mvd_l1 += 2;
                                    break;

                                case B_L0_4x8:
                                    macro[n].ref_idx_l0[p] = ref_idx_l0[dup_ref_l0++];
                                    for (s=0; s<3; s+=2) {
                                        macro[n].mvd[0][p][s][0] = mvd_l0[dup_mvd_l0];
                                        macro[n].mvd[0][p][s][1] = mvd_l0[dup_mvd_l0+1];
                                    }
                                    dup_mvd_l0 += 2;
                                    for (s=1; s<4; s+=2) {
                                        macro[n].mvd[0][p][s][0] = mvd_l0[dup_mvd_l0];
                                        macro[n].mvd[0][p][s][1] = mvd_l0[dup_mvd_l0+1];
                                    }
                                    dup_mvd_l0 += 2;
                                    break;

                                case B_L1_4x8:
                                    macro[n].ref_idx_l1[p] = ref_idx_l1[dup_ref_l1++];
                                    for (s=0; s<3; s+=2) {
                                        macro[n].mvd[0][p][s][0] = mvd_l1[dup_mvd_l1];
                                        macro[n].mvd[0][p][s][1] = mvd_l1[dup_mvd_l1+1];
                                    }
                                    dup_mvd_l1 += 2;
                                    for (s=1; s<4; s+=2) {
                                        macro[n].mvd[0][p][s][0] = mvd_l1[dup_mvd_l1];
                                        macro[n].mvd[0][p][s][1] = mvd_l1[dup_mvd_l1+1];
                                    }
                                    dup_mvd_l1 += 2;
                                    break;

                                case B_Bi_4x8:
                                    macro[n].ref_idx_l0[p] = ref_idx_l0[dup_ref_l0++];
                                    macro[n].ref_idx_l1[p] = ref_idx_l1[dup_ref_l1++];
                                    for (s=0; s<3; s+=2) {
                                        macro[n].mvd[0][p][s][0] = mvd_l0[dup_mvd_l0];
                                        macro[n].mvd[0][p][s][1] = mvd_l0[dup_mvd_l0+1];
                                        macro[n].mvd[0][p][s][0] = mvd_l1[dup_mvd_l1];
                                        macro[n].mvd[0][p][s][1] = mvd_l1[dup_mvd_l1+1];
                                    }
                                    dup_mvd_l0 += 2;
                                    dup_mvd_l1 += 2;
                                    for (s=1; s<4; s+=2) {
                                        macro[n].mvd[0][p][s][0] = mvd_l0[dup_mvd_l0];
                                        macro[n].mvd[0][p][s][1] = mvd_l0[dup_mvd_l0+1];
                                        macro[n].mvd[0][p][s][0] = mvd_l1[dup_mvd_l1];
                                        macro[n].mvd[0][p][s][1] = mvd_l1[dup_mvd_l1+1];
                                    }
                                    dup_mvd_l0 += 2;
                                    dup_mvd_l1 += 2;
                                    break;

                                case B_L0_4x4:
                                    macro[n].ref_idx_l0[p] = ref_idx_l0[dup_ref_l0++];
                                    for (s=0; s<4; s++) {
                                        macro[n].mvd[0][p][s][0] = mvd_l0[dup_mvd_l0++];
                                        macro[n].mvd[0][p][s][1] = mvd_l0[dup_mvd_l0++];
                                    }
                                    break;

                                case B_L1_4x4:
                                    macro[n].ref_idx_l1[p] = ref_idx_l1[dup_ref_l1++];
                                    for (s=0; s<4; s++) {
                                        macro[n].mvd[0][p][s][0] = mvd_l1[dup_mvd_l1++];
                                        macro[n].mvd[0][p][s][1] = mvd_l1[dup_mvd_l1++];
                                    }
                                    break;

                                case B_Bi_4x4:
                                    macro[n].ref_idx_l0[p] = ref_idx_l0[dup_ref_l0++];
                                    macro[n].ref_idx_l1[p] = ref_idx_l1[dup_ref_l1++];
                                    for (s=0; s<4; s++) {
                                        macro[n].mvd[0][p][s][0] = mvd_l0[dup_mvd_l0++];
                                        macro[n].mvd[0][p][s][1] = mvd_l0[dup_mvd_l0++];
                                        macro[n].mvd[0][p][s][0] = mvd_l1[dup_mvd_l1++];
                                        macro[n].mvd[0][p][s][1] = mvd_l1[dup_mvd_l1++];
                                    }
                                    break;

                                default:
                                    break;
                            }
                        }
                        break;

                    default:
                        break;
                }
            }

            /* sanity check parsing of previous macroblock */
            if (index_sub!=0 && index_sub!=4)
                rvmessage("logfile: poc=%d mb=%d: no. of sub_mb_type's is: %d", pic_order_cnt, mb_addr, index_sub);
            /* ref_idx duplication needs more work before this check will pass
            if (dup_ref_l0>index_ref_l0)
                rvmessage("logfile: poc=%d mb=%d: reference picture index l0 overrun: %d>%d", pic_order_cnt, mb_addr, dup_ref_l0, index_ref_l0);
            if (dup_ref_l1>index_ref_l1)
            rvmessage("logfile: poc=%d mb=%d: reference picture index l1 overrun: %d>%d", pic_order_cnt, mb_addr, dup_ref_l1, index_ref_l1);*/
            if (dup_mvd_l0>index_mvd_l0)
                rvmessage("logfile: poc=%d mb=%d: motion vector index l0 overrun: %d>%d", pic_order_cnt, mb_addr, dup_mvd_l0, index_mvd_l0);
            if (dup_mvd_l1>index_mvd_l1)
                rvmessage("logfile: poc=%d mb=%d: motion vector index l1 overrun: %d>%d", pic_order_cnt, mb_addr, dup_mvd_l1, index_mvd_l1);

            /* start new macroblock */
            if (++n==num_macros)
                break;

            /* increment idr_num if */
            if (type==2 && prev_type!=2)
                idr_num++;
            prev_type = type;

            /* fill in macroblock metadata */
            macro[n].idr_num = idr_num;
            macro[n].pic_order_cnt = pic_order_cnt;
            macro[n].slice_num = slice_num;
            macro[n].mb_addr = mb_addr;
            macro[n].slice_type = slice_type;
            macro[n].mb_type = slice_is_b(slice_type)? B_Skip : P_Skip;

            /* reset syntax element arrays */
            memset(mvd_l0, 0, sizeof(mvd_l0));
            memset(mvd_l1, 0, sizeof(mvd_l1));
            memset(ref_idx_l0, 0, sizeof(ref_idx_l0));
            memset(ref_idx_l1, 0, sizeof(ref_idx_l1));

            /* reset syntax element indicies */
            index_sub = 0;
            index_ref_l0 = 0;
            index_ref_l1 = 0;
            index_mvd_l0 = 0;
            index_mvd_l1 = 0;
            index_block = 0;

            continue;
        }

        /* macroblock skip flag */
        int mb_skip_flag;
        if (matchvalue(line, "mb_skip_flag", &mb_skip_flag)) {
            /* seems to be inverted */
            mb_skip_flag = !mb_skip_flag;
            if (mb_skip_flag) {
                if (slice_is_p(slice_type))
                    macro[n].mb_type = P_Skip;
                else
                    macro[n].mb_type = B_Skip;
                continue;
            }
        }

        /* macroblock field flag */
        int mb_field_decoding_flag;
        if (matchlast(line, "mb_field_decoding_flag", &mb_field_decoding_flag)) {
            macro[n].mb_field_decoding_flag = mb_field_decoding_flag;
            continue;
        }

        /* macroblock type */
        int mb_type;
        if (matchlast(line, "mb_type", &mb_type)) {
            /* fixups to make mb_type match enumeration value */
            if (slice_is_i(slice_type))
                macro[n].mb_type = mb_type_t(mb_type);
            else if (slice_is_p(slice_type)) {
                /* p-slice mb_type seems to be +1 with cabac */
                if (pps->entropy_coding_mode_flag)
                    mb_type--;
                if (mb_type>=5)
                    macro[n].mb_type = mb_type_t(mb_type-5); /* intra macroblock */
                else
                    macro[n].mb_type = mb_type_t(mb_type+P_OFFSET);
            } else {
                if (mb_type>=23)
                    macro[n].mb_type = mb_type_t(mb_type-23);
                else
                    macro[n].mb_type = mb_type_t(mb_type+B_OFFSET);
            }
            continue;
        }

        /* sub macroblock type */
        int sub_mb_type;
        if (matchlast(line, "sub_mb_type", &sub_mb_type)) {
            macro[n].sub_mb_type[index_sub++] = sub_mb_type_t(sub_mb_type);
            continue;
        }

        /* transform size flag */
        int transform_flag;
        if (matchvalue(line, "transform size 8x8 flag", &transform_flag)) {
            macro[n].transform_size_8x8_flag = transform_flag;
            continue;
        }

        /* intra prediction mode */
        int pred_mode;
        if (matchlast(line, "intra4x4_pred_mode", &pred_mode)) {
            macro[n].rem_intra_pred_mode[index_block++] = (intra_4x4_pred_mode_t)pred_mode;
            continue;
        }
        if (matchlast(line, "intra_intra_chroma_pred_mode", &pred_mode)) {
            macro[n].intra_chroma_pred_mode = intra_chroma_pred_mode_t(pred_mode);
            continue;
        }

        /* coded block pattern */
        int coded_block_pattern;
        if (matchlast(line, "coded_block_pattern", &coded_block_pattern)) {
            macro[n].coded_block_pattern = coded_block_pattern;
            continue;
        }

        /* reference index list */
        int ref_idx;
        if (matchlast(line, "ref_idx_l0", &ref_idx)) {
            ref_idx_l0[index_ref_l0++] = ref_idx;
            continue;
        }
        if (matchlast(line, "ref_idx_l1", &ref_idx)) {
            ref_idx_l1[index_ref_l1++] = ref_idx;
            continue;
        }

        /* motion vector list */
        int mvd;
        if (matchlast(line, "mvd_l0", &mvd)) {
            mvd_l0[index_mvd_l0++] = mvd;
            continue;
        }
        if (matchlast(line, "mvd_l1", &mvd)) {
            mvd_l1[index_mvd_l1++] = mvd;
            continue;
        }

        /* mb_qp_delta */
        int mb_qp_delta;
        if (matchlast(line, "mb_qp_delta", &mb_qp_delta)) {
            macro[n].mb_qp_delta = mb_qp_delta;
            continue;
        }
        if (matchlast(line, "Delta quant", &mb_qp_delta)) {
            macro[n].mb_qp_delta = mb_qp_delta;
            continue;
        }

    }
    if (ferror(trace)) {
        rverror("failed to read from trace file");
        return -1;
    }
    return 0;
}

/* parse the sequence parameter header from an hm hevc decoder logfile */
int parse_hmlog_header(FILE *trace, struct hevc_vid_parameter_set *vps, struct hevc_seq_parameter_set *sps, struct hevc_pic_parameter_set *pps)
{
    char line[256];

    /* first clear the parameter sets */
    memset(vps, 0, sizeof(struct hevc_vid_parameter_set));
    memset(sps, 0, sizeof(struct hevc_seq_parameter_set));
    memset(pps, 0, sizeof(struct hevc_pic_parameter_set));

    /* loop over lines in trace file */
    while (fgets(line, sizeof(line), trace)) {
        int value;

        /* look for video parameter set syntax elements */
        if (matchlast(line, "vps_max_sub_layers_minus1", &value))
            vps->max_sub_layers_minus1 = value;
        if (matchlast(line, "vps_num_hrd_parameters", &value))
            vps->num_hrd_parameters = value;

        /* look for sequence parameter set syntax elements */
        if (matchparen(line, "chroma_format_idc", &value))
            sps->chroma_format_idc = value;
        if (matchparen(line, "pic_width_in_luma_samples", &value))
            sps->pic_width_in_luma_samples = value;
        if (matchparen(line, "pic_height_in_luma_samples", &value))
            sps->pic_height_in_luma_samples = value;

        /* look for picture parameter set syntax elements */
        if (matchparen(line, "weighted_pred_flag", &value))
            pps->weighted_pred_flag = value;

        /* return when all required information has been read */
        if (matchparen(line, "first_slice_in_pic_flag", &value)) {
            vps->occupied = 1;
            sps->occupied = 1;
            pps->occupied = 1;
            /* calculate derived parameters in sps */
            sps->SubWidthC = sps->chroma_format_idc==1 || sps->chroma_format_idc==2? 2 : 1;
            sps->SubHeightC = sps->chroma_format_idc==1? 2 : 1;
            sps->MinCbLog2SizeY = sps->log2_min_luma_coding_block_size_minus3 + 3;
            sps->CtbLog2SizeY = sps->MinCbLog2SizeY + sps->log2_diff_max_min_luma_coding_block_size;
            sps->MinCbSizeY = 1 << sps->MinCbLog2SizeY;
            sps->CtbSizeY = 1 << sps->CtbLog2SizeY;
            sps->PicWidthInMinCbsY = sps->pic_width_in_luma_samples / sps->MinCbSizeY;
            sps->PicWidthInCtbsY = (sps->pic_width_in_luma_samples + sps->CtbSizeY - 1) / sps->CtbSizeY;
            sps->PicHeightInMinCbsY = sps->pic_height_in_luma_samples / sps->MinCbSizeY;
            sps->PicHeightInCtbsY = (sps->pic_height_in_luma_samples + sps->CtbSizeY - 1) / sps->CtbSizeY;
            sps->PicSizeInMinCbsY = sps->PicWidthInMinCbsY * sps->PicHeightInMinCbsY;
            sps->PicSizeInCtbsY = sps->PicWidthInCtbsY * sps->PicHeightInCtbsY;
            sps->PicSizeInSamplesY = sps->pic_width_in_luma_samples * sps->pic_height_in_luma_samples;
            sps->PicWidthInSamplesC = sps->pic_width_in_luma_samples / sps->SubWidthC;
            sps->PicHeightInSamplesC = sps->pic_height_in_luma_samples / sps->SubHeightC;
            if (sps->chroma_format_idc==0 || sps->separate_colour_plane_flag) {
                sps->CtbWidthC  = 0;
                sps->CtbHeightC = 0;
            } else {
                sps->CtbWidthC  = sps->CtbSizeY / sps->SubWidthC;
                sps->CtbHeightC = sps->CtbSizeY / sps->SubHeightC;
            }
            return 0;
        }
    }
    if (ferror(trace))
        rverror("failed to read from trace file");
    return -1;
}

/* parse a number of macroblocks from an hm hevc decoder logfile */
int parse_hmlog_macros(FILE *trace, int num_macros, struct rvhevccodingunit macro[], struct hevc_vid_parameter_set *vps, struct hevc_seq_parameter_set *sps, struct hevc_pic_parameter_set *pps)
{
    char line[256];

    /* syntax elements */
    int slice_type = 0;
    int mvd_l0[32] = {0};
    int mvd_l1[32] = {0};
    int ref_idx_l0[4] = {0};
    int ref_idx_l1[4] = {0};

    /* loop over lines in trace file */
    int n = -1;
    int idr_num = -1, prev_type = -1;
    while (fgets(line, sizeof(line), trace)) {

        /* slice type */
        if (matchlast(line, "slice_type", &slice_type))
            continue;

        /* start of next macroblock in frame */
        int pic_order_cnt, cu_addr, slice_num, type;
        if (sscanf(line, "*********** POC: %d (I/P) MB: %d Slice: %d Type %d **********", &pic_order_cnt, &cu_addr, &slice_num, &type)==4) {

            /* finish storing data into previous macroblock */

            /* arrange and duplicate motion vectors */

            /* sanity check parsing of previous macroblock */

            /* start new macroblock */
            if (++n==num_macros)
                break;

            /* increment idr_num if */
            if (type==2 && prev_type!=2)
                idr_num++;
            prev_type = type;

            /* fill in macroblock metadata */
            macro[n].idr_num = idr_num;
            macro[n].pic_order_cnt = pic_order_cnt;
            macro[n].slice_num = slice_num;
            macro[n].cu_addr = cu_addr;
            //macro[n].slice_type = slice_type;
            //macro[n].mb_type = slice_is_b(slice_type)? B_Skip : P_Skip;

            /* reset syntax element arrays */
            memset(mvd_l0, 0, sizeof(mvd_l0));
            memset(mvd_l1, 0, sizeof(mvd_l1));
            memset(ref_idx_l0, 0, sizeof(ref_idx_l0));
            memset(ref_idx_l1, 0, sizeof(ref_idx_l1));

            continue;
        }

        /* macroblock type */
#if 0
        int mb_type;
        if (matchlast(line, "mb_type", &mb_type)) {
            /* fixups to make mb_type match enumeration value */
            if (slice_is_i(slice_type))
                macro[n].mb_type = mb_type;
            else if (slice_is_p(slice_type)) {
                /* p-slice mb_type seems to be +1 with cabac */
                if (pps->entropy_coding_mode_flag)
                    mb_type--;
                if (mb_type>=5)
                    macro[n].mb_type = mb_type-5; /* intra macroblock */
                else
                    macro[n].mb_type = mb_type+P_OFFSET;
            } else {
                if (mb_type>=23)
                    macro[n].mb_type = mb_type-23;
                else
                    macro[n].mb_type = mb_type+B_OFFSET;
            }
            continue;
        }
#endif

    }
    if (ferror(trace)) {
        rverror("failed to read from trace file");
        return -1;
    }
    return 0;
}
