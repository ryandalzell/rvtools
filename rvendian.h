
#ifndef _ENDIAN_H_
#define _ENDIAN_H_

short swapendians(short d);
long swapendian3(long d);
int swapendian(int d);
long long swapendianll(long long d);

#endif
