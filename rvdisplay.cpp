/*
 * Description: X-Window display functions for video frames.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-07-09 11:37:22 $
 * Revision   : $Revision: 1.90 $
 * Copyright  : (c) 2005 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/*
 * based on tmndecode (H.263 decoder) (C) 1996  Telenor R&D, Norway
 *        Karl Olav Lillevold <Karl.Lillevold@nta.no>
 */

/*
 * based on mpeg2decode, (C) 1994, MPEG Software Simulation Group
 * and mpeg2play, (C) 1994 Stefan Eckart
 *                         <stefan@lis.e-technik.tu-muenchen.de>
 *
 */

 /* the Xlib interface is closely modeled after
  * mpeg_play 2.0 by the Berkeley Plateau Research Group
  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <assert.h>

#include "rvutil.h"
#include "rvdisplay.h"
#include "yuv2rgb.h"
#include "rvh264.h"
#include "rvlogfile.h"

#ifdef SHMEM
static int gXErrorFlag;
static int HandleXError(Display *dpy, XErrorEvent *event)
{
    gXErrorFlag = 1;
    return 0;
}
#endif

static unsigned char *clp_base, *clp;
void init_clip_table()
{
    if (clp_base==NULL) {
        int i;

        clp_base = (unsigned char *)rvalloc(NULL, 1024, 0);

        clp = clp_base + 384;

        for (i=-384; i<640; i++)
            clp[i] = (i<0) ? 0 : ((i>255) ? 255 : i);
    }
}

void free_clip_table()
{
    if (clp_base) {
        rvfree(clp_base);
        clp_base = clp = NULL;
    }
}

/****************************************************************************

  4x4 ordered dither

  Threshold pattern:

     0  8  2 10
    12  4 14  6
     3 11  1  9
    15  7 13  5

 ****************************************************************************/

static unsigned char ytab[16 * (256 + 16)];
static unsigned char uvtab[256 * 269 + 270];

void ord4x4_dither_init(unsigned char pixel[])
{
    int i, j, v;
    unsigned char ctab[256 + 32];

    for (i = 0; i < 256 + 16; i++)
    {
        v = (i - 8) >> 4;
        if (v < 2)
            v = 2;
        else if (v > 14)
            v = 14;
        for (j = 0; j < 16; j++)
            ytab[16 * i + j] = pixel[(v << 4) + j];
    }

    for (i = 0; i < 256 + 32; i++)
    {
        v = (i + 48 - 128) >> 5;
        if (v < 0)
            v = 0;
        else if (v > 3)
            v = 3;
        ctab[i] = v;
    }

    for (i = 0; i < 255 + 15; i++)
        for (j = 0; j < 255 + 15; j++)
            uvtab[256 * i + j] = (ctab[i + 16] << 6) | (ctab[j + 16] << 4) | (ctab[i] << 2) | ctab[j];
}

void ord4x4_dither_frame(unsigned char *src[], unsigned char *dst, int width, int height, int zoom)
{
    int i, j;
    unsigned char *py = src[0];
    unsigned char *pu = src[1];
    unsigned char *pv = src[2];

    /* apply expansion factor */
    width  *= zoom;
    height *= zoom;

    for (j = 0; j < height; j += 4)
    {
        register unsigned int uv;

        /* line j + 0 */
        for (i = 0; i < width; i += 8)
        {
            uv = uvtab[(*pu++ << 8) | *pv++];
            *dst++ = ytab[((*py++) << 4) | (uv & 15)];
            *dst++ = ytab[((*py++ + 8) << 4) | (uv >> 4)];
            uv = uvtab[((*pu++ << 8) | *pv++) + 1028];
            *dst++ = ytab[((*py++ + 2) << 4) | (uv & 15)];
            *dst++ = ytab[((*py++ + 10) << 4) | (uv >> 4)];
            uv = uvtab[(*pu++ << 8) | *pv++];
            *dst++ = ytab[((*py++) << 4) | (uv & 15)];
            *dst++ = ytab[((*py++ + 8) << 4) | (uv >> 4)];
            uv = uvtab[((*pu++ << 8) | *pv++) + 1028];
            *dst++ = ytab[((*py++ + 2) << 4) | (uv & 15)];
            *dst++ = ytab[((*py++ + 10) << 4) | (uv >> 4)];
        }

        pu -= width/2;
        pv -= width/2;

        /* line j + 1 */
        for (i = 0; i < width; i += 8)
        {
            uv = uvtab[((*pu++ << 8) | *pv++) + 2056];
            *dst++ = ytab[((*py++ + 12) << 4) | (uv >> 4)];
            *dst++ = ytab[((*py++ + 4) << 4) | (uv & 15)];
            uv = uvtab[((*pu++ << 8) | *pv++) + 3084];
            *dst++ = ytab[((*py++ + 14) << 4) | (uv >> 4)];
            *dst++ = ytab[((*py++ + 6) << 4) | (uv & 15)];
            uv = uvtab[((*pu++ << 8) | *pv++) + 2056];
            *dst++ = ytab[((*py++ + 12) << 4) | (uv >> 4)];
            *dst++ = ytab[((*py++ + 4) << 4) | (uv & 15)];
            uv = uvtab[((*pu++ << 8) | *pv++) + 3084];
            *dst++ = ytab[((*py++ + 14) << 4) | (uv >> 4)];
            *dst++ = ytab[((*py++ + 6) << 4) | (uv & 15)];
        }

        /* line j + 2 */
        for (i = 0; i < width; i += 8)
        {
            uv = uvtab[((*pu++ << 8) | *pv++) + 1542];
            *dst++ = ytab[((*py++ + 3) << 4) | (uv & 15)];
            *dst++ = ytab[((*py++ + 11) << 4) | (uv >> 4)];
            uv = uvtab[((*pu++ << 8) | *pv++) + 514];
            *dst++ = ytab[((*py++ + 1) << 4) | (uv & 15)];
            *dst++ = ytab[((*py++ + 9) << 4) | (uv >> 4)];
            uv = uvtab[((*pu++ << 8) | *pv++) + 1542];
            *dst++ = ytab[((*py++ + 3) << 4) | (uv & 15)];
            *dst++ = ytab[((*py++ + 11) << 4) | (uv >> 4)];
            uv = uvtab[((*pu++ << 8) | *pv++) + 514];
            *dst++ = ytab[((*py++ + 1) << 4) | (uv & 15)];
            *dst++ = ytab[((*py++ + 9) << 4) | (uv >> 4)];
        }

        pu -= width/2;
        pv -= width/2;

        /* line j + 3 */
        for (i = 0; i < width; i += 8)
        {
            uv = uvtab[((*pu++ << 8) | *pv++) + 3598];
            *dst++ = ytab[((*py++ + 15) << 4) | (uv >> 4)];
            *dst++ = ytab[((*py++ + 7) << 4) | (uv & 15)];
            uv = uvtab[((*pu++ << 8) | *pv++) + 2570];
            *dst++ = ytab[((*py++ + 13) << 4) | (uv >> 4)];
            *dst++ = ytab[((*py++ + 5) << 4) | (uv & 15)];
            uv = uvtab[((*pu++ << 8) | *pv++) + 3598];
            *dst++ = ytab[((*py++ + 15) << 4) | (uv >> 4)];
            *dst++ = ytab[((*py++ + 7) << 4) | (uv & 15)];
            uv = uvtab[((*pu++ << 8) | *pv++) + 2570];
            *dst++ = ytab[((*py++ + 13) << 4) | (uv >> 4)];
            *dst++ = ytab[((*py++ + 5) << 4) | (uv & 15)];
        }
    }
}

/* Data for ConvertYUVtoRGB*/
long int crv_tab[256];
long int cbu_tab[256];
long int cgu_tab[256];

long int cgv_tab[256];
long int tab_76309[256];

void init_dither_tab()
{
    int i;
    const long int crv = 104597;  /* fra matrise i global.h */
    const long int cbu = 132201;
    const long int cgu = 25675;
    const long int cgv = 53279;

    for (i = 0; i < 256; i++) {
        crv_tab[i] = (i-128) * crv;
        cbu_tab[i] = (i-128) * cbu;
        cgu_tab[i] = (i-128) * cgu;
        cgv_tab[i] = (i-128) * cgv;
        tab_76309[i] = 76309*(i-16);
    }
}

/**********************************************************************
 *
 *  Name:            ConvertYUVtoRGB
 *  Description:     Converts YUV image to RGB (packed mode)
 *
 *  Input:           pointer to source luma, Cr, Cb, destination,
 *                       image width and height
 *  Returns:
 *  Side effects:
 *
 *  Date: 951208    Author: Karl.Lillevold@nta.no
 *
 ***********************************************************************/
void ConvertYUVtoRGB(unsigned char *src[], unsigned char *dst, int width, int height, int zoom)
{
    int y11, y21;
    int y12, y22;
    int y13, y23;
    int y14, y24;
    int u, v;
    int i, j;
    int c11, c21, c31, c41;
    int c12, c22, c32, c42;
    unsigned int DW;

    /* source pointers */
    unsigned char *py1 = src[0];
    unsigned char *pu  = src[1];
    unsigned char *pv  = src[2];
    unsigned char *py2 = py1 + width;

    /* destination pointers */
    unsigned char *d1 = dst + width*zoom*height*zoom*3 - width*zoom*3;
    unsigned char *d2 = d1 - width*zoom*3;

    unsigned int *id1 = (unsigned int *)d1;
    unsigned int *id2 = (unsigned int *)d2;

    for (j=0; j<height*zoom; j+=2) {
        /* line j + 0 */
        for (i=0; i<width; i+=4) {
            u = *pu++;
            v = *pv++;
            c11 = crv_tab[v];
            c21 = cgu_tab[u];
            c31 = cgv_tab[v];
            c41 = cbu_tab[u];
            u = *pu++;
            v = *pv++;
            c12 = crv_tab[v];
            c22 = cgu_tab[u];
            c32 = cgv_tab[v];
            c42 = cbu_tab[u];

            y11 = tab_76309[*py1++]; /* (255/219)*65536 */
            y12 = tab_76309[*py1++];
            y13 = tab_76309[*py1++]; /* (255/219)*65536 */
            y14 = tab_76309[*py1++];

            y21 = tab_76309[*py2++];
            y22 = tab_76309[*py2++];
            y23 = tab_76309[*py2++];
            y24 = tab_76309[*py2++];

            /* RGBR*/
            DW = ((clp[(y11 + c41)>>16])) |
                 ((clp[(y11 - c21 - c31)>>16])<<8) |
                 ((clp[(y11 + c11)>>16])<<16) |
                 ((clp[(y12 + c41)>>16])<<24);
            *id1++ = DW;

            /* GBRG*/
            DW = ((clp[(y12 - c21 - c31)>>16])) |
                 ((clp[(y12 + c11)>>16])<<8) |
                 ((clp[(y13 + c42)>>16])<<16) |
                 ((clp[(y13 - c22 - c32)>>16])<<24);
            *id1++ = DW;

            /* BRGB*/
            DW = ((clp[(y13 + c12)>>16])) |
                 ((clp[(y14 + c42)>>16])<<8) |
                 ((clp[(y14 - c22 - c32)>>16])<<16) |
                 ((clp[(y14 + c12)>>16])<<24);
            *id1++ = DW;

            /* RGBR*/
            DW = ((clp[(y21 + c41)>>16])) |
                 ((clp[(y21 - c21 - c31)>>16])<<8) |
                 ((clp[(y21 + c11)>>16])<<16) |
                 ((clp[(y22 + c41)>>16])<<24);
            *id2++ = DW;

            /* GBRG*/
            DW = ((clp[(y22 - c21 - c31)>>16])) |
                 ((clp[(y22 + c11)>>16])<<8) |
                 ((clp[(y23 + c42)>>16])<<16) |
                 ((clp[(y23 - c22 - c32)>>16])<<24);
            *id2++ = DW;

            /* BRGB*/
            DW = ((clp[(y23 + c12)>>16])) |
                 ((clp[(y24 + c42)>>16])<<8) |
                 ((clp[(y24 - c22 - c32)>>16])<<16) |
                 ((clp[(y24 + c12)>>16])<<24);
            *id2++ = DW;
        }
        id1 -= (9 * width*zoom)>>2;
        id2 -= (9 * width*zoom)>>2;
        if (j%zoom==0) {
            py1 += width;
            py2 += width;
        } else {
            py1 -= width;
            py2 -= width;
        }
    }
}

/*
 * create and map a window to display video in,
 * allocate colors and shared memory
 */
int create_window(struct rvwin *win, Display *display, int width, int height, int verbose)
{
    int i, j;
    unsigned int fg, bg;
    XEvent xev;
    XSetWindowAttributes xswa;
    unsigned int mask;
    unsigned char pixel[256];
    XGCValues values;
    unsigned long valuemask;

    /* initialise rvwin structre */
    win->display = display;
    win->width   = width;
    win->height  = height;

    /* get default visual */
    Visual *visual = DefaultVisual(display, DefaultScreen(display));
    unsigned int depth = DefaultDepth(display, DefaultScreen(display));

    /* check visual is supported */
    if (!((visual->c_class == TrueColor && depth == 32) || (visual->c_class == TrueColor && depth == 24)
        || (visual->c_class == TrueColor && depth == 16) || (visual->c_class == PseudoColor && depth == 8)))
       rvexit("8-bit PseudoColor or 16/24/32-bit TrueColor display required");

    if (verbose>=2) {
        if (visual->c_class == TrueColor && depth == 32)
            rvmessage("visual: 32-bit TrueColor");
        if (visual->c_class == TrueColor && depth == 24)
            rvmessage("visual: 24-bit TrueColor");
        if (visual->c_class == TrueColor && depth == 16)
            rvmessage("visual: 16-bit TrueColor");
        if (visual->c_class == PseudoColor && depth == 8)
            rvmessage("visual: 8-bit PseudoColor (using 4x4 ordered dither)");
    }

    /* set width and height of the display window */
    XSizeHints *hints  = XAllocSizeHints();
    hints->min_width   = 32;
    hints->max_width   = width;
    hints->base_width  = width;
    hints->min_height  = 32;
    hints->max_height  = height;
    hints->base_height = height;
    hints->flags = PBaseSize | PMinSize | PMaxSize;

    /* get some colors */
    bg = WhitePixel(display, DefaultScreen(display));
    fg = BlackPixel(display, DefaultScreen(display));

    /* create the window */
    mask = CWBackPixel | CWBorderPixel | CWBitGravity;
    if (depth == 32 || depth == 24 || depth == 16) {
        mask |= CWColormap;
        xswa.colormap = XCreateColormap(display, DefaultRootWindow (display), visual, AllocNone);
    }
    xswa.background_pixel = bg;
    xswa.border_pixel = fg;
    xswa.bit_gravity = StaticGravity; /* static gravity here eliminates flicker when resizing */
    win->window = XCreateWindow (display, DefaultRootWindow(display), 0, 0, width, height, 1, depth, InputOutput, visual, mask, &xswa);

    /* set the window manager properties */
    XStoreName(win->display, win->window, win->winname);
    XSetWMNormalHints(win->display, win->window, hints);
    XFree(hints);

    /* map window and wait for map */
    XSelectInput(display, win->window, StructureNotifyMask);
    XMapWindow(display, win->window);
    do {
        XNextEvent(display, &xev);
    } while (xev.type != MapNotify || xev.xmap.event != win->window);
    XSelectInput(display, win->window, NoEventMask);

    /* get actual windows size after map */
    Window root;
    int x, y;
    unsigned int border;
    XGetGeometry(display, win->window, &root, &x, &y, &win->width, &win->height, &border, &depth);

    /* prepare a graphics context */
    values.foreground = WhitePixel(display, DefaultScreen(display));
    values.background = BlackPixel(display, DefaultScreen(display));
    values.line_style = LineSolid;
    values.cap_style = CapButt;
    values.graphics_exposures = 0;
    valuemask = GCForeground | GCBackground | GCLineStyle | GCCapStyle | GCGraphicsExposures;

    /* create a graphics context */
    win->gc = XCreateGC(win->display, win->window, valuemask, &values);

    /* create the large and small fonts (second name is a fallback) */
    win->fontname[0][0] = "*-fixed-medium-*-18-*";
    win->fontname[0][1] = "*-*-medium-r-*-18-*";
    win->fontname[1][0] = "*-fixed-medium-*-10-*";
    win->fontname[1][1] = "*-*-medium-r-*-10-*";
    for (i=0; i<2; i++) {
        for (j=0; j<2; j++) {
            if (verbose>=2)
                rvmessage("loading font: %s", win->fontname[i][j]);
            win->font[i] = XLoadQueryFont(display, win->fontname[i][j]);
            if (win->font[i]==NULL)
                rvmessage("failed to load font: %s", win->fontname[i][j]);
            else
                break;
        }
    }

    /* create a pixmap */
    win->pixmap = XCreatePixmap(win->display, win->window, width, height, depth);
    if (win->pixmap<0)
        rvexit("failed to create pixmap");

    /* allocate colors */
    init_clip_table();
    if (depth == 8) {
        int priv = 0;
        XWindowAttributes xwa;
        Colormap cmap = DefaultColormap(display, DefaultScreen(display));

        /*
        * colour space conversion coefficients
        *
        * entries are {crv,cbu,cgu,cgv}
        *
        * crv=(255/224)*65536*(1-cr)/0.5
        * cbu=(255/224)*65536*(1-cb)/0.5
        * cgu=(255/224)*65536*(cb/cg)*(1-cb)/0.5
        * cgv=(255/224)*65536*(cr/cg)*(1-cr)/0.5
        *
        * where Y=cr*R+cg*G+cb*B (cr+cg+cb=1)
        */

        const int conversion_matrix[8][4] =
        {
            {117504, 138453, 13954, 34903}, /* no sequence_display_extension */
            {117504, 138453, 13954, 34903}, /* ITU-R Rec. 709 (1990) */
            {104597, 132201, 25675, 53279}, /* unspecified */
            {104597, 132201, 25675, 53279}, /* reserved */
            {104448, 132798, 24759, 53109}, /* FCC */
            {104597, 132201, 25675, 53279}, /* ITU-R Rec. 624-4 System B, G */
            {104597, 132201, 25675, 53279}, /* SMPTE 170M */
            {117579, 136230, 16907, 35559}  /* SMPTE 240M (1987) */
        };

        /* matrix coefficients (MPEG-1 = TMN parameters) */
        int crv = conversion_matrix[5][0];
        int cbu = conversion_matrix[5][1];
        int cgu = conversion_matrix[5][2];
        int cgv = conversion_matrix[5][3];

        /* color allocation:
        * i is the (internal) 8 bit color number, it consists of separate
        * bit fields for Y, U and V: i = (yyyyuuvv), we don't use yyyy=0000
        * yyyy=0001 and yyyy=1111, this leaves 48 colors for other applications
        *
        * the allocated colors correspond to the following Y, U and V values:
        * Y:   40, 56, 72, 88, 104, 120, 136, 152, 168, 184, 200, 216, 232
        * U,V: -48, -16, 16, 48
        *
        * U and V values span only about half the color space; this gives
        * usually much better quality, although highly saturated colors can
        * not be displayed properly
        *
        * translation to R,G,B is implicitly done by the color look-up table
        */
        for (i=32; i<240; i++) {
            /* color space conversion */
            int y = 16*((i>>4)&15) + 8;
            int u = 32*((i>>2)&3)  - 48;
            int v = 32*(i&3)       - 48;

            y = 76309 * (y - 16); /* (255/219)*65536 */

            int r = clp[(y + crv*v + 32768)>>16];
            int g = clp[(y - cgu*u -cgv*v + 32768)>>16];
            int b = clp[(y + cbu*u + 32786)>>16];

            /* X11 colors are 16 bit */
            XColor xcolor;
            xcolor.red   = r << 8;
            xcolor.green = g << 8;
            xcolor.blue  = b << 8;

            if (XAllocColor(display, cmap, &xcolor) != 0)
                pixel[i] = xcolor.pixel;
            else {
                /* allocation failed, have to use a private colormap */
                if (priv)
                    rvexit("couldn't allocate private colormap");

                priv = 1;

                if (verbose>=0) rvmessage("using private colormap (%d colors were available)", i-32);

                /* free colors. */
                while (--i >= 32)
                {
                    unsigned long tmp = pixel[i]; /* because XFreeColors expects unsigned long */
                    XFreeColors(display, cmap, &tmp, 1, 0);
                }

                /* i is now 31, this restarts the outer loop */

                /* create private colormap */
                XGetWindowAttributes(display, win->window, &xwa);
                cmap = XCreateColormap(display, win->window, xwa.visual, AllocNone);
                XSetWindowColormap(display, win->window, cmap);
            }
        }
    }

#ifdef SHMEM
    if (XShmQueryExtension(display))
        win->shmem = 1;
    else {
        win->shmem = 0;
        if (verbose>=0) rvmessage("shared memory not supported, reverting to normal Xlib");
    }

    gXErrorFlag = 0;
    XSetErrorHandler(HandleXError);
    XFlush(display);

    if (win->shmem)
    {
        /* allocate ximage structure (but not image itself) */
        win->ximage = XShmCreateImage(display, visual, depth, ZPixmap, NULL, &win->shminfo, width*zoom, height*zoom);
        if (win->ximage==NULL) {
            if (verbose>=0) rvmessage("shared memory error, disabling (Ximage error)");
            goto shmemerror;
        }

        /* allocate shared memory for image */
        win->shminfo.shmid = shmget(IPC_PRIVATE, win->ximage->bytes_per_line * win->ximage->height, IPC_CREAT | 0777);
        if (win->shminfo.shmid<0) {
            XDestroyImage(win->ximage);
            if (verbose>=0) rvmessage("shared memory error, disabling (seg id error)");
            goto shmemerror;
        }
        win->shminfo.shmaddr = win->ximage->data = shmat(win->shminfo.shmid, 0, 0);
        if (win->shminfo.shmaddr==((char *) -1)) {
            XDestroyImage(win->ximage);
            if (verbose>=0) rvmessage("shared memory error, disabling (address error)");
            goto shmemerror;
        }

        /* associate shared memory with ximage */
        win->shminfo.readOnly = False;
        if (!XShmAttach(display, &win->shminfo)) {
            XDestroyImage(win->ximage);
            shmdt(win->shminfo.shmaddr);
            shmctl(win->shminfo.shmid, IPC_RMID, 0);
            if (verbose>=0) rvmessage("shared memory error, disabling (attach error)");
            goto shmemerror;
        }

        XSync(display, False);

        if (gXErrorFlag) {
            /* Ultimate failure here. */
            /*XShmDetach(display, &win->shminfo);*/
            XDestroyImage(win->ximage);
            shmdt(win->shminfo.shmaddr);
            shmctl(win->shminfo.shmid, IPC_RMID, 0);
            if (verbose>=0) rvmessage("shared memory error, disabling");
            gXErrorFlag = 0;
            goto shmemerror;
        } else {
            shmctl(win->shminfo.shmid, IPC_RMID, 0);
        }

        if (verbose>=1) rvmessage("using shared memory");
    }
    else
    {
shmemerror:
        win->shmem = 0;
    }
    XSetErrorHandler(NULL);
    XFlush(display);
#endif


    if (depth == 32 || depth == 24 || depth == 16)
        InitColorDither(display, win->window);
    else
        ord4x4_dither_init(pixel);

    return 1;
}

void resize_window(struct rvwin *win, int width, int height)
{
    /* resize window */
    XResizeWindow(win->display, win->window, width, height);

    /* update window metadata */
    win->width = width;
    win->height = height;
}

void resize_pixmap(struct rvwin *win, int width, int height)
{
    XSizeHints *hints = XAllocSizeHints();

    /* prepare new max size hints */
    hints->max_width  = width;
    hints->max_height = height;
    hints->flags = PMaxSize;

    /* resize pixmap */
    XSetWMNormalHints(win->display, win->window, hints);
    XFreePixmap(win->display, win->pixmap);
    int depth = DefaultDepth(win->display, DefaultScreen(win->display));
    win->pixmap = XCreatePixmap(win->display, win->window, width, height, depth);

    XFree(hints);
}

XImage *create_ximage(struct rvwin *win, int width, int height)
{
    char dummy;

    Visual *visual = DefaultVisual(win->display, DefaultScreen(win->display));
    int depth = DefaultDepth(win->display, DefaultScreen(win->display));
    XImage *ximage = XCreateImage(win->display, visual, depth, ZPixmap, 0, &dummy, width, height, 8, 0);
    /* this is not portable to 64-bit */
    //ximage->data = (char *)rvalloc(NULL, width*height*(depth>8 ? sizeof(int) : sizeof(char)), 0);
    ximage->data = (char *)rvalloc(NULL, width*height*(depth>8 ? 4 : 1), 0);

    //rvmessage("ximage: %d bits per pixel", ximage->bits_per_pixel);

    /* Always work in native bit and byte order. This tells Xlib to
     * reverse bit and byte order if necessary when crossing a
     * network. Frankly, this part of XImages is somewhat
     * underdocumented, so this may not be exactly correct.
     */
    int t = 1;
    if (*(char *)&t == 1)
        ximage->bitmap_bit_order = ximage->byte_order = LSBFirst;
    else
        ximage->bitmap_bit_order = ximage->byte_order = MSBFirst;

    return ximage;
}

void resize_ximage(struct rvwin *win, int width, int height)
{
    if (win->ximage)
        rvfree(win->ximage->data);
    win->ximage = create_ximage(win, width, height);
}

void free_ximage(struct rvwin *win)
{
    if (win->ximage)
        XDestroyImage(win->ximage);
}

void dither_image(struct rvwin *win, unsigned char *data[3], fourcc_t fourcc, int bpp, int zoom)
{
    /* dither image using correct algorithm for display type */
    int depth = DefaultDepth(win->display, DefaultScreen(win->display));

    switch (fourcc) {
        case I420:
        case IYUV:
        case Y800:
            switch (depth) {
                case 32:
                case 24:
                    if (win->ximage->bits_per_pixel == 24)
                        ConvertYUVtoRGB(data, (unsigned char *)win->ximage->data, win->ximage->width, win->ximage->height, zoom);
                    else
                        YUVplanar_RGB32(data, (unsigned char *)win->ximage->data, win->ximage->width, win->ximage->height, zoom);
                    break;

                case 16:
                    Color16DitherImage(data, (unsigned char *)win->ximage->data, win->ximage->width, win->ximage->height, zoom);
                    break;

                case 8:
                    ord4x4_dither_frame(data, (unsigned char *)win->ximage->data, win->ximage->width, win->ximage->height, zoom);
                    break;

                default:
                    rvexit("unsupported display depth for planar yuv data: %d", depth);
            }
            break;

        case I422:
        case YU16:
            switch (depth) {
                case 32:
                case 24:
                    YUV422_RGB32(data, (unsigned char *)win->ximage->data, win->ximage->width, win->ximage->height, zoom);
                    break;

                default:
                    rvexit("unsupported display depth for planar yuv data: %d", depth);
            }
            break;

        case I444:
            switch (depth) {
                case 32:
                case 24:
                    YUVplanar444_RGB32(data, (unsigned char *)win->ximage->data, win->ximage->width, win->ximage->height, zoom, bpp);
                    break;

                default:
                    rvexit("unsupported display depth for yuv 4:4:4 planar data: %d", depth);
            }
            break;

        case UYVY:
            switch (depth) {
                case 32:
                case 24:
                    UYVY_RGB32(data[0], (unsigned char *)win->ximage->data, win->ximage->width, win->ximage->height, zoom);
                    break;

                default:
                    rvexit("unsupported display depth for yuv 4:2:2 packed data: %d", depth);
            }
            break;

        case YUY2:
            switch (depth) {
                case 32:
                case 24:
                    YUY2_RGB32(data[0], (unsigned char *)win->ximage->data, win->ximage->width, win->ximage->height, zoom);
                    break;

                default:
                    rvexit("unsupported display depth for yuv 4:2:2 packed data: %d", depth);
            }
            break;

        case TWOVUY:
        case VYUY:
            switch (depth) {
                case 32:
                case 24:
                    VYUY_RGB32(data[0], (unsigned char *)win->ximage->data, win->ximage->width, win->ximage->height, zoom);
                    break;

                default:
                    rvexit("unsupported display depth for yuv 4:2:2 packed data: %d", depth);
            }
            break;


        case RGB2:
            switch (depth) {
                case 32:
                case 24:
                    RGB24_RGB32(data[0], (unsigned char *)win->ximage->data, win->ximage->width, win->ximage->height, zoom, bpp);
                    break;
            }
            break;

        case RGBP:
            switch (depth) {
                case 32:
                case 24:
                    RGBplanar_RGB32(data, (unsigned char *)win->ximage->data, win->ximage->width, win->ximage->height, zoom, bpp);
                    break;
            }
            break;

        case V210:
            switch (depth) {
                case 32:
                case 24:
                    V210_RGB32(data[0], (unsigned char *)win->ximage->data, win->ximage->width, win->ximage->height, zoom);
                    break;
            }
            break;

        case YU15:
            switch (depth) {
                case 32:
                case 24:
                    YU15_RGB32(data, (unsigned char *)win->ximage->data, win->ximage->width, win->ximage->height, zoom);
                    break;
            }
            break;

        case YU20:
            switch (depth) {
                case 32:
                case 24:
                    YU20_RGB32(data, (unsigned char *)win->ximage->data, win->ximage->width, win->ximage->height, zoom);
                    break;
            }
            break;

        case NV12:
            switch (depth) {
                case 32:
                case 24:
                    NV12_RGB32(data, (unsigned char *)win->ximage->data, win->ximage->width, win->ximage->height, zoom);
                    break;

                default:
                    rvexit("unsupported display depth for planar yuv data: %d", depth);
            }
            break;

        case NV16:
            switch (depth) {
                case 32:
                case 24:
                    NV16_RGB32(data, (unsigned char *)win->ximage->data, win->ximage->width, win->ximage->height, zoom);
                    break;

                default:
                    rvexit("unsupported display depth for planar yuv data: %d", depth);
            }
            break;

        case NV20:
            switch (depth) {
                case 32:
                case 24:
                    NV20_RGB32(data, (unsigned char *)win->ximage->data, win->ximage->width, win->ximage->height, zoom);
                    break;

                default:
                    rvexit("unsupported display depth for planar yuv data: %d", depth);
            }
            break;

        case XV20:
            switch (depth) {
                case 32:
                case 24:
                    XV20_RGB32(data, (unsigned char *)win->ximage->data, win->ximage->width, win->ximage->height, zoom);
                    break;

                default:
                    rvexit("unsupported display depth for planar yuv data: %d", depth);
            }
            break;

    }
}

void display_ximage(struct rvwin *win, int x, int y)
{
    /* display dithered image */
    //GC gc = DefaultGC(win->display, DefaultScreen(win->display));
#ifdef SHMEM
    if (win->shmem)
    {
        XShmPutImage(win->display, win->pixmap, win->gc, win->ximage, 0, 0, x, y, win->ximage->width, win->ximage->height, True);
        XFlush(win->display);

        int CompletionType = XShmGetEventBase(win->display) + ShmCompletion;
        while (1)
        {
            XEvent xev;
            XNextEvent(win->display, &xev);
            if (xev.type == CompletionType)
                break;
        }
    }
    else
#endif
    {
        XPutImage(win->display, win->pixmap, win->gc, win->ximage, 0, 0, x, y, win->ximage->width, win->ximage->height);
    }

}

void display_grid(struct rvwin *win, int x, int y, int width, int height, int gauge, int line)
{
    int x0, y0, i=0;

    /* set the drawing attributes in the graphics context */
    XSetLineAttributes(win->display, win->gc, line, LineSolid, CapButt, JoinMiter);
    XSetForeground(win->display, win->gc, WhitePixel(win->display, DefaultScreen(win->display)));

    XSegment *segments = (XSegment *)rvalloc(NULL, (width/gauge+height/gauge)*sizeof(XSegment), 0);
    if (segments==NULL)
        return;

    /* draw the grid */
    for (x0=x+gauge; x0<x+width; x0+=gauge, i++) {
        segments[i].x1 = x0;
        segments[i].y1 = 0;
        segments[i].x2 = x0;
        segments[i].y2 = height;
    }
    for (y0=y+gauge; y0<y+height; y0+=gauge, i++) {
        segments[i].x1 = x;
        segments[i].y1 = y0;
        segments[i].x2 = x+width;
        segments[i].y2 = y0;
    }

    XDrawSegments(win->display, win->pixmap, win->gc, segments, i);

    free(segments);
}

void display_mark(struct rvwin *win, int x, int y, int width, int zoom, int xmark, int ymark)
{
    XColor red;

    if (!XAllocNamedColor(win->display, DefaultColormap(win->display, DefaultScreen(win->display)), "red", &red, &red))
        rvmessage("failed to allocate the colour red");

    /* set the drawing attributes in the graphics context */
    XSetLineAttributes(win->display, win->gc, 2, LineSolid, CapButt, JoinMiter);
    XSetForeground(win->display, win->gc, red.pixel);

    /* draw the marked macroblock */
    XDrawRectangle(win->display, win->pixmap, win->gc, x+xmark*16*zoom, y+ymark*16*zoom, 16*zoom, 16*zoom);
}

void display_pel(struct rvwin *win, int x, int y, int width, int height, int zoom, int xpel, int ypel, int chroma, int value)
{
    char string[32];

    /* set the drawing attributes in the graphics context */
    XSetLineAttributes(win->display, win->gc, 1, LineSolid, CapButt, JoinMiter);
    XSetForeground(win->display, win->gc, WhitePixel(win->display, DefaultScreen(win->display)));

    /* set the small font in the graphics context */
    if (!XSetFont(win->display, win->gc, win->font[1]->fid))
        rvmessage("failed to set the font in the graphics context: %s", win->fontname[1][0]);

    /* display a chroma pel TODO different subsampling */
    int size = zoom;
    if (chroma) {
        xpel &= ~1;
        ypel &= ~1;
        size *= 2;
    }

    /* highlight the selected pel */
    XDrawRectangle(win->display, win->pixmap, win->gc, x+xpel*zoom, y+ypel*zoom, size, size);

    /* prepare the description string */
    int len = snprintf(string, sizeof(string), "%d,%d: %d 0x%02x", xpel>>chroma, ypel>>chroma, value, value);

    /* get the font metrics of the chosen font */
    int dir, ascent, descent;
    XCharStruct cs;
    XTextExtents(win->font[1], string, len, &dir, &ascent, &descent, &cs);
    int wid = XTextWidth(win->font[1], string, len);

    /* choose where to draw the description */
    int x0 = x+(xpel+1+chroma)*zoom+1;
    if (x0+wid>x+width*zoom)
        x0 = x+(xpel+chroma)*zoom-wid;
    int y0 = y+ypel*zoom+ascent;
    if (y0+descent>y+height*zoom)
        y0 = y+ypel*zoom;

    /* describe the selected pel */
    XDrawImageString(win->display, win->pixmap, win->gc, x0, y0, string, strlen(string));
}

void display_numbers(struct rvwin *win, int x, int y, int width, int height, int zoom, int grid, int field, int mbaff, int coord)
{
    int fontindex;
    int x0, y0, i;
    char string[11];

    const int spc = grid<16 ? 16 : grid;
    const int inc = spc*zoom;
    const int max = field? width*height/512 : width*height/256;

    /* field picture and coordinate mode overrides mbaff */
    if (field || coord)
        mbaff = 0;

    /* set the drawing attributes in the graphics context */
    XSetForeground(win->display, win->gc, WhitePixel(win->display, DefaultScreen(win->display)));

    if (zoom<=2) {
        /* set the small font in the graphics context */
        fontindex = 1;
    } else {
        /* set the large font in the graphics context */
        fontindex = 0;
    }

    /* set the font in the graphics context */
    if (!XSetFont(win->display, win->gc, win->font[fontindex]->fid))
        rvmessage("failed to set the font in the graphics context: %s", win->fontname[fontindex][0]);

    /* get the font metrics of the chosen font */
    int dir, ascent, descent;
    XCharStruct cs;
    strncpy(string, "0123456789", sizeof(string));
    XTextExtents(win->font[fontindex], string, strlen(string), &dir, &ascent, &descent, &cs);

    /* draw the number grid */
    for (i=0, y0=y+2+inc/2; y0<y+height*zoom; y0+=inc) {
        for (x0=x+inc/2; x0<x+width*zoom; x0+=inc) {
            /* display numbers up to 999 normally, subsequently as three digits with leading zeros */
            int len;
            if (coord)
                /* display macroblock coordinates instead of number */
                len = snprintf(string, sizeof(string), "%d,%d", i%(width/spc), i/(width/spc));
            else if (zoom>=2)
                len = snprintf(string, sizeof(string), "%d", i);
            else
                len = snprintf(string, sizeof(string), i>999?"%03d":"%d", i%1000);
            int wid = XTextWidth(win->font[fontindex], string, len);
            if (wid<=inc)
                XDrawString(win->display, win->pixmap, win->gc, x0-wid/2, y0, string, len);
            /* special increment for macroblock adaptive frame/field mode */
            i += mbaff? 2 : 1;
        }
        if (mbaff) {
            /* return to left hand side of image */
            if (i&1)
                i--;                /* bottom macroblock -> top macroblock */
            else
                i -= 2*width/spc-1;  /* top macroblock -> bottom macroblock */
        }
        /* restart numbering for second field */
        if (field)
            if (i==max)
                i = 0;
    }
}

void display_info(struct rvwin *win, int x, int y, int framenum, int lastframe, int time, int gain, int threshold)
{
    char string[32];

    /* set the drawing attributes in the graphics context */
    XSetForeground(win->display, win->gc, WhitePixel(win->display, DefaultScreen(win->display)));

    /* set the large font in the graphics context */
    if (!XSetFont(win->display, win->gc, win->font[0]->fid))
        rvmessage("failed to set the font in the graphics context: %s", win->fontname[0][0]);

    /* prepare the string for display */
    int len = snprintf(string, sizeof(string), "frame: %d/%d", framenum, lastframe);
    if (time)
        len += snprintf(string+len, sizeof(string)-len, " time: %d", time);
    if (gain)
        len += snprintf(string+len, sizeof(string)-len, " gain: %d", gain);
    if (threshold)
        len += snprintf(string+len, sizeof(string)-len, " threshold: %d", threshold);

    /* centre the frame number text box */
    int x0 = x + win->ximage->width/2 - XTextWidth(win->font[0], string, len)/2;
    int y0 = y + 20;

    /* display the frame number text box */
    XDrawImageString(win->display, win->pixmap, win->gc, x0, y0, string, strlen(string));
}

void display_name(struct rvwin *win, int x, int y, const char *name, int numimages)
{
    char string[32];

    /* set the drawing attributes in the graphics context */
    XSetForeground(win->display, win->gc, WhitePixel(win->display, DefaultScreen(win->display)));

    /* set the large font in the graphics context */
    if (!XSetFont(win->display, win->gc, win->font[0]->fid))
        rvmessage("failed to set the font in the graphics context: %s", win->fontname[0][0]);

    /* prepare the string for display */
    int len = snprintf(string, sizeof(string), "%s", get_basename(name));

    /* place the text at the top middle of each image */
    int x0 = x + win->ximage->width/2 - XTextWidth(win->font[0], string, len)/2;
    int y0 = y + 40;

    /* display the frame number text box */
    XDrawImageString(win->display, win->pixmap, win->gc, x0, y0, string, strlen(string));
}

void display_psnr(struct rvwin *win, int x, int y, struct rvstats stats)
{
    char string[48];

    /* set the drawing attributes in the graphics context */
    XSetForeground(win->display, win->gc, WhitePixel(win->display, DefaultScreen(win->display)));

    /* set the large font in the graphics context */
    if (!XSetFont(win->display, win->gc, win->font[0]->fid))
        rvmessage("failed to set the font in the graphics context: %s", win->fontname[0][0]);

    /* prepare the string for display */
    int len;
    if (stats.ssim>0)
        len = snprintf(string, sizeof(string), "psnr: %.2fdB mse: %.2f ssim: %.2f maxdiff: %d", stats.psnr, stats.mse, stats.ssim, stats.maxdiff);
    else
        len = snprintf(string, sizeof(string), "psnr: %.2fdB mse: %.2f maxdiff: %d", stats.psnr, stats.mse, stats.maxdiff);

    /* place the text at the bottom middle of each image */
    int x0 = x + win->ximage->width/2 - XTextWidth(win->font[0], string, len)/2;
    int y0 = y + win->ximage->height - 20;

    /* display the frame number text box */
    XDrawImageString(win->display, win->pixmap, win->gc, x0, y0, string, strlen(string));
}

void display_stat(struct rvwin *win, int x, int y, struct rvstats stats)
{
    char string[48];

    /* set the drawing attributes in the graphics context */
    XSetForeground(win->display, win->gc, WhitePixel(win->display, DefaultScreen(win->display)));

    /* set the large font in the graphics context */
    if (!XSetFont(win->display, win->gc, win->font[0]->fid))
        rvmessage("failed to set the font in the graphics context: %s", win->fontname[0][0]);

    /* prepare the string for display */
    int len = snprintf(string, sizeof(string), "mean: %.2f var: %.2f skew: %.2f", stats.mean, stats.var, stats.skew);

    /* place the text at the bottom middle of each image */
    int x0 = x + win->ximage->width/2 - XTextWidth(win->font[0], string, len)/2;
    int y0 = y + win->ximage->height - 20;

    /* display the frame number text box */
    XDrawImageString(win->display, win->pixmap, win->gc, x0, y0, string, strlen(string));
}

void display_prob(struct rvwin *win, int x, int y, double entropy, double mean)
{
    char string[64];

    /* set the drawing attributes in the graphics context */
    XSetForeground(win->display, win->gc, WhitePixel(win->display, DefaultScreen(win->display)));

    /* set the large font in the graphics context */
    if (!XSetFont(win->display, win->gc, win->font[0]->fid))
        rvmessage("failed to set the font in the graphics context: %s", win->fontname[0][0]);

    /* prepare the string for display */
    int len = snprintf(string, sizeof(string), "entropy: %.2f bits/pel, mean pel: %.1f", entropy, mean);

    /* place the text at the bottom middle of each image */
    int x0 = x + win->ximage->width/2 - XTextWidth(win->font[0], string, len)/2;
    int y0 = y + win->ximage->height - 40;

    /* display the frame number text box */
    XDrawImageString(win->display, win->pixmap, win->gc, x0, y0, string, strlen(string));
}

void display_timecode(struct rvwin *win, int x, int y, int hours, int mins, int secs, int frames)
{
    char string[32];

    /* set the drawing attributes in the graphics context */
    XSetForeground(win->display, win->gc, WhitePixel(win->display, DefaultScreen(win->display)));

    /* set the large font in the graphics context */
    if (!XSetFont(win->display, win->gc, win->font[0]->fid))
        rvmessage("failed to set the font in the graphics context: %s", win->fontname[0][0]);

    /* prepare the string for display */
    int len = snprintf(string, sizeof(string), "timecode: %02d:%02d:%02d:%02d", hours, mins, secs, frames);

    /* centre the timecode text box */
    int x0 = x + win->ximage->width/2 - XTextWidth(win->font[0], string, len)/2;
    int y0 = y + win->ximage->height - 60;

    /* display the timecode text box */
    XDrawImageString(win->display, win->pixmap, win->gc, x0, y0, string, strlen(string));
}

void display_m2v_params(struct rvwin *win, int x, int y, struct rvm2vpicture *picture)
{
    char string[48];

    /* sanity check */
    if (picture==NULL)
        return;

    /* set the drawing attributes in the graphics context */
    XSetForeground(win->display, win->gc, WhitePixel(win->display, DefaultScreen(win->display)));

    /* set the large font in the graphics context */
    if (!XSetFont(win->display, win->gc, win->font[0]->fid))
        rvmessage("failed to set the font in the graphics context: %s", win->fontname[0][0]);

    /* get the font metrics of the large font */
    int dir, ascent, descent;
    XCharStruct cs;
    strncpy(string, "0123456789", sizeof(string));
    XTextExtents(win->font[0], string, strlen(string), &dir, &ascent, &descent, &cs);
    int line = ascent + descent;

    /* loop over each string for display */
    int i, j;
    for (i=0, j=0; i<16; i++) {
        const char describe_pct[4] = { ' ', 'i', 'p', 'b' };
        /* prepare the strings for display */
        int len;
        switch (i) {
            case  0: len = snprintf(string, sizeof(string), "picture_coding_type=%c-%s", describe_pct[picture->picture_coding_type], picture->extension.picture_structure==3? "frame" : "field"); break;
            case  1: len = snprintf(string, sizeof(string), "vbv_delay=0x%04x", picture->vbv_delay); break;
            case  2: if (picture->picture_coding_type==PCT_I) continue;
                     len = snprintf(string, sizeof(string), "f_code[0][0]=%d", picture->extension.f_code[0][0]%15); break;
            case  3: if (picture->picture_coding_type==PCT_I) continue;
                     len = snprintf(string, sizeof(string), "f_code[0][1]=%d", picture->extension.f_code[0][1]%15); break;
            case  4: if (picture->picture_coding_type!=PCT_B) continue;
                     len = snprintf(string, sizeof(string), "f_code[1][0]=%d", picture->extension.f_code[1][0]%15); break;
            case  5: if (picture->picture_coding_type!=PCT_B) continue;
                     len = snprintf(string, sizeof(string), "f_code[1][1]=%d", picture->extension.f_code[1][1]%15); break;
            case  6: len = snprintf(string, sizeof(string), "intra_dc_precision=%d", picture->extension.intra_dc_precision); break;
            case  7: len = snprintf(string, sizeof(string), "q_scale_type=%d", picture->extension.q_scale_type); break;
            case  8: len = snprintf(string, sizeof(string), "intra_vlc_format=%d", picture->extension.intra_vlc_format); break;
            case  9: len = snprintf(string, sizeof(string), "alternate_scan=%d", picture->extension.alternate_scan); break;
            case 10: len = snprintf(string, sizeof(string), "progressive_frame=%d", picture->extension.progressive_frame); break;
            case 11: len = snprintf(string, sizeof(string), "top_field_first=%d", picture->extension.top_field_first); break;
            case 12: len = snprintf(string, sizeof(string), "load_intra_quantiser_matrix=%d", picture->quantmatrix.load_intra_quantiser_matrix); break;
            case 13: len = snprintf(string, sizeof(string), "load_non_intra_quantiser_matrix=%d", picture->quantmatrix.load_non_intra_quantiser_matrix); break;
            case 14: len = snprintf(string, sizeof(string), "size_in_bytes=%jd", (intmax_t)picture->bytes); break;
            case 15: len = snprintf(string, sizeof(string), "stuffing_bytes=%jd", (intmax_t)picture->stuffing); break;
        }

        /* put the picture parameters in the top right */
        int x0 = x + win->ximage->width - XTextWidth(win->font[0], string, len) - 10;
        int y0 = y + line + line*j++ + 10;

        /* display the picture parameters */
        XDrawImageString(win->display, win->pixmap, win->gc, x0, y0, string, len);
    }
}

void display_m2v_ratios(struct rvwin *win, int x, int y, struct rvm2vsequence *sequence, struct rvm2vpicture *picture)
{
    char string[32];

    /* sanity check */
    if (picture==NULL)
        return;

    /* measure macroblock type ratios */
    int num_mb;
    int num_skip = 0, num_intra = 0, num_bidir = 0, num_forw = 0, num_back = 0, num_nome = 0;
    int qp_sum = 0;
    for (num_mb=0; num_mb<sequence->width_in_mbs*sequence->height_in_mbs; num_mb++) {
        struct rvm2vmacro *macro = &picture->macro[num_mb];
        if (macro->skipped)
            num_skip++;
        else if (macroblock_intra(macro->type))
            num_intra++;
        else if (macroblock_motion_forward(macro->type) && macroblock_motion_backward(macro->type))
            num_bidir++;
        else if (macroblock_motion_forward(macro->type))
            num_forw++;
        else if (macroblock_motion_backward(macro->type))
            num_back++;
        else if (macroblock_pattern(macro->type))
            num_nome++;
        qp_sum += macro->quantiser_scale_code;
    }

    /* set the drawing attributes in the graphics context */
    XSetForeground(win->display, win->gc, WhitePixel(win->display, DefaultScreen(win->display)));

    /* set the large font in the graphics context */
    if (!XSetFont(win->display, win->gc, win->font[0]->fid))
        rvmessage("failed to set the font in the graphics context: %s", win->fontname[0][0]);

    /* get the font metrics of the large font */
    int dir, ascent, descent;
    XCharStruct cs;
    strncpy(string, "0123456789", sizeof(string));
    XTextExtents(win->font[0], string, strlen(string), &dir, &ascent, &descent, &cs);
    int line = ascent + descent;

    /* loop over each string for display */
    int i, j;
    for (i=0, j=0; i<7; i++) {
        /* prepare the strings for display */
        int len;
        switch (i) {
            case  0: len = snprintf(string, sizeof(string), "avg_qp=%.1f", (double)qp_sum/num_mb); break;
            case  1: if (picture->picture_coding_type==PCT_I) continue;
                     len = snprintf(string, sizeof(string), "skipped=%.1f%%", num_skip*100.0/num_mb); break;
            case  2: len = snprintf(string, sizeof(string), "intra=%.1f%%", num_intra*100.0/num_mb); break;
            case  3: if (picture->picture_coding_type!=PCT_B) continue;
                     len = snprintf(string, sizeof(string), "bidir=%.1f%%", num_bidir*100.0/num_mb); break;
            case  4: if (picture->picture_coding_type==PCT_I) continue;
                     len = snprintf(string, sizeof(string), "forward=%.1f%%", num_forw*100.0/num_mb); break;
            case  5: if (picture->picture_coding_type!=PCT_B) continue;
                     len = snprintf(string, sizeof(string), "backward=%.1f%%", num_back*100.0/num_mb); break;
            case  6: if (picture->picture_coding_type!=PCT_P) continue;
                     len = snprintf(string, sizeof(string), "no-me=%.1f%%", num_nome*100.0/num_mb); break;
        }

        /* put the picture parameters in the top right */
        int x0 = x + win->ximage->width - XTextWidth(win->font[0], string, len) - 10;
        int y0 = y + win->ximage->height/4 + line + line*j++;

        /* display the picture parameters */
        XDrawImageString(win->display, win->pixmap, win->gc, x0, y0, string, len);
    }
}

void display_m2v_quantmat(struct rvwin *win, int x, int y, struct rvm2vpicture *picture)
{
    char string[32];

    /* sanity check */
    if (picture==NULL)
        return;

    /* set the drawing attributes in the graphics context */
    XSetForeground(win->display, win->gc, WhitePixel(win->display, DefaultScreen(win->display)));

    /* set the large font in the graphics context */
    if (!XSetFont(win->display, win->gc, win->font[0]->fid))
        rvmessage("failed to set the font in the graphics context: %s", win->fontname[0][0]);

    /* get the font metrics of the large font */
    int dir, ascent, descent;
    XCharStruct cs;
    strncpy(string, "0123456789", sizeof(string));
    XTextExtents(win->font[0], string, strlen(string), &dir, &ascent, &descent, &cs);
    int line = ascent + descent;

    /* loop over each string for display */
    int i, row = 0;
    if (picture->quantmatrix.load_intra_quantiser_matrix)
        for (i=0; i<8; i++) {
            int *q = &picture->quantmatrix.intra_quantiser_matrix[i*8];

            /* prepare the strings for display */
            int len = snprintf(string, sizeof(string), "%2d,%2d,%2d,%2d,%2d,%2d,%2d,%2d",
                            q[0], q[1], q[2], q[3], q[4], q[5], q[6], q[7]);

            /* put the picture parameters in the top left */
            int x0 = x + 10;
            int y0 = y + (++row)*line + 10;

            /* display the picture parameters */
            XDrawImageString(win->display, win->pixmap, win->gc, x0, y0, string, len);
        }
    if (picture->quantmatrix.load_non_intra_quantiser_matrix)
        for (i=0; i<8; i++) {
            int *q = &picture->quantmatrix.non_intra_quantiser_matrix[i*8];

            /* prepare the strings for display */
            int len = snprintf(string, sizeof(string), "%2d,%2d,%2d,%2d,%2d,%2d,%2d,%2d",
                            q[0], q[1], q[2], q[3], q[4], q[5], q[6], q[7]);

            /* put the picture parameters in the top left */
            int x0 = x + 10;
            int y0 = y + line + (++row)*line + 10;

            /* display the picture parameters */
            XDrawImageString(win->display, win->pixmap, win->gc, x0, y0, string, len);
        }
#if 0
    if (picture->quantmatrix.load_chroma_intra_quantiser_matrix)
        for (i=0; i<8; i++) {
            int *q = &picture->quantmatrix.chroma_intra_quantiser_matrix[i*8];

            /* prepare the strings for display */
            int len = snprintf(string, sizeof(string), "%2d,%2d,%2d,%2d,%2d,%2d,%2d,%2d",
                            q[0], q[1], q[2], q[3], q[4], q[5], q[6], q[7]);

            /* put the picture parameters in the top left */
            int x0 = x + 10;
            int y0 = y + (++row)*line + 10;

            /* display the picture parameters */
            XDrawImageString(win->display, win->pixmap, win->gc, x0, y0, string, len);
        }
    if (picture->quantmatrix.load_chroma_non_intra_quantiser_matrix)
        for (i=0; i<8; i++) {
            int *q = &picture->quantmatrix.chroma_non_intra_quantiser_matrix[i*8];

            /* prepare the strings for display */
            int len = snprintf(string, sizeof(string), "%2d,%2d,%2d,%2d,%2d,%2d,%2d,%2d",
                            q[0], q[1], q[2], q[3], q[4], q[5], q[6], q[7]);

            /* put the picture parameters in the top left */
            int x0 = x + 10;
            int y0 = y + line + (++row)*line + 10;

            /* display the picture parameters */
            XDrawImageString(win->display, win->pixmap, win->gc, x0, y0, string, len);
        }
#endif
}

void display_m2v_macro(struct rvwin *win, int x, int y, struct rvm2vmacro *macro)
{
    char string[32];

    /* sanity check */
    if (macro==NULL)
        return;

    /* set the drawing attributes in the graphics context */
    XSetForeground(win->display, win->gc, WhitePixel(win->display, DefaultScreen(win->display)));

    /* set the large font in the graphics context */
    if (!XSetFont(win->display, win->gc, win->font[0]->fid))
        rvmessage("failed to set the font in the graphics context: %s", win->fontname[0][0]);

    /* get the font metrics of the large font */
    int dir, ascent, descent;
    XCharStruct cs;
    strncpy(string, "0123456789", sizeof(string));
    XTextExtents(win->font[0], string, strlen(string), &dir, &ascent, &descent, &cs);
    int line = ascent + descent;

    /* loop over each string for display */
    int i, j;
    for (i=0, j=0; i<11; i++) {
        /* prepare the strings for display */
        int len;
        switch (i) {
            case  0: len = snprintf(string, sizeof(string), "mb_addr=%d", macro->addr); break;
            case  1: len = snprintf(string, sizeof(string), "mb_type=%s", macro->skipped? "skipped" :
                                                                          macroblock_intra(macro->type)? "intra" :
                                                                          macroblock_motion_forward(macro->type) && macroblock_motion_backward(macro->type)? "bidir" :
                                                                          macroblock_motion_forward(macro->type)? "forward" :
                                                                          macroblock_motion_backward(macro->type)? "backward" :
                                                                          macroblock_pattern(macro->type)? "no-mc" : "fixme");
                                                                          break;
            case  2: len = snprintf(string, sizeof(string), "dct_type=%d", macro->dct_type); break;
            case  3: len = snprintf(string, sizeof(string), "quant=%d%s", macro->quantiser_scale_code, macroblock_quant(macro->type)? "(new)":""); break;
            case  4: if (!macroblock_motion_forward(macro->type)) continue;
                     len = snprintf(string, sizeof(string), "forw mv[0]=%d,%d", macro->vector[0][0].x, macro->vector[0][0].y); break;
            case  5: if (!macroblock_motion_forward(macro->type)) continue;
                     if (motion_vector_count(macro->frame_motion_type, macro->field_motion_type)==1) continue;
                     len = snprintf(string, sizeof(string), "forw mv[1]=%d,%d", macro->vector[1][0].x, macro->vector[1][0].y); break;
            case  6: if (!macroblock_motion_backward(macro->type)) continue;
                     len = snprintf(string, sizeof(string), "back mv[0]=%d,%d", macro->vector[0][1].x, macro->vector[0][1].y); break;
            case  7: if (!macroblock_motion_backward(macro->type)) continue;
                     if (motion_vector_count(macro->frame_motion_type, macro->field_motion_type)==1) continue;
                     len = snprintf(string, sizeof(string), "back mv[1]=%d,%d", macro->vector[1][1].x, macro->vector[1][1].y); break;
            case  8: len = snprintf(string, sizeof(string), "cbp=0x%x", macro->pattern_code); break;
            case  9: len = snprintf(string, sizeof(string), "num_bits=%d", macro->num_bits); break;
            case 10: len = snprintf(string, sizeof(string), "stuff_bits=%d", macro->stuff_bits); break;
        }

        /* put the picture parameters in the top left */
        int x0 = x + 35;
        int y0 = y + line + line*j++ + 10;

        /* display the picture parameters */
        XDrawImageString(win->display, win->pixmap, win->gc, x0, y0, string, len);
    }
}

void display_m2v_coeff(struct rvwin *win, int x, int y, struct rvm2vmacro *macro)
{
    char string[256];

    /* sanity check */
    if (macro==NULL)
        return;

    /* set the drawing attributes in the graphics context */
    XSetForeground(win->display, win->gc, WhitePixel(win->display, DefaultScreen(win->display)));

    /* set the small font in the graphics context */
    if (!XSetFont(win->display, win->gc, win->font[1]->fid))
        rvmessage("failed to set the font in the graphics context: %s", win->fontname[1][0]);

    /* get the font metrics of the small font */
    int dir, ascent, descent;
    XCharStruct cs;
    strncpy(string, "0123456789", sizeof(string));
    XTextExtents(win->font[1], string, strlen(string), &dir, &ascent, &descent, &cs);
    int line = ascent + descent;

    /* loop over block of coefficients */
    int i, j, row = 0;
    for (i=0; i<6; i++) {
        coeff_t *t = &macro->coeff[i][0];

        /* find last non-zero coeff */
        for (j=63; j>=0; j--)
            if (t[j]!=0)
                break;
        int last = j;

        /* prepare the strings for display */
        int len = 0;
        for (j=0; j<=last; j++)
            len += snprintf(string+len, sizeof(string)-len, "%3d ", t[j]);

        /* put the macroblock coeffs in the mid left */
        int x0 = x + 35;
        int y0 = y + (++row)*line + 180;

        /* display the picture parameters */
        XDrawImageString(win->display, win->pixmap, win->gc, x0, y0, string, len);
    }
    row++;

    /* loop over block of decoded values */
    for (i=0; i<6; i++) {
        for (j=0; j<8; j++) {
            coeff_t *t = &macro->pixel[i][j*8];

            /* prepare the strings for display */
            int len = snprintf(string, sizeof(string), "%3d%3d%3d%3d%3d%3d%3d%3d",
                            t[0], t[1], t[2], t[3], t[4], t[5], t[6], t[7]);

            /* put the macroblock coeffs in the mid left */
            int x0 = x + 35;
            int y0 = y + (++row)*line + 180;

            /* display the picture parameters */
            XDrawImageString(win->display, win->pixmap, win->gc, x0, y0, string, len);
        }
        row++;
    }
}

void display_m2v_overlay(struct rvwin *win, int x, int y, int width, int height, int zoom, int drawmode, struct rvm2vpicture *toppicture, struct rvm2vpicture *botpicture)
{
    int len, wid;
    char string[32];
    struct rvm2vpicture *picture = toppicture;

    int num_macros = width*height/256;
    int num_pics = 1;

    /* sanity check */
    if (toppicture==NULL)
        return;

    /* show field pictures as over and under */
    if (field_picture(picture)) {
        num_macros /= 2;
        num_pics *=2;
        /* sanity check */
        if (botpicture==NULL)
            return;
    }

    /* select the items to draw */
    const int draw_mv_nums    = drawmode & DRAWMODE_MVS;
    const int draw_mv_vec     = drawmode & DRAWMODE_VEC;
    const int draw_mv_sel     = drawmode & DRAWMODE_REF;
    const int draw_status_bar = drawmode & DRAWMODE_BAR;
    const int draw_dct_mode   = drawmode & DRAWMODE_DCT;
    const int draw_numbits    = drawmode & DRAWMODE_NUM;
    const int draw_decoded    = drawmode & DRAWMODE_DEC;

    /* set the drawing attributes in the graphics context */
    if (!XSetLineAttributes(win->display, win->gc, 1, LineSolid, CapButt, JoinMiter))
        rvmessage("failed to set the line width to %d in the graphics context", 1);
    XSetForeground(win->display, win->gc, WhitePixel(win->display, DefaultScreen(win->display)));

    /* set the small font in the graphics context */
    if (!XSetFont(win->display, win->gc, win->font[1]->fid))
        rvmessage("failed to set the font in the graphics context: %s", win->fontname[1][0]);

    /* get the font metrics of the small font */
    int dir, ascent, descent;
    XCharStruct cs;
    strncpy(string, "0123456789", sizeof(string));
    XTextExtents(win->font[1], string, strlen(string), &dir, &ascent, &descent, &cs);
    int line = ascent + descent;
    int hoff = (descent - ascent)/2;

    /* loop over macroblocks in frame */
    int i, n;
    for (i=0; i<num_pics; i++) {
        for (n=0; n<num_macros; n++) {

            /* calculate coordinates of macroblock origin */
            int x0 = (n % (width/16)) * 16 * zoom;
            int y0 = (((n / (width/16)) * 16) + i*height/2) * zoom;

            /* add image  offset */
            x0 += x;
            y0 += y;

            /* calculate coordinates of macroblock centre */
            int cx = x0 + 8*zoom;
            int cy = y0 + 8*zoom;

            /* calculate coordinates of macroblock right bottom */
            int bx = x0 + 16*zoom;
            int by = y0 + 16*zoom;

            /* calculate coefficients of macroblock inside edges */
            //int lx = x0 + zoom;
            //int ly = y0 + zoom;
            //int rx = x0 + 14*zoom;
            //int ry = y0 + 14*zoom;

            /* draw field coding overlay */
            if (draw_dct_mode && picture->macro[n].dct_type) {
                int y1, i=0;
                int size = 2;

                XSegment *segments = (XSegment *)rvalloc(NULL, 7*sizeof(XSegment), 0);
                if (segments==NULL)
                    return;

                /* draw the grid */
                for (y1=size*zoom; y1<=14*zoom; y1+=size*zoom, i++) {
                    segments[i].x1 = x0+2*zoom;
                    segments[i].y1 = y0+y1;
                    segments[i].x2 = x0+14*zoom;
                    segments[i].y2 = y0+y1;
                }

                XDrawSegments(win->display, win->pixmap, win->gc, segments, i);

                free(segments);
            }

            if (picture->picture_coding_type==PCT_I) {
                if (picture->extension.concealment_motion_vectors) {
                    if (draw_mv_nums && !draw_decoded) {
                        /* display predicted vectors */
                        len = snprintf(string, sizeof(string), "%d,%d", picture->macro[n].delta[0][0].x, picture->macro[n].delta[0][0].y);
                        wid = XTextWidth(win->font[1], string, len);
                        XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff, string, len);
                    }
                    if (draw_mv_nums && draw_decoded) {
                        /* display decoded vectors */
                        len = snprintf(string, sizeof(string), "%d,%d", picture->macro[n].vector[0][0].x, picture->macro[n].vector[0][0].y);
                        wid = XTextWidth(win->font[1], string, len);
                        XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff, string, len);
                    }
                    if (draw_mv_vec) {
                        /* draw decoded vectors */
                        if (picture->macro[n].vector[0][0].x || picture->macro[n].vector[0][0].y)
                            XDrawLine(win->display, win->pixmap, win->gc, cx, cy, cx + (picture->macro[n].vector[0][0].x*zoom)/2, cy + (picture->macro[n].vector[0][0].y*zoom)/2);
                    }
                }
            }

            //shade_image(&image, x, y, BLUE);
            if (picture->picture_coding_type==PCT_P) {
                if (macroblock_motion_forward(picture->macro[n].type) || (macroblock_intra(picture->macro[n].type) && picture->extension.concealment_motion_vectors)) {
                    int dual_prime = dmv(picture->macro[n].frame_motion_type, picture->macro[n].field_motion_type);
                    int num_vectors = motion_vector_count(picture->macro[n].frame_motion_type, picture->macro[n].field_motion_type);
                    if (num_vectors==1) {
                        if (draw_mv_nums && !draw_decoded) {
                            /* display predicted vectors */
                            len = snprintf(string, sizeof(string), "%d,%d", picture->macro[n].delta[0][0].x, picture->macro[n].delta[0][0].y);
                            if (dual_prime)
                                len += snprintf(string+len, sizeof(string)-len, ",%+d,%+d", picture->macro[n].dmvector.x, picture->macro[n].dmvector.y);
                            wid = XTextWidth(win->font[1], string, len);
                            XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff, string, len);
                        }
                        if (draw_mv_nums && draw_decoded) {
                            /* display decoded vectors */
                            len = snprintf(string, sizeof(string), "%d,%d", picture->macro[n].vector[0][0].x, picture->macro[n].vector[0][0].y);
                            if (dual_prime) {
                                len += snprintf(string+len, sizeof(string)-len, ",%d,%d", picture->macro[n].vector[2][0].x, picture->macro[n].vector[2][0].y);
                                if (!field_picture(picture))
                                    len += snprintf(string+len, sizeof(string)-len, ",%d,%d", picture->macro[n].vector[3][0].x, picture->macro[n].vector[3][0].y);
                            }
                            wid = XTextWidth(win->font[1], string, len);
                            XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff, string, len);
                        }
                        if (draw_mv_vec) {
                            /* draw decoded vectors */
                            if (picture->macro[n].vector[0][0].x || picture->macro[n].vector[0][0].y)
                                XDrawLine(win->display, win->pixmap, win->gc, cx, cy, cx + (picture->macro[n].vector[0][0].x*zoom)/2, cy + (picture->macro[n].vector[0][0].y*zoom)/2);
                        }
                    } else if (num_vectors==2) {
                        if (draw_mv_sel) {
                            XDrawLine(win->display, win->pixmap, win->gc, x0, cy, bx, cy);
                            len = snprintf(string, sizeof(string), "%c", picture->macro[n].motion_vertical_field_select[0][0]? 'B' : 'T');
                            XDrawImageString(win->display, win->pixmap, win->gc, x0, y0+ascent, string, len);
                            len = snprintf(string, sizeof(string), "%c", picture->macro[n].motion_vertical_field_select[1][0]? 'B' : 'T');
                            XDrawImageString(win->display, win->pixmap, win->gc, x0, cy+ascent, string, len);
                        }
                        if (draw_mv_nums && !draw_decoded) {
                            /* display predicted vectors */
                            len = snprintf(string, sizeof(string), "%d,%d", picture->macro[n].delta[0][0].x, picture->macro[n].delta[0][0].y);
                            wid = XTextWidth(win->font[1], string, len);
                            XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff-4*zoom, string, len);
                            len = snprintf(string, sizeof(string), "%d,%d", picture->macro[n].delta[1][0].x, picture->macro[n].delta[1][0].y);
                            wid = XTextWidth(win->font[1], string, len);
                            XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff+4*zoom, string, len);
                        }
                        if (draw_mv_nums && draw_decoded) {
                            /* display decoded vectors */
                            len = snprintf(string, sizeof(string), "%d,%d", picture->macro[n].vector[0][0].x, picture->macro[n].vector[0][0].y);
                            wid = XTextWidth(win->font[1], string, len);
                            XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff-4*zoom, string, len);
                            len = snprintf(string, sizeof(string), "%d,%d", picture->macro[n].vector[1][0].x, picture->macro[n].vector[1][0].y);
                            wid = XTextWidth(win->font[1], string, len);
                            XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff+4*zoom, string, len);
                        }
                        if (draw_mv_vec) {
                            /* draw decoded vectors */
                            if (picture->macro[n].vector[0][0].x || picture->macro[n].vector[0][0].y)
                                XDrawLine(win->display, win->pixmap, win->gc, cx, cy-4*zoom, cx + (picture->macro[n].vector[0][0].x*zoom)/2, cy-4*zoom + (picture->macro[n].vector[0][0].y*zoom)/2);
                            if (picture->macro[n].vector[1][0].x || picture->macro[n].vector[1][0].y)
                                XDrawLine(win->display, win->pixmap, win->gc, cx, cy+4*zoom, cx + (picture->macro[n].vector[1][0].x*zoom)/2, cy+4*zoom + (picture->macro[n].vector[1][0].y*zoom)/2);
                        }
                    }
                }
            }

            if (picture->picture_coding_type==PCT_B && !picture->macro[n].skipped) {
                if (macroblock_motion_forward(picture->macro[n].type) || (macroblock_intra(picture->macro[n].type) && picture->extension.concealment_motion_vectors)) {
                    int num_vectors = motion_vector_count(picture->macro[n].frame_motion_type, picture->macro[n].field_motion_type);
                    if (num_vectors==1) {
                        if (draw_mv_nums && !draw_decoded) {
                            /* display predicted vectors */
                            len = snprintf(string, sizeof(string), "%d,%d", picture->macro[n].delta[0][0].x, picture->macro[n].delta[0][0].y);
                            wid = XTextWidth(win->font[1], string, len);
                            XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff-line/2, string, len);
                        }
                        if (draw_mv_nums && draw_decoded) {
                            /* display decoded vectors */
                            len = snprintf(string, sizeof(string), "%d,%d", picture->macro[n].vector[0][0].x, picture->macro[n].vector[0][0].y);
                            wid = XTextWidth(win->font[1], string, len);
                            XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff-line/2, string, len);
                        }
                        if (draw_mv_vec) {
                            /* draw decoded vectors */
                            if (picture->macro[n].vector[0][0].x || picture->macro[n].vector[0][0].y)
                                XDrawLine(win->display, win->pixmap, win->gc, cx, cy, cx + (picture->macro[n].vector[0][0].x*zoom)/2, cy + (picture->macro[n].vector[0][0].y*zoom)/2);
                        }
                    } else if (num_vectors==2) {
                        if (draw_mv_sel) {
                            XDrawLine(win->display, win->pixmap, win->gc, x0, cy, bx, cy);
                            len = snprintf(string, sizeof(string), "%c", picture->macro[n].motion_vertical_field_select[0][0]? 'B' : 'T');
                            XDrawImageString(win->display, win->pixmap, win->gc, x0, y0+ascent, string, len);
                            len = snprintf(string, sizeof(string), "%c", picture->macro[n].motion_vertical_field_select[1][0]? 'B' : 'T');
                            XDrawImageString(win->display, win->pixmap, win->gc, x0, cy+ascent, string, len);
                        }
                        if (draw_mv_nums && !draw_decoded) {
                            /* display predicted vectors */
                            len = snprintf(string, sizeof(string), "%d,%d", picture->macro[n].delta[0][0].x, picture->macro[n].delta[0][0].y);
                            wid = XTextWidth(win->font[1], string, len);
                            XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff-4*zoom-line/2, string, len);
                            len = snprintf(string, sizeof(string), "%d,%d", picture->macro[n].delta[1][0].x, picture->macro[n].delta[1][0].y);
                            wid = XTextWidth(win->font[1], string, len);
                            XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff+4*zoom-line/2, string, len);
                        }
                        if (draw_mv_nums && draw_decoded) {
                            /* display decoded vectors */
                            len = snprintf(string, sizeof(string), "%d,%d", picture->macro[n].vector[0][0].x, picture->macro[n].vector[0][0].y);
                            wid = XTextWidth(win->font[1], string, len);
                            XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff-4*zoom-line/2, string, len);
                            len = snprintf(string, sizeof(string), "%d,%d", picture->macro[n].vector[1][0].x, picture->macro[n].vector[1][0].y);
                            wid = XTextWidth(win->font[1], string, len);
                            XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff+4*zoom-line/2, string, len);
                        }
                        if (draw_mv_vec) {
                            /* draw decoded vectors */
                            if (picture->macro[n].vector[0][0].x || picture->macro[n].vector[0][0].y)
                                XDrawLine(win->display, win->pixmap, win->gc, cx, cy-4*zoom, cx + (picture->macro[n].vector[0][0].x*zoom)/2, cy-4*zoom + (picture->macro[n].vector[0][0].y*zoom)/2);
                            if (picture->macro[n].vector[1][0].x || picture->macro[n].vector[1][0].y)
                                XDrawLine(win->display, win->pixmap, win->gc, cx, cy+4*zoom, cx + (picture->macro[n].vector[1][0].x*zoom)/2, cy+4*zoom + (picture->macro[n].vector[1][0].y*zoom)/2);
                        }
                    }
                }
                if (macroblock_motion_backward(picture->macro[n].type)) {
                    int num_vectors = motion_vector_count(picture->macro[n].frame_motion_type, picture->macro[n].field_motion_type);
                    if (num_vectors==1) {
                        if (draw_mv_nums && !draw_decoded) {
                            /* display predicted vectors */
                            len = snprintf(string, sizeof(string), "%d,%d", picture->macro[n].delta[0][1].x, picture->macro[n].delta[0][1].y);
                            wid = XTextWidth(win->font[1], string, len);
                            XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff+line/2, string, len);
                        }
                        if (draw_mv_nums && draw_decoded) {
                            /* display decoded vectors */
                            len = snprintf(string, sizeof(string), "%d,%d", picture->macro[n].vector[0][1].x, picture->macro[n].vector[0][1].y);
                            wid = XTextWidth(win->font[1], string, len);
                            XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff+line/2, string, len);
                        }
                        if (draw_mv_vec) {
                            /* draw decoded vectors */
                            if (picture->macro[n].vector[0][1].x || picture->macro[n].vector[0][1].y)
                                XDrawLine(win->display, win->pixmap, win->gc, cx, cy, cx + (picture->macro[n].vector[0][1].x*zoom)/2, cy + (picture->macro[n].vector[0][1].y*zoom)/2);
                        }
                    } else if (num_vectors==2) {
                        if (draw_mv_sel) {
                            XDrawLine(win->display, win->pixmap, win->gc, x0, cy, bx, cy);
                            len = snprintf(string, sizeof(string), "%c", picture->macro[n].motion_vertical_field_select[0][1]? 'B' : 'T');
                            XDrawImageString(win->display, win->pixmap, win->gc, x0, y0+2*ascent, string, len);
                            len = snprintf(string, sizeof(string), "%c", picture->macro[n].motion_vertical_field_select[1][1]? 'B' : 'T');
                            XDrawImageString(win->display, win->pixmap, win->gc, x0, cy+2*ascent, string, len);
                        }
                        if (draw_mv_nums && !draw_decoded) {
                            /* display predicted vectors */
                            len = snprintf(string, sizeof(string), "%d,%d", picture->macro[n].delta[0][1].x, picture->macro[n].delta[0][1].y);
                            wid = XTextWidth(win->font[1], string, len);
                            XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff-4*zoom+line/2, string, len);
                            len = snprintf(string, sizeof(string), "%d,%d", picture->macro[n].delta[1][1].x, picture->macro[n].delta[1][1].y);
                            wid = XTextWidth(win->font[1], string, len);
                            XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff+4*zoom+line/2, string, len);
                        }
                        if (draw_mv_nums && draw_decoded) {
                            /* display decoded vectors */
                            len = snprintf(string, sizeof(string), "%d,%d", picture->macro[n].vector[0][1].x, picture->macro[n].vector[0][1].y);
                            wid = XTextWidth(win->font[1], string, len);
                            XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff-4*zoom+line/2, string, len);
                            len = snprintf(string, sizeof(string), "%d,%d", picture->macro[n].vector[1][1].x, picture->macro[n].vector[1][1].y);
                            wid = XTextWidth(win->font[1], string, len);
                            XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff+4*zoom+line/2, string, len);
                        }
                        if (draw_mv_vec) {
                            /* draw decoded vectors */
                            if (picture->macro[n].vector[0][1].x || picture->macro[n].vector[0][1].y)
                                XDrawLine(win->display, win->pixmap, win->gc, cx, cy-4*zoom, cx + (picture->macro[n].vector[0][1].x*zoom)/2, cy-4*zoom + (picture->macro[n].vector[0][1].y*zoom)/2);
                            if (picture->macro[n].vector[1][1].x || picture->macro[n].vector[1][1].y)
                                XDrawLine(win->display, win->pixmap, win->gc, cx, cy+4*zoom, cx + (picture->macro[n].vector[1][1].x*zoom)/2, cy+4*zoom + (picture->macro[n].vector[1][1].y*zoom)/2);
                        }
                    }
                }
            }

            /* draw "status bar" overlay */
            len = 0;
            if (macroblock_quant(picture->macro[n].type) || (n % (width/16))==0)
                len += snprintf(string+len, sizeof(string)-len, "Qp=%d", picture->macro[n].quantiser_scale_code);

            if (len && draw_status_bar) {
                wid = XTextWidth(win->font[1], string, len);
                XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, by-descent, string, len);
            }

            /* draw stuffing bits overlay */
            len = 0;
            if (picture->macro[n].stuff_bits)
                len += snprintf(string+len, sizeof(string)-len, "%dB", picture->macro[n].stuff_bits/8);

            if (len && draw_status_bar) {
                wid = XTextWidth(win->font[1], string, len);
                XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, by-descent-line, string, len);
            }

            /* draw number of bits */
            len = 0;
            if (picture->macro[n].num_bits)
                len += snprintf(string+len, sizeof(string)-len, "%db", picture->macro[n].num_bits);

            if (len && draw_numbits) {
                wid = XTextWidth(win->font[1], string, len);
                XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, y0+ascent, string, len);
            }

        }
        picture = botpicture;
    }
}

void display_264_params(struct rvwin *win, int x, int y, const struct rv264picture *pic)
{
    char string[48];

    /* sanity check */
    if (pic==NULL)
        return;

    const struct slice_header *sh            = pic->sh[0];
    //const struct rv264pic_parameter_set *pps = sh->pps;
    //const struct hevc_seq_parameter_set *sps = pps->sps;

    /* set the drawing attributes in the graphics context */
    XSetForeground(win->display, win->gc, WhitePixel(win->display, DefaultScreen(win->display)));

    /* set the large font in the graphics context */
    if (!XSetFont(win->display, win->gc, win->font[0]->fid))
        rvmessage("failed to set the font in the graphics context: %s", win->fontname[0][0]);

    /* get the font metrics of the large font */
    int dir, ascent, descent;
    XCharStruct cs;
    strncpy(string, "0123456789", sizeof(string));
    XTextExtents(win->font[0], string, strlen(string), &dir, &ascent, &descent, &cs);
    int line = ascent + descent;

    /* loop over each string for display */
    int i, j;
    for (i=0, j=0; i<6; i++) {
        /* prepare the strings for display */
        int len;
        switch (i) {
            case  0: len = snprintf(string, sizeof(string), "pic_order_count_lsb=%d", sh->pic_order_cnt_lsb); break;
            case  1: len = snprintf(string, sizeof(string), "slice_type=%s", slice_type_name[sh->slice_type]); break;
            case  2: len = snprintf(string, sizeof(string), "field_pic=%s", sh->field_pic_flag? "field" : "frame"); break;
            case  3: len = snprintf(string, sizeof(string), "num_ref_idx_l0_active=%d", sh->num_ref_idx_active[0]); break;
            case  4: len = snprintf(string, sizeof(string), "num_ref_idx_l1_active=%d", sh->num_ref_idx_active[1]); break;
            case  5: len = snprintf(string, sizeof(string), "SliceQPY=%d", sh->SliceQPY); break;
        }

        /* put the picture parameters in the top right */
        int x0 = x + win->ximage->width - XTextWidth(win->font[0], string, len) - 10;
        int y0 = y + line + line*j++ + 10;

        /* display the picture parameters */
        XDrawImageString(win->display, win->pixmap, win->gc, x0, y0, string, len);
    }
}

/* cached font metrics, to avoid calling XTextExtents for every motion vector */
static int dir, ascent, descent;
static XCharStruct cs;

static void display_264_motion_vector(struct rvwin *win, const int16_t mv[2], int cx, int cy, int hoff)
{
    /* display decoded vectors */
    char string[32];
    int len = snprintf(string, sizeof(string), "%d,%d", mv[0], mv[1]);
    int wid = XTextWidth(win->font[1], string, len);
    XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-(descent - ascent)/2+hoff, string, len);
}

void display_264_motion_vectors(struct rvwin *win, const int16_t mv[2], const int16_t mvd[2], int cx, int cy, int zoom, int drawmode, int hoff)
{
    const int draw_mv_vec  = drawmode & DRAWMODE_VEC;
    const int draw_mv_nums = drawmode & DRAWMODE_MVS;
    const int draw_decoded = drawmode & DRAWMODE_DEC;

    if (draw_mv_vec) {
        if (mv)
            if (mv[0] || mv[1])
                XDrawLine(win->display, win->pixmap, win->gc, cx, cy, cx + (mv[0]*zoom)/2, cy + (mv[1]*zoom)/2);
    }
    if (draw_mv_nums) {
        if (draw_decoded) {
            if (mv)
                display_264_motion_vector(win, mv, cx, cy, hoff);
        } else {
            if (mvd)
                display_264_motion_vector(win, mvd, cx, cy, hoff);
        }
    }
}

void display_264_motion_vectors_null(struct rvwin *win, int cx, int cy, int zoom, int drawmode, int hoff)
{
    const int draw_mv_nums = drawmode & DRAWMODE_MVS;

    /* double dash string */
    static const char *doubledash = "-,-";

    if (draw_mv_nums) {
        /* display decoded vectors */
        int wid = XTextWidth(win->font[1], doubledash, 3);
        XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-(descent - ascent)/2+hoff, doubledash, 3);
    }
}

void display_264_overlay(struct rvwin *win, int x, int y, int width, int height, int zoom, int drawmode, struct rv264macro macro[], int mbaff, int field_pic_flag, int bottom_field_flag)
{
    int len, wid;
    char string[32];

    const int num_macros = width*height/(field_pic_flag? 512 : 256);

    /* select the items to draw */
    const int draw_intra_pred = drawmode & DRAWMODE_INT;
    const int draw_mv_nums    = drawmode & DRAWMODE_MVS;
    const int draw_ref_idx    = drawmode & DRAWMODE_REF;
    const int draw_mbaff      = drawmode & DRAWMODE_DCT;
    const int draw_status_bar = drawmode & DRAWMODE_BAR;
    const int draw_decoded    = drawmode & DRAWMODE_DEC;

    /* first put a visual indicator that this is a field picture frame */
    if (field_pic_flag && bottom_field_flag) {
        if (!XSetLineAttributes(win->display, win->gc, 3, LineOnOffDash, CapButt, JoinMiter))
            rvmessage("failed to set the line width to %d in the graphics context", 3);
        XDrawLine(win->display, win->pixmap, win->gc, x, y+height/2*zoom, x+width*zoom, y+height/2*zoom);
    }

    /* set the drawing attributes in the graphics context */
    if (!XSetLineAttributes(win->display, win->gc, 1, LineSolid, CapButt, JoinMiter))
        rvmessage("failed to set the line width to %d in the graphics context", 1);
    XSetForeground(win->display, win->gc, WhitePixel(win->display, DefaultScreen(win->display)));

    /* set the small font in the graphics context */
    if (!XSetFont(win->display, win->gc, win->font[1]->fid))
        rvmessage("failed to set the font in the graphics context: %s", win->fontname[1][0]);

    /* get the font metrics of the small font */
    strncpy(string, "0123456789", sizeof(string));
    XTextExtents(win->font[1], string, strlen(string), &dir, &ascent, &descent, &cs);
    int line = ascent + descent;
    int hoff = (descent - ascent)/2; /* "the hoff" factor */

    /* count slices */
    int prev_slice = -1;

    /* loop over macroblocks in frame */
    int n;
    for (n=0; n<num_macros; n++) {
        int mb_type = macro[n].mb_type;

        /* calculate coordinates of macroblock origin */
        int x0, y0;
        if (mbaff) {
            /* first calculate coordinates of macroblock pair */
            x0 = ((n/2) % (width/16));
            y0 = (n / (width/16));
            /* then fix bottom bits of y */
            if (n&1)
                y0 |= 1;
            else
                y0 &= ~1;
            /* then scale to pixel units */
            x0 *= 16 * zoom;
            y0 *= 16 * zoom;
        } else {
            x0 = (n % (width/16)) * 16 * zoom;
            y0 = (n / (width/16)) * 16 * zoom + bottom_field_flag * (height/32) * 16 * zoom;
        }

        /* add image  offset */
        x0 += x;
        y0 += y;

        /* calculate coordinates of macroblock centre */
        int cx = x0 + 8*zoom;
        int cy = y0 + 8*zoom;

        /* calculate coordinates of macroblock right bottom */
        int bx = x0 + 16*zoom;
        int by = y0 + 16*zoom;

        /* calculate coefficients of macroblock inside edges */
        int lx = x0 + zoom;
        int ly = y0 + zoom;
        int rx = x0 + 14*zoom;
        int ry = y0 + 14*zoom;

        /* draw field coding in frame macroblock overlay */
        if (draw_mbaff && !field_pic_flag && (macro[n].mb_field_decoding_flag || ((n&1) && macro[n-1].mb_field_decoding_flag))) {
            int y1, i=0;
            int size = 2;

            XSegment *segments = (XSegment *)rvalloc(NULL, 7*sizeof(XSegment), 0);
            if (segments==NULL)
                return;

            /* draw the grid */
            for (y1=size*zoom; y1<=14*zoom; y1+=size*zoom, i++) {
                segments[i].x1 = x0+2*zoom;
                segments[i].y1 = y0+y1;
                segments[i].x2 = x0+14*zoom;
                segments[i].y2 = y0+y1;
            }

            XDrawSegments(win->display, win->pixmap, win->gc, segments, i);

            free(segments);
        }

        /* draw overlay based on macroblock type */
        switch (mb_type) {
            case I_NxN: /*shade_image(&image, x, y, RED);*/
            {
                int x1, y1, i=0;
                int size = macro[n].transform_size_8x8_flag? 8 : 4;

                const int x4x4[16] = {2, 6, 2, 6, 10, 14, 10, 14, 2, 6, 2, 6, 10, 14, 10, 14};
                const int y4x4[16] = {2, 2, 6, 6, 2, 2, 6, 6, 10, 10, 14, 14, 10, 10, 14, 14};
                const int x8x8[4]  = {4, 12, 4, 12};
                const int y8x8[4]  = {4, 4, 12, 12};

                XSegment *segments = (XSegment *)rvalloc(NULL, (4+4)*sizeof(XSegment), 0);
                if (segments==NULL)
                    return;

                /* draw the grid */
                for (x1=size*zoom; x1<=16*zoom; x1+=size*zoom, i++) {
                    segments[i].x1 = x0+x1;
                    segments[i].y1 = y0;
                    segments[i].x2 = x0+x1;
                    segments[i].y2 = y0+16*zoom;
                }
                for (y1=size*zoom; y1<=16*zoom; y1+=size*zoom, i++) {
                    segments[i].x1 = x0;
                    segments[i].y1 = y0+y1;
                    segments[i].x2 = x0+16*zoom;
                    segments[i].y2 = y0+y1;
                }

                XDrawSegments(win->display, win->pixmap, win->gc, segments, i);

                free(segments);

                if (draw_intra_pred && zoom>=3 && !draw_decoded) {
                    int i;
                    int num_intra_pred = macro[n].transform_size_8x8_flag? 4 : 16;
                    for (i=0; i<num_intra_pred; i++) {
                        if (macro[n].rem_intra_pred_mode[i]<0)
                            continue;
                        len = snprintf(string, sizeof(string), "%d", macro[n].rem_intra_pred_mode[i]);
                        wid = XTextWidth(win->font[1], string, len);
                        if (macro[n].transform_size_8x8_flag)
                            XDrawImageString(win->display, win->pixmap, win->gc, x0+x8x8[i]*zoom-wid/2, y0+y8x8[i]*zoom-hoff, string, len);
                        else
                            XDrawImageString(win->display, win->pixmap, win->gc, x0+x4x4[i]*zoom-wid/2, y0+y4x4[i]*zoom-hoff, string, len);
                    }
                }
                if (draw_intra_pred && zoom>=3 && draw_decoded) {
                    int i;
                    int num_intra_pred = macro[n].transform_size_8x8_flag? 4 : 16;
                    for (i=0; i<num_intra_pred; i++) {
                        len = snprintf(string, sizeof(string), "%d", macro[n].Intra4x4PredMode[i]);
                        wid = XTextWidth(win->font[1], string, len);
                        if (macro[n].transform_size_8x8_flag)
                            XDrawImageString(win->display, win->pixmap, win->gc, x0+x8x8[i]*zoom-wid/2, y0+y8x8[i]*zoom-hoff, string, len);
                        else
                            XDrawImageString(win->display, win->pixmap, win->gc, x0+x4x4[i]*zoom-wid/2, y0+y4x4[i]*zoom-hoff, string, len);
                    }
                }
            }
            break;

            case I_PCM: /*shade_image(&image, x, y, CYAN);*/ break;

            case I_16x16_0_0_0:
            case I_16x16_0_1_0:
            case I_16x16_0_2_0:
            case I_16x16_0_0_1:
            case I_16x16_0_1_1:
            case I_16x16_0_2_1:
                // Intra_16x16_Vertical prediction mode.
                //shade_image(&image, x, y, ORANGE);
                XDrawLine(win->display, win->pixmap, win->gc, lx, ly, rx, ly);
                break;

            case I_16x16_1_0_0:
            case I_16x16_1_1_0:
            case I_16x16_1_2_0:
            case I_16x16_1_0_1:
            case I_16x16_1_1_1:
            case I_16x16_1_2_1:
                // Intra_16x16_Horizontal prediction mode.
                //shade_image(&image, x, y, ORANGE);
                XDrawLine(win->display, win->pixmap, win->gc, lx, ly, lx, ry);
                break;

            case I_16x16_2_0_0:
            case I_16x16_2_1_0:
            case I_16x16_2_2_0:
            case I_16x16_2_0_1:
            case I_16x16_2_1_1:
            case I_16x16_2_2_1:
                // Intra_16x16_DC prediction mode.
                //shade_image(&image, x, y, ORANGE);
                XDrawRectangle(win->display, win->pixmap, win->gc, lx, ly, zoom, zoom);
                break;

            case I_16x16_3_0_0:
            case I_16x16_3_1_0:
            case I_16x16_3_2_0:
            case I_16x16_3_0_1:
            case I_16x16_3_1_1:
            case I_16x16_3_2_1:
                // Intra_16x16_Plane prediction mode.
                //shade_image(&image, x, y, ORANGE);
                XDrawLine(win->display, win->pixmap, win->gc, lx, ly, rx, ly);
                XDrawLine(win->display, win->pixmap, win->gc, lx, ly, lx, ry);
                break;

            //shade_image(&image, x, y, BLUE);
            case P_L0_16x16:
                if (draw_ref_idx) {
                    len = snprintf(string, sizeof(string), "%d", macro[n].ref_idx_l0[0]);
                    XDrawImageString(win->display, win->pixmap, win->gc, x0, y0+ascent, string, len);
                }
                display_264_motion_vectors(win, macro[n].mv[0][0], macro[n].mvd[0][0][0], cx, cy, zoom, drawmode, 0);
                break;

            case P_L0_L0_16x8:
                XDrawLine(win->display, win->pixmap, win->gc, x0, cy, bx, cy);
                if (draw_ref_idx) {
                    len = snprintf(string, sizeof(string), "%d", macro[n].ref_idx_l0[0]);
                    XDrawImageString(win->display, win->pixmap, win->gc, x0, y0+ascent, string, len);
                    len = snprintf(string, sizeof(string), "%d", macro[n].ref_idx_l0[2]);
                    XDrawImageString(win->display, win->pixmap, win->gc, x0, y0+ascent+8*zoom, string, len);
                }
                display_264_motion_vectors(win, macro[n].mv[0][0], macro[n].mvd[0][0][0], cx, cy-4*zoom, zoom, drawmode, 0);
                // FIXME ludh264 always uses +4*2 when parsing 8x16 and 16x8 partitions
                display_264_motion_vectors(win, macro[n].mv[0][8], macro[n].mvd[0][1][0], cx, cy+4*zoom, zoom, drawmode, 0);
                break;

            case P_L0_L0_8x16:
                XDrawLine(win->display, win->pixmap, win->gc, cx, y0, cx, by);
                if (draw_ref_idx) {
                    len = snprintf(string, sizeof(string), "%d", macro[n].ref_idx_l0[0]);
                    XDrawImageString(win->display, win->pixmap, win->gc, x0, y0+ascent, string, len);
                    len = snprintf(string, sizeof(string), "%d", macro[n].ref_idx_l0[1]);
                    XDrawImageString(win->display, win->pixmap, win->gc, x0+8*zoom, y0+ascent, string, len);
                }
                display_264_motion_vectors(win, macro[n].mv[0][0], macro[n].mvd[0][0][0], cx-4*zoom, cy, zoom, drawmode, 0);
                display_264_motion_vectors(win, macro[n].mv[0][4], macro[n].mvd[0][1][0], cx+4*zoom, cy, zoom, drawmode, 0);
                break;

            case P_8x8:
            case P_8x8ref0:
            {
                XDrawLine(win->display, win->pixmap, win->gc, x0, cy, bx, cy);
                XDrawLine(win->display, win->pixmap, win->gc, cx, y0, cx, by);
                int p, o=2*zoom;
                for (p=0; p<4; p++) {

                    /* calculate coordinates of partition origin */
                    int px = x0 + (p&1? 8*zoom : 0);
                    int py = y0 + (p>1? 8*zoom : 0);

                    /* calculate coordinates of partition centre */
                    int pcx = px + 4*zoom;
                    int pcy = py + 4*zoom;

                    /* calculate coordinates of macroblock right bottom */
                    int pbx = px + 8*zoom;
                    int pby = py + 8*zoom;

                    if (draw_ref_idx) {
                        len = snprintf(string, sizeof(string), "%d", macro[n].ref_idx_l0[p]);
                        XDrawImageString(win->display, win->pixmap, win->gc, px, py+ascent, string, len);
                    }

                    switch (macro[n].sub_mb_type[p]) {
                        case P_L0_8x8:
                            display_264_motion_vectors(win, macro[n].mv[0][4*p], macro[n].mvd[0][p][0], pcx, pcy, zoom, drawmode, 0);
                            break;

                        case P_L0_8x4:
                            XDrawLine(win->display, win->pixmap, win->gc, px, pcy, pbx, pcy);
                            display_264_motion_vectors(win, macro[n].mv[0][4*p]  , macro[n].mvd[0][p][0], pcx, pcy-o, zoom, drawmode, 0);
                            display_264_motion_vectors(win, macro[n].mv[0][4*p+2], macro[n].mvd[0][p][0], pcx, pcy+o, zoom, drawmode, 0);
                            break;

                        case P_L0_4x8:
                            XDrawLine(win->display, win->pixmap, win->gc, pcx, py, pcx, pby);
                            display_264_motion_vectors(win, macro[n].mv[0][4*p]  , macro[n].mvd[0][p][0], pcx-o, pcy, zoom, drawmode, 0);
                            display_264_motion_vectors(win, macro[n].mv[0][4*p+1], macro[n].mvd[0][p][0], pcx-o, pcy, zoom, drawmode, 0);
                            break;

                        case P_L0_4x4:
                            XDrawLine(win->display, win->pixmap, win->gc, px, pcy, pbx, pcy);
                            XDrawLine(win->display, win->pixmap, win->gc, pcx, py, pcx, pby);
                            display_264_motion_vectors(win, macro[n].mv[0][4*p+0], macro[n].mvd[0][p][0], pcx-o, pcy-o, zoom, drawmode, 0);
                            display_264_motion_vectors(win, macro[n].mv[0][4*p+1], macro[n].mvd[0][p][1], pcx+o, pcy-o, zoom, drawmode, 0);
                            display_264_motion_vectors(win, macro[n].mv[0][4*p+2], macro[n].mvd[0][p][2], pcx-o, pcy+o, zoom, drawmode, 0);
                            display_264_motion_vectors(win, macro[n].mv[0][4*p+3], macro[n].mvd[0][p][3], pcx+o, pcy+o, zoom, drawmode, 0);
                            break;

                        default:
                            rvabort("sub-macroblock type not expected");
                            break;
                    }
                }
                break;
            }

            case P_Skip:
                if (draw_mv_nums && draw_decoded) {
                    len = snprintf(string, sizeof(string), "%d,%d", macro[n].mv[0][0][0], macro[n].mv[0][0][1]);
                    wid = XTextWidth(win->font[1], string, len);
                    XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff, string, len);
                }
                break;

            case B_Direct_16x16:
                display_264_motion_vectors(win, macro[n].mv[0][0], NULL, cx, cy, zoom, drawmode, -line/2);
                display_264_motion_vectors(win, macro[n].mv[1][0], NULL, cx, cy, zoom, drawmode, line/2);
                break;

#define SETDASH { len=1; string[0]='-'; }
#define SETDOUBLEDASH { len=3; string[0]='-'; string[1]=','; string[2]='-'; }

            case B_L0_16x16:
            case B_L1_16x16:
            case B_Bi_16x16:
                if (draw_ref_idx) {
                    if (mb_type!=B_L1_16x16)
                        len = snprintf(string, sizeof(string), "%d", macro[n].ref_idx_l0[0]);
                    else
                        SETDASH;
                    XDrawImageString(win->display, win->pixmap, win->gc, x0, y0+ascent, string, len);
                    if (mb_type!=B_L0_16x16)
                        len = snprintf(string, sizeof(string), "%d", macro[n].ref_idx_l1[0]);
                    else
                        SETDASH;
                    XDrawImageString(win->display, win->pixmap, win->gc, x0, y0+ascent+line, string, len);
                }
                if (mb_type!=B_L1_16x16)
                    display_264_motion_vectors(win, macro[n].mv[0][0], macro[n].mvd[0][0][0], cx, cy, zoom, drawmode, -line/2);
                else
                    display_264_motion_vectors_null(win, cx, cy, zoom, drawmode, -line/2);
                if (mb_type!=B_L0_16x16)
                    display_264_motion_vectors(win, macro[n].mv[1][0], macro[n].mvd[1][0][0], cx, cy, zoom, drawmode, line/2);
                else
                    display_264_motion_vectors_null(win, cx, cy, zoom, drawmode, line/2);
                break;

            case B_L0_L0_16x8:
            case B_L1_L1_16x8:
            case B_L0_L1_16x8:
            case B_L1_L0_16x8:
            case B_L0_Bi_16x8:
            case B_L1_Bi_16x8:
            case B_Bi_L0_16x8:
            case B_Bi_L1_16x8:
            case B_Bi_Bi_16x8:
                XDrawLine(win->display, win->pixmap, win->gc, x0, cy, bx, cy);
                if (draw_ref_idx) {
                    /* first partition */
                    if (mb_type!=B_L1_L1_16x8 && mb_type!=B_L1_L0_16x8 && mb_type!=B_L1_Bi_16x8)
                        len = snprintf(string, sizeof(string), "%d", macro[n].ref_idx_l0[0]);
                    else
                        SETDASH;
                    XDrawImageString(win->display, win->pixmap, win->gc, x0, y0+ascent, string, len);
                    if (mb_type!=B_L0_L0_16x8 && mb_type!=B_L0_L1_16x8 && mb_type!=B_L0_Bi_16x8)
                        len = snprintf(string, sizeof(string), "%d", macro[n].ref_idx_l1[0]);
                    else
                        SETDASH;
                    XDrawImageString(win->display, win->pixmap, win->gc, x0, y0+ascent+line, string, len);
                    /* second partition */
                    if (mb_type!=B_L1_L1_16x8 && mb_type!=B_L0_L1_16x8 && mb_type!=B_Bi_L1_16x8)
                        len = snprintf(string, sizeof(string), "%d", macro[n].ref_idx_l0[2]);
                    else
                        SETDASH;
                    XDrawImageString(win->display, win->pixmap, win->gc, x0, y0+ascent+8*zoom, string, len);
                    if (mb_type!=B_L0_L0_16x8 && mb_type!=B_L1_L0_16x8 && mb_type!=B_Bi_L0_16x8)
                        len = snprintf(string, sizeof(string), "%d", macro[n].ref_idx_l1[2]);
                    else
                        SETDASH;
                    XDrawImageString(win->display, win->pixmap, win->gc, x0, y0+ascent+8*zoom+line, string, len);
                }
                /* first partition */
                if (mb_type!=B_L1_L1_16x8 && mb_type!=B_L1_L0_16x8 && mb_type!=B_L1_Bi_16x8)
                    //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[0][0][0][0], macro[n].mvd[0][0][0][1]);
                    display_264_motion_vectors(win, macro[n].mv[0][0], macro[n].mvd[0][0][0], cx, cy-4*zoom, zoom, drawmode, -line/2);
                else
                    display_264_motion_vectors_null(win, cx, cy-4*zoom, zoom, drawmode, -line/2);
                if (mb_type!=B_L0_L0_16x8 && mb_type!=B_L0_L1_16x8 && mb_type!=B_L0_Bi_16x8)
                    //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[1][0][0][0], macro[n].mvd[1][0][0][1]);
                    display_264_motion_vectors(win, macro[n].mv[1][0], macro[n].mvd[1][0][0], cx, cy-4*zoom, zoom, drawmode, line/2);
                else
                    display_264_motion_vectors_null(win, cx, cy-4*zoom, zoom, drawmode, line/2);
                /* second partition */
                if (mb_type!=B_L1_L1_16x8 && mb_type!=B_L0_L1_16x8 && mb_type!=B_Bi_L1_16x8)
                    //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[0][2][0][0], macro[n].mvd[0][2][0][1]);
                    display_264_motion_vectors(win, macro[n].mv[0][0], macro[n].mvd[0][2][0], cx, cy+4*zoom, zoom, drawmode, -line/2);
                else
                    display_264_motion_vectors_null(win, cx, cy+4*zoom, zoom, drawmode, -line/2);
                if (mb_type!=B_L0_L0_16x8 && mb_type!=B_L1_L0_16x8 && mb_type!=B_Bi_L0_16x8)
                    //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[1][2][0][0], macro[n].mvd[1][2][0][1]);
                    display_264_motion_vectors(win, macro[n].mv[1][0], macro[n].mvd[1][2][0], cx, cy+4*zoom, zoom, drawmode, line/2);
                else
                    display_264_motion_vectors_null(win, cx, cy+4*zoom, zoom, drawmode, line/2);
                break;

            case B_L0_L0_8x16:
            case B_L1_L1_8x16:
            case B_L0_L1_8x16:
            case B_L1_L0_8x16:
            case B_L0_Bi_8x16:
            case B_L1_Bi_8x16:
            case B_Bi_L0_8x16:
            case B_Bi_L1_8x16:
            case B_Bi_Bi_8x16:
                XDrawLine(win->display, win->pixmap, win->gc, cx, y0, cx, by);
                if (draw_ref_idx) {
                    /* first partition */
                    if (mb_type!=B_L1_L1_8x16 && mb_type!=B_L1_L0_8x16 && mb_type!=B_L1_Bi_8x16)
                        len = snprintf(string, sizeof(string), "%d", macro[n].ref_idx_l0[0]);
                    else
                        SETDASH;
                    XDrawImageString(win->display, win->pixmap, win->gc, x0, y0+ascent, string, len);
                    if (mb_type!=B_L0_L0_8x16 && mb_type!=B_L0_L1_8x16 && mb_type!=B_L0_Bi_8x16)
                        len = snprintf(string, sizeof(string), "%d", macro[n].ref_idx_l1[0]);
                    else
                        SETDASH;
                    XDrawImageString(win->display, win->pixmap, win->gc, x0, y0+ascent+line, string, len);
                    /* second partition */
                    if (mb_type!=B_L1_L1_8x16 && mb_type!=B_L0_L1_8x16 && mb_type!=B_Bi_L1_8x16)
                        len = snprintf(string, sizeof(string), "%d", macro[n].ref_idx_l0[1]);
                    else
                        SETDASH;
                    XDrawImageString(win->display, win->pixmap, win->gc, x0+8*zoom, y0+ascent, string, len);
                    if (mb_type!=B_L0_L0_8x16 && mb_type!=B_L1_L0_8x16 && mb_type!=B_Bi_L0_8x16)
                        len = snprintf(string, sizeof(string), "%d", macro[n].ref_idx_l1[1]);
                    else
                        SETDASH;
                    XDrawImageString(win->display, win->pixmap, win->gc, x0+8*zoom, y0+ascent+line, string, len);
                }
                /* first partition */
                if (mb_type!=B_L1_L1_8x16 && mb_type!=B_L1_L0_8x16 && mb_type!=B_L1_Bi_8x16)
                    //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[0][0][0][0], macro[n].mvd[0][0][0][1]);
                    display_264_motion_vectors(win, macro[n].mv[0][0], macro[n].mvd[0][0][0], cx-4*zoom, cy, zoom, drawmode, -line/2);
                else
                    display_264_motion_vectors_null(win, cx-4*zoom, cy, zoom, drawmode, -line/2);
                if (mb_type!=B_L0_L0_8x16 && mb_type!=B_L0_L1_8x16 && mb_type!=B_L0_Bi_8x16)
                    //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[1][0][0][0], macro[n].mvd[1][0][0][1]);
                    display_264_motion_vectors(win, macro[n].mv[1][0], macro[n].mvd[1][0][0], cx-4*zoom, cy, zoom, drawmode, line/2);
                else
                    display_264_motion_vectors_null(win, cx-4*zoom, cy, zoom, drawmode, line/2);
                /* second partition */
                if (mb_type!=B_L1_L1_8x16 && mb_type!=B_L0_L1_8x16 && mb_type!=B_Bi_L1_8x16)
                    //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[0][1][0][0], macro[n].mvd[0][1][0][1]);
                    display_264_motion_vectors(win, macro[n].mv[0][2], macro[n].mvd[0][1][0], cx+4*zoom, cy, zoom, drawmode, -line/2);
                else
                    display_264_motion_vectors_null(win, cx+4*zoom, cy, zoom, drawmode, -line/2);
                if (mb_type!=B_L0_L0_8x16 && mb_type!=B_L1_L0_8x16 && mb_type!=B_Bi_L0_8x16)
                    //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[1][1][0][0], macro[n].mvd[1][1][0][1]);
                    display_264_motion_vectors(win, macro[n].mv[1][2], macro[n].mvd[1][1][0], cx+4*zoom, cy, zoom, drawmode, line/2);
                else
                    display_264_motion_vectors_null(win, cx+4*zoom, cy, zoom, drawmode, line/2);
                break;

            case B_8x8:
            {
                XDrawLine(win->display, win->pixmap, win->gc, x0, cy, bx, cy);
                XDrawLine(win->display, win->pixmap, win->gc, cx, y0, cx, by);
                int p, o=2*zoom;
                for (p=0; p<4; p++) {
                    sub_mb_type_t sub_mb_type = macro[n].sub_mb_type[p];

                    if (sub_mb_type==B_Direct_8x8)
                        continue;

                    /* calculate coordinates of partition origin */
                    int px = x0 + (p&1? 8*zoom : 0);
                    int py = y0 + (p>1? 8*zoom : 0);

                    /* calculate coordinates of partition centre */
                    int pcx = px + 4*zoom;
                    int pcy = py + 4*zoom;

                    /* calculate coordinates of macroblock right bottom */
                    int pbx = px + 8*zoom;
                    int pby = py + 8*zoom;

                    /* calculate index in 4x4 coords */
                    static const int ptoq[] = {0, 2, 8, 10};
                    int q = ptoq[p];

                    if (draw_ref_idx) {
                        if (sub_mb_type!=B_L1_8x8 && sub_mb_type!=B_L1_8x4 && sub_mb_type!=B_L1_4x8 && sub_mb_type!=B_L1_4x4)
                            len = snprintf(string, sizeof(string), "%d", macro[n].ref_idx_l0[p]);
                        else
                            SETDASH;
                        XDrawImageString(win->display, win->pixmap, win->gc, px, py+ascent, string, len);
                        if (sub_mb_type!=B_L0_8x8 && sub_mb_type!=B_L0_8x4 && sub_mb_type!=B_L0_4x8 && sub_mb_type!=B_L0_4x4)
                            len = snprintf(string, sizeof(string), "%d", macro[n].ref_idx_l1[p]);
                        else
                            SETDASH;
                        XDrawImageString(win->display, win->pixmap, win->gc, px, py+ascent+line, string, len);
                    }

                    switch (sub_mb_type) {
                        case B_L0_8x8:
                        case B_L1_8x8:
                        case B_Bi_8x8:
                            if (sub_mb_type!=B_L1_8x8)
                                //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[0][p][0][0], macro[n].mvd[0][p][0][1]);
                                display_264_motion_vectors(win, macro[n].mv[0][q], macro[n].mvd[0][p][0], pcx, pcy, zoom, drawmode, -line/2);
                            else
                                SETDOUBLEDASH;
                            //XDrawImageString(win->display, win->pixmap, win->gc, pcx-wid/2, pcy-hoff-line/2, string, len);
                            if (sub_mb_type!=B_L0_8x8)
                                //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[1][p][0][0], macro[n].mvd[1][p][0][1]);
                                display_264_motion_vectors(win, macro[n].mv[1][q], macro[n].mvd[1][p][0], pcx, pcy, zoom, drawmode, line/2);
                            else
                                SETDOUBLEDASH;
                            //XDrawImageString(win->display, win->pixmap, win->gc, pcx-wid/2, pcy-hoff+line/2, string, len);
                            break;

                        case B_L0_8x4:
                        case B_L1_8x4:
                        case B_Bi_8x4:
                            XDrawLine(win->display, win->pixmap, win->gc, px, pcy, pbx, pcy);
                            if (sub_mb_type!=B_L1_8x4)
                                //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[0][p][0][0], macro[n].mvd[0][p][0][1]);
                                display_264_motion_vectors(win, macro[n].mv[0][q], macro[n].mvd[0][p][0], pcx, pcy-o, zoom, drawmode, -line/2);
                            else
                                SETDOUBLEDASH;
                            //XDrawImageString(win->display, win->pixmap, win->gc, pcx-wid/2, pcy-hoff-o-line/2, string, len);
                            if (sub_mb_type!=B_L0_8x4)
                                //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[1][p][0][0], macro[n].mvd[1][p][0][1]);
                                display_264_motion_vectors(win, macro[n].mv[1][q], macro[n].mvd[1][p][0], pcx, pcy-o, zoom, drawmode, line/2);
                            else
                                SETDOUBLEDASH;
                            //XDrawImageString(win->display, win->pixmap, win->gc, pcx-wid/2, pcy-hoff-o+line/2, string, len);
                            if (sub_mb_type!=B_L1_8x4)
                                //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[0][p][2][0], macro[n].mvd[0][p][2][1]);
                                display_264_motion_vectors(win, macro[n].mv[0][q+4], macro[n].mvd[0][p][2], pcx, pcy+o, zoom, drawmode, -line/2);
                            else
                                SETDOUBLEDASH;
                            //XDrawImageString(win->display, win->pixmap, win->gc, pcx-wid/2, pcy-hoff+o-line/2, string, len);
                            if (sub_mb_type!=B_L0_8x4)
                                //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[1][p][2][0], macro[n].mvd[1][p][2][1]);
                                display_264_motion_vectors(win, macro[n].mv[1][q+4], macro[n].mvd[0][p][2], pcx, pcy+o, zoom, drawmode, line/2);
                            else
                                SETDOUBLEDASH;
                            //XDrawImageString(win->display, win->pixmap, win->gc, pcx-wid/2, pcy-hoff+o+line/2, string, len);
                            break;

                        case B_L0_4x8:
                        case B_L1_4x8:
                        case B_Bi_4x8:
                            XDrawLine(win->display, win->pixmap, win->gc, pcx, py, pcx, pby);
                            if (sub_mb_type!=B_L1_4x8)
                                //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[0][p][0][0], macro[n].mvd[0][p][0][1]);
                                display_264_motion_vectors(win, macro[n].mv[0][q], macro[n].mvd[0][p][0], pcx-o, pcy, zoom, drawmode, -line/2);
                            else
                                SETDOUBLEDASH;
                            //XDrawImageString(win->display, win->pixmap, win->gc, pcx-wid/2-o, pcy-hoff-line/2, string, len);
                            if (sub_mb_type!=B_L0_4x8)
                                //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[1][p][0][0], macro[n].mvd[1][p][0][1]);
                                display_264_motion_vectors(win, macro[n].mv[1][q], macro[n].mvd[1][p][0], pcx-o, pcy, zoom, drawmode, line/2);
                            else
                                SETDOUBLEDASH;
                            //XDrawImageString(win->display, win->pixmap, win->gc, pcx-wid/2-o, pcy-hoff+line/2, string, len);
                            if (sub_mb_type!=B_L1_4x8)
                                //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[0][p][1][0], macro[n].mvd[0][p][1][1]);
                                display_264_motion_vectors(win, macro[n].mv[0][q+1], macro[n].mvd[0][p][1], pcx+o, pcy, zoom, drawmode, -line/2);
                            else
                                SETDOUBLEDASH;
                            //XDrawImageString(win->display, win->pixmap, win->gc, pcx-wid/2+o, pcy-hoff-line/2, string, len);
                            if (sub_mb_type!=B_L0_4x8)
                                //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[0][p][1][0], macro[n].mvd[0][p][1][1]);
                                display_264_motion_vectors(win, macro[n].mv[1][q+1], macro[n].mvd[1][p][1], pcx+o, pcy, zoom, drawmode, line/2);
                            else
                                SETDOUBLEDASH;
                            //XDrawImageString(win->display, win->pixmap, win->gc, pcx-wid/2+o, pcy-hoff+line/2, string, len);
                            break;

                        case B_L0_4x4:
                        case B_L1_4x4:
                        case B_Bi_4x4:
                            XDrawLine(win->display, win->pixmap, win->gc, px, pcy, pbx, pcy);
                            XDrawLine(win->display, win->pixmap, win->gc, pcx, py, pcx, pby);
                            if (sub_mb_type!=B_L1_4x4)
                                //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[0][p][0][0], macro[n].mvd[0][p][0][1]);
                                display_264_motion_vectors(win, macro[n].mv[0][q], macro[n].mvd[0][p][0], pcx-o, pcy-o, zoom, drawmode, -line/2);
                            else
                                SETDOUBLEDASH;
                            //XDrawImageString(win->display, win->pixmap, win->gc, pcx-wid/2-o, pcy-hoff-o-line/2, string, len);
                            if (sub_mb_type!=B_L0_4x4)
                                //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[1][p][0][0], macro[n].mvd[1][p][0][1]);
                                display_264_motion_vectors(win, macro[n].mv[1][q], macro[n].mvd[1][p][0], pcx-o, pcy-o, zoom, drawmode, line/2);
                            else
                                SETDOUBLEDASH;
                            //XDrawImageString(win->display, win->pixmap, win->gc, pcx-wid/2-o, pcy-hoff-o+line/2, string, len);
                            if (sub_mb_type!=B_L1_4x4)
                                //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[0][p][1][0], macro[n].mvd[0][p][1][1]);
                                display_264_motion_vectors(win, macro[n].mv[0][q+1], macro[n].mvd[0][p][1], pcx+o, pcy-o, zoom, drawmode, -line/2);
                            else
                                SETDOUBLEDASH;
                            //XDrawImageString(win->display, win->pixmap, win->gc, pcx-wid/2+o, pcy-hoff-o-line/2, string, len);
                            if (sub_mb_type!=B_L0_4x4)
                                //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[1][p][1][0], macro[n].mvd[1][p][1][1]);
                                display_264_motion_vectors(win, macro[n].mv[1][q+1], macro[n].mvd[1][p][1], pcx+o, pcy-o, zoom, drawmode, line/2);
                            else
                                SETDOUBLEDASH;
                            //XDrawImageString(win->display, win->pixmap, win->gc, pcx-wid/2+o, pcy-hoff-o+line/2, string, len);
                            if (sub_mb_type!=B_L1_4x4)
                                //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[0][p][2][0], macro[n].mvd[0][p][2][1]);
                                display_264_motion_vectors(win, macro[n].mv[0][q+4], macro[n].mvd[0][p][2], pcx-o, pcy+o, zoom, drawmode, -line/2);
                            else
                                SETDOUBLEDASH;
                            //XDrawImageString(win->display, win->pixmap, win->gc, pcx-wid/2-o, pcy-hoff+o-line/2, string, len);
                            if (sub_mb_type!=B_L0_4x4)
                                //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[1][p][2][0], macro[n].mvd[1][p][2][1]);
                                display_264_motion_vectors(win, macro[n].mv[1][q+4], macro[n].mvd[1][p][2], pcx-o, pcy+o, zoom, drawmode, line/2);
                            else
                                SETDOUBLEDASH;
                            //XDrawImageString(win->display, win->pixmap, win->gc, pcx-wid/2-o, pcy-hoff+o+line/2, string, len);
                            if (sub_mb_type!=B_L1_4x4)
                                //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[0][p][3][0], macro[n].mvd[0][p][3][1]);
                                display_264_motion_vectors(win, macro[n].mv[0][q+5], macro[n].mvd[0][p][3], pcx+o, pcy+o, zoom, drawmode, -line/2);
                            else
                                SETDOUBLEDASH;
                            //XDrawImageString(win->display, win->pixmap, win->gc, pcx-wid/2+o, pcy-hoff+o-line/2, string, len);
                            if (sub_mb_type!=B_L0_4x4)
                                //len = snprintf(string, sizeof(string), "%d,%d", macro[n].mvd[1][p][3][0], macro[n].mvd[1][p][3][1]);
                                display_264_motion_vectors(win, macro[n].mv[1][q+5], macro[n].mvd[1][p][3], pcx+o, pcy+o, zoom, drawmode, line/2);
                            else
                                SETDOUBLEDASH;
                            //XDrawImageString(win->display, win->pixmap, win->gc, pcx-wid/2+o, pcy-hoff+o+line/2, string, len);
                            break;

                        case B_Direct_8x8:
                            display_264_motion_vectors(win, macro[n].mv[0][q], macro[n].mvd[0][p][0], pcx, pcy, zoom, drawmode, -line/2);
                            display_264_motion_vectors(win, macro[n].mv[1][q], macro[n].mvd[1][p][0], pcx, pcy, zoom, drawmode, line/2);
                            break;

                        default:
                            break;
                    }
                }
                break;
            }

            case B_Skip:
                break;
        }

        /* draw "status bar" overlay */
        len = 0;
        if (macro[n].mb_qp_delta)
            len += snprintf(string+len, sizeof(string)-len, "dQp=%+d ", macro[n].mb_qp_delta);
        if (macro[n].slice_num != prev_slice)
            len += snprintf(string+len, sizeof(string)-len, "slice=%d", macro[n].slice_num);
        prev_slice = macro[n].slice_num;

        if (len && draw_status_bar) {
            wid = XTextWidth(win->font[1], string, len);
            XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, by-descent, string, len);
        }

    }
}

void display_265_params(struct rvwin *win, int x, int y, const struct rvhevcpicture *pic)
{
    char string[48];

    /* sanity check */
    if (pic==NULL)
        return;

    const struct hevc_slice_header *sh       = &pic->sh;
    //const struct hevc_pic_parameter_set *pps = sh->pps;
    //const struct hevc_seq_parameter_set *sps = pps->sps;

    /* set the drawing attributes in the graphics context */
    XSetForeground(win->display, win->gc, WhitePixel(win->display, DefaultScreen(win->display)));

    /* set the large font in the graphics context */
    if (!XSetFont(win->display, win->gc, win->font[0]->fid))
        rvmessage("failed to set the font in the graphics context: %s", win->fontname[0][0]);

    /* get the font metrics of the large font */
    int dir, ascent, descent;
    XCharStruct cs;
    strncpy(string, "0123456789", sizeof(string));
    XTextExtents(win->font[0], string, strlen(string), &dir, &ascent, &descent, &cs);
    int line = ascent + descent;

    /* loop over each string for display */
    int i, j;
    for (i=0, j=0; i<6; i++) {
        /* prepare the strings for display */
        int len;
        switch (i) {
            case  0: len = snprintf(string, sizeof(string), "pic_order_count=%d", sh->PicOrderCntVal); break;
            case  1: len = snprintf(string, sizeof(string), "slice_type=%c-slice", sh->slice_type==0?'B':sh->slice_type==1?'P':'I'); break;
            case  2: len = snprintf(string, sizeof(string), "num_ref_idx_l0_active=%d", sh->num_ref_idx_l0_active); break;
            case  3: len = snprintf(string, sizeof(string), "num_ref_idx_l1_active=%d", sh->num_ref_idx_l1_active); break;
            case  4: len = snprintf(string, sizeof(string), "MaxNumMergeCand=%d", sh->MaxNumMergeCand); break;
            case  5: len = snprintf(string, sizeof(string), "SliceQPY=%d", sh->SliceQPY); break;
        }

        /* put the picture parameters in the top right */
        int x0 = x + win->ximage->width - XTextWidth(win->font[0], string, len) - 10;
        int y0 = y + line + line*j++ + 10;

        /* display the picture parameters */
        XDrawImageString(win->display, win->pixmap, win->gc, x0, y0, string, len);
    }
}

void display_265_transform_split(struct rvwin *win, int xb, int yb, int CbSize, int zoom, int depth, const struct rvhevcpicture *pic)
{
    int split_transform_flag = pic->tu.get(xb, yb)->split_transform_flag & (1<<depth);
    if (split_transform_flag) {
        /* calculate coordinates of coding block origin */
        int x0 = xb * zoom;
        int y0 = yb * zoom;

        /* calculate coordinates of transform block centre */
        int cx = x0 + CbSize/2*zoom;
        int cy = y0 + CbSize/2*zoom;

        /* calculate coordinates of coding block right bottom */
        int bx = x0 + CbSize*zoom;
        int by = y0 + CbSize*zoom;

        /* draw transfrom block split */
        XDrawLine(win->display, win->pixmap, win->gc, x0, cy, bx, cy);
        XDrawLine(win->display, win->pixmap, win->gc, cx, y0, cx, by);

        /* ooh recursive */
        int xs = xb + CbSize/2;
        int ys = yb + CbSize/2;
        display_265_transform_split(win, xb, yb, CbSize/2, zoom, depth+1, pic);
        display_265_transform_split(win, xb, ys, CbSize/2, zoom, depth+1, pic);
        display_265_transform_split(win, xs, yb, CbSize/2, zoom, depth+1, pic);
        display_265_transform_split(win, xs, ys, CbSize/2, zoom, depth+1, pic);
    } else {
    }
}

void display_265_intra_pred_mode(struct rvwin *win, const hevc_intra_pred_mode_t mode, int cx, int cy, int ascent, int descent)
{
    char string[32];
    int len = snprintf(string, sizeof(string), "%d", mode);
    int wid = XTextWidth(win->font[1], string, len);
    int hoff = (descent - ascent)/2;
    XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff, string, len);
}

static void display_265_motion_vector(struct rvwin *win, const struct mv_t &mv, int cx, int cy, int ascent, int descent)
{
    /* display decoded vectors */
    char string[32];
    int len = snprintf(string, sizeof(string), "%d,%d", mv.x, mv.y);
    int wid = XTextWidth(win->font[1], string, len);
    int hoff = (descent - ascent)/2;
    XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, cy-hoff, string, len);
}

void display_265_motion_vectors(struct rvwin *win, const struct hevc_motion &mvi, int cx, int cy, int zoom, int drawmode, int ascent, int descent)
{
    const int draw_mv_vec  = drawmode & DRAWMODE_VEC;
    const int draw_mv_nums = drawmode & DRAWMODE_MVS;

    if (draw_mv_vec) {
        /* draw decoded vectors */
        if (mvi.predFlag[0] && (mvi.mv[0].x || mvi.mv[0].y))
            XDrawLine(win->display, win->pixmap, win->gc, cx, cy, cx + (mvi.mv[0].x*zoom)/2, cy + (mvi.mv[0].y*zoom)/2);
        if (mvi.predFlag[1] && (mvi.mv[1].x || mvi.mv[1].y))
            XDrawLine(win->display, win->pixmap, win->gc, cx, cy, cx + (mvi.mv[1].x*zoom)/2, cy + (mvi.mv[1].y*zoom)/2);
    }
    if (draw_mv_nums) {
        /* display decoded vectors */
        if (mvi.predFlag[0] && mvi.predFlag[1]) {
            display_265_motion_vector(win, mvi.mv[0], cx, cy-(ascent+descent)/2, ascent, descent);
            display_265_motion_vector(win, mvi.mv[1], cx, cy+(ascent+descent)/2, ascent, descent);
        } else if (mvi.predFlag[0])
            display_265_motion_vector(win, mvi.mv[0], cx, cy, ascent, descent);
        else if (mvi.predFlag[1])
            display_265_motion_vector(win, mvi.mv[1], cx, cy, ascent, descent);
    }
}

void display_265_overlay(struct rvwin *win, int x, int y, int width, int height, int zoom, int drawmode, const struct rvhevcpicture *pic)
{
    /* sanity check */
    if (pic==NULL)
        return;

    const hevc_seq_parameter_set* sps = pic->sh.pps->sps;
    int minCbSize = sps->MinCbSizeY;

    /* select the items to draw */
    const int draw_intra_pred = drawmode & DRAWMODE_INT;
    const int draw_mv_vec  = drawmode & DRAWMODE_VEC;
    const int draw_mv_nums = drawmode & DRAWMODE_MVS;
    //const int draw_ref_idx    = drawmode & DRAWMODE_REF;
    const int draw_trans_split= drawmode & DRAWMODE_DCT;
    const int draw_part_split = drawmode & DRAWMODE_BAR;
    const int draw_status_bar = drawmode & DRAWMODE_BAR;

    /* set the drawing attributes in the graphics context */
    if (!XSetLineAttributes(win->display, win->gc, 1, LineSolid, CapButt, JoinMiter))
        rvmessage("failed to set the line width to %d in the graphics context", 1);
    XSetForeground(win->display, win->gc, WhitePixel(win->display, DefaultScreen(win->display)));

    /* set the small font in the graphics context */
    if (!XSetFont(win->display, win->gc, win->font[1]->fid))
        rvmessage("failed to set the font in the graphics context: %s", win->fontname[1][0]);

    /* get the font metrics of the small font */
    char string[32];
    int dir, ascent, descent;
    XCharStruct cs;
    strncpy(string, "0123456789", sizeof(string));
    XTextExtents(win->font[1], string, strlen(string), &dir, &ascent, &descent, &cs);

    /* loop over minimum size coding blocks in frame */
    int prev_cu_qp = 0;
    for (int ycb=0; ycb<sps->PicHeightInMinCbsY; ycb++) {
        for (int xcb=0; xcb<sps->PicWidthInMinCbsY; xcb++) {
            const struct rvhevccodingunit *cu = pic->cu.get_in_units(xcb, ycb);
            int log2CbSize = cu->log2CbSize;
            if (log2CbSize==0) {
                continue;
            }
            int CbSize = 1<<log2CbSize;
            int HalfCbSize = 1<<(log2CbSize-1);

            /* calculate coordinates of coding block */
            int xb = xcb*minCbSize;
            int yb = ycb*minCbSize;

            /* calculate coordinates of coding block origin */
            int x0 = xcb*minCbSize * zoom;
            int y0 = ycb*minCbSize * zoom;

            /* add image offset */
            x0 += x;
            y0 += y;

            /* calculate coordinates of coding block centre */
            int cx = x0 + CbSize/2*zoom;
            int cy = y0 + CbSize/2*zoom;

            /* calculate coordinates of coding block right bottom */
            int bx = x0 + CbSize*zoom;
            int by = y0 + CbSize*zoom;

            /* calculate coordinates of coding block first quadrant */
            int x14 = x0 + CbSize/4*zoom;
            int y14 = y0 + CbSize/4*zoom;

            /* calculate coordinates of coding block fourth quadrant */
            int y34 = y0 + CbSize*3/4*zoom;
            int x34 = x0 + CbSize*3/4*zoom;

            /* calculate coefficients of coding block inside edges */
            //int lx = x0 + zoom;
            //int ly = y0 + zoom;
            //int rx = x0 + 14*zoom;
            //int ry = y0 + 14*zoom;

            /* draw coding blocks, only top and left edge need to be drawn */
            XDrawLine(win->display, win->pixmap, win->gc, x0, y0, bx, y0);
            XDrawLine(win->display, win->pixmap, win->gc, x0, y0, x0, by);

            /* draw the transform split */
            if (draw_trans_split) {
                XSetLineAttributes(win->display, win->gc, 1, LineOnOffDash, CapButt, JoinMiter);
                display_265_transform_split(win, xb, yb, CbSize, zoom, 0, pic);
                XSetLineAttributes(win->display, win->gc, 1, LineSolid, CapButt, JoinMiter);
            }

            /* draw intra pred mode */
            //shade_image(&image, x, y, CbSize, CbSize, RED);
            if (draw_intra_pred) {
                if (cu->pred_mode==HEVC_MODE_INTRA) {
                    hevc_part_mode_t partMode = cu->PartMode;

                    switch (partMode) {
                        case HEVC_PART_2Nx2N:
                            display_265_intra_pred_mode(win, cu->IntraPredMode[0], cx, cy, ascent, descent);
                            break;
                        case HEVC_PART_NxN:
                            display_265_intra_pred_mode(win, cu->IntraPredMode[0], cx-CbSize/4*zoom+1, cy-CbSize/4*zoom+1, ascent, descent);
                            display_265_intra_pred_mode(win, cu->IntraPredMode[1], cx+CbSize/4*zoom+1, cy-CbSize/4*zoom+1, ascent, descent);
                            display_265_intra_pred_mode(win, cu->IntraPredMode[2], cx-CbSize/4*zoom+1, cy+CbSize/4*zoom+1, ascent, descent);
                            display_265_intra_pred_mode(win, cu->IntraPredMode[3], cx+CbSize/4*zoom+1, cy+CbSize/4*zoom+1, ascent, descent);
                            break;
                        default:
                            assert(false);
                            break;
                    }
                }
            }

            /* draw prediction block and motion vectors */
            if (cu->pred_mode==HEVC_MODE_INTER || cu->pred_mode==HEVC_MODE_SKIP) {
                hevc_part_mode_t partMode = cu->PartMode;

                /* draw prediction partition */
                if (draw_part_split) {
                    /* set line style for prediction partition */
                    XSetLineAttributes(win->display, win->gc, 1, LineDoubleDash, CapButt, JoinMiter);

                    switch (partMode) {
                        case HEVC_PART_2Nx2N:
                            break;
                        case HEVC_PART_NxN:
                            XDrawLine(win->display, win->pixmap, win->gc, x0, cy, bx, cy);
                            XDrawLine(win->display, win->pixmap, win->gc, cx, y0, cx, by);
                            break;
                        case HEVC_PART_2NxN:
                            XDrawLine(win->display, win->pixmap, win->gc, x0, cy, bx, cy);
                            break;
                        case HEVC_PART_Nx2N:
                            XDrawLine(win->display, win->pixmap, win->gc, cx, y0, cx, by);
                            break;
                        case HEVC_PART_2NxnU:
                            XDrawLine(win->display, win->pixmap, win->gc, x0, y14, bx, y14);
                            break;
                        case HEVC_PART_2NxnD:
                            XDrawLine(win->display, win->pixmap, win->gc, x0, y34, bx, y34);
                            break;
                        case HEVC_PART_nLx2N:
                            XDrawLine(win->display, win->pixmap, win->gc, x14, y0, x14, by);
                            break;
                        case HEVC_PART_nRx2N:
                            XDrawLine(win->display, win->pixmap, win->gc, x34, y0, x34, by);
                            break;
                        default:
                            assert(false);
                            break;
                    }

                    /* revert line style */
                    XSetLineAttributes(win->display, win->gc, 1, LineSolid, CapButt, JoinMiter);
                }

                /* draw or display motion vectors */
                if (draw_mv_vec || draw_mv_nums) {
                    switch (partMode) {
                        case HEVC_PART_2Nx2N:
                            display_265_motion_vectors(win, pic->pu.get(xb, yb)->motion, cx, cy, zoom, drawmode, ascent, descent);
                            break;
                        case HEVC_PART_NxN:
                            display_265_motion_vectors(win, pic->pu.get(xb           , yb           )->motion, cx-CbSize/4*zoom, cy-CbSize/4*zoom, zoom, drawmode, ascent, descent);
                            display_265_motion_vectors(win, pic->pu.get(xb+HalfCbSize, yb           )->motion, cx+CbSize/4*zoom, cy-CbSize/4*zoom, zoom, drawmode, ascent, descent);
                            display_265_motion_vectors(win, pic->pu.get(xb           , yb+HalfCbSize)->motion, cx-CbSize/4*zoom, cy+CbSize/4*zoom, zoom, drawmode, ascent, descent);
                            display_265_motion_vectors(win, pic->pu.get(xb+HalfCbSize, yb+HalfCbSize)->motion, cx+CbSize/4*zoom, cy+CbSize/4*zoom, zoom, drawmode, ascent, descent);
                            break;
                        case HEVC_PART_2NxN:
                            display_265_motion_vectors(win, pic->pu.get(xb, yb           )->motion, cx, cy-CbSize/4*zoom, zoom, drawmode, ascent, descent);
                            display_265_motion_vectors(win, pic->pu.get(xb, yb+HalfCbSize)->motion, cx, cy+CbSize/4*zoom, zoom, drawmode, ascent, descent);
                            break;
                        case HEVC_PART_Nx2N:
                            display_265_motion_vectors(win, pic->pu.get(xb           , yb)->motion, cx-CbSize/4*zoom, cy, zoom, drawmode, ascent, descent);
                            display_265_motion_vectors(win, pic->pu.get(xb+HalfCbSize, yb)->motion, cx+CbSize/4*zoom, cy, zoom, drawmode, ascent, descent);
                            break;
                        case HEVC_PART_2NxnU:
                            display_265_motion_vectors(win, pic->pu.get(xb           , yb)->motion, cx, y0+CbSize  /8*zoom, zoom, drawmode, ascent, descent);
                            display_265_motion_vectors(win, pic->pu.get(xb,   yb+CbSize/4)->motion, cx, y0+CbSize*5/8*zoom, zoom, drawmode, ascent, descent);
                            break;
                        case HEVC_PART_2NxnD:
                            display_265_motion_vectors(win, pic->pu.get(xb           , yb)->motion, cx, y0+CbSize*3/8*zoom, zoom, drawmode, ascent, descent);
                            display_265_motion_vectors(win, pic->pu.get(xb, yb+CbSize*3/4)->motion, cx, y0+CbSize*7/8*zoom, zoom, drawmode, ascent, descent);
                            break;
                        case HEVC_PART_nLx2N:
                            display_265_motion_vectors(win, pic->pu.get(xb           , yb)->motion, x0+CbSize  /8*zoom, cy, zoom, drawmode, ascent, descent);
                            display_265_motion_vectors(win, pic->pu.get(xb+CbSize/4  , yb)->motion, x0+CbSize*5/8*zoom, cy, zoom, drawmode, ascent, descent);
                            break;
                        case HEVC_PART_nRx2N:
                            display_265_motion_vectors(win, pic->pu.get(xb           , yb)->motion, x0+CbSize*3/8*zoom, cy, zoom, drawmode, ascent, descent);
                            display_265_motion_vectors(win, pic->pu.get(xb+CbSize*3/4, yb)->motion, x0+CbSize*7/8*zoom, cy, zoom, drawmode, ascent, descent);
                            break;
                        default:
                            assert(false);
                            break;
                    }
                }
            }

            /* draw "status bar" overlay */
            int len = 0;
            int cu_qp = cu->qpy;
            if ((cu_qp!=prev_cu_qp) || (xcb==0 && ycb==0))
                len += snprintf(string+len, sizeof(string)-len, "Qp=%d", cu_qp);
            prev_cu_qp = cu_qp;

            if (len && draw_status_bar) {
                int wid = XTextWidth(win->font[1], string, len);
                XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, by-descent, string, len);
            }
#if 0

            /* draw stuffing bits overlay */
            len = 0;
            if (picture->macro[n].stuff_bits)
                len += snprintf(string+len, sizeof(string)-len, "%dB", picture->macro[n].stuff_bits/8);

            if (len && draw_status_bar) {
                wid = XTextWidth(win->font[1], string, len);
                XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, by-descent-line, string, len);
            }

            /* draw number of bits */
            len = 0;
            if (picture->macro[n].num_bits)
                len += snprintf(string+len, sizeof(string)-len, "%db", picture->macro[n].num_bits);

            if (len && draw_numbits) {
                wid = XTextWidth(win->font[1], string, len);
                XDrawImageString(win->display, win->pixmap, win->gc, cx-wid/2, y0+ascent, string, len);
            }
#endif
        }
    }
}

void free_display(struct rvwin *win)
{
    int i;
#ifdef SHMEM
    if (win->shmem) {
        XShmDetach(win->display, &win->shminfo);
        XDestroyImage(win->ximage);
        shmdt(win->shminfo.shmaddr);
        shmctl(win->shminfo.shmid, IPC_RMID, 0);
    }
#endif
    for (i=0; i<2; i++)
        XFreeFont(win->display, win->font[i]);
    XFreeGC(win->display, win->gc);
    XFreePixmap(win->display, win->pixmap);
    free_clip_table();
}
