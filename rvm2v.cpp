/*
 * Description: Functions related to the MPEG2 video standard.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-10-28 16:07:45 $
 * Revision   : $Revision: 1.80 $
 * Copyright  : (c) 2007 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdlib.h>
#include <string.h>

#include "rvm2v.h"
#include "rvtransform.h"
#include "rvrecon.h"

/*
 * mpeg2 lookup tables
 */

const char *aspect_ratio_table[5] = {
    "NA", "1:1", "4:3", "16:9", "2.21:1"
};

const float frame_rate_table[9] = {
    0.0, 24000.0/1001.0, 24.0, 25.0, 30000.0/1001.0, 30.0, 50.0, 60000.0/1001.0, 60.0
};

static const int default_intra_quantiser_matrix[64] = {
    8 , 16, 19, 22, 26, 27, 29, 34,
    16, 16, 22, 24, 27, 29, 34, 37,
    19, 22, 26, 27, 29, 34, 34, 38,
    22, 22, 26, 27, 29, 34, 37, 40,
    22, 26, 27, 29, 32, 35, 40, 48,
    26, 27, 29, 32, 35, 40, 48, 58,
    26, 27, 29, 34, 38, 46, 56, 69,
    27, 29, 35, 38, 46, 56, 69, 83
};

static const int default_non_intra_quantiser_matrix[64] = {
    16, 16, 16, 16, 16, 16, 16, 16,
    16, 16, 16, 16, 16, 16, 16, 16,
    16, 16, 16, 16, 16, 16, 16, 16,
    16, 16, 16, 16, 16, 16, 16, 16,
    16, 16, 16, 16, 16, 16, 16, 16,
    16, 16, 16, 16, 16, 16, 16, 16,
    16, 16, 16, 16, 16, 16, 16, 16,
    16, 16, 16, 16, 16, 16, 16, 16
};

static const int quantiser_scale_table[2][32] = {
        {0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,58,60,62},
        {0,1,2,3,4,5,6,7,8,10,12,14,16,18,20,22,24,28,32,36,40,44,48,52,56,64,72,80,88,96,104,112}
    };

static const int block_count[4] = {0, 6, 8, 12};

static void decode_m2v_dc(struct rvm2vmacro *macro, int chroma_format, int dct_dc_pred[3]);
static int decode_m2v_motion_vector(int f_code, int motion_code, int motion_residual);
static void decode_m2v_vectors(struct rvm2vmacro *macro, const struct rvm2vpicture *picture, struct mv_t PMV[2][2]);
static int check_m2v_vectors(struct rvm2vmacro *macro, const char *filename, int picno, const struct rvm2vpicture *picture, int mbaddr, int width, int height);

/*
 * mpeg2 utility functions.
 */
void describe_profile(int profile_and_level, const char **p, const char **l)
{
    *p = "";
    *l = "";
    if (profile_and_level&0x80) {
        /* escape bit set */
        switch (profile_and_level) {
            case 0x8e : *p = "MVP"; *l = "LL"; break;
            case 0x8d : *p = "MVP"; *l = "ML"; break;
            case 0x8b : *p = "MVP"; *l = "H14"; break;
            case 0x8a : *p = "MVP"; *l = "HL"; break;
            case 0x85 : *p = "422P"; *l = "ML"; break;
            case 0x82 : *p = "422P"; *l = "HL"; break;
        }
    } else {
        switch (profile_and_level>>4) {
            case 0x5 : *p = "SP"; break;
            case 0x4 : *p = "MP"; break;
            case 0x3 : *p = "SNR"; break;
            case 0x2 : *p = "SPT"; break;
            case 0x1 : *p = "HP"; break;
        }
        switch (profile_and_level&0xf) {
            case 0xa : *l = "LL"; break;
            case 0x8 : *l = "ML"; break;
            case 0x6 : *l = "H14"; break;
            case 0x4 : *l = "HL"; break;
        }
    }
}

const char *describe_picture(struct rvm2vpicture *picture)
{
    switch (picture->extension.picture_structure) {
        case PST_FRAME  : return "frame";
        case PST_TOP    : return "top field";
        case PST_BOTTOM : return "bottom field";
        default         : return "unknown";
    }
}

int frame_picture(const struct rvm2vpicture *picture)
{
    return picture->extension.picture_structure == PST_FRAME;
}

int field_picture(const struct rvm2vpicture *picture)
{
    return picture->extension.picture_structure != PST_FRAME;
}

int top_field_picture(const struct rvm2vpicture *picture)
{
    return picture->extension.picture_structure == PST_TOP;
}

int bottom_field_picture(const struct rvm2vpicture *picture)
{
    return picture->extension.picture_structure == PST_BOTTOM;
}

int first_field_picture(const struct rvm2vpicture *picture, const struct rvm2vpicture *prev_picture)
{
    if (!picture || !prev_picture)
        return 0;
    return picture->temporal_reference != prev_picture->temporal_reference;
}

int second_field_picture(const struct rvm2vpicture *picture, const struct rvm2vpicture *prev_picture)
{
    if (!picture || !prev_picture)
        return 0;
    return picture->temporal_reference == prev_picture->temporal_reference;
}

int macroblock_intra(int macroblock_type)
{
    return macroblock_type & 0x2;
}

int macroblock_pattern(int macroblock_type)
{
    return macroblock_type & 0x4;
}

int macroblock_motion_backward(int macroblock_type)
{
    return macroblock_type & 0x8;
}

int macroblock_motion_forward(int macroblock_type)
{
    return macroblock_type & 0x10;
}

int macroblock_quant(int macroblock_type)
{
    return macroblock_type & 0x20;
}

int macroblock_error(int macroblock_type)
{
    return macroblock_type & 0xC0;
}

int macroblock_error_badvectors(int macroblock_type)
{
    return macroblock_type & 0x80;
}

static void mark_error_macroblock(struct rvm2vmacro *macro)
{
    macro->type |= 0x40;
}

static void mark_error_badvectors(struct rvm2vmacro *macro)
{
    macro->type |= 0x80; /* FIXME this error marking scheme can't scale as 'type' is a char */
}

int motion_vector_count(int frame_motion_type, int field_motion_type)
{
    if (frame_motion_type)
        return frame_motion_type==1? 2 : 1;
    if (field_motion_type)
        return field_motion_type==2? 2 : 1;
    rvmessage("neither frame_motion_type nor field_motion_type set in motion_vector_count");
    return 0;
}

enum mv_format_t mv_format(int frame_motion_type, int field_motion_type)
{
    if (frame_motion_type)
        return frame_motion_type==2? MV_FORMAT_FRAME : MV_FORMAT_FIELD;
    if (field_motion_type)
        return MV_FORMAT_FIELD;
    rvmessage("neither frame_motion_type nor field_motion_type set in mv_format");
    return MV_FORMAT_FRAME;
}

int dmv(int frame_motion_type, int field_motion_type)
{
    if (frame_motion_type)
        return frame_motion_type==3? 1 : 0;
    if (field_motion_type)
        return field_motion_type==3? 1 : 0;
    rvmessage("neither frame_motion_type nor field_motion_type set in motion_vector_count");
    return 0;
}

int picture_repeat(int progressive_sequence, int progressive_frame, int repeat_first_field, int top_field_first)
{
    if (progressive_sequence==0) {
        if (progressive_frame==0)
            return 2; /* fields */
        else {
            if (repeat_first_field)
                return 3; /* fields */
            else
                return 2; /* fields */
        }
    } else {
        if (repeat_first_field) {
            if (top_field_first)
                return 3; /* frames */
            else
                return 2; /* frames */
        } else
            return 1; /* frames */
    }
}

int max_bit_rate_profile_level(int profile, int level)
{
    switch (profile) {
        case 5: /* simple */
            return 15000000;
        case 4: /* main */
            switch (level) {
                case 4 : return 80000000;
                case 6 : return 60000000;
                case 8 : return 15000000;
                case 10: return 4000000;
                default: return 0;
            }
        case 1: /* high */
            switch (level) {
                case 4 : return 100000000;
                case 6 : return 80000000;
                case 8 : return 20000000;
                default: return 0;
            }
        case 0: /* 4:2:2 */
            switch (level) {
                case 2 : return 300000000;
                case 5 : return 50000000;
                default: return 0;
            }
        default:
            return 0;
    }
}

int max_vbv_buffer_size_profile_level(int profile, int level)
{
    switch (profile) {
        case 5: /* simple */
            return 1835008;
        case 4: /* main */
            switch (level) {
                case 4 : return 9781248;
                case 6 : return 7340032;
                case 8 : return 1835008;
                case 10: return 475136;
                default: return 0;
            }
        case 1: /* high */
            switch (level) {
                case 4 : return 12222464;
                case 6 : return 9781248;
                case 8 : return 2441216;
                default: return 0;
            }
        case 0: /* 4:2:2 */
            switch (level) {
                case 2 : return 47185920;
                case 5 : return 9437184;
                default: return 0;
            }
        default:
            return 0;
    }
}

/*
 * mpeg2 housekeeping functions.
 */

struct rvm2vpicture *alloc_m2v_picture()
{
    size_t size = sizeof(struct rvm2vpicture);
    struct rvm2vpicture *picture = (struct rvm2vpicture *)rvalloc(NULL, size, 1);
    return picture;
}

void free_m2v_picture(struct rvm2vpicture* picture)
{
    if (picture) {
        rvfree(picture->macro);
        free(picture);
    }
}

void free_m2v_sequence(struct rvm2vsequence* sequence)
{
    int i;
    if (sequence) {
        for (i=0; i<sequence->size_pic_array; i++) {
            if (sequence->picture[i])
                free_m2v_picture(sequence->picture[i]);
            if (sequence->picture2[i])
                free_m2v_picture(sequence->picture2[i]);
        }
        rvfree(sequence->picture);
        rvfree(sequence->picture2);
        free(sequence);
    }
}

/*
 * mpeg2 header parsing functions.
 */

int find_m2v_next_start_code(struct bitbuf *bb)
{
    alignbits(bb);
    while (showbits24(bb)!=0x000001) {
        /* move along by one byte */
        flushbits(bb, 8);
        if (eofbits(bb))
            return -1;
    }
    return showbits32(bb);
}

int parse_m2v_sequence(struct bitbuf *bb, struct rvm2vsequence *seq)
{
    int i;

    seq->horizontal_size_value = getbits(bb, 12);                               /* horizontal_size_value */
    seq->vertical_size_value = getbits(bb, 12);                                 /* vertical_size_value */
    seq->aspect_ratio_information = getbits(bb, 4);                             /* aspect_ratio_information */
    seq->frame_rate_code = getbits(bb, 4);                                      /* frame_rate_code */
    seq->bit_rate = getbits(bb, 18);                                            /* bit_rate_value */
    if (!getbit(bb))                                                            /* marker_bit */
        rvmessage("mpeg2 sequence header: marker_bit should be 1");
    seq->vbv_buffer_size = getbits(bb, 10);                                     /* vbv_buffer_size_value */
    seq->constrained_parameters_flag = getbit(bb);                              /* constrained_parameters_flag */
    seq->load_intra_quantiser_matrix = getbit(bb);                              /* load_intra_quantiser_matrix */
    if (seq->load_intra_quantiser_matrix)
        for (i=0; i<64; i++)
            seq->intra_quantiser_matrix[zigzag[i]] = seq->chroma_intra_quantiser_matrix[zigzag[i]] = getbits8(bb);
    seq->load_non_intra_quantiser_matrix = getbit(bb);                          /* load_non_intra_quantiser_matrix */
    if (seq->load_non_intra_quantiser_matrix)
        for (i=0; i<64; i++)
            seq->non_intra_quantiser_matrix[zigzag[i]] = seq->chroma_non_intra_quantiser_matrix[zigzag[i]] = getbits8(bb);
    seq->frame_rate = frame_rate_table[seq->frame_rate_code];
    return 0;
}

void parse_m2v_user_data(struct bitbuf *bb, int verbose)
{
    size_t i = 0;
    size_t len = 256;

    /* allocate buffer for user data */
    char *s = (char *)rvalloc(NULL, len*sizeof(char), 0);

    /* loop over user data */
    while (showbits24(bb)!=0x000001) {
        if (i==len)
            s = (char *)rvalloc(s, len+=256, 0);
        s[i++] = getbits8(bb);                  /* user_data */
    }
    s[i] = '\0';

    /* display user data */
    if (verbose>=2)
        rvmessage("user data: %s", s);
    free(s);
}

int parse_m2v_extension_and_user_data(struct bitbuf *bb, int verbose)
{
    /* get start code type */
    do {
        int start = find_m2v_next_start_code(bb);
        if (start<0)
            return -1;
        switch (start) {
            case 0x1B2: /* user data start code */
                flushbits(bb, 32);              /* user_data_start_code */
                parse_m2v_user_data(bb, verbose);
                break;

            case 0x1B5: /* extension start code */
                flushbits(bb, 32);              /* extension_start_code */
                start = getbits(bb, 4);         /* extension_start_code_identifier */
                switch (start) {
                    default:
                        break;
                }
                break;

            default:
                return 0;
        }
    } while (1);
}

int parse_m2v_sequence_extension(struct bitbuf *bb, struct rvm2vsequence *seq)
{
    int bit_rate_extension;
    int vbv_buffer_size_extension;
    seq->profile_and_level = getbits8(bb);      /* profile_and_level_indication */
    seq->progressive_sequence = getbit(bb);     /* progressive_sequence */
    seq->chroma_format = getbits(bb, 2);        /* chroma_format */
    seq->horizontal_size_value += 4096*getbits(bb, 2);/* horizontal_size_extension */
    seq->vertical_size_value += 4096*getbits(bb, 2);/* vertical_size_extension */
    bit_rate_extension = getbits(bb, 12);       /* bit_rate_extension */
    if (!getbit(bb))                            /* marker_bit */
        rvmessage("mpeg2 sequence extension: marker_bit should be 1");
    vbv_buffer_size_extension = getbits8(bb);   /* vbv_buffer_size_extension */
    seq->low_delay = getbits(bb, 1);            /* low_delay */
    seq->frame_rate_extension_n = getbits(bb, 2);/* frame_rate_extension_n */
    seq->frame_rate_extension_d = getbits(bb, 5);/* frame_rate_extension_d */
    seq->vbv_buffer_size |= vbv_buffer_size_extension << 10;
    seq->bit_rate |= bit_rate_extension << 18;
    seq->frame_rate *= ((float)seq->frame_rate_extension_n + 1.0) / ((float)seq->frame_rate_extension_d + 1.0);
    return 0;
}

int parse_m2v_sequence_display_extension(struct bitbuf *bb, struct rvm2vsequence *seq)
{
    flushbits(bb, 3);                           /* video_format */
    if (getbit(bb)) {                           /* colour_description */
        flushbits(bb, 8);                       /* colour_primaries */
        flushbits(bb, 8);                       /* transfer_characteristics */
        flushbits(bb, 8);                       /* matrix_coefficients */
    }
    seq->display_horizontal_size = getbits(bb, 14);/* display_horizontal_size */
    if (!getbit(bb))                            /* marker_bit */
        rvmessage("mpeg2 sequence display extension: marker_bit should be 1");
    seq->display_vertical_size = getbits(bb, 14);/* display_vertical_size */
    return 0;
}

int parse_m2v_quant_matrix_extension(struct bitbuf *bb, struct rvm2vquantmatrix *mat)
{
    int i;
    mat->load_intra_quantiser_matrix = getbit(bb);                              /* load_intra_quantiser_matrix */
    if (mat->load_intra_quantiser_matrix)
        for (i=0; i<64; i++)
            mat->intra_quantiser_matrix[zigzag[i]] = getbits8(bb);
    mat->load_non_intra_quantiser_matrix = getbit(bb);                          /* load_non_intra_quantiser_matrix */
    if (mat->load_non_intra_quantiser_matrix)
        for (i=0; i<64; i++)
            mat->non_intra_quantiser_matrix[zigzag[i]] = getbits8(bb);
    mat->load_chroma_intra_quantiser_matrix = getbit(bb);                       /* load_chroma_intra_quantiser_matrix */
    if (mat->load_chroma_intra_quantiser_matrix)
        for (i=0; i<64; i++)
            mat->chroma_intra_quantiser_matrix[zigzag[i]] = getbits8(bb);
    mat->load_chroma_non_intra_quantiser_matrix = getbit(bb);                   /* load_chroma_non_intra_quantiser_matrix */
    if (mat->load_chroma_non_intra_quantiser_matrix)
        for (i=0; i<64; i++)
            mat->chroma_non_intra_quantiser_matrix[zigzag[i]] = getbits8(bb);
    return 0;
}

int parse_m2v_picture_display_extension(struct bitbuf *bb, int progressive_sequence, int repeat_first_field, int top_field_first, int picture_structure)
{
    int i, number_of_frame_centre_offsets;
    if (progressive_sequence==1) {
        if (repeat_first_field==1) {
            if (top_field_first==1)
                number_of_frame_centre_offsets = 3;
            else
                number_of_frame_centre_offsets = 2;
        } else {
                number_of_frame_centre_offsets = 1;
        }
    } else {
        if (picture_structure!=PST_FRAME) {
            number_of_frame_centre_offsets = 1;
        } else {
            if (repeat_first_field==1)
                number_of_frame_centre_offsets = 3;
            else
                number_of_frame_centre_offsets = 2;
        }
    }
    for (i=0; i<number_of_frame_centre_offsets; i++) {
        flushbits(bb, 16);                      /* frame_centre_horizontal_offset */
        if (!getbit(bb))                        /* marker_bit */
            rvmessage("mpeg2 picture display extension: marker_bit should be 1");
        flushbits(bb, 16);                      /* frame_centre_vertical_offset */
        if (!getbit(bb))                        /* marker_bit */
            rvmessage("mpeg2 picture display extension: marker_bit should be 1");
    }
    return 0;
}

int parse_m2v_copyright_extension(struct bitbuf *bb)
{
    flushbits(bb, 1);                           /* copyright_flag */
    flushbits(bb, 8);                           /* copyright_identifier */
    flushbits(bb, 1);                           /* original_or_copy */
    flushbits(bb, 7);                           /* reserved */
    if (!getbit(bb))                            /* marker_bit */
        rvmessage("mpeg2 copyright extension: marker_bit should be 1");
    flushbits(bb, 20);                          /* copyright_number_1 */
    if (!getbit(bb))                            /* marker_bit */
        rvmessage("mpeg2 copyright extension: marker_bit should be 1");
    flushbits(bb, 22);                          /* copyright_number_2 */
    if (!getbit(bb))                            /* marker_bit */
        rvmessage("mpeg2 copyright extension: marker_bit should be 1");
    flushbits(bb, 22);                          /* copyright_number_3 */
    return 0;
}

int parse_m2v_group_of_pictures(struct bitbuf *bb, int *gop_flags)
{
    flushbits(bb, 25);                          /* time_code */
    int closed_gop = getbits(bb, 1);            /* closed_gop */
    int broken_link = getbits(bb, 1);           /* broken_link */
    if (gop_flags)
        *gop_flags = NEW_GOP | (closed_gop & CLOSED_GOP) | (broken_link | BROKEN_LINK);
    return 0;
}

struct rvm2vpicture *parse_m2v_picture_header(struct bitbuf *bb)
{
    /* allocate picture structure */
    struct rvm2vpicture *pic = alloc_m2v_picture();
    if (pic==NULL)
        return pic;

    pic->temporal_reference = getbits(bb, 10);  /* temporal_reference */
    pic->picture_coding_type = (enum pct_t)getbits(bb, 3);  /* picture_coding_type */
    pic->vbv_delay = getbits(bb, 16);           /* vbv_delay */
    if (pic->picture_coding_type==PCT_P || pic->picture_coding_type==PCT_B) {
        pic->full_pel_forward_vector = getbit(bb);/* full_pel_forward_vector */
        pic->forward_f_code = getbits(bb, 3);   /* forward_f_code */
    }
    if (pic->picture_coding_type==PCT_B) {
        pic->full_pel_backward_vector = getbit(bb);/* full_pel_backward_vector */
        pic->backward_f_code = getbits(bb, 3);  /* backward_f_code */
    }
    while (getbit(bb)==1) {
        flushbits(bb, 8);                       /* extra_information_picture */
    }
    return pic;
}

int parse_m2v_picture_extension(struct bitbuf *bb, struct rvm2vextension *ext)
{
    ext->f_code[0][0] = getbits(bb, 4);         /* f_code forward horizontal */
    ext->f_code[0][1] = getbits(bb, 4);         /* f_code forward vertical */
    ext->f_code[1][0] = getbits(bb, 4);         /* f_code backward horizontal */
    ext->f_code[1][1] = getbits(bb, 4);         /* f_code backward vertical */
    ext->intra_dc_precision = getbits(bb, 2);   /* intra_dc_precision */
    ext->picture_structure = (enum pst_t)getbits(bb, 2);    /* picture_structure */
    ext->top_field_first = getbit(bb);          /* top_field_first */
    ext->frame_pred_frame_dct = getbit(bb);     /* frame_pred_frame_dct */
    ext->concealment_motion_vectors = getbit(bb);/* concealment_motion_vectors */
    ext->q_scale_type = getbit(bb);             /* q_scale_type */
    ext->intra_vlc_format = getbit(bb);         /* intra_vlc_format */
    ext->alternate_scan = getbit(bb);           /* alternate_scan */
    ext->repeat_first_field = getbit(bb);       /* repeat_first_field */
    ext->chroma_420_type = getbit(bb);          /* chroma_420_type */
    ext->progressive_frame = getbit(bb);        /* progressive_frame */
    if (getbit(bb)) {                           /* composite_display_flag */
        flushbits(bb, 1);                       /* v_axis */
        flushbits(bb, 3);                       /* field_sequence */
        flushbits(bb, 1);                       /* sub_carrier */
        flushbits(bb, 7);                       /* burst_amplitude */
        flushbits(bb, 8);                       /* sub_carrier_phase */
    }
    return 0;
}

/*
 * mpeg2 macroblock parsing functions.
 */

static int parse_m2v_macroblock_address_increment(struct bitbuf *bb)
{
    if (showbits(bb, 1 ) == 1 ) /* 1           */ { flushbits(bb, 1 ); return 1;  }
    if (showbits(bb, 3 ) == 3 ) /* 011         */ { flushbits(bb, 3 ); return 2;  }
    if (showbits(bb, 3 ) == 2 ) /* 010         */ { flushbits(bb, 3 ); return 3;  }
    if (showbits(bb, 4 ) == 3 ) /* 0011        */ { flushbits(bb, 4 ); return 4;  }
    if (showbits(bb, 4 ) == 2 ) /* 0010        */ { flushbits(bb, 4 ); return 5;  }
    if (showbits(bb, 5 ) == 3 ) /* 00011       */ { flushbits(bb, 5 ); return 6;  }
    if (showbits(bb, 5 ) == 2 ) /* 00010       */ { flushbits(bb, 5 ); return 7;  }
    if (showbits(bb, 7 ) == 7 ) /* 0000111     */ { flushbits(bb, 7 ); return 8;  }
    if (showbits(bb, 7 ) == 6 ) /* 0000110     */ { flushbits(bb, 7 ); return 9;  }
    if (showbits(bb, 8 ) == 11) /* 00001011    */ { flushbits(bb, 8 ); return 10; }
    if (showbits(bb, 8 ) == 10) /* 00001010    */ { flushbits(bb, 8 ); return 11; }
    if (showbits(bb, 8 ) == 9 ) /* 00001001    */ { flushbits(bb, 8 ); return 12; }
    if (showbits(bb, 8 ) == 8 ) /* 00001000    */ { flushbits(bb, 8 ); return 13; }
    if (showbits(bb, 8 ) == 7 ) /* 00000111    */ { flushbits(bb, 8 ); return 14; }
    if (showbits(bb, 8 ) == 6 ) /* 00000110    */ { flushbits(bb, 8 ); return 15; }
    if (showbits(bb, 10) == 23) /* 0000010111  */ { flushbits(bb, 10); return 16; }
    if (showbits(bb, 10) == 22) /* 0000010110  */ { flushbits(bb, 10); return 17; }
    if (showbits(bb, 10) == 21) /* 0000010101  */ { flushbits(bb, 10); return 18; }
    if (showbits(bb, 10) == 20) /* 0000010100  */ { flushbits(bb, 10); return 19; }
    if (showbits(bb, 10) == 19) /* 0000010011  */ { flushbits(bb, 10); return 20; }
    if (showbits(bb, 10) == 18) /* 0000010010  */ { flushbits(bb, 10); return 21; }
    if (showbits(bb, 11) == 35) /* 00000100011 */ { flushbits(bb, 11); return 22; }
    if (showbits(bb, 11) == 34) /* 00000100010 */ { flushbits(bb, 11); return 23; }
    if (showbits(bb, 11) == 33) /* 00000100001 */ { flushbits(bb, 11); return 24; }
    if (showbits(bb, 11) == 32) /* 00000100000 */ { flushbits(bb, 11); return 25; }
    if (showbits(bb, 11) == 31) /* 00000011111 */ { flushbits(bb, 11); return 26; }
    if (showbits(bb, 11) == 30) /* 00000011110 */ { flushbits(bb, 11); return 27; }
    if (showbits(bb, 11) == 29) /* 00000011101 */ { flushbits(bb, 11); return 28; }
    if (showbits(bb, 11) == 28) /* 00000011100 */ { flushbits(bb, 11); return 29; }
    if (showbits(bb, 11) == 27) /* 00000011011 */ { flushbits(bb, 11); return 30; }
    if (showbits(bb, 11) == 26) /* 00000011010 */ { flushbits(bb, 11); return 31; }
    if (showbits(bb, 11) == 25) /* 00000011001 */ { flushbits(bb, 11); return 32; }
    if (showbits(bb, 11) == 24) /* 00000011000 */ { flushbits(bb, 11); return 33; }
    if (showbits(bb, 11) == 8 ) /* 00000001000 */ { flushbits(bb, 11); return 34; } /* macroblock escape */
    return -1;
}

static int parse_m2v_macroblock_type(struct bitbuf *bb, enum pct_t picture_coding_type)
{
    if (picture_coding_type==PCT_I) {
        if (showbits(bb, 1) == 1 ) /* 1      */ { flushbits(bb, 1); return 0x02; } /*        Intra        */
        if (showbits(bb, 2) == 1 ) /* 01     */ { flushbits(bb, 2); return 0x22; } /*     Intra, Quant    */
    } else if (picture_coding_type==PCT_P) {
        if (showbits(bb, 1) == 1 ) /* 1      */ { flushbits(bb, 1); return 0x14; } /*     MC, Coded       */
        if (showbits(bb, 2) == 1 ) /* 01     */ { flushbits(bb, 2); return 0x04; } /*    No MC, Coded     */
        if (showbits(bb, 3) == 1 ) /* 001    */ { flushbits(bb, 3); return 0x10; } /*   MC, Not Coded     */
        if (showbits(bb, 5) == 3 ) /* 00011  */ { flushbits(bb, 5); return 0x02; } /*         Intra       */
        if (showbits(bb, 5) == 2 ) /* 00010  */ { flushbits(bb, 5); return 0x34; } /*  MC, Coded, Quant   */
        if (showbits(bb, 5) == 1 ) /* 00001  */ { flushbits(bb, 5); return 0x24; } /* No MC, Coded, Quant */
        if (showbits(bb, 6) == 1 ) /* 000001 */ { flushbits(bb, 6); return 0x22; } /*     Intra, Quant    */
    } else if (picture_coding_type==PCT_B) {
        if (showbits(bb, 2) == 2 ) /* 10     */ { flushbits(bb, 2); return 0x18; } /*   Interp, Not Coded */
        if (showbits(bb, 2) == 3 ) /* 11     */ { flushbits(bb, 2); return 0x1C; } /*     Interp, Coded   */
        if (showbits(bb, 3) == 2 ) /* 010    */ { flushbits(bb, 3); return 0x08; } /*    Bwd, Not Coded   */
        if (showbits(bb, 3) == 3 ) /* 011    */ { flushbits(bb, 3); return 0x0C; } /*      Bwd, Coded     */
        if (showbits(bb, 4) == 2 ) /* 0010   */ { flushbits(bb, 4); return 0x10; } /*    Fwd, Not Coded   */
        if (showbits(bb, 4) == 3 ) /* 0011   */ { flushbits(bb, 4); return 0x14; } /*      Fwd, Coded     */
        if (showbits(bb, 5) == 3 ) /* 00011  */ { flushbits(bb, 5); return 0x02; } /*          Intra      */
        if (showbits(bb, 5) == 2 ) /* 00010  */ { flushbits(bb, 5); return 0x3C; } /* Interp, Coded, Quant*/
        if (showbits(bb, 6) == 3 ) /* 000011 */ { flushbits(bb, 6); return 0x34; } /*  Fwd, Coded, Quant  */
        if (showbits(bb, 6) == 2 ) /* 000010 */ { flushbits(bb, 6); return 0x2C; } /*  Bwd, Coded, Quant  */
        if (showbits(bb, 6) == 1 ) /* 000001 */ { flushbits(bb, 6); return 0x22; } /*      Intra, Quant   */
    } else if (picture_coding_type==PCT_D) {
        if (showbits(bb, 1) == 1 ) /* 1      */ { flushbits(bb, 1); return 0x02; } /*        Intra        */
    }

    return -1;
}

static int parse_m2v_motion_code(struct bitbuf *bb)
{
    if (showbits(bb, 11) == 25 ) /* 00000011001 */ { flushbits(bb, 11); return -16; }
    if (showbits(bb, 11) == 27 ) /* 00000011011 */ { flushbits(bb, 11); return -15; }
    if (showbits(bb, 11) == 29 ) /* 00000011101 */ { flushbits(bb, 11); return -14; }
    if (showbits(bb, 11) == 31 ) /* 00000011111 */ { flushbits(bb, 11); return -13; }
    if (showbits(bb, 11) == 33 ) /* 00000100001 */ { flushbits(bb, 11); return -12; }
    if (showbits(bb, 11) == 35 ) /* 00000100011 */ { flushbits(bb, 11); return -11; }
    if (showbits(bb, 10) == 19 ) /* 0000010011  */ { flushbits(bb, 10); return -10; }
    if (showbits(bb, 10) == 21 ) /* 0000010101  */ { flushbits(bb, 10); return -9;  }
    if (showbits(bb, 10) == 23 ) /* 0000010111  */ { flushbits(bb, 10); return -8;  }
    if (showbits(bb, 8 ) == 7  ) /* 00000111    */ { flushbits(bb, 8 ); return -7;  }
    if (showbits(bb, 8 ) == 9  ) /* 00001001    */ { flushbits(bb, 8 ); return -6;  }
    if (showbits(bb, 8 ) == 11 ) /* 00001011    */ { flushbits(bb, 8 ); return -5;  }
    if (showbits(bb, 7 ) == 7  ) /* 0000111     */ { flushbits(bb, 7 ); return -4;  }
    if (showbits(bb, 5 ) == 3  ) /* 00011       */ { flushbits(bb, 5 ); return -3;  }
    if (showbits(bb, 4 ) == 3  ) /* 0011        */ { flushbits(bb, 4 ); return -2;  }
    if (showbits(bb, 3 ) == 3  ) /* 011         */ { flushbits(bb, 3 ); return -1;  }
    if (showbits(bb, 1 ) == 1  ) /* 1           */ { flushbits(bb, 1 ); return 0;   }
    if (showbits(bb, 3 ) == 2  ) /* 010         */ { flushbits(bb, 3 ); return 1;   }
    if (showbits(bb, 4 ) == 2  ) /* 0010        */ { flushbits(bb, 4 ); return 2;   }
    if (showbits(bb, 5 ) == 2  ) /* 00010       */ { flushbits(bb, 5 ); return 3;   }
    if (showbits(bb, 7 ) == 6  ) /* 0000110     */ { flushbits(bb, 7 ); return 4;   }
    if (showbits(bb, 8 ) == 10 ) /* 00001010    */ { flushbits(bb, 8 ); return 5;   }
    if (showbits(bb, 8 ) == 8  ) /* 00001000    */ { flushbits(bb, 8 ); return 6;   }
    if (showbits(bb, 8 ) == 6  ) /* 00000110    */ { flushbits(bb, 8 ); return 7;   }
    if (showbits(bb, 10) == 22 ) /* 0000010110  */ { flushbits(bb, 10); return 8;   }
    if (showbits(bb, 10) == 20 ) /* 0000010100  */ { flushbits(bb, 10); return 9;   }
    if (showbits(bb, 10) == 18 ) /* 0000010010  */ { flushbits(bb, 10); return 10;  }
    if (showbits(bb, 11) == 34 ) /* 00000100010 */ { flushbits(bb, 11); return 11;  }
    if (showbits(bb, 11) == 32 ) /* 00000100000 */ { flushbits(bb, 11); return 12;  }
    if (showbits(bb, 11) == 30 ) /* 00000011110 */ { flushbits(bb, 11); return 13;  }
    if (showbits(bb, 11) == 28 ) /* 00000011100 */ { flushbits(bb, 11); return 14;  }
    if (showbits(bb, 11) == 26 ) /* 00000011010 */ { flushbits(bb, 11); return 15;  }
    if (showbits(bb, 11) == 24 ) /* 00000011000 */ { flushbits(bb, 11); return 16;  }
    return -255;
}

static int parse_m2v_dmvector(struct bitbuf *bb)
{
    if (showbits(bb, 2) == 3 ) /* 11 */ { flushbits(bb, 2); return -1; }
    if (showbits(bb, 1) == 0 ) /* 0  */ { flushbits(bb, 1); return 0;  }
    if (showbits(bb, 2) == 2 ) /* 10 */ { flushbits(bb, 2); return 1;  }
    return -255;
}

static int parse_m2v_dct_dc_size_luminance(struct bitbuf *bb)
{
    if (showbits(bb, 3) == 4   ) /* 100       */ { flushbits(bb, 3); return 0;  }
    if (showbits(bb, 2) == 0   ) /* 00        */ { flushbits(bb, 2); return 1;  }
    if (showbits(bb, 2) == 1   ) /* 01        */ { flushbits(bb, 2); return 2;  }
    if (showbits(bb, 3) == 5   ) /* 101       */ { flushbits(bb, 3); return 3;  }
    if (showbits(bb, 3) == 6   ) /* 110       */ { flushbits(bb, 3); return 4;  }
    if (showbits(bb, 4) == 14  ) /* 1110      */ { flushbits(bb, 4); return 5;  }
    if (showbits(bb, 5) == 30  ) /* 11110     */ { flushbits(bb, 5); return 6;  }
    if (showbits(bb, 6) == 62  ) /* 111110    */ { flushbits(bb, 6); return 7;  }
    if (showbits(bb, 7) == 126 ) /* 1111110   */ { flushbits(bb, 7); return 8;  }
    if (showbits(bb, 8) == 254 ) /* 11111110  */ { flushbits(bb, 8); return 9;  }
    if (showbits(bb, 9) == 510 ) /* 111111110 */ { flushbits(bb, 9); return 10; }
    if (showbits(bb, 9) == 511 ) /* 111111111 */ { flushbits(bb, 9); return 11; }
    return -1;
}

static int parse_m2v_dct_dc_size_chrominance(struct bitbuf *bb)
{
    if (showbits(bb, 2)  == 0    ) /* 00         */ { flushbits(bb, 2);  return 0;  }
    if (showbits(bb, 2)  == 1    ) /* 01         */ { flushbits(bb, 2);  return 1;  }
    if (showbits(bb, 2)  == 2    ) /* 10         */ { flushbits(bb, 2);  return 2;  }
    if (showbits(bb, 3)  == 6    ) /* 110        */ { flushbits(bb, 3);  return 3;  }
    if (showbits(bb, 4)  == 14   ) /* 1110       */ { flushbits(bb, 4);  return 4;  }
    if (showbits(bb, 5)  == 30   ) /* 11110      */ { flushbits(bb, 5);  return 5;  }
    if (showbits(bb, 6)  == 62   ) /* 111110     */ { flushbits(bb, 6);  return 6;  }
    if (showbits(bb, 7)  == 126  ) /* 1111110    */ { flushbits(bb, 7);  return 7;  }
    if (showbits(bb, 8)  == 254  ) /* 11111110   */ { flushbits(bb, 8);  return 8;  }
    if (showbits(bb, 9)  == 510  ) /* 111111110  */ { flushbits(bb, 9);  return 9;  }
    if (showbits(bb, 10) == 1022 ) /* 1111111110 */ { flushbits(bb, 10); return 10; }
    if (showbits(bb, 10) == 1023 ) /* 1111111111 */ { flushbits(bb, 10); return 11; }
    return -1;
}

static struct rvtcoeff parse_m2v_coefficient_zero(struct bitbuf *bb, int first, int mpeg1)
{
    struct rvtcoeff tcoeff;
    tcoeff.run   = -1;
    tcoeff.level = 0;
    tcoeff.sign  = 0;

if (first) {
    if (showbits(bb, 1 ) == 1  ) /* 1s                */ { flushbits(bb, 1 ); tcoeff.run = 0 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
} else {
    if (showbits(bb, 2 ) == 2  ) /* 10                */ { flushbits(bb, 2 ); tcoeff.run = 0 ; tcoeff.level =  0; return tcoeff; } /* End of Block */
    if (showbits(bb, 2 ) == 3  ) /* 11s               */ { flushbits(bb, 2 ); tcoeff.run = 0 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
}
    if (showbits(bb, 3 ) == 3  ) /* 011s              */ { flushbits(bb, 3 ); tcoeff.run = 1 ; tcoeff.level = 1 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 4 ) == 4  ) /* 0100s             */ { flushbits(bb, 4 ); tcoeff.run = 0 ; tcoeff.level = 2 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 4 ) == 5  ) /* 0101s             */ { flushbits(bb, 4 ); tcoeff.run = 2 ; tcoeff.level = 1 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 5 ) == 5  ) /* 00101s            */ { flushbits(bb, 5 ); tcoeff.run = 0 ; tcoeff.level = 3 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 5 ) == 7  ) /* 00111s            */ { flushbits(bb, 5 ); tcoeff.run = 3 ; tcoeff.level = 1 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 5 ) == 6  ) /* 00110s            */ { flushbits(bb, 5 ); tcoeff.run = 4 ; tcoeff.level = 1 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 6 ) == 6  ) /* 000110s           */ { flushbits(bb, 6 ); tcoeff.run = 1 ; tcoeff.level = 2 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 6 ) == 7  ) /* 000111s           */ { flushbits(bb, 6 ); tcoeff.run = 5 ; tcoeff.level = 1 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 6 ) == 5  ) /* 000101s           */ { flushbits(bb, 6 ); tcoeff.run = 6 ; tcoeff.level = 1 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 6 ) == 4  ) /* 000100s           */ { flushbits(bb, 6 ); tcoeff.run = 7 ; tcoeff.level = 1 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 7 ) == 6  ) /* 0000110s          */ { flushbits(bb, 7 ); tcoeff.run = 0 ; tcoeff.level = 4 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 7 ) == 4  ) /* 0000100s          */ { flushbits(bb, 7 ); tcoeff.run = 2 ; tcoeff.level = 2 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 7 ) == 7  ) /* 0000111s          */ { flushbits(bb, 7 ); tcoeff.run = 8 ; tcoeff.level = 1 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 7 ) == 5  ) /* 0000101s          */ { flushbits(bb, 7 ); tcoeff.run = 9 ; tcoeff.level = 1 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 38 ) /* 00100110s         */ { flushbits(bb, 8 ); tcoeff.run = 0 ; tcoeff.level = 5 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 33 ) /* 00100001s         */ { flushbits(bb, 8 ); tcoeff.run = 0 ; tcoeff.level = 6 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 37 ) /* 00100101s         */ { flushbits(bb, 8 ); tcoeff.run = 1 ; tcoeff.level = 3 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 36 ) /* 00100100s         */ { flushbits(bb, 8 ); tcoeff.run = 3 ; tcoeff.level = 2 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 39 ) /* 00100111s         */ { flushbits(bb, 8 ); tcoeff.run = 10; tcoeff.level = 1 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 35 ) /* 00100011s         */ { flushbits(bb, 8 ); tcoeff.run = 11; tcoeff.level = 1 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 34 ) /* 00100010s         */ { flushbits(bb, 8 ); tcoeff.run = 12; tcoeff.level = 1 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 32 ) /* 00100000s         */ { flushbits(bb, 8 ); tcoeff.run = 13; tcoeff.level = 1 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 10) == 10 ) /* 0000001010s       */ { flushbits(bb, 10); tcoeff.run = 0 ; tcoeff.level = 7 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 10) == 12 ) /* 0000001100s       */ { flushbits(bb, 10); tcoeff.run = 1 ; tcoeff.level = 4 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 10) == 11 ) /* 0000001011s       */ { flushbits(bb, 10); tcoeff.run = 2 ; tcoeff.level = 3 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 10) == 15 ) /* 0000001111s       */ { flushbits(bb, 10); tcoeff.run = 4 ; tcoeff.level = 2 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 10) == 9  ) /* 0000001001s       */ { flushbits(bb, 10); tcoeff.run = 5 ; tcoeff.level = 2 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 10) == 14 ) /* 0000001110s       */ { flushbits(bb, 10); tcoeff.run = 14; tcoeff.level = 1 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 10) == 13 ) /* 0000001101s       */ { flushbits(bb, 10); tcoeff.run = 15; tcoeff.level = 1 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 10) == 8  ) /* 0000001000s       */ { flushbits(bb, 10); tcoeff.run = 16; tcoeff.level = 1 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 29 ) /* 000000011101s     */ { flushbits(bb, 12); tcoeff.run = 0 ; tcoeff.level = 8 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 24 ) /* 000000011000s     */ { flushbits(bb, 12); tcoeff.run = 0 ; tcoeff.level = 9 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 19 ) /* 000000010011s     */ { flushbits(bb, 12); tcoeff.run = 0 ; tcoeff.level = 10; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 16 ) /* 000000010000s     */ { flushbits(bb, 12); tcoeff.run = 0 ; tcoeff.level = 11; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 27 ) /* 000000011011s     */ { flushbits(bb, 12); tcoeff.run = 1 ; tcoeff.level = 5 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 20 ) /* 000000010100s     */ { flushbits(bb, 12); tcoeff.run = 2 ; tcoeff.level = 4 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 28 ) /* 000000011100s     */ { flushbits(bb, 12); tcoeff.run = 3 ; tcoeff.level = 3 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 18 ) /* 000000010010s     */ { flushbits(bb, 12); tcoeff.run = 4 ; tcoeff.level = 3 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 30 ) /* 000000011110s     */ { flushbits(bb, 12); tcoeff.run = 6 ; tcoeff.level = 2 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 21 ) /* 000000010101s     */ { flushbits(bb, 12); tcoeff.run = 7 ; tcoeff.level = 2 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 17 ) /* 000000010001s     */ { flushbits(bb, 12); tcoeff.run = 8 ; tcoeff.level = 2 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 31 ) /* 000000011111s     */ { flushbits(bb, 12); tcoeff.run = 17; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 26 ) /* 000000011010s     */ { flushbits(bb, 12); tcoeff.run = 18; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 25 ) /* 000000011001s     */ { flushbits(bb, 12); tcoeff.run = 19; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 23 ) /* 000000010111s     */ { flushbits(bb, 12); tcoeff.run = 20; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 22 ) /* 000000010110s     */ { flushbits(bb, 12); tcoeff.run = 21; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 26 ) /* 0000000011010s    */ { flushbits(bb, 13); tcoeff.run = 0 ; tcoeff.level = 12; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 25 ) /* 0000000011001s    */ { flushbits(bb, 13); tcoeff.run = 0 ; tcoeff.level = 13; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 24 ) /* 0000000011000s    */ { flushbits(bb, 13); tcoeff.run = 0 ; tcoeff.level = 14; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 23 ) /* 0000000010111s    */ { flushbits(bb, 13); tcoeff.run = 0 ; tcoeff.level = 15; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 22 ) /* 0000000010110s    */ { flushbits(bb, 13); tcoeff.run = 1 ; tcoeff.level = 6 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 21 ) /* 0000000010101s    */ { flushbits(bb, 13); tcoeff.run = 1 ; tcoeff.level = 7 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 20 ) /* 0000000010100s    */ { flushbits(bb, 13); tcoeff.run = 2 ; tcoeff.level = 5 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 19 ) /* 0000000010011s    */ { flushbits(bb, 13); tcoeff.run = 3 ; tcoeff.level = 4 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 18 ) /* 0000000010010s    */ { flushbits(bb, 13); tcoeff.run = 5 ; tcoeff.level = 3 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 17 ) /* 0000000010001s    */ { flushbits(bb, 13); tcoeff.run = 9 ; tcoeff.level = 2 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 16 ) /* 0000000010000s    */ { flushbits(bb, 13); tcoeff.run = 10; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 31 ) /* 0000000011111s    */ { flushbits(bb, 13); tcoeff.run = 22; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 30 ) /* 0000000011110s    */ { flushbits(bb, 13); tcoeff.run = 23; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 29 ) /* 0000000011101s    */ { flushbits(bb, 13); tcoeff.run = 24; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 28 ) /* 0000000011100s    */ { flushbits(bb, 13); tcoeff.run = 25; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 27 ) /* 0000000011011s    */ { flushbits(bb, 13); tcoeff.run = 26; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 31 ) /* 00000000011111s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 16; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 30 ) /* 00000000011110s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 17; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 29 ) /* 00000000011101s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 18; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 28 ) /* 00000000011100s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 19; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 27 ) /* 00000000011011s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 20; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 26 ) /* 00000000011010s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 21; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 25 ) /* 00000000011001s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 22; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 24 ) /* 00000000011000s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 23; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 23 ) /* 00000000010111s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 24; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 22 ) /* 00000000010110s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 25; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 21 ) /* 00000000010101s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 26; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 20 ) /* 00000000010100s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 27; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 19 ) /* 00000000010011s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 28; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 18 ) /* 00000000010010s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 29; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 17 ) /* 00000000010001s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 30; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 16 ) /* 00000000010000s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 31; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 24 ) /* 000000000011000s  */ { flushbits(bb, 15); tcoeff.run = 0 ; tcoeff.level = 32; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 23 ) /* 000000000010111s  */ { flushbits(bb, 15); tcoeff.run = 0 ; tcoeff.level = 33; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 22 ) /* 000000000010110s  */ { flushbits(bb, 15); tcoeff.run = 0 ; tcoeff.level = 34; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 21 ) /* 000000000010101s  */ { flushbits(bb, 15); tcoeff.run = 0 ; tcoeff.level = 35; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 20 ) /* 000000000010100s  */ { flushbits(bb, 15); tcoeff.run = 0 ; tcoeff.level = 36; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 19 ) /* 000000000010011s  */ { flushbits(bb, 15); tcoeff.run = 0 ; tcoeff.level = 37; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 18 ) /* 000000000010010s  */ { flushbits(bb, 15); tcoeff.run = 0 ; tcoeff.level = 38; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 17 ) /* 000000000010001s  */ { flushbits(bb, 15); tcoeff.run = 0 ; tcoeff.level = 39; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 16 ) /* 000000000010000s  */ { flushbits(bb, 15); tcoeff.run = 0 ; tcoeff.level = 40; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 31 ) /* 000000000011111s  */ { flushbits(bb, 15); tcoeff.run = 1 ; tcoeff.level =  8; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 30 ) /* 000000000011110s  */ { flushbits(bb, 15); tcoeff.run = 1 ; tcoeff.level =  9; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 29 ) /* 000000000011101s  */ { flushbits(bb, 15); tcoeff.run = 1 ; tcoeff.level = 10; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 28 ) /* 000000000011100s  */ { flushbits(bb, 15); tcoeff.run = 1 ; tcoeff.level = 11; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 27 ) /* 000000000011011s  */ { flushbits(bb, 15); tcoeff.run = 1 ; tcoeff.level = 12; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 26 ) /* 000000000011010s  */ { flushbits(bb, 15); tcoeff.run = 1 ; tcoeff.level = 13; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 25 ) /* 000000000011001s  */ { flushbits(bb, 15); tcoeff.run = 1 ; tcoeff.level = 14; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 19 ) /* 0000000000010011s */ { flushbits(bb, 16); tcoeff.run = 1 ; tcoeff.level = 15; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 18 ) /* 0000000000010010s */ { flushbits(bb, 16); tcoeff.run = 1 ; tcoeff.level = 16; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 17 ) /* 0000000000010001s */ { flushbits(bb, 16); tcoeff.run = 1 ; tcoeff.level = 17; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 16 ) /* 0000000000010000s */ { flushbits(bb, 16); tcoeff.run = 1 ; tcoeff.level = 18; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 20 ) /* 0000000000010100s */ { flushbits(bb, 16); tcoeff.run = 6 ; tcoeff.level = 3 ; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 26 ) /* 0000000000011010s */ { flushbits(bb, 16); tcoeff.run = 11; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 25 ) /* 0000000000011001s */ { flushbits(bb, 16); tcoeff.run = 12; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 24 ) /* 0000000000011000s */ { flushbits(bb, 16); tcoeff.run = 13; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 23 ) /* 0000000000010111s */ { flushbits(bb, 16); tcoeff.run = 14; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 22 ) /* 0000000000010110s */ { flushbits(bb, 16); tcoeff.run = 15; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 21 ) /* 0000000000010101s */ { flushbits(bb, 16); tcoeff.run = 16; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 31 ) /* 0000000000011111s */ { flushbits(bb, 16); tcoeff.run = 27; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 30 ) /* 0000000000011110s */ { flushbits(bb, 16); tcoeff.run = 28; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 29 ) /* 0000000000011101s */ { flushbits(bb, 16); tcoeff.run = 29; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 28 ) /* 0000000000011100s */ { flushbits(bb, 16); tcoeff.run = 30; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 27 ) /* 0000000000011011s */ { flushbits(bb, 16); tcoeff.run = 31; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 6 ) == 1  ) /* 000001            */ { /* Escape */
        flushbits(bb, 6 );
        tcoeff.run = getbits(bb, 6);
        if (mpeg1) {
            tcoeff.level = getbits8(bb);
            if (tcoeff.level>127)
                tcoeff.level -= 256;
            if (tcoeff.level==0)
                tcoeff.level += getbits8(bb);
            if (tcoeff.level==-128)
                tcoeff.level += getbits8(bb) - 128;
        } else {
            tcoeff.level = getbits(bb, 12);
            if (tcoeff.level>2047)
                tcoeff.level -= 4096;
        }
    }
    return tcoeff;
}

static struct rvtcoeff parse_m2v_coefficient_one(struct bitbuf *bb)
{
    struct rvtcoeff tcoeff;
    tcoeff.run   = -1;
    tcoeff.level = 0;
    tcoeff.sign  = 0;

    if (showbits(bb, 4 ) == 6  ) /* 0110              */ { flushbits(bb, 4 ); tcoeff.run = 0 ; tcoeff.level =  0; return tcoeff; } /* End of Block */
    if (showbits(bb, 2 ) == 2  ) /* 10s               */ { flushbits(bb, 2 ); tcoeff.run = 0 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 3 ) == 2  ) /* 010s              */ { flushbits(bb, 3 ); tcoeff.run = 1 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 3 ) == 6  ) /* 110s              */ { flushbits(bb, 3 ); tcoeff.run = 0 ; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 5 ) == 5  ) /* 00101s            */ { flushbits(bb, 5 ); tcoeff.run = 2 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 4 ) == 7  ) /* 0111s             */ { flushbits(bb, 4 ); tcoeff.run = 0 ; tcoeff.level =  3; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 5 ) == 7  ) /* 00111s            */ { flushbits(bb, 5 ); tcoeff.run = 3 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 6 ) == 6  ) /* 000110s           */ { flushbits(bb, 6 ); tcoeff.run = 4 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 5 ) == 6  ) /* 00110s            */ { flushbits(bb, 5 ); tcoeff.run = 1 ; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 6 ) == 7  ) /* 000111s           */ { flushbits(bb, 6 ); tcoeff.run = 5 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 7 ) == 6  ) /* 0000110s          */ { flushbits(bb, 7 ); tcoeff.run = 6 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 7 ) == 4  ) /* 0000100s          */ { flushbits(bb, 7 ); tcoeff.run = 7 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 5 ) == 28 ) /* 11100s            */ { flushbits(bb, 5 ); tcoeff.run = 0 ; tcoeff.level =  4; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 7 ) == 7  ) /* 0000111s          */ { flushbits(bb, 7 ); tcoeff.run = 2 ; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 7 ) == 5  ) /* 0000101s          */ { flushbits(bb, 7 ); tcoeff.run = 8 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 7 ) == 120) /* 1111000s          */ { flushbits(bb, 7 ); tcoeff.run = 9 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 5 ) == 29 ) /* 11101s            */ { flushbits(bb, 5 ); tcoeff.run = 0 ; tcoeff.level =  5; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 6 ) == 5  ) /* 000101s           */ { flushbits(bb, 6 ); tcoeff.run = 0 ; tcoeff.level =  6; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 7 ) == 121) /* 1111001s          */ { flushbits(bb, 7 ); tcoeff.run = 1 ; tcoeff.level =  3; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 38 ) /* 00100110s         */ { flushbits(bb, 8 ); tcoeff.run = 3 ; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 7 ) == 122) /* 1111010s          */ { flushbits(bb, 7 ); tcoeff.run = 10; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 33 ) /* 00100001s         */ { flushbits(bb, 8 ); tcoeff.run = 11; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 37 ) /* 00100101s         */ { flushbits(bb, 8 ); tcoeff.run = 12; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 36 ) /* 00100100s         */ { flushbits(bb, 8 ); tcoeff.run = 13; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 6 ) == 4  ) /* 000100s           */ { flushbits(bb, 6 ); tcoeff.run = 0 ; tcoeff.level =  7; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 39 ) /* 00100111s         */ { flushbits(bb, 8 ); tcoeff.run = 1 ; tcoeff.level =  4; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 252) /* 11111100s         */ { flushbits(bb, 8 ); tcoeff.run = 2 ; tcoeff.level =  3; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 253) /* 11111101s         */ { flushbits(bb, 8 ); tcoeff.run = 4 ; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 9 ) == 4  ) /* 000000100s        */ { flushbits(bb, 9 ); tcoeff.run = 5 ; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 9 ) == 5  ) /* 000000101s        */ { flushbits(bb, 9 ); tcoeff.run = 14; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 9 ) == 7  ) /* 000000111s        */ { flushbits(bb, 9 ); tcoeff.run = 15; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 10) == 13 ) /* 0000001101s       */ { flushbits(bb, 10); tcoeff.run = 16; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 7 ) == 123) /* 1111011s          */ { flushbits(bb, 7 ); tcoeff.run = 0 ; tcoeff.level =  8; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 7 ) == 124) /* 1111100s          */ { flushbits(bb, 7 ); tcoeff.run = 0 ; tcoeff.level =  9; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 35 ) /* 00100011s         */ { flushbits(bb, 8 ); tcoeff.run = 0 ; tcoeff.level = 10; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 34 ) /* 00100010s         */ { flushbits(bb, 8 ); tcoeff.run = 0 ; tcoeff.level = 11; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 32 ) /* 00100000s         */ { flushbits(bb, 8 ); tcoeff.run = 1 ; tcoeff.level =  5; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 10) == 12 ) /* 0000001100 s      */ { flushbits(bb, 10); tcoeff.run = 2 ; tcoeff.level =  4; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 28 ) /* 000000011100 s    */ { flushbits(bb, 12); tcoeff.run = 3 ; tcoeff.level =  3; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 18 ) /* 000000010010 s    */ { flushbits(bb, 12); tcoeff.run = 4 ; tcoeff.level =  3; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 30 ) /* 000000011110 s    */ { flushbits(bb, 12); tcoeff.run = 6 ; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 21 ) /* 000000010101 s    */ { flushbits(bb, 12); tcoeff.run = 7 ; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 17 ) /* 000000010001 s    */ { flushbits(bb, 12); tcoeff.run = 8 ; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 31 ) /* 000000011111 s    */ { flushbits(bb, 12); tcoeff.run = 17; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 26 ) /* 000000011010 s    */ { flushbits(bb, 12); tcoeff.run = 18; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 25 ) /* 000000011001 s    */ { flushbits(bb, 12); tcoeff.run = 19; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 23 ) /* 000000010111 s    */ { flushbits(bb, 12); tcoeff.run = 20; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 22 ) /* 000000010110 s    */ { flushbits(bb, 12); tcoeff.run = 21; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 250) /* 11111010s         */ { flushbits(bb, 8 ); tcoeff.run = 0 ; tcoeff.level = 12; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 251) /* 11111011s         */ { flushbits(bb, 8 ); tcoeff.run = 0 ; tcoeff.level = 13; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 254) /* 11111110s         */ { flushbits(bb, 8 ); tcoeff.run = 0 ; tcoeff.level = 14; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 255) /* 11111111s         */ { flushbits(bb, 8 ); tcoeff.run = 0 ; tcoeff.level = 15; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 22 ) /* 0000000010110s    */ { flushbits(bb, 13); tcoeff.run = 1 ; tcoeff.level =  6; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 21 ) /* 0000000010101s    */ { flushbits(bb, 13); tcoeff.run = 1 ; tcoeff.level =  7; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 20 ) /* 0000000010100s    */ { flushbits(bb, 13); tcoeff.run = 2 ; tcoeff.level =  5; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 19 ) /* 0000000010011s    */ { flushbits(bb, 13); tcoeff.run = 3 ; tcoeff.level =  4; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 18 ) /* 0000000010010s    */ { flushbits(bb, 13); tcoeff.run = 5 ; tcoeff.level =  3; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 17 ) /* 0000000010001s    */ { flushbits(bb, 13); tcoeff.run = 9 ; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 16 ) /* 0000000010000s    */ { flushbits(bb, 13); tcoeff.run = 10; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 31 ) /* 0000000011111s    */ { flushbits(bb, 13); tcoeff.run = 22; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 30 ) /* 0000000011110s    */ { flushbits(bb, 13); tcoeff.run = 23; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 29 ) /* 0000000011101s    */ { flushbits(bb, 13); tcoeff.run = 24; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 28 ) /* 0000000011100s    */ { flushbits(bb, 13); tcoeff.run = 25; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 27 ) /* 0000000011011s    */ { flushbits(bb, 13); tcoeff.run = 26; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 31 ) /* 00000000011111s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 16; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 30 ) /* 00000000011110s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 17; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 29 ) /* 00000000011101s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 18; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 28 ) /* 00000000011100s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 19; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 27 ) /* 00000000011011s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 20; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 26 ) /* 00000000011010s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 21; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 25 ) /* 00000000011001s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 22; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 24 ) /* 00000000011000s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 23; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 23 ) /* 00000000010111s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 24; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 22 ) /* 00000000010110s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 25; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 21 ) /* 00000000010101s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 26; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 20 ) /* 00000000010100s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 27; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 19 ) /* 00000000010011s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 28; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 18 ) /* 00000000010010s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 29; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 17 ) /* 00000000010001s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 30; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 14) == 16 ) /* 00000000010000s   */ { flushbits(bb, 14); tcoeff.run = 0 ; tcoeff.level = 31; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 24 ) /* 000000000011000s  */ { flushbits(bb, 15); tcoeff.run = 0 ; tcoeff.level = 32; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 23 ) /* 000000000010111s  */ { flushbits(bb, 15); tcoeff.run = 0 ; tcoeff.level = 33; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 22 ) /* 000000000010110s  */ { flushbits(bb, 15); tcoeff.run = 0 ; tcoeff.level = 34; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 21 ) /* 000000000010101s  */ { flushbits(bb, 15); tcoeff.run = 0 ; tcoeff.level = 35; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 20 ) /* 000000000010100s  */ { flushbits(bb, 15); tcoeff.run = 0 ; tcoeff.level = 36; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 19 ) /* 000000000010011s  */ { flushbits(bb, 15); tcoeff.run = 0 ; tcoeff.level = 37; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 18 ) /* 000000000010010s  */ { flushbits(bb, 15); tcoeff.run = 0 ; tcoeff.level = 38; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 17 ) /* 000000000010001s  */ { flushbits(bb, 15); tcoeff.run = 0 ; tcoeff.level = 39; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 16 ) /* 000000000010000s  */ { flushbits(bb, 15); tcoeff.run = 0 ; tcoeff.level = 40; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 31 ) /* 000000000011111s  */ { flushbits(bb, 15); tcoeff.run = 1 ; tcoeff.level =  8; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 30 ) /* 000000000011110s  */ { flushbits(bb, 15); tcoeff.run = 1 ; tcoeff.level =  9; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 29 ) /* 000000000011101s  */ { flushbits(bb, 15); tcoeff.run = 1 ; tcoeff.level = 10; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 28 ) /* 000000000011100s  */ { flushbits(bb, 15); tcoeff.run = 1 ; tcoeff.level = 11; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 27 ) /* 000000000011011s  */ { flushbits(bb, 15); tcoeff.run = 1 ; tcoeff.level = 12; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 26 ) /* 000000000011010s  */ { flushbits(bb, 15); tcoeff.run = 1 ; tcoeff.level = 13; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 15) == 25 ) /* 000000000011001s  */ { flushbits(bb, 15); tcoeff.run = 1 ; tcoeff.level = 14; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 19 ) /* 0000000000010011s */ { flushbits(bb, 16); tcoeff.run =  1; tcoeff.level = 15; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 18 ) /* 0000000000010010s */ { flushbits(bb, 16); tcoeff.run =  1; tcoeff.level = 16; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 17 ) /* 0000000000010001s */ { flushbits(bb, 16); tcoeff.run =  1; tcoeff.level = 17; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 16 ) /* 0000000000010000s */ { flushbits(bb, 16); tcoeff.run =  1; tcoeff.level = 18; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 20 ) /* 0000000000010100s */ { flushbits(bb, 16); tcoeff.run =  6; tcoeff.level =  3; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 26 ) /* 0000000000011010s */ { flushbits(bb, 16); tcoeff.run = 11; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 25 ) /* 0000000000011001s */ { flushbits(bb, 16); tcoeff.run = 12; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 24 ) /* 0000000000011000s */ { flushbits(bb, 16); tcoeff.run = 13; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 23 ) /* 0000000000010111s */ { flushbits(bb, 16); tcoeff.run = 14; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 22 ) /* 0000000000010110s */ { flushbits(bb, 16); tcoeff.run = 15; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 21 ) /* 0000000000010101s */ { flushbits(bb, 16); tcoeff.run = 16; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 31 ) /* 0000000000011111s */ { flushbits(bb, 16); tcoeff.run = 27; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 30 ) /* 0000000000011110s */ { flushbits(bb, 16); tcoeff.run = 28; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 29 ) /* 0000000000011101s */ { flushbits(bb, 16); tcoeff.run = 29; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 28 ) /* 0000000000011100s */ { flushbits(bb, 16); tcoeff.run = 30; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits16(bb)   == 27 ) /* 0000000000011011s */ { flushbits(bb, 16); tcoeff.run = 31; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 6 ) == 1  ) /* 000001            */ { /* Escape */
        flushbits(bb, 6 );
        tcoeff.run = getbits(bb, 6);
        tcoeff.level = getbits(bb, 12);
        if (tcoeff.level>2047)
            tcoeff.level -= 4096;
    }
    return tcoeff;
}

int parse_m2v_motion_vector(struct bitbuf *bb, const struct rvm2vpicture *picture, struct rvm2vmacro *macro, int r, int s)
{
    int motion_code, motion_residual = 0;

    motion_code = parse_m2v_motion_code(bb);
    if (motion_code<-16)
        return -1;
    if (picture->extension.f_code[s][0]!=1 && motion_code!=0)
        motion_residual = getbits(bb, picture->extension.f_code[s][0]-1);
    if (dmv(macro->frame_motion_type, macro->field_motion_type))
        macro->dmvector.x = parse_m2v_dmvector(bb);
    if (macro->dmvector.x<-1)
        return -1;

    macro->delta[r][s].x = decode_m2v_motion_vector(picture->extension.f_code[s][0], motion_code, motion_residual);

    motion_code = parse_m2v_motion_code(bb);
    if (motion_code<-16)
        return -1;
    if (picture->extension.f_code[s][1]!=1 && motion_code!=0)
        motion_residual = getbits(bb, picture->extension.f_code[s][1]-1);
    if (dmv(macro->frame_motion_type, macro->field_motion_type))
        macro->dmvector.y = parse_m2v_dmvector(bb);
    if (macro->dmvector.y<-1)
        return -1;

    macro->delta[r][s].y = decode_m2v_motion_vector(picture->extension.f_code[s][1], motion_code, motion_residual);

    return 0;
}

int parse_m2v_motion_vectors(struct bitbuf *bb, const struct rvm2vpicture *picture, struct rvm2vmacro *macro, int s)
{
    int frame_motion_type = macro->frame_motion_type;
    int field_motion_type = macro->field_motion_type;

    if (motion_vector_count(frame_motion_type, field_motion_type)==1) {
        if (mv_format(frame_motion_type, field_motion_type)==MV_FORMAT_FIELD && dmv(frame_motion_type, field_motion_type)!=1)
            macro->motion_vertical_field_select[0][s] = getbit(bb);
        if (parse_m2v_motion_vector(bb, picture, macro, 0, s)<0)
            return -1;
    } else {
        macro->motion_vertical_field_select[0][s] = getbit(bb);
        if (parse_m2v_motion_vector(bb, picture, macro, 0, s)<0)
            return -1;
        macro->motion_vertical_field_select[1][s] = getbit(bb);
        if (parse_m2v_motion_vector(bb, picture, macro, 1, s)<0)
            return -1;
    }
    return 0;
}

int parse_m2v_coded_block_pattern(struct bitbuf *bb)
{
   if (showbits(bb, 3) == 7  ) /* 111       */ { flushbits(bb, 3); return 60; }
   if (showbits(bb, 4) == 13 ) /* 1101      */ { flushbits(bb, 4); return 4 ; }
   if (showbits(bb, 4) == 12 ) /* 1100      */ { flushbits(bb, 4); return 8 ; }
   if (showbits(bb, 4) == 11 ) /* 1011      */ { flushbits(bb, 4); return 16; }
   if (showbits(bb, 4) == 10 ) /* 1010      */ { flushbits(bb, 4); return 32; }
   if (showbits(bb, 5) == 19 ) /* 10011     */ { flushbits(bb, 5); return 12; }
   if (showbits(bb, 5) == 18 ) /* 10010     */ { flushbits(bb, 5); return 48; }
   if (showbits(bb, 5) == 17 ) /* 10001     */ { flushbits(bb, 5); return 20; }
   if (showbits(bb, 5) == 16 ) /* 10000     */ { flushbits(bb, 5); return 40; }
   if (showbits(bb, 5) == 15 ) /* 01111     */ { flushbits(bb, 5); return 28; }
   if (showbits(bb, 5) == 14 ) /* 01110     */ { flushbits(bb, 5); return 44; }
   if (showbits(bb, 5) == 13 ) /* 01101     */ { flushbits(bb, 5); return 52; }
   if (showbits(bb, 5) == 12 ) /* 01100     */ { flushbits(bb, 5); return 56; }
   if (showbits(bb, 5) == 11 ) /* 01011     */ { flushbits(bb, 5); return 1 ; }
   if (showbits(bb, 5) == 10 ) /* 01010     */ { flushbits(bb, 5); return 61; }
   if (showbits(bb, 5) == 9  ) /* 01001     */ { flushbits(bb, 5); return 2 ; }
   if (showbits(bb, 5) == 8  ) /* 01000     */ { flushbits(bb, 5); return 62; }
   if (showbits(bb, 6) == 15 ) /* 001111    */ { flushbits(bb, 6); return 24; }
   if (showbits(bb, 6) == 14 ) /* 001110    */ { flushbits(bb, 6); return 36; }
   if (showbits(bb, 6) == 13 ) /* 001101    */ { flushbits(bb, 6); return 3 ; }
   if (showbits(bb, 6) == 12 ) /* 001100    */ { flushbits(bb, 6); return 63; }
   if (showbits(bb, 7) == 23 ) /* 0010111   */ { flushbits(bb, 7); return 5 ; }
   if (showbits(bb, 7) == 22 ) /* 0010110   */ { flushbits(bb, 7); return 9 ; }
   if (showbits(bb, 7) == 21 ) /* 0010101   */ { flushbits(bb, 7); return 17; }
   if (showbits(bb, 7) == 20 ) /* 0010100   */ { flushbits(bb, 7); return 33; }
   if (showbits(bb, 7) == 19 ) /* 0010011   */ { flushbits(bb, 7); return 6 ; }
   if (showbits(bb, 7) == 18 ) /* 0010010   */ { flushbits(bb, 7); return 10; }
   if (showbits(bb, 7) == 17 ) /* 0010001   */ { flushbits(bb, 7); return 18; }
   if (showbits(bb, 7) == 16 ) /* 0010000   */ { flushbits(bb, 7); return 34; }
   if (showbits(bb, 8) == 31 ) /* 00011111  */ { flushbits(bb, 8); return 7 ; }
   if (showbits(bb, 8) == 30 ) /* 00011110  */ { flushbits(bb, 8); return 11; }
   if (showbits(bb, 8) == 29 ) /* 00011101  */ { flushbits(bb, 8); return 19; }
   if (showbits(bb, 8) == 28 ) /* 00011100  */ { flushbits(bb, 8); return 35; }
   if (showbits(bb, 8) == 27 ) /* 00011011  */ { flushbits(bb, 8); return 13; }
   if (showbits(bb, 8) == 26 ) /* 00011010  */ { flushbits(bb, 8); return 49; }
   if (showbits(bb, 8) == 25 ) /* 00011001  */ { flushbits(bb, 8); return 21; }
   if (showbits(bb, 8) == 24 ) /* 00011000  */ { flushbits(bb, 8); return 41; }
   if (showbits(bb, 8) == 23 ) /* 00010111  */ { flushbits(bb, 8); return 14; }
   if (showbits(bb, 8) == 22 ) /* 00010110  */ { flushbits(bb, 8); return 50; }
   if (showbits(bb, 8) == 21 ) /* 00010101  */ { flushbits(bb, 8); return 22; }
   if (showbits(bb, 8) == 20 ) /* 00010100  */ { flushbits(bb, 8); return 42; }
   if (showbits(bb, 8) == 19 ) /* 00010011  */ { flushbits(bb, 8); return 15; }
   if (showbits(bb, 8) == 18 ) /* 00010010  */ { flushbits(bb, 8); return 51; }
   if (showbits(bb, 8) == 17 ) /* 00010001  */ { flushbits(bb, 8); return 23; }
   if (showbits(bb, 8) == 16 ) /* 00010000  */ { flushbits(bb, 8); return 43; }
   if (showbits(bb, 8) == 15 ) /* 00001111  */ { flushbits(bb, 8); return 25; }
   if (showbits(bb, 8) == 14 ) /* 00001110  */ { flushbits(bb, 8); return 37; }
   if (showbits(bb, 8) == 13 ) /* 00001101  */ { flushbits(bb, 8); return 26; }
   if (showbits(bb, 8) == 12 ) /* 00001100  */ { flushbits(bb, 8); return 38; }
   if (showbits(bb, 8) == 11 ) /* 00001011  */ { flushbits(bb, 8); return 29; }
   if (showbits(bb, 8) == 10 ) /* 00001010  */ { flushbits(bb, 8); return 45; }
   if (showbits(bb, 8) == 9  ) /* 00001001  */ { flushbits(bb, 8); return 53; }
   if (showbits(bb, 8) == 8  ) /* 00001000  */ { flushbits(bb, 8); return 57; }
   if (showbits(bb, 8) == 7  ) /* 00000111  */ { flushbits(bb, 8); return 30; }
   if (showbits(bb, 8) == 6  ) /* 00000110  */ { flushbits(bb, 8); return 46; }
   if (showbits(bb, 8) == 5  ) /* 00000101  */ { flushbits(bb, 8); return 54; }
   if (showbits(bb, 8) == 4  ) /* 00000100  */ { flushbits(bb, 8); return 58; }
   if (showbits(bb, 9) == 7  ) /* 000000111 */ { flushbits(bb, 9); return 31; }
   if (showbits(bb, 9) == 6  ) /* 000000110 */ { flushbits(bb, 9); return 47; }
   if (showbits(bb, 9) == 5  ) /* 000000101 */ { flushbits(bb, 9); return 55; }
   if (showbits(bb, 9) == 4  ) /* 000000100 */ { flushbits(bb, 9); return 59; }
   if (showbits(bb, 9) == 3  ) /* 000000011 */ { flushbits(bb, 9); return 27; }
   if (showbits(bb, 9) == 2  ) /* 000000010 */ { flushbits(bb, 9); return 39; }
   if (showbits(bb, 9) == 1  ) /* 000000001 */ { flushbits(bb, 9); return 0;  }  /* not used with 4:2:0 chroma */
   return -1;
}

int parse_m2v_macroblock(struct bitbuf *bb, const struct rvm2vsequence *sequence, const struct rvm2vpicture *picture, struct rvm2vmacro *macro, int picno, int mb_addr)
{
    macro->addr = mb_addr;
    /* parse macroblock type */
    macro->type = parse_m2v_macroblock_type(bb, picture->picture_coding_type);
    if (macro->type<0) {
        rvmessage("picture %d, tempref %d, macroblock %d: failed to parse macroblock_type", picno, picture->temporal_reference, mb_addr);
        return -1;
    }

    /* TODO spatial_temporal_weight_code */

    /* parse macroblock motion type */
    if (macroblock_motion_forward(macro->type) || macroblock_motion_backward(macro->type)) {
        if (picture->extension.picture_structure==PST_FRAME) {
            if (picture->extension.frame_pred_frame_dct==0)
                macro->frame_motion_type = getbits(bb, 2);
            else
                macro->frame_motion_type = 2;
        } else
            macro->field_motion_type = getbits(bb, 2);
    } else if (picture->extension.picture_structure==PST_FRAME)
        macro->frame_motion_type = 2;
    else
        macro->field_motion_type = 1;
    /* this is handled above
    if (macroblock_intra(macro->type) && picture->extension.concealment_motion_vectors) {
        if (picture->extension.picture_structure==PST_FRAME)
            macro->frame_motion_type = 2;
        else
            macro->field_motion_type = 1;
    }*/
    if (!macroblock_intra(macro->type) && !macroblock_motion_forward(macro->type) && picture->picture_coding_type==PCT_P) {
        if (picture->extension.picture_structure==PST_FRAME)
            macro->frame_motion_type = 2;
        else {
            macro->field_motion_type = 1;
            macro->motion_vertical_field_select[0][0] = picture->extension.picture_structure==PST_TOP? 0 : 1;
        }
    }
    if (picture->extension.picture_structure==PST_FRAME && picture->extension.frame_pred_frame_dct==0 && (macroblock_intra(macro->type) || macroblock_pattern(macro->type)))
        macro->dct_type = getbit(bb);

    /* parse macroblock quant */
    if (macroblock_quant(macro->type))
        macro->quantiser_scale_code = getbits(bb, 5);

    /* parse macroblock motion vectors */
    if (macroblock_motion_forward(macro->type) || (macroblock_intra(macro->type) && picture->extension.concealment_motion_vectors)) {
        if (parse_m2v_motion_vectors(bb, picture, macro, 0)<0) {
            rvmessage("picture %d, tempref %d, macroblock %d: failed to parse motion_vectors x", picno, picture->temporal_reference, mb_addr);
            mark_error_macroblock(macro);
            return -1;
        }
    }
    if (macroblock_motion_backward(macro->type)) {
        if (parse_m2v_motion_vectors(bb, picture, macro, 1)<0) {
            rvmessage("picture %d, tempref %d, macroblock %d: failed to parse motion_vectors y", picno, picture->temporal_reference, mb_addr);
            mark_error_macroblock(macro);
            return -1;
        }
    }
    if (macroblock_intra(macro->type) && picture->extension.concealment_motion_vectors)
        flushbits(bb, 1);

    /* parse macroblock pattern */
    if (macroblock_intra(macro->type))
        macro->pattern_code = 0xFF;
    else
        macro->pattern_code = 0;
    if (macroblock_pattern(macro->type)) {
        int i, cbp = parse_m2v_coded_block_pattern(bb);
        if (sequence->chroma_format==1 && cbp==0)
            rvmessage("picture %d, tempref %d, macroblock %d: coded_block_pattern of zero is not permitted when chroma_format is 4:2:0", picno, picture->temporal_reference, mb_addr);
        // FIXME remove these loops for efficiency.
        for (i=0; i<6; i++)
            if (cbp & (1<<(5-i)))
                macro->pattern_code |= 1<<i;
        if (sequence->chroma_format==2) {
            cbp = getbits(bb, 2);
            for (i=6; i<8; i++)
                if (cbp & (1<<(7-i)))
                    macro->pattern_code |= 1<<i;
        }
        if (sequence->chroma_format==3) {
            cbp = getbits(bb, 6);
            for (i=6; i<12; i++)
                if (cbp & (1<<(11-i)))
                    macro->pattern_code |= 1<<i;
        }
    }
    if (macro->pattern_code<0) {
        rvmessage("picture %d, tempref %d, macroblock %d: failed to parse coded_block_pattern", picno, picture->temporal_reference, mb_addr);
        mark_error_macroblock(macro);
        return -1;
    }

    /* parse block */
    int block;
    for (block=0; block<block_count[sequence->chroma_format]; block++) {
        if ((1<<block) & macro->pattern_code) {
            struct rvtcoeff tcoeff;
            int coeff=0;

            /* parse intra dc tcoeff */
            if (macroblock_intra(macro->type)) {
                int dct_dc_size, dct_diff;
                if (block<4) {
                    /* luma intra dc */
                    dct_dc_size = parse_m2v_dct_dc_size_luminance(bb);
                    if (dct_dc_size<0) {
                        rvmessage("picture %d, tempref %d, macroblock %d, block %d: failed to parse dct_dc_size_luminance", picno, picture->temporal_reference, mb_addr, block);
                        mark_error_macroblock(macro);
                        return -1;
                    }
                } else {
                    /* chroma intra dc */
                    dct_dc_size = parse_m2v_dct_dc_size_chrominance(bb);
                    if (dct_dc_size<0) {
                        rvmessage("picture %d, tempref %d, macroblock %d, block %d: failed to parse dct_dc_size_luminance", picno, picture->temporal_reference, mb_addr, block);
                        mark_error_macroblock(macro);
                        return -1;
                    }
                }
                if (dct_dc_size==0)
                    dct_diff = 0;
                else {
                    dct_diff = getbits(bb, dct_dc_size);
                    int half_range = 1<<(dct_dc_size-1);
                    if (dct_diff<half_range)
                        dct_diff = (dct_diff+1) - (2*half_range);
                }
                macro->coeff[block][coeff++] = dct_diff;
            }

            /* parse remaining tcoeffs */
            while (picture->picture_coding_type!=PCT_D) {
                /* read next tcoeff */
                if (macroblock_intra(macro->type) && picture->extension.intra_vlc_format)
                    tcoeff = parse_m2v_coefficient_one(bb);
                else
                    tcoeff = parse_m2v_coefficient_zero(bb, !coeff, sequence->mpeg1);
                if (tcoeff.run<0) {
                    rvmessage("picture %d, tempref %d, macroblock %d, block %d: failed to parse coefficient %d", picno, picture->temporal_reference, mb_addr, block, coeff);
                    mark_error_macroblock(macro);
                    return -1;
                }

                /* check for syntax errors */
                if (tcoeff.run==0 && tcoeff.level==0 && coeff==0)
                    rvmessage("picture %d, tempref %d, macroblock %d, block %d: EOB is only coefficient code in block", picno, picture->temporal_reference, mb_addr, block);

                /* exit loop if end of block */
                if (tcoeff.run==0 && tcoeff.level==0)
                    break;

                /* process coefficient */
                coeff += tcoeff.run;
                if (coeff>=64) {
                    rvmessage("picture %d, tempref %d, macroblock %d, block %d: too many coefficients: %d", picno, picture->temporal_reference, mb_addr, block, coeff);
                    mark_error_macroblock(macro);
                    return -1;
                }
                macro->coeff[block][coeff++] = tcoeff.sign ? -tcoeff.level : tcoeff.level;
            }
        }
    }

    /* parse end of macroblock */
    if (picture->picture_coding_type==PCT_D)
        if (getbit(bb)!=1)
            rvmessage("picture %d, tempref %d, macroblock %d: D-picture: end_of_macroblock is not '1'", picno, picture->temporal_reference, mb_addr);
    return 0;
}

int find_m2v_picture_end(struct bitbuf *bb, struct rvm2vsequence *sequence, int picno)
{
    do {
        /* find next start code */
        int start = find_m2v_next_start_code(bb);

        /* picture start code */
        if (start==0x100)
            return 0;

        /* picture  extension headers */
        if (start==0x1B5) {
            flushbits(bb, 8);
            continue;
        }

        /* picture user data */
        if (start==0x1B2) {
            flushbits(bb, 8);
            continue;
        }

        /* skip out of sequence slice */
        if (start>=0x100 && start<=0x17F) { /* TODO check this range */
            //rvmessage("%s: picture %d: skipping out sequence slice number: %08x", bb->filename, picno, start);
            flushbits(bb, 8);
            continue;
        }

        /* repeated sequence header */
        if (start==0x1B3)
            return 0;

        /* parse group of pictures header */
        if (start==0x1B8)
            return 0;

        /* check for end of sequence */
        if (eofbits(bb))
            return -1;
        if (start==0x1B7)
            /* sequence_end_code */
            return -1;

        /* unknown start code */
        rvmessage("%s: picture %d: unknown start code: %08x", bb->filename, picno, start);
        flushbits(bb, 8);

    } while (1);
}

int find_m2v_next_picture(struct bitbuf *bb, struct rvm2vsequence *sequence, int picno, int *gop_flags, int verbose)
{
    if (gop_flags)
        *gop_flags = 0;

    do {
        /* find next start code */
        int start = find_m2v_next_start_code(bb);

        /* picture start code */
        if (start==0x100)
            return 0;

        /* skip out of sequence slice */
        if (start>=0x100 && start<=0x17F) { /* TODO check this range */
            rvmessage("%s: picture %d: skipping out of sequence slice number: %08x", bb->filename, picno, start);
            flushbits(bb, 8);
            continue;
        }

        /* parse repeated sequence header */
        if (start==0x1B3) {
            flushbits(bb, 32);          /* sequence_header_code */
            if (parse_m2v_sequence(bb, sequence)<0)
                rvmessage("%s: picture %d: failed to parse repeated sequence header", bb->filename, picno);
            continue;
        }

        /* parse repeated extension headers */
        if (start==0x1B5) {
            flushbits(bb, 32);          /* extension_start_code */
            int id = getbits(bb, 4);    /* extension_start_code_identifier */
            switch (id) {
                case 0x1: /* sequence extension */
                    parse_m2v_sequence_extension(bb, sequence);
                    break;

                case 0x2: /* sequence display extension */
                    parse_m2v_sequence_display_extension(bb, sequence);
                    break;

                case 0x4: /* copyright extension */
                    parse_m2v_copyright_extension(bb);
                    break;

                default:
                    rvmessage("%s: invalid extension header found after sequence header: 0x%1x", bb->filename, id);
                    break;
            }
            continue;
        }

        /* parse repeated user data */
        if (start==0x1B2) {
            flushbits(bb, 32);                      /* user_data_start_code */
            parse_m2v_user_data(bb, verbose);
            continue;
        }

        /* parse group of pictures header */
        if (start==0x1B8) {
            flushbits(bb, 32);                      /* group_start_code */
            parse_m2v_group_of_pictures(bb, gop_flags);
            start = find_m2v_next_start_code(bb);
            while (start==0x1B2) {
                flushbits(bb, 32);                  /* user_data_start_code */
                parse_m2v_user_data(bb, verbose);
                start = find_m2v_next_start_code(bb);
            }
            continue;
        }

        /* check for end of sequence */
        if (eofbits(bb))
            return -1;
        if (start==0x1B7)
            /* sequence_end_code */
            return -1;

        /* custom start codes */
        if (start==0x1B0) {
            flushbits(bb, 32);
            int asi_timestamp = getbits8(bb);
            asi_timestamp |= getbits8(bb) << 8;
            flushbits(bb, 8);
            asi_timestamp |= getbits8(bb) << 16;
            asi_timestamp |= getbits8(bb) << 24;
            flushbits(bb, 8);
            int cap_timestamp = getbits8(bb);
            cap_timestamp |= getbits8(bb) << 8;
            flushbits(bb, 8);
            cap_timestamp |= getbits8(bb) << 16;
            cap_timestamp |= getbits8(bb) << 24;
            flushbits(bb, 8);
            int temporal_reference = getbits8(bb);
            temporal_reference |= getbits8(bb) << 8;
            flushbits(bb, 8);
            temporal_reference |= getbits8(bb) << 16;
            temporal_reference |= getbits8(bb) << 24;
            flushbits(bb, 8);
            rvmessage("special timestamp start code: asi_timestamp=%d cap_timestamp=%d temporal_reference=%d", asi_timestamp, cap_timestamp, temporal_reference);
            if (showbits32(bb)!=0x1B1)
                rvmessage("timestamp start code is not followed by flush data: 0x%08x", showbits32(bb));
            continue;
        }

        /* unknown start code */
        rvmessage("%s: picture %d: unknown start code: %08x", bb->filename, picno, start);
        flushbits(bb, 8);

    } while (1);
}

struct rvm2vpicture *parse_m2v_picture_header_and_extensions(struct bitbuf *bb, struct rvm2vsequence *sequence, int picno, int verbose)
{
    /* parse picture header and picture coding extension */
    flushbits(bb, 32);                          /* picture_start_code */
    struct rvm2vpicture *picture = parse_m2v_picture_header(bb);
    if (picture==NULL) {
        rvmessage("picture %d: failed to parse picture header", picno);
        return NULL;
    }

    /* convenient pointer to picture header extension */
    struct rvm2vextension *extension = &picture->extension;

    /* store decode order */
    picture->decode_order = picno;

    /* store sequence quant matricies */
    picture->quantmatrix.load_intra_quantiser_matrix = sequence->load_intra_quantiser_matrix;
    if (picture->quantmatrix.load_intra_quantiser_matrix)
        memcpy(picture->quantmatrix.intra_quantiser_matrix, sequence->intra_quantiser_matrix, 64*sizeof(picture->quantmatrix.intra_quantiser_matrix[0]));
    picture->quantmatrix.load_non_intra_quantiser_matrix = sequence->load_non_intra_quantiser_matrix;
    if (picture->quantmatrix.load_non_intra_quantiser_matrix)
        memcpy(picture->quantmatrix.non_intra_quantiser_matrix, sequence->non_intra_quantiser_matrix, 64*sizeof(picture->quantmatrix.non_intra_quantiser_matrix[0]));

    /* set sequence load matrix flags to zero, this won't affect decoding, but
     * will allow distinction between reused or new quant matricies */
    sequence->load_intra_quantiser_matrix = sequence->load_non_intra_quantiser_matrix = 0;
    sequence->load_chroma_intra_quantiser_matrix = sequence->load_chroma_non_intra_quantiser_matrix = 0;

    /* parse picture extensions and user data */
    int start;
    do {
        start = find_m2v_next_start_code(bb);
        if (start<0) {
            rvmessage("picture %d, tempref %d: failed to find next start code in picture extensions and user data", picno, picture->temporal_reference);
            rvfree(picture);
            return NULL;
        }
        if (start==0x1B2) { /* user data start code */
            flushbits(bb, 32);                  /* user_data_start_code */
            parse_m2v_user_data(bb, verbose);
        } else if (start==0x1B5) { /* extension start code */
            flushbits(bb, 32);                  /* extension_start_code */
            int id = getbits(bb, 4);            /* extension_start_code_identifier */
            switch (id) {
                case 0x3: /* quant matrix extension */
                    parse_m2v_quant_matrix_extension(bb, &picture->quantmatrix);
                    /* copy latest quant matricies to sequence struct for use in decoding */
                    if (picture->quantmatrix.load_intra_quantiser_matrix)
                        memcpy(sequence->intra_quantiser_matrix, picture->quantmatrix.intra_quantiser_matrix, 64*sizeof(picture->quantmatrix.intra_quantiser_matrix[0]));
                    if (picture->quantmatrix.load_non_intra_quantiser_matrix)
                        memcpy(sequence->non_intra_quantiser_matrix, picture->quantmatrix.non_intra_quantiser_matrix, 64*sizeof(picture->quantmatrix.non_intra_quantiser_matrix[0]));
                    if (picture->quantmatrix.load_chroma_intra_quantiser_matrix)
                        memcpy(sequence->chroma_intra_quantiser_matrix, picture->quantmatrix.chroma_intra_quantiser_matrix, 64*sizeof(picture->quantmatrix.chroma_intra_quantiser_matrix[0]));
                    if (picture->quantmatrix.load_chroma_non_intra_quantiser_matrix)
                        memcpy(sequence->chroma_non_intra_quantiser_matrix, picture->quantmatrix.chroma_non_intra_quantiser_matrix, 64*sizeof(picture->quantmatrix.chroma_non_intra_quantiser_matrix[0]));
                    break;

                case 0x4: /* copyright extension */
                    parse_m2v_copyright_extension(bb);
                    break;

                case 0x7: /* picture display extension */
                    parse_m2v_picture_display_extension(bb, sequence->progressive_sequence, extension->repeat_first_field, extension->top_field_first, extension->picture_structure);
                    break;

                case 0x8: /* picture coding extension */
                    parse_m2v_picture_extension(bb, extension);
                    break;

                default:
                    rvmessage("picture %d, tempref %d: unknown extension header found after picture header: 0x%1x", picno, picture->temporal_reference, id);
                    break;
            }
        }
    } while (start==0x1B2 || start==0x1B5);

    if (start==0x100 || start>0x1AF) {
        rvmessage("picture %d, tempref %d: found a non-slice start code at start of picture data: 0x%08x", picno, picture->temporal_reference, start);
        rvfree(picture);
        return NULL;
    }

    /* mpeg1 won't have a picture extension */
    if (sequence->mpeg1) {
        extension->f_code[0][0] = extension->f_code[0][1] = picture->forward_f_code;
        extension->f_code[1][0] = extension->f_code[1][1] = picture->backward_f_code;
        extension->intra_dc_precision = 0;          /* 8-bit Intra-DC precision */
        extension->picture_structure = PST_FRAME;   /* frame-picture, because progressive_sequence = '1' */
        extension->frame_pred_frame_dct = 1;        /* only frame-based prediction and frame DCT */
        extension->concealment_motion_vectors = 0;  /* no concealment motion vectors */
        extension->q_scale_type = 0;                /* linear quantiser_scale */
        extension->intra_vlc_format = 0;            /* MPEG-1 VLC table for Intra MBs */
        extension->alternate_scan = 0;              /* MPEG-1 zigzag scanning order */
        extension->repeat_first_field = 0;          /* because progressive_sequence = '1' */
        extension->chroma_420_type = 1;             /* chrominance is "frame-based", because progressive_sequence = '1' */
        extension->progressive_frame = 1;           /* because progressive_sequence = '1' */
    }

    return picture;
}

struct rvm2vpicture *parse_m2v_picture(struct bitbuf *bb, struct rvm2vsequence *sequence, int picno, int verbose)
{
    /* note bit position at start of picture */
    off_t pos = tellbits(bb);

    /* parse picture header and extensions */
    struct rvm2vpicture *picture = parse_m2v_picture_header_and_extensions(bb, sequence, picno, verbose);
    if (picture==NULL) {
        rvmessage("picture %d: failed to parse picture header", picno);
        return NULL;
    }

    /* convenient pointer to picture header extension */
    struct rvm2vextension *extension = &picture->extension;

    /* allocate array of macroblocks */
    size_t size = sequence->width_in_mbs*sequence->height_in_mbs*sizeof(struct rvm2vmacro);
    if (field_picture(picture))
        size /= 2;
    picture->macro = (struct rvm2vmacro *)rvalloc(NULL, size, 1);

    /* loop over slices in picture */
    int start;
    int prev_mb_addr = -1;
    int mb_addr = -1;
    do {
        /* reset the dc predictors at the start of each slice */
        int dct_dc_reset = 1 << (7+extension->intra_dc_precision);
        int dct_dc_pred[3] = {dct_dc_reset, dct_dc_reset, dct_dc_reset};

        /* reset the motion vector predictors at the start of each slice */
        const struct mv_t PMV_ZERO={0,0};
        struct mv_t PMV[2][2] = {{PMV_ZERO, PMV_ZERO}, {PMV_ZERO, PMV_ZERO}};

        /* calculate first macroblock address in slice */
        int slice_start_code = getbits32(bb); /* slice_start_code */
        slice_start_code &= 0xff;
        int mb_row;
        if (sequence->vertical_size_value>2800)
            mb_row = (getbits(bb, 3) << 7) + slice_start_code - 1;   /* slice_vertical_position_extension */
        else
            mb_row = slice_start_code - 1;
        if (mb_row>=sequence->height_in_mbs) {
            rvmessage("picture %d, tempref %d, macroblock %d: failed to parse slice start code: row number out of bounds", picno, picture->temporal_reference, mb_addr);
            mark_error_macroblock(&picture->macro[0]);
            return picture;
        }

        if (!sequence->mpeg1)
            prev_mb_addr = mb_row * sequence->width_in_mbs - 1;

        /* TODO priority_breakpoint */

        /* initialise quantiser scale code */
        int quant = getbits(bb, 5);

        /* slice extension */
        if (getbit(bb)) {                   /* slice_extension_flag */
            flushbits(bb, 1);               /* intra_slice */
            flushbits(bb, 1);               /* slice_picture_id_enable */
            flushbits(bb, 6);               /* slice_picture_id */
            while (getbit(bb))              /* extra_bit_slice */
                flushbits(bb, 8);           /* extra_information_slice */
        }

        /* look for missing macroblocks */
        if (prev_mb_addr != mb_addr)
            rvmessage("picture %d, tempref %d, macroblock %d: %d missing macroblocks in previous slice", picno, picture->temporal_reference, mb_addr, prev_mb_addr-mb_addr);

        /* loop over macroblocks in slice */
        struct rvm2vmacro *macro = picture->macro;
        do {
            off_t bitpos = tellbits(bb);

            /* discard mpeg1 macroblock stuffing */
            if (sequence->mpeg1) {
                while (showbits(bb, 11)==15)
                    flushbits(bb, 11);
            }

            /* read next macroblock address increment */
            int i, mb_incr = 34;
            for (i=-1; mb_incr==34; i++) {
                mb_incr = parse_m2v_macroblock_address_increment(bb);
                if (mb_incr<0) {
                    rvmessage("picture %d, tempref %d, macroblock %d: failed to parse macroblock address increment", picno, picture->temporal_reference, mb_addr);
                    mark_error_macroblock(&picture->macro[mb_addr+1]);
                    goto resync;
                }
            }
            mb_incr += i*33;

            /* look for skipped macroblocks, note that a new slice may have started */
            int num_skipped = prev_mb_addr + mb_incr - 1 - mb_addr;

            /* reset predictors after skipped macroblocks */
            if (num_skipped)
                dct_dc_pred[0]=dct_dc_pred[1]=dct_dc_pred[2]=dct_dc_reset;
            if (num_skipped && picture->picture_coding_type==PCT_P)
                PMV[0][0]=PMV[0][1]=PMV[1][0]=PMV[1][1]=PMV_ZERO;

            /* set motion parameters in skipped macroblocks */
            for (i=1; i<=num_skipped; i++) {
                macro = picture->macro + mb_addr + i;
                macro->skipped = 1;
                macro->quantiser_scale_code = quant;
                if (picture->extension.picture_structure==PST_FRAME) {
                    /* frame prediction */
                    macro->frame_motion_type = 2;
                } else {
                    /* field prediction */
                    macro->field_motion_type = 1;
                    if (picture->extension.picture_structure==PST_TOP)
                        macro->motion_vertical_field_select[0][0] = macro->motion_vertical_field_select[0][1] = 0;
                    else
                        macro->motion_vertical_field_select[0][0] = macro->motion_vertical_field_select[0][1] = 1;
                }
                if (picture->picture_coding_type==PCT_P) {
                    macro->type = 0x10;
                    macro->vector[0][0] = PMV_ZERO;
                } else if (picture->picture_coding_type==PCT_B) {
                    if (mb_addr+i>0)
                        macro->type = (macro-1)->type & 0x18;
                    macro->vector[0][0] = PMV[0][0];
                    macro->vector[0][1] = PMV[0][1];
                }
            }
            /* move over skipped macroblocks */
            mb_addr = prev_mb_addr + mb_incr;
            prev_mb_addr = mb_addr;
            macro = picture->macro + mb_addr;

            /* sanity check */
            if (mb_addr >= sequence->width_in_mbs*sequence->height_in_mbs) {
                rvmessage("picture %d, tempref %d, macroblock %d: too many macroblocks in picture", picno, picture->temporal_reference, mb_addr);
                goto resync;
            }

            /* initialise macroblock */
            macro->incr = mb_incr;
            macro->quantiser_scale_code = quant;

            /* parse macroblock layer */
            if (parse_m2v_macroblock(bb, sequence, picture, macro, picno, mb_addr)<0) {
                rvmessage("picture %d, tempref %d, macroblock %d: failed to parse mpeg2 macroblock", picno, picture->temporal_reference, mb_addr);
                mark_error_macroblock(macro);
                goto resync;
            }

            /* perform dc coefficient inverse prediction */
            if (macroblock_intra(macro->type))
                decode_m2v_dc(macro, sequence->chroma_format, dct_dc_pred);
            else
                dct_dc_pred[0]=dct_dc_pred[1]=dct_dc_pred[2]=dct_dc_reset;

            /* reset motion vector predictors */
            if (!macroblock_motion_forward(macro->type) && picture->picture_coding_type==PCT_P)
                if (!(macroblock_intra(macro->type) && extension->concealment_motion_vectors))
                    PMV[0][0]=PMV[0][1]=PMV[1][0]=PMV[1][1]=PMV_ZERO;

            /* perform motion vector inverse prediction */
            if (macroblock_intra(macro->type) && !extension->concealment_motion_vectors)
                PMV[0][0]=PMV[0][1]=PMV[1][0]=PMV[1][1]=PMV_ZERO;
            else {
                decode_m2v_vectors(macro, picture, PMV);
                /* concealment motion vectors may point outside the picture so don't check them */
                if (!macroblock_intra(macro->type))
                    if (!check_m2v_vectors(macro, bb->filename, picno, picture, mb_addr, sequence->width_in_mbs*16, sequence->height_in_mbs*16>>field_picture(picture)))
                        mark_error_badvectors(macro);
            }

            /* remember previous macroblock quant */
            quant = macro->quantiser_scale_code;

            /* measure number of bits in macroblock */
            macro->num_bits = tellbits(bb) - bitpos;
        } while (showbits(bb, 23));

resync:
        /* find next start code, measuring zero stuffing between slices */
        alignbits(bb);
        int zero_stuff = 1;
        while (showbits24(bb)!=0x000001) {
            /* move along by one byte */
            if (getbits(bb, 8)!=0)
                zero_stuff = 0;
            macro->stuff_bits += 8;
            picture->stuffing++;
            if (eofbits(bb))
                break;
        }
        start = showbits32(bb);

        /* report resync */
        if (macro->stuff_bits && !zero_stuff)
            rvmessage("picture %d, tempref %d, macroblock %d: found new slice %d after %d garbage bytes", picno, picture->temporal_reference, mb_addr, start&0xff, macro->stuff_bits/8);
        else if (macro->stuff_bits && zero_stuff && verbose>=1)
            rvmessage("picture %d, tempref %d, macroblock %d: found new slice %d after %d stuffing bytes", picno, picture->temporal_reference, mb_addr, start&0xff, zero_stuff);

    } while (start>=0x101 && start<=0x1AF);

    /* calculate size of picture */
    picture->bytes = (tellbits(bb)-pos)/8;

    return picture;
}

struct rvm2vsequence *parse_m2v_headers(struct bitbuf *bb, int verbose)
{
    struct rvm2vsequence *sequence = NULL;

    /* search for a start code */
    int start, pos = numbits(bb);
    while (1) {
        start = find_m2v_next_start_code(bb);
        /* look for a sequence start code */
        if (start==0x1B3)
            break;
        if (start<0)
            return NULL;
        /* look for a sequence end code */
        if (start==0x1B7)
            return NULL;
        /* move along */
        flushbits(bb, 8);
    }
    if (verbose>=1 && numbits(bb)-pos>7)
        rvmessage("%jd bits of leading garbage before sequence start code", numbits(bb)-pos);

    /* allocate sequence structure TODO support multiple sequences */
    size_t size = sizeof(struct rvm2vsequence);
    sequence = (struct rvm2vsequence *)rvalloc(NULL, size, 0);

    /* initialise sequence structure */
    memset(sequence, 0, size);
    memcpy(sequence->intra_quantiser_matrix, default_intra_quantiser_matrix, 64*sizeof(int));
    memcpy(sequence->non_intra_quantiser_matrix, default_non_intra_quantiser_matrix, 64*sizeof(int));
    memcpy(sequence->chroma_intra_quantiser_matrix, default_intra_quantiser_matrix, 64*sizeof(int));
    memcpy(sequence->chroma_non_intra_quantiser_matrix, default_non_intra_quantiser_matrix, 64*sizeof(int));

    /* allocate array of pictures in sequence */
    sequence->size_pic_array = 1024*128; /* FIXME how big should this be? each 1024 is a 4k page */
    sequence->picture = (struct rvm2vpicture **)rvalloc(NULL, sequence->size_pic_array*sizeof(struct rvm2vpicture *), 1);
    sequence->picture2 = (struct rvm2vpicture **)rvalloc(NULL, sequence->size_pic_array*sizeof(struct rvm2vpicture *), 1);

    /* parse sequence header */
    flushbits(bb, 32);                      /* sequence_header_code */
    if (parse_m2v_sequence(bb, sequence)<0)
        rvexit("%s: failed to parse mpeg2 sequence layer", bb->filename);

    /* check for mpeg1 sequence */
    start = find_m2v_next_start_code(bb);
    if (start<0) {
        rvmessage("%s: failed to find next start code after sequence start", bb->filename);
        return sequence;
    }
    if (start!=0x1B5) {
        /* mpeg1 sequence */
        sequence->mpeg1 = 1;
        sequence->progressive_sequence = 1;     /* progressive sequence */
        sequence->chroma_format = 1;            /* 4:2:0 */
        sequence->frame_rate_extension_n = 0;   /* MPEG-1 frame-rate */
        sequence->frame_rate_extension_d = 0;

        /* calculate decoded frame height and width */
        sequence->width_in_mbs = (sequence->horizontal_size_value+15) / 16;
        sequence->height_in_mbs = (sequence->vertical_size_value+15) / 16;

        /* report sequence parameters */
        if (verbose>=1) {
            rvmessage("%s: mpeg1%s %dx%d@%.2ffps bitrate=%.2f kbps vbv_buffer_size=%d", get_basename(bb->filename),
                    sequence->constrained_parameters_flag? " constrained parameters" : "",
                    sequence->horizontal_size_value, sequence->vertical_size_value,
                    sequence->frame_rate, sequence->bit_rate*400.0/1000.0, sequence->vbv_buffer_size);
        }

        return sequence;
    }

    flushbits(bb, 32);                  /* extension_start_code */
    flushbits(bb, 4);                   /* extension_start_code_identifier */

    /* parse sequence extension */
    if (parse_m2v_sequence_extension(bb, sequence)<0)
        rvexit("%s: failed to parse mpeg2 sequence extension", bb->filename);

    /* parse sequence extensions and user data */
    do {
        start = find_m2v_next_start_code(bb);
        if (start<0) {
            rvmessage("%s: failed to find next start code in sequence extensions and user data", bb->filename);
            break;
        }
        if (start==0x1B2) { /* user data start code */
            flushbits(bb, 32);          /* user_data_start_code */
            parse_m2v_user_data(bb, verbose);
        } else if (start==0x1B5) { /* extension start code */
            flushbits(bb, 32);          /* extension_start_code */
            int id = getbits(bb, 4);    /* extension_start_code_identifier */
            switch (id) {
                case 0x2: /* sequence display extension */
                    parse_m2v_sequence_display_extension(bb, sequence);
                    break;

                case 0x4: /* copyright extension */
                    parse_m2v_copyright_extension(bb);
                    break;

                case 0x5: /* sequence scalable extension */
                    rvmessage("%s: TODO sequence scalable extension", bb->filename);
                    break;

                default:
                    rvmessage("%s: invalid extension header found after sequence header: 0x%1x", bb->filename, id);
                    break;
            }
        }
    } while (start==0x1B2 || start==0x1B5);

    /* calculate decoded frame height and width */
    sequence->width_in_mbs = (sequence->horizontal_size_value+15) / 16;
    sequence->height_in_mbs = sequence->progressive_sequence?
            (sequence->vertical_size_value+15) / 16 :
            2*((sequence->vertical_size_value+31) / 32);

    /* report sequence parameters */
    if (verbose>=1) {
        const char *p, *l;
        describe_profile(sequence->profile_and_level, &p, &l);
        rvmessage("%s: %s@%s %dx%d@%.2ffps bitrate=%.2f kbps vbv_buffer_size=%d", get_basename(bb->filename), p, l,
                  sequence->horizontal_size_value, sequence->vertical_size_value,
                  sequence->frame_rate, sequence->bit_rate*400.0/1000.0, sequence->vbv_buffer_size);
    }

    /* bitstream is now positioned at start of first picture */
    return sequence;
}

/*
 * mpeg2 analysis functions.
 */
void find_m2v_motion_vector_range(struct rvm2vpicture *picture, struct rvm2vsequence *sequence, int *maxx, int *minx, int *maxy, int *miny)
{
    int mbx, mby;

    *maxx = 0;
    *minx = 0;
    *maxy = 0;
    *miny = 0;

    /* sanity check */
    if (picture==NULL || picture->macro==NULL)
        return;

    /* loop over macroblocks */
    for (mby=0; mby<sequence->height_in_mbs>>field_picture(picture); mby++) {
        for (mbx=0; mbx<sequence->width_in_mbs; mbx++) {
            int mb_addr = mby*sequence->width_in_mbs + mbx;
            struct rvm2vmacro *macro = picture->macro + mb_addr;

            /* don't examine intra and skipped macroblocks */
            if (macroblock_intra(macro->type) || macro->incr==0)
                continue;

            /* forward vectors */
            *maxx = mmax(*maxx, macro->vector[0][0].x);
            *minx = mmin(*minx, macro->vector[0][0].x);
            *maxy = mmax(*maxy, macro->vector[0][0].y);
            *miny = mmin(*miny, macro->vector[0][0].y);

            /* backward vectors */
            if (picture->picture_coding_type==PCT_B) {
                *maxx = mmax(*maxx, macro->vector[0][1].x);
                *minx = mmin(*minx, macro->vector[0][1].x);
                *maxy = mmax(*maxy, macro->vector[0][1].y);
                *miny = mmin(*miny, macro->vector[0][1].y);
            }

            /* field vectors */
            if (motion_vector_count(macro->frame_motion_type, macro->field_motion_type)==2) {
                /* forward vectors */
                *maxx = mmax(*maxx, macro->vector[1][0].x);
                *minx = mmin(*minx, macro->vector[1][0].x);
                *maxy = mmax(*maxy, macro->vector[1][0].y);
                *miny = mmin(*miny, macro->vector[1][0].y);

                /* backward vectors */
                if (picture->picture_coding_type==PCT_B) {
                    *maxx = mmax(*maxx, macro->vector[1][1].x);
                    *minx = mmin(*minx, macro->vector[1][1].x);
                    *maxy = mmax(*maxy, macro->vector[1][1].y);
                    *miny = mmin(*miny, macro->vector[1][1].y);
                }
            }

            /* TODO field picture, dual-prime vectors */
        }
    }
}

/*
 * mpeg2 decoding functions.
 */

static void decode_m2v_dc(struct rvm2vmacro *macro, int chroma_format, int dct_dc_pred[3])
{
    int block;
    int cc[4][12] = {
        {0},
        {0,0,0,0,1,2},
        {0,0,0,0,1,2,1,2},
        {0,0,0,0,1,2,1,2,1,2,1,2}
    };

    /* loop over blocks */
    for (block=0; block<block_count[chroma_format]; block++) {
        /* skip not coded blocks */
        if ((1<<block) & macro->pattern_code) {
            macro->coeff[block][0] = dct_dc_pred[cc[chroma_format][block]] + macro->coeff[block][0];
            dct_dc_pred[cc[chroma_format][block]] = macro->coeff[block][0];
        }
    }
}

static int decode_m2v_motion_vector(int f_code, int motion_code, int motion_residual)
{
    int delta;
    if (f_code==1 || motion_code==0)
        delta = motion_code;
    else {
        delta = ((abs(motion_code)-1) * (1<<(f_code-1))) + motion_residual + 1;
        if (motion_code<0)
            delta = -delta;
    }
    return delta;
}

static int decode_m2v_motion_wrap(int vector, int f_code)
{
    int r_size = f_code-1;
    int f = 1<<r_size;
    int high = 16*f - 1;
    int low  = -16*f;
    int range = 32*f;

    if (vector<low)
        return vector + range;
    else if (vector>high)
        return vector - range;
    else
        return vector;
}

static void decode_m2v_vector(struct rvm2vmacro *macro, const struct rvm2vpicture *picture, struct mv_t PMV[2][2], int r, int s)
{
    /* inverse predict motion vector x */
    macro->vector[r][s].x = PMV[r][s].x + macro->delta[r][s].x;
    macro->vector[r][s].x = decode_m2v_motion_wrap(macro->vector[r][s].x, picture->extension.f_code[s][0]);
    PMV[r][s].x = macro->vector[r][s].x;

    /* inverse predict motion vector y */
    if (mv_format(macro->frame_motion_type, macro->field_motion_type)==MV_FORMAT_FIELD && picture->extension.picture_structure==PST_FRAME)
        PMV[r][s].y >>= 1;
    macro->vector[r][s].y = PMV[r][s].y + macro->delta[r][s].y;
    macro->vector[r][s].y = decode_m2v_motion_wrap(macro->vector[r][s].y, picture->extension.f_code[s][1]);
    PMV[r][s].y = macro->vector[r][s].y;
    if (mv_format(macro->frame_motion_type, macro->field_motion_type)==MV_FORMAT_FIELD && picture->extension.picture_structure==PST_FRAME)
        PMV[r][s].y <<= 1;
}

static void decode_m2v_vectors(struct rvm2vmacro *macro, const struct rvm2vpicture *picture, struct mv_t PMV[2][2])
{
    int r;

    if (macro->frame_motion_type==1 || macro->field_motion_type==2) {
        /* decode forward motion vectors */
        if (macroblock_motion_forward(macro->type))
            for (r=0; r<2; r++)
                decode_m2v_vector(macro, picture, PMV, r, 0);

        /* decode backward motion vectors */
        if (macroblock_motion_backward(macro->type))
            for (r=0; r<2; r++)
                decode_m2v_vector(macro, picture, PMV, r, 1);
    }
    if (macro->frame_motion_type==2 || macro->field_motion_type==1) {
        /* decode forward motion vectors */
        if (macroblock_motion_forward(macro->type) || (macroblock_intra(macro->type) && picture->extension.concealment_motion_vectors)) {
            decode_m2v_vector(macro, picture, PMV, 0, 0);
            PMV[1][0] = PMV[0][0];
        }

        /* decode backward motion vectors */
        if (macroblock_motion_backward(macro->type)) {
            decode_m2v_vector(macro, picture, PMV, 0, 1);
            PMV[1][1] = PMV[0][1];
        }
    }
    if (macro->frame_motion_type==3 || macro->field_motion_type==3) {
        /* decode forward motion vectors */
        decode_m2v_vector(macro, picture, PMV, 0, 0);

        /* update motion vector predictors */
        PMV[1][0] = PMV[0][0];

        /* dual prime additional arithmetic */
        if (!field_picture(picture)) {
            int top_field_first = picture->extension.top_field_first;
            macro->vector[2][0].x = div2(macro->vector[0][0].x * (top_field_first? 1 : 3)) + macro->dmvector.x;
            macro->vector[2][0].y = div2(macro->vector[0][0].y * (top_field_first? 1 : 3)) + macro->dmvector.y - 1;
            macro->vector[3][0].x = div2(macro->vector[0][0].x * (top_field_first? 3 : 1)) + macro->dmvector.x;
            macro->vector[3][0].y = div2(macro->vector[0][0].y * (top_field_first? 3 : 1)) + macro->dmvector.y + 1;
        } else if (top_field_picture(picture)) {
            macro->vector[2][0].x = div2(macro->vector[0][0].x) + macro->dmvector.x;
            macro->vector[2][0].y = div2(macro->vector[0][0].y) + macro->dmvector.y - 1;
        } else {
            macro->vector[2][0].x = div2(macro->vector[0][0].x) + macro->dmvector.x;
            macro->vector[2][0].y = div2(macro->vector[0][0].y) + macro->dmvector.y + 1;
        }
    }

}

static int check_m2v_vectors(struct rvm2vmacro *macro, const char *filename, int picno, const struct rvm2vpicture *picture, int mbaddr, int width, int height)
{
    int pass = 1;

    /* TODO add checks for all other motion types */
    if (macro->frame_motion_type==2) {
        /* check forward motion vectors */
        if (macroblock_motion_forward(macro->type) || (macroblock_intra(macro->type) && picture->extension.concealment_motion_vectors))
            if (!recon_check(filename, picno, mbaddr, width, height, 16, 16, macro->vector[0][0]))
                pass = 0;

        /* check backward motion vectors */
        if (macroblock_motion_backward(macro->type))
            if (!recon_check(filename, picno, mbaddr, width, height, 16, 16, macro->vector[1][0]))
                pass = 0;
    }

    return pass;
}

static void decode_m2v_iquant(struct rvm2vmacro *macro, const struct rvm2vsequence *sequence, int quantiser_scale_code, int alternate_scan, int intra_dc_precision, int q_scale_type)
{
    int block;

    const int *inverse_scan = alternate_scan? zigzag_alternate_inverse : zigzag_inverse;
    const int quantiser_scale = quantiser_scale_table[q_scale_type][quantiser_scale_code];

    /* loop over blocks */
    for (block=0; block<block_count[sequence->chroma_format]; block++) {
        /* skip not coded blocks */
        if ((1<<block) & macro->pattern_code) {
            int index = 0;
            int sum = 0;
            coeff_t iquant[64];

            /* select quantiser matrix */
            const int *quantiser_matrix = block<4? (macroblock_intra(macro->type)? sequence->intra_quantiser_matrix : sequence->non_intra_quantiser_matrix) :
                    (macroblock_intra(macro->type)? sequence->chroma_intra_quantiser_matrix : sequence->chroma_non_intra_quantiser_matrix);

            /* reconstruct intra dc coeff */
            if (macroblock_intra(macro->type)) {
                iquant[0] = macro->coeff[block][0] << (3-intra_dc_precision);
                index++;
            }

            /* reconstruct coeffs TODO remember EOB to shorten loop */
            for (; index<64; index++) {
                int level = macro->coeff[block][inverse_scan[index]];
                int sign  = macroblock_intra(macro->type)? 0 : level > 0? 1 : level < 0? -1 : 0;
                iquant[index] = (2*level + sign) * quantiser_matrix[index] * quantiser_scale / 32;
            }

            /* mpeg1 mismatch control */
            if (sequence->mpeg1) {
                for (index=0; index<64; index++) {
                    if ((iquant[index]&1) == 0) {
                        int sign  = iquant[index] > 0? 1 : iquant[index] < 0? -1 : 0;
                        iquant[index] -= sign;
                    }
                }
            }

            /* clip coeffs and sum for mismatch control */
            for (index=0; index<64; index++) {
                /* TODO see if dc coeff really needs clipping */
                if (iquant[index] > 2047)
                    macro->pixel[block][index] = 2047;
                else if (iquant[index] < -2048)
                    macro->pixel[block][index] = -2048;
                else
                    macro->pixel[block][index] = iquant[index];
                sum ^= macro->pixel[block][index];
            }

            /* mpeg2 mismatch control */
            if (!sequence->mpeg1) {
                if ((sum & 1) == 0) {
                    if (macro->pixel[block][63] & 1)
                        macro->pixel[block][63]--;
                    else
                        macro->pixel[block][63]++;
                }
            }
        }
    }
}

static void decode_m2v_idct(struct rvm2vmacro *macro, int chroma_format, int refidct)
{
    int block;

    /* loop over blocks */
    for (block=0; block<block_count[chroma_format]; block++)
        /* skip not coded blocks */
        if ((1<<block) & macro->pattern_code) {
            if (refidct)
                reference_idct(macro->pixel[block], macro->pixel[block]);
            else
                chen_idct(macro->pixel[block], macro->pixel[block]);
        }
}

static void decode_m2v_fetch(struct rvm2vmacro *macro, struct mv_t vector[4][2], int s, int mbx, int mby, int width, int height, int field, int chroma_format, sample_t *reference, int combine, sample_t pels[][64])
{
    int i, j;
    sample_t sample[3][8][64]; /* TODO use [8][8] for clarity */

    if (macro->frame_motion_type==1) {
        /* select fields to predict from */
        int field[2];
        field[0] = macro->motion_vertical_field_select[0][s];
        field[1] = macro->motion_vertical_field_select[1][s];

        /* fetch luma motion compensated reference data for top field */
        recon_field(reference, width, sample[0][0], 8*2, 8, 4, 16*mbx+0, 16*mby+0+field[0], vector[0][s]);
        recon_field(reference, width, sample[0][1], 8*2, 8, 4, 16*mbx+8, 16*mby+0+field[0], vector[0][s]);
        recon_field(reference, width, sample[0][2], 8*2, 8, 4, 16*mbx+0, 16*mby+8+field[0], vector[0][s]);
        recon_field(reference, width, sample[0][3], 8*2, 8, 4, 16*mbx+8, 16*mby+8+field[0], vector[0][s]);

        /* fetch luma motion compensated reference data for bottom field */
        recon_field(reference, width, sample[0][0]+8, 8*2, 8, 4, 16*mbx+0, 16*mby+0+field[1], vector[1][s]);
        recon_field(reference, width, sample[0][1]+8, 8*2, 8, 4, 16*mbx+8, 16*mby+0+field[1], vector[1][s]);
        recon_field(reference, width, sample[0][2]+8, 8*2, 8, 4, 16*mbx+0, 16*mby+8+field[1], vector[1][s]);
        recon_field(reference, width, sample[0][3]+8, 8*2, 8, 4, 16*mbx+8, 16*mby+8+field[1], vector[1][s]);

        if (chroma_format==2) {
            /* calculate chroma motion vector */
            struct mv_t chroma_vector[2];
            chroma_vector[0].x = vector[0][s].x / 2;
            chroma_vector[0].y = vector[0][s].y;
            chroma_vector[1].x = vector[1][s].x / 2;
            chroma_vector[1].y = vector[1][s].y;

            /* fetch chroma motion compensated reference data for frame */
            reference += width*height;
            recon_field(reference, width/2, sample[0][4]+0, 8*2, 8, 4, 8*mbx, 16*mby+field[0], chroma_vector[0]);
            recon_field(reference, width/2, sample[0][4]+8, 8*2, 8, 4, 8*mbx, 16*mby+field[1], chroma_vector[1]);
            recon_field(reference, width/2, sample[0][6]+0, 8*2, 8, 4, 8*mbx, 16*mby+8+field[0], chroma_vector[0]);
            recon_field(reference, width/2, sample[0][6]+8, 8*2, 8, 4, 8*mbx, 16*mby+8+field[1], chroma_vector[1]);
            reference += width*height/2;
            recon_field(reference, width/2, sample[0][5]+0, 8*2, 8, 4, 8*mbx, 16*mby+field[0], chroma_vector[0]);
            recon_field(reference, width/2, sample[0][5]+8, 8*2, 8, 4, 8*mbx, 16*mby+field[1], chroma_vector[1]);
            recon_field(reference, width/2, sample[0][7]+0, 8*2, 8, 4, 8*mbx, 16*mby+8+field[0], chroma_vector[0]);
            recon_field(reference, width/2, sample[0][7]+8, 8*2, 8, 4, 8*mbx, 16*mby+8+field[1], chroma_vector[1]);
        } else {
            /* calculate chroma motion vector */
            struct mv_t chroma_vector[2];
            chroma_vector[0].x = vector[0][s].x / 2;
            chroma_vector[0].y = vector[0][s].y / 2;
            chroma_vector[1].x = vector[1][s].x / 2;
            chroma_vector[1].y = vector[1][s].y / 2;

            /* fetch chroma motion compensated reference data for frame */
            reference += width*height;
            recon_field(reference, width/2, sample[0][4]+0, 8*2, 8, 4, 8*mbx, 8*mby+field[0], chroma_vector[0]);
            recon_field(reference, width/2, sample[0][4]+8, 8*2, 8, 4, 8*mbx, 8*mby+field[1], chroma_vector[1]);
            reference += width*height/4;
            recon_field(reference, width/2, sample[0][5]+0, 8*2, 8, 4, 8*mbx, 8*mby+field[0], chroma_vector[0]);
            recon_field(reference, width/2, sample[0][5]+8, 8*2, 8, 4, 8*mbx, 8*mby+field[1], chroma_vector[1]);
        }
    }
    if (macro->frame_motion_type==2) {
        /* fetch luma motion compensated reference data for frame */
        recon_frame(reference, width, sample[0][0], 8, 8, 8, 16*mbx+0, 16*mby+0, vector[0][s]);
        recon_frame(reference, width, sample[0][1], 8, 8, 8, 16*mbx+8, 16*mby+0, vector[0][s]);
        recon_frame(reference, width, sample[0][2], 8, 8, 8, 16*mbx+0, 16*mby+8, vector[0][s]);
        recon_frame(reference, width, sample[0][3], 8, 8, 8, 16*mbx+8, 16*mby+8, vector[0][s]);

        if (chroma_format==2) {
            /* calculate chroma motion vector */
            struct mv_t chroma_vector;
            chroma_vector.x = vector[0][s].x / 2;
            chroma_vector.y = vector[0][s].y;

            /* fetch chroma motion compensated reference data for frame */
            reference += width*height;
            recon_frame(reference, width/2, sample[0][4], 8, 8, 8, 8*mbx, 16*mby, chroma_vector);
            recon_frame(reference, width/2, sample[0][6], 8, 8, 8, 8*mbx, 16*mby+8, chroma_vector);
            reference += width*height/2;
            recon_frame(reference, width/2, sample[0][5], 8, 8, 8, 8*mbx, 16*mby, chroma_vector);
            recon_frame(reference, width/2, sample[0][7], 8, 8, 8, 8*mbx, 16*mby+8, chroma_vector);
        } else {
            /* calculate chroma motion vector */
            struct mv_t chroma_vector;
            chroma_vector.x = vector[0][s].x / 2;
            chroma_vector.y = vector[0][s].y / 2;

            /* fetch chroma motion compensated reference data for frame */
            reference += width*height;
            recon_frame(reference, width/2, sample[0][4], 8, 8, 8, 8*mbx, 8*mby, chroma_vector);
            reference += width*height/4;
            recon_frame(reference, width/2, sample[0][5], 8, 8, 8, 8*mbx, 8*mby, chroma_vector);
        }
    }
    if (macro->frame_motion_type==3) {
        /* sanity check */
        if (s)
            rvmessage("can't have dual prime prediction in b-frames");

        /* fetch luma motion compensated reference data for top field from same parity */
        recon_field(reference, width, sample[1][0], 8*2, 8, 4, 16*mbx+0, 16*mby+0, vector[0][0]);
        recon_field(reference, width, sample[1][1], 8*2, 8, 4, 16*mbx+8, 16*mby+0, vector[0][0]);
        recon_field(reference, width, sample[1][2], 8*2, 8, 4, 16*mbx+0, 16*mby+8, vector[0][0]);
        recon_field(reference, width, sample[1][3], 8*2, 8, 4, 16*mbx+8, 16*mby+8, vector[0][0]);

        /* fetch luma motion compensated reference data for top field from opposite parity */
        recon_field(reference, width, sample[2][0], 8*2, 8, 4, 16*mbx+0, 16*mby+0+1, vector[2][0]);
        recon_field(reference, width, sample[2][1], 8*2, 8, 4, 16*mbx+8, 16*mby+0+1, vector[2][0]);
        recon_field(reference, width, sample[2][2], 8*2, 8, 4, 16*mbx+0, 16*mby+8+1, vector[2][0]);
        recon_field(reference, width, sample[2][3], 8*2, 8, 4, 16*mbx+8, 16*mby+8+1, vector[2][0]);

        /* fetch luma motion compensated reference data for bottom field from same parity */
        recon_field(reference, width, sample[1][0]+8, 8*2, 8, 4, 16*mbx+0, 16*mby+0+1, vector[0][0]);
        recon_field(reference, width, sample[1][1]+8, 8*2, 8, 4, 16*mbx+8, 16*mby+0+1, vector[0][0]);
        recon_field(reference, width, sample[1][2]+8, 8*2, 8, 4, 16*mbx+0, 16*mby+8+1, vector[0][0]);
        recon_field(reference, width, sample[1][3]+8, 8*2, 8, 4, 16*mbx+8, 16*mby+8+1, vector[0][0]);

        /* fetch luma motion compensated reference data for bottom field from opposite parity */
        recon_field(reference, width, sample[2][0]+8, 8*2, 8, 4, 16*mbx+0, 16*mby+0, vector[3][0]);
        recon_field(reference, width, sample[2][1]+8, 8*2, 8, 4, 16*mbx+8, 16*mby+0, vector[3][0]);
        recon_field(reference, width, sample[2][2]+8, 8*2, 8, 4, 16*mbx+0, 16*mby+8, vector[3][0]);
        recon_field(reference, width, sample[2][3]+8, 8*2, 8, 4, 16*mbx+8, 16*mby+8, vector[3][0]);

        if (chroma_format==2) {
            /* calculate chroma motion vector */
            struct mv_t chroma_vector[4];
            chroma_vector[0].x = vector[0][s].x / 2;
            chroma_vector[0].y = vector[0][s].y;
            chroma_vector[2].x = vector[2][s].x / 2;
            chroma_vector[2].y = vector[2][s].y;
            chroma_vector[3].x = vector[3][s].x / 2;
            chroma_vector[3].y = vector[3][s].y;

            /* fetch chroma motion compensated reference data */
            reference += width*height;
            recon_field(reference, width/2, sample[1][4]+0, 8*2, 8, 4, 8*mbx, 16*mby+0, chroma_vector[0]);
            recon_field(reference, width/2, sample[2][4]+0, 8*2, 8, 4, 8*mbx, 16*mby+1, chroma_vector[2]);
            recon_field(reference, width/2, sample[1][4]+8, 8*2, 8, 4, 8*mbx, 16*mby+1, chroma_vector[0]);
            recon_field(reference, width/2, sample[2][4]+8, 8*2, 8, 4, 8*mbx, 16*mby+0, chroma_vector[3]);
            recon_field(reference, width/2, sample[1][6]+0, 8*2, 8, 4, 8*mbx, 16*mby+0+8, chroma_vector[0]);
            recon_field(reference, width/2, sample[2][6]+0, 8*2, 8, 4, 8*mbx, 16*mby+1+8, chroma_vector[2]);
            recon_field(reference, width/2, sample[1][6]+8, 8*2, 8, 4, 8*mbx, 16*mby+1+8, chroma_vector[0]);
            recon_field(reference, width/2, sample[2][6]+8, 8*2, 8, 4, 8*mbx, 16*mby+0+8, chroma_vector[3]);
            reference += width*height/2;
            recon_field(reference, width/2, sample[1][5]+0, 8*2, 8, 4, 8*mbx, 16*mby+0, chroma_vector[0]);
            recon_field(reference, width/2, sample[2][5]+0, 8*2, 8, 4, 8*mbx, 16*mby+1, chroma_vector[2]);
            recon_field(reference, width/2, sample[1][5]+8, 8*2, 8, 4, 8*mbx, 16*mby+1, chroma_vector[0]);
            recon_field(reference, width/2, sample[2][5]+8, 8*2, 8, 4, 8*mbx, 16*mby+0, chroma_vector[3]);
            recon_field(reference, width/2, sample[1][7]+0, 8*2, 8, 4, 8*mbx, 16*mby+0+8, chroma_vector[0]);
            recon_field(reference, width/2, sample[2][7]+0, 8*2, 8, 4, 8*mbx, 16*mby+1+8, chroma_vector[2]);
            recon_field(reference, width/2, sample[1][7]+8, 8*2, 8, 4, 8*mbx, 16*mby+1+8, chroma_vector[0]);
            recon_field(reference, width/2, sample[2][7]+8, 8*2, 8, 4, 8*mbx, 16*mby+0+8, chroma_vector[3]);
        } else {
            /* calculate chroma motion vector */
            struct mv_t chroma_vector[4];
            chroma_vector[0].x = vector[0][s].x / 2;
            chroma_vector[0].y = vector[0][s].y / 2;
            chroma_vector[2].x = vector[2][s].x / 2;
            chroma_vector[2].y = vector[2][s].y / 2;
            chroma_vector[3].x = vector[3][s].x / 2;
            chroma_vector[3].y = vector[3][s].y / 2;

            /* fetch chroma motion compensated reference data */
            reference += width*height;
            recon_field(reference, width/2, sample[1][4]+0, 8*2, 8, 4, 8*mbx, 8*mby+0, chroma_vector[0]);
            recon_field(reference, width/2, sample[2][4]+0, 8*2, 8, 4, 8*mbx, 8*mby+1, chroma_vector[2]);
            recon_field(reference, width/2, sample[1][4]+8, 8*2, 8, 4, 8*mbx, 8*mby+1, chroma_vector[0]);
            recon_field(reference, width/2, sample[2][4]+8, 8*2, 8, 4, 8*mbx, 8*mby+0, chroma_vector[3]);
            reference += width*height/4;
            recon_field(reference, width/2, sample[1][5]+0, 8*2, 8, 4, 8*mbx, 8*mby+0, chroma_vector[0]);
            recon_field(reference, width/2, sample[2][5]+0, 8*2, 8, 4, 8*mbx, 8*mby+1, chroma_vector[2]);
            recon_field(reference, width/2, sample[1][5]+8, 8*2, 8, 4, 8*mbx, 8*mby+1, chroma_vector[0]);
            recon_field(reference, width/2, sample[2][5]+8, 8*2, 8, 4, 8*mbx, 8*mby+0, chroma_vector[3]);
        }

        /* combine predictions */
        for (i=0; i<block_count[chroma_format]; i++)
            for (j=0; j<64; j++)
                sample[0][i][j] = div2(sample[1][i][j] + sample[2][i][j]);
    }
    if (macro->field_motion_type==1) {
        /* select field to predict from */
        field = macro->motion_vertical_field_select[0][s];

        /* fetch luma motion compensated reference data for field */
        recon_field(reference, width, sample[0][0], 8, 8, 8, 16*mbx+0, 32*mby+0+field, vector[0][s]);
        recon_field(reference, width, sample[0][1], 8, 8, 8, 16*mbx+8, 32*mby+0+field, vector[0][s]);
        recon_field(reference, width, sample[0][2], 8, 8, 8, 16*mbx+0, 32*mby+16+field, vector[0][s]);
        recon_field(reference, width, sample[0][3], 8, 8, 8, 16*mbx+8, 32*mby+16+field, vector[0][s]);

        if (chroma_format==2) {
            /* calculate chroma motion vector */
            struct mv_t chroma_vector;
            chroma_vector.x = vector[0][s].x / 2;
            chroma_vector.y = vector[0][s].y;

            /* fetch chroma motion compensated reference data for frame */
            reference += width*height;
            recon_field(reference, width/2, sample[0][4], 8, 8, 8, 8*mbx, 32*mby+field, chroma_vector);
            recon_field(reference, width/2, sample[0][6], 8, 8, 8, 8*mbx, 32*mby+16+field, chroma_vector);
            reference += width*height/2;
            recon_field(reference, width/2, sample[0][5], 8, 8, 8, 8*mbx, 32*mby+field, chroma_vector);
            recon_field(reference, width/2, sample[0][7], 8, 8, 8, 8*mbx, 32*mby+16+field, chroma_vector);
        } else {
            /* calculate chroma motion vector */
            struct mv_t chroma_vector;
            chroma_vector.x = vector[0][s].x / 2;
            chroma_vector.y = vector[0][s].y / 2;

            /* fetch chroma motion compensated reference data for frame */
            reference += width*height;
            recon_field(reference, width/2, sample[0][4], 8, 8, 8, 8*mbx, 16*mby+field, chroma_vector);
            reference += width*height/4;
            recon_field(reference, width/2, sample[0][5], 8, 8, 8, 8*mbx, 16*mby+field, chroma_vector);
        }
    }
    if (macro->field_motion_type==2) {
        /* select fields to predict from */
        int field[2];
        field[0] = macro->motion_vertical_field_select[0][s];
        field[1] = macro->motion_vertical_field_select[1][s];

        /* fetch luma motion compensated reference data for upper 16x8 */
        recon_field(reference, width, sample[0][0], 8, 8, 8, 16*mbx+0, 32*mby+0+field[0], vector[0][s]);
        recon_field(reference, width, sample[0][1], 8, 8, 8, 16*mbx+8, 32*mby+0+field[0], vector[0][s]);

        /* fetch luma motion compensated reference data for lower 16x8 */
        recon_field(reference, width, sample[0][2], 8, 8, 8, 16*mbx+0, 32*mby+16+field[1], vector[1][s]);
        recon_field(reference, width, sample[0][3], 8, 8, 8, 16*mbx+8, 32*mby+16+field[1], vector[1][s]);

        if (chroma_format==2) {
            /* calculate chroma motion vector */
            struct mv_t chroma_vector[2];
            chroma_vector[0].x = vector[0][s].x / 2;
            chroma_vector[0].y = vector[0][s].y;
            chroma_vector[1].x = vector[1][s].x / 2;
            chroma_vector[1].y = vector[1][s].y;

            /* fetch chroma motion compensated reference data for upper 8x8 */
            reference += width*height;
            recon_field(reference, width/2, sample[0][4]+0,  8, 8, 8, 8*mbx, 32*mby+field[0], chroma_vector[0]);
            recon_field(reference, width/2, sample[0][6]+0,  8, 8, 8, 8*mbx, 32*mby+16+field[1], chroma_vector[1]);
            reference += width*height/2;
            recon_field(reference, width/2, sample[0][5]+0,  8, 8, 8, 8*mbx, 32*mby+field[0], chroma_vector[0]);
            recon_field(reference, width/2, sample[0][7]+0,  8, 8, 8, 8*mbx, 32*mby+16+field[1], chroma_vector[1]);
        } else {
            /* calculate chroma motion vector */
            struct mv_t chroma_vector[2];
            chroma_vector[0].x = vector[0][s].x / 2;
            chroma_vector[0].y = vector[0][s].y / 2;
            chroma_vector[1].x = vector[1][s].x / 2;
            chroma_vector[1].y = vector[1][s].y / 2;

            /* fetch chroma motion compensated reference data for frame */
            reference += width*height;
            recon_field(reference, width/2, sample[0][4]+0,  8, 8, 4, 8*mbx, 16*mby+field[0], chroma_vector[0]);
            recon_field(reference, width/2, sample[0][4]+32, 8, 8, 4, 8*mbx, 16*mby+8+field[1], chroma_vector[1]);
            reference += width*height/4;
            recon_field(reference, width/2, sample[0][5]+0,  8, 8, 4, 8*mbx, 16*mby+field[0], chroma_vector[0]);
            recon_field(reference, width/2, sample[0][5]+32, 8, 8, 4, 8*mbx, 16*mby+8+field[1], chroma_vector[1]);
        }
    }
    if (macro->field_motion_type==3) {
        /* sanity check */
        if (s)
            rvmessage("can't have dual prime prediction in b-frames");

        /* fetch luma motion compensated reference data from field of same parity */
        recon_field(reference, width, sample[1][0], 8, 8, 8, 16*mbx+0, 32*mby+0+field, vector[0][0]);
        recon_field(reference, width, sample[1][1], 8, 8, 8, 16*mbx+8, 32*mby+0+field, vector[0][0]);
        recon_field(reference, width, sample[1][2], 8, 8, 8, 16*mbx+0, 32*mby+16+field, vector[0][0]);
        recon_field(reference, width, sample[1][3], 8, 8, 8, 16*mbx+8, 32*mby+16+field, vector[0][0]);

        /* fetch luma motion compensated reference data from field of opposite parity */
        recon_field(reference, width, sample[2][0], 8, 8, 8, 16*mbx+0, 32*mby+0+(!field), vector[2][0]);
        recon_field(reference, width, sample[2][1], 8, 8, 8, 16*mbx+8, 32*mby+0+(!field), vector[2][0]);
        recon_field(reference, width, sample[2][2], 8, 8, 8, 16*mbx+0, 32*mby+16+(!field), vector[2][0]);
        recon_field(reference, width, sample[2][3], 8, 8, 8, 16*mbx+8, 32*mby+16+(!field), vector[2][0]);

        if (chroma_format==2) {
            /* calculate chroma motion vector */
            struct mv_t chroma_vector[3];
            chroma_vector[0].x = vector[0][s].x / 2;
            chroma_vector[0].y = vector[0][s].y;
            chroma_vector[2].x = vector[2][s].x / 2;
            chroma_vector[2].y = vector[2][s].y;

            /* fetch chroma motion compensated reference data */
            reference += width*height;
            recon_field(reference, width/2, sample[1][4], 8, 8, 8, 8*mbx, 32*mby+field, chroma_vector[0]);
            recon_field(reference, width/2, sample[2][4], 8, 8, 8, 8*mbx, 32*mby+(!field), chroma_vector[2]);
            recon_field(reference, width/2, sample[1][6], 8, 8, 8, 8*mbx, 32*mby+16+field, chroma_vector[0]);
            recon_field(reference, width/2, sample[2][6], 8, 8, 8, 8*mbx, 32*mby+16+(!field), chroma_vector[2]);
            reference += width*height/2;
            recon_field(reference, width/2, sample[1][5], 8, 8, 8, 8*mbx, 32*mby+field, chroma_vector[0]);
            recon_field(reference, width/2, sample[2][5], 8, 8, 8, 8*mbx, 32*mby+(!field), chroma_vector[2]);
            recon_field(reference, width/2, sample[1][7], 8, 8, 8, 8*mbx, 32*mby+16+field, chroma_vector[0]);
            recon_field(reference, width/2, sample[2][7], 8, 8, 8, 8*mbx, 32*mby+16+(!field), chroma_vector[2]);
        } else {
            /* calculate chroma motion vector */
            struct mv_t chroma_vector[3];
            chroma_vector[0].x = vector[0][s].x / 2;
            chroma_vector[0].y = vector[0][s].y / 2;
            chroma_vector[2].x = vector[2][s].x / 2;
            chroma_vector[2].y = vector[2][s].y / 2;

            /* fetch chroma motion compensated reference data */
            reference += width*height;
            recon_field(reference, width/2, sample[1][4], 8, 8, 8, 8*mbx, 16*mby+field, chroma_vector[0]);
            recon_field(reference, width/2, sample[2][4], 8, 8, 8, 8*mbx, 16*mby+(!field), chroma_vector[2]);
            reference += width*height/4;
            recon_field(reference, width/2, sample[1][5], 8, 8, 8, 8*mbx, 16*mby+field, chroma_vector[0]);
            recon_field(reference, width/2, sample[2][5], 8, 8, 8, 8*mbx, 16*mby+(!field), chroma_vector[2]);
        }

        /* combine predictions */
        for (i=0; i<block_count[chroma_format]; i++)
            for (j=0; j<64; j++)
                sample[0][i][j] = div2(sample[1][i][j] + sample[2][i][j]);
    }

    /* combine or copy samples */
    if (combine) {
        for (i=0; i<block_count[chroma_format]; i++)
            for (j=0; j<64; j++)
                pels[i][j] = div2(pels[i][j] + sample[0][i][j]);
    } else
        memcpy(pels[0], sample[0][0], block_count[chroma_format]*64);
}

static void decode_m2v_sample(struct rvm2vmacro *macro, int chroma_format, sample_t pels[][64])
{
    int block, i;

    for (block=0; block<block_count[chroma_format]; block++)
        /* no point checking cbp */
        if (macro->dct_type && (block<4 || chroma_format>1)) {
            /* luma block field dct coding */
            for (i=0; i<64; i++) {
                int j = ((i&0x18)<<1) + (i&0x7) + ((block&2)<<2);
                int k = ((i&0x20)>>4) + (block&5);
                pels[k][j] = (sample_t) clip(macro->pixel[block][i], 0, 255);
            }
        } else
            /* frame dct coding */
            for (i=0; i<64; i++)
                pels[block][i] = (sample_t) clip(macro->pixel[block][i], 0, 255);

}

static void decode_m2v_recon(struct rvm2vmacro *macro, int chroma_format, sample_t pels[][64])
{
    int block, i;

    for (block=0; block<block_count[chroma_format]; block++)
        /* skip not coded blocks */
        if ((1<<block) & macro->pattern_code) {
            if (macro->dct_type && (block<4 || chroma_format>1)) {
                /* field dct coding */
                for (i=0; i<64; i++) {
                    int j = ((i&0x18)<<1) + (i&0x7) + ((block&2)<<2);
                    int k = ((i&0x20)>>4) + (block&5);
                    int sample = (int)macro->pixel[block][i] + (int)pels[k][j];
                    pels[k][j] = (sample_t) clip(sample, 0, 255);
                }
            } else {
                for (i=0; i<64; i++) {
                    int sample = (int)macro->pixel[block][i] + (int)pels[block][i];
                    pels[block][i] = (sample_t) clip(sample, 0, 255);
                }
            }
        }

}

sample_t *decode_m2v(struct rvm2vpicture *picture, struct rvm2vsequence *sequence, sample_t *forward_reference, sample_t *backward_reference, int picno, int refidct)
{
    int mbx, mby;

    const int frame_width  = 16*sequence->width_in_mbs;
    const int frame_height = 16*sequence->height_in_mbs;
    const int field_height = frame_height >> field_picture(picture);

    /* sanity check */
    if (picture==NULL || picture->macro==NULL)
        return NULL;

    /* swap reference pointers in P-pictures */
    if (picture->picture_coding_type==PCT_P)
        forward_reference = backward_reference;

    /* allocate picture */
    size_t size;
    switch (sequence->chroma_format) {
        case 3 : size = frame_width*field_height*6/2*sizeof(sample_t); break;
        case 2 : size = frame_width*field_height*4/2*sizeof(sample_t); break;
        default: size = frame_width*field_height*3/2*sizeof(sample_t); break;
    }
    sample_t *frame = (sample_t *)rvalloc(NULL, size, 0);

    /* loop over macroblocks */
    for (mby=0; mby<sequence->height_in_mbs>>field_picture(picture); mby++) {
        for (mbx=0; mbx<sequence->width_in_mbs; mbx++) {
            int mb_addr = mby*sequence->width_in_mbs + mbx;
            struct rvm2vmacro *macro = picture->macro + mb_addr;

            /* sanity check */
            if (mbx==0 && macro->incr==0 && !sequence->mpeg1)
                rvmessage("picture %d, tempref %d, macroblock %d: macroblock at start of slice is skipped", picno, picture->temporal_reference, mb_addr);
            if (mbx==sequence->width_in_mbs-1 && macro->incr==0 && !sequence->mpeg1)
                rvmessage("picture %d, tempref %d, macroblock %d: macroblock at end of slice is skipped", picno, picture->temporal_reference, mb_addr);

            /* TODO optimise for skipped macroblocks */

            /* inverse quantise macroblock */
            decode_m2v_iquant(macro, sequence, macro->quantiser_scale_code, picture->extension.alternate_scan, picture->extension.intra_dc_precision, picture->extension.q_scale_type);

            /* inverse transform macroblock */
            decode_m2v_idct(macro, sequence->chroma_format, refidct);

            /* prepare error concealment motion vectors TODO this could be smarter */
            struct mv_t zero[4][2] = {{{0,0}}};

            /* motion compensate macroblock */
            sample_t pels[8][64];
            struct mv_t (*vector)[2];
            vector = macroblock_error_badvectors(macro->type)? zero : macro->vector;

            if (picture->picture_coding_type==PCT_P)
                if (!macroblock_intra(macro->type) && forward_reference!=NULL)
                    decode_m2v_fetch(macro, vector, 0, mbx, mby, frame_width, frame_height, bottom_field_picture(picture), sequence->chroma_format, forward_reference, 0, pels);
            if (picture->picture_coding_type==PCT_B) {
                if (macroblock_motion_forward(macro->type) && forward_reference!=NULL && macroblock_motion_backward(macro->type) && backward_reference!=NULL) {
                    decode_m2v_fetch(macro, vector, 0, mbx, mby, frame_width, frame_height, bottom_field_picture(picture), sequence->chroma_format, forward_reference, 0, pels);
                    decode_m2v_fetch(macro, vector, 1, mbx, mby, frame_width, frame_height, bottom_field_picture(picture), sequence->chroma_format, backward_reference, 1, pels);
                }
                else if (macroblock_motion_forward(macro->type) && forward_reference!=NULL)
                    decode_m2v_fetch(macro, vector, 0, mbx, mby, frame_width, frame_height, bottom_field_picture(picture), sequence->chroma_format, forward_reference, 0, pels);
                else if (macroblock_motion_backward(macro->type) && backward_reference!=NULL)
                    decode_m2v_fetch(macro, vector, 1, mbx, mby, frame_width, frame_height, bottom_field_picture(picture), sequence->chroma_format, backward_reference, 0, pels);
            }

            /* reconstruct macroblock */
            if (macroblock_intra(macro->type))
                decode_m2v_sample(macro, sequence->chroma_format, pels);
            else
                decode_m2v_recon(macro, sequence->chroma_format, pels);

            /* copy macroblock into frame */
            copy_macroblock_into_picture(frame, frame_width, field_height, sequence->chroma_format, mbx, mby, pels);

        }
    }

    return frame;
}
