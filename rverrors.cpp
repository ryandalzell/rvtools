/*
 * Description: Add bit errors and/or packet loss to video bitstream data.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-06-23 13:26:19 $
 * Revision   : $Revision: 1.29 $
 * Copyright  : (c) 2005 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include "getopt.h"

#include "rvutil.h"
#include "rvbits.h"
#include "rvheader.h"
#include "rvh264.h"

#define MAX_NAL_UNITS 16

/* poor man's pseudorandom number generator */
#ifdef WIN32
#define srand48 srand
double drand48()
{
    return (double)rand() / (double)RAND_MAX;
}
#endif

const char *appname;

void usage(int exitcode)
{
    fprintf(stderr, "%s: introduce bit errors and/or packet loss errors into a data stream\n", appname);
    fprintf(stderr, "usage: adderror [options] [<file>] [<file>...]\n");
    fprintf(stderr, "\t-b <num> : uniformly distribute one bit error every <num> bits\n");
    fprintf(stderr, "\t-p <num> : uniformly drop one packet in every <num> packets\n");
    fprintf(stderr, "\t-s <num> : set packet size to <num> bytes\n");
    fprintf(stderr, "\t-a       : arbitrarily reorder packets\n");
    fprintf(stderr, "\t-m       : mpeg4 mode - use video packets\n");
    fprintf(stderr, "\t-n       : h.264 mode - use nal units\n");
    fprintf(stderr, "\t-r       : use random seed, default is repeatable behaviour\n");
    fprintf(stderr, "\t-o <file>: write output to file\n");
    fprintf(stderr, "\t--       : disable argument processing\n");
    fprintf(stderr, "\t-h       : print this usage message\n");
    exit(exitcode);
}

int add_bit_errors(char *data, int len, float ber)
{
    int i, j;
    int errors=0;
    for (i=0; i<len; i++) {
        for (j=0; j<8; j++) {
            if (drand48() < ber) {
                data[i] = data[i] ^ (1<<j);
                errors++;
            }
        }
    }
    return errors;
}

void write_nal_units(char *nal[], size_t bytes[], int units, int random, FILE *fileout)
{
    int i, todo=units;

    if (random==2) {
        /* write access units to output in reverse order */
        for (i=units-1; i>=0; i--)
            fwrite(nal[i], bytes[i], 1, fileout);
    } else if (random==1) {
        /* write access units to output in random order */
        while (todo) {
            int ran = (int)(drand48() * (double)units);
            if (bytes[ran]) {
                fwrite(nal[ran], bytes[ran], 1, fileout);
                bytes[ran] = 0;
                todo--;
            }
        }
    } else {
        /* write access units to output in normal order*/
        for (i=0; i<units; i++)
            fwrite(nal[i], bytes[i], 1, fileout);
    }
}

int main(int argc, char *argv[])
{
    FILE *filein = stdin;
    FILE *fileout = stdout;
    char *filename[RV_MAXFILES] = {0};
    char *outfile = NULL;
    int fileindex = 0;
    int numfiles = 0;
    char *data;
    int size = 1024;
    double ber = 0.0, plr = 0.0;
    int bit_errors = 0, lost_packets = 0;
    int total_units = 0;
    int random = 0;
    int reorder = 0;

    /* command line defaults */
    int verbose = 0;
    int filetype = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"bit-error-rate",   1, NULL, 'b'},
            {"packet-loss-rate", 1, NULL, 'p'},
            {"packet-size",      1, NULL, 's'},
            {"reorder",          0, NULL, 'a'},
            {"mpeg4",            0, NULL, 'm'},
            {"h.264",            0, NULL, 'n'},
            {"random",           0, NULL, 'r'},
            {"output",           1, NULL, 'o'},
            {"quiet",            0, NULL, 'q'},
            {"verbose",          0, NULL, 'v'},
            {"usage",            0, NULL, 'h'},
            {"help",             0, NULL, 'h'},
            {NULL,               0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "b:p:s:amnro:qvh", long_options, NULL);
        if (optchar==-1)
            break;

        int arg;
        switch (optchar) {
            case 'b':
                arg = atoi(optarg);
                if (arg==0)
                    rvexit("-b switch cannot have an argument of zero");
                ber = 1.0/(double)arg;
                rvmessage("using bit error rate of %.2e", ber);
                break;

            case 'p':
                arg = atoi(optarg);
                if (arg==0)
                    rvexit("-p switch cannot have an argument of zero");
                plr = 1.0/(double)arg;
                rvmessage("using packet loss rate of %.2f%%", plr*100);
                break;

            case 's':
                size = atoi(optarg);
                if (size==0)
                    rvexit("cannot have a packet size of zero bytes");
                rvmessage("using packet size of %d bytes", size);
                break;

            case 'a':
                reorder = 1;
                break;

            case 'm':
                filetype = M4V;
                break;

            case 'n':
                filetype = H264;
                break;

            case 'r':
                random = 1;
                break;

            case 'o':
                outfile = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < sizeof(filename)/sizeof(filename[0]))
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    /* sanity check command line */
    if (ber==0.0 && plr==0.0 && reorder==0) {
        rvmessage("you must specify an error mode");
        return 2;
    }
    if (numfiles==0)
        rvexit("sorry, %s cannot read from filein at present", appname);

    /* open output file */
    if (outfile)
        fileout = fopen(outfile, "wb");
    if (fileout==NULL)
        rverror("failed to open output file \"%s\"", outfile);

    /* ignore SIGPIPE on output file */
    if (signal(SIGPIPE, SIG_IGN)==SIG_ERR)
        rverror("failed to set ignore on SIGPIPE");

    /* seed random number generator */
    if (random)
        srand48(getpid());
    else
        srand48(1974); /* use fixed seed for repeatibility */

    /* allocate data buffer */
    data = (char *)rvalloc(NULL, size*sizeof(char), 0);

    /* main loop */
    do {
        /* open input file */
        if (numfiles)
            filein = fopen(filename[fileindex], "rb");
        if (filein==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex]);

        /* mpeg4 video packet loss */
        if (filetype==M4V) {
            /* prepare to parse m4v file */
            struct bitbuf *bb = initbits_filename(filename[fileindex],  B_GETBITS);
            if (bb==NULL)
                break;

            /* header flags */
            int video_object_shape;
            int vti_size;
            int sprite_enable;
            int scalability;
            int enhancement_type;
            int ce_disable;
            struct ce_header ce;
            int interlaced;

            /* init bitcount */
            int head = numbits(bb);
            int tail = 0;

            /* need to parse a vol header first */
            do {
                /* look for a vol header */
                int start=showbits32(bb);
                if (start>=0x00000120 && start<=0x0000012f) {
                    int video_object_type;
                    int video_object_verid;
                    int width;
                    int height;
                    int obmc_disable;
                    int quant_type;
                    int quarter_sample;
                    int data_partitioned;
                    int rvlc;
                    flushbits(bb, 32);
                    parse_vol(bb, &video_object_type, &video_object_verid, &video_object_shape,
                              &vti_size, &width, &height, &interlaced, &obmc_disable, &sprite_enable,
                              &quant_type, &quarter_sample, &data_partitioned, &rvlc,
                              &scalability, &enhancement_type, &ce_disable, &ce);
                    alignbits(bb);
                    break;
                }

                /* keep on looking */
                flushbits(bb, 8);

            } while(!eofbits(bb));

            /* check for eof */
            if (eofbits(bb)) {
                rvmessage("could not find a vol header in file \"%s\"", filename[fileindex]);
            } else {

                /* loop over video packets */
                int fcode = 0;
                int frame = -1;
                int packet = -1;
                do {
                    /* look for a vop header or resync marker*/
                    int vop_coding_type;
                    int vti;
                    do {
                        /* look for a vop header */
                        int start=showbits32(bb);
                        if (start==0x000001b6) {
                            tail = numbits(bb);
                            flushbits(bb, 32);
                            parse_vop(bb, vti_size, video_object_shape, sprite_enable, scalability,
                                    enhancement_type, ce_disable, &ce, interlaced,
                                    &vop_coding_type, &vti, &fcode);
                            alignbits(bb);
                            frame++;
                            packet = -1;
                            break;
                        }

                        /* look for a resync marker */
                        if (frame>=0) {
                            int marker_length = 0;
                            if (vop_coding_type==VOP_I)
                                marker_length = 17;
                            else if (vop_coding_type==VOP_P && fcode)
                                marker_length = 16+fcode;
                            start=showbits(bb, marker_length);
                            if (start==1) {
                                tail = numbits(bb);
                                packet++;
                                break;
                            }
                        }

                        /* move along, nothing to see here */
                        flushbits(bb, 8);

                    } while (!eofbits(bb));

                    /* time to finish up */
                    if (eofbits(bb))
                        tail = numbits(bb);

                    /* measure size of video packet, including headers */
                    int bytes = tail - head;
                    if (bytes%8)
                        rvexit("resynchronisation point not byte aligned");
                    bytes /= 8;

                    /* check the buffer is big enough */
                    if (bytes>size) {
                        data = (char *)rvalloc(data, bytes, 0);
                        size = bytes;
                    }

                    /* read data packet from input */
                    size_t read = fread(data, 1, bytes, filein);
                    if (read!=bytes)
                        rverror("only read %zd bytes out of %d from input", read, bytes);

                    /* copy or drop */
                    int copy = 1;
                    if (packet==-1) {
                        /* always copy vol headers */
                        copy = 1;
                    } else if (frame<1) {
                        /* don't drop packets in first frame */
                        copy = 1;
                    } else if (drand48() < plr) {
                        /* drop this packet */
                        copy = 0;
                        lost_packets++;
                        if (verbose>=1)
                            rvmessage("dropping video packet of %d bytes in frame %d", bytes, frame);
                    }

                    /* write data packet to output */
                    if (copy) {
                        /* add bit errors */
                        int errors = add_bit_errors(data, bytes, ber);
                        if (errors) {
                            if (verbose>=1)
                                rvmessage("adding %d bit errors to video packet %d in frame %d", errors, packet, frame);
                            bit_errors += errors;
                        }

                        if (fwrite(data, bytes, 1, fileout)!=1)
                            break;
                    }

                    /* start looking for next video packet */
                    head = tail;
                    flushbits(bb, 8);

                } while (!eofbits(bb) && !ferror(fileout));
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");
            }

            /* tidy up */
            freebits(bb);
        }
        /* h.264 nal unit loss */
        else if (filetype==H264)
        {
            char *nal[MAX_NAL_UNITS] = {0};
            size_t size[MAX_NAL_UNITS]  = {0};
            size_t bytes[MAX_NAL_UNITS] = {0};
            struct rv264sequence seq = {0};

            /* prepare to parse h.264 file */
            struct bitbuf *bb = initbits_filename(filename[fileindex],  B_GETBITS);
            if (bb==NULL)
                break;

            /* loop over nal units */
            int frame = -1;
            int unit = 0;
            int nal_unit_type, prev_nal_unit_type = 0;
            int pic_order_cnt = 0, prev_pic_order_cnt = 0;
            do {
                /* note byte count at start of nal unit */
                int head = numbits(bb) / 8;

                /* look for a start code prefix */
                while (!eofbits(bb) && showbits24(bb)!=0x000001 && showbits32(bb)!=0x00000001)
                    flushbits(bb, 8);               /* leading_zero_8bits */
                if (showbits24(bb)!=0x000001)
                    flushbits(bb, 8);               /* zero_byte */

                /* parse nal unit */
                if (!eofbits(bb)) {
                    flushbits(bb, 24);              /* start_code_prefix_one_3bytes */

                    /* parse nal_unit header */
                    int nal_ref_idc;
                    int ret = parse_nal_unit_header(bb, &nal_unit_type, &nal_ref_idc);
                    if (ret<0)
                        rvexit("failed to parse nal unit %d in file \"%s\"", total_units, filename[fileindex]);

                    /* parse sequence parameter set */
                    if (nal_unit_type==7) {
                        struct rv264seq_parameter_set *sps = parse_sequence_param(bb);
                        if (sps==NULL)
                            rvexit("failed to parse sequence parameter set header in file \"%s\"", filename[fileindex]);

                        if (verbose>=1)
                            rvmessage("seq_parameter_set_id=%d", sps->seq_parameter_set_id);

                        rvfree(seq.sps[sps->seq_parameter_set_id]);
                        seq.sps[sps->seq_parameter_set_id] = sps;
                    }

                    /* parse picture parameter set */
                    if (nal_unit_type==8) {
                        struct rv264pic_parameter_set *pps = parse_picture_param(bb, seq.sps, verbose); // FIXME support multiple sequences.
                        if (pps==NULL)
                            rvexit("failed to parse picture parameter set header in file \"%s\"", filename[fileindex]);

                        if (verbose>=1)
                            rvmessage("pic_parameter_set_id=%d seq_parameter_set_id=%d", pps->pic_parameter_set_id, pps->seq_parameter_set_id);

                        rvfree(seq.pps[pps->pic_parameter_set_id]);
                        seq.pps[pps->pic_parameter_set_id] = pps;
                    }

                    /* parse slice header */
                    if (nal_unit_type==1 || nal_unit_type==5) {
                        struct slice_header *sh = parse_slice_header(bb, nal_unit_type, nal_ref_idc, seq.pps, verbose);
                        if (sh==NULL)
                            rvexit("failed to parse slice header in file \"%s\"", filename[fileindex]);

                        /* look for end of access unit (commented code is for reordering across picture boundaries) */
                        if (sh->PicOrderCnt != prev_pic_order_cnt /*&& prev_pic_order_cnt%4==0*/) {
                            if (verbose>=1)
                                rvmessage("writing access unit: %d nal units of type %d", unit, prev_nal_unit_type);
                            write_nal_units(nal, bytes, unit, 1, fileout);
                            unit = 0;
                        }

                        pic_order_cnt = sh->PicOrderCnt;
                    }

                    /* look for a start code prefix */
                    alignbits(bb);
                    while (!eofbits(bb) && showbits24(bb)!=0x000001 && showbits32(bb)!=0x00000001)
                        flushbits(bb, 8);           /* trailing_zero_8bits */

                    /* calculate size of nal unit */
                    int tail = numbits(bb) / 8;
                    bytes[unit] = tail - head;
                    if (verbose>=2)
                        rvmessage("nal unit %d is %zd bytes", total_units, bytes[unit]);

                    /* check the buffer is big enough */
                    if (bytes[unit]>size[unit]) {
                        nal[unit] = (char *)rvalloc(nal[unit], bytes[unit], 0);
                        size[unit] = bytes[unit];
                    }

                    /* read nal unit from input */
                    size_t read = fread(nal[unit], 1, bytes[unit], filein);
                    if (read!=bytes[unit])
                        rverror("only read %zd bytes out of %zd from input", read, bytes[unit]);
                    if (++unit==MAX_NAL_UNITS)
                        rvexit("number of nal units exceeds supported limit: %d", MAX_NAL_UNITS);

                    /* add bit errors */
                    if (nal_unit_type==1) {
                        int errors = add_bit_errors(nal[unit], bytes[unit], ber);
                        if (errors) {
                            if (verbose>=1)
                                rvmessage("adding %d bit errors to nal unit %d in frame %d", errors, unit, frame);
                            bit_errors += errors;
                        }
                    }

                    /* only discard or reorder VCL nal units */
                    if (nal_unit_type>5) {
                        /* write nal units to output */
                        write_nal_units(nal, bytes, unit, 0, fileout);
                        unit = 0;
                    }

                    prev_nal_unit_type = nal_unit_type;
                    prev_pic_order_cnt = pic_order_cnt;
                    total_units++;
                }

            } while (!eofbits(bb) && !ferror(fileout));
            if (ferror(fileout) && errno!=EPIPE)
                rverror("failed to write to output");

            /* write remaining nal units to output */
            write_nal_units(nal, bytes, unit, 1, fileout);

            /* tidy up */
            freebits(bb);
        }
        /* generic packet loss */
        else if (ber>0 || plr>0) {
            while (!feof(filein) && !ferror(filein) && !ferror(fileout)) {
                int len = fread(data, 1, size, filein);

                /* add bit errors */
                bit_errors += add_bit_errors(data, len, ber);

                if (fwrite(data, len, 1, fileout)!=1)
                    break;
            }
            if (ferror(fileout) && errno!=EPIPE)
                rverror("failed to write to output");
        }

        /* close input file */
        if (numfiles)
            fclose(filein);
    } while (++fileindex<numfiles);

    if (verbose>=0)
    {
        rvmessage("introduced %d bit errors", bit_errors);
        rvmessage("introduced %d lost packets", lost_packets);
        if (filetype==H264)
            rvmessage("processed %d nal units", total_units);
    }

    /* tidy up */
    rvfree(data);

    return 0;
}
