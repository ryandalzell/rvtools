
#ifndef _RVCRC_H_
#define _RVCRC_H_

typedef u_int32_t crc32_t;

crc32_t crc32(unsigned char *data, size_t size);

#endif
