/*
 * Description: YUV4MPEG file format functions, to avoid using an external library.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-04-27 10:33:50 $
 * Revision   : $Revision: 1.9 $
 * Copyright  : (c) 2005 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "rvutil.h"

int write_y4m_stream_header(int width, int height, chromaformat_t chromaformat, FILE *stream)
{
    const char *chromastring = NULL;
    switch (chromaformat) {
        case UNKNOWN:    chromastring = NULL; break;
        case YUV400:     chromastring = "mono"; break;
        case YUV420:     chromastring = "420mpeg2"; break;
        case YUV420mpeg: chromastring = "420mpeg2"; break;
        case YUV420jpeg: chromastring = "420jpeg"; break;
        case YUV420dv:   chromastring = "420paldv"; break;
        case YUV422:     chromastring = "422"; break;
        case YUV444:     chromastring = "444"; break;
    }
    if (chromastring)
        return fprintf(stream, "YUV4MPEG2 W%d H%d C%s Ip F25:1 A4:3\n", width, height, chromastring);
    else
        return fprintf(stream, "YUV4MPEG2 W%d H%d Ip F25:1 A4:3\n", width, height);
}

int write_y4m_frame_header(FILE *stream)
{
    return fprintf(stream, "FRAME\n");
}

static int read_y4m_tagged_field(char *field, int len, FILE *stream)
{
    int i;
    for (i=0; i<len; i++) {
        field[i]=fgetc(stream);
        if (field[i] == ' ' || field[i] == '\n') {
            ungetc(field[i], stream);
            break;
        }
    }
    if (i==len)
        rvexit("buffer overflow detected in read_y4m_tagged_field");
    return i;
}

int read_y4m_stream_header(int *width, int *height, fourcc_t *fourcc, FILE *stream)
{
    char c, data[16];
    int i = 0;

    /* check for magic string */
    i += fread(data, 1, 9, stream);
    if (i != 9)
        return -1;
    if (strncmp(data, "YUV4MPEG2", 9) != 0) {
        rvmessage("failed to find yuv4mpeg stream header magic string: %.9s", data);
        return -1;
    }

    /* read tagged fields */
    while ((c = fgetc(stream)) == ' ') {
        c = fgetc(stream);
        i+=2;
        switch (c)
        {
            case 'W':
                i += read_y4m_tagged_field(data, sizeof(data)/sizeof(char), stream);
                *width = atoi(data);
                break;
            case 'H':
                i += read_y4m_tagged_field(data, sizeof(data)/sizeof(char), stream);
                *height = atoi(data);
                break;
            case 'I':
                i += read_y4m_tagged_field(data, sizeof(data)/sizeof(char), stream);
                break;
            case 'F':
                i += read_y4m_tagged_field(data, sizeof(data)/sizeof(char), stream);
                // TODO rvmessage("framerate=%s", data);
                break;
            case 'A':
                i += read_y4m_tagged_field(data, sizeof(data)/sizeof(char), stream);
                break;
            case 'C':
                i += read_y4m_tagged_field(data, sizeof(data)/sizeof(char), stream);
                if (fourcc) {
                    if (strncmp(data, "420jpeg", 7)==0 || strncmp(data, "420paldv", 8)==0 || strncmp(data, "420", 3)==0)
                        *fourcc = I420;
                    else if (strncmp(data, "422", 3)==0)
                        *fourcc = I422;
                    else if (strncmp(data, "444", 3)==0)
                        *fourcc = I444;
                    else
                        rvmessage("unknown yu4mpeg chroma format: %s", data);
                }
                break;
            case 'X':
                i += read_y4m_tagged_field(data, sizeof(data)/sizeof(char), stream);
                /* TODO store these for later */
                break;
            default:
                rvmessage("unknown yuv4mpeg tag: %c", c);
                break;
        }
        //rvmessage("%c: %s", c, data);
    }
    i++;

    /* check stream header parsed correctly */
    if (c != '\n') {
        rvmessage("yuv4mpeg stream header did not parse correctly");
        return -1;
    }

    return i;
}

int read_y4m_frame_header(FILE *stream)
{
    char c, data[16];
    int i = 0;

    /* check for magic string */
    i += fread(data, 1, 5, stream);
    if (i != 5)
        return feof(stream)? 0 : -1;
    if (strncmp(data, "FRAME", 5) != 0) {
        rvmessage("failed to find yuv4mpeg frame header magic string: %.5s", data);
        return -1;
    }

    /* read tagged fields */
    while ((c = fgetc(stream)) == ' ') {
        c = fgetc(stream);
        i += 2;
        switch (c)
        {
        case 'X':
            i += read_y4m_tagged_field(data, sizeof(data)/sizeof(char), stream);
            break;
        }
    }
    i++;

    /* check stream header parsed correctly */
    if (c != '\n') {
        rvmessage("yuv4mpeg frame header did not parse correctly");
        return -1;
    }

    return i;
}

static int read_y4m_tagged_field_bits(struct bitbuf *bb, char *field, int len)
{
    int i;
    for (i=0; i<len; i++) {
        field[i]=getbyte(bb);
        if (field[i] == ' ' || field[i] == '\n') {
            unflushbits(bb, 8); //ungetc(field[i], stream);
            break;
        }
    }
    if (i==len)
        rvexit("buffer overflow detected in read_y4m_tagged_field");
    return i;
}

int read_y4m_stream_header_bits(struct bitbuf *bb, int *width, int *height, framerate_t *framerate, fourcc_t *fourcc)
{
    char c, data[16];
    int i = 0;
    int numer, denom;

    /* check for magic string */
    i += readbits(bb, data, 9);
    if (i != 9)
        return -1;
    if (strncmp(data, "YUV4MPEG2", 9) != 0) {
        rvmessage("failed to find yuv4mpeg stream header magic string: %.9s", data);
        return -1;
    }

    /* set default for optional fields */
    *fourcc = I420;

    /* read tagged fields */
    while ((c = getbyte(bb)) == ' ') {
        c = getbyte(bb);
        i+=2;
        switch (c)
        {
            case 'W':
                i += read_y4m_tagged_field_bits(bb, data, sizeof(data)/sizeof(char));
                if (width)
                    *width = atoi(data);
                break;
            case 'H':
                i += read_y4m_tagged_field_bits(bb, data, sizeof(data)/sizeof(char));
                if (height)
                    *height = atoi(data);
                break;
            case 'I':
                i += read_y4m_tagged_field_bits(bb, data, sizeof(data)/sizeof(char));
                break;
            case 'F':
                i += read_y4m_tagged_field_bits(bb, data, sizeof(data)/sizeof(char));
                numer = atoi(data);
                denom = atoi(strchr(data, ':')+1);
                if (framerate)
                    *framerate = (framerate_t)numer / (framerate_t)denom;
                break;
            case 'A':
                i += read_y4m_tagged_field_bits(bb, data, sizeof(data)/sizeof(char));
                break;
            case 'C':
                i += read_y4m_tagged_field_bits(bb, data, sizeof(data)/sizeof(char));
                if (fourcc) {
                    if (strncmp(data, "422", 3)==0)
                        *fourcc = I422;
                    else if (strncmp(data, "444", 3)==0)
                        *fourcc = I444;
                }
                break;
            case 'X':
                i += read_y4m_tagged_field_bits(bb, data, sizeof(data)/sizeof(char));
                /* TODO store these for later */
                break;
            default:
                rvmessage("unknown yuv4mpeg tag: %c", c);
                break;
        }
        //rvmessage("%c: %s", c, data);
    }
    i++;

    /* check stream header parsed correctly */
    if (c != '\n') {
        rvmessage("yuv4mpeg stream header did not parse correctly");
        return -1;
    }

    return i;
}

int read_y4m_frame_header_bits(struct bitbuf *bb)
{
    char c, data[16];
    int i = 0;

    /* check for magic string */
    i += readbits(bb, data, 5);
    if (i != 5)
        return -1;
    if (strncmp(data, "FRAME", 5) != 0) {
        rvmessage("failed to find yuv4mpeg frame header magic string: %.5s", data);
        return -1;
    }

    /* read tagged fields */
    while ((c = getbyte(bb)) == ' ') {
        c = getbyte(bb);
        i += 2;
        switch (c)
        {
        case 'X':
            i += read_y4m_tagged_field_bits(bb, data, sizeof(data)/sizeof(char));
            break;
        }
    }
    i++;

    /* check stream header parsed correctly */
    if (c != '\n') {
        rvmessage("yuv4mpeg frame header did not parse correctly");
        return -1;
    }

    return i;
}

int write_y4m_frame_header_bits(struct bitbuf *bb)
{
    putbits(bb, 8, 'F');
    putbits(bb, 8, 'R');
    putbits(bb, 8, 'A');
    putbits(bb, 8, 'M');
    putbits(bb, 8, 'E');
    putbits(bb, 8, '\n');
    return 0;
}
