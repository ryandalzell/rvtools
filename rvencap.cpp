/*
 * Description: Encapsulate an elementary stream into a transport stream.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-06-23 13:26:19 $
 * Revision   : $Revision: 1.25 $
 * Copyright  : (c) 2010 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <math.h>
#include "getopt.h"

#include "rvutil.h"
#include "rvm2v.h"
#include "rvh264.h"
#include "rvmp2.h"
#include "rvcrc.h"

const char *appname;

void usage(int exitcode)
{
    fprintf(stderr, "%s: encapsulate an elementary stream into a transport stream\n", appname);
    fprintf(stderr, "usage: %s [options] [<file>] [<file>...]\n", appname);
    fprintf(stderr, "  -n, --numframes     : number of frames (default: all)\n");
    fprintf(stderr, "  -r, --bitrate       : bitrate of output stream (default: auto)\n");
    fprintf(stderr, "  -f, --framerate     : framerate of output stream (default: same as input)\n");
    fprintf(stderr, "  -o, --output        : write output to file\n");
    fprintf(stderr, "  -q, --quiet         : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "  -v, --verbose       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "  --                  : disable argument processing\n");
    fprintf(stderr, "  -h, --help, --usage : print this usage message\n");
    exit(exitcode);
}


struct rvts {
    /* program and pid definitions */
    int stream_number;
    int program_number;
    int pat_pid;
    int pmt_pid;
    int vid_pid;
    int pcr_pid;

    /* pat and pmt */
    unsigned char *pat_section;
    unsigned char *pmt_section;
    int pat_section_length;
    int pmt_section_length;

    /* continuity counters */
    int pat_continuity_counter;
    int pmt_continuity_counter;
    int vid_continuity_counter;
    int pcr_continuity_counter;

    /* packet counter */
    int packets;

    /* 27MHz system time clock and packet time clock */
    long long stc;
    long long ptc;
    long long frame_interval;

    /* 90kHz decoding clock */
    long long dts;

    /* pat and pmt insertion frequency */
    long long pat_period;
    long long pmt_period;
    long long last_pat;
    long long last_pmt;

    /* pcr insertion frequency */
    long long pcr_period;
    long long last_pcr;

    /* buffer tracking */
    int buffer_level;
    long long packet_time;
#define pic_removal_size 64
    struct {
        long long dts;
        int bytes;
    } pic_removal[pic_removal_size];
    int pic_removal_write;
    int pic_removal_read;
};

void redo_ts_packetiser_bitrate(struct rvts *ts, int bitrate);
void redo_ts_packetiser_framerate(struct rvts *ts, double framerate);

struct rvts *init_ts_packetiser(stream_type_t stream_type, int bitrate, double framerate)
{
    /* allocate packetiser object */
    struct rvts *ts = (struct rvts *)rvalloc(NULL, sizeof(struct rvts), 1);

    /* program and pid definitions TODO paramaterise */
    ts->stream_number = 0;
    ts->program_number = 1;
    ts->pat_pid = 0;
    ts->pmt_pid = 64;
    ts->vid_pid = 312;
    ts->pcr_pid = ts->vid_pid;

    /* prepare a pat and a pmt for transport streams */
    {
        ts->pat_section = (unsigned char *)rvalloc(NULL, 1024, 0);
        ts->pmt_section = (unsigned char *)rvalloc(NULL, 1024, 0);

        struct bitbuf *patbuf = initbits_memory(ts->pat_section, 1024);
        struct bitbuf *pmtbuf = initbits_memory(ts->pmt_section, 1024);

        /* prepare pat */
        int pat[1][2] = {{ts->program_number, ts->pmt_pid}};
        ts->pat_section_length = put_program_association_section(patbuf, 1, pat);

        /* prepare pmt */
        int pmt[1][2] = {{stream_type, ts->vid_pid}};
        ts->pmt_section_length = put_program_map_section(pmtbuf, ts->program_number, ts->pcr_pid, 1, pmt);

        /* tidy up */
        freebits(patbuf);
        freebits(pmtbuf);
    }

    /* continuity counters */
    ts->pat_continuity_counter = 0;
    ts->pmt_continuity_counter = 0;
    ts->vid_continuity_counter = 0;
    ts->pcr_continuity_counter = 0;

    /* packet counter */
    ts->packets = 0;

    /* initialise 27MHz system time clock and packet time clock */
    ts->stc = 0;
    ts->ptc = ts->stc;
    redo_ts_packetiser_framerate(ts, framerate);

    /* initialise 90kHz decoding clock */
    ts->dts = 0;

    /* pat and pmt insertion frequency */
    ts->pat_period = 400 * 27000; /* 400 ms */
    ts->pmt_period = 400 * 27000; /* 400 ms */
    ts->last_pat = ts->stc - ts->pat_period;
    ts->last_pmt = ts->stc - ts->pmt_period;

    /* pcr insertion frequency */
    ts->pcr_period = 25 * 27000; /* 25 ms */
    ts->last_pcr = ts->stc - ts->pcr_period;

    /* initialise buffer tracking */
    ts->buffer_level = 0;
    redo_ts_packetiser_bitrate(ts, bitrate);
    ts->pic_removal_write = 0;
    ts->pic_removal_read = 0;

    return ts;
}

void redo_ts_packetiser_bitrate(struct rvts *ts, int bitrate)
{
    if (bitrate)
        ts->packet_time = 27000000ll * 188ll * 8ll / (long long)bitrate;
    else
        ts->packet_time = 0;
}

void redo_ts_packetiser_framerate(struct rvts *ts, double framerate)
{
    if (framerate)
        ts->frame_interval = llround(27000000.0 / framerate);
    else
        ts->frame_interval = 0;
}

void free_ts_packetiser(struct rvts *ts)
{
    rvfree(ts);
}

void update_ts_packetiser_frame(struct rvts *ts)
{
    /* increment system time clock */
    ts->stc += ts->frame_interval;
}

int run_ts_packetiser(struct rvts *ts, struct bitbuf *ob, unsigned char *data, size_t size, long long pts, long long dts)
{
    int pointer = 0;

    /* remember the dts for removal from the buffer */
    ts->pic_removal[ts->pic_removal_write].dts = dts;
    ts->pic_removal[ts->pic_removal_write].bytes = size;
    ts->pic_removal_write = (ts->pic_removal_write+1)%pic_removal_size;

    /* loop over ts packets in data chunk provided */
    int ts_packets;
    for (ts_packets=0; pointer<size; ) {

        /* decide which packet to send next */
        int num_packets = 1;

        /* pcr packet has the highest priority */
        if (ts->ptc >= ts->last_pcr+ts->pcr_period) {
            if (ts->pcr_pid != ts->vid_pid) {
                /* special pid for pcr */
                put_ts_packet_pcr(ob, ts->pcr_pid, ts->pcr_continuity_counter, ts->ptc);
                /* no continuity increment in pcr-only packets */
            } else {
                /* pcr in video packets */
                if (pointer==0)
                    pointer += put_ts_packet_pes_start(ob, ts->vid_pid, ts->vid_continuity_counter++, 1, ts->ptc, ts->stream_number, pts, dts, data+pointer, size-pointer);
                else
                    pointer += put_ts_packet_pes_data(ob, ts->vid_pid, ts->vid_continuity_counter++, 1, ts->ptc, data+pointer, size-pointer);
            }
            ts->last_pcr = ts->ptc;
        }

        /* insert the pat and pmt at regular intervals */
        else if (ts->ptc >= ts->last_pat+ts->pat_period) {
            num_packets = put_section(ob, ts->pat_pid, ts->pat_continuity_counter, ts->pat_section, ts->pat_section_length);
            ts->pat_continuity_counter += num_packets;
            ts->last_pat = ts->ptc;
        }
        else if (ts->ptc >= ts->last_pmt+ts->pmt_period) {
            num_packets = put_section(ob, ts->pmt_pid, ts->pmt_continuity_counter, ts->pmt_section, ts->pmt_section_length);
            ts->pmt_continuity_counter += num_packets;
            ts->last_pmt = ts->ptc;
        }

        /* insert padding packets to keep the bitrate constant */
        else if (ts->ptc < ts->stc) {
            put_ts_packet_null(ob);
        }

        /* insert video data pes packet */
        else {

            /* sanity check */
            if (pointer==0) {
                if (pts*300ll<ts->ptc)
                    rvmessage("packet %5d: pts is in the past: pts=%.1fms pcr=%.1fms (%.1fms)", ts->packets, pts_to_ms(pts), pcr_to_ms(ts->ptc), pcr_to_ms(pts*300ll-ts->ptc));
                if (dts*300ll<ts->ptc)
                    rvmessage("packet %5d: dts is in the past: dts=%.1fms pcr=%.1fms (%.1fms)", ts->packets, pts_to_ms(dts), pcr_to_ms(ts->ptc), pcr_to_ms(dts*300ll-ts->ptc));
            }

            if (pointer==0)
                pointer += put_ts_packet_pes_start(ob, ts->vid_pid, ts->vid_continuity_counter++, 0, 0ll, ts->stream_number, pts, dts, data+pointer, size-pointer);
            else
                pointer += put_ts_packet_pes_data(ob, ts->vid_pid, ts->vid_continuity_counter++, 0, 0ll, data+pointer, size-pointer);
        }

        /* update buffer tracking */
        ts->buffer_level += 188*num_packets;
        if (ts->ptc >= ts->pic_removal[ts->pic_removal_read].dts) {
            ts->buffer_level -= ts->pic_removal[ts->pic_removal_read].bytes;
            ts->pic_removal_read = (ts->pic_removal_read+1)%pic_removal_size;
            //rvmessage("buffer_level=%d", buffer_level);
        }

        /* update statistics */
        ts_packets += num_packets;
        ts->packets += num_packets;
        ts->ptc += ts->packet_time*num_packets;
    }

    return ts_packets;
}

size_t read_m2v_frame(struct bitbuf *bb, unsigned char **data, size_t *size)
{
    int slice_data = 0;
    size_t framesize = 0;
    while (!eofbits(bb)) {
        int start = showbits32(bb);

        /* look for a start code that indicates end of frame */
        if (slice_data && (start==0x100 || start==0x1B3)) {
            break;
        }

        /* look for a start code that indicates start of slice data */
        if (start>=0x100 && start<=0x1AF)
            slice_data = 1;

        /* dynamically grow buffer */
        if (framesize==*size) {
            *size += 4096;
            *data = (unsigned char *)rvalloc(*data, *size, 0);
        }

        (*data)[framesize++] = getbyte(bb);
    }
    return framesize;
}

size_t read_264_access_unit(struct bitbuf *bb, unsigned char **data, size_t *size, int verbose)
{
    int access_unit = 0;
    int slice_data = 0;
    int num_units = 0;
    size_t framesize = 0;

    while (!eofbits(bb) && !access_unit) {
        int nal_unit_type, zero_byte_present = 0;
        if (showbits24(bb)==0x1 || showbits32(bb)==0x1) {
            if (showbits24(bb)!=0x1) {
                flushbits(bb, 8);                       /* zero_byte */
                zero_byte_present = 1;
            }
            nal_unit_type = showbits32(bb) & 0x1f;
        } else {
            flushbits(bb, 8);                           /* leading_zero_8bits */
            continue;
        }

        if (verbose>=2)
            rvmessage("info: nal: unit_type=%s", nal_unit_type_code(nal_unit_type));

        /* decide what to do based on nal unit type */
        switch (nal_unit_type) {
            case NAL_AUD :
            case NAL_EOQ :
            case NAL_EOS :
                /* last nal unit in access unit */
                if (slice_data)
                    access_unit = 1;
                break;

            case NAL_CODED_SLICE_NON_IDR :
            case NAL_CODED_SLICE_PARTITION_A :
            case NAL_CODED_SLICE_PARTITION_B :
            case NAL_CODED_SLICE_PARTITION_C :
            case NAL_CODED_SLICE_IDR :
            case NAL_CODED_AUX :
            case NAL_CODED_EXT :
                if (slice_data && zero_byte_present)
                    /* first nal unit in next access unit */
                    access_unit = -1;
                /* nal unit that indicates start of slice data */
                slice_data = 1;
                break;

            case NAL_SEI :
            case NAL_SPS :
            case NAL_PPS :
            case NAL_FILLER :
            case NAL_SPS_EXT :
            case NAL_PREFIX :
            case NAL_SUBSET_SPS :
                if (slice_data)
                    access_unit = -1;
                /* nal unit is part of current access unit */
                break;
        }

        if (access_unit==-1)
            break;

        /* read nal unit */
        do {
            /* dynamically grow buffer */
            if (framesize==*size) {
                *size += 4096;
                *data = (unsigned char *)rvalloc(*data, *size, 0);
            }

            (*data)[framesize++] = getbyte(bb);

        } while (showbits24(bb)!=0x1 && showbits32(bb)!=0x1 && !eofbits(bb));
        num_units++;
    }

    //rvmessage("info: access unit contains %d nal units and %d bytes", num_units, framesize);

    return framesize;
}

int main(int argc, char *argv[])
{
    FILE *filein = stdin;
    struct bitbuf *ob = NULL;
    char *filename[1] = {0};
    const char *outfile = "-";
    int fileindex = 0;
    int numfiles = 0;
    int totframes = 0;
    int pespackets = 0;
    int ts_packets = 0;

    /* data buffer */
    size_t size = 4096;
    unsigned char *data = NULL;

    /* output packetiser */
    filetype_t filetype = TS;
    struct rvts *ts = NULL;

    /* command line defaults */
    const char *format = "ts";
    int bitrate = 0;
    float framerate = 0.0f;
    int numframes = -1;
    int stream_number = 0;
    int verbose = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"numframes", 1, NULL, 'n'},
            {"bitrate",   1, NULL, 'r'},
            {"framerate", 1, NULL, 'f'},
            {"output",    1, NULL, 'o'},
            {"quiet",     0, NULL, 'q'},
            {"verbose",   0, NULL, 'v'},
            {"usage",     0, NULL, 'h'},
            {"help",      0, NULL, 'h'},
            {NULL,        0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "n:r:f:o:qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 'n':
                numframes = atoi(optarg);
                if (numframes<=0)
                    rvexit("invalid value for numframes: %d", numframes);
                break;

            case 'r':
                bitrate = atoi(optarg);
                if (bitrate<0)
                    rvexit("invalid value for bitrate: %d", bitrate);
                break;

            case 'f':
                framerate = atof(optarg);
                if (bitrate<0)
                    rvexit("invalid value for framerate: %.2f", framerate);
                break;

            case 'o':
                outfile = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < sizeof(filename)/sizeof(filename[0]))
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    /* open output file */
    ob = initbits_filename(outfile, B_PUTBITS);
    if (ob==NULL)
        rverror("failed to open output file \"%s\"", outfile);

    /* ignore SIGPIPE on output file */
    if (signal(SIGPIPE, SIG_IGN)==SIG_ERR)
        rverror("failed to set ignore on SIGPIPE");

    /* allocate data buffer */
    data = (unsigned char *)rvalloc(NULL, size*sizeof(unsigned char), 0);

    /* loop over input files */
    int frameno = 0;
    do {
        if (numfiles)
            filein = fopen(filename[fileindex], "rb");
        if (filein==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex]);

        /* open input file */
        struct bitbuf *bb = initbits_filename(filename[fileindex], B_GETBITS);
        if (bb==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex]);

        /* determine input file type */
        divine_t divine = divine_filetype(bb, filename[fileindex], NULL, verbose);

        switch (divine.filetype) {
            case M2V:
            {
                struct rvm2vsequence *sequence = NULL;
                struct rvm2vpicture  *picture = NULL;

                /* find sequence header */
                int pos = numbits(bb);
                while (!eofbits(bb)) {
                    int start = showbits32(bb);
                    /* look for a sequence start code */
                    if (start==0x1B3)
                        break;
                    /* move along */
                    flushbits(bb, 8);
                }
                if (eofbits(bb))
                    rvexit("failed to find sequence header in m2v sequence \"%s\"", filename[fileindex]);
                if (verbose>=1 && numbits(bb)-pos>7)
                    rvmessage("%jd bits of leading garbage before sequence start code", numbits(bb)-pos);

                /* read sequence header and first frame */
                int framesize = read_m2v_frame(bb, &data, &size);
                if (framesize==0)
                    rvexit("failed to read first frame in m2v sequence \"%s\"", filename[fileindex]);

                /* decode sequence header */
                {
                    struct bitbuf *fb = initbits_memory(data, framesize);

                    if (showbits32(fb)==0x1B3) {
                        sequence = parse_m2v_headers(fb, 0);
                    }

                    if (showbits32(fb)==0x1B8) {
                        flushbits(fb, 32);
                        parse_m2v_group_of_pictures(fb, NULL);
                        alignbits(fb);
                    }

                    if (showbits32(fb)==0x100) {
                        picture = parse_m2v_picture_header_and_extensions(fb, sequence, frameno, verbose);
                    }

                    freebits(fb);
                }

                if (verbose>=1)
                    rvmessage("info: elementary stream bitrate is %.2fMbps, frame rate is %.2fps", sequence->bit_rate*400.0/1e6, sequence->frame_rate);

                /* initialise the output packetiser */
                if (strcasecmp(format, "null")==0) {
                    filetype = SKIP;
                }
                else if (strcasecmp(format, "pes")==0) {
                    filetype = PES;
                }
                else if (strcasecmp(format, "ts")==0) {
                    filetype = TS;

                    ts = init_ts_packetiser(STREAM_TYPE_VIDEO_MPEG2, bitrate, 0);
                }
                else
                    rvexit("unknown output data format: %s", format);

                /* initialise the bitrate */
                if (bitrate==0) {
                    /* set transport stream rate to 5% greater than elementary stream rate */
                    bitrate = sequence->bit_rate*400 * 21/20;
                    redo_ts_packetiser_bitrate(ts, bitrate);
                    if (verbose>=1)
                        rvmessage("info: transport stream bitrate is %.2fMbps, packet time is %.1fus", bitrate/1000000.0, ts->packet_time/27.0);
                }

                /* initialise the framerate */
                if (framerate==0.0f) {
                    framerate = sequence->frame_rate;
                    redo_ts_packetiser_framerate(ts, framerate);
                }

                /* initialise pts and dts FIXME fix the assumption of M */
                const int M = 3;
                long long dts = ts->ptc/300ll + picture->vbv_delay;

                /* loop over frames in input */
                for (frameno=0; (totframes<numframes || numframes<0) && !eofbits(bb) && !errorbits(bb) && !errorbits(ob); frameno++)
                {

                    /* derive the 90kHz pts */
                    long long pts = dts; /* default for low_delay and B-pictures */
                    if (sequence && picture)
                        if (!sequence->low_delay)
                            if (picture->picture_coding_type==PCT_I || picture->picture_coding_type==PCT_P)
                                pts = dts + M * 90000.0f / framerate;

                    /* choose what to do with the data */
                    if (filetype==SKIP) {
                        int i;
                        for (i=0; i<framesize; i++)
                            putbits(ob, 8, data[i]);
                    }
                    else if (filetype==PES) {
                        if (verbose>=2)
                            rvmessage("frame %d: pes packet %4d: %d bytes", frameno, pespackets, framesize);

                        /* first pes packet of frame */
                        put_pes_packet(ob, VIDEO_STREAM_X + stream_number, 1, pts, dts, data, framesize);

                        pespackets++;
                    }
                    else if (filetype==TS) {
                        if (verbose>=2)
                            rvmessage("frame %d: pes packet %4d: %d bytes dts=%.1f pts=%.1f", frameno, pespackets, framesize, pts_to_ms(dts), pts_to_ms(pts));

                        /* add the frame to the transport stream as a single pes */
                        ts_packets += run_ts_packetiser(ts, ob, data, framesize, pts, dts);
                        update_ts_packetiser_frame(ts);

                        //rvmessage("ptc=%.1f stc=%.1f", pcr_to_ms(ts->ptc), pcr_to_ms(ts->stc));

                        pespackets++;
                    }
                    else
                        rvexit("unknown output data format: %s", format);

                    totframes++;

                    /* read next frame */
                    framesize = read_m2v_frame(bb, &data, &size);
                    if (framesize==0)
                        break;

                    /* decode frame headers to find picture type */
                    {
                        struct bitbuf *fb = initbits_memory(data, framesize);

                        if (showbits32(fb)==0x1B3) {
                            if (sequence)
                                free_m2v_sequence(sequence);
                            sequence = parse_m2v_headers(fb, 0);
                        }

                        if (showbits32(fb)==0x1B8) {
                            flushbits(fb, 32);
                            parse_m2v_group_of_pictures(fb, NULL);
                            alignbits(fb);
                        }

                        if (showbits32(fb)==0x100) {
                            if (sequence) {
                                if (picture)
                                    free_m2v_picture(picture);
                                picture = parse_m2v_picture_header_and_extensions(fb, sequence, frameno, verbose);
                            }
                        }

                        freebits(fb);
                    }

                    /* increment dts */
                    dts += 90000.0 / framerate;
                }
                break;
            }

            case H264:
            {
                struct rv264sequence *sequence = NULL;
                struct rv264picture  *picture = NULL;
                struct rv264seq_parameter_set *sps = NULL;

                /* find first sequence parameter set nal */
                int zerobyte, pos = numbits(bb);
                while (!eofbits(bb)) {
                    int nal_unit_type = find_next_nal_unit(bb, &zerobyte);
                    /* look for sps nal unit type */
                    if (nal_unit_type==7)
                        break;
                    /* move along */
                    flushbits(bb, 8);
                }
                if (eofbits(bb))
                    rvexit("failed to find sequence parameter set nal in h264 sequence \"%s\"", filename[fileindex]);
                if (verbose>=1 && numbits(bb)-pos>7+zerobyte*8)
                    rvmessage("%jd bits of leading garbage before sequence parameter set nal unit (zero_byte %spresent)", numbits(bb)-pos, zerobyte? "" : "not ");

                /* read sequence header and first frame */
                int framesize = read_264_access_unit(bb, &data, &size, verbose);
                if (framesize==0)
                    rvexit("failed to read first frame in m2v sequence \"%s\"", filename[fileindex]);

                /* decode sequence header */
                {
                    struct bitbuf *fb = initbits_memory(data, framesize);

                    sequence = parse_264_params(fb, verbose);
                    if (sequence==NULL) {
                        rvmessage("failed to find sequence parameters in h264 sequence \"%s\"", filename[fileindex]);
                        break;
                    }
                    sps = sequence->sps[sequence->active_sps];

                    if (!eofbits(fb))
                        picture = parse_264_picture_headers(fb, sequence, verbose);

                    freebits(fb);
                }

                /* calculate elementary stream bitrate and framerate */
                int es_bitrate = (sps->vui.nal_hrd_parameters.bit_rate_value_minus1[0]+1) * (1<<(sps->vui.nal_hrd_parameters.bit_rate_scale+6));
                float es_framerate = (float)sps->vui.time_scale / (2.0*(float)sps->vui.num_units_in_tick);
                if (verbose>=1)
                    rvmessage("info: elementary stream bitrate is %.2fMbps, frame rate is %.2ffps", es_bitrate/1000000.0, es_framerate);

                /* initialise the bitrate */
                if (bitrate==0) {
                    /* set transport stream rate to 20% greater than elementary stream rate */
                    bitrate = es_bitrate * 5/4;
                }

                /* initialise the framerate */
                if (framerate==0.0f) {
                    framerate = es_framerate;
                }

                if (bitrate==0) {
                    rvmessage("error: need to specify transport stream bitrate as a bitrate is not encoded in elementary stream \"%s\"", filename[fileindex]);
                    break;
                }
                if (framerate==0.0f) {
                    rvmessage("error: need to specify a framerate as it is not encoded in elementary stream \"%s\"", filename[fileindex]);
                    break;
                }

                /* initialise the output packetiser */
                if (strcasecmp(format, "null")==0) {
                    filetype = SKIP;
                }
                else if (strcasecmp(format, "pes")==0) {
                    filetype = PES;
                }
                else if (strcasecmp(format, "ts")==0) {
                    filetype = TS;

                    ts = init_ts_packetiser(STREAM_TYPE_VIDEO_H264, bitrate, framerate);
                    if (verbose>=1)
                        rvmessage("info: transport stream bitrate is %.2fMbps, packet time is %.1fus", bitrate/1000000.0, ts->packet_time/27.0);
                }
                else
                    rvexit("unknown output data format: %s", format);

                /* initialise pts and dts FIXME fix the assumption of M */
                const int M = 3;
                long long dts = ts->ptc/300ll;
                long long initial_cpb_removal_delay;
                if (sps->vui_parameters_present_flag && sps->vui.nal_hrd_parameters_present_flag) {
                    /* use initial buffer delay from hrd parameters */
                    initial_cpb_removal_delay = sps->vui.nal_hrd_parameters.initial_cpb_removal_delay[0];
                } else
                    /* use an estimated initial buffer delay */
                    initial_cpb_removal_delay = 90000ll*8ll/10ll;
                dts += initial_cpb_removal_delay;
                if (verbose>=1)
                    rvmessage("info: initial buffer delay is %lldms", initial_cpb_removal_delay/90);

                /* loop over frames in input */
                for (frameno=0; (totframes<numframes || numframes<0) && !eofbits(bb) && !errorbits(bb) && !errorbits(ob); frameno++)
                {

                    /* derive the 90kHz pts */
                    long long pts = dts; /* default for low_delay and B-pictures */
                    if (sequence && picture)
                        //if (!sequence->low_delay)
                            if (slice_is_i(picture->sh[0]->slice_type) || slice_is_p(picture->sh[0]->slice_type))
                                pts = dts + M * 90000.0f / framerate;

                    /* choose what to do with the data */
                    if (strcasecmp(format, "null")==0) {
                        int i;
                        for (i=0; i<framesize; i++)
                            putbits(ob, 8, data[i]);
                    }
                    else if (strcasecmp(format, "pes")==0) {
                        if (verbose>=2)
                            rvmessage("frame %d: pes packet %4d: %d bytes", frameno, pespackets, framesize);

                        /* first pes packet of frame */
                        put_pes_packet(ob, VIDEO_STREAM_X + stream_number, 1, pts, dts, data, framesize);

                        pespackets++;
                    }
                    else if (strcasecmp(format, "ts")==0) {
                        if (verbose>=2)
                            rvmessage("frame %d: pes packet %4d: %d bytes dts=%.1f pts=%.1f", frameno, pespackets, framesize, pts_to_ms(dts), pts_to_ms(pts));

                        /* add the frame to the transport stream as a single pes */
                        ts_packets += run_ts_packetiser(ts, ob, data, framesize, pts, dts);
                        update_ts_packetiser_frame(ts);

                        //rvmessage("ptc=%.1f stc=%.1f", pcr_to_ms(ts->ptc), pcr_to_ms(ts->stc));

                        pespackets++;
                    }
                    else
                        rvexit("unknown output data format: %s", format);

                    totframes++;

                    /* read next frame */
                    framesize = read_264_access_unit(bb, &data, &size, verbose);
                    if (framesize==0)
                        break;

                    /* decode frame headers to find picture type */
                    {
                        struct bitbuf *fb = initbits_memory(data, framesize);

                        picture = parse_264_picture_headers(fb, sequence, verbose);

                        freebits(fb);
                    }

                    /* increment dts */
                    dts += 90000.0 / framerate;
                }
                break;
            }

            case EMPTY:
                rvexit("empty file: \"%s\"", filename[fileindex]);
                break;

            default:
                rvmessage("filetype of file \"%s\" is not supported: %s", filename[fileindex], filetypename[divine.filetype]);
                break;
        }
        if (ferror(filein))
            rverror("failed to read from input");
        if (errorbits(ob) && errno!=EPIPE)
            rverror("failed to write to output");

        if (numfiles)
            fclose(filein);

    } while (++fileindex<numfiles && !errorbits(ob));

    if (verbose>=0)
        rvmessage("processed %d frames, %d pes packets, %d ts packets", totframes, pespackets, ts_packets);

    /* tidy up */
    if (ts)
        free_ts_packetiser(ts);
    rvfree(data);

    return 0;
}
