/*
 * Description: Display information about raw and compressed video files.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2010-01-22 15:52:40 $
 * Revision   : $Revision: 1.137 $
 * Copyright  : (c) 2005 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <math.h>
#include "getopt.h"
#include <assert.h>

#include "rvutil.h"
#include "rvbits.h"
#include "rvheader.h"
#include "rvy4m.h"
#include "rvendian.h"

#include "rvh261.h"
#include "rvm2v.h"
#include "rvmp2.h"
#include "rvh264.h"
#include "rvhevc.h"
#include "rvvc1.h"
#include "rvasf.h"
#include "rvm2a.h"

/* approx. meaning of each flag (really, it's standard dependent) */
#define F_FRAME   0
#define F_LACED   1
#define F_ERROR   2
#define F_REFER   3
#define F_MOTION  4
#define F_QUANT   5
#define F_ENTROPY 6
#define F_CHROMA  7
#define F_TIMECODE 8
#define F_CAPTION 9
#define F_OTHER   10

/* per-file video information */
struct videoinfo {
    char flags[12];
    int width;
    int height;
    int pixels;
    int lines;
    framerate_t framerate;
    const char *type;
    const char *subtype;
    int frames;
    int pictures;
    int slices;
};

/* per-file audio information */
struct audioinfo {
    char flags[12];
    int  samples;
    int  bits;
    int  bitrate;
    const char *type;
    const char *subtype;
    int  channels;
    int  lfe;
};

/* per-file container information */
struct muxerinfo {
    char flags[12];
    int vstreams;
    stream_type_t vstream_type;
    fourcc_t vstream_format_identifier;
    int astreams;
    stream_type_t astream_type;
    fourcc_t astream_format_identifier;
    const char *type;
    const char *subtype;
    int bitrate;
    int packets;
};

/* videoblank videoinfo */
const struct videoinfo videoblank = {
    "-----------",
    0,
    0,
    0,
    0,
    0.0,
    "",
    "",
    0,
    0
};

/* audioblank audioinfo */
const struct audioinfo audioblank = {
    "-----------",
    0,
    0,
    0,
    "",
    "",
    0,
    0
};

/* muxerblank muxerinfo */
const struct muxerinfo muxerblank = {
    "-----------",
    0,
    STREAM_TYPE_RESERVED,
    0x0,
    0,
    STREAM_TYPE_RESERVED,
    0x0,
    "",
    "",
    0
};
/* videoblank subtype */
#define BLANK_BYTES 13
char space[BLANK_BYTES];

/* per-frame information */
enum frame_type_t {
    I_FRAME = 1,
    P_FRAME,
    B_FRAME,
    D_FRAME,
    S_FRAME,
    /* h.264 frame types */
    IDR_FRAME, /* IDR Picture */
    /* hevc frame types */
    CRA_FRAME,
};

enum field_type_t {
    FRAME = 1,
    TOP_FIELD,
    BOT_FIELD,
};

struct timecode {
    char present;
    char frames;
    char seconds;
    char minutes;
    char hours;
};

/* per-picture information */
struct frameinfo {
    int start_gop;
    enum frame_type_t frame_type;
    enum field_type_t field_type;
    unsigned int position;
    int order;
    int bytes;
    int stuffbytes;
    int slices;
    int delay;
    int reference;
    struct timecode timecode;
};

/* per-slice information */
struct sliceinfo {
    int bytes;
    int first_mb_in_slice;
    slice_type_t slice_type;
    int order;
};

/* timing information */
struct timinginfo {
    long long stc;  /* system time clock */
    long long pts;  /* presentation time stamp */
    long long dts;  /* decoding time stamp */
};

/* the "default" jpeg huffman tables */
static const struct huffman_table annexk[2][2] =
{
    {
        {
            { 0, 1, 5, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0 },
            { {0}, {0}, {1,2,3,4,5}, {6}, {7}, {8}, {9}, {10}, {11}, {0},  {0},  {0},  {0},  {0},  {0},  {0}, }
        },
        {
            { 0, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0 },
            { {0}, {0,1,2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {0},  {0},  {0},  {0},  {0}, }
        }
    },
    {
        {
            { 0, 2, 1, 3, 3, 2, 4, 3, 5, 5, 4, 4, 0, 0, 1, 125 },
            {
                {0}, {1,2}, {3},
                {0x00,0x04,0x11},
                {0x05,0x12,0x21},
                {0x31,0x41},
                {0x06,0x13,0x51,0x61},
                {0x07,0x22,0x71},
                {0x14,0x32,0x81,0x91,0xa1},
                {0x08,0x23,0x42,0xb1,0xc1},
                {0x15,0x52,0xd1,0xf0},
                {0x24,0x33,0x62,0x72},
                {0}, {0}, {0x82},
                {0x09,0x0a,0x16,0x17,0x18,0x19,0x1a,0x25,0x26,0x27,0x28,0x29,0x2a,0x34,
                0x35,0x36,0x37,0x38,0x39,0x3a,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4a,0x53,0x54,0x55,0x56,0x57,0x58,
                0x59,0x5a,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6a,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7a,0x83,0x84,
                0x85,0x86,0x87,0x88,0x89,0x8a,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9a,0xa2,0xa3,0xa4,0xa5,0xa6,
                0xa7,0xa8,0xa9,0xaa,0xb2,0xb3,0xb4,0xb5,0xb6,0xb7,0xb8,0xb9,0xba,0xc2,0xc3,0xc4,0xc5,0xc6,0xc7,0xc8,
                0xc9,0xca,0xd2,0xd3,0xd4,0xd5,0xd6,0xd7,0xd8,0xd9,0xda,0xe1,0xe2,0xe3,0xe4,0xe5,0xe6,0xe7,0xe8,0xe9,
                0xea,0xf1,0xf2,0xf3,0xf4,0xf5,0xf6,0xf7,0xf8,0xf9,0xfa},
            }
        },
        {
            { 0, 2, 1, 2, 4, 4, 3, 4, 7, 5, 4, 4, 0, 1, 2, 119 },
            {
                {0}, {0,1}, {2},
                {0x03,0x11},
                {0x04,0x05,0x21,0x31},
                {0x06,0x12,0x41,0x51},
                {0x07,0x61,0x71},
                {0x13,0x22,0x32,0x81},
                {0x08,0x14,0x42,0x91,0xa1,0xb1,0xc1},
                {0x09,0x23,0x33,0x52,0xf0},
                {0x15,0x62,0x72,0xd1},
                {0x0a,0x16,0x24,0x34},
                {0}, {0xe1},
                {0x25,0xf1},
                {0x17,0x18,0x19,0x1a,0x26,0x27,0x28,0x29,0x2a,0x35,0x36,0x37,0x38,0x39,
                0x3a,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4a,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5a,0x63,0x64,0x65,
                0x66,0x67,0x68,0x69,0x6a,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7a,0x82,0x83,0x84,0x85,0x86,0x87,0x88,
                0x89,0x8a,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9a,0xa2,0xa3,0xa4,0xa5,0xa6,0xa7,0xa8,0xa9,0xaa,
                0xb2,0xb3,0xb4,0xb5,0xb6,0xb7,0xb8,0xb9,0xba,0xc2,0xc3,0xc4,0xc5,0xc6,0xc7,0xc8,0xc9,0xca,0xd2,0xd3,
                0xd4,0xd5,0xd6,0xd7,0xd8,0xd9,0xda,0xe2,0xe3,0xe4,0xe5,0xe6,0xe7,0xe8,0xe9,0xea,0xf2,0xf3,0xf4,0xf5,
                0xf6,0xf7,0xf8,0xf9,0xfa},
            }
        }
    }
};

const char *appname;

void usage(int exitcode)
{
    fprintf(stderr, "%s: print detailed information on codec related files\n", appname);
    fprintf(stderr, "usage: %s [options] [<file>] [<file>...]\n", appname);
    fprintf(stderr, "  -t, --type          : force type of file (default: autodetect)\n");
    fprintf(stderr, "  -f, --fps           : framerate, for bitrate calculation (default: 30.0)\n");
    fprintf(stderr, "  -b, --basename      : use basename of file in listing\n");
    fprintf(stderr, "  -r, --raw           : use raw numbers in listing (default: human readable)\n");
    fprintf(stderr, "  -d, --description   : display description of flags\n");
    fprintf(stderr, "  -p, --picture       : print information about each frame\n");
    fprintf(stderr, "  -s, --slice         : print information about each slice\n");
    fprintf(stderr, "  -g, --gop-structure : print brief gop structure\n");
    fprintf(stderr, "  -q, --quiet         : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "  -v, --verbose       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "  --                  : disable argument processing\n");
    fprintf(stderr, "  -h, --help, --usage : print this usage message\n");
    exit(exitcode);
}

/* called after finding the start of the next picture start to calculate size of previous picture */
static void calc_framesize(int index, struct frameinfo *finfo, int num_elements, int bytes)
{
    static int prevbytes;
    if (index==1) {
        finfo[index-1].bytes = bytes; /* includes picture header etc. */
        prevbytes = bytes;
    } else if (index>1 && index<=num_elements) {
        finfo[index-1].bytes = bytes - prevbytes;
        prevbytes = bytes;
    }
}

static int comp_matrix(const int *a, const int *b)
{
    int i;
    for (i=0; i<64; i++)
        if (a[i]!=b[i])
            return -1;
    return 0;
}

static void dump_matrix(const char *filename, const char *where, int frameno, const int *matrix, int *shadow)
{
    int i;
    if (comp_matrix(matrix, shadow)==0)
        return;
    memcpy(shadow, matrix, 64*sizeof(int));
    rvmessage("%s: %s: frame %d:", filename, where, frameno);
    for (i=0; i<8; i++)
        rvmessage("%2d %2d %2d %2d %2d %2d %2d %2d",
                    matrix[8*i+0], matrix[8*i+1], matrix[8*i+2], matrix[8*i+3],
                    matrix[8*i+4], matrix[8*i+5], matrix[8*i+6], matrix[8*i+7]);
}

struct videoinfo grok_yuv(struct bitbuf *bb, const char *filename, const char *format, struct stat *st)
{
    struct videoinfo info = videoblank;

    /* get height and width */
    int width = 0, height = 0;
    framerate_t framerate = 0.0;
    lookup_format(format? format : get_extension(filename), &width, &height, NULL, &framerate);

    /* get subsampling */
    int hsub, vsub;
    fourcc_t fourcc = divine_pixel_format(filename);
    if (fourcc==0)
        fourcc = I420;
    get_chromasub(fourcc, &hsub, &vsub);

    /* file information */
    info.type = "yuv";
    info.subtype = (char *) fourccname(fourcc);
    info.width = width;
    info.height = height;
    info.pixels = width;
    info.lines = height;
    info.framerate = framerate;
    if (width && height)
        info.frames = st->st_size / get_framesize(width, height, hsub, vsub, 8);
    return info;
}

struct videoinfo grok_bmp(struct bitbuf *bb, const char *filename)
{
    struct videoinfo info = videoblank;

    /* default file information */
    info.type = "bmp";
    info.subtype = space;

    /* read bitmap file header */
    int width, height, bits, compression;
    if (parse_bmp(bb, &width, &height, &bits, &compression)<0)
        return videoblank;

    /* file information */
    const char *compression_name[3] = { "", "RLE-8", "RLE-4" };
    snprintf(space, BLANK_BYTES, "%dbpp %s", bits, compression_name[compression]);
    info.width = width;
    info.height = height;
    info.pixels = width;
    info.lines = height;
    info.framerate = 1.0;
    info.frames = 1;
    info.pictures = 1;

    return info;
}

struct videoinfo grok_sgi(struct bitbuf *bb, const char *filename)
{
    struct videoinfo info = videoblank;

    /* default file information */
    info.type = "sgi";
    info.subtype = space;

    /* read sgi image file header */
    int width, height, channels, rle, bpc;
    if (parse_sgi(bb, &width, &height, &channels, &rle, &bpc)<0)
        return videoblank;

    /* file information */
    snprintf(space, BLANK_BYTES, "%dbpp %s", bpc*8, rle? "RLE" : "");
    info.width = width;
    info.height = height;
    info.pixels = width;
    info.lines = height;
    info.framerate = 1.0;
    info.frames = 1;
    info.pictures = 1;

    return info;
}

struct muxerinfo grok_ts(struct bitbuf *bb, const char *filename, int verbose)
{
    int i, j;
    struct muxerinfo info = muxerblank;

    /* default file information */
    info.type = "ts";
    info.subtype = space;

    /* initialise program information tables */
    struct program_association_table *pat = (struct program_association_table *)rvalloc(NULL, sizeof(struct program_association_table), 1);
    struct program_map_table *pmt[MAX_PMTS] = {NULL};
    pat->version_displayed = -1;

    /* initialise section buffers */
    /* buffers include space for an extra transport packet of packet stuffing bytes */
    u_int8_t pat_section[SECTION_BUFFER_SIZE];
    u_int8_t *pmt_section[MAX_PMTS] = {NULL};
    int pat_pointer = 0, pmt_pointer[MAX_PMTS];

    /* buffer for list of unique_pids pids */
    int unique_pids[128];
    int num_unique_pids = 0;

    /* initialise bitrate measure */
    int pcr_pid = 0;
    long long first_pcr[8192];
    long long last_pcr[8192];
    int pcr_wraparound[8192];
    int first_pcr_packet[8192];
    int last_pcr_packet[8192];
    memset(first_pcr, -1, sizeof(first_pcr));
    memset(last_pcr, -1, sizeof(last_pcr));
    memset(pcr_wraparound, 0, sizeof(pcr_wraparound));
    memset(first_pcr_packet, 0, sizeof(first_pcr_packet));
    memset(last_pcr_packet, 0, sizeof(last_pcr_packet));

    /* initialise pid statistics */
    int pid_count[8192];
    memset(pid_count, 0, sizeof(pid_count));

    /* loop over transport packets */
    u_int8_t packet[188];
    do {

        /* read the next complete transport stream packet */
        if (read_transport_packet(bb, verbose, packet)<0)
            break;

        /* check for synchronisation */
        //if (showbits(bb, 8)!=0x47 && !eofbits(bb)) {
        //    rvmessage("%s: packet %5d: packet does not have end sync, discarding and resyncing", filename, info.packets);
        //    continue;
        //}
        info.packets++;

        /* prepare to parse transport stream packet */
        struct bitbuf *pb = initbits_memory(packet, 188);

        /* parse transport packet header */
        int transport_error_indicator, payload_unit_start_indicator;
        int pid, adaptation_field_control, continuity_counter, pcr_flag;
        int discontinuity_indicator;
        long long pcr;
        int header_size = parse_transport_packet_header(pb, verbose, info.packets, &transport_error_indicator,
                &payload_unit_start_indicator, &pid, &adaptation_field_control, &continuity_counter,
                &discontinuity_indicator, &pcr_flag, &pcr);
        if (header_size<0) {
            freebits(pb);
            continue;
        }
        if (transport_error_indicator) {
            if (verbose>=1)
                rvmessage("transport packet %d indicated an error exists in the packet", info.packets);
            freebits(pb);
            continue;
        }

        /* keep list of unique pids */
        for (i=0; i<num_unique_pids; i++)
            if (unique_pids[i]==pid)
                break;
        if (i==num_unique_pids && num_unique_pids<sizeof(unique_pids)/sizeof(int))
            unique_pids[num_unique_pids++] = pid;

        /* count pid frequency */
        pid_count[pid]++;

        /* measure bitrate for each pid */
        if (pcr_flag) {
            if (pcr_pid==0 && pcr!=0)
                pcr_pid = pid; /* use this pid to measure bitrate */
            /* find first and last pcrs in bitstream */
            if (first_pcr[pid]<0) {
                first_pcr[pid] = pcr;
                first_pcr_packet[pid] = info.packets-1;
            } else {
                const long long acceptable_pcr_jitter = 27000000ll;     // filter degenerate pcr jitter from actual wraparound count.
                if (pcr + acceptable_pcr_jitter < (last_pcr[pid]<0? first_pcr[pid] : last_pcr[pid])) {
                    if (verbose>=1)
                        rvmessage("pcr_wraparound: pcr=%lld last_pcr=%lld", pcr, (last_pcr[pid]<0? first_pcr[pid] : last_pcr[pid]));
                    pcr_wraparound[pid]++;
                }
                last_pcr[pid] = pcr;
                last_pcr_packet[pid] = info.packets-1;
            }
            if (verbose>=2)
                rvmessage("pcr: pid=%d pcr=%lld", pid, pcr);
        }

        /* calculate payload size and check sanity */
        int payload_size = 188-header_size;
        if (payload_size==0) {
            freebits(pb);
            continue;
        }
        if (payload_size<0) {
            if (verbose>=1)
                rvmessage("error parsing transport packet header, resyncing");
            freebits(pb);
            continue;
        }

        /* parse transport packet payload */
        if (adaptation_field_control==1 || adaptation_field_control==3) {
            if (pid==0x0000) {
                /* TODO check continuity_counter */

                /* look for new pat section */
                if (payload_unit_start_indicator) {
                    payload_size -= parse_pointer_field(pb);
                    pat_pointer = 0;
                }

                /* copy payload to pat section */
                while (payload_size) {
                    if (pat_pointer==1024) {
                        rvmessage("%s: packet %5d: pat section is too large", filename, info.packets);
                        break;
                    }
                    pat_section[pat_pointer++] = getbits8(pb);
                    payload_size--;
                }

                /* try to parse pmt section */
                struct bitbuf *sb = initbits_memory(pat_section, pat_pointer);
                int ret = parse_program_association_section(sb, pat_pointer, verbose, pat);
                freebits(sb);

                if (ret>0 && pat->version_displayed!=pat->version_number) {
                    if (verbose>=1)
                        describe_program_association(pat);

                    pat->version_displayed = pat->version_number;
                }

                /* allocate pmt table and pmt section for each program */
                if (ret>0)
                    for (i=0; i<pat->number_programs; i++)
                        if (pmt[i]==NULL) {
                            pmt[i] = (struct program_map_table *)rvalloc(NULL, sizeof(struct program_map_table), 1);
                            pmt_section[i] = (unsigned char *)rvalloc(NULL, SECTION_BUFFER_SIZE, 1);
                            pmt[i]->version_displayed = -1;
                            pmt_pointer[i] = 0;
                        }

            } else {
                for (i=0; i<pat->number_programs; i++)
                    if (pid==pat->program[i].program_map_pid) {
                        /* TODO check continuity_counter */

                        /* look for new pmt section */
                        if (payload_unit_start_indicator) {
                            payload_size -= parse_pointer_field(pb);
                            pmt_pointer[i] = 0;
                        }

                        /* copy payload to pmt section */
                        while (payload_size) {
                            if (pmt_pointer[i]==1024) {
                                rvmessage("%s: packet %5d: pmt section is too large", filename, info.packets);
                                break;
                            }
                            pmt_section[i][pmt_pointer[i]++] = getbits8(pb);
                            payload_size--;
                        }

                        /* try to parse pmt section */
                        struct bitbuf *sb = initbits_memory(pmt_section[i], pmt_pointer[i]);
                        int ret = parse_program_map_section(sb, pmt_pointer[i], verbose, pmt[i]);
                        freebits(sb);

                        if (ret>0 && pmt[i]->version_displayed!=pmt[i]->version_number) {
                            /* display pmt */
                            if (verbose>=1)
                                describe_program_map(pmt[i]);

                            /* count number of video and audio streams */
                            for (j=0; j<pmt[i]->number_streams; j++) {
                                switch (pmt[i]->stream[j].stream_type) {
                                    case STREAM_TYPE_VIDEO_MPEG1 :
                                    case STREAM_TYPE_VIDEO_MPEG2 :
                                    case STREAM_TYPE_VIDEO_MPEG4 :
                                    case STREAM_TYPE_VIDEO_H264 :
                                    case STREAM_TYPE_VIDEO_H264SVC :
                                    case STREAM_TYPE_VIDEO_H264MVC :
                                    case STREAM_TYPE_VIDEO_JPEG2000 :
                                    case STREAM_TYPE_VIDEO_HEVC :
                                    case STREAM_TYPE_VIDEO_AVS :
                                    //case STREAM_TYPE_VIDEO_WMV :
                                    case STREAM_TYPE_VIDEO_DIRAC :
                                    case STREAM_TYPE_VIDEO_VC1 :
                                        info.vstreams++;
                                        if (info.vstreams==1)
                                            info.vstream_type = pmt[i]->stream[j].stream_type;
                                        else if (info.vstream_type != pmt[i]->stream[j].stream_type)
                                            info.vstream_type = STREAM_TYPE_MIXED;
                                        break;

                                    case STREAM_TYPE_AUDIO_MPEG1 :
                                    case STREAM_TYPE_AUDIO_MPEG2 :
                                    case STREAM_TYPE_AUDIO_AAC :
                                    case STREAM_TYPE_AUDIO_LOAS :
                                    case STREAM_TYPE_AUDIO_MPEG4_RAW :
                                    case STREAM_TYPE_AUDIO_AC3 :
                                    case STREAM_TYPE_AUDIO_DTS_6CHAN :
                                    case STREAM_TYPE_AUDIO_TRUEHD :
                                    case STREAM_TYPE_AUDIO_DTS_8CHAN :
                                    case STREAM_TYPE_AUDIO_DTS :
                                        info.astreams++;
                                        if (info.astreams==1)
                                            info.astream_type = pmt[i]->stream[j].stream_type;
                                        else if (info.astream_type != pmt[i]->stream[j].stream_type)
                                            info.astream_type = STREAM_TYPE_MIXED;
                                        break;

                                    /* also look for video and audio stream types identified with the registration_descriptor */
                                    case STREAM_TYPE_PRIVATE_DATA :
                                    {
                                        switch (pmt[i]->stream[j].format_identifier) {
                                            case 0x56432d31: // vc-1 video
                                            case 0x56432d34: // vc-4 video
                                                info.vstreams++;
                                                if (info.vstreams==1) {
                                                    info.vstream_type = STREAM_TYPE_PRIVATE_DATA;
                                                    info.vstream_format_identifier = pmt[i]->stream[j].format_identifier;
                                                } else if (info.vstream_format_identifier != pmt[i]->stream[j].format_identifier)
                                                    info.vstream_type = STREAM_TYPE_MIXED;
                                                break;

                                            case 0x41432D33: // atsc ac-3 audio
                                            case 0x42535344: // smpte s302m pcm audio
                                            case 0x47413934: // atsc a/53
                                            case 0x6d6c7061: // dolby truehd audio
                                            case 0x4d54524d: // d-vhs
                                            case 0x4f707573: // opus audio
                                                info.astreams++;
                                                if (info.astreams==1) {
                                                    info.astream_type = STREAM_TYPE_PRIVATE_DATA;
                                                    info.astream_format_identifier = pmt[i]->stream[j].format_identifier;
                                                } else if (info.astream_format_identifier != pmt[i]->stream[j].format_identifier)
                                                    info.astream_type = STREAM_TYPE_MIXED;
                                                break;
                                        }
                                    }
                                }
                            }

                            pmt[i]->version_displayed = pmt[i]->version_number;
                        }
                    }
            }
        }
        assert(payload_size>=0);

        /* removing packet stuffing bytes or unhandled packets */
        //while (payload_size--)
        //    flushbits(pb, 8);

        /* tidy up */
        freebits(pb);

    } while (!eofbits(bb));

    /* check for proper program information table */
    int tables_bad = pat->number_programs? 0 : 1;
    for (i=0; i<pat->number_programs; i++)
        if (pmt[i]->program_number && pmt[i]->number_streams==0)
            tables_bad = 1;
    if (!tables_bad)
        info.flags[F_FRAME] = 't';

    /* display list of unique pids */
    if (verbose>=1) {
        char string[1024];
        int n = 0;
        for (i=0; i<num_unique_pids; i++)
            n += snprintf(string+n, sizeof(string)-n, "%d ", unique_pids[i]);
        rvmessage("%d unique pids: %s", num_unique_pids, string);
    }

    /* calculate average bitrate */
    if (pcr_pid>0 && first_pcr[pcr_pid]>=0 && last_pcr[pcr_pid]>=0 && last_pcr[pcr_pid]!=first_pcr[pcr_pid]) {
        long long bitrate = (last_pcr_packet[pcr_pid]-first_pcr_packet[pcr_pid]) * 188ll*8ll;
        bitrate *= 27000000ll;
        bitrate /= pcr_wraparound[pcr_pid]*MAX_PCR_VALUE+last_pcr[pcr_pid]-first_pcr[pcr_pid];
        info.bitrate = (int) bitrate;
    }

    /* display per-pid statistics */
    if (verbose>=1) {
        rvmessage("+------+----------+-------+----------+");
        rvmessage("|  pid |  packets | ratio |  bitrate +");
        rvmessage("+------+----------+-------+----------+");
        for (i=0; i<8192; i++)
            if (pid_count[i]) {
                int pp = pcr_pid;
                if (last_pcr[i]>first_pcr[i]) {
                    /* pid has a timebase */
                    pp = i;
                }
                if (last_pcr[pp]>first_pcr[pp]) {
                    double bitrate = pid_count[i] * 188.0*8.0;
                    bitrate *= 27.0;
                    bitrate /= pcr_wraparound[i]*MAX_PCR_VALUE+last_pcr[pp]-first_pcr[pp];
                    rvmessage("| %4d | %8d | %4.1f%% | %4.1fMbps |", i, pid_count[i], 100.0*pid_count[i]/info.packets, bitrate);
                } else
                    rvmessage("| %4d | %8d | %4.1f%% |          |", i, pid_count[i], 100.0*pid_count[i]/info.packets);
            }
        rvmessage("+------+----------+-------+----------+");
    }

    /* tidy up */
    for (i=0; i<MAX_PMTS; i++) {
        rvfree(pmt_section[i]);
        rvfree(pmt[i]);
    }
    rvfree(pat);

    return info;
}

struct muxerinfo grok_ps(struct bitbuf *bb, const char *filename)
{
    struct muxerinfo info = muxerblank;

    /* default file information */
    info.type = "ps";
    info.subtype = space;

    return info;
}

struct muxerinfo grok_pes(struct bitbuf *bb, const char *filename, int verbose)
{
    struct muxerinfo info = muxerblank;

    /* default file information */
    info.type = "pes";
    info.subtype = space;

    /* loop over pes packets */
    do {
        int i;

        /* look for next start code */
        int leading = 0;
        while (showbits24(bb)!=0x000001 && !eofbits(bb))
            leading += flushbits(bb, 8);
        if (eofbits(bb))
            break;
        if (leading && verbose>=1)
            rvmessage("%d bits of leading garbage before start code", leading);

        flushbits(bb, 24);                                  /* packet_start_code_prefix */
        int stream_id = getbits8(bb);                       /* stream_id */
        int pes_packet_length = getbits(bb, 16);            /* PES_packet_length */

        /* describe pes packet */
        if (verbose>=2)
            describe_pes_packet(stream_id, pes_packet_length);

        /* lookup stream id */
        if (stream_id>=0xc0 && stream_id<=0xdf)
            info.subtype = "audio";
        else if (stream_id>=0xe0 && stream_id<=0xef)
            info.subtype = "video";

        for (i=0; i<pes_packet_length; i++)
            flushbits(bb, 8);                               /* PES_packet_data_byte */

        info.packets++;
    } while (!eofbits(bb));

    /* tidy up */

    return info;
}

struct videoinfo grok_avi(struct bitbuf *bb, const char *filename, int verbose)
{
    struct videoinfo info = videoblank;

    /* default file information */
    info.type = "avi";
    info.subtype = space;

    /* skip past avi header */
    unsigned int fourcc = getdword(bb);
    unsigned int size   = getdword(bb);
    if (fourcc!=FOURCC('R','I','F','F')) {
        rvmessage("input file is not a RIFF file: %s", fourccname(fourcc));
        return videoblank;
    }
    fourcc = getdword(bb);
    if (fourcc!=FOURCC('A','V','I',' ')) {
        rvmessage("input file is not an AVI file: %s", fourccname(fourcc));
        return videoblank;
    }

    /* initialise stream index */
    int index = -1;

    /* loop over lists and chunks */
    int type = FOURCC('n','u','l','l');
    while (!eofbits(bb)) {
        int frames, width, height;

        fourcc = getdword(bb);
        size   = getdword(bb);

        //rvmessage("%s with %d bytes", fourccname(fourcc), size);

        switch (fourcc) {

            case FOURCC('L','I','S','T'):
                parse_list(bb, verbose, (int *)&fourcc, size);
                if (fourcc==FOURCC('s','t','r','l'))
                    index++;
                break;

            case FOURCC('a','v','i','h'):
                parse_avih(bb, verbose, &frames, &width, &height);
                /* file information */
                info.width = width;
                info.height = height;
                info.pixels = width;
                info.lines = height;
                info.frames = frames;
                info.pictures = frames;
                break;

            case FOURCC('s','t','r','h'):
                parse_strh(bb, verbose, index, &type);
                break;

            //case FOURCC('s','t','r','f'):

            /* ascii string chunks */
            case FOURCC('s','t','r','n'):
            case FOURCC('I','S','F','T'):
                parse_strn(bb, verbose, size);
                break;

            default:
                if (verbose>=2)
                    rvmessage("skipping unknown chunk %s", fourccname(fourcc));
                size = (size + 1) & ~1;
                flushbits(bb, size*8);
                break;
        }
    }

    /* tidy up */

    return info;
}

struct muxerinfo grok_qt(struct bitbuf *bb, const char *filename)
{
    struct muxerinfo info = muxerblank;

    /* default file information */
    info.type = "qt";
    info.subtype = space;

    return info;
}

struct muxerinfo grok_asf(struct bitbuf *bb, const char *filename, int verbose)
{
    struct muxerinfo info = muxerblank;

    /* default file information */
    info.type = "asf";
    info.subtype = space;

    /* loop over objects */
    do {
        char string[37];

        /* read object id and size */
        struct guid guid = getguid(bb);             /* object id */
        unsigned long long size = getqword(bb);     /* object size */

        //rvmessage("object with guid {%s} and size %lld", stringify_guid(guid, string), size);

        if (strcmp(stringify_guid(guid, string), ASF_HEADER_OBJECT)==0) {
            if (parse_header_object(bb, verbose)<0)
                return muxerblank;

        } else if (strcmp(stringify_guid(guid, string), ASF_FILE_PROPERTIES_OBJECT)==0) {
            int flags;
            long long data_packets_count;
            int data_packet_size;
            parse_file_properties_object(bb, verbose, &data_packets_count, &flags, &data_packet_size);
            if (flags & 1)   /* broadcast flag */
                info.flags[F_FRAME] = 'b';
            if (flags & 2)   /* seekable flag */
                info.flags[F_OTHER] = 's';
            info.packets = data_packets_count;

        } else if (strcmp(stringify_guid(guid, string), ASF_STREAM_PROPERTIES_OBJECT)==0) {
            int codec_specific_data_size;
            unsigned char *codec_specific_data = NULL;

            parse_stream_properties_object(bb, verbose, &codec_specific_data_size, &codec_specific_data);

            rvfree(codec_specific_data);

        } else if (strcmp(stringify_guid(guid, string), ASF_HEADER_EXTENSION_OBJECT)==0) {
            parse_header_extension_object(bb, verbose);

        } else if (strcmp(stringify_guid(guid, string), ASF_CODEC_LIST_OBJECT)==0) {
            parse_codec_list_object(bb, verbose);

        } else if (strcmp(stringify_guid(guid, string), ASF_CONTENT_DESCRIPTION_OBJECT)==0) {
            parse_content_description_object(bb, verbose);

        } else if (strcmp(stringify_guid(guid, string), ASF_STREAM_BITRATE_PROPERTIES_OBJECT)==0) {
            parse_stream_bitrate_properties_object(bb, verbose);

        } else if (strcmp(stringify_guid(guid, string), ASF_DATA_OBJECT)==0) {
            /* no need to parse any further */
            return info;

        } else {
            /* skip unknown object */
            size -= 24;
            flushbits(bb, size*8);
            if (verbose>=1)
                rvmessage("skipping unknown object: %s", stringify_guid(guid, string));
        }

    } while (!eofbits(bb));

    return info;
}

struct muxerinfo grok_webm(struct bitbuf *bb, const char *filename, int verbose)
{
    struct muxerinfo info = muxerblank;

    /* default file information */
    info.type = "matroska";
    info.subtype = space;

    /* ebml header defaults */
    unsigned version = 1;
    unsigned readversion = 1;
    unsigned doctypeversion = 1;
    unsigned doctypereadversion = 1;

    /* read ebml header element (level 0) */
    if (ebmlid(bb)!=0x1a45dfa3)
        rvexit("not an ebml file"); /* sanity check */
    unsigned long long headersize = ebmlsize(bb);
    off_t pos = numbits(bb);
    while (numbits(bb)-pos<headersize*8) {
        unsigned id = ebmlid(bb);
        unsigned long long size = ebmlsize(bb);
        switch (id) {
            case 0x4286 : version = getbits(bb, size*8); break;
            case 0x42f7 : readversion = getbits(bb, size*8); break;
            case 0x4282 : for (int i=0; i<mmin(size, sizeof(space)); i++) space[i] = getbyte(bb); break;
            case 0x4287 : doctypeversion = getbits(bb, size*8); break;
            case 0x4285 : doctypereadversion = getbits(bb, size*8); break;
            case 0xffffffff : rvexit("found reserved ebml id");
            default : flushbits(bb, size*8); break;
        }
    }

    if (verbose>=1) {
        rvmessage("matroska v%d, (min v%d)", version, readversion);
        rvmessage("doctype=%s, v%d (min v%d)", info.subtype, doctypeversion, doctypereadversion);
    }

    /* read segment element (level 0) */
    if (ebmlid(bb)!=0x18538067)
        rvexit("no data segment in matroska file"); /* sanity check */
    unsigned long long segmentsize = ebmlsize(bb);
    pos = numbits(bb);
    rvmessage("segmentsize=%llu", segmentsize);

    /* read top level elements (level 1) */
    while (numbits(bb)-pos<segmentsize*8) {
        unsigned id = ebmlid(bb);
        unsigned long long size = ebmlsize(bb);
        switch (id) {
            case 0x114d9b74 : rvmessage("seek information size=%llu", size); flushbits(bb, size*8); break;
            case 0x1549a966 : rvmessage("segment information size=%llu", size); flushbits(bb, size*8); break;
            case 0x1f43b675 : rvmessage("cluster size=%llu", size); flushbits(bb, size*8); break;
            case 0x1654ae6b : rvmessage("track size=%llu", size); flushbits(bb, size*8); break;
            case 0x1c53bb6b : rvmessage("track size=%llu", size); flushbits(bb, size*8); break;
            default : rvmessage("unknown id %x", id); flushbits(bb, size*8); break;
        }
    }

    return info;
}

struct videoinfo grok_yuv4mpeg(const char *filename)
{
    FILE *filein = stdin;
    struct videoinfo info = videoblank;

    /* open yuv4mpeg file */
    if (filename)
        filein = fopen(filename, "rb");
    if (filein==NULL)
        rverror("failed to open yuv4mpeg file \"%s\"", filename);

    /* parse yuv4mpeg stream header */
    fourcc_t fourcc;
    if (read_y4m_stream_header(&info.width, &info.height, &fourcc, filein)<0)
        rverror("failed to read yuv4mpeg stream header");

    /* allocate storage for frame */
    int size = get_framesize(info.width, info.height, fourcc);
    char *data = (char *)rvalloc(NULL, size*sizeof(char), 0);

    /* loop over frames */
    do {
        /* read each frame header plus frame data */
        if (read_y4m_frame_header(filein)>0)
            if (fread(data, size, 1, filein)==1)
                info.frames++;
    } while (!feof(filein) && !ferror(filein));
    if (ferror(filein))
        rverror("failed to read from input");

    /* file information */
    info.pixels = info.width;
    info.lines = info.height;
    info.type = "yuv4";
    info.subtype = (char *) fourccname(fourcc);

    /* tidy up */
    rvfree(data);
    if (filename)
        fclose(filein);
    return info;
}

struct videoinfo grok_h261(struct bitbuf *bb, const char *filename, int verbose, struct stat *st)
{
    struct videoinfo info = videoblank;

    /* default file information */
    info.type = "261";
    info.subtype = "";

    int hires;
    /* look for a picture start code */
    do {
        int start = showbits(bb, 20);
        if (start==0x00010) {
            flushbits(bb, 20);

            /* parse picture layer */
            int width, height;
            if (parse_261_picture(bb, verbose, &width, &height, &hires)<0)
                rvexit("failed to parse h.261 picture layer in file \"%s\"", filename);

            /* don't count start code that finishes last frame */
            if (st->st_size*8-numbits(bb) > 8) {
                info.width = width;
                info.height = height;
                info.pixels = width;
                info.lines = height;
                if (hires)
                    info.subtype = "hires";
                info.frames++;
            }
        }

        /* move along by one bit */
        flushbits(bb, 1);
    } while (!eofbits(bb));

    /* tidy up */

    return info;
}

struct videoinfo grok_h263(struct bitbuf *bb, const char *filename, struct stat *st, struct frameinfo *finfo, int num_elements)
{
    struct videoinfo info = videoblank;

    /* default file information */
    info.type = "263";
    info.subtype = space;

    /* look for a start code */
    do {
        /* look for a picture start code */
        int start = showbits(bb, 22);
        if (start==0x00020) {
            int width, height;
            int pct, umv, sac, apm, pb;
            int aic, df, ss, rps, isd, aiv, mq, ipb, rpr, rru;

            /* note byte count before parsing vop header */
            int bytes = (numbits(bb) - 22) / 8;

            /* parse picture header */
            flushbits(bb, 22);
            if (parse_263(bb, &width, &height, &pct, &umv, &sac, &apm, &pb, &aic, &df, &ss, &rps, &isd, &aiv, &mq, &ipb, &rpr, &rru)<0)
                rvexit("failed to parse h.263 picture header in file \"%s\"", filename);

            /* align bitstream */
            alignbits(bb);

            /* don't count start code that finishes last frame */
            if (st->st_size*8-numbits(bb) > 8) {
                info.width = width;
                info.height = height;
                info.pixels = width;
                info.lines = height;

                /* check for annexes */
                if (umv)
                    space[0] = 'D';
                if (sac)
                    space[1] = 'E';
                if (apm)
                    space[2] = 'F';
                if (pb)
                    space[3] = 'G';
                if (aic)
                    space[4] = 'I';
                if (df)
                    space[5] = 'J';
                if (aiv)
                    space[6] = 'S';
                if (mq)
                    space[7] = 'T';

                /* store frame information */
                if (info.frames<num_elements) {
                    switch (pct) {
                        case 0: finfo[info.frames].frame_type = I_FRAME; break;
                        case 1: finfo[info.frames].frame_type = P_FRAME; break;
                    }
                }

                /* update flags */
                if (pct==0 && info.flags[F_FRAME]!='p')
                    info.flags[F_FRAME] = 'i';
                if (pct==1)
                    info.flags[F_FRAME] = 'p';

                /* store previous frame information */
                calc_framesize(info.frames, finfo, num_elements, bytes);

                /* count frames */
                info.frames++;

                continue;
            }
        }

        /* look for a (byte-aligned) gob header */
        start = showbits(bb, 17);
        if (start==0x00001) {
            flushbits(bb, 17);
            alignbits(bb);
            info.flags[F_ERROR] = 'g';
            continue;
        }

        /* move along */
        flushbits(bb, 8);
    } while (!eofbits(bb));

    /* calc last frame size */
    calc_framesize(info.pictures, finfo, num_elements, numbits(bb)/8);

    /* tidy up */

    return info;
}

struct videoinfo grok_h264(struct bitbuf *bb, const char *filename, int verbose, struct stat *st, struct frameinfo *finfo, struct sliceinfo *sinfo, int num_elements)
{
    struct videoinfo info = videoblank;
    struct rv264sequence seq = {0};

    /* default file information */
    info.type = "264";
    info.subtype = space;

    /* use access unit delimiters, if present, to cross reference frame count */
    int access_unit = 0;

    /* look for a start code prefix */
    int refresh = 0;
    int end_of_stream = 0;
    int num_nals = 0;
    do {
        /* note byte count before parsing byte stream nal_unit */
        int zerobyte, bytes = numbits(bb) / 8;

        /* look for a nal unit */
        find_next_nal_unit(bb, &zerobyte);

        /* parse nal unit */
        if (!eofbits(bb)) {
            if (zerobyte)
                flushbits(bb, 8);           /* zero_byte */
            flushbits(bb, 24);              /* start_code_prefix_one_3bytes */

            /* parse nal_unit */
            int nal_unit_type, nal_ref_idc;
            int ret = parse_nal_unit_header(bb, &nal_unit_type, &nal_ref_idc);
            if (ret<0)
                rvexit("failed to parse nal unit in file \"%s\"", filename);
            /* sanity check end of stream */
            if (end_of_stream)
                rvmessage("warning: nal unit type %d found after end of stream nal unit", nal_unit_type);

            /* prepare to parse rbsp */
            struct bitbuf *rbsp = extract_rbsp(bb, verbose);
            if (verbose>=2)
                rvmessage("nal %d: length=%-5d ref_idc=%d unit_type=%s", num_nals, numbitsleft(rbsp)/8, nal_ref_idc, nal_unit_type_code(nal_unit_type));

            /* lookup nal_unit_type */
            struct slice_header *sh;
            static int prev_first_mb_in_slice, prev_pic_order_cnt;
            switch (nal_unit_type) {
                case 1: /* normal slice */
                case 5: /* idr slice */

                    /* parse slice header */
                    sh = parse_slice_header(rbsp, nal_unit_type, nal_ref_idc, seq.pps, verbose);
                    if (sh==NULL) {
                        //rvexit("failed to parse slice header in file \"%s\"", filename);
                        break;
                    }

                    /* count slices */
                    if (info.pictures<num_elements)
                        finfo[info.pictures].slices++;

                    /* look for first slice in an access unit */
                    if (sh->first_mb_in_slice==0) {
                        /* record per-frame information */
                        if (info.pictures<num_elements) {
                            /* update coded picture type */
                            if (nal_unit_type==5)
                                finfo[info.pictures].frame_type = IDR_FRAME;
                            else if (sh->slice_type==SLICE_I || sh->slice_type==SLICE_SI || sh->slice_type==SLICE_CI || sh->slice_type==SLICE_CSI) {
                                if (finfo[info.pictures].frame_type != B_FRAME && finfo[info.pictures].frame_type != P_FRAME)
                                    finfo[info.pictures].frame_type = I_FRAME;
                            } else if (sh->slice_type==SLICE_P || sh->slice_type==SLICE_SP || sh->slice_type==SLICE_CP || sh->slice_type==SLICE_CSP) {
                                if (finfo[info.pictures].frame_type != B_FRAME)
                                    finfo[info.pictures].frame_type = P_FRAME;
                            } else if (sh->slice_type==SLICE_B || sh->slice_type==SLICE_CB)
                                finfo[info.pictures].frame_type = B_FRAME;

                            /* update coded picture type */
                            if (!sh->field_pic_flag)
                                finfo[info.pictures].field_type = FRAME;
                            else if (!sh->bottom_field_flag)
                                finfo[info.pictures].field_type = TOP_FIELD;
                            else
                                finfo[info.pictures].field_type = BOT_FIELD;

                            /* add other data */
                            finfo[info.pictures].order = sh->PicOrderCnt;
                            finfo[info.pictures].reference = nal_ref_idc!=0;
                        }

                        /* calculate frame size */
                        calc_framesize(info.pictures, finfo, num_elements, bytes);

                        /* update counts */
                        info.pictures++;
                        if (!sh->bottom_field_flag)
                            info.frames++;

                    }

                    /* record per-slice information */
                    if (info.slices<num_elements) {
                        sinfo[info.slices].bytes = rbsp->membytes;
                        sinfo[info.slices].first_mb_in_slice = sh->first_mb_in_slice;
                        sinfo[info.slices].slice_type = sh->slice_type;
                        sinfo[info.slices].order = sh->PicOrderCnt;
                    }
                    info.slices++;

                    /* update flags */
                    switch (sh->slice_type) {
                        case SLICE_I:
                        case SLICE_SI:
                        case SLICE_CI:
                        case SLICE_CSI:
                            /* don't overwrite p- or b-slice flag */
                            if (info.flags[F_FRAME]=='-')
                                info.flags[F_FRAME] = 'i';
                            break;

                        case SLICE_P:
                        case SLICE_SP:
                        case SLICE_CP:
                        case SLICE_CSP:
                            /* don't overwrite b-slice flag */
                            if (info.flags[F_FRAME]!='b')
                                info.flags[F_FRAME] = 'p';
                            break;

                        case SLICE_B:
                        case SLICE_CB:
                            info.flags[F_FRAME] = 'b';
                            break;
                    }

                    if (sh->redundant_pic_cnt)
                        info.flags[F_FRAME] = 'r';

                    if (sh->disable_deblocking_filter_idc != 1)
                        if (info.flags[F_OTHER] == '-')
                            info.flags[F_OTHER] = 'd';

                    /* multiple slices in picture */
                    if (sh->first_mb_in_slice && info.flags[F_ERROR]=='-')
                        info.flags[F_ERROR] = 's';

                    /* arbitray slice order - assumes arbitray within picture boundaries */
                    if (sh->first_mb_in_slice < prev_first_mb_in_slice && sh->PicOrderCnt == prev_pic_order_cnt)
                        if (info.flags[F_ERROR] != 'f')
                            info.flags[F_ERROR] = 'a';

                    /* reference picture list modification */
                    if (sh->ref_pic_list_reordering.ref_pic_list_reordering_flag[0] || sh->ref_pic_list_reordering.ref_pic_list_reordering_flag[1])
                        info.flags[F_REFER] = 'm';

                    if (nal_unit_type==1)
                        if (!refresh)
                            rvmessage("warning: coded picture data without an IDR");
                    /* idr slice, also only print above message once */
                    refresh = 1;

                    prev_first_mb_in_slice = sh->first_mb_in_slice;
                    prev_pic_order_cnt = sh->PicOrderCnt;
                    break;

                case 2:
                case 3:
                case 4:
                    /* data partitioned slice */
                    rvmessage("time to implement data partitioned slices");
                    if (!refresh)
                        rvmessage("warning: coded picture data without an IDR");
                    /* this will override the pps value */
                    info.flags[F_ERROR] = 'p';
                    break;

                case 6:
                    /* supplemental enhancement information */
                    parse_sei(rbsp, &seq, verbose);

                    /* look for an sei timecode */
                    if (seq.sei_message.pic_timing.seconds_flag) {
                        /* update flags */
                        info.flags[F_TIMECODE] = 't';

                        /* update timecode */
                        if (info.pictures<num_elements) {
                            finfo[info.pictures].timecode.present = 1;
                            finfo[info.pictures].timecode.frames  = seq.sei_message.pic_timing.n_frames;
                            finfo[info.pictures].timecode.seconds = seq.sei_message.pic_timing.seconds_value;
                            finfo[info.pictures].timecode.minutes = seq.sei_message.pic_timing.minutes_value;
                            finfo[info.pictures].timecode.hours   = seq.sei_message.pic_timing.hours_value;
                        }
                    }

                    /* look for valid cc_data in sei */
                    if (seq.sei_message.user_data_registered_itu_t_t35.cc_valid)
                        /* update flags */
                        info.flags[F_CAPTION] = 'c';
                    break;

                case 7:
                {
                    /* sequence parameter set */
                    struct rv264seq_parameter_set *sps = parse_sequence_param(rbsp);
                    if (sps==NULL)
                        rvexit("failed to parse sequence parameter set header in file \"%s\"", filename);
                    info.pixels = info.width = sps->FrameCropWidthInSamples;
                    info.height = info.lines = sps->FrameCropHeightInSamples;
                    if (sps->vui_parameters_present_flag && sps->vui.timing_info_present_flag)
                        info.framerate = (float)sps->vui.time_scale / (2.0*(float)sps->vui.num_units_in_tick);
                    else
                        info.framerate = 0.0;
                    if (verbose>=3)
                        rvmessage("seq_parameter_set_id=%d, vui parameters %spresent", sps->seq_parameter_set_id, sps->vui_parameters_present_flag? "" : "not ");
                    const char *string = "";
                    switch (sps->profile_idc) {
                        case 66  : string = "BP"; break;
                        case 77  : string = "MP"; break;
                        case 88  : string = "EP"; break;
                        case 100 : string = "HP"; break;
                        case 110 : string = "H10"; break;
                        case 122 : string = "H422"; break;
                        case 144 : string = "H444"; break; /* obsolete, removed from spec. */
                        case 244 : string = "H444"; break;
                        case 44  : string = "C444"; break; /* calvlc 4:4:4 intra profile */
                        case 83  : string = "SBP"; break;  /* scalable baseline profile */
                        case 86  : string = "SHP"; break;  /* scalable high profile */
                        case 118 : string = "MVHP"; break; /* multiview high profile */
                        case 128 : string = "SHP"; break;  /* stereo high profile */
                        default : rvmessage("warning: unknown h.264 profile: %d", sps->profile_idc);
                    }
                    snprintf(space, BLANK_BYTES, "%s@%.1f %2d", string, (float)sps->level_idc/10.0, sps->max_num_ref_frames);
                    if (sps->mb_adaptive_frame_field_flag)
                        info.flags[F_LACED] = 'a';
                    else if (sps->frame_mbs_only_flag)
                        info.flags[F_LACED] = 'p';
                    else
                        info.flags[F_LACED] = 'i';
                    if (sps->qpprime_y_zero_transform_bypass_flag)
                        info.flags[F_QUANT] = 'l';
                    else if (sps->seq_scaling_matrix_present_flag)
                        info.flags[F_QUANT] = 's';
                    /* save for a different flag */
                    switch (sps->bit_depth_luma_minus8) {
                        case 1: info.flags[F_OTHER] = '9'; break;
                        case 2: info.flags[F_OTHER] = '0'; break;
                        case 3: info.flags[F_OTHER] = '1'; break;
                        case 4: info.flags[F_OTHER] = '2'; break;
                    }
                    switch (sps->chroma_format_idc) {
                        case 0 : info.flags[F_CHROMA] = 'm'; break;
                        case 2 : info.flags[F_CHROMA] = '2'; break;
                        case 3 : info.flags[F_CHROMA] = '4'; break;
                    }
                    //rvfree(seq.sps[sps->seq_parameter_set_id]);
                    seq.sps[sps->seq_parameter_set_id] = sps;
                    break;
                }

                case 8:
                {
                    /* picture parameter set */
                    struct rv264pic_parameter_set *pps = parse_picture_param(rbsp, seq.sps, verbose); // FIXME support multiple sequences.
                    if (pps!=NULL) {
                        if (verbose>=3)
                            rvmessage("pic_parameter_set_id=%d seq_parameter_set_id=%d", pps->pic_parameter_set_id, pps->seq_parameter_set_id);
                        if (pps->entropy_coding_mode_flag)
                            info.flags[F_ENTROPY] = 'a';
                        if (pps->transform_8x8_mode_flag)
                            info.flags[F_MOTION] = '8';
                        if (pps->weighted_pred_flag)
                            /* TODO this will override 8x8 mode */
                            info.flags[F_MOTION] = 'w';
                        if (pps->num_slice_groups_minus1>0)
                            info.flags[F_ERROR] = 'f'; //'0' + p->num_slice_groups_minus1;
                        if (pps->pic_scaling_matrix_present_flag)
                            info.flags[F_QUANT] = 'p';
                        rvfree(seq.pps[pps->pic_parameter_set_id]);
                        seq.pps[pps->pic_parameter_set_id] = pps;
                    }
                    break;
                }

                case 9:
                    /* access unit delimiter */
                    //rvmessage("primary_pic_type=%d", getbits(rbsp, 3));
                    access_unit++;
                    break;

                case 10:
                    /* end of sequence */
                    refresh = 0;
                    break;

                case 11:
                    /* end of stream */
                    end_of_stream = 1;
                    break;

                case 12:
                    /* filler data */
                    break;

                default:
                    if (verbose>=0)
                        rvmessage("unknown nal_unit_type: %d", nal_unit_type);
                    break;
            }

            /* tidy up */
            free_rbsp(rbsp);

        } else {
            break;
        }

        /* count nal units */
        num_nals++;

    } while (!eofbits(bb));

    /* calc last frame size */
    calc_framesize(info.pictures, finfo, num_elements, numbits(bb)/8);

    /* cross reference frame count to access unit count */
    if (verbose>=1)
        if (access_unit)
            if (access_unit != info.pictures)
                rvmessage("picture count and access unit count are not the same: %d != %d", info.pictures, access_unit);

    /* tidy up */

    return info;
}

struct videoinfo grok_hevc(struct bitbuf *bb, const char *filename, int verbose, struct stat *st, struct frameinfo *finfo, struct sliceinfo *sinfo, int num_elements)
{
    struct videoinfo info = videoblank;
    struct rvhevcvideo vid = {0};
    struct hevc_slice_header_state prev = {0};

    /* default file information */
    info.type = "hevc";
    info.subtype = space;

    /* use access unit delimiters, if present, to cross reference frame count */
    int access_unit = 0;

    /* look for a start code prefix */
    int refresh = 0;
    int end_of_stream = 0;

    do {
        /* note byte count before parsing byte stream nal_unit */
        int zerobyte, bytes = numbits(bb) / 8;

        /* look for a nal unit */
        find_next_nal_unit(bb, &zerobyte);

        /* parse nal unit */
        if (!eofbits(bb)) {
            if (zerobyte)
                flushbits(bb, 8);           /* zero_byte */
            flushbits(bb, 24);              /* start_code_prefix_one_3bytes */

            /* parse nal_unit */
            int nal_unit_type;
            int ret = parse_hevc_nal_unit_header(bb, &nal_unit_type);
            if (ret<0)
                rvexit("failed to parse nal unit in file \"%s\"", filename);
            /* sanity check end of stream */
            if (end_of_stream)
                rvmessage("warning: nal unit type %d found after end of stream nal unit", nal_unit_type);

            /* prepare to parse rbsp */
            struct bitbuf *rbsp = extract_rbsp(bb, verbose);
            if (numbitsleft(rbsp)==0)
                break;
            if (verbose>=2)
                rvmessage("nal: length=%d unit_type=%d %s", numbitsleft(rbsp)/8, nal_unit_type, hevc_nal_unit_type_code(nal_unit_type));

            /* lookup nal_unit_type */
            struct hevc_slice_header *sh;
            //static int prev_first_mb_in_slice, prev_pic_order_cnt;
            switch (nal_unit_type) {
                case TRAIL_N   : /* normal trailing slice */
                case TRAIL_R   : /* normal trailing slice */
                case TSA_N     :
                case TSA_R     :
                case STSA_N    :
                case STSA_R    :
                /* leading pictures */
                case RADL_N    : /* random access */
                case RADL_R    : /* random access */
                case RASL_N    : /* random access */
                case RASL_R    : /* random access */
                /* intra random access point (IRAP) pictures */
                case BLA_W_LP  :
                case BLA_W_RADL:
                case BLA_N_LP  : /* broken link access without leading pictures */
                case IDR_W_RADL: /* idr slice may have leading pictures */
                case IDR_N_LP  : /* idr slice without leading pictures */
                case CRA_NUT   : /* clean random access */
                    /* parse slice header */
                    sh = parse_hevc_slice_header(rbsp, nal_unit_type, vid.pps, &prev);
                    if (sh==NULL) {
                        /* this message is fairly normal in a network captured stream which does not start with param set nals */
                        if (verbose>=1)
                            rvmessage("failed to parse slice header in file \"%s\"", filename);
                        break;
                    }

                    /* count slices */
                    if (info.pictures<num_elements)
                        finfo[info.pictures].slices++;

                    /* look for first slice in an access unit */
                    if (sh->first_slice_segment_in_pic_flag==1) {
                        /* record per-frame information */
                        if (info.pictures<num_elements) {
                            /* update coded picture type */
                            if (nal_unit_type==IDR_W_RADL || nal_unit_type==IDR_N_LP)
                                finfo[info.pictures].frame_type = IDR_FRAME;
                            else if (nal_unit_type==CRA_NUT)
                                finfo[info.pictures].frame_type = CRA_FRAME;
                            else if (sh->slice_type==I_SLICE) {
                                    finfo[info.pictures].frame_type = I_FRAME;
                            } else if (sh->slice_type==P_SLICE) {
                                    finfo[info.pictures].frame_type = P_FRAME;
                            } else if (sh->slice_type==B_SLICE)
                                finfo[info.pictures].frame_type = B_FRAME;
                            /* add other data */
                            finfo[info.pictures].field_type = FRAME;
                            finfo[info.pictures].order = sh->PicOrderCntVal;
                        }

                        /* calculate frame size */
                        calc_framesize(info.pictures, finfo, num_elements, bytes);

                        /* update counts */
                        info.pictures++;
                        info.frames++;

                    }

                    /* record per-slice information */
                    if (info.slices<num_elements) {
                        sinfo[info.slices].bytes = rbsp->membytes;
                        sinfo[info.slices].first_mb_in_slice = sh->first_slice_segment_in_pic_flag? 0 : 1;
                        //sinfo[info.slices].slice_type = sh->slice_type;
                        sinfo[info.slices].order = sh->PicOrderCntVal;
                    }
                    info.slices++;

                    /* update flags */
                    switch (sh->slice_type) {
                        case I_SLICE:
                            /* don't overwrite p- or b-slice flag */
                            if (info.flags[F_FRAME]=='-')
                                info.flags[F_FRAME] = 'i';
                            break;

                        case P_SLICE:
                            /* don't overwrite b-slice flag */
                            if (info.flags[F_FRAME]!='b')
                                info.flags[F_FRAME] = 'p';
                            break;

                        case B_SLICE:
                            info.flags[F_FRAME] = 'b';
                            break;
                    }
#if 0
                    if (sh->redundant_pic_cnt)
                        info.flags[F_FRAME] = 'r';

                    if (sh->disable_deblocking_filter_idc != 1)
                        if (info.flags[F_OTHER] == '-')
                            info.flags[F_OTHER] = 'd';
#endif
                    /* multiple slices in picture */
                    if (!sh->first_slice_segment_in_pic_flag && info.flags[F_ERROR]=='-')
                        info.flags[F_ERROR] = 's';

                    if (nal_unit_type<IDR_W_RADL && nal_unit_type>IDR_N_LP)
                        if (!refresh)
                            rvmessage("warning: coded picture data without an IDR");
                    /* idr slice, also only print above message once */
                    refresh = 1;

                    if ( !(nal_unit_type>=RADL_N && nal_unit_type<=RASL_R) ) {
                        prev.prevPicOrderCntLsb = sh->slice_pic_order_cnt_lsb;
                        prev.prevPicOrderCntMsb = sh->PicOrderCntMsb;
                    }
                    break;

                case PREFIX_SEI_NUT:
                case SUFFIX_SEI_NUT:
                    /* supplemental enhancement information */
                    parse_hevc_sei_rbsp(rbsp, vid.sps[0], nal_unit_type, verbose);
                    break;

                case VPS_NUT:
                {
                    /* video parameter set */
                    struct hevc_vid_parameter_set *vps = parse_hevc_vid_param(rbsp);
                    if (vps==NULL)
                        rvexit("failed to parse video parameter set header in file \"%s\"", filename);
                    if (vps->timing_info_present_flag)
                        info.framerate = (framerate_t)vps->time_scale / (framerate_t)vps->num_units_in_tick;
                    rvfree(vid.vps[vps->vid_parameter_set_id]);
                    vid.vps[vps->vid_parameter_set_id] = vps;
                    break;
                }

                case SPS_NUT:
                {
                    /* sequence parameter set */
                    struct hevc_seq_parameter_set *sps = parse_hevc_seq_param(rbsp);
                    if (sps==NULL)
                        rvexit("failed to parse sequence parameter set header in file \"%s\"", filename);
                    info.pixels = sps->pic_width_in_luma_samples;
                    info.lines = sps->pic_height_in_luma_samples;
                    info.width = info.pixels;
                    info.height = info.lines;
                    if (sps->vui_parameters_present_flag && sps->vui.timing_info_present_flag)
                        info.framerate = (framerate_t)sps->vui.time_scale / (framerate_t)sps->vui.num_units_in_tick;
                    if (verbose>=1)
                        rvmessage("seq_parameter_set_id=%d, vui parameters %spresent", sps->seq_parameter_set_id, sps->vui_parameters_present_flag? "" : "not ");
#if 0
                    char *string = "";
                    switch (sps->profile_idc) {
                        case 66  : string = "BP"; break;
                        case 77  : string = "MP"; break;
                        case 88  : string = "EP"; break;
                        case 100 : string = "HP"; break;
                        case 110 : string = "H10"; break;
                        case 122 : string = "H422"; break;
                        case 144 : string = "H444"; break; /* obsolete, removed from spec. */
                        case 244 : string = "H444"; break;
                        case 44  : string = "C444"; break; /* calvlc 4:4:4 intra profile */
                        case 83  : string = "SBP"; break;  /* scalable baseline profile */
                        case 86  : string = "SHP"; break;  /* scalable high profile */
                        case 118 : string = "MVHP"; break; /* multiview high profile */
                        case 128 : string = "SHP"; break;  /* stereo high profile */
                        default : rvmessage("warning: unknown h.264 profile: %d", sps->profile_idc);
                    }
                    snprintf(info.subtype, BLANK_BYTES, "%s@%.1f %2d", string, (float)sps->level_idc/10.0, sps->num_ref_frames);
                    if (sps->qpprime_y_zero_transform_bypass_flag)
                        info.flags[F_QUANT] = 'l';
                    /* save for a different flag */
#endif
                    if (sps->vui_parameters_present_flag && sps->vui.field_seq_flag)
                        info.flags[F_LACED] = 'i';
                    if (sps->scaling_list_enable_flag)
                        info.flags[F_QUANT] = 's';
                    if (sps->amp_enabled_flag)
                        info.flags[F_MOTION] = 'a';
                    if (sps->sample_adaptive_offset_enabled_flag)
                        info.flags[F_ENTROPY] = 's';
                    switch (sps->bit_depth_luma_minus8) {
                        case 1: info.flags[F_OTHER] = '9'; break;
                        case 2: info.flags[F_OTHER] = '0'; break;
                        case 3: info.flags[F_OTHER] = '1'; break;
                        case 4: info.flags[F_OTHER] = '2'; break;
                    }
                    switch (sps->chroma_format_idc) {
                        case 0 : info.flags[F_CHROMA] = 'm'; break;
                        case 2 : info.flags[F_CHROMA] = '2'; break;
                        case 3 : info.flags[F_CHROMA] = '4'; break;
                    }
                    rvfree(vid.sps[sps->seq_parameter_set_id]);
                    vid.sps[sps->seq_parameter_set_id] = sps;
                    break;
                }

                case PPS_NUT:
                {
                    /* picture parameter set */
                    struct hevc_pic_parameter_set *pps = parse_hevc_pic_param(rbsp, vid.sps); // FIXME support multiple sequences.
                    if (pps==NULL)
                        rvexit("failed to parse picture parameter set header in file \"%s\"", filename);
                    if (verbose>=1)
                        rvmessage("pic_parameter_set_id=%d seq_parameter_set_id=%d", pps->pic_parameter_set_id, pps->seq_parameter_set_id);
                    if (pps->weighted_pred_flag)
                        info.flags[F_MOTION] = 'w';
                    rvfree(vid.pps[pps->pic_parameter_set_id]);
                    vid.pps[pps->pic_parameter_set_id] = pps;

                    break;
                }

                case AUD_NUT:
                    /* access unit delimiter */
                    access_unit++;
                    break;

                case EOS_NUT:
                    /* end of sequence */
                    refresh = 0;
                    break;

                case EOB_NUT:
                    /* end of stream */
                    end_of_stream = 1;
                    break;

                case FD_NUT:
                    /* filler data */
                    break;

                default:
                    if (verbose>=0)
                        rvmessage("unknown nal_unit_type: %d", nal_unit_type);
                    break;
            }

            /* tidy up */
            free_rbsp(rbsp);

        } else {
            break;
        }
    } while (!eofbits(bb));

    /* calc last frame size */
    calc_framesize(info.pictures, finfo, num_elements, numbits(bb)/8);

    /* cross reference frame count to access unit count */
    if (verbose>=1)
        if (access_unit)
            if (access_unit != info.pictures)
                rvmessage("picture count and access unit count are not the same: %d != %d", info.pictures, access_unit);

    /* tidy up */

    return info;
}

struct videoinfo grok_m2v(struct bitbuf *bb, const char *filename, struct frameinfo *finfo, int num_elements, int verbose)
{
    struct videoinfo info = videoblank;

    /* default file information */
    int mpeg2 = 0;
    info.type = "m1v";
    info.subtype = space;

    /* shadow copy of quantiser mactricies */
    int shadow[4][64] = {{0}};

    /* dynamic gop and paff detection */
    int gop_start = 0;
    int gop_length = 0;
    int num_frames = 0;
    int num_fields = 0;

    /* copy of initial f_codes */
    int f_code[2][2] = {{0}};

    do {
        int start;

        /* mpeg2 header fields */
        struct rvm2vsequence sequence;

        /* look for a start code */
        int stuffbytes = 0;
        do {
            start=showbits24(bb);
            if (start==1) {
                flushbits(bb, 24);
                start = getbyte(bb);
                break;
            } else if (eofbits(bb))  {
                start = -1;
                break;
            } else {
                /* we are always byte aligned here */
                if (getbyte(bb)==0)
                    stuffbytes++;
                else
                    stuffbytes = 0;
            }
        } while (1);

        /* lookup start code type */
        if (start==0xb2) {
            parse_m2v_user_data(bb, verbose);
        } else if (start==0xb3) {
            int ret = parse_m2v_sequence(bb, &sequence);
            if (ret<0)
                rvmessage("failed to parse mpeg2 sequence header in file \"%s\"", filename);
            else {
                info.width = sequence.horizontal_size_value;
                info.height = sequence.vertical_size_value;
                info.pixels = sequence.horizontal_size_value;
                info.lines = sequence.vertical_size_value;
                info.framerate = sequence.frame_rate;
                if (sequence.load_intra_quantiser_matrix || sequence.load_non_intra_quantiser_matrix)
                    info.flags[F_QUANT] = 'm';

                /* count stuffing bytes before repeated sequence start code */
                if (info.pictures>0 && info.pictures<num_elements)
                    finfo[info.pictures-1].stuffbytes += stuffbytes;

                /* dump quantisation matricies */
                if (verbose>=1 && sequence.load_intra_quantiser_matrix) {
                    dump_matrix(filename, "sequence intra", num_frames, sequence.intra_quantiser_matrix, shadow[0]);
                    sequence.load_intra_quantiser_matrix = 0;
                }
                if (verbose>=1 && sequence.load_non_intra_quantiser_matrix) {
                    dump_matrix(filename, "sequence inter", num_frames, sequence.non_intra_quantiser_matrix, shadow[1]);
                    sequence.load_non_intra_quantiser_matrix = 0;
                }
            }
        } else if (start==0xb5) { /* extension start code */
            mpeg2 = 1;
            info.type = "m2v";
            /* get extension start code identifier */
            start = getbits(bb, 4);

            /* lookup start code identifier */
            switch (start) {
                int ret;
                case 0x1:   /* sequence extension */
                    ret = parse_m2v_sequence_extension(bb, &sequence);
                    if (ret<0)
                        rvmessage("failed to parse mpeg2 sequence extension header in file \"%s\"", filename);
                    else {
                        const char *p, *l;
                        /* determine profile and level */
                        describe_profile(sequence.profile_and_level, &p, &l);
                        /* determine image format */
                        chromaformat_t format = UNKNOWN;
                        switch (sequence.chroma_format) {
                            case 1: format = YUV420; break;
                            case 2: format = YUV422; break;
                            case 3: format = YUV444; break;
                        }
                        snprintf(space, BLANK_BYTES, "%s@%s %s", p, l, chromaformatname[format]);
                        info.width = sequence.horizontal_size_value;
                        info.height = sequence.vertical_size_value;
                        info.pixels = sequence.horizontal_size_value;
                        info.lines = sequence.vertical_size_value;
                        info.framerate = sequence.frame_rate;
                        if (sequence.progressive_sequence)
                            info.flags[F_LACED] = 'p';
                        else
                            info.flags[F_LACED] = 'i';
                    }
                    break;

                case 0x2:   /* sequence display extension */
                    ret = parse_m2v_sequence_display_extension(bb, &sequence);
                    if (ret<0)
                        rvexit("failed to parse mpeg2 sequence display extension header in file \"%s\"", filename);
                    else {
                        info.pixels = sequence.display_horizontal_size;
                        info.lines = sequence.display_vertical_size;
                    }
                    break;

                case 0x3:   /* quant matrix extension */
                {
                    struct rvm2vquantmatrix quantmatrix;
                    ret = parse_m2v_quant_matrix_extension(bb, &quantmatrix);
                    if (ret<0)
                        rvmessage("failed to parse mpeg2 quant matrix extension header in file \"%s\"", filename);
                    else {
                        if (sequence.load_intra_quantiser_matrix || sequence.load_non_intra_quantiser_matrix ||
                            sequence.load_chroma_intra_quantiser_matrix || sequence.load_chroma_non_intra_quantiser_matrix)
                            info.flags[F_QUANT] = 'm';
                        /* dump quantisation matricies */
                        if (verbose>=1 && sequence.load_intra_quantiser_matrix) {
                            dump_matrix(filename, "picture intra", num_frames-1, sequence.intra_quantiser_matrix, shadow[0]);
                            sequence.load_intra_quantiser_matrix = 0;
                        }
                        if (verbose>=1 && sequence.load_non_intra_quantiser_matrix) {
                            dump_matrix(filename, "picture inter", num_frames-1, sequence.non_intra_quantiser_matrix, shadow[1]);
                            sequence.load_non_intra_quantiser_matrix = 0;
                        }
                        if (verbose>=1 && sequence.load_chroma_intra_quantiser_matrix) {
                            dump_matrix(filename, "picture intra chroma", num_frames-1, sequence.chroma_intra_quantiser_matrix, shadow[2]);
                            sequence.load_chroma_intra_quantiser_matrix = 0;
                        }
                        if (verbose>=1 && sequence.load_chroma_non_intra_quantiser_matrix) {
                            dump_matrix(filename, "picture inter chroma", num_frames-1, sequence.chroma_non_intra_quantiser_matrix, shadow[3]);
                            sequence.load_chroma_non_intra_quantiser_matrix = 0;
                        }
                    }
                    break;
                }

                default:
                    /* realign bistream on unparsed extension */
                    alignbits(bb);
            }
        } else if (start==0xb7) {
            /* sequence end code */
        } else if (start==0xb8) {
            int ret = parse_m2v_group_of_pictures(bb, NULL);
            if (ret<0)
                rvmessage("failed to parse mpeg2 gop header in file \"%s\"", filename);
            else {
                if (info.pictures<num_elements)
                    finfo[info.pictures].start_gop = 1;
                if (info.pictures>0 && info.pictures<num_elements)
                    finfo[info.pictures-1].stuffbytes += stuffbytes;
                /* ignore length of first gop */
                if (gop_start>0 && gop_length==0)
                    gop_length = info.frames-gop_start;
                else if (gop_length != info.frames-gop_start)
                    info.flags[F_FRAME] = 'a';
                gop_start = info.frames;
            }
        } else if (start==0x00) {
            /* note byte count before parsing picture header */
            unsigned int bytes = (numbits(bb) - 32) / 8;

            /* parse picture header */
            struct rvm2vpicture *picture = parse_m2v_picture_header(bb);
            if (picture==NULL)
                rvmessage("failed to parse mpeg2 picture header in file \"%s\"", filename);
            else {
                struct rvm2vextension *extension = &picture->extension;

                /* fill in default picture parameters for mpeg1 */
                if (!mpeg2) {
                    extension->picture_structure = PST_FRAME;
                }

                /* next start code */
                alignbits(bb);
                while (showbits24(bb)!=1 && !eofbits(bb)) {
                    int zero_byte = getbits8(bb);
                    if (zero_byte)
                        rvmessage("invalid value of zero_byte in next_start_code: %d", zero_byte);
                }

                if (mpeg2) {
                    /* parse start code */
                    start = getbits32(bb);
                    if (start!=0x1b5)
                        rvmessage("did not find mpeg2 picture coding extension after picture header: %08x", start);
                    start = getbits(bb, 4);
                    if (start!=0x8)
                        rvmessage("did not find mpeg2 picture coding extension identifier: %1x", start);

                    /* parse picture coding extension */
                    int ret = parse_m2v_picture_extension(bb, extension);
                    if (ret<0)
                        rvmessage("failed to parse mpeg2 picture coding extension header in file \"%s\"", filename);
                }

                /* store picture information */
                if (info.pictures<num_elements) {

                    /* store frame information */
                    switch (picture->picture_coding_type) {
                        case PCT_I: finfo[info.pictures].frame_type = I_FRAME; break;
                        case PCT_P: finfo[info.pictures].frame_type = P_FRAME; break;
                        case PCT_B: finfo[info.pictures].frame_type = B_FRAME; break;
                        case PCT_D: finfo[info.pictures].frame_type = D_FRAME; break;
                    }

                    /* store field information */
                    switch (extension->picture_structure) {
                        case 1: finfo[info.pictures].field_type = TOP_FIELD; break;
                        case 2: finfo[info.pictures].field_type = BOT_FIELD; break;
                        case 3: finfo[info.pictures].field_type = FRAME;     break;
                    }

                    finfo[info.pictures].position = bytes;
                    finfo[info.pictures].order = picture->temporal_reference;
                    finfo[info.pictures].delay = picture->vbv_delay;
                }

                /* count picture types */
                if (extension->picture_structure==3)
                    num_frames++;
                else
                    num_fields++;

                /* update flags */
                switch (picture->picture_coding_type) {
                    case PCT_I:
                        /* don't overwrite p- or b-picture flag */
                        if (info.flags[F_FRAME]=='-')
                            info.flags[F_FRAME] = 'i';
                        break;

                    case PCT_P:
                        /* don't overwrite b-picture flag */
                        if (info.flags[F_FRAME]!='b')
                            info.flags[F_FRAME] = 'p';
                        break;

                    case PCT_B:
                        info.flags[F_FRAME] = 'b';
                        break;

                    default:
                        break;
                }

                /* look for dynamic f_code */
                if (mpeg2) {
                    if (picture->picture_coding_type!=PCT_I) {
                        if (f_code[0][0]==0) {
                            f_code[0][0] = extension->f_code[0][0];
                            f_code[0][1] = extension->f_code[0][1];
                        } else if (f_code[0][0] != extension->f_code[0][0])
                            info.flags[F_MOTION] = 'd';
                        else if (f_code[0][1] != extension->f_code[0][1])
                            info.flags[F_MOTION] = 'd';
                        if (picture->picture_coding_type==PCT_B) {
                            if (f_code[1][0]==0) {
                                f_code[1][0] = extension->f_code[1][0];
                                f_code[1][1] = extension->f_code[1][1];
                            } else if (f_code[1][0] != extension->f_code[1][0])
                                info.flags[F_MOTION] = 'd';
                            else if (f_code[1][1] != extension->f_code[1][1])
                                info.flags[F_MOTION] = 'd';
                        }
                    }
                }
                if (picture->vbv_delay!=0xffff)
                    info.flags[F_OTHER] = 'v';

                /* store previous picture information */
                calc_framesize(info.pictures, finfo, num_elements, bytes);
                if (info.pictures>0 && info.pictures<num_elements)
                    finfo[info.pictures-1].stuffbytes += stuffbytes;

                info.pictures++;
                if (mpeg2==0 || (extension->picture_structure==1 && !extension->top_field_first) || (extension->picture_structure==2 && extension->top_field_first) || extension->picture_structure==3)
                    info.frames++;

                free_m2v_picture(picture);
            }
        } else if (start >= 0x01 && start <= 0xaf) {
            /* info.pictures is incremented at picture start code */
            int index = info.pictures-1;

            /* check index is in range in case a slice is encountered before a picture start code */
            if (index>=0 && index<num_elements) {
                /* count slices in picture */
                finfo[index].slices++;
                /* count stuffing bytes between slices */
                finfo[index].stuffbytes += stuffbytes;
            }
        } else if (start<0) {
            break;
        } else {
            if (verbose>=1)
                rvmessage("unknown start code: %02x", start);
        }

        /* align bitstream */
        int align = alignbits(bb);
        if (start!=0x00)
            if (align!=0)
                rvmessage("stuffing bits not correct: 0x%x", align);
    } while (!eofbits(bb));

    /* update flags */
    if (num_frames && num_fields)
        info.flags[F_LACED] = 'a';
    if (num_fields%2==1)
        info.flags[F_ERROR] = 'f';

    /* calc last frame size */
    calc_framesize(info.pictures, finfo, num_elements, numbits(bb)/8);

    /* tidy up */

    return info;
}

struct videoinfo grok_m4v(struct bitbuf *bb, const char *filename, int verbose, struct frameinfo *finfo, int num_elements)
{
    struct videoinfo info = videoblank;

    /* default file information */
    info.type = "m4v";
    info.subtype = space;

    /* default header fields */
    int version = 1;
    int resyncbits = 0;

    /* look for a start code or resync marker */
    int profile, packets = 0;
    do {
        /* mpeg4 header fields */
        int video_object_shape;
        int vti_size;
        int interlaced;
        int sprite_enable;
        int scalability;
        int enhancement_type;
        int ce_disable;
        struct ce_header ce;
        int vop_coding_type;

        /* look for a start code */
        int start=showbits32(bb);

        /* lookup start code type */
        if (start>=0x100 && start<=0x11f) {
            if (verbose>=1)
                rvmessage("video object id=%d", start-0x100);
            flushbits(bb, 32);
            parse_vo(bb);
        } else if (start>=0x120 && start<=0x12f) {
            int video_object_type;
            int video_object_verid;
            int obmc_disable;
            int quant_type;
            int quarter_sample;
            int data_partitioned;
            int rvlc;
            if (verbose>=1)
                rvmessage("video object layer=%d", start-0x120);
            flushbits(bb, 32);
            int ret = parse_vol(bb, &video_object_type, &video_object_verid, &video_object_shape,
                                &vti_size, &info.width, &info.height, &interlaced, &obmc_disable, &sprite_enable,
                                &quant_type, &quarter_sample, &data_partitioned, &rvlc,
                                &scalability, &enhancement_type, &ce_disable, &ce);
            if (ret<0)
                rvexit("failed to parse mpeg4 vol header in file \"%s\"", filename);
            info.pixels = info.width;
            info.lines = info.height;
            if (video_object_type>=10)
                version = 2;
            if (video_object_verid==2)
                version = 2;
            if (interlaced)
                info.flags[F_LACED] = 'i';
            else
                info.flags[F_LACED] = 'p';
            if (!obmc_disable)
                info.flags[F_OTHER] = 'o';
            if (quant_type)
                info.flags[F_QUANT] = '1';
            if (quarter_sample)
                info.flags[F_MOTION] = 'q';
            if (data_partitioned)
                info.flags[F_ERROR] = 'p';
            if (data_partitioned && rvlc)
                info.flags[F_ERROR] = 'r';
            if (verbose>=1)
                rvmessage("vop_time_increment size=%d", vti_size);
        } else if (start==0x1b0) {
            flushbits(bb, 32);
            parse_visual_object_sequence(bb, &profile);
            if (verbose>=1)
                rvmessage("video object sequence, profile=%d", profile);
        } else if (start==0x1b2) {
            int bytes;
            char user_data[32] = {0};
            flushbits(bb, 32);
            parse_user_data(bb, &bytes, user_data, 32);
            if (verbose>=1)
                rvmessage("user data, %d bytes: %s", bytes, user_data);
        } else if (start==0x1b3) {
            flushbits(bb, 32);
            parse_group_of_vop(bb);
            if (verbose>=1)
                rvmessage("group of vop");
        } else if (start==0x1b5) {
            int visual_object_verid, visual_object_type;
            const char *lookup[5] = {"reserved", "video", "texture", "mesh", "face"};
            flushbits(bb, 32);
            parse_visual_object(bb, &visual_object_verid, &visual_object_type);
            if (verbose>=1)
                rvmessage("visual object, verid=%d, type=%s", visual_object_verid, lookup[visual_object_type]);
            if (visual_object_verid==2)
                version = 2;
        } else if (start==0x1b6) {
            int vti, fcode;

            /* note byte count before parsing vop header */
            int bytes = (numbits(bb) - 32) / 8;

            /* parse vop header */
            flushbits(bb, 32);
            int ret = parse_vop(bb, vti_size, video_object_shape, sprite_enable, scalability, enhancement_type,
                                ce_disable, &ce, interlaced, &vop_coding_type, &vti, &fcode);
            if (ret<0)
                rvexit("failed to parse mpeg4 vol header in file \"%s\"", filename);

            /* store frame information */
            if (info.frames<num_elements) {
                switch (vop_coding_type) {
                    case VOP_I: finfo[info.frames].frame_type = I_FRAME; break;
                    case VOP_P: finfo[info.frames].frame_type = P_FRAME; break;
                    case VOP_B: finfo[info.frames].frame_type = B_FRAME; break;
                    case VOP_S: finfo[info.frames].frame_type = S_FRAME; break;
                }
            }
            finfo[info.frames].slices = packets? packets+1 : 0;

            /* store previous frame information */
            calc_framesize(info.frames, finfo, num_elements, bytes);

            /* update resync codeword length */
            resyncbits = vop_coding_type==VOP_I? 17 : 16+fcode;

            /* update flags */
            switch (vop_coding_type) {
                case VOP_I:
                    /* don't overwrite p- or b-vop flag */
                    if (info.flags[F_FRAME]=='-')
                        info.flags[F_FRAME] = 'i';
                    break;

                case VOP_P:
                    /* don't overwrite b-vop flag */
                    if (info.flags[F_FRAME]!='b')
                        info.flags[F_FRAME] = 'p';
                    break;

                case VOP_B:
                    info.flags[F_FRAME] = 'b';
                    break;
            }

            info.frames++;
            packets = 0;
        }
        /* look for a video packet header */
        else if (resyncbits && showbits(bb, resyncbits)==0x00001) {
            flushbits(bb, resyncbits);
            int ret = parse_vp(bb, video_object_shape, sprite_enable, vop_coding_type, vti_size);
            if (ret<0)
                rvexit("failed to parse mpeg4 video packet header in file \"%s\"", filename);
            if (info.flags[F_ERROR]=='-')
                info.flags[F_ERROR] = 'm';
            packets++;
            alignbits(bb);
            continue;
        } else {
            flushbits(bb, 8);
            continue;
        }

        /* align bitstream */
        int align = alignbits(bb);
        if (start!=0x1b6)
            if (align!=0  && align!=1  && align!=3  && align!=7 && align!=15 && align!=31 && align!=63 && align!=127)
                rvmessage("stuffing bits not correct: 0x%x", align);
    } while (!eofbits(bb));

    /* prepare subtype string */
    if (profile>=1 && profile<= 3)
        sprintf(space, "v%d SP@L%d", version, profile-1);
    else if (profile>=17 && profile<=18)
        sprintf(space, "v%d SSP@L%d", version, profile-17);
    else if (profile>=33 && profile<=34)
        sprintf(space, "v%d CP@L%d", version, profile-33);
    else if (profile>=50 && profile<=52)
        sprintf(space, "v%d MP@L%d", version, profile-50);
    else if (profile==66)
        sprintf(space, "v%d NBP@L2", version);
    else if (profile==81)
        sprintf(space, "v%d STP@L1", version);
    else if (profile>=97 && profile<=98)
        sprintf(space, "v%d SFAP@L%d", version, profile-97);
    else if (profile>=113 && profile<=114)
        sprintf(space, "v%d BATP@L%d", version, profile-113);
    else if (profile>=129 && profile<=130)
        sprintf(space, "v%d HP@L%d", version, profile-129);
    else if (profile>=145 && profile<=148)
        sprintf(space, "v%d ARTSP@L%d", version, profile-145);
    else if (profile>=161 && profile<=163)
        sprintf(space, "v%d CSP@L%d", version, profile-161);
    else if (profile>=177 && profile<=180)
        sprintf(space, "v%d ACEP@L%d", version, profile-177);
    else if (profile>=193 && profile<=194)
        sprintf(space, "v%d ACP@L%d", version, profile-193);
    else if (profile>=209 && profile<=211)
        sprintf(space, "v%d ACTP@L%d", version, profile-209);
    else if (profile>=240 && profile<=245)
        sprintf(space, "v%d ASP@L%d", version, profile-240);
    else if (profile==247)
        sprintf(space, "v%d ASP@L3b", version);
    else
        sprintf(space, "v%d", version);

    /* calc last frame size */
    calc_framesize(info.pictures, finfo, num_elements, numbits(bb)/8);

    /* tidy up */

    return info;
}

struct videoinfo grok_jpeg(struct bitbuf *bb, const char *filename, int verbose)
{
    struct videoinfo info = videoblank;

    /* default file information */
    info.type = "jpeg";
    info.subtype = "";

    int marker;
    int scan = 0;
    int restart = 0;
    struct huffman_table table[2][4];
    /* look for markers */
    do {
        int len;
        chromaformat_t format;

        marker = showbits16(bb);
        switch (marker) {
            case 0xffc0: /* SOF0 */
                flushbits(bb, 16);
                len = parse_sof(bb, &info.width, &info.height, &format);
                if (len!=0)
                    rvmessage("warning: sof did not parse correctly: %d bits", len);
                info.pixels = info.width;
                info.lines = info.height;
                switch (format) {
                    case YUV400: info.subtype = "base mono"; break;
                    case YUV420: info.subtype = "base 420"; break;
                    case YUV422: info.subtype = "base 422"; break;
                    default    : info.subtype = "baseline"; break;
                }
                break;

            case 0xffc1: flushbits(bb, 16); parse_sof(bb, &info.width, &info.height, &format); info.subtype = "extended";  break; /* SOF1 */
            case 0xffc2: flushbits(bb, 16); parse_sof(bb, &info.width, &info.height, &format); info.subtype = "progress";  break; /* SOF2 */
            case 0xffc3: flushbits(bb, 16); parse_sof(bb, &info.width, &info.height, &format); info.subtype = "lossless";  break; /* SOF3 */
            case 0xffc5: flushbits(bb, 16); parse_sof(bb, &info.width, &info.height, &format); info.subtype = "diff sequ"; break; /* SOF5 */
            case 0xffc6: flushbits(bb, 16); parse_sof(bb, &info.width, &info.height, &format); info.subtype = "diff prog"; break; /* SOF6 */
            case 0xffc7: flushbits(bb, 16); parse_sof(bb, &info.width, &info.height, &format); info.subtype = "diff loss"; break; /* SOF7 */
            case 0xffc8: flushbits(bb, 16); parse_sof(bb, &info.width, &info.height, &format); info.subtype = "reserved";  break; /* SOF8 */
            case 0xffc9: flushbits(bb, 16); parse_sof(bb, &info.width, &info.height, &format); info.subtype = "arith ext"; break; /* SOF9 */
            case 0xffca: flushbits(bb, 16); parse_sof(bb, &info.width, &info.height, &format); info.subtype = "arith pro"; break; /* SOF10 */
            case 0xffcb: flushbits(bb, 16); parse_sof(bb, &info.width, &info.height, &format); info.subtype = "arith los"; break; /* SOF11 */

            case 0xffc4: /* DHT */
                flushbits(bb, 16);
                len = parse_dht(bb, &table);
                if (len!=0)
                    rvmessage("warning: dht did not parse correctly: %d bits left", len);
                break;

            case 0xffd9: /* EOI */
                flushbits(bb, 16);
                info.frames++;
                break;

            case 0xffda: /* SOS */
                if (info.frames==0)
                    scan++;
                flushbits(bb, 16);
                break;

            //case 0xffdc: flushbits(bb, 16); flushbits(bb, 16); height = getbits(bb, 16); break;          /* DNL */

            case 0xffdd: /* DRI */
                flushbits(bb, 16);
                flushbits(bb, 16);
                restart = getbits(bb, 16);
                break;

            default:
                /* no marker, move along */
                flushbits(bb, 8);
                break;
        }
    } while (!eofbits(bb));

    /* test huffman tables against annexk */
    int match = 1;
    int tableclass, destination;
    int i, j;
    for (tableclass=0; tableclass<2 && match==1; tableclass++) {
        for (destination=0; destination<2 && match==1; destination++) {
            for (i=0; i<16 && match==1; i++) {
                if (table[tableclass][destination].l[i] != annexk[tableclass][destination].l[i]) {
                    if (verbose>=1)
                        printf("huffman mismatch: class=%d destination=%d length=%d: l=%d l=%d\n",
                               tableclass, destination, i, table[tableclass][destination].l[i], annexk[tableclass][destination].l[i]);
                    match = 0;
                    break;
                }
                for (j=0; j<table[tableclass][destination].l[i]; j++)
                    if (table[tableclass][destination].v[i][j] != annexk[tableclass][destination].v[i][j])
                        match = 0;
            }
        }
    }

    /* set flags */
    if (scan>1)
        info.flags[F_OTHER] = 'n'; /* multiple scans */
    if (restart>0)
        info.flags[F_ERROR] = 'r'; /* restart markers */
    if (!match)
        info.flags[F_ENTROPY] = 'h'; /* custom huffman tables */

    /* tidy up */

    return info;
}

struct videoinfo grok_real(struct bitbuf *bb, const char *filename)
{
    struct videoinfo info = videoblank;

    /* default file information */
    info.type = "real";
    info.subtype = space;

    /* parse realmedia file */
    int done = 0;
    do {
        /* look for next chunk */
        int chunktype = showbits32(bb);
        int chunksize;

        switch (chunktype) {
            case 0x2e524d46 : /* .RMF */
                flushbits(bb, 32);                  /* chunk type */
                chunksize = getbits32(bb);          /* chunk size */
                flushbits(bb, 32);                  /* chunk version */
                if (chunksize==0x12)
                    flushbits(bb, 32);              /* file version */
                else
                    flushbits(bb, 16);
                break;
                flushbits(bb, 32);                  /* number of headers */
            case 0x50524f50 : /* PROP */
                flushbits(bb, 32);                  /* chunk type */
                break;
            case 0x434f4e54 : /* CONT */
                flushbits(bb, 32);                  /* chunk type */
                break;
            case 0x4d445052 : /* MDPR */
                flushbits(bb, 32);                  /* chunk type */
                break;
            case 0x5649444f : /* VIDO */
                flushbits(bb, 32);                  /* chunk type */
                rvmessage("%08x %08x", showbits32(bb), BTOH32(RV40));
                switch (getbits32(bb)) {
                    case BTOH32(RV10) : info.subtype = "RV10"; break;
                    case BTOH32(RV13) : info.subtype = "RV13"; break;
                    case BTOH32(RV20) : info.subtype = "RV20"; break;
                    case BTOH32(RV30) : info.subtype = "RV30"; break;
                    case BTOH32(RV40) : info.subtype = "RV40"; break;
                }
                break;
            case 0x44415441 : /* DATA */
                done = 1;
                break;
            default:
                /* no chuck type here so move along */
                flushbits(bb, 8);
                break;
        }

    } while (!eofbits(bb) && !done);

    /* file information */

    /* tidy up */

    return info;
}

struct videoinfo grok_vc1(struct bitbuf *bb, const char *filename)
{
    struct videoinfo info = videoblank;

    /* default file information */
    info.type = "vc1";
    info.subtype = space;

    /* parse vc-1 file */
    int profile = 0;
    int level = 0;
    do {
        int ret;

        /* look for a start code */
        int start=showbits32(bb);

        /* lookup start code type */
        switch (start) {
            case 0x10f: /* sequence start code */
                flushbits(bb, 32);
                ret = parse_sequence_layer(bb, &profile, &level, &info.width, &info.height);
                if (ret<0)
                    rvexit("failed to parse vc-1 sequence header in file \"%s\"", filename);
                info.pixels = info.width;
                info.lines = info.height;
                break;

            case 0x10a: /* end of sequence start code */
                flushbits(bb, 32);
                break;

            case 0x10e: /* entry point start code */
                flushbits(bb, 32);
                break;

            case 0x10d: /* frame start code */
                flushbits(bb, 32);
                break;

            case 0x10c: /* field start code */
                flushbits(bb, 32);
                break;

            case 0x10b: /* slice start code */
                flushbits(bb, 32);
                break;

            case 0x11f: /* sequence level user data */
                flushbits(bb, 32);
                break;

            case 0x11e: /* entry level user data */
                flushbits(bb, 32);
                break;

            case 0x11d: /* frame level user data */
                flushbits(bb, 32);
                break;

            case 0x11c: /* field level user data */
                flushbits(bb, 32);
                break;

            case 0x11b: /* slice level user data */
                flushbits(bb, 32);
                break;

            default:
                /* no start code, move along */
                flushbits(bb, 8);
                break;
        }
    } while (!eofbits(bb));

    /* prepare subtype string */
    if (profile==2)
        sprintf(space, "AP@L%d", level);

    /* tidy up */

    return info;
}

struct videoinfo grok_rcv(struct bitbuf *bb, const char *filename)
{
    struct videoinfo info = videoblank;

    /* default file information */
    info.type = "vc1";
    info.subtype = space;

    /* parse vc-1 rcv file */
    info.frames = swapendian3(getbits(bb, 24));
    int constant = getbits8(bb);
    if (constant!=0xc5)
        rvmessage("warning: expected constant 0xc5 in vc-1 rcv file: 0x%02x", constant);
    constant = swapendian(getbits32(bb));
    if (constant!=0x00000004)
        rvmessage("warning: expected constant 0x00000004 in vc-1 rcv file: 0x%08x", constant);
    int profile = getbits(bb, 4);
    if (profile==0 || profile==4) {
        /* simple and main profile */
        flushbits(bb, 3);               /* frmrtq_postproc */
        flushbits(bb, 5);               /* bitrtq_postproc */
        int loopfilter = getbit(bb);    /* loopfilter */
        flushbits(bb, 1);               /* reserved3 */
        flushbits(bb, 1);               /* multires */
        flushbits(bb, 1);               /* reserved4 */
        flushbits(bb, 1);               /* fastuvmc */
        int extended_mv = getbit(bb);   /* extended_mv */
        int dquant = getbits(bb, 2);    /* dquant */
        flushbits(bb, 1);               /* vstransform */
        flushbits(bb, 1);               /* reserved5 */
        flushbits(bb, 1);               /* overlap */
        int syncmarker = getbit(bb);    /* syncmarker */
        flushbits(bb, 1);               /* rangered */
        flushbits(bb, 3);               /* maxbframes */
        flushbits(bb, 2);               /* quantizer */
        flushbits(bb, 1);               /* finterpflag */
        flushbits(bb, 1);               /* reserved6 */

        /* set flags */
        if (loopfilter)
            info.flags[F_OTHER] = 'l';
        if (extended_mv)
            info.flags[F_MOTION] = 'e';
        if (dquant==1 || dquant==2)
            info.flags[F_QUANT] = 'd';
        if (syncmarker)
            info.flags[F_ERROR] = 'm';
    } else if (profile==12) {
        /* advanced profile */
        flushbits(bb, 28);              /* reserved7 */
    } else {
        rvmessage("warning: unknown profile in vc-1 rcv file: %d", profile);
    }
    info.height = swapendian(getbits32(bb));
    info.width = swapendian(getbits32(bb));
    info.lines = info.height;
    info.pixels = info.width;
    constant = getbits32(bb);
    if (constant!=0x0c000000)
        rvmessage("warning: expected constant 0x0000000c in vc-1 rcv file: 0x%08x", swapendian(constant));
    int level = 0;
    if (profile==0 || profile==4) {
        /* simple and main profile */
        level = getbits(bb, 3);
        flushbits(bb, 1);                   /* cbr */
        flushbits(bb, 4);                   /* res1 */
        flushbits(bb, 24);                  /* hrd_buffer */
        flushbits(bb, 32);                  /* hrd_rate */
        flushbits(bb, 32);                  /* framerate */
    } else if (profile==12) {
        /* advanced profile */
        level = getbits(bb, 3);
        flushbits(bb, 1);                   /* cbr */
        flushbits(bb, 4);                   /* res1 */
        flushbits(bb, 56-32);
        flushbits(bb, 32);                  /* res2 */
        flushbits(bb, 32);                  /* framerate */
    }

    /* prepare subtype string */
    if (profile==12)
        sprintf(space, "AP@L%d", level);
    else {
        const char *p="", *l="";
        if (profile==0)
            p = "SP";
        if (profile==4)
            p = "MP";
        if (level==0)
            l = "LL";
        if (level==2)
            l = "ML";
        if (level==4)
            l = "HL";
        sprintf(space, "%s@%s", p, l);
    }

    /* tidy up */

    return info;
}

struct videoinfo grok_ivf(struct bitbuf *bb, const char *filename)
{
    struct videoinfo info = videoblank;

    /* default file information */
    info.type = "ivf";
    info.subtype = space;

    /* parse av1 ivf file */
    flushbits(bb, 32);              /* signature: DKIF */
    flushbits(bb, 16);              /* version */
    flushbits(bb, 16);              /* length of header in bytes */
    int fourcc = getdword(bb);      /* codec fourcc */
    memcpy(space, (void *)&fourcc, 4);  /* everything is little endian, right? */
    info.width = getword(bb);       /* width in pixels */
    info.height = getword(bb);      /* height in pixels */
    info.framerate = getdword(bb);  /* frame rate */
    info.framerate /= getdword(bb); /* frame rate time scale */
    info.frames = getdword(bb);     /* number of frames */

    return info;
}

struct audioinfo grok_au(struct bitbuf *bb, const char *filename)
{
    struct audioinfo info = audioblank;

    /* default file information */
    info.type = "au";
    info.subtype = "";

    /* read au file header */
    flushbits(bb, 32);
    flushbits(bb, 32);
    flushbits(bb, 32);
    int encoding = getbits32(bb);
    info.samples = getbits32(bb);
    info.channels = getbits32(bb);

    /* set encoding */
    switch (encoding) {
        case 1 : info.bits = 8;  info.subtype="u-law"; break;
        case 2 : info.bits = 8;  info.subtype="pcm"; break;
        case 3 : info.bits = 16; info.subtype="pcm"; break;
        case 4 : info.bits = 24; info.subtype="pcm"; break;
        case 5 : info.bits = 32; info.subtype="pcm"; break;
        case 6 : info.bits = 32; info.subtype="float"; break;
        case 7 : info.bits = 64; info.subtype="float"; break;
        case 23: info.bits = 4;  info.subtype="g.721"; break;
        case 24: info.bits = 4;  info.subtype="g.722"; break;
        case 25: info.bits = 4;  info.subtype="g.723"; break;
        case 26: info.bits = 5;  info.subtype="g.723"; break;
        case 27: info.bits = 8;  info.subtype="A-law"; break;
    }

    /* tidy up */

    return info;
}

struct rvwav {
    u_int32_t type;
    u_int32_t size;
    u_int16_t format;
    u_int16_t channels;
    u_int32_t samples;
    u_int32_t byterate;
    u_int16_t blockalign;
    u_int16_t bits;
}__attribute__((packed));

struct audioinfo grok_wav(struct bitbuf *bb, const char *filename)
{
    FILE *filein = stdin;
    struct audioinfo info = audioblank;

    /* open wave file */
    if (filename)
        filein = fopen(filename, "rb");
    if (filein==NULL)
        rverror("failed to open wave file \"%s\"", filename);

    /* default file information */
    info.type = "wav";
    info.subtype = space;

    /* skip initial offset */
    char skip[12];
    if (fread(&skip, 12, 1, filein)!=1)
        rverror("failed to skip 12 bytes in wave file");

    /* read wave file header */
    /* this will only work on a little-endian machine I think */
    struct rvwav wav;
    if (fread(&wav, sizeof(struct rvwav), 1, filein)!=1)
        rverror("failed to read wave header from input");

    /* file information */
    info.samples = wav.samples;
    info.bits = wav.bits;
    info.channels = wav.channels;

    /* subtype information */
    switch (wav.format) {
        case 1    : info.subtype = "pcm"; break;
        case 2    : info.subtype = "ms adpcm"; break;
        case 3    : info.subtype = "ieee float"; break;
        case 6    : info.subtype = "a-law"; break;
        case 7    : info.subtype = "u-law"; break;
        case 17   : info.subtype = "ima adpcm"; break;
        case 20   : info.subtype = "g.723"; break;
        case 34   : info.subtype = "truespeech"; break;
        case 48   : info.subtype = "ac2"; break;
        case 49   : info.subtype = "gsm 6.10"; break;
        case 64   : info.subtype = "g.721"; break;
        case 80   : info.subtype = "mpeg"; break;
        case 85   : info.subtype = "mp3"; break;
        case 6172 : info.subtype = "voxware"; break;
        case 65534: info.subtype = "extensible"; break;
        case 65535: info.subtype = "development"; break;
        default   : snprintf(space, sizeof(space), "%d", wav.format); info.subtype = space; break;
    }

    /* tidy up */
    if (filename)
        fclose(filein);
    return info;
}

struct audioinfo grok_mpa(struct bitbuf *bb, const char *filename)
{
    struct audioinfo info = audioblank;

    /* default file information */
    info.type = "mpa";
    info.subtype = space;

    /* read frame header */
    flushbits(bb, 11);                          /* frame sync */
    int mpeg_version = 4 - getbits(bb, 2);      /* mpeg audio version */
    int mpeg_layer = 4 - getbits(bb, 2);        /* mpeg layer */
    int protection_bit = getbit(bb);            /* protection bits */
    int bit_rate_index = getbits(bb, 4);        /* bitrate index */
    int sampling_index = getbits(bb, 2);        /* sampling rate index */
    flushbits(bb, 1);                           /* padding bit */
    flushbits(bb, 1);                           /* private bit */
    int channel_mode = getbits(bb, 2);          /* channel mode */
    flushbits(bb, 9);

    switch (mpeg_layer) {
        case 1: info.type = "mp1"; break;
        case 2: info.type = "mp2"; break;
        case 3: info.type = "mp3"; break;
    }
    if (mpeg_version==2)
        info.subtype = "v2";
    if (mpeg_version==4)
        info.subtype = "v2.5";
    info.samples = m2a_sampling_rate_table[mpeg_version-1][sampling_index];
    info.bits = 16;
    info.bitrate = m2a_bit_rate_table[mpeg_version>1][mpeg_layer-1][bit_rate_index]*1000;
    info.channels = channel_mode==3? 1 : 2;

    /* set flags */
    if (protection_bit)
        info.flags[F_ERROR] = 'c';

    return info;
}

struct audioinfo grok_ac3(struct bitbuf *bb, const char *filename)
{
    struct audioinfo info = audioblank;

    /* default file information */
    info.type = "ac3";
    info.subtype = space;

    /* look for the start of a syncframe() */
    while (showbits16(bb)!=0x0b77)
        flushbits(bb, 8);

    /* read synchronisation information */
    flushbits(bb, 16);                          /* synchronisation word */
    flushbits(bb, 16);                          /* crc */
    int fscod = getbits(bb, 2);                 /* sample rate code */
    int frmsizecod = getbits(bb, 6);            /* frame size code */

    /* read bit stream information */
    flushbits(bb, 5);                           /* bit stream identification */
    flushbits(bb, 3);                           /* bit stream mode */
    int acmod = getbits(bb, 3);                 /* audio coding mode */
    if ((acmod&0x1) && (acmod!=0x1))            /* if 3 front channels */
        flushbits(bb, 2);
    if (acmod & 0x4)                            /* if a surround channel exists */
        flushbits(bb, 2);
    if (acmod == 0x2)                           /* if in 2/0 mode */
        flushbits(bb, 2);
    int lfeon = getbit(bb);

    info.bits = 16;
    switch (fscod) {
        case 2: info.samples = 32000; break;
        case 1: info.samples = 44100; break;
        case 0: info.samples = 48000; break;
    }
    switch (acmod) {
        case 0: info.channels = 2; break;
        case 1: info.channels = 1; break;
        case 2: info.channels = 2; break;
        case 3: info.channels = 3; break;
        case 4: info.channels = 3; break;
        case 5: info.channels = 4; break;
        case 6: info.channels = 4; break;
        case 7: info.channels = 5; break;
    }
    info.lfe = lfeon;
    switch (frmsizecod>>1) {
        case 0 : info.bitrate =  32000; break;
        case 1 : info.bitrate =  40000; break;
        case 2 : info.bitrate =  48000; break;
        case 3 : info.bitrate =  56000; break;
        case 4 : info.bitrate =  64000; break;
        case 5 : info.bitrate =  80000; break;
        case 6 : info.bitrate =  96000; break;
        case 7 : info.bitrate = 112000; break;
        case 8 : info.bitrate = 128000; break;
        case 9 : info.bitrate = 160000; break;
        case 10: info.bitrate = 192000; break;
        case 11: info.bitrate = 224000; break;
        case 12: info.bitrate = 256000; break;
        case 13: info.bitrate = 320000; break;
        case 14: info.bitrate = 384000; break;
        case 15: info.bitrate = 448000; break;
        case 16: info.bitrate = 512000; break;
        case 17: info.bitrate = 576000; break;
        case 18: info.bitrate = 640000; break;
    }

    return info;
}

void print_gop_structure(int pictures, struct frameinfo *finfo, FILE *fileout)
{
    int i, bottom = 0;
    int n = mmin(pictures, 79);
    char *gopstring = (char *)rvalloc(NULL, n+2*sizeof(char), 0);

    char lookup[6] = {'?', 'I', 'P', 'B', 'S', 'R'};

    /* print frame/top field pictures */
    for (i=0; i<n; i++) {
        if (finfo[i].field_type==FRAME) {
            gopstring[i] = lookup[finfo[i].frame_type];
        } else if (finfo[i].field_type==TOP_FIELD) {
            gopstring[i] = lookup[finfo[i].frame_type];;
            bottom = 1;
        } else {
            gopstring[i] = ' ';
            bottom = 1;
        }
    }
    gopstring[i]   = '\n';
    gopstring[i+1] = '\0';
    fputs(gopstring, fileout);

    /* print bottom field pictures */
    if (bottom) {
        for (i=0; i<n; i++) {
            if (finfo[i].field_type==FRAME) {
                gopstring[i] = ' ';
            } else if (finfo[i].field_type==TOP_FIELD) {
                gopstring[i] = ' ';
            } else {
                gopstring[i] = lookup[finfo[i].frame_type];;
            }
        }
        gopstring[i]   = '\n';
        gopstring[i+1] = '\0';
        fputs(gopstring, fileout);
    }

    rvfree(gopstring);
}

int main(int argc, char *argv[])
{
    const char *filename[RV_MAXFILES] = {0};
    int fileindex = 0;
    int numfiles = 0;

    /* command line defaults */
    char *forcetype = NULL;
    float fps = 30.0;
    int basename = 0;
    int rawdata = 0;
    int description = 0;
    int perpicture = 0;
    int perslice = 0;
    int gopstructure = 0;
    int num_elements = 8192;
    int verbose = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"type",          1, NULL, 't'},
            {"fps",           1, NULL, 'f'},
            {"basename",      0, NULL, 'b'},
            {"raw",           0, NULL, 'r'},
            {"description",   0, NULL, 'd'},
            {"picture",       0, NULL, 'p'},
            {"slice",         0, NULL, 's'},
            {"gop-structure", 0, NULL, 'g'},
            {"quiet",         0, NULL, 'q'},
            {"verbose",       0, NULL, 'v'},
            {"usage",         0, NULL, 'h'},
            {"help",          0, NULL, 'h'},
            {NULL,            0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "t:f:brdpsgqvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 't':
                forcetype = optarg;
                break;

            case 'f':
                fps = atof(optarg);
                if (fps<0.0001 || fps>1000.0)
                    rvexit("invalid value for frames per second: %f", fps);
                break;

            case 'b':
                basename = 1;
                break;

            case 'r':
                rawdata = 1;
                break;

            case 'd':
                description = 1;
                break;

            case 'p':
                perpicture = 1;
                break;

            case 's':
                perslice = 1;
                break;

            case 'g':
                gopstructure = 1;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < RV_MAXFILES)
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    /* use filein if no input filenames */
    if (numfiles==0)
        filename[0] = "-";

    /* loop over input files */
    do {

        /* clear subtype */
        strncpy(space, "         ", BLANK_BYTES);

        /* allocate storage for per-frame information */
        struct frameinfo *finfo = (struct frameinfo *)rvalloc(NULL, num_elements*sizeof(struct frameinfo), 1);

        /* allocate storage for per-slice information */
        struct sliceinfo *sinfo = (struct sliceinfo *)rvalloc(NULL, num_elements*sizeof(struct sliceinfo), 1);

        /* stat file */
        struct stat st;
        memset(&st, 0, sizeof(st));
        if (numfiles==0) {
            if (fstat(0, &st)<0)
                rverror("failed to stat standard input");
        } else {
            if (stat(filename[fileindex], &st)<0)
                rverror("failed to stat file \"%s\"", filename[fileindex]);
        }

        /* skip directories */
        if (S_ISDIR(st.st_mode)) {
            rvmessage("skipping directory \"%s\"", basename? get_basename(filename[fileindex]) : filename[fileindex]);
            continue;
        }

        /* prepare to parse file */
        struct bitbuf *bb = initbits_filename(filename[fileindex],  B_GETBITS);
        if (bb==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex]);

        /* determine filetype */
        divine_t divine = divine_filetype(bb, filename[fileindex], forcetype, verbose);
        if (rewindbits(bb)<0)
            rverror("searched too far on non-seekable input file \"%s\"", filename[fileindex]);

        /* check for empty files */
        if (st.st_size==0 && !S_ISFIFO(st.st_mode)) {
            fprintf(stdout, "--------    0x0   @0.0    empty                0/0                  0.0s %s\n", basename? get_basename(filename[fileindex]) : filename[fileindex]);
        /* determine audio filetype */
        } else if (divine.filetype==AU || divine.filetype==WAV || divine.filetype==MPA || divine.filetype==AC3) {

            /* select what to do based on filetype */
            struct audioinfo info = audioblank;
            switch (divine.filetype) {
                case AU:
                    info = grok_au(bb, filename[fileindex]);
                    break;

                case WAV:
                    info = grok_wav(bb, filename[fileindex]);
                    break;

                case MPA:
                    /* mpeg audio needs to be positioned correctly */
                    flushbits(bb, divine.leading);
                    info = grok_mpa(bb, filename[fileindex]);
                    break;

                case AC3:
                    info = grok_ac3(bb, filename[fileindex]);
                    break;

                default:
                    break;
            }

            /* format channel description */
            char channels[8];
            snprintf(channels, sizeof(channels), "%d.%d", info.channels, info.lfe? 1 : 0);

            /* format duration in human readable terms */
            char duration[10] = "";
            //if (info.framerate)
            //    describe_duration(duration, sizeof(duration), info.frames/(double)info.framerate);

            /* print file information */
            if (description && fileindex==0) {
                //fprintf(stdout, "/-------- frame structure  : i:i-picture only p:p-pictures present b:b-pictures present\n");
                //fprintf(stdout, "|/------- interlaced       : p:progressive i:interlaced a:adaptive frame-field\n");
                fprintf(stdout, "||/------ error resilience : c:crc\n");
                //fprintf(stdout, "|||/----- motion estimation: q:quarter pel w:weighted prediction\n");
                //fprintf(stdout, "||||/---- quantisation     : 1:method one quant, m:custom matrix, l:lossless\n");
                //fprintf(stdout, "|||||/--- entropy          : a:arithmetic coding\n");
                //fprintf(stdout, "||||||/-- chroma type      : m:monochrome, 2:4:2:2 4:4:4:4\n");
                //fprintf(stdout, "|||||||/- others           : d:deblocking filter o:obmc 0:10-bit 2:12-bit v:vbv\n");
                //fprintf(stdout, "||||||||\n");
            }
            fprintf(stdout, "%11s %6dx%2dbit %7s %12s %9s %7.1fkbps %9s %s\n", info.flags, info.samples, info.bits, info.type, info.subtype,
                    info.channels==1? "mono" : info.channels==2? "stereo" : channels, info.bitrate/1000.0,
                    duration, basename? get_basename(filename[fileindex]) : filename[fileindex]);
            //fprintf(stdout, "%8s %4dx%-4d@%5.2f %4s %12s %5d/%-5d %7s %9s %s\n"

        }
        /* determine video filetype */
        else if (divine.filetype==TS || divine.filetype==PS || divine.filetype==PES || divine.filetype==QT || divine.filetype==ASF || divine.filetype==WEBM) {

            /* select what to do based on filetype */
            struct muxerinfo info = muxerblank;
            switch (divine.filetype) {
                case TS:
                    info = grok_ts(bb, filename[fileindex], verbose);
                    break;

                case PS:
                    info = grok_ps(bb, filename[fileindex]);
                    break;

                case PES:
                    info = grok_pes(bb, filename[fileindex], verbose);
                    break;

                case QT:
                    info = grok_qt(bb, filename[fileindex]);
                    break;

                case ASF:
                    info = grok_asf(bb, filename[fileindex], verbose);
                    break;

                case WEBM:
                    info = grok_webm(bb, filename[fileindex], verbose);
                    break;

                default:
                    break;
            }

            /* format bitrate in human readable terms */
            char bitrate[8] = "";
            /* get fps from bitstream parameters else use command line value */
            if (!rawdata && info.bitrate)
                describe_bitrate(bitrate, sizeof(bitrate), info.bitrate, 2, 0);

            /* format duration in human readable terms */
            char duration[10] = "";
            double length = 0.0;
            if (info.bitrate)
                length = info.packets*188.0/(double)info.bitrate*8.0;
            if (!rawdata)
                describe_duration(duration, sizeof(duration), length);

            /* print file information */
            /* print file information */
            if (description && fileindex==0) {
                fprintf(stdout, "/-------- metadata  : t:transport stream psi tables\n");
                //fprintf(stdout, "|/------- interlaced       : p:progressive i:interlaced a:adaptive frame-field\n");
                //fprintf(stdout, "||/------ error resilience : c:crc\n");
                //fprintf(stdout, "|||/----- motion estimation: q:quarter pel w:weighted prediction\n");
                //fprintf(stdout, "||||/---- quantisation     : 1:method one quant, m:custom matrix, l:lossless, s:sps scaling list, p:pps scaling list\n");
                //fprintf(stdout, "|||||/--- entropy          : a:arithmetic coding\n");
                //fprintf(stdout, "||||||/-- chroma type      : m:monochrome, 2:4:2:2 4:4:4:4\n");
                //fprintf(stdout, "|||||||/- others           : d:deblocking filter o:obmc 0:10-bit 2:12-bit v:vbv\n");
                //fprintf(stdout, "||||||||\n");
            }
            fprintf(stdout, "%11s %dx %-7s %dx %-7s %4s %10s %7d ", info.flags, info.vstreams, name_stream_type(info.vstream_type, info.vstream_format_identifier), info.astreams, name_stream_type(info.astream_type, info.astream_format_identifier), info.type, info.subtype, info.packets);
            if (!rawdata)
                fprintf(stdout, "%7s %9s ", bitrate, duration);
            else
                fprintf(stdout, "%7d %8.1fs ", info.bitrate, length);
            fprintf(stdout, "%s\n", basename? get_basename(filename[fileindex]) : filename[fileindex]);

        } else {

            /* select what to do based on filetype */
            struct videoinfo info = videoblank;
            switch (divine.filetype) {
                case YUV:
                    info = grok_yuv(bb, filename[fileindex], forcetype, &st);
                    break;

                case YUV4MPEG:
                    info = grok_yuv4mpeg(filename[fileindex]);
                    break;

                case BMP:
                    info = grok_bmp(bb, filename[fileindex]);
                    break;

                case SGI:
                    info = grok_sgi(bb, filename[fileindex]);
                    break;

                case AVI:
                    info = grok_avi(bb, filename[fileindex], verbose);
                    break;

                case H261:
                    info = grok_h261(bb, filename[fileindex], verbose, &st);
                    break;

                case H263:
                    info = grok_h263(bb, filename[fileindex], &st, finfo, num_elements);
                    break;

                case H264:
                    info = grok_h264(bb, filename[fileindex], verbose, &st, finfo, sinfo, num_elements);
                    break;

                case HEVC:
                    info = grok_hevc(bb, filename[fileindex], verbose, &st, finfo, sinfo, num_elements);
                    break;

                case M2V:
                    info = grok_m2v(bb, filename[fileindex], finfo, num_elements, verbose);
                    break;

                case M4V:
                    info = grok_m4v(bb, filename[fileindex], verbose, finfo, num_elements);
                    break;

                case JPEG:
                    info = grok_jpeg(bb, filename[fileindex], verbose);
                    break;

                case REAL:
                    info = grok_real(bb, filename[fileindex]);
                    break;

                case VC1:
                    info = grok_vc1(bb, filename[fileindex]);
                    break;

                case RCV:
                    info = grok_rcv(bb, filename[fileindex]);
                    break;

                case IVF:
                    info = grok_ivf(bb, filename[fileindex]);
                    break;

                default:
                    break;
            }

            /* format bitrate in human readable terms */
            /* get fps from bitstream parameters else use command line value */
            char bitrate[8] = {0};
            if (!info.framerate)
                info.framerate = fps;
            int bit_rate = 0;
            if (info.frames>0) {
                if (st.st_size)
                    bit_rate = 8*st.st_size*info.framerate/info.frames;
                else
                    bit_rate = numbits(bb)*info.framerate/info.frames;
            }
            if (!rawdata)
                describe_bitrate(bitrate, sizeof(bitrate), bit_rate, 2, 0);

            /* format duration in human readable terms */
            char duration[10] = "";
            double length = 0.0;
            if (info.framerate>0.0)
                length = info.frames/(double)info.framerate;
            if (!rawdata)
                describe_duration(duration, sizeof(duration), length);

            /* temporary hack until all filetypes are updated */
            if (!info.pictures && info.frames)
                info.pictures = info.frames;

            /* print picture information */
            if (perpicture) {
                int i;
                int period = mmin(round(info.framerate), 60);
                int history[60];
                for (i=0; i<info.pictures && i<num_elements; i++) {
                    const char *framestring, *fieldstring;
                    switch (finfo[i].frame_type) {
                        case I_FRAME: framestring = "I-"; break;
                        case P_FRAME: framestring = "P-"; break;
                        case B_FRAME: framestring = "B-"; break;
                        case D_FRAME: framestring = "D-"; break;
                        case S_FRAME: framestring = "S-"; break;
                        /* h.264 frame types */
                        case IDR_FRAME: framestring = "IDR-"; break;
                        /* hevc frame types */
                        case CRA_FRAME: framestring = "CRA-"; break;
                        default: framestring = "?"; break;
                    }
                    switch (finfo[i].field_type) {
                        case FRAME:     fieldstring = "Frm"; break;
                        case TOP_FIELD: fieldstring = "Top"; break;
                        case BOT_FIELD: fieldstring = "Bot"; break;
                        default: fieldstring = "?"; break;
                    }
                    fprintf(stdout, "%4s %4d: %4s%3s %3s order %-5d %6d bytes (%5d stuffing) %4d slice%s @ pos %-9u", finfo[i].start_gop? "GOP>" : "    ", i, framestring, fieldstring,
                            finfo[i].reference? "ref" : "non",
                            finfo[i].order, finfo[i].bytes, finfo[i].stuffbytes, finfo[i].slices, finfo[i].slices==1? " " : "s", finfo[i].position);
                    if (finfo[i].delay)
                        fprintf(stdout, " delay %5d", finfo[i].delay);

                    /* timecode */
                    if (finfo[i].timecode.present)
                        fprintf(stdout, " %02d:%02d:%02d:%02d ", finfo[i].timecode.hours, finfo[i].timecode.minutes, finfo[i].timecode.seconds, finfo[i].timecode.frames);

                    /* calculate bitrate in sliding window over 'period' pictures */
                    if (period)
                        history[i%period] = finfo[i].bytes;
                    int j, sum = 0;
                    for (j=0; j<period; j++)
                        sum += history[j];
                    if (i>=period-1)
                        fprintf(stdout, " last sec: %.2f kbits/sec", sum*8/1024.0);
                    fprintf(stdout, "\n");
                }
            }

            /* print slice information */
            if (perslice) {
                int i;
                for (i=0; i<info.slices; i++)
                    fprintf(stdout, "    %5d: %8s order %-5d first_mb %-4d bytes %3d\n", i, slice_type_name[sinfo[i].slice_type], sinfo[i].order, sinfo[i].first_mb_in_slice, sinfo[i].bytes);
            }

            /* print GOP structure */
            if (gopstructure)
                print_gop_structure(info.pictures, finfo, stdout);

            /* print file information */
            if (description && fileindex==0) {
                fprintf(stdout, "/----------- frame structure  : i:i-picture only p:p-pictures present b:b-pictures present\n");
                fprintf(stdout, "|/---------- interlaced       : p:progressive i:interlaced a:adaptive frame-field\n");
                fprintf(stdout, "||/--------- error resilience : g:gob m:resync marker p:partitioned r:rvlc s:slices f:fmo a:aso\n");
                fprintf(stdout, "|||/-------- reference picture: m:reference picture modification\n");
                fprintf(stdout, "||||/------- motion estimation: q:quarter pel w:weighted prediction a:asymmetric partitions 8:8x8 transform\n");
                fprintf(stdout, "|||||/------ quantisation     : 1:method one quant, m:custom matrix, l:lossless s:scaling list\n");
                fprintf(stdout, "||||||/----- entropy          : a:arithmetic coding, h:custom huffman tables s:sao\n");
                fprintf(stdout, "|||||||/---- chroma type      : m:monochrome, 2:4:2:2 4:4:4:4\n");
                fprintf(stdout, "||||||||/--- timecode         : t:sei timecode\n");
                fprintf(stdout, "|||||||||/-- captions         : c:closed captions\n");
                fprintf(stdout, "||||||||||/- others           : d:deblocking filter o:obmc 0:10-bit 2:12-bit v:vbv\n");
                fprintf(stdout, "|||||||||||\n");
            }
            if (description && divine.filetype==H263 && fileindex==0) {
                fprintf(stdout, "Annex D - Unrestricted Motion Vector Mode (H.263, H.263+)\n");
                fprintf(stdout, "Annex E - Syntax-based Arithmetic Coding (H.263)\n");
                fprintf(stdout, "Annex F - Advanced Prediction Mode (H.263, H.263+)\n");
                fprintf(stdout, "Annex J - Deblocking Filter Mode (H.263+)\n");
                fprintf(stdout, "Annex I - Advanced INTRA Coding Mode (H.263+)\n");
                fprintf(stdout, "Annex K - Slice Structured Mode (H.263+)\n");
                fprintf(stdout, "Annex S - Alternative INTER VLC Mode (H.263+)\n");
                fprintf(stdout, "Annex T - Modified Quantization Mode (H.263+)\n");
            }
            fprintf(stdout, "%11s %4dx%-4d@%5.2f %4s %12s %5d/%-5d ", info.flags, info.width, info.height, info.framerate, info.type, info.subtype, info.frames, info.pictures);
            if (!rawdata)
                fprintf(stdout, "%7s %9s ", bitrate, duration);
            else
                fprintf(stdout, "%7d %8.1fs ", bit_rate, length);
            fprintf(stdout, "%s\n", basename? get_basename(filename[fileindex]) : filename[fileindex]);
        }

        /* tidy up */
        freebits(bb);
        rvfree(finfo);
        rvfree(sinfo);

    } while (++fileindex<numfiles);

    /* tidy up */

    return 0;
}
