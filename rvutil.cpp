/*
 * Description: Common functions for rvtools applications.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2010-01-29 13:14:11 $
 * Revision   : $Revision: 1.67 $
 * Copyright  : (c) 2005 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <ctype.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <math.h>

#include "rvutil.h"
#include "rvm2a.h"

#define true 1
#define false 0

extern const char *appname;
static int not_at_home;

/* arithmetic utility functions */
unsigned int clog2 (unsigned int x)
{
    unsigned int result = 0;
    /* assumes x>0 */
    --x;
    while (x > 0) {
        ++result;
        x >>= 1;
    }

    return result;
}

unsigned int flog2 (unsigned int x)
{
    unsigned int result = 0;
    while (x > 0) {
        ++result;
        x >>= 1;
    }

    return result-1;
}

/* message utility functions */
static int rvgohome(char *message)
{
    /* move cursor to home position */
    if (not_at_home) {
        not_at_home = 0;
        return sprintf(message, "\n");
    }
    return 0;
}

void rverror(const char *format, ...)
{
    va_list ap;
    char message[256];
    int exitcode = errno;

    /* move cursor to home position */
    int len = rvgohome(message);

    /* start output with application name */
    len += snprintf(message+len, sizeof(message)-len, "%s: ", appname);

    /* print message and system error message */
    va_start(ap, format);
    vsnprintf(message+len, sizeof(message)-len, format, ap);
    perror(message);
    va_end(ap);

    exit(exitcode);
}

void rvexit(const char *format, ...)
{
    va_list ap;
    char message[256];

    /* move cursor to home position */
    int len = rvgohome(message);

    /* start output with application name */
    len += snprintf(message+len, sizeof(message)-len, "%s: ", appname);

    /* print message */
    va_start(ap, format);
    vsnprintf(message+len, sizeof(message)-len, format, ap);
    fprintf(stderr, "%s\n", message);
    va_end(ap);

    exit(1);
}

void rvmessage(const char *format, ...)
{
    va_list ap;
    char message[256];

    /* move cursor to home position */
    int len = rvgohome(message);

    /* start output with application name */
    len += snprintf(message+len, sizeof(message)-len, "%s: ", appname);

    /* print message */
    va_start(ap, format);
    vsnprintf(message+len, sizeof(message)-len, format, ap);
    fprintf(stderr, "%s\n", message);
    va_end(ap);
}

void rvstatus(const char *format, ...)
{
    va_list ap;
    char message[256];

    /* start output with carriage return and application name */
    int len = snprintf(message, sizeof(message), "\r%s: ", appname);

    /* print message */
    va_start(ap, format);
    vsnprintf(message+len, sizeof(message)-len, format, ap);
    fprintf(stderr, "%s", message);
    va_end(ap);

    /* mark cursor not at home position */
    not_at_home = 1;
}

void rvabort(const char *format, ...)
{
    va_list ap;
    char message[256];

    /* move cursor to home position */
    int len = rvgohome(message);

    /* start output with application name */
    len += snprintf(message+len, sizeof(message)-len, "%s: ", appname);

    /* print message */
    va_start(ap, format);
    vsnprintf(message+len, sizeof(message)-len, format, ap);
    fprintf(stderr, "%s\n", message);
    va_end(ap);

    abort();
}

void *rvalloc(void *ptr, size_t size, int clear)
{
    if (ptr)
        ptr = realloc(ptr, size);
    else if (clear)
        ptr = calloc(1, size);
    else
        ptr = malloc(size);
    if (ptr==NULL)
        rverror("failed to allocate %zd bytes", size);
    /* special case for realloc with clear */
    if (ptr && clear)
        memset(ptr, 0, size);
    return ptr;
}

void rvfree(void *ptr)
{
    if (ptr)
        free(ptr);
}

const char *get_basename(const char *appname)
{
    const char *p = NULL;
    if (appname)
        p = strrchr(appname, '/');
    if (p)
        return p + 1;
    else
        return appname;
}

const char *get_extension(const char *filename)
{
    const char *p = NULL;
    if (filename)
        p = strrchr(filename, '.');
    if (p)
        return p + 1;
    else
        return NULL;
}

char *rvupper(char *s)
{
    char *c;

    for (c=s; *c; c++)
        *c = toupper(*c);

    return s;
}

char *rvlower(char *s)
{
    char *c;

    for (c=s; *c; c++)
        *c = tolower(*c);

    return s;
}

/* return the string length of an integer in decimal */
int rvdeclen(int n)
{
    int l = 0;
    while (n) {
        n /= 10;
        l++;
    }
    return l;
}

/* get time in msec, wraps every ~50 days */
unsigned int rvmsec()
{
    struct timeval tv;
    if (gettimeofday(&tv, NULL)<0)
        rverror("failed to get time of day");
    return tv.tv_sec*1000 + tv.tv_usec/1000;
}

/* get time in usec */
unsigned long long rvusec()
{
    struct timeval tv;
    if (gettimeofday(&tv, NULL)<0)
        rverror("failed to get time of day");
    return tv.tv_sec*1000000 + tv.tv_usec;
}

/* get time in a 90kHz clock */
unsigned long long rv90kHz()
{
    struct timeval tv;
    if (gettimeofday(&tv, NULL)<0)
        rverror("failed to get time of day");
    return tv.tv_sec*90000 + tv.tv_usec*9/100;
}

/* get time in a 27MHz clock */
unsigned long long rv27MHz()
{
    struct timespec tv;
    if (clock_gettime(CLOCK_MONOTONIC, &tv)<0)
        rverror("failed to get monotonic clock time");
    return tv.tv_sec*27000000 + tv.tv_nsec*27/1000;
}

/*
 * describe fourcc code as a string
 * not thread safe, or even usable multiple times on the same line
 */
const char *describe_fourcc(int fourcc)
{
    static char s[5];
    s[4] = '\0';
    uint32_t *code = (uint32_t *)s;
    *code = fourcc;
    return s;
}

/*
 * describe bitrate in human readable terms
 * extra - create a slightly more verbose description
 */
char *describe_bitrate(char *string, int len, int bitrate, int precision, int extra)
{
    const int kilo = 1000; /* use decimal divisors TODO make configurable */
    int suffix = ' ';
    float f = (float)bitrate;

    if (string==NULL)
        return NULL;

    /* describe bitrate in human readable terms */
    if (f>kilo*kilo*kilo) {
        f /= kilo*kilo*kilo;
        suffix = 'G';
    } else if (f>(kilo*kilo)) {
        f /= kilo*kilo;
        suffix = 'M';
    } else if (f>kilo) {
        f /= kilo;
        suffix = 'K';
    }
    snprintf(string, len, "%.*f%c%s", precision, f, suffix, extra? "bps" : "");
    return string;
}

/* describe number in human readable terms */
char *describe_number(char *string, int len, long long int n, int precision)
{
    const int kilo = 1000; /* use decimal divisors TODO make configurable */
    int suffix = ' ';
    double f = (double)n;

    if (string==NULL)
        return NULL;

    /* describe bitrate in human readable terms */
    if (f>kilo*kilo*kilo) {
        f /= kilo*kilo*kilo;
        suffix = 'G';
    } else if (f>kilo*kilo) {
        f /= kilo*kilo;
        suffix = 'M';
    } else if (f>kilo) {
        f /= kilo;
        suffix = 'K';
    }
    snprintf(string, len, "%.*f%c", precision, f, suffix);
    return string;
}

/* describe time in seconds in human readable terms */
char *describe_duration(char *string, int len, double seconds)
{
    if (string==NULL)
        return NULL;

    /* describe time in seconds in human readable terms */
    int days  = (int) (seconds/60.0/60.0/24.0);
    seconds -= days*60.0*60.0*24.0;
    int hours = (int) (seconds/60.0/60.0);
    seconds -= hours*60.0*60.0;
    int mins  = (int) (seconds/60.0);
    seconds -= mins*60.0;

    if (days)
        snprintf(string, len, "%d days %dh%dm%04.1fs", days, hours, mins, seconds);
    else if (hours)
        snprintf(string, len, "%dh%dm%04.1fs", hours, mins, seconds);
    else if (mins)
        snprintf(string, len, "%dm%04.1fs", mins, seconds);
    else
        snprintf(string, len, "%4.1fs", seconds);
    return string;
}

/* describe time in the 90kHz clock as millisecs in human readable terms */
char *describe_90kHz(char *string, int len, long long time)
{
    if (string==NULL)
        return NULL;

    /* normalise into milliseconds */
    time = time/90ll;

    /* break down into parts */
    int msecs = (int) (time%1000ll);
    time /= 1000ll;
    int secs = (int) (time%60ll);
    time /= 60ll;
    int mins = (int) (time%60ll);
    time /= 60ll;
    int hours = (int) (time%60ll);
    time /= 60ll;

    /* describe time in seconds in human readable terms */
    if (hours)
        snprintf(string, len, "%dhr %dm %3ds %3dms", hours, mins, secs, msecs);
    else if (mins)
        snprintf(string, len, "%dm %3ds %3dms", mins, secs, msecs);
    else if (secs)
        snprintf(string, len, "%ds %3dms", secs, msecs);
    else
        snprintf(string, len, "%dms", msecs);
    return string;
}

/* describe time in the 27MHz clock as microsecs in human readable terms */
char *describe_27MHz(char *string, int len, long long time)
{
    if (string==NULL)
        return NULL;

    /* normalise into microseconds */
    time = time/27ll;

    /* break down into 000's parts */
    int usecs = (int) (time%1000ll);
    time /= 1000ll;
    int msecs = (int) (time%1000ll);
    time /= 1000ll;
    int secs = (int) (time%60ll);
    time /= 60ll;
    int mins = (int) (time%60ll);
    time /= 60ll;
    //int hours = (int) (time%60ll);
    //time /= 60ll;

    /* describe time in seconds in human readable terms */
    if (mins)
        snprintf(string, len, "%dm %3ds %3dms %3dus", mins, secs, msecs, usecs);
    else if (secs)
        snprintf(string, len, "%ds %3dms %3dus", secs, msecs, usecs);
    else if (msecs)
        snprintf(string, len, "%dms %3dus", msecs, usecs);
    else
        snprintf(string, len, "%dus", usecs);
    return string;
}

char *describe_now(char *string, int len)
{
    const time_t t = time(NULL);
    struct tm *tm = localtime(&t);

    /* format time */
    snprintf(string, len, "%02d:%02d:%02d", tm->tm_hour, tm->tm_min, tm->tm_sec);

    return string;
}

/*
 * image size format strings.
 */

const char *rvformats = "sqcif, qcif, cif, 2cif, 4cif, 16cif, ntsc, pal\n"
        "qsif, sif, 2sif, 4sif, d1, 480p, 480i, 486p, 486i, 576p, 576i, 720p, 1080i, 1080p, fhd, 2k, qhd, uhd, 4k\n"
        "cga, qvga, ega, vga, mcga, hgc, mda, svga, xga, xvga, xga+, wxga, sxga,\n"
        "wxga+, sxga+, wsxga, wsxga+, uxga, wuxga";

int lookup_format(const char *format, int *width, int *height, int *interlaced, framerate_t *framerate)
{
    int w, h;
    int i = 0;
    framerate_t f = 25.0;

    /* lookup the dimensions */
    if (strncmp(format, "sqcif", 5)==0) {
        w = 128;
        h = 96;
        format += 5;
    } else if (strncmp(format, "qcif", 4)==0) {
        w = 176;
        h = 144;
        format += 4;
    } else if (strncmp(format, "cif", 3)==0) {
        w = 352;
        h = 288;
        format += 3;
    } else if (strncmp(format, "2cif", 4)==0) {
        w = 704;
        h = 288;
        format += 4;
    } else if (strncmp(format, "4cif", 4)==0) {
        w = 704;
        h = 576;
        i = 1;
        f = 25.0;
        format += 4;
    } else if (strncmp(format, "16cif", 5)==0) {
        w = 1408;
        h = 1152;
        format += 5;
    } else if (strncmp(format, "ntsc", 4)==0) {
        w = 720;
        h = 480;
        i = 1;
        f = 29.97;
        format += 4;
    } else if (strncmp(format, "pal", 3)==0) {
        w = 720;
        h = 576;
        i = 1;
        f = 25.0;
        format += 3;
    } else if (strncmp(format, "qsif", 4)==0) {
        w = 176;
        h = 120;
        format += 4;
    } else if (strncmp(format, "sif", 3)==0) {
        w = 352;
        h = 240;
        format += 3;
    } else if (strncmp(format, "2sif", 4)==0) {
        w = 704;
        h = 240;
        format += 4;
    } else if (strncmp(format, "4sif", 4)==0) {
        w = 704;
        h = 480;
        i = 1;
        f = 29.97;
        format += 4;
    } else if (strncmp(format, "d1", 2)==0) {
        w = 720;
        h = 486;
        i = 1;
        f = 29.97;
        format += 2;
    }
    /* arbitrary standard */
    else if (sscanf(format, "%dx%d", &w, &h)==2) {
        format += rvdeclen(w) + 1 + rvdeclen(h);
    }
    /* hdtv standard */
    else if (strncmp(format, "480", 3)==0) {
        w = 720;
        h = 480;
        f = 29.97;
        format += 3;
    } else if (strncmp(format, "486", 3)==0) {
        w = 720;
        h = 486;
        f = 29.97;
        format += 3;
    } else if (strncmp(format, "576", 3)==0) {
        w = 720;
        h = 576;
        f = 25.0;
        format += 3;
    } else if (strncmp(format, "720", 3)==0) {
        w = 1280;
        h = 720;
        f = 60.0;
        format += 3;
    } else if (strncmp(format, "hd", 2)==0) {
        w = 1280;
        h = 720;
        f = 60.0;
        format += 2;
    } else if (strncmp(format, "hdv", 3)==0) {
        w = 1440;
        h = 1080;
        f = 29.97;
        format += 3;
    } else if (strncmp(format, "1080", 4)==0) {
        w = 1920;
        h = 1080;
        f = 29.97;
        format += 4;
    } else if (strncmp(format, "fhd", 3)==0) {
        w = 1920;
        h = 1080;
        f = 29.97;
        format += 3;
    } else if (strncmp(format, "1088", 4)==0) {
        w = 1920;
        h = 1088;
        f = 29.97;
        format += 4;
    } else if (strncmp(format, "2k", 2)==0) {
        w = 2048;
        h = 1080;
        f = 24.0;
        format += 2;
    } else if (strncmp(format, "qhd", 3)==0) {
        w = 2560;
        h = 1440;
        f = 29.97;
        format += 3;
    } else if (strncmp(format, "uhd", 3)==0) {
        w = 3840;
        h = 2160;
        f = 29.97;
        format += 3;
    } else if (strncmp(format, "2160", 3)==0) {
        w = 3840;
        h = 2160;
        f = 29.97;
        format += 4;
    } else if (strncmp(format, "4k", 2)==0) {
        w = 4096;
        h = 2160;
        f = 24.0;
        format += 2;
    }
    /* computer standard */
    else if (strncmp(format, "cga", 3)==0) {
        w = 320;
        h = 200;
        format += 3;
    } else if (strncmp(format, "qvga", 4)==0) {
        w = 320;
        h = 240;
        format += 4;
    } else if (strncmp(format, "ega", 3)==0) {
        w = 640;
        h = 350;
        format += 3;
    } else if (strncmp(format, "vga", 3)==0) {
        w = 640;
        h = 480;
        format += 3;
    } else if (strncmp(format, "mcga", 4)==0) {
        w = 640;
        h = 480;
        format += 4;
    } else if (strncmp(format, "hgc", 3)==0) {
        w = 720;
        h = 348;
        format += 3;
    } else if (strncmp(format, "mda", 3)==0) {
        w = 720;
        h = 350;
        format += 3;
    } else if (strncmp(format, "svga", 4)==0) {
        w = 800;
        h = 600;
        format += 4;
    } else if (strncmp(format, "xga", 3)==0) {
        w = 1024;
        h = 768;
        format += 3;
    } else if (strncmp(format, "xvga", 4)==0) {
        w = 1024;
        h = 768;
        format += 4;
    } else if (strncmp(format, "xga+", 4)==0) {
        w = 1152;
        h = 864;
        format += 4;
    } else if (strncmp(format, "wxga", 4)==0) {
        w = 1280;
        h = 768;
        format += 4;
    } else if (strncmp(format, "sxga", 4)==0) {
        w = 1280;
        h = 1024;
        format += 4;
    } else if (strncmp(format, "wxga+", 5)==0) {
        w = 1440;
        h = 900;
        format += 5;
    } else if (strncmp(format, "sxga+", 5)==0) {
        w = 1400;
        h = 1050;
        format += 5;
    } else if (strncmp(format, "wsxga", 5)==0) {
        w = 1600;
        h = 1024;
        format += 5;
    } else if (strncmp(format, "wsxga+", 6)==0) {
        w = 1680;
        h = 1050;
        format += 6;
    } else if (strncmp(format, "uxga", 4)==0) {
        w = 1600;
        h = 1200;
        format += 4;
    } else if (strncmp(format, "wuxga", 5)==0) {
        w = 1920;
        h = 1200;
        format += 4;
    }
    /* error */
    else {
        if (height)
            *height = 0;
        if (width)
            *width = 0;
        if (interlaced)
            *interlaced = 0;
        if (framerate)
            *framerate = 0.0;
        return -1;
    }

    /* look for an optional i/p code */
    int field_rate = 0;
    if (*format=='i' || *format=='I') {
        i = 1;
        field_rate = 1;
        format++;
    }
    if (*format=='p' || *format=='P') {
        i = 0;
        format++;
    }

    /* look for the EBU slash */
    if (*format=='/') {
        field_rate = 0;
        format++;
    }

    /* look for a framerate */
    if (*format!='\0') {
        f = 0.0;
        if (strcmp(format, "2997")==0)
            f = 29.97;
        else if (strcmp(format, "5994")==0)
            f = 59.94;
        else
            f = strtof(format, NULL);
        if (field_rate)
            f /= 2.0;
    }

    /* only return requested parameters */
    if (height)
        *height = h;
    if (width)
        *width = w;
    if (interlaced)
        *interlaced = i;
    if (framerate)
        *framerate = f;

    return 0;
}

/*
 * chroma subsampling format strings.
 */

const char *rvchromas = "444, 422, 420, 420mpeg, 420jpeg, 420dv, 411";

int lookup_chroma(const char *chroma, int *hsub, int *vsub)
{
    if (strcmp(chroma, "444")==0) {
        *hsub = 1;
        *vsub = 1;
    } else if (strcmp(chroma, "422")==0) {
        *hsub = 2;
        *vsub = 1;
    } else if (strncmp(chroma, "420", 3)==0) {
        *hsub = 2;
        *vsub = 2;
    } else if (strcmp(chroma, "411")==0) {
        *hsub = 4;
        *vsub = 1;
    } else
        return -1;
    return 0;
}

int fourcc_is_planar(const fourcc_t fourcc)
{
    return fourcc==I420 || fourcc==YU12 || fourcc==I422 || fourcc==YU16 || fourcc==IYUV || fourcc==Y800 || fourcc==I444 ||
           fourcc==YU15 || fourcc==YU20 || fourcc==NV12 || fourcc==NV16 || fourcc==NV15 || fourcc==NV20;
}

/*
 * pixel format (fourcc) strings.
 */

const char *rvfourccs = "I420, YU12, IYUV, I422, YU16, UYVY, YUY2, 2VUY, VYUY, Y800, RGB2, RGBP, V210, YU15, YU20, NV12, NV16, NV15, NV20, XV20";

/* convert a fourcc string into an integer */
fourcc_t get_fourcc(const char *fccode)
{
    int i;
    fourcc_t fourcc = 0;

    if (fccode) {
        for (i=0; i<4; i++)
            fourcc |= toupper(*(fccode+i)) << (i*8);
    }

    return fourcc;
}

/* get chroma subsampling factors from fourcc */
void get_chromasub(fourcc_t fourcc, int *hsub, int *vsub)
{
    switch (fourcc) {
        /* 4:2:0 */
        case I420:
        case YU12:
        case IYUV:
        case YU15:
        case NV12:
        case NV15:
            *hsub = *vsub = 2;
            break;

        /* 4:2:2 */
        case I422:
        case YU16:
        case UYVY:
        case YUY2:
        case TWOVUY:
        case VYUY:
        case V210:
        case YU20:
        case NV16:
        case NV20:
        case XV20:
            *hsub = 2;
            *vsub = 1;
            break;

        /* 4:0:0 */
        case Y800:
            *hsub = *vsub = 0;
            break;

        /* 4:4:4 */
        case RGB2:
        case RGBP:
        case I444:
            *hsub = *vsub = 1;
            break;

        /* unknown fourcc */
        default:
            *hsub = *vsub = -1;
            break;
    }
}

/* get bits per pel from fourcc */
int get_bitsperpel(fourcc_t fourcc)
{
    switch (fourcc) {
        case I420:
        case YU12:
        case IYUV:
        case I422:
        case YU16:
        case UYVY:
        case YUY2:
        case TWOVUY:
        case VYUY:
        case Y800:
        case RGB2:
        case RGBP:
        case I444:
        case NV12:
        case NV16:
            return 8;
            break;

        case V210:
        case YU15:
        case YU20:
        case NV15:
        case NV20:
        case XV20:
            return 10;
            break;

        default:
            return 0;
            break;
    }
}

/* get number of planes from fourcc */
int get_numplanes(fourcc_t fourcc)
{
    switch (fourcc) {
        case Y800:
            return 1;
            break;

        case NV12:
        case NV16:
        case NV15:
        case NV20:
            return 2;
            break;

        case I420:
        case YU12:
        case IYUV:
        case YU15:
        case I422:
        case YU16:
        case YU20:
            return 3;
            break;

        /* 4:4:4 */
        case I444:
            return 4;
            break;

        /* unknown fourcc */
        default:
            return 1;
            break;
    }
}

/* chroma format strings */
const char *chromaformatname[] = {
    "",
    "lum",
    "420",
    "420mpeg",
    "420jpeg",
    "420dv",
    "422",
    "444"
};

/* get chroma format from fourcc */
chromaformat_t get_chromaformat(fourcc_t fourcc)
{
    switch (fourcc) {
        case I420:
        case YU12:
        case IYUV:
        case YU15:
        case NV12:
        case NV15:
            return YUV420jpeg;

        case I422:
        case YU16:
        case UYVY:
        case YUY2:
        case VYUY:
        case YU20:
        case NV16:
        case NV20:
        case XV20:
            return YUV422;

        case Y800:
            return YUV400;
            break;

        case RGB2:
        case RGBP:
        case I444:
            return YUV444;

        default:
            return UNKNOWN;
    }
}

const char *fourccname(const fourcc_t fourcc)
{
    switch (fourcc) {
        case I420: return "I420";
        case YU12: return "YU12";
        case IYUV: return "IYUV";
        case I422: return "I422";
        case YU16: return "YU16";
        case UYVY: return "UYVY";
        case YUY2: return "YUY2";
      case TWOVUY: return "2VUY";
        case VYUY: return "VYUY";
        case Y800: return "Y800";
        case RGB2: return "RGB2";
        case RGBP: return "RGBP";
        case V210: return "V210";
        case YU15: return "YU15";
        case YU20: return "YU20";
        case NV12: return "NV12";
        case NV16: return "NV16";
        case NV15: return "NV15";
        case NV20: return "NV20";
        case XV20: return "XV20";
      //default  : return "unknown";
    }

    /* FIXME not thread safe */
    static char s[5];
    snprintf(s, 5, "%4s", (char *)&fourcc);
    return s;
}

int get_framesize(int width, int height, int hsub, int vsub, int bpp)
{
    const int pelsize = (bpp+7)/8;
    size_t framesize = width*height*pelsize;
    if (hsub && vsub)
        framesize += 2*framesize/hsub/vsub;

    return framesize;
}

int get_framesize(int width, int height, fourcc_t fourcc)
{
    int framesize = 0;

    switch (fourcc) {
        case I420:
        case YU12:
        case IYUV:
        case NV12:
            //*hsub = *vsub = 2;
            framesize = width*height + 2*width*height/2/2;
            break;

        case I422:
        case YU16:
        case UYVY:
        case YUY2:
        case TWOVUY:
        case VYUY:
        case NV16:
            //*hsub = 2; *vsub = 1;
            framesize = width*height + 2*width*height/2;
            break;

        case Y800:
            //*hsub = *vsub = 0;
            framesize = width*height;
            break;

        case RGB2:
        case RGBP:
        case I444:
            framesize = 3*width*height;
            break;

        case V210:
            framesize = 8*(((width+47)/48)*48)*height/3;
            break;

        case YU15:
        case NV15:
            //*hsub = 2; *vsub = 2; bpp=10;
            framesize = 2*width*height + 4*width*height/4;
            break;

        case YU20:
        case NV20:
            //*hsub = 2; *vsub = 1; bpp=10;
            framesize = 2*width*height + 4*width*height/2;
            break;

        case XV20:
            //*hsub = 2; *vsub = 1; bpp=10; packed 32-bit;
            framesize = 4*((width+2)/3)*height + 4*((width+2)/3)*height;
            break;

        default:
            rvexit("get_framesize: unsupported fourcc: %s (0x%08x)", fourccname(fourcc), fourcc);
    }

    return framesize;
}

/*
 * magic to determine the image parameters of a raw video file
 */
int divine_image_dims(int *width, int *height, int *interlaced, framerate_t *framerate, fourcc_t *fourcc, const char *format, const char *filename)
{
    /* lookup the pixel format */
    fourcc_t c = divine_pixel_format(format? format : filename);
    if (fourcc)
        *fourcc = c? c : I420;

    /* first use format string */
    if (format)
        if (lookup_format(format, width, height, interlaced, framerate)>=0)
            return 0;

    /* then try to get format from filename extension */
    if (filename) {
        format = get_extension(filename);
        if (format) {
            /* early exit for yuv4mpeg files */
            if (strncmp(format, "y4m", 3)==0 || strncmp(format, "yuv4mpeg", 8)==0)
                return -1;

            if (lookup_format(format, width, height, interlaced, framerate)>=0)
                return 0;
        }
    }

    /* then try to find height and width within the filename */
    if (filename) {
        int w = 0, h = 0;
        float f = 30.0;
        const char *p = filename;

        while (*p) {
            if (isdigit(*p)) {
                char *e;

                /* read width */
                w = strtol(p, &e, 10);
                p = e;

                /* check for 'x' */
                if (*p!='x' && *p!='X') {
                    w = 0;
                    continue;
                }

                /* check for height */
                p++;
                if (!isdigit(*p))
                    continue;

                /* read height */
                h = strtol(p, &e, 10);
                p = e;

                /* look for a possible framerate */
                if (*p=='p' || *p=='i' || *p=='_' || *p=='-') {
                    p++;
                    rvmessage("framerate=%s", p);
                    float r = 0.0;
                    /* the decimal point is often missing from the following two framerates */
                    if (strcmp(p, "2997")==0)
                        r = 29.97;
                    else if (strcmp(p, "5994")==0)
                        r = 59.94;
                    else
                        r = strtof(p, NULL);
                    if (r!=0.0)
                        f = r;
                }

                /* found a pattern, done */
                break;
            }
            p++;
        }

        if (w && h) {
            //rvmessage("found %dx%d_%.2f in filename", w, h, f);
            if (width)
                *width = w;
            if (height)
                *height = h;
            if (framerate)
                *framerate = f;
            return 0;
        }
    }

    /* failed to divine image dimensions */
    return -1;
}

/*
 * magic to determine the pixel format of a raw video file
 */
fourcc_t divine_pixel_format(const char *filename)
{
    fourcc_t fourcc = 0;

    /* look for a chroma format string */
    //I420, YU12, IYUV, I422, YU16, UYVY, YUY2, 2VUY, VYUY, Y800, RGB2, RGBP, V210, YU15, YU20
    if (filename) {
        /* copy filename string and upper case it */
        int len = strlen(filename);
        char *s = (char *)rvalloc(NULL, len+1, 0);
        strcpy(s, filename);
        rvupper(s);

        if (strstr(s, ".I420."))
            fourcc = I420;
        else if (strstr(s, ".YU12."))
            fourcc = YU12;
        else if (strstr(s, ".IYUV."))
            fourcc = IYUV;
        else if (strstr(s, ".I422."))
            fourcc = I422;
        else if (strstr(s, ".I444."))
            fourcc = I444;
        else if (strstr(s, ".YU16."))
            fourcc = YU16;
        else if (strstr(s, ".UYVY."))
            fourcc = UYVY;
        else if (strstr(s, ".YUY2."))
            fourcc = YUY2;
        else if (strstr(s, ".2VUY."))
            fourcc = TWOVUY;
        else if (strstr(s, ".VYUY."))
            fourcc = VYUY;
        else if (strstr(s, ".Y800."))
            fourcc = Y800;
        else if (strstr(s, ".RGB2."))
            fourcc = RGB2;
        else if (strstr(s, ".RGBP."))
            fourcc = RGBP;
        else if (strstr(s, ".V210."))
            fourcc = V210;
        else if (strstr(s, ".YU15."))
            fourcc = YU15;
        else if (strstr(s, ".YU20."))
            fourcc = YU20;
        else if (strstr(s, ".NV12."))
            fourcc = NV12;
        else if (strstr(s, ".NV16."))
            fourcc = NV16;
        else if (strstr(s, ".NV15."))
            fourcc = NV15;
        else if (strstr(s, ".NV20."))
            fourcc = NV20;
        else if (strstr(s, ".XV20."))
            fourcc = XV20;
        rvfree(s);
    }

    return fourcc;
}

/*
 * filetype strings
 */
const char *filetypename[] = {
    "skip",
    "missing",
    "empty",
    "other",
    "yuv",
    "yuv4mpeg",
    "bmp",
    "sgi",
    "ts",
    "ps",
    "pes",
    "riff",
    "avi",
    "qt",
    "asf",
    "h261",
    "h263",
    "h264",
    "hevc",
    "m1v",
    "m2v",
    "m4v",
    "jpeg",
    "real",
    "avs",
    "vc1",
    "rcv",
    "av1",
    "ivf",
    "webm",
    "au",
    "wav",
    "mpa",
    "ac3",
};

/* magic to determine file type */
divine_t divine_filetype(struct bitbuf *bb, const char *filename, const char *format, int verbose)
{
    divine_t d = {OTHER, 0};
    int width = 0, height = 0;

    /* first stat file */
    struct stat st;
    fstat(bb->infile, &st);
    if (!S_ISFIFO(st.st_mode))
        if (st.st_size==0) {
            d.filetype = EMPTY;
            return d;
        }

    /* attempt to classify file based on format string (not file extension) */
    if (format) {
        if (strncasecmp(format, "bmp",  3)==0) d.filetype = BMP;
        if (strncasecmp(format, "sgi",  3)==0) d.filetype = SGI;
        if (strncasecmp(format, "ts",   2)==0) d.filetype = TS;
        if (strncasecmp(format, "ps",   2)==0) d.filetype = PS;
        if (strncasecmp(format, "pes",  3)==0) d.filetype = PES;
        if (strncasecmp(format, "riff", 4)==0) d.filetype = RIFF;
        if (strncasecmp(format, "avi",  3)==0) d.filetype = AVI;
        if (strncasecmp(format, "qt",   2)==0) d.filetype = QT;
        if (strncasecmp(format, "asf",  3)==0) d.filetype = ASF;
        if (strncasecmp(format, "261",  3)==0) d.filetype = H261;
        if (strncasecmp(format, "h261", 4)==0) d.filetype = H261;
        if (strncasecmp(format, "263",  3)==0) d.filetype = H263;
        if (strncasecmp(format, "h263", 4)==0) d.filetype = H263;
        if (strncasecmp(format, "264",  3)==0) d.filetype = H264;
        if (strncasecmp(format, "h264", 4)==0) d.filetype = H264;
        if (strncasecmp(format, "avc",  3)==0) d.filetype = H264;
        if (strncasecmp(format, "265",  3)==0) d.filetype = HEVC;
        if (strncasecmp(format, "h265", 4)==0) d.filetype = HEVC;
        if (strncasecmp(format, "hevc", 4)==0) d.filetype = HEVC;
        if (strncasecmp(format, "m1v",  3)==0) d.filetype = M1V;
        if (strncasecmp(format, "m2v",  3)==0) d.filetype = M2V;
        if (strncasecmp(format, "m4v",  3)==0) d.filetype = M4V;
        if (strncasecmp(format, "jpg",  3)==0) d.filetype = JPEG;
        if (strncasecmp(format, "real", 4)==0) d.filetype = REAL;
        if (strncasecmp(format, "avs",  3)==0) d.filetype = AVS;
        if (strncasecmp(format, "vc1",  3)==0) d.filetype = VC1;
        if (strncasecmp(format, "rcv",  3)==0) d.filetype = RCV;
        if (strncasecmp(format, "av1",  3)==0) d.filetype = AV1;
        if (strncasecmp(format, "obu",  3)==0) d.filetype = AV1;
        if (strncasecmp(format, "ivf",  3)==0) d.filetype = IVF;
        if (strncasecmp(format, "webm", 4)==0) d.filetype = WEBM;
        if (strncasecmp(format, "au",   2)==0) d.filetype = AU;
        if (strncasecmp(format, "wav",  3)==0) d.filetype = WAV;
        if (strncasecmp(format, "mp1",  3)==0) d.filetype = MPA;
        if (strncasecmp(format, "mp2",  3)==0) d.filetype = MPA;
        if (strncasecmp(format, "mp3",  3)==0) d.filetype = MPA;
        if (strncasecmp(format, "yuv",  3)==0) d.filetype = YUV;
        if (lookup_format(format, &width, &height, NULL, NULL)>=0) d.filetype = YUV;
    } else
        /* use file extension instead */
        format = get_extension(filename);

    /* lookup file extension NOTE file extension only used for yuv size formats */
    if (format) {
        if (lookup_format(format, &width, &height, NULL, NULL)>=0)
            d.filetype = YUV;
        else if (strncasecmp(format, "zip", 3)==0)
            /* zip files seem to be aliased to h.263 */
            d.filetype = SKIP;
        /* it's generally impossible to tell h.264 and hevc apart by content alone */
        else if (strncasecmp(format, "264",  3)==0) d.filetype = H264;
        else if (strncasecmp(format, "h264", 4)==0) d.filetype = H264;
        else if (strncasecmp(format, "avc",  3)==0) d.filetype = H264;
        else if (strncasecmp(format, "265",  3)==0) d.filetype = HEVC;
        else if (strncasecmp(format, "h265", 4)==0) d.filetype = HEVC;
        else if (strncasecmp(format, "hevc", 4)==0) d.filetype = HEVC;
    }

    /* optionally use start codes to classify file */
    if (bb==NULL)
        return d;

    /* attempt to classify file based on start codes */
    if (d.filetype==OTHER) {

        /* search for a match */
        do {
            /* skip id3 tags */
            if (showbits24(bb)==0x494433) {
                d.leading += skip_id3(bb);
            }

            /* look for an h.261 start code */
            if (showbits(bb, 20)==0x00010) {
                d.filetype = H261;
                break;
            }

            /* this has been changed so all filetypes require byte alignment,
             * this is to reduce the number of false positive h.261 detections
             */
            /* the remaining filetypes require byte alignment
            if (numbits(bb)%8) {
                leading += getbit(bb);
                continue;
            } */

            /* look for a bitmap signature */
            /* NB: showbits() is big-endian whereas bmps are little-endian */
            if (showbits16(bb)==0x424d) {
                /* HACK assumes bitmap is smaller than 16MiB */
                if ((llshowbits(bb, 16+32) & 0xff)==0x00) {
                    d.filetype = BMP;
                    break;
                }
            }

            /* look for an sgi image file magic number */
            if (showbits16(bb)==474) {
                d.filetype = SGI;
                break;
            }

            /* look for an h.263 start code */
            if (showbits(bb, 22)==0x00020) {
                d.filetype = H263;
                break;
            }

            /* look for a jpeg SOI marker */
            if (showbits16(bb)==0xffd8) {
                d.filetype = JPEG;
                break;
            }

            /* look for a number of transport stream sync bytes at the correct spacing */
            if (showbits(bb, 8)==0x47) {
                const int n = 3; /* number of following sync bytes to check */
                int i;
                if (verbose>=2)
                    rvmessage("about to jump 188 bytes to check transport stream sync");
                for (i=0; i<n; i++) {
                    d.leading += flushbits(bb, 8*188);
                    if (showbits(bb, 8)!=0x47) {
                        /* check for 204 byte packets */
                        d.leading += flushbits(bb, 8*16);
                        if (showbits(bb, 8)!=0x47) {
                            /* not a transport stream */
                            break;
                        }
                    }
                }
                if (i==n) {
                    d.filetype = TS;
                    d.leading -= n*8*188;
                    break;
                }
            }

            /* look for two mpeg1 audio frame syncs at the correct spacing */
            if (showbits(bb, 11)==0x7ff) {
                int frame_header = showbits32(bb);
                int mpeg_version = 4 - ((frame_header >> 19) & 0x3);
                int mpeg_layer = 4 - ((frame_header >> 17) & 0x3);
                int bit_rate_index = (frame_header >> 12) & 0xf;
                int sampling_rate_index = (frame_header >> 10) & 0x3;
                int padding = (frame_header >> 9) & 0x1;

                /* sanity check */
                if (mpeg_version!=3 && mpeg_layer!=4 && bit_rate_index!=0 && bit_rate_index!=15 && sampling_rate_index!=3) {

                    /* lookup tables */
                    int bit_rate = m2a_bit_rate_table[mpeg_version>1][mpeg_layer-1][bit_rate_index];
                    int sample_rate = m2a_sampling_rate_table[mpeg_version-1][sampling_rate_index];

                    /* calculate frame length in bytes */
                    int frame_length;
                    if (mpeg_layer==1)
                        frame_length = (12*bit_rate*1000/sample_rate + padding) * 4;
                    else
                        frame_length = 144*bit_rate*1000/sample_rate + padding;

                    if (verbose>=2)
                        rvmessage("about to jump %d bytes to check mpeg audio frame sync", frame_length);

                    d.leading += flushbits(bb, 8*frame_length);
                    if (showbits(bb, 11)==0x7ff) {
                        d.filetype = MPA;
                        d.leading -= 8*frame_length;
                        break;
                    }
                }
            }

            /* look for an a52/ac3 syncword */
            if (showbits16(bb)==0x0b77) {
                d.filetype = AC3;
                break;
            }

            /* look for an AVC/HEVC AnnexB long start code,
             * this avoid a clash with mpeg-4 which only uses
             * short (24-bit) start codes */
            unsigned long long longstart = llshowbits(bb, 40);
            if ((longstart&0xffffffff00) == 0x0000000100llu) {
                /* AVC */
                int mask_ref_idc = longstart & 0x9f; /* includes forbidden_zero_bit and nal_unit_type */
                /* look for an h.264 IDR, non-IDR, SEI, SPS or AUD NAL unit */
                if (mask_ref_idc==5 || mask_ref_idc==1 || mask_ref_idc==6 || mask_ref_idc==7 || mask_ref_idc==9) {
                    d.filetype = H264;
                    break;
                }

                /* HEVC */
                int nal_unit_type = (longstart & 0xfe) >> 1;  /* includes forbidden_zero_bit and nal_unit_type */
                /* look for a VPS, SPS, PPS or AUD NAL unit */
                if (nal_unit_type>=32 && nal_unit_type<=35) {
                    d.filetype = HEVC;
                    break;
                }
                /* also look for an IDR NAL unit, for non-aligned captures * /
                if (nal_unit_type==19 || nal_unit_type==20) {
                    d.filetype = HEVC;
                    break;
                }*/
            }

            /* the remaining filetypes use a 32-bit identifier */
            int start = showbits32(bb);

            /* look for the start of a yuv4mpeg format string */
            if (start==BIGFOURCC('Y','U','V','4')) {
                d.filetype = YUV4MPEG;
                break;
            }

            /* look for an mpeg2 sequence header code */
            if (start==0x000001b3) {
                d.filetype = M2V;
                break;
            }

            /* look for an mpeg2 program stream pack start code */
            if (start==0x000001ba) {
                d.filetype = PS;
                break;
            }

            /* look for a pes packet start code */
            if (start>=0x000001bc && start<=0x000001ff) {
                d.filetype = PES;
                break;
            }

            /* look for a RIFF file (type of RIFF file is determined later) */
            if (start==BIGFOURCC('R','I','F','F')) {
                d.filetype = RIFF;
                break;
            }

            /* look for a Quicktime file */
            if (start==BIGFOURCC('m','o','o','v')) {
                d.filetype = QT;
                d.leading -= 32;
                break;
            }
            if (start==BIGFOURCC('f','t','y','p')) {
                d.filetype = QT;
                d.leading -= 32;
                break;
            }

            /* look for a Quicktime file with data atom first */
            if (start==BIGFOURCC('m','d','a','t')) {
                d.filetype = QT;
                d.leading -= 32;
                break;
            }

            /* look for an Active Streaming Format file */
            if (start==0x3026b275) {
                d.filetype = ASF;
                break;
            }

            /* look for a RealMedia file */
            if (start==BIGFOURCC('.','R','M','F')) {
                d.filetype = REAL;
                break;
            }

            /* look for a vc-1 file */
            /* this can alias an mpeg4 video object but it ought to be very rare */
            if (start==0x10f) {
                d.filetype = VC1;
                break;
            }

            /* look for a vc-1 Annex L file */
            if (start==0xc5040000) {
                d.filetype = RCV;
                //leading = 0;
                break;
            }

            /* look for an av1 ivf file */
            if (start==BIGFOURCC('D','K','I','F')) {
                d.filetype = IVF;
                break;
            }

            /* look for a webm file */
            if (start==0x1a45dfa3) {
                /* strictly speaking this is just an EBML file */
                d.filetype = WEBM;
                break;
            }

            /*
             * note that it is impossible to definitely tell mpeg4 and h.264
             * apart due to a clash of start codes when nal_ref_idc==0
             * the following should work in most cases... the order is important
             */

            /* look for an h.264/hevc short start code NAL unit */
            if ((start&0xffffff00) == 0x00000100) {
                int mask_ref_idc = start & 0x9f; /* forbidden_zero_bit and nal_unit_type */
                /* look for an h.264 IDR, SPS or AUD NAL unit */
                if (mask_ref_idc==5 || mask_ref_idc==7 || mask_ref_idc==9) {
                    d.filetype = H264;
                    break;
                }

                int nal_unit_type = (start & 0xfe) >> 1;  /* forbidden_zero_bit and nal_unit_type */
                /* look for a VPS, SPS, PPS or AUD NAL unit */
                if (nal_unit_type>=32 && nal_unit_type<=35) {
                    d.filetype = HEVC;
                    break;
                }
                /* also look for an IDR NAL unit, for non-aligned captures */
                if (nal_unit_type==19 || nal_unit_type==20) {
                    d.filetype = HEVC;
                    break;
                }
            }

            /* look for a suitable mpeg4 start code */
            if (start>=0x100 && start<=0x11f) { /* video_object_start_code */
                d.filetype = M4V;
                break;
            } else if (start==0x1b0) { /* video_object_sequence_start_code */
                d.filetype = M4V;
                break;
            } else if (start==0x1b5) { /* visual_object_start_code */
                d.filetype = M4V;
                break;
            }

            /* look for an au start code */
            if (start==BIGFOURCC('.','s','n','d')) {
                d.filetype = AU;
                break;
            }

            /* next byte */
            d.leading += flushbits(bb, 8);

        } while(d.filetype==OTHER && !eofbits(bb));

        if (d.filetype!=OTHER && d.leading)
            if (verbose>=1)
                rvmessage("%d bits of leading garbage in %s file \"%s\"", d.leading, filetypename[d.filetype], filename);

        /* attempt to further classify type of RIFF file */
        if (d.filetype==RIFF) {
            flushbits(bb, 32);      /* chunk id */
            flushbits(bb, 32);      /* chunk size */
            if (showbits32(bb)==BIGFOURCC('A','V','I',' ')) {
                d.filetype = AVI;
            }

            if (showbits32(bb)==BIGFOURCC('W','A','V','E')) {
                d.filetype = WAV;
            }
        }

        /* distinguish between m2v picture_start_code and m4v video_object_start_code */
        if (d.filetype==M4V && showbits32(bb)==0x100) {
            /* have to check further */
            flushbits(bb, 32);
            int start = showbits32(bb);
            if (start>=0x120 && start<=0x12f) /* video_object_layer_start_code */
                d.filetype = M4V;
            else
                d.filetype = M2V;
        }

    }

    return d;
}

/* determine if a number is within a range specifier, which is of the format e.g. "0-5,8-9" */
int frame_match(unsigned int num, const char *format, int verbose)
{
    const unsigned int max = ~(unsigned int)0;
    unsigned int start, end = max;

    char *p;
    const char *last = format + strlen(format);
    for (p=(char *)format; p<last; p++) {
        start = strtol(p, &p, 10);
        switch (*p) {
            case '\0':
                end = max;
            case ',':
                end = start;
                break;
            case '-':
                end = strtol(p+1, &p, 10);
                if (end==0)
                    end = max;
                break;
            case '+':
                end = start + strtol(p+1, &p, 10) - 1;
                if (end<start)
                    end = max;
                break;
            default:
                rvexit("I'm confused, string \"%s\", character \"%c\"", format, *p);
                break;
        }
        if (num>=start && num<=end) {
            if (verbose>=2)
                fprintf(stderr, "match frame %d in format %s\n", num, format);
            return 1;
        }
    }
    if (verbose>=2)
        fprintf(stderr, "no match for frame %d in format %s (end=%d)\n", num, format, end);
    /* return -1 to indicate there are no more matches in higher values of num */
    return num>end? -1 : 0;
}
