/*
 * Description: Variable length bitstream reading functions.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-10-29 18:55:02 $
 * Revision   : $Revision: 1.39 $
 * Copyright  : (c) 2005 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/*
 * based on tmndecode (H.263 decoder) (C) 1996  Telenor R&D, Norway
 *        Karl Olav Lillevold <Karl.Lillevold@nta.no>
 */

/*
 * based on mpeg2decode, (C) 1994, MPEG Software Simulation Group
 * and mpeg2play, (C) 1994 Stefan Eckart
 *                         <stefan@lis.e-technik.tu-muenchen.de>
 *
 */

/*
 * How (I think) this all works:
 *
 * There are two buffers: rdbfr and rdptr.
 * - rdbfr is a 2048 byte IO buffer which allows more flexibility
 *    than using the libc buffered read
 * - rdptr is a 12 byte buffer from which the bytes are shifted
 *      [0:3]   - previous 32-bits in stream
 *      [4:7]   - current  32-bits in stream
 *      [8:11]  - next     32-bits in stream
 *   this allows the bit shift to go 32 bits either side of the main
 *    32 bits of data
 * - bufbits is the number of bits left in the current 32-bit buffer
 *   and from this a pointer is calculated to read the current bit
 *   position
 *
 * - fillbuf()
 *    fillbuf() moves the 'rdptr' along the 'rdbfr' by 64-bits and
 *    refills the rdbfr if necessary
 *
 * TODO probably all of this needs rewritten with 64-bit previous,
 *      current and next for llshowbits() to be guaranteed to work
 * TODO currenly end of file detection is buggy as fillbuf always
 *      moves along by 64-bits even if there isn't that much data
 *      left in the rdbfr[]
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include "rvbits.h"
#include "rvutil.h"
#include "rvcrc.h"

#ifndef O_BINARY
#define O_BINARY 0
#endif

#define TRACE
#ifdef TRACE
static unsigned int code;
static int len;
#endif

/* to mask the n least significant bits of an integer */
static const unsigned int mask[33] =
{
    0x00000000,0x00000001,0x00000003,0x00000007,
    0x0000000f,0x0000001f,0x0000003f,0x0000007f,
    0x000000ff,0x000001ff,0x000003ff,0x000007ff,
    0x00000fff,0x00001fff,0x00003fff,0x00007fff,
    0x0000ffff,0x0001ffff,0x0003ffff,0x0007ffff,
    0x000fffff,0x001fffff,0x003fffff,0x007fffff,
    0x00ffffff,0x01ffffff,0x03ffffff,0x07ffffff,
    0x0fffffff,0x1fffffff,0x3fffffff,0x7fffffff,
    0xffffffff
};

static const unsigned long long mask64[65] =
{
    0x0000000000000000LL,0x0000000000000001LL,0x0000000000000003LL,0x0000000000000007LL,
    0x000000000000000fLL,0x000000000000001fLL,0x000000000000003fLL,0x000000000000007fLL,
    0x00000000000000ffLL,0x00000000000001ffLL,0x00000000000003ffLL,0x00000000000007ffLL,
    0x0000000000000fffLL,0x0000000000001fffLL,0x0000000000003fffLL,0x0000000000007fffLL,
    0x000000000000ffffLL,0x000000000001ffffLL,0x000000000003ffffLL,0x000000000007ffffLL,
    0x00000000000fffffLL,0x00000000001fffffLL,0x00000000003fffffLL,0x00000000007fffffLL,
    0x0000000000ffffffLL,0x0000000001ffffffLL,0x0000000003ffffffLL,0x0000000007ffffffLL,
    0x000000000fffffffLL,0x000000001fffffffLL,0x000000003fffffffLL,0x000000007fffffffLL,
    0x00000000ffffffffLL,0x00000001ffffffffLL,0x00000003ffffffffLL,0x00000007ffffffffLL,
    0x0000000fffffffffLL,0x0000001fffffffffLL,0x0000003fffffffffLL,0x0000007fffffffffLL,
    0x000000ffffffffffLL,0x000001ffffffffffLL,0x000003ffffffffffLL,0x000007ffffffffffLL,
    0x00000fffffffffffLL,0x00001fffffffffffLL,0x00003fffffffffffLL,0x00007fffffffffffLL,
    0x0000ffffffffffffLL,0x0001ffffffffffffLL,0x0003ffffffffffffLL,0x0007ffffffffffffLL,
    0x000fffffffffffffLL,0x001fffffffffffffLL,0x003fffffffffffffLL,0x007fffffffffffffLL,
    0x00ffffffffffffffLL,0x01ffffffffffffffLL,0x03ffffffffffffffLL,0x07ffffffffffffffLL,
    0x0fffffffffffffffLL,0x1fffffffffffffffLL,0x3fffffffffffffffLL,0x7fffffffffffffffLL,
    0xffffffffffffffffLL
};

/*
 * call either of the following initialisation functions
 * before first getbits or showbits
 */

/* initialize buffer using a filename */
struct bitbuf *initbits_filename(const char *bitfile, int flags)
{
    /* sanity check */
    if ((flags & (B_GETBITS | B_PUTBITS)) == 0)
        return NULL;

    /* allocate new buffer struct */
    struct bitbuf *bb = (struct bitbuf *)rvalloc(NULL, sizeof(struct bitbuf), 1);

    /* init new buffer struct */
    bb->filename = bitfile;
    bb->rdptr = bb->rdbfr + 2048;
    bb->outcnt = 8;
    bb->bytecnt = 0;

    /* open bitstream file */
    if (openbits(bb, bitfile, flags)<0) {
        rvfree(bb);
        return NULL;
    }

    /* get size of file */
    bb->numbytes = sizebits(bb);

    return bb;
}

/* initialize buffer using a memory buffer */
struct bitbuf *initbits_memory(unsigned char *buf, int bytes)
{
    /* allocate new buffer struct */
    struct bitbuf *bb = (struct bitbuf *)rvalloc(NULL, sizeof(struct bitbuf), 1);
    if (bb==NULL) {
        rverror("rvbits: failed to allocate bitstream buffer");
        return NULL;
    }
    bb->infile = -1;

    /* init new buffer struct */
    bb->filename = "buffer";
    bb->rdptr = bb->rdbfr + 2048 ;
    bb->outcnt = 8;
    bb->bytecnt = 0;

    /* copy pointer to memory buffer */
    bb->buffer = buf;   /* memory buffer origin */
    bb->membuf = buf;   /* memory buffer pointer */
    bb->membytes = bytes;
    bb->numbytes = bytes;
    if (bb->membuf==NULL) {
        free(bb);
        return NULL;
    }

    return bb;
}

/* TODO use a string for flags like fopen() */
int openbits(struct bitbuf *bb, const char *bitfile, int flags)
{
    /* sanity check */
    if ((flags & (B_GETBITS | B_PUTBITS)) == 0)
        return -1;

    /* open bitstream file */
    if (bitfile && strcmp(bitfile, "-")) {
        if (flags & B_GETBITS) {
            bb->infile = open(bitfile, O_RDONLY|O_BINARY);
            if (bb->infile<0) {
                rverror("rvbits: failed to open bitstream file for reading \"%s\"", bitfile);
                return -1;
            }
        } else {
            bb->outfile = fopen(bitfile, "wb");
            if (bb->outfile==NULL) {
                rverror("rvbits: failed to open bitstream file for writing \"%s\"", bitfile);
                return -1;
            }
        }
    } else {
        if (flags & B_GETBITS)
            bb->infile = 0; /* stdin */
        else
            bb->outfile = stdout;
    }

    return 0;
}

off_t tellbits(struct bitbuf *bb)
{
    return bb->bitbits;
}

int rewindbits(struct bitbuf *bb)
{
    return seekbits(bb, 0);
}

/* TODO seekbits and tellbits should probably just be in bytes */
off_t seekbits(struct bitbuf *bb, off_t pos)
{
    off_t newpos = 0;

    /* sanity check */
    if (bb->bitbits==0)
        return 0;

    /* FIXME non-byte-aligned seeks not supported yet */
    if (pos%8)
        rvabort("rvbits: attempted to seek to a non-byte-aligned position: %jd", pos);

    /* seek file based buffer */
    if (bb->infile>=0) {
        /* only try to seek if the input file has been read more than once */
        if (bb->readbytes>2048) {
            newpos = lseek(bb->infile, pos/8, SEEK_SET)*8;
            if (newpos<0)
                /* fatal error */
                rverror("failed to seek in file \"%s\"", bb->filename);
            if (newpos!=pos)
                /* seek failed, probably past end of file */
                return newpos;
            bb->rdptr = bb->rdbfr + 2048;
            bb->bufbytes = 0;
            bb->bufbits = 0;
            bb->readbytes = pos/8;
        } else {
            newpos = pos;
            bb->rdptr = bb->rdbfr + pos/8;
            bb->bufbits = 64;
        }
    }

    /* seek memory based buffer */
    if (bb->membuf) {
        /* TODO check for seeking past end of memory buffer */
        newpos = pos;
        bb->membuf -= bb->readbytes - pos/8;
        bb->membytes += bb->readbytes - pos/8;
        if (bb->readbytes>2048) {
            bb->rdptr = bb->rdbfr + 2048;
            bb->bufbits = 0;
        } else {
            bb->rdptr = bb->rdbfr + pos/8;
            bb->bufbits = 64;
        }
    }

    /* reset counters */
    bb->bitbits = pos;

    /* reset eof flag */
    bb->eof = 0;

    return newpos;
}

int eofbits(struct bitbuf *bb)
{
    return bb->eof && bb->bitbits>=bb->readbytes*8;
}

int errorbits(struct bitbuf *bb)
{
    if (bb->outfile)
        return ferror(bb->outfile);
    return 0;
}

/* truncate the output file and discard remaining output bits */
void truncbits(struct bitbuf *bb)
{
    fseek(bb->outfile, 0, SEEK_SET);
    bb->outcnt = 8;
    bb->bytecnt = 0;
}

/* stat file opened by bitbuf */
struct stat statbits(struct bitbuf *bb)
{
    struct stat st = {0};

    if (fstat(bb->infile, &st)<0)
        rvmessage("failed to stat file \"%s\": %s", bb->filename, strerror(errno));

    return st;
}

/* return filesize of bitbuf
 * 0 for pipes and errors */
off_t sizebits(struct bitbuf *bb)
{
    struct stat st = statbits(bb);

    return st.st_size;
} 

void freebits(struct bitbuf *bb)
{
    if (bb->infile>2 && close(bb->infile)<0)
        rverror("rvbits: failed to close bitstream file");
    free(bb);
}

static void fillbuf(struct bitbuf *bb)
{
    bb->rdptr += 8;

    if (bb->rdptr >= bb->rdbfr+bb->bufbytes) {
        /* copy last word to start of buffer */
        bb->rdbfr[0] = bb->rdbfr[bb->bufbytes+0];
        bb->rdbfr[1] = bb->rdbfr[bb->bufbytes+1];
        bb->rdbfr[2] = bb->rdbfr[bb->bufbytes+2];
        bb->rdbfr[3] = bb->rdbfr[bb->bufbytes+3];

        /* read in new buffer */
        if (bb->infile>=0) {
            bb->bufbytes = read(bb->infile, bb->rdbfr+4, 2048);
            if (bb->bufbytes<0)
                rverror("rvbits: failed to read from bitstream file");
        } else if (bb->membuf) {
            bb->bufbytes = mmin(bb->membytes, 2048);
            //if (bb->bufbytes==0)
            //    rvmessage("rvbits: out of data in memory bitstream");
            if (bb->bufbytes>0) {
                memcpy(bb->rdbfr+4, bb->membuf, bb->bufbytes);
                bb->membuf += bb->bufbytes;
                bb->membytes -= bb->bufbytes;
            }
        } else
            rverror("rvbits: not initialised");

        /* zero unused end of buffer */
        if (bb->bufbytes==0) {
            bb->eof = 1;
            memset(bb->rdbfr+4, 0, 2048);
        } else if (bb->bufbytes<2048) {
            //bb->eof = 1;
            memset(bb->rdbfr+bb->bufbytes+4, 0, 2048-bb->bufbytes);
        }

        /* reset read pointer */
        bb->rdptr = bb->rdbfr;

        /* update total bit count */
        bb->readbytes += bb->bufbytes;
    }

    bb->bufbits += 64; // FIXME when bb->bufbytes==0 this isn't correct.
}

static void outbyte(struct bitbuf *bb)
{
  if (bb->outfile)
    fputc(bb->outbfr, bb->outfile);
  else {
    if (bb->membytes==0)
      rvexit("rvbits: buffer overflow in memory bitstream");
    bb->membuf[bb->bytecnt] = bb->outbfr;
    bb->membytes--;
  }
  bb->outcnt = 8;
  bb->bytecnt++;
}

/* write rightmost n (0<=n<=32) bits of val to outfile */
void putbits(struct bitbuf *bb, int n, int val)
{
  int i;
  unsigned int mask;
  /*char bitstring[32];

  if (trace) {
    if (n > 0) {
      BitPrint(n,val,bitstring);
      fprintf(tf,bitstring);
    }
  }*/

  mask = 1 << (n-1); /* selects first (leftmost) bit */

  for (i=0; i<n; i++) {
    bb->outbfr <<= 1;

    if (val & mask)
      bb->outbfr |= 1;

    mask >>= 1; /* select next bit */
    bb->outcnt--;

    if (bb->outcnt==0) { /* 8 bit buffer full */
      outbyte(bb);
    }
  }
}

/* write rightmost n (0<=n<=64) bits of val to outfile */
void llputbits(struct bitbuf *bb, int n, unsigned long long val)
{
  int i;
  unsigned long long mask;

  mask = 1ll << (n-1); /* selects first (leftmost) bit */

  for (i=0; i<n; i++) {
    bb->outbfr <<= 1;

    if (val & mask)
      bb->outbfr |= 1;

    mask >>= 1; /* select next bit */
    bb->outcnt--;

    if (bb->outcnt==0) { /* 8 bit buffer full */
      outbyte(bb);
    }
  }
}

/* return next n bits (right adjusted) without advancing, n<=24 (poss 25) */
/* TODO remove all runtime instances of n>24 */
unsigned int showbits(struct bitbuf *bb, unsigned int n)
{
    unsigned char *v;
    unsigned int b;
    int c;
    unsigned int l;

    if (bb->bufbits < n)
        fillbuf(bb);

    v = bb->rdptr + ((96 - bb->bufbits)>>3);
    b = (v[0]<<24) | (v[1]<<16) | (v[2]<<8) | v[3];
    c = ((bb->bufbits-1) & 7) + 25;
    l = (b>>(c-n)) & mask[n];

#ifdef TRACE
    code = l;
    len = n;
#endif

    return l;
}

/* return next bit without advancing, faster than showbits() */
unsigned int showbit(struct bitbuf *bb)
{
    unsigned char *v;
    int c;
    unsigned int l;

    if (bb->bufbits < 1)
        fillbuf(bb);

    v = bb->rdptr + ((96 - bb->bufbits)>>3);
    c = (bb->bufbits-1) & 7; /* [0:7] */
    l = (v[0]>>c) & 0x00000001;

#ifdef TRACE
    code = l;
    len = 1;
#endif

    return l;
}

/* return next 8 bits without advancing, faster than showbits() */
unsigned int showbits8(struct bitbuf *bb)
{
    unsigned char *v;
    unsigned int b;
    int c;
    unsigned int l;

    if (bb->bufbits < 8)
        fillbuf(bb);

    v = bb->rdptr + ((96 - bb->bufbits)>>3);
    b = (v[0]<<8) | v[1];
    c = ((bb->bufbits-1) & 7) + 1; /* [1:8] */
    l = (b>>c) & 0x000000ff;

#ifdef TRACE
    code = l;
    len = 8;
#endif

    return l;
}

/* return next 16 bits without advancing, faster than showbits() */
unsigned int showbits16(struct bitbuf *bb)
{
    unsigned char *v;
    unsigned int b;
    int c;
    unsigned int l;

    if (bb->bufbits < 16)
        fillbuf(bb);

    v = bb->rdptr + ((96 - bb->bufbits)>>3);
    b = (v[0]<<16) | (v[1]<<8) | v[2];
    c = ((bb->bufbits-1) & 7) + 1; /* [1:8] */
    l = (b>>c) & 0x0000ffff;

#ifdef TRACE
    code = l;
    len = 16;
#endif

    return l;
}

/* return next 24 bits without advancing, faster than showbits() */
unsigned int showbits24(struct bitbuf *bb)
{
    unsigned char *v;
    unsigned int b;
    int c;
    unsigned int l;

    if (bb->bufbits < 24)
        fillbuf(bb);

    v = bb->rdptr + ((96 - bb->bufbits)>>3);
    b = (v[0]<<24) | (v[1]<<16) | (v[2]<<8) | v[3];
    c = ((bb->bufbits-1) & 7) + 1; /* [1:8] */
    l = (b>>c) & 0x00ffffff;

#ifdef TRACE
    code = l;
    len = 24;
#endif

    return l;
}

/* return next 32 bits without advancing, faster than (int) llshowbits() */
unsigned int showbits32(struct bitbuf *bb)
{
    unsigned char *v;
    unsigned long long b;
    int c;
    unsigned long long l;

    if (bb->bufbits < 32)
        fillbuf(bb);

    v = bb->rdptr + ((96 - bb->bufbits)>>3);
    b = (v[0]<<24) | (v[1]<<16) | (v[2]<<8) | v[3];
    b <<= 8;
    b |= (unsigned int)(v[4]);
    c = ((bb->bufbits-1) & 7) + 1; /* [1:8] */
    l = (b>>c) & 0xffffffffll;

#ifdef TRACE
    code = l;
    len = 32;
#endif

    return (unsigned int) l;
}

unsigned long long llshowbits(struct bitbuf *bb, unsigned int n)
{
    unsigned char *v;
    unsigned long long b;
    int c;
    unsigned long long l;

    if (bb->bufbits < n)
        fillbuf(bb);

    v = bb->rdptr + ((96 - bb->bufbits)>>3);
    b = (v[0]<<24) | (v[1]<<16) | (v[2]<<8) | v[3];
    b <<= 32;
    b |= (unsigned int)(v[4]<<24) | (v[5]<<16) | (v[6]<<8) | v[7];
    c = ((bb->bufbits-1) & 7) + 25 + 32;
    l = (b>>(c-n)) & mask64[n];

#ifdef TRACE
    code = l;
    len = n;
#endif

    return l;
}

/* advance by n bits */
unsigned int flushbits(struct bitbuf *bb, unsigned int n)
{
    if (bb->copybits)
        putbits(bb, n, showbits(bb, n));
    bb->bitbits += n;
    bb->bufbits -= n;
    while (bb->bufbits < 0)
        fillbuf(bb);
    return n;
}

/* advance by 1 bit */
unsigned int flushbit(struct bitbuf *bb)
{
    if (bb->copybits)
        putbits(bb, 1, showbit(bb));
    bb->bitbits++;
    bb->bufbits--;
    if (bb->bufbits < 0)
        fillbuf(bb);
    return 1;
}

unsigned int unflushbits(struct bitbuf *bb, unsigned int n)
{
    /* NOTE this is only guaranteed to work for up to 32-bits */
    bb->bitbits -= n;
    bb->bufbits += n;
    return n;
}

/* return next n bits (right adjusted) */
unsigned int getbits(struct bitbuf *bb, unsigned int n)
{
    unsigned int l;

    l = showbits(bb, n);
    flushbits(bb, n);

    return l;
}

/* return next 8 bits, faster than getbits() */
unsigned int getbits8(struct bitbuf *bb)
{
    unsigned int l;

    l = showbits8(bb);
    flushbits(bb, 8);

    return l;
}

/* return next 32 bits, faster than llgetbits() and getbits() should not be used for n>24 */
unsigned int getbits32(struct bitbuf *bb)
{
    unsigned int l;

    l = showbits32(bb);
    flushbits(bb, 32);

    return l;
}

/* return next n bits (right adjusted) */
unsigned long long llgetbits(struct bitbuf *bb, unsigned int n)
{
    unsigned long long l;

    l = llshowbits(bb, n);
    flushbits(bb, n);

    return l;
}

/* return next bit (could be made faster than getbits(1)) */
unsigned int getbit(struct bitbuf *bb)
{
    unsigned int l;

    l = showbit(bb);
    flushbit(bb);

    return l;
}

/* return next byte (only works when byte aligned) */
unsigned int getbyte(struct bitbuf *bb)
{
    unsigned char *v;

    if (bb->bufbits & 7)
        rvabort("bitstream not byte aligned: %d", bb->bufbits);

    if (bb->bufbits < 8)
        fillbuf(bb);

    v = bb->rdptr + ((96 - bb->bufbits)>>3);

    flushbits(bb, 8);

    return (int) v[0];
}

/* return next word little endian (only works when byte aligned) */
unsigned int getword(struct bitbuf *bb)
{
    unsigned char *v;
    unsigned short *b;

    if (bb->bufbits & 7)
        rvabort("bitstream not byte aligned: %d", bb->bufbits);

    if (bb->bufbits < 16)
        fillbuf(bb);

    v = bb->rdptr + ((96 - bb->bufbits)>>3);
    b = (unsigned short *)v;

    flushbits(bb, 16);

    return (int) b[0];
}

/* return next double word little endian (only works when byte aligned) */
unsigned int getdword(struct bitbuf *bb)
{
    unsigned char *v;
    unsigned int *b;

    if (bb->bufbits & 7)
        rvabort("bitstream not byte aligned: %d", bb->bufbits);

    if (bb->bufbits < 32)
        fillbuf(bb);

    v = bb->rdptr + ((96 - bb->bufbits)>>3);
    b = (unsigned int *)v;

    flushbits(bb, 32);

    return b[0];
}

/* return next quad word little endian (only works when byte aligned) */
unsigned long long getqword(struct bitbuf *bb)
{
    unsigned int dword1 = getdword(bb);
    unsigned int dword2 = getdword(bb);

    return (unsigned long long)dword2 << 32 | (unsigned long long)dword1;
}

/* return next block of data little endian (only works when byte aligned) */
int readbits(struct bitbuf *bb, void *data, int size)
{
    int bytes;
    unsigned char *d = (unsigned char *)data;

    if (bb->bufbits & 7) {
        rvmessage("bitstream not byte aligned: %d", bb->bufbits);
        return -1;
    }

    if (bb->bufbits<=0)
        fillbuf(bb);

    for (bytes=0; bytes<size && !eofbits(bb); ) {
        unsigned char *v = bb->rdptr + ((96 - bb->bufbits)>>3);
        int n = mmin(bb->rdbfr+bb->bufbytes+4-v, size-bytes);

        if (n<=0) {
            fillbuf(bb);
            continue;
        }

        memcpy(d, v, n);
        flushbits(bb, n*8);

        d += n;
        bytes += n;
    }

    return bytes;
}

/* advance by 0-7 bits to align to next byte-boundary */
unsigned int alignbits(struct bitbuf *bb)
{
    int align = bb->bufbits % 8;
    if (align)
        return getbits(bb, align);
    return 0;
}

/* return number of bits read */
off_t numbits(struct bitbuf *bb)
{
    /* we do this here, although it is a hack, to keep
     * the performance high in the rest of the code
     */
    return bb->bitbits < bb->readbytes*8? bb->bitbits : bb->readbytes*8;
}

/* return number of bits which would align bitstream */
int numalignbits(struct bitbuf *bb)
{
    return bb->bufbits % 8;
}

/* return number of bits left in bitstream - only works for memory buffers and non-growing files */
int numbitsleft(struct bitbuf *bb)
{
    return bb->numbytes*8 - bb->bitbits;
}

#ifdef TRACE
unsigned int tracebits(struct bitbuf *bb)
{
    return code;
}

int tracelen(struct bitbuf *bb)
{
    return len;
}
#endif

/* return next unsigned Exp-Golomb coded word */
unsigned int uexpbits(struct bitbuf *bb)
{
    int b, zerobits = -1;
    for (b=0; !b && zerobits<32; zerobits++) // test zerobits as a low cost sanity check in case things go wrong.
        b = getbit(bb);
    unsigned int code = (1<<zerobits) - 1 + getbits(bb, zerobits);
    return code;
}

/* return next signed Exp-Golomb coded word */
signed int sexpbits(struct bitbuf *bb)
{
    unsigned int code = uexpbits(bb);
    if (code&1)
        return (int)(code >> 1) + 1;
    else
        return -(int)(code >> 1);
}

/* return next truncated Exp-Golomb coded word */
unsigned int texpbits(struct bitbuf *bb, int range)
{
    if (range>1)
        return uexpbits(bb);
    else
        return !getbit(bb);
}

/* calculate crc-32 on memory bitstream buffer */
crc32_t crc32bits(struct bitbuf *bb)
{
    if (bb->membuf==NULL || bb->bytecnt==0)
        return 0;

    return crc32(bb->membuf, bb->bytecnt);
}

/* return an ebml element id */
unsigned ebmlid(struct bitbuf *bb)
{
    if (showbits(bb, 1)==1)
        return getbits8(bb) & 0x7f;
    else if (showbits(bb, 2)==1)
        return getbits(bb, 16) & 0x7fff;  // ebml is big-endian.
    else if (showbits(bb, 3)==1)
        return getbits(bb, 24) & 0x7fffff;
    else if (showbits(bb, 4)==1)
        return getbits32(bb) & 0x7fffffff;
    else
        return 0xffffffff; // reserved id.
}

/* return an ebml data size */
unsigned long long ebmlsize(struct bitbuf *bb)
{
    int i;
    for (i=1; i<=8; i++)
        if (showbits(bb, i)==1) {
            flushbits(bb, i);
            return llgetbits(bb, i*7);
        }
    return 0xffffffffffffffffLL; // unknown size.
}

/* return an ebml u-integer type */
unsigned long long ebmluinteger(struct bitbuf *bb)
{
    unsigned long long size = ebmlsize(bb);
    return llgetbits(bb, size*8);
}
