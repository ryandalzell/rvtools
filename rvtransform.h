
#ifndef _RVTRANSFORM_H_
#define _RVTRANSFORM_H_

#include "rvcodec.h"

void reference_idct(const coeff_t *coeff, diff_t *sample);
void chen_idct(const coeff_t *coeff, diff_t *sample);

void ihadamard_4x4(coeff_t tblock[4][4], coeff_t block[4][4]);

#endif
