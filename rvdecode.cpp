/*
 * Description: Raw video file decoder
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-07-08 18:13:19 $
 * Revision   : $Revision: 1.37 $
 * Copyright  : (c) 2005-2007 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <assert.h>
#include "getopt.h"

#include "rvutil.h"
#include "rvh261.h"
#include "rvh264.h"
#include "rvm2v.h"
#include "rvy4m.h"
#include "ludh264/ludh264.h"

#ifdef HAVE_LIBDE265
#include <libde265/de265.h>
#endif

const char *appname;
int verbose;

void usage(int exitcode)
{
    fprintf(stderr, "%s: raw video file decoder\n", appname);
    fprintf(stderr, "usage: %s [options] [<file>] [<file>...]\n", appname);
    fprintf(stderr, "  -n, --numframes     : number of frames to decode (default: all)\n");
    fprintf(stderr, "  -r, --refidct       : use reference (floating point) inverse dct (default: fast dct)\n");
    fprintf(stderr, "  -f, --fieldpic      : write field pictures (default: combine field pictures into frame pictures)\n");
    fprintf(stderr, "  -g, --goptest       : test for closed gops, open gops will be decoded with corruption\n");
    fprintf(stderr, "  -o, --output <file> : write output to file\n");
    fprintf(stderr, "  -4. --yuv4mpeg      : write yuv4mpeg format to output (default: raw yuv format)\n");
    fprintf(stderr, "  -q, --quiet         : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "  -v, --verbose       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "  --                  : disable argument processing\n");
    fprintf(stderr, "  -h, --help, --usage : print this usage message\n");
    exit(exitcode);
}

static int fwrite_picture(unsigned char *data, int width, int height, int chroma_format, int hsize, int vsize, FILE *file)
{
    int i;

    /* write luma plane */
    for (i=0; i<vsize; i++) {
        fwrite(data, hsize, 1, file);
        data += width;
    }
    data += (height-vsize)*width;
    if (ferror(file))
        return 0;

    /* chroma subsampling */
    if (chroma_format==1 || chroma_format==2) {
        width >>= 1;
        hsize >>= 1;
    }
    if (chroma_format==1) {
        height >>= 1;
        vsize >>= 1;
    }

    /* write chroma planes */
    for (i=0; i<vsize; i++) {
        fwrite(data, hsize, 1, file);
        data += width;
    }
    data += (height-vsize)*width;
    for (i=0; i<vsize; i++) {
        fwrite(data, hsize, 1, file);
        data += width;
    }
    data += (height-vsize)*width;
    if (ferror(file))
        return 0;

    return 1;
}

int main(int argc, char *argv[])
{
    FILE *fileout = stdout;
    const char *filename[RV_MAXFILES] = {0};
    char *outfile = NULL;
    int fileindex = 0;
    int numfiles = 0;
    int totframes = 0;

    /* command line defaults */
    int numframes = -1;
    int refidct = 0;
    int fieldpic = 0;
    int goptest = 0;
    int yuv4mpeg = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"numframes",1, NULL, 'n'},
            {"refidct",  0, NULL, 'r'},
            {"fieldpic", 0, NULL, 'f'},
            {"goptest",  0, NULL, 'g'},
            {"output",   1, NULL, 'o'},
            {"yuv4mpeg", 0, NULL, '4'},
            {"quiet",    0, NULL, 'q'},
            {"verbose",  0, NULL, 'v'},
            {"usage",    0, NULL, 'h'},
            {"help",     0, NULL, 'h'},
            {NULL,       0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "n:rfgo:4qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 'n':
                numframes = atoi(optarg);
                break;

            case 'r':
                refidct = 1;
                break;

            case 'f':
                fieldpic = 1;
                break;

            case 'g':
                goptest = 1;
                break;

            case 'o':
                outfile = optarg;
                break;

            case '4':
                yuv4mpeg = 1;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < RV_MAXFILES)
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    /* use filein if no input filenames */
    if (numfiles==0)
        filename[0] = "-";

    /* open output file */
    if (outfile)
        fileout = fopen(outfile, "wb");
    if (fileout==NULL)
        rverror("failed to open output file \"%s\"", outfile);

    /* ignore SIGPIPE on output file */
    if (signal(SIGPIPE, SIG_IGN)==SIG_ERR)
        rverror("failed to set ignore on SIGPIPE");

    /* loop over input files */
    do {

        /* prepare to parse file */
        struct bitbuf *bb = initbits_filename(filename[fileindex], B_GETBITS);
        if (bb==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex]);

        /* determine filetype */
        divine_t divine = divine_filetype(bb, filename[fileindex], NULL, verbose);
        if (rewindbits(bb)<0)
            rverror("searched too far on non-seekable input file \"%s\"", filename[fileindex]);

        /* select what to do based on filetype */
        switch (divine.filetype) {
            case H261:
            {
                int picno;

                /* parse h.261 file */
                struct rv261picture *picture = parse_h261(bb, filename[fileindex], verbose);

                /* decode each picture */
                sample_t *reference = NULL;
                for (picno=0; picno<numframes || numframes<0; picno++) {
                    if (picture[picno].macro==NULL) {
                        rvfree(reference);
                        break;
                    }

                    sample_t *frame = decode_261(&picture[picno], reference);

                    /* write picture to output */
                    if (fwrite(frame, picture[picno].width*picture[picno].height*3/2, 1, fileout)!=1)
                        break;
                    else
                        totframes++;

                    /* free used reference frame */
                    rvfree(reference);

                    /* current frame becomes reference frame */
                    reference = frame;
                }
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");

                /* tidy up */
                for (picno=0; ; picno++) {
                    if (picture[picno].macro==NULL)
                        break;
                    free(picture[picno].macro);
                }
                rvfree(picture);
                break;
            }

            case M2V:
            {
                int picno;

                /* parse mpeg2 headers */
                struct rvm2vsequence *sequence = parse_m2v_headers(bb, verbose);
                if (sequence==NULL) {
                    rvmessage("failed to parse headers in mpeg2 sequence \"%s\"", filename[fileindex]);
                    break;
                }

                /* get frame parameters */
                int width  = sequence->width_in_mbs*16;
                int height = sequence->height_in_mbs*16;
                chromaformat_t chromaformat = sequence->chroma_format==2? YUV422 : YUV420mpeg;

                /* write yuv4mpeg stream header */
                if (yuv4mpeg)
                    if (write_y4m_stream_header(sequence->horizontal_size_value, sequence->vertical_size_value >> fieldpic, chromaformat, fileout)<0)
                        break;

                /* decode each picture */
                sample_t *reference[2] = {NULL, NULL};
                sample_t *prevpic[2] = {NULL, NULL}; /* for field order output */
                for (picno=0; picno<numframes || numframes<0; picno++) {
                    if (verbose>=2)
                        rvmessage("picture %d", picno);

                    /* find next picture start code */
                    int gop_flags;
                    if (find_m2v_next_picture(bb, sequence, picno, &gop_flags, verbose)<0)
                        break;

                    /* parse first picture */
                    struct rvm2vpicture *picture = parse_m2v_picture(bb, sequence, picno, verbose);
                    if (picture==NULL) {
                        rvmessage("failed to parse picture %d of mpeg2 sequence \"%s\"", picno, filename[fileindex]);
                        break;
                    }

                    /* decode first picture */
                    sample_t *firstpic = decode_m2v(picture, sequence, reference[0], reference[1], picno, refidct);
                    if (firstpic==NULL) {
                        rvmessage("failed to decode picture %d of mpeg2 sequence \"%s\"", picno, filename[fileindex]);
                        break;
                    }

                    /* check if picture was field picture */
                    sample_t *newframe = NULL;
                    if (field_picture(picture)) {
                        picno++;

                        /* tidy up from first field picture */
                        free_m2v_picture(picture);

                        /* parse second picture */
                        picture = parse_m2v_picture(bb, sequence, picno, verbose);
                        if (picture==NULL) {
                            rvmessage("failed to parse second field picture %d of mpeg2 sequence \"%s\"", picno, filename[fileindex]);
                            break;
                        }

                        sample_t *secondpic;
                        if (picture->picture_coding_type==PCT_P) {

                            /* prepare reference frame from previous field pictures */
                            sample_t *reffield, *refframe;
                            if (picno==1) {
                                /* prepare reference frame from just first field picture */
                                reffield = NULL;
                                refframe = copy_fields_into_frame(firstpic, firstpic, width, height, sequence->chroma_format);
                            } else if (picture->extension.picture_structure==PST_BOTTOM) {
                                reffield = copy_field_from_frame(reference[1], 0, width, height, sequence->chroma_format);
                                refframe = copy_fields_into_frame(firstpic, reffield, width, height, sequence->chroma_format);
                            } else {
                                reffield = copy_field_from_frame(reference[1], 1, width, height, sequence->chroma_format);
                                refframe = copy_fields_into_frame(reffield, firstpic, width, height, sequence->chroma_format);
                            }

                            /* decode second picture */
                            secondpic = decode_m2v(picture, sequence, NULL, refframe, picno, refidct);
                            if (secondpic==NULL) {
                                rvmessage("failed to decode second field picture %d of mpeg2 sequence \"%s\"", picno, filename[fileindex]);
                                break;
                            }

                            rvfree(reffield);
                            rvfree(refframe);

                        } else {

                            /* decode second picture */
                            secondpic = decode_m2v(picture, sequence, reference[0], reference[1], picno, refidct);
                            if (firstpic==NULL) {
                                rvmessage("failed to decode second field picture %d of mpeg2 sequence \"%s\"", picno, filename[fileindex]);
                                break;
                            }

                        }

                        /* output decoded picture pair */
                        if (fieldpic) {
                            if (prevpic[0]) {
                                /* write yuv4mpeg frame header for first picture */
                                if (yuv4mpeg)
                                    if (write_y4m_frame_header(fileout)<0)
                                        break;

                                /* reorder output of pictures */
                                if (picture->picture_coding_type==PCT_B) {
                                    /* write first B-picture to output */
                                    fwrite_picture(firstpic, width, height/2, sequence->chroma_format, sequence->horizontal_size_value, sequence->vertical_size_value/2, fileout);
                                    rvfree(firstpic);
                                } else {
                                    /* write previous I- or P-picture to output */
                                    fwrite_picture(prevpic[0], width, height/2, sequence->chroma_format, sequence->horizontal_size_value, sequence->vertical_size_value/2, fileout);
                                    rvfree(prevpic[0]);
                                    prevpic[0] = firstpic;
                                }

                                /* write yuv4mpeg frame header for second picture */
                                if (yuv4mpeg)
                                    if (write_y4m_frame_header(fileout)<0)
                                        break;

                                /* reorder output of pictures */
                                if (picture->picture_coding_type==PCT_B) {
                                    /* write second B-picture to output */
                                    fwrite_picture(secondpic, width, height/2, sequence->chroma_format, sequence->horizontal_size_value, sequence->vertical_size_value/2, fileout);
                                    rvfree(secondpic);
                                } else {
                                    /* write previous I- or P-picture to output */
                                    fwrite_picture(prevpic[1], width, height/2, sequence->chroma_format, sequence->horizontal_size_value, sequence->vertical_size_value/2, fileout);
                                    rvfree(prevpic[1]);
                                    prevpic[1] = secondpic;
                                }
                                totframes++;
                            } else {
                                prevpic[0] = firstpic;
                                prevpic[1] = secondpic;
                            }
                        }

                        /* merge field pictures */
                        if (picture->extension.picture_structure==PST_BOTTOM)
                            newframe = copy_fields_into_frame(firstpic, secondpic, width, height, sequence->chroma_format);
                        else
                            newframe = copy_fields_into_frame(secondpic, firstpic, width, height, sequence->chroma_format);

                        if (!fieldpic) {
                            rvfree(firstpic);
                            rvfree(secondpic);
                        }

                    } else
                        /* frame picture */
                        newframe = firstpic;

                    /* output decoded frame */
                    if (!fieldpic) {
                        if (reference[1]) {
                            /* write yuv4mpeg frame header */
                            if (yuv4mpeg)
                                if (write_y4m_frame_header(fileout)<0)
                                    break;

                            /* reorder output of frames */
                            if (picture->picture_coding_type==PCT_B) {
                                /* write B-picture to output */
                                fwrite_picture(newframe, width, height, sequence->chroma_format, sequence->horizontal_size_value, sequence->vertical_size_value, fileout);
                            } else {
                                /* write previous I- or P-picture to output */
                                fwrite_picture(reference[1], width, height, sequence->chroma_format, sequence->horizontal_size_value, sequence->vertical_size_value, fileout);
                            }
                            totframes++;
                        }
                    }

                    /* reorder reference frames */
                    if (reference[1]) {
                        if (picture->picture_coding_type==PCT_B) {
                            rvfree(newframe);
                        } else {
                            rvfree(reference[0]);
                            reference[0] = reference[1];
                            reference[1] = newframe;
                        }
                    } else
                        reference[1] = newframe;

                    /* check output file */
                    if (ferror(fileout) && errno!=EPIPE)
                        rverror("failed to write picture %d of mpeg2 sequence \"%s\"", picno, filename[fileindex]);

                    /* test for closed gops by blanking forward reference frame */
                    if (goptest && gop_flags) {
                        int chroma_size;
                        switch (sequence->chroma_format) {
                            case 3 : chroma_size = 2*width*height/1; break;
                            case 2 : chroma_size = 2*width*height/2; break;
                            default: chroma_size = 2*width*height/4; break;
                        }
                        if (reference[0]) {
                            memset(reference[0], 0, width*height);
                            memset(reference[0]+width*height, 128, chroma_size);
                        }
                    }

                    /* tidy up */
                    free_m2v_picture(picture);
                }

                /* write yuv4mpeg frame header */
                if (yuv4mpeg)
                    if (write_y4m_frame_header(fileout)<0)
                        break;

                if (!fieldpic) {
                    /* write last frame */
                    fwrite_picture(reference[1], width, height, sequence->chroma_format, sequence->horizontal_size_value, sequence->vertical_size_value, fileout);
                } else {
                    /* write last picture pair */
                    //fwrite_picture(prevpic[0], width, height/2, sequence->chroma_format, sequence->horizontal_size_value, sequence->vertical_size_value/2, fileout);
                    //if (yuv4mpeg)
                    //    write_y4m_frame_header(fileout);
                    //fwrite_picture(prevpic[1], width, height/2, sequence->chroma_format, sequence->horizontal_size_value, sequence->vertical_size_value/2, fileout);
                }
                totframes++;


                /* tidy up */
                rvfree(reference[0]);
                rvfree(reference[1]);
                rvfree(prevpic[0]);
                rvfree(prevpic[1]);
                free_m2v_sequence(sequence);

                break;
            }

#if 0
            case H264:
            {
                int picno;

                /* find sps and pps */
                struct rv264sequence *seq = parse_264_params(bb, verbose);

                /* write yuv4mpeg stream header */
                chromaformat_t chromaformat = seq->sps[seq->active_sps]->chroma_format_idc==2? YUV422 : YUV420mpeg;
                if (yuv4mpeg)
                    if (write_y4m_stream_header(seq->sps[seq->active_sps]->PicWidthInMbs, seq->sps[seq->active_sps]->FrameHeightInMbs, chromaformat, fileout)<0)
                        break;

                /* parse each picture */
                for (picno=0; picno<numframes || numframes<0; picno++) {
                    if (verbose>=2)
                        rvmessage("picture %d", picno);

                    /* parse first picture */
                    struct rv264picture *picture = parse_264_picture(bb, seq, verbose);
                    if (picture==NULL)
                        /* end of file */
                        break;

                    /* decode first picture */
                    sample_t *firstpic = decode_264_picture(picture, seq->sps[seq->active_sps], picno);
                    if (firstpic==NULL) {
                        rvmessage("failed to decode picture %d of h.264 sequence \"%s\"", picno, filename[fileindex]);
                        free_264_picture(picture);
                        break;
                    }

                    /* write yuv4mpeg frame header for first picture */
                    if (yuv4mpeg)
                        if (write_y4m_frame_header(fileout)<0)
                            break;

                    /* write picture to output */
                    fwrite_picture(firstpic, picture->pps->sps->PicWidthInMbs*16, picture->pps->sps->FrameHeightInMbs*16, picture->pps->sps->chroma_format_idc, picture->pps->sps->PicWidthInMbs*16, picture->pps->sps->FrameHeightInMbs*16, fileout);
                    totframes++;

                    /* tidy up */
                    rvfree(firstpic);
                    free_264_picture(picture);
                }

                /* tidy up */
                free_264_sequence(seq);

                break;
            }
#endif

            case H264:
            {
                /* allocate data buffer */
                const size_t size = 2048*1024;
                uint8_t *data = (uint8_t *)rvalloc(NULL, (size+4)*sizeof(uint8_t), 0);
                size_t pos = 0;

                /* initialise decode library */
                struct rv264sequence *seq = alloc_264_sequence();
                ludh264_init(seq);

                /* loop over input data chunks */
                bool eof = false;
                struct rv264picture *pic = NULL;
                do {
                    if (!eof) {
                        size_t read = readbits(bb, data+pos, size-pos);
                        if (read==0) {
                            /* check for errors */
                            if (errorbits(bb))
                                rverror("failed to read from input");

                            /* otherwise check for end of file */
                            eof = eofbits(bb);
                        }
                        pos += read;
                    }

                    /* parse the bitstream for the next nal unit */
                    uint32_t consumed = pos;
                    uint8_t* buf;
                    uint32_t len;
                    RetCode r = ludh264_parse_bytestream_nalu(seq, data, &consumed, &buf, &len, eof);
                    if (r!=LUDH264_SUCCESS)
                        rvexit("failed to parse bytestream nal unit");

                    /* decode the nal unit */
                    r = ludh264_decode_nalu(seq, buf, len, &pic);
                    if (r!=LUDH264_SUCCESS)
                        rvexit("failed to decode nal unit");

                    /* track consumed bytes and remove them from the buffer */
                    pos -= consumed;
                    if (consumed)
                        memmove(data, data + consumed, pos);

                    if (pic) {
                        void* buffer;
                        uint32_t buffer_size;
                        uint32_t width, height;
                        LUDH264_VIDEO_FORMAT format;

                        ludh264_get_picture(seq, pic, &buffer, &buffer_size, &format, &width, &height);
                        size_t written = fwrite(buffer, 1, buffer_size, fileout);
                        ludh264_free_picture(seq, pic);
                        if (written != buffer_size)
                            break;
                        totframes++;
                    }
                } while (pos > 0 || eof==false);
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");

                /* continue decoding until no more pictures are emitted */
                do {
                    RetCode r = ludh264_decode_nalu(seq, NULL, 0, &pic);
                    if (r!=LUDH264_SUCCESS)
                        rvexit("failed to decode nal unit");
                    if (pic) {
                        void* buffer;
                        uint32_t buffer_size;
                        uint32_t width, height;
                        LUDH264_VIDEO_FORMAT format;

                        ludh264_get_picture(seq, pic, &buffer, &buffer_size, &format, &width, &height);
                        size_t written = fwrite(buffer, 1, buffer_size, fileout);
                        ludh264_free_picture(seq, pic);
                        if (written != buffer_size)
                            break;
                        totframes++;
                    }
                } while (pic);
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");

                /* tidy up */
                ludh264_destroy(seq);
                free_264_sequence(seq);
                free(data);

                break;
            }

            case HEVC:
            {
#ifdef HAVE_LIBDE265
                #define BUFFER_SIZE 40960
                #define NUM_THREADS 4

                de265_decoder_context* ctx = de265_new_decoder();

                /* configure de265 decoder */
                de265_set_parameter_bool(ctx, DE265_DECODER_PARAM_BOOL_SEI_CHECK_HASH, 1);
                de265_set_parameter_bool(ctx, DE265_DECODER_PARAM_SUPPRESS_FAULTY_PICTURES, false);
                //de265_set_parameter_bool(ctx, DE265_DECODER_PARAM_DISABLE_DEBLOCKING, disable_deblocking);
                //de265_set_parameter_bool(ctx, DE265_DECODER_PARAM_DISABLE_SAO, disable_sao);

                if (verbose>=2) {
                    de265_set_parameter_int(ctx, DE265_DECODER_PARAM_DUMP_SPS_HEADERS, 1);
                    de265_set_parameter_int(ctx, DE265_DECODER_PARAM_DUMP_VPS_HEADERS, 1);
                    de265_set_parameter_int(ctx, DE265_DECODER_PARAM_DUMP_PPS_HEADERS, 1);
                }
                if (verbose>=3) {
                    de265_set_parameter_int(ctx, DE265_DECODER_PARAM_DUMP_SLICE_HEADERS, 1);
                }
                if (0) {
                    de265_set_parameter_int(ctx, DE265_DECODER_PARAM_ACCELERATION_CODE, de265_acceleration_SCALAR);
                }
                if (0) {
                    //de265_set_limit_TID(ctx, highestTID);
                }

                de265_set_verbosity(verbose);
                if (verbose>=1)
                    rvmessage("libde265 v%s", de265_get_version());

                //err = de265_start_worker_threads(ctx, NUM_THREADS);

                /* decoding loop */
                bool stop = false;
                while (!stop) {
                    const struct de265_image* img = de265_get_next_picture(ctx);
                    while (!img) {

                        /* decode some more */
                        int more = 1;
                        de265_error err = de265_decode(ctx, &more);
                        if (more && de265_isOK(err))
                            img = de265_get_next_picture(ctx);
                        else if (more && err==DE265_ERROR_WAITING_FOR_INPUT_DATA) {
                            /* read a chunk of input data */
                            uint8_t buf[BUFFER_SIZE];
                            size_t read = readbits(bb, buf, BUFFER_SIZE);
                            if (read) {
                                err = de265_push_data(ctx, buf, read, 0, NULL);
                                if (!de265_isOK(err)) {
                                    rverror("failed to push hevc data to decoder: %s", de265_get_error_text(err));
                                }
                            }

                            if (eofbits(bb)) {
                                err = de265_flush_data(ctx); // indicate end of stream
                            }
                        } else if (!more) {
                            /* decoding finished */
                            if (!de265_isOK(err))
                                rvmessage("error decoding frame: %s", de265_get_error_text(err));
                            stop = true;
                            break;
                        } else
                            rvmessage("error decoding frame: %s", de265_get_error_text(err));

                        /* show warnings in decode */
                        while (1) {
                            de265_error warning = de265_get_warning(ctx);
                            if (de265_isOK(warning))
                                break;

                            rvmessage("warning: %s", de265_get_error_text(warning));
                        }
                    }

                    /* retrieve available images */
                    if (img) {
                        /* report sequence parameters */
                        if (totframes==0 && verbose>=0) { // FIXME only works on first file.
                            const char *describe_chroma_format[] = {"mono", "4:2:0", "4:2:2", "4:4:4"};
                            rvmessage("%s: %dx%d %s", get_basename(filename[fileindex]), de265_get_image_width(img,0), de265_get_image_height(img,0),
                                        describe_chroma_format[de265_get_chroma_format(img)]);
                        }

                        /* report frame parameters */
                        if (verbose>=1) {
                            const char* nal_unit_name;
                            int nuh_layer_id;
                            int nuh_temporal_id;
                            de265_get_image_NAL_header(img, NULL, &nal_unit_name, &nuh_layer_id, &nuh_temporal_id);

                            rvmessage("NAL: %s layer:%d temporal:%d", nal_unit_name, nuh_layer_id, nuh_temporal_id);
                        }

                        /* write decoded image to output */
                        for (int c=0; c<3; c++) {
                            int stride;
                            const uint8_t* p = de265_get_image_plane(img, c, &stride);
                            int width = de265_get_image_width(img, c);

                            for (int y=0; y<de265_get_image_height(img,c); y++) {
                                fwrite(p + y*stride, width, 1, fileout);
                            }
                        }

                        totframes++;
                    }
                }

                /* tidy up */
                de265_free_decoder(ctx);
                break;
#else
                rvexit("hevc support is not compiled in, install libde265 and recompile");
#endif
            }

            case EMPTY:
                rvexit("empty file: \"%s\"", filename[fileindex]);
                break;

            default:
                rvmessage("filetype of file \"%s\" is not supported: %s", filename[fileindex], filetypename[divine.filetype]);
                break;
        }

        /* tidy up */
        freebits(bb);

    } while (++fileindex<numfiles && !ferror(fileout));
    if (ferror(fileout) && errno!=EPIPE)
        rverror("failed to write to output");

    if (verbose>=0)
        rvmessage("decoded %d frames", totframes);

    /* tidy up */
    if (outfile)
        fclose(fileout);

    return 0;
}
