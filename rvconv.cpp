/*
 * Description: Convert video file formats without data conversion.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2010-01-22 15:52:39 $
 * Revision   : $Revision: 1.30 $
 * Copyright  : (c) 2005-2022 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include "getopt.h"

#include "rvutil.h"
#include "rvheader.h"
#include "rvendian.h"

/* HACK avoid namespace clash */
#undef UYVY
#undef YUY2
#undef V210
#undef NV12
#undef NV16

/* list of formats supported */
enum format_t {
    UNSUPPORTED,
    UYVY,
    YUY2,
    RAWTEKTV,
    AV1X,
    BITMAP,
    DIB555,
    SGIIMAGE,
    V210,
    NV12,
    NV16
};

/* bitmap header */
struct bitmap {
    u_int16_t  signature;
    u_int32_t  size;
    u_int16_t  reserved1;
    u_int16_t  reserved2;
    u_int32_t  offset;
    u_int32_t  headersize;
    u_int32_t  width;
    u_int32_t  height;
    u_int16_t  planes;
    u_int16_t  bits;
    u_int32_t  compression;
    u_int32_t  datasize;
    u_int32_t  hres;
    u_int32_t  vres;
    u_int32_t  colours;
    u_int32_t  important_colours;
}__attribute__((packed));

const char *appname;

void usage(int exitcode)
{
    fprintf(stderr, "%s: convert raw video data from one format to another, without data conversion\n", appname);
    fprintf(stderr, "usage: %s [options] [<file>] [<file>...]\n", appname);
    fprintf(stderr, "  -f, --format    : format of input data to convert from (default: uyvy)\n");
    fprintf(stderr, "     (formats     : uyvy, yuy2, rawTekTV, av1x, bmp, dib555, sgi, v210, nv12, nv16)\n");
    fprintf(stderr, "  -r, --reverse   : reverse direction, convert to specified format\n");
    fprintf(stderr, "  -s, --size      : image size format\n");
    fprintf(stderr, "     (formats     : %s)\n", rvformats);
    fprintf(stderr, "  -a, --first     : index of first frame to output (default: 0)\n");
    fprintf(stderr, "  -n, --numframes : number of frames to output (default: all)\n");
    fprintf(stderr, "  -o, --output    : write output to file\n");
    fprintf(stderr, "  -q, --quiet     : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "  -v, --verbose   : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "  --              : disable argument processing\n");
    fprintf(stderr, "  -h, --usage     : print this usage message\n");
    exit(exitcode);
}

unsigned char *convert_packed_rgb15_yuv(unsigned short *rgb, int width, int height)
{
    /* allocate 8-bit 4:4:4 yuv data buffer */
    size_t size = width*height*3;
    unsigned char *dst = (unsigned char *)rvalloc(NULL, size, 0);

    /* use pointers to each component */
    unsigned char *yuv[3] = {dst, dst+width*height, dst+2*width*height};

    /* convert from packed RGB to planar YUV */
    int i;
    if (width<=720) {
        /* convert R'G'B' to YPbPr according to Rec. 601 */
        for (i=0; i<width*height; i++) {
            unsigned char R = (rgb[i]>>10) & 0x1f;
            unsigned char G = (rgb[i]>>5)  & 0x1f;
            unsigned char B = (rgb[i]>>0)  & 0x1f;
            yuv[0][i] = (unsigned char)( 2.392*R + 4.696*G + 0.912*B);
            yuv[1][i] = (unsigned char)((0.5/0.886)*(8*B - yuv[0][i])) + 128;
            yuv[2][i] = (unsigned char)((0.5/0.701)*(8*R - yuv[0][i])) + 128;
        }
    } else {
        /* convert R'G'B' to YPbPr according to Rec. 709 */
        for (i=0; i<width*height; i++) {
            unsigned char R = (rgb[i]>>10) & 0x1f;
            unsigned char G = (rgb[i]>>5)  & 0x1f;
            unsigned char B = (rgb[i]>>0)  & 0x1f;
            yuv[0][i] = (unsigned char)(1.7008*R + 5.7216*G + 0.5776*B);
            yuv[1][i] = (unsigned char)((1.0/1.8556)*(8*B - yuv[0][i])) + 128;
            yuv[2][i] = (unsigned char)((1.0/1.5748)*(8*R - yuv[0][i])) + 128;
        }
    }

    return dst;
}

unsigned short *convert_packed_rgb48_yuv(unsigned short *rgb, int width, int height)
{
    /* allocate 16-bit 4:4:4 yuv data buffer */
    size_t size = width*height*3*2;
    unsigned short *dst = (unsigned short *)rvalloc(NULL, size, 0);

    /* use pointers to each component */
    unsigned short *yuv[3] = {dst, dst+width*height, dst+2*width*height};

    /* convert from packed RGB to planar YUV */
    int i;
    if (width<=720) {
        /* convert R'G'B' to YPbPr according to Rec. 601 */
        for (i=0; i<width*height; i++) {
            int j = 3*i;
            yuv[0][i] = (unsigned short)( 0.299*rgb[j+0] + 0.587*rgb[j+1] + 0.114*rgb[j+2]);
            yuv[1][i] = (unsigned short)((0.5/0.886)*(rgb[j+2] - yuv[0][i])) + 32768;
            yuv[2][i] = (unsigned short)((0.5/0.701)*(rgb[j+0] - yuv[0][i])) + 32768;
        }
    } else {
        /* convert R'G'B' to YPbPr according to Rec. 709 */
        for (i=0; i<width*height; i++) {
            int j = 3*i;
            yuv[0][i] = (unsigned short)(0.2126*rgb[j+0] + 0.7152*rgb[j+1] + 0.0722*rgb[j+2]);
            yuv[1][i] = (unsigned short)((1.0/1.8556)*(rgb[j+2] - yuv[0][i])) + 32768;
            yuv[2][i] = (unsigned short)((1.0/1.5748)*(rgb[j+0] - yuv[0][i])) + 32768;
        }
    }

    return dst;
}

unsigned char *convert_planar_rgb24_yuv(unsigned char *src, int width, int height)
{
    /* allocate 8-bit 4:4:4 yuv data buffer */
    size_t size = width*height*3;
    unsigned char *dst = (unsigned char *)rvalloc(NULL, size, 0);

    /* use pointers to each component */
    unsigned char *yuv[3] = {dst, dst+width*height, dst+2*width*height};
    unsigned char *rgb[3] = {src, src+width*height, src+2*width*height};

    /* convert from planar RGB to planar YUV */
    int i;
    if (width<=720) {
        /* convert R'G'B' to YPbPr according to Rec. 601 */
        for (i=0; i<width*height; i++) {
            unsigned char R = rgb[0][i];
            unsigned char G = rgb[1][i];
            unsigned char B = rgb[2][i];
            yuv[0][i] = (unsigned char)(0.299*R + 0.587*G + 0.114*B);
            yuv[1][i] = (unsigned char)((0.5/0.886)*(B - yuv[0][i])) + 128;
            yuv[2][i] = (unsigned char)((0.5/0.701)*(R - yuv[0][i])) + 128;
        }
    } else {
        /* convert R'G'B' to YPbPr according to Rec. 709 */
        for (i=0; i<width*height; i++) {
            unsigned char R = rgb[0][i];
            unsigned char G = rgb[1][i];
            unsigned char B = rgb[2][i];
            yuv[0][i] = (unsigned char)(0.2126*R + 0.7152*G + 0.0722*B);
            yuv[1][i] = (unsigned char)((1.0/1.8556)*(B - yuv[0][i])) + 128;
            yuv[2][i] = (unsigned char)((1.0/1.5748)*(R - yuv[0][i])) + 128;
        }
    }

    return dst;
}

unsigned short *convert_planar_rgb48_yuv(unsigned short *src, int width, int height)
{
    /* allocate 16-bit 4:4:4 yuv data buffer */
    size_t size = width*height*3*2;
    unsigned short *dst = (unsigned short *)rvalloc(NULL, size, 0);

    /* use pointers to each component */
    unsigned short *yuv[3] = {dst, dst+width*height, dst+2*width*height};
    unsigned short *rgb[3] = {src, src+width*height, src+2*width*height};

    /* convert from planar RGB to planar YUV */
    int i;
    if (width<=720) {
        /* convert R'G'B' to YPbPr according to Rec. 601 */
        for (i=0; i<width*height; i++) {
            yuv[0][i] = (unsigned short)(0.299*rgb[0][i] + 0.587*rgb[1][i] + 0.114*rgb[2][i]);
            yuv[1][i] = (unsigned short)((0.5/0.886)*(rgb[2][i] - yuv[0][i])) + 32768;
            yuv[2][i] = (unsigned short)((0.5/0.701)*(rgb[0][i] - yuv[0][i])) + 32768;
        }
    } else {
        /* convert R'G'B' to YPbPr according to Rec. 709 */
        for (i=0; i<width*height; i++) {
            yuv[0][i] = (unsigned short)(0.2126*rgb[0][i] + 0.7152*rgb[1][i] + 0.0722*rgb[2][i]);
            yuv[1][i] = (unsigned short)((1.0/1.8556)*(rgb[2][i] - yuv[0][i])) + 32768;
            yuv[2][i] = (unsigned short)((1.0/1.5748)*(rgb[0][i] - yuv[0][i])) + 32768;
        }
    }

    return dst;
}

unsigned char *convert_yuv16_yuv8(unsigned short *src, int width, int height, int hsub, int vsub)
{
    size_t size;

    /* allocate 8-bit yuv data buffer */
    if (hsub && vsub)
        size = width*height + 2*width*height/hsub/vsub;
    else
        size = width*height;
    unsigned char *dst = (unsigned char *)rvalloc(NULL, size, 0);

    /* convert from 16-bit to 8-bit */
    int i;
    for (i=0; i<size; i++)
        dst[i] = (unsigned char)((src[i] + 0x80) >> 8);

    return dst;
}

int sgi_expandrow_char(unsigned char *optr, unsigned char *iptr)
{
    unsigned char pixel, count;

    unsigned char *start = optr;

    while (1) {
        pixel = *iptr++;
        if ( !(count = (pixel & 0x7f)) )
            break;
        if (pixel & 0x80) {
            while (count--)
                *optr++ = *iptr++;
        } else  {
            pixel = *iptr++;
            while (count--)
                *optr++ = pixel;
        }
    }

    return optr-start;
}

int main(int argc, char *argv[])
{
    FILE *filein = stdin;
    FILE *fileout = stdout;
    char *filename[RV_MAXFILES] = {0};
    char *outfile = NULL;
    int fileindex = 0;
    int numfiles = 0;
    int totframes = 0;
    enum format_t inpformat = UNSUPPORTED;
    enum format_t outformat = UNSUPPORTED;

    /* data buffer */
    size_t size = 0;
    unsigned char *data = NULL;

    /* command line defaults */
    const char *formatname = "uyvy";
    int reverse = 0;
    int width = 0;
    int height = 0;
    const char *format = NULL;
    int start = 0;
    int numframes = 0;
    int verbose = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"format",    1, NULL, 'f'},
            {"reverse",   0, NULL, 'r'},
            {"size",      1, NULL, 's'},
            {"first",     1, NULL, 'a'},
            {"numframes", 1, NULL, 'n'},
            {"output",    1, NULL, 'o'},
            {"quiet",     0, NULL, 'q'},
            {"verbose",   0, NULL, 'v'},
            {"usage",     0, NULL, 'h'},
            {"help",      0, NULL, 'h'},
            {NULL,        0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "f:rs:a:n:o:qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 'f':
                formatname = optarg;
                break;

            case 'r':
                reverse = 1;
                break;

            case 's':
                format = optarg;
                break;

            case 'a':
                start = atoi(optarg);
                break;

            case 'n':
                numframes = atoi(optarg);
                break;

            case 'o':
                outfile = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < sizeof(filename)/sizeof(filename[0]))
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    /* determine format of conversion */
    if (strcasecmp(formatname, "uyvy")==0 || strcasecmp(formatname, "601")==0)
        inpformat = outformat = UYVY;
    else if (strcasecmp(formatname, "yuy2")==0 || strcasecmp(formatname, "yuyv")==0)
        inpformat = outformat = YUY2;
    else if (strcasecmp(formatname, "rawtektv")==0)
        inpformat = outformat = RAWTEKTV;
    else if (strcasecmp(formatname, "av1x")==0)
        inpformat = outformat = AV1X;
    else if (strcasecmp(formatname, "bmp")==0 || strcasecmp(formatname, "dib")==0)
        inpformat = outformat = BITMAP;
    else if (strcasecmp(formatname, "dib555")==0)
        inpformat = outformat = DIB555;
    else if (strcasecmp(formatname, "sgi")==0 || strcasecmp(formatname, "sgiimage")==0)
        inpformat = outformat = SGIIMAGE;
    else if (strcasecmp(formatname, "v210")==0 || strcasecmp(formatname, "sgiimage")==0)
        inpformat = outformat = V210;
    else if (strcasecmp(formatname, "nv12")==0 || strcasecmp(formatname, "nvidia12")==0)
        inpformat = outformat = NV12;
    else if (strcasecmp(formatname, "nv16")==0 || strcasecmp(formatname, "nvidia16")==0)
        inpformat = outformat = NV16;
    else
        rvexit("unknown format: %s", formatname);

    /* sanity check command line */
    if (start<0)
        rvexit("invalid value for start frame index: %d", start);

    /* open output file */
    if (outfile)
        fileout = fopen(outfile, "wb");
    if (fileout==NULL)
        rverror("failed to open output file \"%s\"", outfile);

    /* ignore SIGPIPE on output file */
    if (signal(SIGPIPE, SIG_IGN)==SIG_ERR)
        rverror("failed to set ignore on SIGPIPE");

    /* loop over input files */
    do {
        int frameno = 0;

        if (numfiles)
            filein = fopen(filename[fileindex], "rb");
        if (filein==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex]);

        /* determine input file dimensions */
        if (divine_image_dims(&width, &height, NULL, NULL, NULL, format, filename[fileindex])<0)
            divine_image_dims(&width, &height, NULL, NULL, NULL, format, outfile);
        if (verbose>=1) rvmessage("width=%d height=%d", width, height);

        /* default behaviour is to convert to planar yuv, reverse is to convert away from planar yuv */
        if (!reverse) {

            /* perform forward format conversion */
            if (inpformat==UYVY || inpformat==YUY2 || inpformat==RAWTEKTV || inpformat==AV1X) {

                /* read rawTekTV file header */
                if (inpformat==RAWTEKTV) {
                    char *buf = (char *)rvalloc(NULL, 8192*sizeof(unsigned char), 0);
                    if (fread(buf, 8192, 1, filein)!=1)
                        rverror("failed to read 8192 bytes from input");

                    /* convert newlines to string endings */
                    char *p;
                    for (p=buf; p<buf+8192; p++)
                        if (*p=='\n')
                            *p = '\0';

                    /* parse rawTekTV file header */
                    for (p=buf; p<buf+8192; p++) {
                        if (verbose>=2 && *p)
                            fprintf(stderr, "%s\n", p);
                        /* look for width and height */
                        if (sscanf(p, "size=%dx%d", &width, &height)==2)
                            if (verbose>=1)
                                rvmessage("width=%d height=%d", width, height);
                        /* look for standard */
                        char standard[256];
                        if (sscanf(p, "standard=%s", standard)==1) {
                            if (strcmp(standard, "ntsc")==0) {
                                width = 720;
                                height = 486;
                            }
                            if (verbose>=1)
                                rvmessage("width=%d height=%d", width, height);
                        }

                        /* move to next string */
                        while (*++p!='\0');
                    }

                    /* sanity check */
                    if (!width || !height)
                        rverror("failed to parse rawTekTV file header");

                    /* tidy up */
                    rvfree(buf);
                }

                /* read Avid file header */
                if (inpformat==AV1X) {
                    //fseeko(filein, 2*1152-2*768, SEEK_CUR);
                }

                /* check width and height are available */
                if (!width || !height)
                    rvexit("unknown image dimensions");

                /* allocate data buffer */
                size = width*height*2;
                data = (unsigned char *)rvalloc(NULL, size*sizeof(unsigned char), 0);

                /* prepare pointers */
                unsigned char *luma = data;
                unsigned char *blue = data + width*height;
                unsigned char *red  = data + width*height*3/2;

                for (frameno=0; !feof(filein) && !ferror(filein) && !ferror(fileout) && (frameno<numframes || numframes<=0); frameno++) {
                    int x, y;
                    unsigned int pixel;

                    if (inpformat==RAWTEKTV) {
                        char buf[16];
                        /* read rawTekTV frame header */
                        if (fread(buf, 16, 1, filein)!=1)
                            break;
                        if (verbose>=2) {
                            fprintf(stderr, "%2hhx %2hhx %2hhx %2hhx ",  buf[0],  buf[1], buf[2], buf[3]);
                            fprintf(stderr, "%2hhx %2hhx %2hhx %2hhx ",  buf[4],  buf[5], buf[6], buf[7]);
                            fprintf(stderr, "%2hhx %2hhx %2hhx %2hhx ",  buf[8],  buf[9], buf[10], buf[11]);
                            fprintf(stderr, "%2hhx %2hhx %2hhx %2hhx\n", buf[12], buf[13], buf[14], buf[15]);
                        }
                    }
                    if (inpformat==AV1X) {
                        /* read av1x frame header */
                        fseeko(filein, 2*72*16, SEEK_CUR);
                    }

                    /* read in frame from input file */
                    if (inpformat==YUY2) {

                        for (y=0; y<height; y++) {
                            for (x=0; x<width; x+=2) {
                                if (fread(&pixel, 4, 1, filein)!=1)
                                    break;

                                /* YUY2 */
                                luma[y*width+x]     = (unsigned char)((pixel>> 0) & 0xff);
                                blue[y*width/2+x/2] = (unsigned char)((pixel>> 8) & 0xff);
                                luma[y*width+x+1]   = (unsigned char)((pixel>>16) & 0xff);
                                red [y*width/2+x/2] = (unsigned char)((pixel>>24) & 0xff);
                            }
                        }

                    } else {

                        for (y=0; y<height; y++) {
                            for (x=0; x<width; x+=2) {
                                if (fread(&pixel, 4, 1, filein)!=1)
                                    break;

                                /* UYVY */
                                blue[y*width/2+x/2] = (unsigned char)((pixel>> 0) & 0xff);
                                luma[y*width+x]     = (unsigned char)((pixel>> 8) & 0xff);
                                red [y*width/2+x/2] = (unsigned char)((pixel>>16) & 0xff);
                                luma[y*width+x+1]   = (unsigned char)((pixel>>24) & 0xff);
                            }
                        }

                    }

                    if (feof(filein) || ferror(filein))
                        break;

                    /* write converted frame to output */
                    if (frameno>=start) {
                        if (fwrite(data, size, 1, fileout) != 1)
                            break;
                        else
                            totframes++;
                    }
                }
                if (ferror(filein))
                    rverror("failed to read from input");
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");

            } else if (inpformat==BITMAP) {

                /* read bitmap file header */
                /* this will only work on a little-endian machine I think */
                struct bitmap bmp;
                if (fread(&bmp, sizeof(struct bitmap), 1, filein)!=1)
                    rverror("failed to read bitmap header from input");

                /* sanity check */
                if (bmp.signature != 0x4D42) {
                    rvmessage("file \"%s\" does not have the signature of a bmp: %04x", filename[fileindex], bmp.signature);
                    goto error;
                }
                if (bmp.bits != 24) {
                    rvmessage("file \"%s\" is not 24-bit colour: %d", filename[fileindex], bmp.bits);
                    goto error;
                }
                if (bmp.compression != 0) {
                    rvmessage("file \"%s\" is compressed which is not supported: %d", filename[fileindex], bmp.compression);
                    goto error;
                }

                /* flip image data to normal raster scan */
                width = bmp.width;
                height = bmp.height;

                if (verbose>=1)
                    rvmessage("width=%d height=%d", width, height);

                /* allocate data buffer */
                size = width*height*3;
                data = (unsigned char *)rvalloc(NULL, size*sizeof(unsigned char), 0);

                /* read in frame from input file */
                int y;
                for (y=height-1; y>=0; y--) {
                    /* read in row from input file */
                    int pixels = fread(data+y*width*3, 3, width, filein);
                    if (pixels!=width) {
                        if (feof(filein))
                            break;
                        rverror("failed to read row %d from input: %d out of %d pixels read", height-1-y, pixels, width);
                    }
                    /* align to dword multiple */
                    unsigned int dummy;
                    int align = (width*3) % 4;
                    if (align)
                        if (fread(&dummy, 4-align, 1, filein)!=1)
                            break;;
                }
                if (feof(filein))
                    break;
                if (ferror(filein))
                    rverror("failed to read from input");

                /* write converted frame to output */
                if (frameno>=start) {
                    if (fwrite(data, size, 1, fileout) != 1)
                        break;
                    else
                        totframes++;
                }
                frameno++;

            } else if (inpformat==DIB555) {

                /* sanity check */
                if (!width) {
                    rvmessage("width must be given for raw DIB file \"%s\"", filename[fileindex]);
                    goto error;
                }
                if (!height) {
                    rvmessage("height must be given for raw DIB file \"%s\"", filename[fileindex]);
                    goto error;
                }


                /* allocate data buffer */
                size = width*height*2;
                data = (unsigned char *)rvalloc(NULL, size*sizeof(unsigned char), 0);

                /* read in each frame from input file */
                for (frameno=0; !feof(filein) && !ferror(filein) && !ferror(fileout) && (frameno<numframes || numframes<=0); frameno++) {
                    int y;

                    /* flip image data to normal raster scan */
                    for (y=height-1; y>=0; y--) {
                        /* read in row from input file */
                        int pixels = fread(data+y*width*2, 2, width, filein);
                        if (pixels!=width) {
                            if (feof(filein))
                                break;
                            rverror("failed to read row %d from input: %d out of %d pixels read", height-1-y, pixels, width);
                        }
                        /* align to dword multiple */
                        unsigned int dummy;
                        int align = (width*2) % 4;
                        if (align)
                            if (fread(&dummy, 4-align, 1, filein)!=1)
                                break;
                    }
                    if (feof(filein))
                        break;;
                    if (ferror(filein))
                        rverror("failed to read from input");

                    /* convert 15-bit RGB to 4:4:4 YUV */
                    unsigned char *yuv = convert_packed_rgb15_yuv((unsigned short *)data, width, height);

                    /* write converted frame to output */
                    if (frameno>=start) {
                        if (fwrite(yuv, width*height*3, 1, fileout) != 1)
                            break;
                        else
                            totframes++;
                    }
                    frameno++;

                    /* tidy up */
                    rvfree(yuv);
                }
                if (ferror(filein))
                    rverror("failed to read from input");
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");

            } else if (inpformat==SGIIMAGE) {

                /* prepare to parse file */
                struct bitbuf *bb = initbits_filename(filename[fileindex], B_GETBITS);
                if (bb==NULL)
                    rverror("failed to open file \"%s\"", filename[fileindex]);

                /* read sgi image file header */
                int channels, rle, bpc;
                if (parse_sgi(bb, &width, &height, &channels, &rle, &bpc)<0)
                    goto error;

                if (verbose>=1)
                    rvmessage("width=%d height=%d bpp=%d %s", width, height, bpc*8, rle? "RLE compressed" : "uncompressed");

                /* allocate data buffer */
                size = width*height*channels*bpc;
                data = (unsigned char *)rvalloc(NULL, size*sizeof(unsigned char), 0);

                /* read image data */
                if (!rle) {
                    int i;

                    /* loop over channels */
                    for (i=0; i<channels; i++) {
                        const int offset = i*width*height;
                        /* read in frame from input file */
                        int x, y;
                        /* flip image data to normal raster scan */
                        for (y=height-1; y>=0; y--) {
                            /* read in row from input file */
                            for (x=0; x<width; x++)
                                if (bpc==2)
                                    ((unsigned short *)data)[offset+y*width+x] = swapendians(getword(bb));
                                else
                                    data[offset+y*width+x] = getbyte(bb);
                        }
                        if (ferror(filein))
                            rverror("failed to read from input");
                    }
                } else {
                    int i;

                    /* prepare offset tables */
                    size_t tablen = height*channels*sizeof(long);
                    unsigned long *starttab = (unsigned long *)rvalloc(NULL, tablen, 0);
                    unsigned long *lengthtab = (unsigned long *)rvalloc(NULL, tablen, 0);
                    if (!starttab || !lengthtab)
                        rverror("failed to allocate %zd bytes", tablen);

                    /* read offset tables */
                    for (i=0; i<height*channels; i++)
                        starttab[i] = getbits32(bb);
                    for (i=0; i<height*channels; i++)
                        lengthtab[i] = getbits32(bb);
                    if (ferror(filein))
                        rverror("failed to read from input");

                    /* file offset before image data */
                    off_t start = tellbits(bb)/8;

                    /* read image data */
                    unsigned char *rle = NULL;
                    for (i=0; !eofbits(bb); i++) {
                        if (i%4096==0)
                            rle = (unsigned char *)rvalloc(rle, i+4096, 0);
                        rle[i] = getbits8(bb);
                    }

                    /* loop over channels */
                    for (i=0; i<channels; i++) {
                        const int offset = i*width*height;
                        /* read in compressed frame from input file */
                        int y;
                        /* flip image data to normal raster scan */
                        for (y=height-1; y>=0; y--) {
                            /* decode scanline */
                            int xsize = sgi_expandrow_char(data+offset+y*width, rle+starttab[i*height+height-y-1]-start);
                            if (xsize!=width)
                                rvexit("rle scanline not %d pixels: %d", width, xsize);
                        }
                        if (ferror(filein))
                            rverror("failed to read from input");
                    }

                    /* tidy up */
                    rvfree(rle);
                    rvfree(lengthtab);
                    rvfree(starttab);
                }

                /* convert 48-bit RGB to 4:4:4 YUV */
                unsigned char *yuv = NULL;
                if (bpc==1) {
                    if (channels>=3)
                        yuv = convert_planar_rgb24_yuv(data, width, height);
                    else
                        rvexit("time to implement grayscale conversion");
                } else if (bpc==2) {
                    unsigned short *yuv16 = NULL;
                    if (channels>=3)
                        yuv16 = convert_planar_rgb48_yuv((unsigned short *)data, width, height);
                    else
                        rvexit("time to implement grayscale conversion");
                    yuv = convert_yuv16_yuv8(yuv16, width, height, 1, 1);
                    rvfree(yuv16);
                } else
                    rvexit("sgi image file: invalid value of bpc: %d", bpc);

                /* write converted frame to output */
                if (frameno>=start) {
                    if (fwrite(yuv, width*height*3, 1, fileout) != 1)
                        break;
                    else
                        totframes++;
                }
                frameno++;

                /* tidy up */
                rvfree(yuv);
                freebits(bb);

            } else if (inpformat==V210) {

                /* check width and height are available */
                if (!width || !height)
                    rvexit("unknown image dimensions");

                /* image stride is a multiple of 48 pixels */
                int stride = ((width+47)/48)*48;

                /* allocate data buffer */
                size = width*height*2;
                data = (unsigned char *)rvalloc(NULL, size*sizeof(unsigned char), 0);

                /* prepare pointers */
                unsigned char *luma = data;
                unsigned char *blue = data + width*height;
                unsigned char *red  = data + width*height*3/2;

                for (frameno=0; !feof(filein) && !ferror(filein) && !ferror(fileout) && (frameno<numframes || numframes<=0); frameno++) {
                    int x, y;

                    /* read in frame from input file */
                    for (y=0; y<height; y++) {
                        for (x=0; x<stride; x+=6) {
                            unsigned int block[4];
                            if (fread(&block, 16, 1, filein)!=1)
                                break;

                            /* V120 */
                            blue[y*width/2+x/2+0] = (block[0] >>  2) & 0xff;
                            luma[y*width+x+0]     = (block[0] >> 12) & 0xff;
                            red [y*width/2+x/2+0] = (block[0] >> 22) & 0xff;
                            luma[y*width+x+1]     = (block[1] >>  2) & 0xff;
                            if (x+2==width)
                                continue;
                            blue[y*width/2+x/2+1] = (block[1] >> 12) & 0xff;
                            luma[y*width+x+2]     = (block[1] >> 22) & 0xff;
                            red [y*width/2+x/2+1] = (block[2] >>  2) & 0xff;
                            luma[y*width+x+3]     = (block[2] >> 12) & 0xff;
                            if (x+4==width)
                                continue;
                            blue[y*width/2+x/2+2] = (block[2] >> 22) & 0xff;
                            luma[y*width+x+4]     = (block[3] >>  2) & 0xff;
                            red [y*width/2+x/2+2] = (block[3] >> 12) & 0xff;
                            luma[y*width+x+5]     = (block[3] >> 22) & 0xff;
                        }
                    }

                    if (feof(filein) || ferror(filein))
                        break;

                    /* write converted frame to output */
                    if (frameno>=start) {
                        if (fwrite(data, size, 1, fileout) != 1)
                            break;
                        else
                            totframes++;
                    }
                }
                if (ferror(filein))
                    rverror("failed to read from input");
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");

            } else if (inpformat==NV12 || inpformat==NV16) {

                /* check width and height are available */
                if (!width || !height)
                    rvexit("unknown image dimensions");

                /* prepare subsampling factors */
                int hsub, vsub;
                switch (inpformat) {
                    case NV12: hsub=2; vsub=2; break;
                    case NV16: hsub=1; vsub=2; break;
                    default  : rvexit("data format is neither nv12 nor nv16");
                }

                /* allocate data buffer */
                size = width*height + 2*width*height/hsub/vsub;
                data = (unsigned char *)rvalloc(NULL, size*sizeof(unsigned char), 0);

                for (frameno=0; !feof(filein) && !ferror(filein) && !ferror(fileout) && (frameno<numframes || numframes<=0); frameno++) {
                    int x, y;

                    /* read in frame from input file */
                    if (fread(data, size, 1, filein)!=1)
                        break;

                    /* prepare pointers */
                    unsigned char *luma = data;
                    unsigned char *blue = luma + width*height;
                    unsigned char *red  = blue + 1;

                    /* write converted frame to output */
                    if (frameno>=start) {
                        /* write luma */
                        if (fwrite(luma, width*height, 1, fileout) != 1)
                            break;
                        /* write chroma */
                        for (y=0; y<height/vsub; y++) {
                            for (x=0; x<width/hsub; x++) {
                                fwrite(blue, 1, 1, fileout);
                                blue += 2;
                            }
                        }
                        for (y=0; y<height/vsub; y++) {
                            for (x=0; x<width/hsub; x++) {
                                fwrite(red, 1, 1, fileout);
                                red += 2;
                            }
                        }
                        if (ferror(fileout))
                            break;
                        totframes++;
                    }
                }
                if (ferror(filein))
                    rverror("failed to read from input");
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");

            } else {
                rvmessage("%s: unsupported format conversion: %s to planar yuv", filename[fileindex], formatname);
            }

        } else { /* if (!reverse) */

            if (outformat==UYVY || outformat==YUY2) {

                /* check width and height are available */
                if (!width || !height)
                    rvexit("unknown image dimensions");

                /* allocate data buffer */
                size = width*height*2;
                data = (unsigned char *)rvalloc(NULL, size*sizeof(unsigned char), 0);

                /* prepare pointers */
                unsigned char *luma = data;
                unsigned char *blue = data + width*height;
                unsigned char *red  = data + width*height*3/2;

                for (frameno=0; !feof(filein) && !ferror(filein) && !ferror(fileout) && (frameno<numframes || numframes<=0); frameno++) {
                    int x, y;

                    /* read in frame from input file */
                    if (fread(data, size, 1, filein) != 1)
                        break;

                    /* write converted frame to output */
                    if (frameno>=start) {
                        if (outformat==YUY2) {

                            for (y=0; y<height; y++) {
                                for (x=0; x<width; x+=2) {
                                    /* YUY2 */
                                    fputc(luma[y*width+x],     fileout);
                                    fputc(blue[y*width/2+x/2], fileout);
                                    fputc(luma[y*width+x+1],   fileout);
                                    fputc(red [y*width/2+x/2], fileout);
                                }
                                if (feof(fileout) || ferror(fileout))
                                    break;
                            }

                        } else {

                            for (y=0; y<height; y++) {
                                for (x=0; x<width; x+=2) {
                                    /* UYVY */
                                    fputc(blue[y*width/2+x/2], fileout);
                                    fputc(luma[y*width+x],     fileout);
                                    fputc(red [y*width/2+x/2], fileout);
                                    fputc(luma[y*width+x+1],   fileout);
                                }
                                if (feof(fileout) || ferror(fileout))
                                    break;
                            }

                        }
                        totframes++;
                    }

                }
                if (ferror(filein))
                    rverror("failed to read from input");
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");

            } else if (outformat==V210) {

                /* check width and height are available */
                if (!width || !height)
                    rvexit("unknown image dimensions");

                /* image stride is a multiple of 48 pixels */
                int stride = ((width+47)/48)*48;

                /* allocate data buffer */
                size = width*height*2;
                data = (unsigned char *)rvalloc(NULL, size*sizeof(unsigned char), 0);

                /* prepare pointers */
                unsigned char *luma = data;
                unsigned char *blue = data + width*height;
                unsigned char *red  = data + width*height*3/2;

                for (frameno=0; !feof(filein) && !ferror(filein) && !ferror(fileout) && (frameno<numframes || numframes<=0); frameno++) {
                    int x, y;

                    /* read in frame from input file */
                    if (fread(data, size, 1, filein) != 1)
                        break;

                    /* write converted frame to output */
                    if (frameno>=start) {
                        for (y=0; y<height; y++) {
                            for (x=0; x<stride; x+=6) {
                                unsigned int block[4] = {0,0,0,0};

                                /* V120 */
                                if (x<width) {
                                    int r = rand();
                                    block[0] |= blue[y*width/2+x/2+0] << 2 | ((r>>30) & 0x3);
                                    block[0] |= luma[y*width+x+0]     << 12| ((r>>28) & 0x3);
                                    block[0] |= red [y*width/2+x/2+0] << 22| ((r>>26) & 0x3);
                                    block[1] |= luma[y*width+x+1]     << 2 | ((r>>24) & 0x3);
                                    block[1] |= blue[y*width/2+x/2+1] << 12| ((r>>22) & 0x3);
                                    block[1] |= luma[y*width+x+2]     << 22| ((r>>20) & 0x3);
                                    block[2] |= red [y*width/2+x/2+1] << 2 | ((r>>18) & 0x3);
                                    block[2] |= luma[y*width+x+3]     << 12| ((r>>16) & 0x3);
                                    block[2] |= blue[y*width/2+x/2+2] << 22| ((r>>14) & 0x3);
                                    block[3] |= luma[y*width+x+4]     << 2 | ((r>>12) & 0x3);
                                    block[3] |= red [y*width/2+x/2+2] << 12| ((r>>10) & 0x3);
                                    block[3] |= luma[y*width+x+5]     << 22| ((r>>8 ) & 0x3);
                                }

                                if (fwrite(&block, 16, 1, fileout)!=1)
                                    break;
                            }
                        }
                    }
                    totframes++;
                }
                if (ferror(filein))
                    rverror("failed to read from input");
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");

            } else if (outformat==NV12 || outformat==NV16) {

                /* check width and height are available */
                if (!width || !height)
                    rvexit("unknown image dimensions");

                /* prepare subsampling factors */
                int hsub, vsub;
                switch (outformat) {
                    case NV12: hsub=2; vsub=2; break;
                    case NV16: hsub=1; vsub=2; break;
                    default  : rvexit("data format is neither nv12 nor nv16");
                }

                /* allocate data buffer */
                size = width*height + 2*width*height/hsub/vsub;
                data = (unsigned char *)rvalloc(NULL, size*sizeof(unsigned char), 0);

                /* prepare pointers */
                unsigned char *luma = data;
                unsigned char *blue = luma + width*height;
                unsigned char *red  = blue + width*height/hsub/vsub;

                for (frameno=0; !feof(filein) && !ferror(filein) && !ferror(fileout) && (frameno<numframes || numframes<=0); frameno++) {
                    int x, y;

                    /* read in frame from input file */
                    if (fread(data, size, 1, filein)!=1)
                        break;

                    /* write converted frame to output */
                    if (frameno>=start) {
                        /* write luma */
                        if (fwrite(luma, width*height, 1, fileout) != 1)
                            break;
                        /* write chroma */
                        for (y=0; y<height/vsub; y++) {
                            for (x=0; x<width/hsub; x++) {
                                fwrite(blue+(y*width/hsub+x), 1, 1, fileout);
                                fwrite(red +(y*width/hsub+x), 1, 1, fileout);
                            }
                        }
                        if (ferror(fileout))
                            break;
                        totframes++;
                    }
                }
                if (ferror(filein))
                    rverror("failed to read from input");
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");

            } else {
                rvmessage("%s: unsupported format conversion: planar yuv to %s", filename[fileindex], formatname);
            }

        }

error:
        /* tidy up */
        rvfree(data);
        if (numfiles)
            fclose(filein);

    } while (++fileindex<numfiles && !ferror(fileout));
    if (ferror(fileout) && errno!=EPIPE)
        rverror("failed to write to output");

    if (verbose>=0)
        rvmessage("converted %d frames", totframes);

    return 0;
}
