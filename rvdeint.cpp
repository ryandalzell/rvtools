/*
 * Description: Deinterlace raw video files.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-06-23 13:26:19 $
 * Revision   : $Revision: 1.25 $
 * Copyright  : (c) 2005,2011,2012 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include "getopt.h"

#include "rvutil.h"

typedef enum {
    WEAVE,
    DISCARD,
    BLEND,
    BOB,
    LINEAR,
    YADIF,
    YADIF2x,
} deint_t;

static const struct {
    const char *name;
    deint_t algo;
    int field; /* 0:top, 1:bottom */
} algos[] = {
    { "weave"  , WEAVE  , 0 },
    { "discard", DISCARD, 0 },
    { "single" , DISCARD, 0 },
    { "top"    , DISCARD, 0 },
    { "bottom" , DISCARD, 1 },
    { "blend"  , BLEND  , 0 },
    { "bob"    , BOB    , 0 },
    { "linear" , LINEAR , 0 },
    { "yadif"  , YADIF  , 0 },
    { "yadif2x", YADIF2x, 0 },
    { "yadifx2", YADIF2x, 0 },
    { "2xyadif", YADIF2x, 0 },
    { NULL     , WEAVE  , 0 },
};

const char *appname;

void usage(int exitcode)
{
    /* prepare list of deinterlacing algorithms */
    int i, len = 0;
    char s[256];
    for (i=0; algos[i].name; i++) {
        if (i==0)
            len = snprintf(s, sizeof(s), "%s", algos[i].name);
        else
            len += snprintf(s+len, sizeof(s)-len, ", %s", algos[i].name);
    }
    fprintf(stderr, "%s: deinterlace raw video files\n", appname);
    fprintf(stderr, "usage: %s [options] [<file>] [<file>...]\n", appname);
    fprintf(stderr, "  -a, --algorithm     : deinterlace algorithm to use: %s (default: discard)\n", s);
    fprintf(stderr, "  -s, --size          : image size format\n");
    fprintf(stderr, "     (formats : %s)\n", rvformats);
    fprintf(stderr, "  -p, --fourcc        : image pixel format fourcc (default: I420)\n");
    fprintf(stderr, "     (formats : %s)\n", rvfourccs);
    fprintf(stderr, "  -n, --numframes     : number of frames (default: all)\n");
    fprintf(stderr, "  -o, --output        : write output to file\n");
    fprintf(stderr, "  -q, --quiet         : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "  -v, --verbose       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "  --                  : disable argument processing\n");
    fprintf(stderr, "  -h, --help, --usage : print this usage message\n");
    exit(exitcode);
}

/*
 * Copyright (C) 2006-2010 Michael Niedermayer <michaelni@gmx.at>
 *               2010      James Darnley <james.darnley@gmail.com>
 *
 */

#define FFABS(x) ((x)<0? -(x) : (x))

#define SCORE(j)    FFABS(cur[above-1+(j)] - cur[below-1-(j)])\
                  + FFABS(cur[above  +(j)] - cur[below  -(j)])\
                  + FFABS(cur[above+1+(j)] - cur[below+1-(j)])

static void yadif_filter_line(uint8_t *dst, const uint8_t * __restrict__ prev, const uint8_t * __restrict__ cur, const uint8_t * __restrict__ next,
                          int w, int below, int above, int parity, int mode)
{
    int x;

    /* parity: 1=previous frame contains temporally closest field to this line */
    const uint8_t *prev2 = parity ? prev : cur ;
    const uint8_t *next2 = parity ? cur  : next;

    for (x = 0; x < w; x++) {
        /* c-pel above in this field
         * d-interpolate current pel in past and future field
         * e-pel below in this field
         */
        int c = cur[above];
        int d = (prev2[0] + next2[0])>>1;
        int e = cur[below];

        /* initial spatial prediction and score */
        int spatial_pred = (c+e)>>1;
        int spatial_score = SCORE(0) - 1;

        /* search horizontal offsets for better spatial prediction */
        int score = SCORE(-1);
        if (score < spatial_score) {
            spatial_score = score;
            spatial_pred  = (cur[above-1] + cur[below+1])>>1;
            score = SCORE(-2);
            if (score < spatial_score) {
                spatial_score = score;
                spatial_pred  = (cur[above-2] + cur[below+2])>>1;
            }
        }
        score = SCORE(1);
        if (score < spatial_score) {
            spatial_score = score;
            spatial_pred  = (cur[above+1] + cur[below-1])>>1;
            score = SCORE(2);
            if (score < spatial_score) {
                spatial_score = score;
                spatial_pred  = (cur[above+2] + cur[below-2])>>1;
            }
        }

        /* find largest temporal differences */
        int temporal_diff = FFABS(prev2[0] - next2[0]);
        int temporal_prev =(FFABS(prev[above] - c) + FFABS(prev[below] - e) )>>1;
        int temporal_next =(FFABS(next[above] - c) + FFABS(next[below] - e) )>>1;
        int diff = mmax3(temporal_diff>>1, temporal_prev, temporal_next);
        if (mode < 2) {
            int b = (prev2[2*above] + next2[2*above])>>1;
            int f = (prev2[2*below] + next2[2*below])>>1;
            int max = mmax3(d-e, d-c, mmin(b-c, f-e));
            int min = mmin3(d-e, d-c, mmax(b-c, f-e));

            diff = mmax3(diff, min, -max);
        }

        /* limit the spatial prediction to the temporal difference */
        dst[0] = clip(spatial_pred, d-diff, d+diff);

        dst++;
        cur++;
        prev++;
        next++;
        prev2++;
        next2++;
    }
}

int main(int argc, char *argv[])
{
    FILE *filein = stdin;
    FILE *fileout = stdout;
    char *filename[RV_MAXFILES] = {0};
    char *outfile = NULL;
    int fileindex = 0;
    int numfiles = 0;
    int totframes = 0;

    /* data buffers - previous, current and next */
    int size = 0;
    unsigned char *data[3] = {NULL, NULL, NULL};

    /* command line defaults */
    deint_t algorithm = DISCARD;
    int dominant_field = 0; /* top field */
    const char *format = NULL;
    const char *fccode = NULL;
    int numframes = -1;
    int verbose = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    int i;
    while (1) {
        static struct option long_options[] = {
            {"algorithm", 1, NULL, 'a'},
            {"size",      1, NULL, 's'},
            {"fourcc",    1, NULL, 'p'},
            {"numframes", 1, NULL, 'n'},
            {"output",    1, NULL, 'o'},
            {"quiet",     0, NULL, 'q'},
            {"verbose",   0, NULL, 'v'},
            {"usage",     0, NULL, 'h'},
            {"help",      0, NULL, 'h'},
            {NULL,        0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "a:s:p:n:o:qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 'a':
                for (i=0; ; i++) {
                    if (algos[i].name==NULL)
                        rvexit("unknown deinterlacing algorithm: %s", optarg);
                    if (strcasecmp(optarg, algos[i].name)==0) {
                        algorithm = algos[i].algo;
                        dominant_field = algos[i].field;
                        break;
                    }
                }
                break;

            case 's':
                format = optarg;
                break;

            case 'p':
                fccode = optarg;
                break;

            case 'n':
                numframes = atoi(optarg);
                if (numframes<=0)
                    rvexit("invalid value for numframes: %d", numframes);
                break;

            case 'o':
                outfile = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < sizeof(filename)/sizeof(filename[0]))
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    /* display notes for each algorithm */
    switch (algorithm) {
        case BOB:
        case LINEAR:
        case YADIF2x:
            if (verbose>=0)
                rvmessage("note: output frame rate is doubled");
            break;

        default:
            break;
    }

    /* open output file */
    if (outfile)
        fileout = fopen(outfile, "wb");
    if (fileout==NULL)
        rverror("failed to open output file \"%s\"", outfile);

    /* ignore SIGPIPE on output file */
    if (signal(SIGPIPE, SIG_IGN)==SIG_ERR)
        rverror("failed to set ignore on SIGPIPE");

    /* loop over input files */
    do {
        int width;
        int height;
        framerate_t framerate;
        fourcc_t fourcc;

        /* determine input file dimensions */
        divine_image_dims(&width, &height, NULL, &framerate, &fourcc, format, filename[fileindex]);

        /* override fourcc */
        if (fccode)
            fourcc = get_fourcc(fccode);

        /* get chroma subsampling from fourcc */
        int hsub, vsub;
        get_chromasub(fourcc, &hsub, &vsub);
        if (hsub==-1 || vsub==-1)
            rvexit("unknown pixel format: %s", fourccname(fourcc));

        /* (re-)allocate data buffers */
        size = get_framesize(width, height, hsub, vsub, 8);
        data[0] = (unsigned char *)rvalloc(data[0], size*sizeof(unsigned char), 0);
        data[1] = (unsigned char *)rvalloc(data[1], size*sizeof(unsigned char), 0);
        data[2] = (unsigned char *)rvalloc(data[2], size*sizeof(unsigned char), 0);

        /* open next input file, or use stdin */
        if (numfiles)
            filein = fopen(filename[fileindex], "rb");
        if (filein==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex]);

        /* pre-load first frame */
        int read = fread(data[0], size, 1, filein);
        if (read!=1)
            rverror("failed from read first frame from file \"%s\"", filename[fileindex]);

        /* first pass in loop will have past frame equal to current */
        memcpy(data[1], data[0], size);

        /* loop over frames in input file */
        int eof = 0;
        int frameno;
        for (frameno=0; !eof && (totframes<numframes || numframes<0) && !feof(filein) && !ferror(filein) && !ferror(fileout); frameno++)
        {
            /* shuffle frame pointers */
            unsigned char *swap = data[2];
            data[2] = data[1]; /* past frame */
            data[1] = data[0]; /* current frame */
            data[0] = swap;

            /* read next frame */
            read = fread(data[0], size, 1, filein); /* future frame */
            if (read!=1) {
                /* last pass in loop with have future frame equal to current */
                memcpy(data[0], data[1], size);
                eof = 1;
            }

            /* prepare pointers to chroma planes */
            unsigned char *past[3]    = {data[2], data[2]+width*height, data[2]+width*height+width/hsub*height/vsub};
            unsigned char *current[3] = {data[1], data[1]+width*height, data[1]+width*height+width/hsub*height/vsub};
            unsigned char *future[3]  = {data[0], data[0]+width*height, data[0]+width*height+width/hsub*height/vsub};

            int j;
            switch (algorithm) {
                case WEAVE: /* ok, so this does nothing */
                    if (fwrite(current[0], size, 1, fileout) == 1)
                        totframes++;
                    break;

                case DISCARD:
                {
                    int line;
                    for (i=0, line=0; i<height; i++) {
                        if (i%2==dominant_field) /* dominant field */
                            fwrite(current[0]+line*width, width, 1, fileout);
                        else {                   /* duplicated field */
                            fwrite(current[0]+line*width, width, 1, fileout);
                            line += 2;
                        }
                    }
                    for (i=0, line=0; i<height/vsub; i++) {
                        if (i%2==dominant_field)
                            fwrite(current[1]+line*width/hsub, width/hsub, 1, fileout);
                        else {
                            fwrite(current[1]+line*width/hsub, width/hsub, 1, fileout);
                            line += 2;
                        }
                    }
                    for (i=0, line=0; i<height/vsub; i++) {
                        if (i%2==dominant_field)
                            fwrite(current[2]+line*width/hsub, width/hsub, 1, fileout);
                        else {
                            fwrite(current[2]+line*width/hsub, width/hsub, 1, fileout);
                            line += 2;
                        }
                    }
                    totframes++;
                    break;
                }

                case BLEND:
                    fwrite(current[0], width, 1, fileout);
                    for (j=1; j<height; j++) {
                        for (i=0; i<width; i++)
                            fputc((current[0][(j-1)*width+i]+current[0][j*width+i]+1)>>1, fileout);
                    }
                    fwrite(current[1], width/hsub, 1, fileout);
                    for (j=1; j<height/vsub; j++) {
                        for (i=0; i<width/hsub; i++)
                            fputc((current[1][(j-1)*width/hsub+i]+current[1][j*width/hsub+i]+1)>>1, fileout);
                    }
                    fwrite(current[2], width/hsub, 1, fileout);
                    for (j=1; j<height/vsub; j++) {
                        for (i=0; i<width/hsub; i++)
                            fputc((current[2][(j-1)*width/hsub+i]+current[2][j*width/hsub+i]+1)>>1, fileout);
                    }
                    totframes++;
                    break;

                case BOB:
                {
                    /* first frame */
                    int line;
                    for (i=0, line=dominant_field; i<height; i+=2) {
                        fwrite(current[0]+line*width, width, 1, fileout);
                        fwrite(current[0]+line*width, width, 1, fileout);
                        line+=2;
                    }
                    for (i=0, line=dominant_field; i<height/vsub; i+=2) {
                        fwrite(current[1]+line*width/hsub, width/hsub, 1, fileout);
                        fwrite(current[1]+line*width/hsub, width/hsub, 1, fileout);
                        line+=2;
                    }
                    for (i=0, line=dominant_field; i<height/vsub; i+=2) {
                        fwrite(current[2]+line*width/hsub, width/hsub, 1, fileout);
                        fwrite(current[2]+line*width/hsub, width/hsub, 1, fileout);
                        line+=2;
                    }
                    /* second frame */
                    for (i=0, line=!dominant_field; i<height; i+=2) {
                        fwrite(current[0]+line*width, width, 1, fileout);
                        fwrite(current[0]+line*width, width, 1, fileout);
                        line+=2;
                    }
                    for (i=0, line=!dominant_field; i<height/vsub; i+=2) {
                        fwrite(current[1]+line*width/hsub, width/hsub, 1, fileout);
                        fwrite(current[1]+line*width/hsub, width/hsub, 1, fileout);
                        line+=2;
                    }
                    for (i=0, line=!dominant_field; i<height/vsub; i+=2) {
                        fwrite(current[2]+line*width/hsub, width/hsub, 1, fileout);
                        fwrite(current[2]+line*width/hsub, width/hsub, 1, fileout);
                        line+=2;
                    }
                    totframes+=2;
                    break;
                }

                case LINEAR:
                {
                    // TODO bottom field first.
                    /* first frame */
                    int line;
                    for (j=0, line=0; j<height-2; j+=2) {
                        fwrite(current[0]+line*width, width, 1, fileout);
                        for (i=0; i<width; i++)
                            fputc((current[0][line*width+i]+current[0][(line+2)*width+i]+1)>>1, fileout);
                        line+=2;
                    }
                    fwrite(current[0]+line*width, width, 1, fileout);
                    fwrite(current[0]+line*width, width, 1, fileout);
                    for (j=0, line=0; j<height/vsub-2; j+=2) {
                        fwrite(current[1]+line*width/hsub, width/hsub, 1, fileout);
                        for (i=0; i<width/hsub; i++)
                            fputc((current[1][line*width/hsub+i]+current[1][(line+2)*width/hsub+i]+1)>>1, fileout);
                        line+=2;
                    }
                    fwrite(current[1]+line*width/hsub, width/hsub, 1, fileout);
                    fwrite(current[1]+line*width/hsub, width/hsub, 1, fileout);
                    for (j=0, line=0; j<height/vsub-2; j+=2) {
                        fwrite(current[2]+line*width/hsub, width/hsub, 1, fileout);
                        for (i=0; i<width/hsub; i++)
                            fputc((current[2][line*width/hsub+i]+current[2][(line+2)*width/hsub+i]+1)>>1, fileout);
                        line+=2;
                    }
                    fwrite(current[2]+line*width/hsub, width/hsub, 1, fileout);
                    fwrite(current[2]+line*width/hsub, width/hsub, 1, fileout);
                    /* second frame */
                    fwrite(current[0]+1*width, width, 1, fileout);
                    fwrite(current[0]+1*width, width, 1, fileout);
                    for (j=2, line=3; j<height; j+=2) {
                        for (i=0; i<width; i++)
                            fputc((current[0][(line-2)*width+i]+current[0][line*width+i]+1)>>1, fileout);
                        fwrite(current[0]+line*width, width, 1, fileout);
                        line+=2;
                    }
                    fwrite(current[1]+1*width/hsub, width/hsub, 1, fileout);
                    fwrite(current[1]+1*width/hsub, width/hsub, 1, fileout);
                    for (j=2, line=3; j<height/vsub; j+=2) {
                        for (i=0; i<width/hsub; i++)
                            fputc((current[1][(line-2)*width/hsub+i]+current[1][line*width/hsub+i]+1)>>1, fileout);
                        fwrite(current[1]+line*width/hsub, width/hsub, 1, fileout);
                        line+=2;
                    }
                    fwrite(current[2]+1*width/hsub, width/hsub, 1, fileout);
                    fwrite(current[2]+1*width/hsub, width/hsub, 1, fileout);
                    for (j=2, line=3; j<height/vsub; j+=2) {
                        for (i=0; i<width/hsub; i++)
                            fputc((current[2][(line-2)*width/hsub+i]+current[2][line*width/hsub+i]+1)>>1, fileout);
                        fwrite(current[2]+line*width/hsub, width/hsub, 1, fileout);
                        line+=2;
                    }
                    totframes+=2;
                    break;
                }

                case YADIF:
                {
                    /* allocate frame of data for deinterlaced results */
                    unsigned char *result[3];
                    result[0] = (unsigned char *)malloc(size*sizeof(unsigned char));
                    result[1] = result[0] + width*height;
                    result[2] = result[1] + width*height/hsub/vsub;

                    {
                        const int parity = 0;
                        const int tff = 1;
                        int y, i;

                        for (i=0; i<3; i++) {
                            int w = i==0? width  : width/hsub;
                            int h = i==0? height : height/vsub;

                            for (y=0; y<h; y++) {
                                if ((y ^ parity) & 1) {
                                    uint8_t *prev = &past[i][y*w];
                                    uint8_t *cur  = &current[i][y*w];
                                    uint8_t *next = &future[i][y*w];
                                    uint8_t *dst  = &result[i][y*w];
                                    int     mode  = y==1 || y+2==h ? 2 : 0;
                                    yadif_filter_line(dst, prev, cur, next, w, y+1<h ? w : -w, y ? -w : w, parity ^ tff, mode);
                                } else {
                                    memcpy(&result[i][y*w], &current[i][y*w], w);
                                }
                            }
                        }
                    }

                    /* write results */
                    if (fwrite(result[0], size, 1, fileout) == 1)
                        totframes++;

                    /* tidy up */
                    free(result[0]);
                    break;
                }

                case YADIF2x:
                {
                    /* allocate frame of data for deinterlaced results */
                    unsigned char *result[3];
                    result[0] = (unsigned char *)malloc(size*sizeof(unsigned char));
                    result[1] = result[0] + width*height;
                    result[2] = result[1] + width*height/hsub/vsub;

                    int parity = 0;
                    for (parity=0; parity<2; parity++)
                    {
                        const int tff = 1;
                        int y, i;

                        for (i=0; i<3; i++) {
                            int w = i==0? width  : width/hsub;
                            int h = i==0? height : height/vsub;

                            for (y=0; y<h; y++) {
                                if ((y ^ parity) & 1) {
                                    uint8_t *prev = &past[i][y*w];
                                    uint8_t *cur  = &current[i][y*w];
                                    uint8_t *next = &future[i][y*w];
                                    uint8_t *dst  = &result[i][y*w];
                                    int     mode  = y==1 || y+2==h ? 2 : 0;
                                    yadif_filter_line(dst, prev, cur, next, w, y+1<h ? w : -w, y ? -w : w, parity ^ tff, mode);
                                } else {
                                    memcpy(&result[i][y*w], &current[i][y*w], w);
                                }
                            }
                        }

                        /* write results */
                        if (fwrite(result[0], size, 1, fileout) == 1)
                            totframes++;
                    }

                    /* tidy up */
                    free(result[0]);
                    break;
                }
            }
        }
        if (ferror(filein))
            rverror("failed to read from input");
        if (ferror(fileout) && errno!=EPIPE)
            rverror("failed to write to output");

        if (numfiles)
            fclose(filein);

    } while (++fileindex<numfiles && !ferror(fileout));

    if (verbose>=0)
        rvmessage("wrote %d frames", totframes);

    /* tidy up */
    rvfree(data[0]);
    rvfree(data[1]);
    rvfree(data[2]);

    return 0;
}
