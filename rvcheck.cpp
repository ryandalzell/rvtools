/*
 * Description: Bitstream syntax and constraints checker.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-12-21 10:47:33 $
 * Revision   : $Revision: 1.45 $
 * Copyright  : (c) 2005-2008 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "getopt.h"
#include <math.h>

#include "rvutil.h"
#include "rvm2v.h"
#include "rvh264.h"
#include "rvmp2.h"
#include "rvcrc.h"

#define STATUS_PARSE_ERROR 2

const char *appname;

void usage(int exitcode)
{
    fprintf(stderr, "%s: bitstream syntax and constraints checker\n", appname);
    fprintf(stderr, "usage: %s [options] [<file>] [<file>...]\n", appname);
    fprintf(stderr, "  -t, --type          : force type of file (default: autodetect)\n");
    fprintf(stderr, "  -p, --pid           : only check given pid in transport stream\n");
    fprintf(stderr, "  -m, --margin        : safe timing margin in transport streams, in seconds, default: 2\n");
    fprintf(stderr, "  -n, --noexit        : attempt to carry on checking after first error\n");
    fprintf(stderr, "  -q, --quiet         : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "  -v, --verbose       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "  --                  : disable argument processing\n");
    fprintf(stderr, "  -h, --help, --usage : print this usage message\n");
    exit(exitcode);
}

bool is_in_list(int needle, int haystack[], const size_t size)
{
    for (int i=0; i<size; i++)
        if (needle==haystack[i])
            return true;
    return false;
}

double vbv_time_interval(struct rvm2vsequence *seq, struct rvm2vpicture *picture, struct rvm2vpicture *prev_picture, struct rvm2vpicture *prev_ref_picture)
{
    /* calculate the interval of time between removal of picture (n) and picture (n+1) */
    /* it's simple really... not! */
    double tdiff;
    if (seq->progressive_sequence==1 && seq->low_delay==0) {
        /* C.9 */
        double T = 1.0 / seq->frame_rate;
        if (prev_picture->picture_coding_type==PCT_B)
            tdiff = picture_repeat(1, 1, prev_picture->extension.repeat_first_field, prev_picture->extension.top_field_first) * T;
        else
            tdiff = picture_repeat(1, 1, prev_ref_picture->extension.repeat_first_field, prev_ref_picture->extension.top_field_first) * T;
    } else if (seq->progressive_sequence==1 && seq->low_delay==1) {
        /* C.10 */
        double T = 1.0 / seq->frame_rate;
        tdiff = picture_repeat(1, 1, prev_picture->extension.repeat_first_field, prev_picture->extension.top_field_first) * T;
    } else if (seq->progressive_sequence==0 && seq->low_delay==0) {
        /* C.11 */
        double T = 0.5 / seq->frame_rate;
        if (prev_picture->picture_coding_type==PCT_B) {
            if (prev_picture->extension.picture_structure==PST_FRAME)
                tdiff = prev_picture->extension.repeat_first_field==0? 2*T : 3*T;
            else
                tdiff = T;
        } else {
            if (prev_picture->extension.picture_structure==PST_FRAME)
                tdiff = prev_ref_picture->extension.repeat_first_field==0? 2*T : 3*T;
            else {
                if (first_field_picture(picture, prev_picture))
                    /* first field */
                    tdiff = T;
                else
                    /* second field */
                    tdiff = prev_ref_picture->extension.picture_structure!=PST_FRAME || prev_ref_picture->extension.repeat_first_field==0? 2*T-T : 3*T-T;
            }
        }
    } else {
        /* C.12 */
        double T = 0.5 / seq->frame_rate;
        if (prev_picture->extension.picture_structure==PST_FRAME)
            tdiff = prev_picture->extension.repeat_first_field==0? 2*T : 3*T;
        else
            tdiff = T;
    }
    return tdiff;
}

double vbv_init_buffer(struct rvm2vsequence *seq, struct rvm2vpicture *picture, int header_bits)
{
    return header_bits + seq->bit_rate*400.0 * picture->vbv_delay/90000.0;
}

int vbv_check_rate(struct rvm2vsequence *seq, struct rvm2vpicture *picture, int vbv_bits, int vbv_prev_delay, double vbv_interval, const char *filename, int picno)
{
    /* calculate the buffer fill rate */
    double vbv_diff = (vbv_prev_delay - picture->vbv_delay) / 90000.0;
    double R = vbv_bits / (vbv_diff + vbv_interval);
    double Rplus = vbv_bits / (vbv_diff - 1.0/90000.0 + vbv_interval);
    double Rminus = vbv_bits / (vbv_diff + 1.0/90000.0 + vbv_interval);

    /* check the rate is constant within the tolerance of vbv_delay quantisation */
    if (Rminus>seq->bit_rate*400) {
        rvmessage("%s: picture %d: vbv rate higher than bitrate %d: %.2f", filename, picno, seq->bit_rate*400, R);
        return -1;
    }
    if (Rplus<seq->bit_rate*400) {
        rvmessage("%s: picture %d: vbv rate lower than bitrate %d: %.2f", filename, picno, seq->bit_rate*400, R);
        return -1;
    }
    return 0;
}

double vbv_add_bits(struct rvm2vsequence *seq, double vbv_interval)
{
    return seq->bit_rate*400.0 * vbv_interval;
}

int check_m2v(struct bitbuf *bb, const char *filename, int verbose)
{
    int status = 0;

    /*
    * NOTE syntax is generally checked within the m2v functions
    * whereas constraints are checked at this level.
    */

    /* parse mpeg2 headers */
    struct rvm2vsequence *seq = parse_m2v_headers(bb, verbose);
    if (seq==NULL) {
        rvmessage("failed to parse headers in mpeg2 sequence \"%s\"", filename);
        status = STATUS_PARSE_ERROR;
        return status;
    }

    /* check mpeg2 headers */
    if (seq->horizontal_size_value%4096==0)
        rvmessage("%s: horizontal_size_value should not be a multiple of 4096 (or zero)", filename);
    if (seq->vertical_size_value%4096==0)
        rvmessage("%s: vertical_size_value should not be a multiple of 4096 (or zero)", filename);
    if (seq->aspect_ratio_information<1 || seq->aspect_ratio_information>4)
        rvmessage("%s: aspect_ratio_information has an invalid value: %d", filename, seq->aspect_ratio_information);
    else {
        /* check some common aspect ratios */
        if (seq->horizontal_size_value==720 && seq->vertical_size_value==576)
            if (seq->aspect_ratio_information!=2)
                rvmessage("%s: PAL sized video should have an aspect ratio of %s, not %s", filename, aspect_ratio_table[2], aspect_ratio_table[seq->aspect_ratio_information]);
        if (seq->horizontal_size_value==720 && seq->vertical_size_value==480)
            if (seq->aspect_ratio_information!=2)
                rvmessage("%s: NTSC sized video should have an aspect ratio of %s, not %s", filename, aspect_ratio_table[2], aspect_ratio_table[seq->aspect_ratio_information]);
        if (seq->horizontal_size_value==1280 && seq->vertical_size_value==720)
            if (seq->aspect_ratio_information!=3)
                rvmessage("%s: 720p sized video should have an aspect ratio of %s, not %s", filename, aspect_ratio_table[3], aspect_ratio_table[seq->aspect_ratio_information]);
        if (seq->horizontal_size_value==1920 && seq->vertical_size_value==1080)
            if (seq->aspect_ratio_information!=3)
                rvmessage("%s: PAL 1080i/1080p sized video should have an aspect ratio of %s, not %s", filename, aspect_ratio_table[3], aspect_ratio_table[seq->aspect_ratio_information]);
    }
    if (seq->bit_rate==0)
        rvmessage("%s: bit_rate_value of zero is forbidden", filename);

    /* check profile and level constraints */
    float sample_rate = seq->horizontal_size_value*seq->vertical_size_value*seq->frame_rate;
    switch (seq->profile_and_level) {
        case 0x58 :
            /* Simple Profile @ Main Level */
            if (seq->horizontal_size_value>720)
                rvmessage("%s: SP@ML should not have a width greater than 720: %d", filename, seq->horizontal_size_value);
            if (seq->vertical_size_value>576)
                rvmessage("%s: SP@ML should not have a height greater than 576: %d", filename, seq->vertical_size_value);
            if (seq->frame_rate>30.0)
                rvmessage("%s: SP@ML should not have a frame rate greater than 30: %.2f", filename, seq->frame_rate);
            if (sample_rate>10368000)
                rvmessage("%s: SP@ML should not have a sample rate greater than 10368000: %d", filename, (int)sample_rate);
            if (seq->bit_rate>15000000)
                rvmessage("%s: SP@ML should not have a bitrate greater than 15000000: %d", filename, seq->bit_rate);
            if (seq->vbv_buffer_size*16*1024>1835008)
                rvmessage("%s: SP@ML should not have a VBV buffer size greater than 1835008: %d", filename, seq->vbv_buffer_size*16*1024);
            break;

        case 0x4a :
            /* Main Profile @ Low Level */
            if (seq->horizontal_size_value>352)
                rvmessage("%s: MP@LL should not have a width greater than 352: %d", filename, seq->horizontal_size_value);
            if (seq->vertical_size_value>288)
                rvmessage("%s: MP@LL should not have a height greater than 288: %d", filename, seq->vertical_size_value);
            if (seq->frame_rate>30.0)
                rvmessage("%s: MP@LL should not have a frame rate greater than 30: %.2f", filename, seq->frame_rate);
            if (sample_rate>3041280)
                rvmessage("%s: MP@LL should not have a sample rate greater than 3041280: %d", filename, (int)sample_rate);
            if (seq->bit_rate>4000000)
                rvmessage("%s: MP@LL should not have a bitrate greater than 4000000: %d", filename, seq->bit_rate);
            if (seq->vbv_buffer_size*16*1024>475136)
                rvmessage("%s: MP@LL should not have a VBV buffer size greater than 475136: %d", filename, seq->vbv_buffer_size*16*1024);
            break;

        case 0x48 :
            /* Main Profile @ Main Level */
            if (seq->horizontal_size_value>720)
                rvmessage("%s: MP@ML should not have a width greater than 720: %d", filename, seq->horizontal_size_value);
            if (seq->vertical_size_value>576)
                rvmessage("%s: MP@ML should not have a height greater than 576: %d", filename, seq->vertical_size_value);
            if (seq->frame_rate>30.0)
                rvmessage("%s: MP@ML should not have a frame rate greater than 30: %.2f", filename, seq->frame_rate);
            if (sample_rate>10368000)
                rvmessage("%s: MP@ML should not have a sample rate greater than 10368000: %d", filename, (int)sample_rate);
            if (seq->bit_rate>15000000)
                rvmessage("%s: MP@ML should not have a bitrate greater than 15000000: %d", filename, seq->bit_rate);
            if (seq->vbv_buffer_size*16*1024>1835008)
                rvmessage("%s: MP@ML should not have a VBV buffer size greater than 1835008: %d", filename, seq->vbv_buffer_size*16*1024);
            break;

        case 0x46 :
            /* Main Profile @ High-1440 Level */
            if (seq->horizontal_size_value>1440)
                rvmessage("%s: MP@H14 should not have a width greater than 1440: %d", filename, seq->horizontal_size_value);
            if (seq->vertical_size_value>1088)
                rvmessage("%s: MP@H14 should not have a height greater than 1088: %d", filename, seq->vertical_size_value);
            if (seq->frame_rate>60.0)
                rvmessage("%s: MP@H14 should not have a frame rate greater than 60: %.2f", filename, seq->frame_rate);
            if (sample_rate>47001600)
                rvmessage("%s: MP@H14 should not have a sample rate greater than 47001600: %d", filename, (int)sample_rate);
            if (seq->bit_rate>60000000)
                rvmessage("%s: MP@H14 should not have a bitrate greater than 60000000: %d", filename, seq->bit_rate);
            if (seq->vbv_buffer_size*16*1024>7340032)
                rvmessage("%s: MP@H14 should not have a VBV buffer size greater than 7340032: %d", filename, seq->vbv_buffer_size*16*1024);
            break;

        case 0x44 :
            /* Main Profile @ High Level */
            if (seq->horizontal_size_value>1920)
                rvmessage("%s: MP@HL should not have a width greater than 1920: %d", filename, seq->horizontal_size_value);
            if (seq->vertical_size_value>1088)
                rvmessage("%s: MP@HL should not have a height greater than 1088: %d", filename, seq->vertical_size_value);
            if (seq->frame_rate>60.0)
                rvmessage("%s: MP@HL should not have a frame rate greater than 60: %.2f", filename, seq->frame_rate);
            if (sample_rate>62668800)
                rvmessage("%s: MP@HL should not have a sample rate greater than 62668800: %d", filename, (int)sample_rate);
            if (seq->bit_rate>80000000)
                rvmessage("%s: MP@HL should not have a bitrate greater than 80000000: %d", filename, seq->bit_rate);
            if (seq->vbv_buffer_size*16*1024>9781248)
                rvmessage("%s: MP@HL should not have a VBV buffer size greater than 9781248: %d", filename, seq->vbv_buffer_size*16*1024);
            break;

        case 0x18:
            /* High profile @ Main Level */
            if (seq->horizontal_size_value>720)
                rvmessage("%s: HP@ML should not have a width greater than 720: %d", filename, seq->horizontal_size_value);
            if (seq->vertical_size_value>576)
                rvmessage("%s: HP@ML should not have a height greater than 576: %d", filename, seq->vertical_size_value);
            if (seq->frame_rate>30.0)
                rvmessage("%s: HP@ML should not have a frame rate greater than 30: %.2f", filename, seq->frame_rate);
            if (seq->chroma_format==1 && sample_rate>14745600)
                rvmessage("%s: HP@ML 4:2:0 should not have a sample rate greater than 14745600: %d", filename, (int)sample_rate);
            if (seq->chroma_format==2 && sample_rate>11059200)
                rvmessage("%s: HP@ML 4:2:2 should not have a sample rate greater than 14745600: %d", filename, (int)sample_rate);
            if (seq->bit_rate>20000000)
                rvmessage("%s: HP@ML should not have a bitrate greater than 20000000: %d", filename, seq->bit_rate);
            if (seq->vbv_buffer_size*16*1024>2441216)
                rvmessage("%s: HP@ML should not have a VBV buffer size greater than 2441216: %d", filename, seq->vbv_buffer_size*16*1024);
            break;

        case 0x16:
            /* High profile @ High-1440 Level */
            if (seq->horizontal_size_value>1440)
                rvmessage("%s: HP@H14 should not have a width greater than 1440: %d", filename, seq->horizontal_size_value);
            if (seq->vertical_size_value>1088)
                rvmessage("%s: HP@H14 should not have a height greater than 1088: %d", filename, seq->vertical_size_value);
            if (seq->frame_rate>60.0)
                rvmessage("%s: HP@H14 should not have a frame rate greater than 60: %.2f", filename, seq->frame_rate);
            if (seq->chroma_format==1 && sample_rate>62668800)
                rvmessage("%s: HP@H14 4:2:0 should not have a sample rate greater than 62668800: %d", filename, (int)sample_rate);
            if (seq->chroma_format==2 && sample_rate>47001600)
                rvmessage("%s: HP@H14 4:2:2 should not have a sample rate greater than 47001600: %d", filename, (int)sample_rate);
            if (seq->bit_rate>80000000)
                rvmessage("%s: HP@H14 should not have a bitrate greater than 80000000: %d", filename, seq->bit_rate);
            if (seq->vbv_buffer_size*16*1024>9781248)
                rvmessage("%s: HP@H14 should not have a VBV buffer size greater than 9781248: %d", filename, seq->vbv_buffer_size*16*1024);
            break;

        case 0x14:
            /* High profile @ High Level */
            if (seq->horizontal_size_value>1920)
                rvmessage("%s: HP@HL should not have a width greater than 1920: %d", filename, seq->horizontal_size_value);
            if (seq->vertical_size_value>1088)
                rvmessage("%s: HP@HL should not have a height greater than 1088: %d", filename, seq->vertical_size_value);
            if (seq->frame_rate>60.0)
                rvmessage("%s: HP@HL should not have a frame rate greater than 60: %.2f", filename, seq->frame_rate);
            if (seq->chroma_format==1 && sample_rate>83558400)
                rvmessage("%s: HP@HL 4:2:0 should not have a sample rate greater than 62668800: %d", filename, (int)sample_rate);
            if (seq->chroma_format==2 && sample_rate>62668800)
                rvmessage("%s: HP@HL 4:2:2 should not have a sample rate greater than 47001600: %d", filename, (int)sample_rate);
            if (seq->bit_rate>100000000)
                rvmessage("%s: HP@HL should not have a bitrate greater than 100000000: %d", filename, seq->bit_rate);
            if (seq->vbv_buffer_size*16*1024>12222464)
                rvmessage("%s: HP@HL should not have a VBV buffer size greater than 12222464: %d", filename, seq->vbv_buffer_size*16*1024);
            break;

        case 0x85 :
            /* 4:2:2 Profile @ Main Level */
            if (seq->horizontal_size_value>720)
                rvmessage("%s: 422@ML should not have a width greater than 720: %d", filename, seq->horizontal_size_value);
            if (seq->vertical_size_value>608)
                rvmessage("%s: 422@ML should not have a height greater than 608: %d", filename, seq->vertical_size_value);
            if (seq->frame_rate>30.0)
                rvmessage("%s: 422@ML should not have a frame rate greater than 30: %.2f", filename, seq->frame_rate);
            if (sample_rate>11059200)
                rvmessage("%s: 422@ML should not have a sample rate greater than 11059200: %d", filename, (int)sample_rate);
            if (seq->bit_rate>50000000)
                rvmessage("%s: 422@ML should not have a bitrate greater than 50000000: %d", filename, seq->bit_rate);
            if (seq->vbv_buffer_size*16*1024>9437184)
                rvmessage("%s: 422@ML should not have a VBV buffer size greater than 9437184: %d", filename, seq->vbv_buffer_size*16*1024);
            break;

        case 0x82 :
            /* 4:2:2 Profile @ High Level */
            if (seq->horizontal_size_value>1920)
                rvmessage("%s: 422@HL should not have a width greater than 1920: %d", filename, seq->horizontal_size_value);
            if (seq->vertical_size_value>1088)
                rvmessage("%s: 422@HL should not have a height greater than 1088: %d", filename, seq->vertical_size_value);
            if (seq->frame_rate>60.0)
                rvmessage("%s: 422@HL should not have a frame rate greater than 60: %.2f", filename, seq->frame_rate);
            if (sample_rate>62668800)
                rvmessage("%s: 422@HL should not have a sample rate greater than 62668800: %d", filename, (int)sample_rate);
            if (seq->bit_rate>300000000)
                rvmessage("%s: 422@HL should not have a bitrate greater than 300000000: %d", filename, seq->bit_rate);
            if (seq->vbv_buffer_size*16*1024>47185920)
                rvmessage("%s: 422@HL should not have a VBV buffer size greater than 47185920: %d", filename, seq->vbv_buffer_size*16*1024);
            break;

        default:
            rvmessage("%s: unknown profile and level: %02x", filename, seq->profile_and_level);
            break;
    }

    /* check profile constraints */
    switch (seq->profile_and_level>>4) {
        case 0x5 :
            /* Simple Profile */
            if (seq->chroma_format>1)
                rvmessage("%s: Simple Profile should not have chroma_format=%d", filename, seq->chroma_format);
            if (seq->frame_rate_extension_n)
                rvmessage("%s: Simple Profile should not have frame_rate_extension_n=%d", filename, seq->frame_rate_extension_n);
            if (seq->frame_rate_extension_d)
                rvmessage("%s: Simple Profile should not have frame_rate_extension_d=%d", filename, seq->frame_rate_extension_d);
            break;
        case 0x4 :
            /* Main Profile */
            if (seq->frame_rate_extension_n)
                rvmessage("%s: Main Profile should not have frame_rate_extension_n=%d", filename, seq->frame_rate_extension_n);
            if (seq->frame_rate_extension_d)
                rvmessage("%s: Main Profile should not have frame_rate_extension_d=%d", filename, seq->frame_rate_extension_d);
            break;
        case 0x1 :
            /* High Profile */
            break;
    }

    /* check vbv constraints */
    if (seq->vbv_buffer_size*16384 > (int)((double)seq->bit_rate*400.0 * 0xffff/90000.0))
        rvmessage("%s: vbv_buffer_size is too large for bit_rate, maximum size %d: %d", filename,
                    (int)((double)seq->bit_rate*400.0 * 0xffff/90000.0/16384.0), seq->vbv_buffer_size);

    /* get frame height and width */
    //int width  = seq->width_in_mbs*16;
    //int height = seq->height_in_mbs*16;

    /* default expectations, they are not as contradictory as would seem */
    int const_bit_rate = 1;
    int const_quant = 1;
    int vbv_vbr_mode = 0;

    /* initialise the video buffer verifier */
    double vbv_level = 0.0;
    int vbv_prev_bits = 0;
    struct rvm2vpicture *prev_picture = NULL;
    struct rvm2vpicture *prev_ref_picture = NULL;

    /* decode each picture */
    int picno, frameno;
    for (picno=0, frameno=0; ; picno++) {

        /* look for end of sequence code */
        if (showbits32(bb)==0x1B7)
            break;

        /* find next picture start code */
        int gop_flags = 0;
        if (find_m2v_next_picture(bb, seq, picno, &gop_flags, verbose)<0) {
            rvmessage("%s: sequence does not end with a sequence end code", filename);
            break;
        }

        /* vbv is measured from the last bit of the picture start code */
        int vbv_bits = numbits(bb) + 32;

        /* parse next picture */
        struct rvm2vpicture *picture = parse_m2v_picture(bb, seq, picno, verbose);
        if (picture==NULL) {
            rvmessage("failed to parse picture %d of mpeg2 sequence \"%s\"", picno, filename);
            status = STATUS_PARSE_ERROR;
            break;
        }

        /* check for parsing errors */
        if (status==0) {
            int mbx, mby;
            /* loop over macroblocks */
            for (mby=0; mby<seq->height_in_mbs>>field_picture(picture) && status==0; mby++) {
                for (mbx=0; mbx<seq->width_in_mbs && status==0; mbx++) {
                    int mb_addr = mby*seq->width_in_mbs + mbx;
                    struct rvm2vmacro *macro = picture->macro + mb_addr;
                    if (macroblock_error(macro->type))
                        status = STATUS_PARSE_ERROR;
                }
            }
        }

        /* check for inefficient coding decisions */
        if (status==0) {
            int mbx, mby;
            /* check for coding a (0,0) motion vector in P-frames */
            if (picture->picture_coding_type==PCT_P) {
                /* loop over macroblocks */
                for (mby=0; mby<seq->height_in_mbs>>field_picture(picture) && status==0; mby++) {
                    for (mbx=0; mbx<seq->width_in_mbs && status==0; mbx++) {
                        int mb_addr = mby*seq->width_in_mbs + mbx;
                        struct rvm2vmacro *macro = picture->macro + mb_addr;
                        if (macro->frame_motion_type==2 && macroblock_motion_forward(macro->type) && macro->pattern_code!=0 && macro->vector[0][0].x==0 && macro->vector[0][0].y==0)
                            rvmessage("picture %3d: macroblock %d: coded (0,0) motion vector, better to use no-mc mode", picno, mb_addr);
                    }
                }
            }
        }

        /* find motion vector range in picture */
        int maxx = 0, minx = 0, maxy = 0, miny = 0;
        if (picture->picture_coding_type!=PCT_I)
            find_m2v_motion_vector_range(picture, seq, &maxx, &minx, &maxy, &miny);

        /* measure size of picture in bits */
        int picture_size = numbits(bb)-vbv_bits;
        if (verbose>=3)
            rvmessage("picture %3d: %9d bits mv range %.1fx%.1f vbv_delay=%5d", picno, picture_size, mmax(maxx, -minx)/2.0, mmax(maxy, -miny)/2.0, picture->vbv_delay);

        /* look for the vbv max rate mode */
        if (picno==0)
            if (picture->vbv_delay==0xffff)
                vbv_vbr_mode = 1;

        /* look for an mquant to disprove constant quant TODO check for quant change in picture and slice */
        if (const_quant) {
            int mbx, mby;
            /* loop over macroblocks */
            for (mby=0; mby<seq->height_in_mbs>>field_picture(picture) && const_quant; mby++) {
                for (mbx=0; mbx<seq->width_in_mbs && const_quant; mbx++) {
                    int mb_addr = mby*seq->width_in_mbs + mbx;
                    struct rvm2vmacro *macro = picture->macro + mb_addr;
                    if (macroblock_quant(macro->type))
                        const_quant = 0;
                }
            }
        }

        /* look for an mquant change between 1 and 31 */
        if (1) {
            int mbx, mby;
            int prev_quant = 0;
            /* loop over macroblocks */
            for (mby=0; mby<seq->height_in_mbs>>field_picture(picture); mby++) {
                for (mbx=0; mbx<seq->width_in_mbs; mbx++) {
                    int mb_addr = mby*seq->width_in_mbs + mbx;
                    struct rvm2vmacro *macro = picture->macro + mb_addr;
                    int quant = macro->quantiser_scale_code;
                    if ((quant==1 && prev_quant==31) || (quant==31 && prev_quant==1))
                        rvmessage("%s: picture %d: macroblock %d: quant bounce from %d to %d", filename, picture->decode_order, mb_addr, prev_quant, quant);
                    prev_quant = quant;
                }
            }
        }

        /* check next picture */
        /* TODO low_delay and B-pictures */
        /* TODO closed_gop and B-picture after new GOP and forward prediction unless broken_link */
        if (seq->progressive_sequence && picture->extension.progressive_frame && !picture->extension.frame_pred_frame_dct)
            rvmessage("%s: picture %d: frame_pred_frame_dct should be '1' in progressive sequences", filename, picno);
        if (picture->extension.progressive_frame && picture->extension.picture_structure!=PST_FRAME)
            rvmessage("%s: picture %d: picture_structure should be \"Frame\" in progressive frames", filename, picno);
        if (vbv_vbr_mode && picture->vbv_delay!=0xffff)
            rvmessage("%s: picture %d: vbv_delay should be 0xffff in vbr mode (initial vbv_delay was 0xffff): %d", filename, picno, picture->vbv_delay);
        if (picture->vbv_delay==0xffff && !vbv_vbr_mode)
            rvmessage("%s: picture %d: vbv_delay should not be 0xffff in cbr mode (initial vbv_delay was not 0xffff)", filename, picno);
        if (picture_size > seq->vbv_buffer_size*16384)
            rvmessage("%s: picture %d: picture is larger than vbv_buffer_size of %d bits: %d", filename, picno, seq->vbv_buffer_size*16384, picture_size);

        /* check profile constraints */
        switch (seq->profile_and_level>>4) {
            case 0x5 :
                /* Simple Profile */
                if (picture->picture_coding_type==PCT_B)
                    rvmessage("%s: picture %d: Simple Profile should not have B-pictures", filename, picno);
                if (picture->extension.intra_dc_precision+8==11)
                    rvmessage("%s: picture %d: Simple Profile should not have intra_dc_precision=%d", filename, picno, picture->extension.intra_dc_precision+8);
                break;
            case 0x4 :
                /* Main Profile */
                if (picture->extension.intra_dc_precision+8==11)
                    rvmessage("%s: picture %d: Main Profile should not have intra_dc_precision=%d", filename, picno, picture->extension.intra_dc_precision+8);
                break;
            case 0x1 :
                /* High Profile */
                break;
        }

        /* check f_code constraints */
        int s, t;
        for (s=0; s<2; s++) {
            for (t=0; t<2; t++) {
                if (picture->extension.f_code[s][t]==0)
                    rvmessage("%s: picture %d: f_code[%d][%d] is forbidden: %d", filename, picno, s, t, picture->extension.f_code[s][t]);
                if (picture->extension.f_code[s][t]>=10 && picture->extension.f_code[s][t]<=14)
                    rvmessage("%s: picture %d: f_code[%d][%d] is reserved: %d", filename, picno, s, t, picture->extension.f_code[s][t]);
                if (picture->extension.f_code[s][t]!=15 && picture->picture_coding_type==PCT_I)
                    rvmessage("%s: picture %d: f_code[%d][%d] should be 15 in an I-picture: %d", filename, picno, s, t, picture->extension.f_code[s][t]);
                if (picture->extension.f_code[s][t]!=15 && picture->picture_coding_type==PCT_P && s==1)
                    rvmessage("%s: picture %d: f_code[%d][%d] should be 15 in a P-picture: %d", filename, picno, s, t, picture->extension.f_code[s][t]);
                if (picture->extension.f_code[s][t]==15 && picture->picture_coding_type==PCT_P && s==0)
                    rvmessage("%s: picture %d: f_code[%d][%d] should not be 15 in a P-picture: %d", filename, picno, s, t, picture->extension.f_code[s][t]);
                if (picture->extension.f_code[s][t]==15 && picture->picture_coding_type==PCT_B)
                    rvmessage("%s: picture %d: f_code[%d][%d] should not be 15 in a B-picture: %d", filename, picno, s, t, picture->extension.f_code[s][t]);
            }
        }

        /* check the mv range uses the full f_code range */
        if (verbose>=3) {
            if (maxx || minx || maxy || miny) {
                if (picture->picture_coding_type!=PCT_I) {
                    int f_code_limit;
                    f_code_limit = 8 << picture->extension.f_code[0][0]; /* units of half pels */
                    if (2*minx >= -f_code_limit && 2*maxx <= f_code_limit-1)
                        rvmessage("%s: picture %d: forward  motion vector x range could be represented with a smaller f_code: range=[%.1f:%.1f] f_code=%d, max=[%.1f:%.1f]", filename, picno, minx/2.0, maxx/2.0, picture->extension.f_code[0][0],
                                -1.0*(float)(4<<picture->extension.f_code[0][0]), (float)(4<<picture->extension.f_code[0][0])-0.5);
                    f_code_limit = 8 << picture->extension.f_code[0][1];
                    if (2*miny >= -f_code_limit && 2*maxy <= f_code_limit-1)
                        rvmessage("%s: picture %d: forward  motion vector y range could be represented with a smaller f_code: range=[%.1f:%.1f] f_code=%d, max=[%.1f:%.1f]", filename, picno, miny/2.0, maxy/2.0, picture->extension.f_code[0][0],
                                -1.0*(float)(4<<picture->extension.f_code[0][0]), (float)(4<<picture->extension.f_code[0][0])-0.5);
                }
                if (picture->picture_coding_type==PCT_B) {
                    int f_code_limit;
                    f_code_limit = 8 << picture->extension.f_code[1][0]; /* units of half pels */
                    if (2*minx >= -f_code_limit && 2*maxx <= f_code_limit-1)
                        rvmessage("%s: picture %d: backward motion vector x range could be represented with a smaller f_code: range=[%.1f:%.1f] f_code=%d, max=[%.1f:%.1f]", filename, picno, minx/2.0, maxx/2.0, picture->extension.f_code[1][0],
                                -1.0*(float)(4<<picture->extension.f_code[0][0]), (float)(4<<picture->extension.f_code[1][0])-0.5);
                    f_code_limit = 8 << picture->extension.f_code[1][1];
                    if (2*miny >= -f_code_limit && 2*maxy <= f_code_limit-1)
                        rvmessage("%s: picture %d: backward motion vector y range could be represented with a smaller f_code: range=[%.1f:%.1f] f_code=%d, max=[%.1f:%.1f]", filename, picno, miny/2.0, maxy/2.0, picture->extension.f_code[1][0],
                                -1.0*(float)(4<<picture->extension.f_code[0][0]), (float)(4<<picture->extension.f_code[1][0])-0.5);
                }
            }
        }

        /* check the video buffer verifier */
        if (picno>0) {
            double vbv_add = 0.0;
            int vbv_remove;

            /* determine the vbv interval */
            double vbv_interval = vbv_time_interval(seq, picture, prev_picture, prev_ref_picture);

            /* check the bitrate */
            if (!vbv_vbr_mode)
                if (vbv_check_rate(seq, picture, vbv_bits-vbv_prev_bits, prev_picture->vbv_delay, vbv_interval, filename, picno)<0)
                    const_bit_rate = 0;

            /* add data at constant bitrate to the buffer */
            if (picno>1)
                vbv_add = vbv_add_bits(seq, vbv_interval);
            vbv_level += vbv_add;
            if (vbv_vbr_mode && vbv_level>seq->vbv_buffer_size*16384.0)
                vbv_level = seq->vbv_buffer_size*16384.0;
            if (!vbv_vbr_mode && vbv_level>seq->vbv_buffer_size*16384.0)
                rvmessage("%s: picture %d: vbv buffer overflow: %.2f bits or %.2f/%d bits (%.2f/%d 16kbits)", filename, picno-1, vbv_level-seq->vbv_buffer_size*16384.0, vbv_level, seq->vbv_buffer_size*16384, vbv_level/16384.0, seq->vbv_buffer_size);

            /* remove the current picture from the vbv buffer */
            if (picno>1)
                vbv_remove = vbv_bits - vbv_prev_bits;
            else
                vbv_remove = vbv_bits;
            vbv_level -= vbv_remove;
            if (vbv_level<0.0)
                rvmessage("%s: picture %d: vbv buffer underflow: %.2f bits", filename, picno-1, vbv_level);

            if (verbose>=4)
                rvmessage("vbv: add %13.2f bits remove %9d bits level %13.2f bits %6.2f%%", vbv_add, vbv_remove, vbv_level, vbv_level*100.0/seq->vbv_buffer_size/16384.0);

        } else {
            if (vbv_vbr_mode)
                vbv_level = seq->vbv_buffer_size*16384.0;
            else
                vbv_level = vbv_init_buffer(seq, picture, vbv_bits);
            if (verbose>=1)
                rvmessage("vbv: %s mode: %d header bits, initial level %.2f bits = %.2f%%", vbv_vbr_mode? "vbr" : "cbr", vbv_bits, vbv_level, vbv_level*100.0/seq->vbv_buffer_size/16384.0);
        }

        /* check second field picture constraints */
        if (second_field_picture(picture, prev_picture) && prev_picture!=NULL) {

            if (!seq->low_delay && (picture->temporal_reference != prev_picture->temporal_reference))
                rvmessage("%s: picture %d: second field temporal_reference should match first field: %d != %d", filename, picno, prev_picture->temporal_reference, picture->temporal_reference);
            /* TODO picture coding type is same as first field, unless IP */
            /* TODO field parity is opposite first field */

        }

        if (frame_picture(picture) || first_field_picture(picture, prev_picture))
            frameno++;

        /* store previous picture data */
        vbv_prev_bits = vbv_bits; /* the headers preceeding the first picture start code and the first picture start code itself are not counted in vbv_delay */
        rvfree(prev_picture);
        prev_picture = alloc_m2v_picture();
        memcpy(prev_picture, picture, sizeof(struct rvm2vpicture));
        if (picture->picture_coding_type==PCT_I || picture->picture_coding_type==PCT_P) {
            rvfree(prev_ref_picture);
            prev_ref_picture = alloc_m2v_picture();
            memcpy(prev_ref_picture, picture, sizeof(struct rvm2vpicture));
        }

        /* tidy up */
        free_m2v_picture(picture);
    }

    /* check average bitrate */
    if (const_bit_rate && !const_quant && !vbv_vbr_mode) {
        int bitrate = (int)round((float)numbits(bb) * seq->frame_rate / (float)frameno / 400.0);
        char string1[32], string2[32];
        if (seq->bit_rate < bitrate*9/10)
            rvmessage("%s: bitrate quoted in bitstream is significantly less than actual bitrate: %s < %s", filename, describe_bitrate(string1, 32, 400*seq->bit_rate, 1, 1), describe_bitrate(string2, 32, 400*bitrate, 1, 1));
        if (seq->bit_rate > bitrate*11/10)
            rvmessage("%s: bitrate quoted in bitstream is significantly more than actual bitrate: %s > %s", filename, describe_bitrate(string1, 32, 400*seq->bit_rate, 1, 1), describe_bitrate(string2, 32, 400*bitrate, 1, 1));
    }

    if (verbose>=1)
        if (const_quant)
            rvmessage("%s: constant quant stream", filename);

    /* tidy up */
    free_m2v_sequence(seq);

    return status;
}

/* decoded picture buffer semantics */
const int MAX_DPB_SIZE = 17; /* DPB is tracked at +1 over the max possible size to catch any errors */

struct dpb_t {
    int FrameNum;
    int PicOrderCnt;
    int LongTermFrameIdx;
    ref_pic_type_t ref_pic_type;
};

static int compare_ref_pic_dec(const void *a, const void *b)
{
    const struct dpb_t *x = (struct dpb_t *)a;
    const struct dpb_t *y = (struct dpb_t *)b;
    /* note this comparison sorts into descending order */
    if (x->FrameNum == y->FrameNum)
        return 0;
    if (x->FrameNum < y->FrameNum)
        return 1;
    return -1;
}

static int compare_long_term_idx(const void *a, const void *b)
{
    const struct dpb_t *x = (struct dpb_t *)a;
    const struct dpb_t *y = (struct dpb_t *)b;
    if (x->LongTermFrameIdx == y->LongTermFrameIdx)
        return 0;
    if (x->LongTermFrameIdx < y->LongTermFrameIdx)
        return -1;
    return 1;
}

static int compare_poc_asc(const void *a, const void *b)
{
    const struct dpb_t *x = (struct dpb_t *)a;
    const struct dpb_t *y = (struct dpb_t *)b;
    if (x->PicOrderCnt == y->PicOrderCnt)
        return 0;
    if (x->PicOrderCnt < y->PicOrderCnt)
        return -1;
    return 1;
}

static int compare_poc_dec(const void *a, const void *b)
{
    const struct dpb_t *x = (struct dpb_t *)a;
    const struct dpb_t *y = (struct dpb_t *)b;
    if (x->PicOrderCnt == y->PicOrderCnt)
        return 0;
    if (x->PicOrderCnt < y->PicOrderCnt)
        return 1;
    return -1;
}

static void flush_dpb(struct dpb_t dpb[MAX_DPB_SIZE][2])
{
    /* all reference pictures are marked as "unused for reference" */
    for (int i=0; i<MAX_DPB_SIZE; i++) {
        dpb[i][0].ref_pic_type = dpb[i][1].ref_pic_type = UNUSED_FOR_REF;
        dpb[i][0].FrameNum = dpb[i][1].FrameNum = -1;
        dpb[i][0].LongTermFrameIdx = dpb[i][1].LongTermFrameIdx = -1;
    }
}

static void shift_dpb(struct dpb_t dpb[MAX_DPB_SIZE][2])
{
    /* shift dpb so there is a new frame entry at the front */
    for (int i=MAX_DPB_SIZE-1; i>0; i--)
        memcpy(&dpb[i], &dpb[i-1], sizeof(dpb_t)*2);
}

static int num_refs_in_dpb(struct dpb_t dpb[MAX_DPB_SIZE][2])
{
    int NumRefFrames = 0;
    for (int i=0; i<MAX_DPB_SIZE; i++)
        if (dpb[i][0].ref_pic_type!=UNUSED_FOR_REF || dpb[i][1].ref_pic_type!=UNUSED_FOR_REF)
            NumRefFrames++;
    return NumRefFrames;
}

static void dump_dpb(struct dpb_t dpb[MAX_DPB_SIZE][2])
{
    static const char *ref_type_name[] = { "UNUSED", "SHORT ", "LONG  " };

    for (int i=0; i<MAX_DPB_SIZE; i++) {
        if (dpb[i][0].ref_pic_type==UNUSED_FOR_REF && dpb[i][1].ref_pic_type==UNUSED_FOR_REF)
            break;
        fprintf(stderr, "[ %2d : poc=%5d fn=%2d ref=%s | poc=%5d fn=%2d ref=%s ]\n", i,
                dpb[i][0].PicOrderCnt, dpb[i][0].FrameNum, ref_type_name[dpb[i][0].ref_pic_type],
                dpb[i][1].PicOrderCnt, dpb[i][1].FrameNum, ref_type_name[dpb[i][1].ref_pic_type]);
    }
}

static void dump_ref_pic_list(struct dpb_t dpb[], int num_ref_idx_active)
{
    static const char *ref_type_name[] = { "UNUSED", "SHORT ", "LONG  " };

    for (int i=0; i<num_ref_idx_active; i++) {
        if (dpb[i].ref_pic_type==UNUSED_FOR_REF)
            break;
        fprintf(stderr, "%2d : poc=%5d picnum=%2d ref=%s \n", i,
                dpb[i].PicOrderCnt, dpb[i].FrameNum, ref_type_name[dpb[i].ref_pic_type]);
    }
    fprintf(stderr, "----------------------------------\n");
}

static void describe_picture_ref_params(const struct rv264picture *picture, const struct slice_header *sh, int fid, int SecondComplementaryField)
{
    char s[256];
    int len = 0;
    len += snprintf(s+len, sizeof(s)-len, "poc=%3d frame_num=%2d :", picture->PicOrderCnt, picture->frame_num);
    if (sh->field_pic_flag)
        len += snprintf(s+len, sizeof(s)-len, " fid=%d", fid);
    if (picture->nal_unit_type==NAL_CODED_SLICE_IDR)
        len += snprintf(s+len, sizeof(s)-len, " IDR-slice");
    else if (slice_is_p(sh->slice_type))
        len += snprintf(s+len, sizeof(s)-len, " P-slice: num_ref_idx_active=%d", sh->num_ref_idx_l0_active_minus1+1);
    else if (slice_is_b(sh->slice_type))
        len += snprintf(s+len, sizeof(s)-len, " B-slice: num_ref_idx_active=%d,%d", sh->num_ref_idx_l0_active_minus1+1, sh->num_ref_idx_l1_active_minus1+1);
    else
        len += snprintf(s+len, sizeof(s)-len, " I-slice");
    if (picture->nal_ref_idc==0)
        len += snprintf(s+len, sizeof(s)-len, " (non-ref)");
    if (sh->dec_ref_pic_marking.long_term_reference_flag)
        len += snprintf(s+len, sizeof(s)-len, " (long-ref)");
    if (SecondComplementaryField)
        len += snprintf(s+len, sizeof(s)-len, " (comp-pair)");
    if (sh->redundant_pic_cnt>0)
        len += snprintf(s+len, sizeof(s)-len, " (redundant)");
    rvmessage("%s", s);
}

int check_264(struct bitbuf *bb, const char *filename, int noexit, int verbose)
{
    int status = 0;

    /*
    * NOTE syntax is generally checked within the 264 functions
    * whereas constraints are checked at this level.
    */

    /* parse sps and pps */
    struct rv264sequence *seq = parse_264_params(bb, verbose-2);
    if (seq==NULL) {
        rvmessage("failed to parse sps and pps in h.264 sequence \"%s\"", filename);
        status = STATUS_PARSE_ERROR;
        return status;
    }

    /* select active sps */
    struct rv264seq_parameter_set *sps = seq->sps[seq->active_sps];

    if (!sps) {
        rvmessage("%s: sps_id=%d not found in file, no checking performed", filename, seq->active_sps);
        return status;
    }

    /* check sps semantics */
    if (sps->frame_mbs_only_flag==0)
        if (sps->direct_8x8_inference_flag!=1)
            rvmessage("%s: when frame_mbs_only_flag is 0, direct_8x8_inference_flag should be 1", filename);

    /* warn about missing VUI parameters */
    if (verbose>=2) {
        if (!sps->vui_parameters_present_flag || !sps->vui.timing_info_present_flag)
            rvmessage("%s: warning: vui timing parameters are missing, no frame rate is encoded in the bitstream", filename);
        if (!sps->vui.nal_hrd_parameters_present_flag && !sps->vui.vcl_hrd_parameters_present_flag)
            rvmessage("%s: warning: hrd parameters are missing, hrd conformance of the bitstream cannot be verified", filename);
    }

    /* select active pps */
    struct rv264pic_parameter_set *pps = seq->pps[seq->active_pps];

    if (pps->seq_parameter_set_id != seq->active_sps)
        rvexit("%s: pps %d does not refer to sps %d, time to fix rvcheck: %d", filename, seq->active_pps, seq->active_sps, pps->seq_parameter_set_id);

    /* check profile constraints */
    switch (sps->profile_idc) {
        case 66:  /* Baseline profile */
            if (sps->frame_mbs_only_flag!=1)
                rvmessage("%s: Baseline profile should not have frame_mbs_only_flag=%d", filename, sps->frame_mbs_only_flag);
            if (pps->weighted_pred_flag!=0)
                rvmessage("%s: Baseline profile should not have weighted_pred_flag=%d", filename, pps->weighted_pred_flag);
            if (pps->weighted_bipred_idc!=0)
                rvmessage("%s: Baseline profile should not have weighted_bipred_idc=%d", filename, pps->weighted_bipred_idc);
            if (pps->entropy_coding_mode_flag!=0)
                rvmessage("%s: Baseline profile should not have entropy_coding_mode_flag=%d", filename, pps->entropy_coding_mode_flag);
            break;

        case 77: /* Main profile */
            if (pps->num_slice_groups_minus1!=0)
                rvmessage("%s: Main profile should not have num_slice_groups_minus1=%d", filename, pps->num_slice_groups_minus1);
            if (pps->redundant_pic_cnt_present_flag!=0)
                rvmessage("%s: Main profile should not have redundant_pic_cnt_present_flag=%d", filename, pps->redundant_pic_cnt_present_flag);
            break;

        case 88: /* Extended profile */
            if (sps->direct_8x8_inference_flag!=1)
                rvmessage("%s: Extended profile should not have direct_8x8_inference_flag=%d", filename, sps->direct_8x8_inference_flag);
            if (pps->entropy_coding_mode_flag!=0)
                rvmessage("%s: Extended profile should not have entropy_coding_mode_flag=%d", filename, pps->entropy_coding_mode_flag);
            break;

        case 100: /* High profile */
            if (pps->num_slice_groups_minus1!=0)
                rvmessage("%s: High profile should not have num_slice_groups_minus1=%d", filename, pps->num_slice_groups_minus1);
            if (pps->redundant_pic_cnt_present_flag!=0)
                rvmessage("%s: High profile should not have redundant_pic_cnt_present_flag=%d", filename, pps->redundant_pic_cnt_present_flag);
            if (sps->chroma_format_idc>1)
                rvmessage("%s: High profile should not have chroma_format_idc=%d", filename, sps->chroma_format_idc);
            if (sps->bit_depth_luma_minus8!=0)
                rvmessage("%s: High profile should not have bit_depth_luma_minus8=%d", filename, sps->bit_depth_luma_minus8);
            if (sps->bit_depth_chroma_minus8!=0)
                rvmessage("%s: High profile should not have bit_depth_chroma_minus8=%d", filename, sps->bit_depth_chroma_minus8);
            if (sps->qpprime_y_zero_transform_bypass_flag!=0)
                rvmessage("%s: High profile should not have qpprime_y_zero_transform_bypass_flag=%d", filename, sps->qpprime_y_zero_transform_bypass_flag);
            break;

        case 110: /* High-10 profile */
            if (pps->num_slice_groups_minus1!=0)
                rvmessage("%s: High profile should not have num_slice_groups_minus1=%d", filename, pps->num_slice_groups_minus1);
            if (pps->redundant_pic_cnt_present_flag!=0)
                rvmessage("%s: High profile should not have redundant_pic_cnt_present_flag=%d", filename, pps->redundant_pic_cnt_present_flag);
            if (sps->chroma_format_idc>1)
                rvmessage("%s: High profile should not have chroma_format_idc=%d", filename, sps->chroma_format_idc);
            if (sps->bit_depth_luma_minus8>2)
                rvmessage("%s: High profile should not have bit_depth_luma_minus8=%d", filename, sps->bit_depth_luma_minus8);
            if (sps->bit_depth_chroma_minus8>2)
                rvmessage("%s: High profile should not have bit_depth_chroma_minus8=%d", filename, sps->bit_depth_chroma_minus8);
            if (sps->qpprime_y_zero_transform_bypass_flag!=0)
                rvmessage("%s: High profile should not have qpprime_y_zero_transform_bypass_flag=%d", filename, sps->qpprime_y_zero_transform_bypass_flag);
            break;

        case 122: /* High 4:2:2 profile */
            if (pps->num_slice_groups_minus1!=0)
                rvmessage("%s: High profile should not have num_slice_groups_minus1=%d", filename, pps->num_slice_groups_minus1);
            if (pps->redundant_pic_cnt_present_flag!=0)
                rvmessage("%s: High profile should not have redundant_pic_cnt_present_flag=%d", filename, pps->redundant_pic_cnt_present_flag);
            if (sps->chroma_format_idc>2)
                rvmessage("%s: High profile should not have chroma_format_idc=%d", filename, sps->chroma_format_idc);
            if (sps->bit_depth_luma_minus8>2)
                rvmessage("%s: High profile should not have bit_depth_luma_minus8=%d", filename, sps->bit_depth_luma_minus8);
            if (sps->bit_depth_chroma_minus8>2)
                rvmessage("%s: High profile should not have bit_depth_chroma_minus8=%d", filename, sps->bit_depth_chroma_minus8);
            if (sps->qpprime_y_zero_transform_bypass_flag!=0)
                rvmessage("%s: High profile should not have qpprime_y_zero_transform_bypass_flag=%d", filename, sps->qpprime_y_zero_transform_bypass_flag);
            break;

        case 244: /* High 4:4:4 Predictive profile */
            if (pps->num_slice_groups_minus1!=0)
                rvmessage("%s: High profile should not have num_slice_groups_minus1=%d", filename, pps->num_slice_groups_minus1);
            if (pps->redundant_pic_cnt_present_flag!=0)
                rvmessage("%s: High profile should not have redundant_pic_cnt_present_flag=%d", filename, pps->redundant_pic_cnt_present_flag);
            if (sps->bit_depth_luma_minus8>6)
                rvmessage("%s: High profile should not have bit_depth_luma_minus8=%d", filename, sps->bit_depth_luma_minus8);
            if (sps->bit_depth_chroma_minus8>6)
                rvmessage("%s: High profile should not have bit_depth_chroma_minus8=%d", filename, sps->bit_depth_chroma_minus8);
            break;

        case 44:
            break;

        default:
            rvmessage("%s: invalid value of profile_idc: %d", filename, sps->profile_idc);
    }

    /* check level constraints */
    int MaxFS = 0;
    int direct_8x8_inference_flag = 0;
    int frame_mbs_only_flag = 0;
    switch (sps->level_idc) {
        case 10:
            MaxFS = 99;
            frame_mbs_only_flag = 1;
            break;

        case 11:
        case 12:
        case 13:
        case 20:
            MaxFS = 396;
            frame_mbs_only_flag = 1;
            break;

        case 21:
            MaxFS = 792;
            break;

        case 22:
            MaxFS = 1620;
            break;

        case 30:
            MaxFS = 1620;
            direct_8x8_inference_flag = 1;
            break;

        case 31:
            MaxFS = 3600;
            direct_8x8_inference_flag = 1;
            break;

        case 32:
            MaxFS = 5120;
            direct_8x8_inference_flag = 1;
            break;

        case 40:
        case 41:
            MaxFS = 8192;
            direct_8x8_inference_flag = 1;
            break;

        case 42:
            MaxFS = 8704;
            direct_8x8_inference_flag = 1;
            frame_mbs_only_flag = 1;
            break;

        case 50:
            MaxFS = 22080;
            direct_8x8_inference_flag = 1;
            frame_mbs_only_flag = 1;
            break;

        case 51:
        case 52:
            MaxFS = 36864;
            direct_8x8_inference_flag = 1;
            frame_mbs_only_flag = 1;
            break;

        default:
            rvmessage("%s: invalid value of level_idc: %d", filename, sps->level_idc);
    }
    int FrameSizeInMbs = sps->PicWidthInMbs*sps->FrameHeightInMbs;
    if (FrameSizeInMbs>MaxFS)
        rvmessage("%s: Level %.1f should not have %d MBs > %d", filename, sps->level_idc/10.0, FrameSizeInMbs, MaxFS);
    if (sps->PicWidthInMbs>sqrt(MaxFS*8))
        rvmessage("%s: Level %.1f should not have %d MBs width > %.2f", filename, sps->level_idc/10.0, sps->PicWidthInMbs, sqrt(MaxFS*8));
    if (sps->FrameHeightInMbs>sqrt(MaxFS*8))
        rvmessage("%s: Level %.1f should not have %d MBs height > %.2f", filename, sps->level_idc/10.0, sps->FrameHeightInMbs, sqrt(MaxFS*8));
    if (sps->direct_8x8_inference_flag==0 && direct_8x8_inference_flag==1)
        rvmessage("%s: Level %.1f prohibits direct_8x8_inference_flag==0", filename, sps->level_idc/10.0);
    if (sps->frame_mbs_only_flag==0 && frame_mbs_only_flag==1)
        rvmessage("%s: Level %.1f prohibits frame_mbs_only_flag==0", filename, sps->level_idc/10.0);

    /* initialise decoded picture buffer */
    const int MaxNumRefFrames = mmax(sps->max_num_ref_frames, 1);
    struct dpb_t dpb[MAX_DPB_SIZE][2] = { { { -1, 0, UNUSED_FOR_REF}, { -1, 0, UNUSED_FOR_REF} } };
    int MaxLongTermFrameIdx = -1;

    if (verbose>=1) {
        rvmessage("sps: max_num_ref_frames=%d gaps_in_frame_num_value_allowed_flag=%d", sps->max_num_ref_frames, sps->gaps_in_frame_num_value_allowed_flag);
        rvmessage("pps: num_ref_idx_l0_active_minus1=%d num_ref_idx_l1_active_minus1=%d", pps->num_ref_idx_l0_active_minus1, pps->num_ref_idx_l1_active_minus1);
    }

    /* parse each picture */
    int picno, numframes=-1, toterrors=0, totwarnings=0;
    int PrevRefFrameNum = 0, PrevPicOrderCnt = 0, PrevPicNo = 0, PrevRefFieldParity = -1;
    int PrevNalRefIdc = 0, PrevFrameNum = 0, PrevFieldParity = -1, PrevSecondComplementaryField = 0;
    for (picno=0; picno<numframes || numframes<0; picno++) {
        int numerrors = 0;

        /* parse next picture */
        struct rv264picture *picture = parse_264_picture_headers(bb, seq, verbose-2);
        if (picture==NULL)
            /* end of file */
            break;

        /* reselect SPS and PPS in case they have been updated */
        sps = seq->sps[seq->active_sps];
        pps = seq->pps[seq->active_pps];

        // TODO multiple slices
        struct slice_header *sh = picture->sh[0];

        /* check slice type - this is non-normative, so actually shouldn't be checked */
        if (picture->aud_present && totwarnings<10) {
            switch (sh->slice_type) {
                case 0: if (picture->primary_pic_type==0) { rvmessage("warning: shouldn't have p-slice when primary_pic_type=0"); totwarnings++; }
                case 1: if (picture->primary_pic_type==0 || picture->primary_pic_type==1) { rvmessage("warning: shouldn't have b-slice when primary_pic_type=%d", picture->primary_pic_type); totwarnings++; }
                default: /* ignore SI/SP slices for now to simplify this matrix */ break;
            }
        }

        /* field or frame picture */
        int fid = 0;
        if (sh->field_pic_flag && sh->bottom_field_flag)
            fid = 1;

        /* is picture the second in a complementary field */
        int SecondComplementaryField = 0;
        if (sh->field_pic_flag && sh->nal_unit_type!=NAL_CODED_SLICE_IDR /* && memory_management_control_operation != 5*/) {
            if (sh->bottom_field_flag != PrevFieldParity && sh->frame_num == PrevFrameNum && !PrevSecondComplementaryField)
                if ( (!sh->nal_ref_idc) == (!PrevNalRefIdc) )
                    SecondComplementaryField = 1;
        }

        /* display picture reference params */
        if (verbose>=2) {
            describe_picture_ref_params(picture, sh, fid, SecondComplementaryField);
        }

        /* prepare variables */
        //int FrameNumWrap = sh->frame_num; /* what is this madness in Equation 8-27? */

        /* update PrevRefFrameNum */
        if (picture->nal_unit_type==NAL_CODED_SLICE_IDR) {
            PrevRefFrameNum = 0;
        }

        /* 7.4.3 sanity check frame_num */
        if (picture->nal_unit_type==NAL_CODED_SLICE_IDR) {
            if (sh->frame_num!=0) {
                rvmessage("%s: picture %d: frame_num must be zero in idr pictures: %d", filename, picno, sh->frame_num);
                numerrors++;
            }
        } else {
            if (sh->frame_num==PrevRefFrameNum) {
                /* a) */
                if (picno!=PrevPicNo+1) {
                    rvmessage("%s: picture %d: frame_num can only repeat in consecutive access units: %d", filename, picno, sh->frame_num);
                    numerrors++;
                }
                /* b) */
                if (sh->field_pic_flag && sh->bottom_field_flag != PrevRefFieldParity && sh->nal_ref_idc!=0) {
                    /* ok */
                } else {
                    if (!sh->field_pic_flag && sh->nal_ref_idc!=0) {
                        rvmessage("%s: picture %d: frame_num must not repeat in reference frame pictures: %d", filename, picno, sh->frame_num);
                        numerrors++;
                    }
                    else if (sh->bottom_field_flag == PrevRefFieldParity && sh->nal_ref_idc!=0) {
                        rvmessage("%s: picture %d: %s-field: frame_num must not repeat in reference same parity field pictures: %d", filename, picno, sh->bottom_field_flag? "bot" : "top", sh->frame_num);
                        numerrors++;
                    }
                    else if (sh->field_pic_flag && sh->nal_ref_idc==0) {
                        rvmessage("%s: picture %d: %s-field: frame_num must not repeat previous reference frame_num in non-reference field pictures: %d", filename, picno, sh->bottom_field_flag? "bot" : "top", sh->frame_num);
                        numerrors++;
                    }
                    else if (sh->nal_ref_idc != PrevNalRefIdc) {
                        rvmessage("%s: picture %d: %s-field: frame_num must not repeat in non-reference opposite parity field pictures: %d", filename, picno, sh->bottom_field_flag? "bot" : "top", sh->frame_num);
                        numerrors++;
                    }
                    else {
                        /* some other reason not noticed yet */
                        rvmessage("%s: picture %d: frame_num must not repeat: %d", filename, picno, sh->frame_num);
                    }
                }
                /* c) I despair at checking the rest of 7.4.3 frame_num restrictions */
            } else {
                int UnusedShortTermFrameNum = (PrevRefFrameNum+1) % sps->MaxFrameNum;
                while (UnusedShortTermFrameNum != sh->frame_num) {
                    for (int i=0; i<MAX_DPB_SIZE; i++) {
                        if ((dpb[i][0].FrameNum == UnusedShortTermFrameNum && dpb[i][0].ref_pic_type!=UNUSED_FOR_REF) ||
                            (dpb[i][1].FrameNum == UnusedShortTermFrameNum && dpb[i][1].ref_pic_type!=UNUSED_FOR_REF)) {
                                rvmessage("%s: picture %d: frame_num already found in dpb: %d", filename, picno, sh->frame_num);
                                dump_dpb(dpb);
                                numerrors++;
                        }
                    }
                    UnusedShortTermFrameNum = (UnusedShortTermFrameNum+1) % sps->MaxFrameNum;
                }

                if (sps->gaps_in_frame_num_value_allowed_flag==0) {
                    if (sh->frame_num != ((PrevRefFrameNum+1) % sps->MaxFrameNum)) {
                        rvmessage("%s: picture %d: frame_num must monotonically increment: %d -> %d", filename, picno, PrevRefFrameNum, sh->frame_num);
                        numerrors++;
                    }
                } else {
                    /* this should be fun to implement */
                }
            }
        }

        /* 8.2 sanity check picture order count */
        if (picture->nal_unit_type==NAL_CODED_SLICE_IDR) {
            if (sh->PicOrderCnt!=0) {
                rvmessage("%s: picture %d: poc must be zero in idr pictures: %d", filename, picno, sh->PicOrderCnt);
                numerrors++;
            }
        } else {
            /* TODO implement the listD, listO semantics, for now some simple checks instead */
            if (SecondComplementaryField && abs(sh->PicOrderCnt-PrevPicOrderCnt)>1) {
                rvmessage("%s: picture %d: poc must be consecutive in complementary field pairs: %d->%d", filename, picno, PrevPicOrderCnt, sh->PicOrderCnt);
                //numerrors++;
            }
        }

        /* 8.2.4.2 initialisation process for reference picture lists */
        if (slice_is_p(sh->slice_type) || slice_is_b(sh->slice_type)) {
            dpb_t RefPicList0[NB_OF_REF_PICS] = {{0}};
            dpb_t RefPicList1[NB_OF_REF_PICS] = {{0}};
            int ref_idx_l0 = 0, ref_idx_l1 = 0;

            const bool dump_lists = false;

            /* When this process is invoked, there shall be at least one reference frame or complementary reference field pair that is
             * currently marked as "used for reference" (i.e., as "used for short-term reference" or "used for long-term reference") and
             * is not marked as "non-existing". */
            int NumRefFrames = num_refs_in_dpb(dpb);
            if (NumRefFrames==0) {
                rvmessage("%s: picture %d: there are no reference frames in the dpb", filename, picno);
                dump_dpb(dpb);
            }

            if (slice_is_p(sh->slice_type) && !sh->field_pic_flag) {
                /* 8.2.4.2.1 Initialisation process for the reference picture list for P and SP slices in frames */
                for (int i=0; i<MAX_DPB_SIZE; i++) {
                    if (dpb[i][0].ref_pic_type==SHORT_TERM_REF && dpb[i][1].ref_pic_type==SHORT_TERM_REF) {
                        int FrameNumWrap = dpb[i][0].FrameNum > sh->frame_num? dpb[i][0].FrameNum-sps->MaxFrameNum : dpb[i][0].FrameNum;
                        int PicNum = FrameNumWrap;

                        RefPicList0[ref_idx_l0].ref_pic_type = SHORT_TERM_REF;
                        RefPicList0[ref_idx_l0].FrameNum = PicNum;
                        RefPicList0[ref_idx_l0].PicOrderCnt = dpb[i][0].PicOrderCnt;
                        ref_idx_l0++;
                    }
                }
                if (ref_idx_l0>1)
                    qsort(RefPicList0, ref_idx_l0, sizeof(dpb_t), compare_ref_pic_dec);
                int numShortTerm = ref_idx_l0;
                for (int i=0; i<MAX_DPB_SIZE; i++) {
                    if (dpb[i][0].ref_pic_type==LONG_TERM_REF && dpb[i][1].ref_pic_type==LONG_TERM_REF) {
                        int LongTermPicNum = dpb[i][0].LongTermFrameIdx;

                        RefPicList0[ref_idx_l0].ref_pic_type = LONG_TERM_REF;
                        RefPicList0[ref_idx_l0].LongTermFrameIdx = LongTermPicNum;
                        RefPicList0[ref_idx_l0].PicOrderCnt = dpb[i][0].PicOrderCnt;
                        ref_idx_l0++;
                    }
                }
                if (ref_idx_l0-numShortTerm>1)
                    qsort(RefPicList0+numShortTerm, ref_idx_l0-numShortTerm, sizeof(dpb_t), compare_long_term_idx);
                if (dump_lists)
                    dump_ref_pic_list(RefPicList0, ref_idx_l0);
            }
            else if (slice_is_p(sh->slice_type) && sh->field_pic_flag) {
                /* 8.2.4.2.2 Initialisation process for the reference picture list for P and SP slices in fields FIXME field parity ordering needs fixed */
                for (int i=0; i<MAX_DPB_SIZE; i++) {
                    for (int j=0; j<2; j++) {
                        int p = fid ^ j;
                        if (dpb[i][p].ref_pic_type==SHORT_TERM_REF) {
                            int FrameNumWrap = dpb[i][p].FrameNum > sh->frame_num? dpb[i][p].FrameNum-sps->MaxFrameNum : dpb[i][p].FrameNum;
                            int PicNum = 2*FrameNumWrap + (j==fid);

                            RefPicList0[ref_idx_l0].ref_pic_type = SHORT_TERM_REF;
                            RefPicList0[ref_idx_l0].FrameNum = PicNum;
                            RefPicList0[ref_idx_l0].PicOrderCnt = dpb[i][p].PicOrderCnt;
                            ref_idx_l0++;
                        }
                    }
                }
                if (ref_idx_l0>1)
                    qsort(RefPicList0, ref_idx_l0, sizeof(dpb_t), compare_ref_pic_dec);
                int numShortTerm = ref_idx_l0;
                for (int i=0; i<MAX_DPB_SIZE; i++) {
                    for (int j=0; j<2; j++) {
                        int p = fid ^ j;
                        if (dpb[i][p].ref_pic_type==LONG_TERM_REF) {
                            int LongTermPicNum = 2*dpb[i][p].LongTermFrameIdx + (j==fid);

                            RefPicList0[ref_idx_l0].ref_pic_type = LONG_TERM_REF;
                            RefPicList0[ref_idx_l0].LongTermFrameIdx = LongTermPicNum;
                            RefPicList0[ref_idx_l0].PicOrderCnt = dpb[i][p].PicOrderCnt;
                            ref_idx_l0++;
                        }
                    }
                }
                if (ref_idx_l0-numShortTerm>1)
                    qsort(RefPicList0+numShortTerm, ref_idx_l0-numShortTerm, sizeof(dpb_t), compare_long_term_idx);
                if (dump_lists)
                    dump_ref_pic_list(RefPicList0, ref_idx_l0);
            }
            else if (slice_is_b(sh->slice_type) && !sh->field_pic_flag) {
                /* 8.2.4.2.3 Initialisation process for reference picture lists for B slices in frames */
                for (int i=0; i<MAX_DPB_SIZE; i++) {
                    if (dpb[i][0].ref_pic_type==SHORT_TERM_REF && dpb[i][1].ref_pic_type==SHORT_TERM_REF) {
                        if (dpb[i][0].PicOrderCnt < sh->PicOrderCnt) {

                            RefPicList0[ref_idx_l0].ref_pic_type = SHORT_TERM_REF;
                            RefPicList0[ref_idx_l0].FrameNum = dpb[i][0].FrameNum;
                            RefPicList0[ref_idx_l0].PicOrderCnt = dpb[i][0].PicOrderCnt;
                            ref_idx_l0++;
                        }
                    }
                }
                if (ref_idx_l0>1)
                    qsort(RefPicList0, ref_idx_l0, sizeof(dpb_t), compare_poc_dec);
                int numShortTerm = ref_idx_l0;
                for (int i=0; i<MAX_DPB_SIZE; i++) {
                    if (dpb[i][0].ref_pic_type==SHORT_TERM_REF && dpb[i][1].ref_pic_type==SHORT_TERM_REF) {
                        if (dpb[i][0].PicOrderCnt >= sh->PicOrderCnt) {

                            RefPicList0[ref_idx_l0].ref_pic_type = SHORT_TERM_REF;
                            RefPicList0[ref_idx_l0].FrameNum = dpb[i][0].FrameNum;
                            RefPicList0[ref_idx_l0].PicOrderCnt = dpb[i][0].PicOrderCnt;
                            ref_idx_l0++;
                        }
                    }
                }
                if (ref_idx_l0-numShortTerm>1)
                    qsort(RefPicList0+numShortTerm, ref_idx_l0-numShortTerm, sizeof(dpb_t), compare_poc_asc);
                numShortTerm = ref_idx_l0;
                for (int i=0; i<MAX_DPB_SIZE; i++) {
                    if (dpb[i][0].ref_pic_type==LONG_TERM_REF && dpb[i][1].ref_pic_type==LONG_TERM_REF) {
                        int LongTermPicNum = dpb[i][0].LongTermFrameIdx;

                        RefPicList0[ref_idx_l0].ref_pic_type = LONG_TERM_REF;
                        RefPicList0[ref_idx_l0].LongTermFrameIdx = LongTermPicNum;
                        RefPicList0[ref_idx_l0].PicOrderCnt = dpb[i][0].PicOrderCnt;
                        ref_idx_l0++;
                    }
                }
                if (ref_idx_l0-numShortTerm>1)
                    qsort(RefPicList0+numShortTerm, ref_idx_l0-numShortTerm, sizeof(dpb_t), compare_long_term_idx);
                /* repeat for reference picture list 1 */
                for (int i=0; i<MAX_DPB_SIZE; i++) {
                    if (dpb[i][0].ref_pic_type==SHORT_TERM_REF && dpb[i][1].ref_pic_type==SHORT_TERM_REF) {
                        if (dpb[i][0].PicOrderCnt > sh->PicOrderCnt) {

                            RefPicList1[ref_idx_l1].ref_pic_type = SHORT_TERM_REF;
                            RefPicList1[ref_idx_l1].FrameNum = dpb[i][0].FrameNum;
                            RefPicList1[ref_idx_l1].PicOrderCnt = dpb[i][0].PicOrderCnt;
                            ref_idx_l1++;
                        }
                    }
                }
                if (ref_idx_l1>1)
                    qsort(RefPicList1, ref_idx_l1, sizeof(dpb_t), compare_poc_asc);
                numShortTerm = ref_idx_l1;
                for (int i=0; i<MAX_DPB_SIZE; i++) {
                    if (dpb[i][0].ref_pic_type==SHORT_TERM_REF && dpb[i][1].ref_pic_type==SHORT_TERM_REF) {
                        if (dpb[i][0].PicOrderCnt <= sh->PicOrderCnt) {

                            RefPicList1[ref_idx_l1].ref_pic_type = SHORT_TERM_REF;
                            RefPicList1[ref_idx_l1].FrameNum = dpb[i][0].FrameNum;
                            RefPicList1[ref_idx_l1].PicOrderCnt = dpb[i][0].PicOrderCnt;
                            ref_idx_l1++;
                        }
                    }
                }
                if (ref_idx_l1>1)
                    qsort(RefPicList1+numShortTerm, ref_idx_l1-numShortTerm, sizeof(dpb_t), compare_poc_asc);
                numShortTerm = ref_idx_l1;
                for (int i=0; i<MAX_DPB_SIZE; i++) {
                    if (dpb[i][0].ref_pic_type==LONG_TERM_REF && dpb[i][1].ref_pic_type==LONG_TERM_REF) {
                        int LongTermPicNum = dpb[i][0].LongTermFrameIdx;

                        RefPicList1[ref_idx_l1].ref_pic_type = LONG_TERM_REF;
                        RefPicList1[ref_idx_l1].LongTermFrameIdx = LongTermPicNum;
                        RefPicList1[ref_idx_l1].PicOrderCnt = dpb[i][0].PicOrderCnt;
                        ref_idx_l1++;
                    }
                }
                if (ref_idx_l1-numShortTerm>1)
                    qsort(RefPicList1+numShortTerm, ref_idx_l1-numShortTerm, sizeof(dpb_t), compare_long_term_idx);
                if (dump_lists) {
                    dump_ref_pic_list(RefPicList0, ref_idx_l0);
                    dump_ref_pic_list(RefPicList1, ref_idx_l1);
                }
            }
            else if (slice_is_b(sh->slice_type) && sh->field_pic_flag) {
                /* 8.2.4.2.3 Initialisation process for reference picture lists for B slices in fields FIXME field parity ordering needs fixed */
                for (int i=0; i<MAX_DPB_SIZE; i++) {
                    for (int j=0; j<2; j++) {
                        if (dpb[i][j].ref_pic_type==SHORT_TERM_REF) {
                            if (dpb[i][j].PicOrderCnt <= sh->PicOrderCnt) {

                                RefPicList0[ref_idx_l0].ref_pic_type = SHORT_TERM_REF;
                                RefPicList0[ref_idx_l0].FrameNum = dpb[i][j].FrameNum;
                                RefPicList0[ref_idx_l0].PicOrderCnt = dpb[i][j].PicOrderCnt;
                                ref_idx_l0++;
                            }
                        }
                    }
                }
                if (ref_idx_l0>1)
                    qsort(RefPicList0, ref_idx_l0, sizeof(dpb_t), compare_poc_dec);
                int numShortTerm = ref_idx_l0;
                for (int i=0; i<MAX_DPB_SIZE; i++) {
                    for (int j=0; j<2; j++) {
                        if (dpb[i][j].ref_pic_type==SHORT_TERM_REF) {
                            if (dpb[i][j].PicOrderCnt > sh->PicOrderCnt) {

                                RefPicList0[ref_idx_l0].ref_pic_type = SHORT_TERM_REF;
                                RefPicList0[ref_idx_l0].FrameNum = dpb[i][j].FrameNum;
                                RefPicList0[ref_idx_l0].PicOrderCnt = dpb[i][j].PicOrderCnt;
                                ref_idx_l0++;
                            }
                        }
                    }
                }
                if (ref_idx_l0-numShortTerm>1)
                    qsort(RefPicList0+numShortTerm, ref_idx_l0-numShortTerm, sizeof(dpb_t), compare_poc_asc);
                numShortTerm = ref_idx_l0;
                for (int i=0; i<MAX_DPB_SIZE; i++) {
                    for (int j=0; j<2; j++) {
                        if (dpb[i][j].ref_pic_type==LONG_TERM_REF) {
                            int LongTermPicNum = 2*dpb[i][j].LongTermFrameIdx + (j==fid);

                            RefPicList0[ref_idx_l0].ref_pic_type = LONG_TERM_REF;
                            RefPicList0[ref_idx_l0].LongTermFrameIdx = LongTermPicNum;
                            RefPicList0[ref_idx_l0].PicOrderCnt = dpb[i][j].PicOrderCnt;
                            ref_idx_l0++;
                        }
                    }
                }
                if (ref_idx_l0-numShortTerm>1)
                    qsort(RefPicList0+numShortTerm, ref_idx_l0-numShortTerm, sizeof(dpb_t), compare_long_term_idx);
                /* repeat for reference picture list 1 */
                for (int i=0; i<MAX_DPB_SIZE; i++) {
                    for (int j=0; j<2; j++) {
                        if (dpb[i][j].ref_pic_type==SHORT_TERM_REF) {
                            if (dpb[i][j].PicOrderCnt > sh->PicOrderCnt) {

                                RefPicList1[ref_idx_l1].ref_pic_type = SHORT_TERM_REF;
                                RefPicList1[ref_idx_l1].FrameNum = dpb[i][j].FrameNum;
                                RefPicList1[ref_idx_l1].PicOrderCnt = dpb[i][j].PicOrderCnt;
                                ref_idx_l1++;
                            }
                        }
                    }
                }
                if (ref_idx_l1>1)
                    qsort(RefPicList1, ref_idx_l1, sizeof(dpb_t), compare_poc_asc);
                numShortTerm = ref_idx_l1;
                for (int i=0; i<MAX_DPB_SIZE; i++) {
                    for (int j=0; j<2; j++) {
                        if (dpb[i][j].ref_pic_type==SHORT_TERM_REF) {
                            if (dpb[i][j].PicOrderCnt <= sh->PicOrderCnt) {

                                RefPicList1[ref_idx_l1].ref_pic_type = SHORT_TERM_REF;
                                RefPicList1[ref_idx_l1].FrameNum = dpb[i][j].FrameNum;
                                RefPicList1[ref_idx_l1].PicOrderCnt = dpb[i][j].PicOrderCnt;
                                ref_idx_l1++;
                            }
                        }
                    }
                }
                if (ref_idx_l1>1)
                    qsort(RefPicList1+numShortTerm, ref_idx_l1-numShortTerm, sizeof(dpb_t), compare_poc_dec);
                numShortTerm = ref_idx_l1;
                for (int i=0; i<MAX_DPB_SIZE; i++) {
                    for (int j=0; j<2; j++) {
                        if (dpb[i][j].ref_pic_type==LONG_TERM_REF) {
                            int LongTermPicNum = dpb[i][j].LongTermFrameIdx;

                            RefPicList1[ref_idx_l1].ref_pic_type = LONG_TERM_REF;
                            RefPicList1[ref_idx_l1].LongTermFrameIdx = LongTermPicNum;
                            RefPicList1[ref_idx_l1].PicOrderCnt = dpb[i][j].PicOrderCnt;
                            ref_idx_l1++;
                        }
                    }
                }
                if (ref_idx_l1-numShortTerm>1)
                    qsort(RefPicList1+numShortTerm, ref_idx_l1-numShortTerm, sizeof(dpb_t), compare_long_term_idx);
                if (dump_lists) {
                    dump_ref_pic_list(RefPicList0, ref_idx_l0);
                    dump_ref_pic_list(RefPicList1, ref_idx_l1);
                }
            }

            /* sanity check the reference picture lists */
            if (ref_idx_l0 < sh->num_ref_idx_l0_active_minus1+1)
                rvmessage("%s: picture %d: reference picture list 0 has %d entries, but num_ref_idx_l0_active is %d",
                          filename, picno, ref_idx_l0, sh->num_ref_idx_l0_active_minus1+1);
            if (slice_is_b(sh->slice_type) && ref_idx_l1 < sh->num_ref_idx_l1_active_minus1+1)
                rvmessage("%s: picture %d: reference picture list 1 has %d entries, but num_ref_idx_l1_active is %d",
                          filename, picno, ref_idx_l1, sh->num_ref_idx_l1_active_minus1+1);
        }


        /* 8.2.5.2 decoding process for gaps in frame_num */
        if (sh->frame_num!=PrevRefFrameNum && sh->frame_num!=((PrevRefFrameNum+1)%sps->MaxFrameNum)) {
            if (!sps->gaps_in_frame_num_value_allowed_flag) {
                rvmessage("%s: picture %d: unintentional loss of pictures: gaps in frame_num from %d to %d",
                            filename, picno, PrevRefFrameNum, sh->frame_num);
                numerrors++;
            } else {
                rvexit("%s: time to implement gaps in frame_num", filename);
            }
        }

        /* update dpb */
        if (picture->nal_ref_idc!=0) {
            int MarkedForLongTermRef = 0;

            /* 8.2.5.1 */
            if (picture->nal_unit_type==NAL_CODED_SLICE_IDR) {

                /* all reference picture are marked as "unused for reference" */
                flush_dpb(dpb);

                if (sh->dec_ref_pic_marking.long_term_reference_flag==0) {
                    dpb[0][fid].ref_pic_type = SHORT_TERM_REF;
                    MaxLongTermFrameIdx = -1;
                } else {
                    dpb[0][fid].ref_pic_type = LONG_TERM_REF;
                    dpb[0][fid].LongTermFrameIdx = 0;
                    MaxLongTermFrameIdx = 0;
                }
                dpb[0][fid].FrameNum = sh->frame_num;
                dpb[0][fid].PicOrderCnt = sh->PicOrderCnt;

                /* for decoded reference frame, both its fields are marked the same as the frame */
                if (!sh->field_pic_flag) {
                    dpb[0][!fid].ref_pic_type = dpb[0][fid].ref_pic_type;
                    dpb[0][!fid].FrameNum = dpb[0][fid].FrameNum;
                    dpb[0][!fid].PicOrderCnt  = dpb[0][fid].PicOrderCnt ;
                    dpb[0][!fid].LongTermFrameIdx = dpb[0][fid].LongTermFrameIdx;
                }


            } else {
                if (sh->dec_ref_pic_marking.adaptive_ref_pic_marking_mode_flag==0) {

                    /* 8.2.5.3 sliding window decoded reference picture marking process */

                    /*  If the current picture is a coded field that is the second field in decoding order of a complementary reference field
                        pair, and the first field has been marked as "used for short-term reference", the current picture and the complementary
                        reference field pair are also marked as "used for short-term reference". */
                    if (SecondComplementaryField && (dpb[0][0].ref_pic_type==SHORT_TERM_REF || dpb[0][1].ref_pic_type==SHORT_TERM_REF)) {
                        dpb[0][fid].ref_pic_type = SHORT_TERM_REF;
                        dpb[0][fid].FrameNum = sh->frame_num;
                        dpb[0][fid].PicOrderCnt  = sh->PicOrderCnt;
                    } else {

                        int numShortTerm = 0;
                        int numLongTerm = 0;

                        for (int i=0; i<MAX_DPB_SIZE; i++)
                            if (dpb[i][0].ref_pic_type==SHORT_TERM_REF || dpb[i][1].ref_pic_type==SHORT_TERM_REF)
                                numShortTerm++;
                            else if (dpb[i][0].ref_pic_type==LONG_TERM_REF || dpb[i][1].ref_pic_type==LONG_TERM_REF)
                                numLongTerm++;

                        /* It is a requirement of bitstream conformance that, after marking the current decoded reference picture, the total number
                        of frames with at least one field marked as "used for reference", plus the number of complementary field pairs with at
                        least one field marked as "used for reference", plus the number of non-paired fields marked as "used for reference" shall
                        not be greater than Max( max_num_ref_frames, 1 ). */
                        if (numShortTerm+numLongTerm>MaxNumRefFrames) {
                            rvmessage("%s: picture %d: length of dpb exceeds max_num_ref_frames: %d > %d",
                                      filename, picno, numShortTerm+numLongTerm, MaxNumRefFrames);
                            numerrors++;
                        }

                        if (numShortTerm+numLongTerm==MaxNumRefFrames) {

                            if (numShortTerm<1) {
                                rvmessage("%s: picture %d: dpb is full but does not contain any short term reference pictures", filename, picno);
                                numerrors++;
                            }

                            if (dpb[MaxNumRefFrames-1][0].ref_pic_type != UNUSED_FOR_REF) {
                                //if (verbose>=4)
                                //    rvmessage("marking frame_num=%d poc=%d as unused for reference",
                                //            dpb[MaxNumRefFrames-1][0].FrameNumWrap, dpb[MaxNumRefFrames-1][0].PicOrderCnt);

                                dpb[MaxNumRefFrames-1][0].ref_pic_type = UNUSED_FOR_REF;
                            }
                            if (dpb[MaxNumRefFrames-1][1].ref_pic_type != UNUSED_FOR_REF) {
                                //if (verbose>=1)
                                //    rvmessage("marking frame_num=%d poc=%d as unused for reference",
                                //            dpb[MaxNumRefFrames-1][1].FrameNumWrap, dpb[MaxNumRefFrames-1][1].PicOrderCnt);

                                dpb[MaxNumRefFrames-1][1].ref_pic_type = UNUSED_FOR_REF;
                            }

                        }

                        /* push new reference frame onto front of dpb */
                        //for (int i=MAX_DPB_SIZE-1; i>0; i--)
                        //    memcpy(&dpb[i], &dpb[i-1], sizeof(dpb_t)*2);
                        shift_dpb(dpb);

                        dpb[0][fid].ref_pic_type = SHORT_TERM_REF;
                        dpb[0][fid].FrameNum = sh->frame_num;
                        dpb[0][fid].PicOrderCnt  = sh->PicOrderCnt;

                        /* for decoded reference frame, both its fields are marked the same as the frame */
                        if (!sh->field_pic_flag) {
                            dpb[0][!fid].ref_pic_type = dpb[0][fid].ref_pic_type;
                            dpb[0][!fid].FrameNum = dpb[0][fid].FrameNum;
                            dpb[0][!fid].PicOrderCnt  = dpb[0][fid].PicOrderCnt ;
                        } else {
                            dpb[0][!fid].ref_pic_type = UNUSED_FOR_REF;
                            dpb[0][!fid].FrameNum = -1;
                            dpb[0][!fid].PicOrderCnt  = 0;
                        }
                    }
                    //dump_dpb(dpb);
                } else {

                    /* 8.2.5.4 */
#define MMCO_VERBOSITY 2
                    for (int i=0; i<sh->dec_ref_pic_marking.nb_of_mmco_cmd; i++) {
                        switch (sh->dec_ref_pic_marking.mmco_commands[i]) {
                            case 1: { /* 8.2.5.4.1 Marking process of a short-term reference picture as "unused for reference" */
                                int picNumX = sh->CurrPicNum - (sh->dec_ref_pic_marking.difference_of_pic_nums_minus1[i]+1);
                                if (verbose>=MMCO_VERBOSITY)
                                    rvmessage("marking a short-term reference picture as \"unused for reference\": %d", picNumX);
                                if (sh->field_pic_flag==0) {
                                    for (int j=0; j<MAX_DPB_SIZE; j++)
                                        if (dpb[j][0].FrameNum == picNumX ||
                                            dpb[j][1].FrameNum == picNumX)
                                                dpb[j][0].ref_pic_type = dpb[j][1].ref_pic_type = UNUSED_FOR_REF;
                                } else {
                                    for (int j=0; j<MAX_DPB_SIZE; j++) {
                                        if (dpb[j][0].FrameNum == picNumX)
                                            dpb[j][0].ref_pic_type = UNUSED_FOR_REF;
                                        if (dpb[j][1].FrameNum == picNumX)
                                            dpb[j][1].ref_pic_type = UNUSED_FOR_REF;
                                    }
                                }
                            }
                            case 2: { /* 8.2.5.4.2 Marking process of a long-term reference picture as "unused for reference" */
                                if (verbose>=MMCO_VERBOSITY)
                                    rvmessage("marking a long-term reference picture as \"unused for reference\": %d", sh->dec_ref_pic_marking.long_term_pic_num[i]);
                                if (sh->field_pic_flag==0) {
                                    for (int j=0; j<MAX_DPB_SIZE; j++)
                                        if (dpb[j][0].LongTermFrameIdx == sh->dec_ref_pic_marking.long_term_pic_num[i] ||
                                            dpb[j][1].LongTermFrameIdx == sh->dec_ref_pic_marking.long_term_pic_num[i])
                                            dpb[j][0].ref_pic_type = dpb[j][1].ref_pic_type = UNUSED_FOR_REF;
                                } else {
                                    for (int j=0; j<MAX_DPB_SIZE; j++) {
                                        if (dpb[j][0].LongTermFrameIdx == sh->dec_ref_pic_marking.long_term_pic_num[i])
                                            dpb[j][0].ref_pic_type = UNUSED_FOR_REF;
                                        if (dpb[j][1].LongTermFrameIdx == sh->dec_ref_pic_marking.long_term_pic_num[i])
                                            dpb[j][1].ref_pic_type = UNUSED_FOR_REF;
                                    }
                                }
                            }
                            case 3: { /* 8.2.5.4.3 Assignment process of a LongTermFrameIdx to a short-term reference picture */
                                int picNumX = sh->CurrPicNum - (sh->dec_ref_pic_marking.difference_of_pic_nums_minus1[i]+1);
                                if (verbose>=MMCO_VERBOSITY)
                                    rvmessage("assigning a long-term frame index to a short-term reference picture: %d -> %d", picNumX, sh->dec_ref_pic_marking.long_term_frame_idx[i]);
                                /* TODO check picture is marked as "used for short-term ref" */
                                if (sh->field_pic_flag==0) {
                                    for (int j=0; j<MAX_DPB_SIZE; j++)
                                        if (dpb[j][0].FrameNum == picNumX ||
                                            dpb[j][1].FrameNum == picNumX) {
                                                dpb[j][0].ref_pic_type = dpb[j][1].ref_pic_type = LONG_TERM_REF;
                                                dpb[j][0].LongTermFrameIdx = dpb[j][1].LongTermFrameIdx = sh->dec_ref_pic_marking.long_term_frame_idx[i];
                                        }
                                } else {
                                    for (int j=0; j<MAX_DPB_SIZE; j++) {
                                        if (dpb[j][0].FrameNum == picNumX) {
                                            dpb[j][0].ref_pic_type = LONG_TERM_REF;
                                            dpb[j][0].LongTermFrameIdx = sh->dec_ref_pic_marking.long_term_frame_idx[i];
                                        }
                                        if (dpb[j][1].FrameNum == picNumX) {
                                            dpb[j][1].ref_pic_type = LONG_TERM_REF;
                                            dpb[j][1].LongTermFrameIdx = sh->dec_ref_pic_marking.long_term_frame_idx[i];
                                        }
                                    }
                                }
                            }
                            case 4: { /* 8.2.5.4.4 Decoding process for MaxLongTermFrameIdx */
                                MaxLongTermFrameIdx = sh->dec_ref_pic_marking.max_long_term_frame_idx_plus1[i]-1;
                                if (verbose>=MMCO_VERBOSITY)
                                    rvmessage("setting a new value for MaxLongTermFrameIdx: %d", MaxLongTermFrameIdx);
                                for (int j=0; j<MAX_DPB_SIZE; j++) {
                                    if (dpb[j][0].LongTermFrameIdx > MaxLongTermFrameIdx && dpb[j][0].ref_pic_type==LONG_TERM_REF)
                                        dpb[j][0].ref_pic_type = UNUSED_FOR_REF;
                                    if (dpb[j][1].LongTermFrameIdx > MaxLongTermFrameIdx && dpb[j][1].ref_pic_type==LONG_TERM_REF)
                                        dpb[j][1].ref_pic_type = UNUSED_FOR_REF;
                                }
                            }
                            case 5: { /* 8.2.5.4.5 Marking process of all reference pictures as "unused for reference" */
                                if (verbose>=MMCO_VERBOSITY)
                                    rvmessage("marking all reference pictures as \"unused for reference\"");
                                /* all reference picture are marked as "unused for reference" */
                                flush_dpb(dpb);
                                MaxLongTermFrameIdx = -1;
                            }
                            case 6: { /* 8.2.5.4.6 Process for assigning a long-term frame index to the current picture */
                                if (verbose>=MMCO_VERBOSITY)
                                    rvmessage("assigning a long-term frame index to the current picture: %d", sh->dec_ref_pic_marking.long_term_frame_idx[i]);
                                for (int j=0; j<MAX_DPB_SIZE; j++) {
                                    if (dpb[j][0].LongTermFrameIdx == sh->dec_ref_pic_marking.long_term_frame_idx[i]) {
                                        dpb[j][0].ref_pic_type = UNUSED_FOR_REF;
                                        dpb[j][0].FrameNum = -1;
                                    }
                                    if (dpb[j][1].LongTermFrameIdx == sh->dec_ref_pic_marking.long_term_frame_idx[i]) {
                                        dpb[j][1].ref_pic_type = UNUSED_FOR_REF;
                                        dpb[j][1].FrameNum = -1;
                                    }
                                }
                                /* add picture to the dpb */
                                shift_dpb(dpb);

                                dpb[0][fid].ref_pic_type     = LONG_TERM_REF;
                                dpb[0][fid].FrameNum     = sh->frame_num;
                                dpb[0][fid].LongTermFrameIdx = sh->dec_ref_pic_marking.long_term_frame_idx[i];
                                dpb[0][fid].PicOrderCnt      = sh->PicOrderCnt;

                                /* for decoded reference frame, both its fields are marked the same as the frame */
                                if (!sh->field_pic_flag) {
                                    dpb[0][!fid].ref_pic_type     = dpb[0][fid].ref_pic_type;
                                    dpb[0][!fid].FrameNum     = dpb[0][fid].FrameNum;
                                    dpb[0][!fid].LongTermFrameIdx = dpb[0][fid].LongTermFrameIdx;
                                    dpb[0][!fid].PicOrderCnt      = dpb[0][fid].PicOrderCnt ;
                                } else {
                                    dpb[0][!fid].ref_pic_type     = UNUSED_FOR_REF;
                                    dpb[0][!fid].FrameNum     = -1;
                                    dpb[0][!fid].LongTermFrameIdx = -1;
                                    dpb[0][!fid].PicOrderCnt      = 0;
                                }

                                /* check number of reference frames */
                                int NumRefFrames = num_refs_in_dpb(dpb);
                                if (NumRefFrames > mmax(MaxNumRefFrames, 1))
                                    rvmessage("number of reference frames is greater than max: %d > %d", NumRefFrames, MaxNumRefFrames);

                                MarkedForLongTermRef = 1;
                            }
                        }
                    }

                    /* 3. When the current picture is not an IDR picture and it was not marked as "used for long-term reference" by
                        memory_management_control_operation equal to 6, it is marked as "used for short-term reference". */
                    if (picture->nal_unit_type!=NAL_CODED_SLICE_IDR && MarkedForLongTermRef==0) {

                        /* push new reference frame onto front of dpb */
                        shift_dpb(dpb);

                        dpb[0][fid].ref_pic_type = SHORT_TERM_REF;
                        dpb[0][fid].FrameNum = sh->frame_num;
                        dpb[0][fid].PicOrderCnt  = sh->PicOrderCnt;

                        /* for decoded reference frame, both its fields are marked the same as the frame */
                        if (!sh->field_pic_flag) {
                            dpb[0][!fid].ref_pic_type = dpb[0][fid].ref_pic_type;
                            dpb[0][!fid].FrameNum = dpb[0][fid].FrameNum;
                            dpb[0][!fid].PicOrderCnt  = dpb[0][fid].PicOrderCnt ;
                        } else {
                            dpb[0][!fid].ref_pic_type = UNUSED_FOR_REF;
                            dpb[0][!fid].FrameNum = -1;
                            dpb[0][!fid].PicOrderCnt  = 0;
                        }
                    }

                    //dump_dpb(dpb);
                }
            }

            /* sanity check state of dpb */
            for (int i=0; i<MAX_DPB_SIZE; i++) {
                if (dpb[i][0].ref_pic_type != UNUSED_FOR_REF && dpb[i][1].ref_pic_type != UNUSED_FOR_REF) {
                    if (dpb[i][0].FrameNum != dpb[i][1].FrameNum) {
                        rvmessage("%s: picture %d: complementary field pair in dpb does not have same frame_num: %d != %d",
                                filename, picno, dpb[i][0].FrameNum, dpb[i][1].FrameNum);
                        numerrors++;
                    }
                }
                if (dpb[i][0].ref_pic_type == SHORT_TERM_REF && dpb[i][1].ref_pic_type == LONG_TERM_REF) {
                    rvmessage("%s: picture %d: complementary field pair in dpb does not have same reference type: %d != %d",
                                filename, picno, dpb[i][0].ref_pic_type, dpb[i][1].ref_pic_type);
                    numerrors++;
                }
                if (dpb[i][0].ref_pic_type == LONG_TERM_REF && dpb[i][1].ref_pic_type == SHORT_TERM_REF) {
                    rvmessage("%s: picture %d: complementary field pair in dpb does not have same reference type: %d != %d",
                                filename, picno, dpb[i][0].ref_pic_type, dpb[i][1].ref_pic_type);
                    numerrors++;
                }

                if (dpb[i][0].ref_pic_type != UNUSED_FOR_REF) {
                    int frame_num = dpb[i][0].FrameNum;
                    int PicOrderCnt = dpb[i][0].PicOrderCnt;
                    for (int j=i+1; j<MAX_DPB_SIZE; j++) {
                        if (j!=i) {
                            /* check frame_num */
                            if (dpb[j][0].ref_pic_type != UNUSED_FOR_REF && frame_num == dpb[j][0].FrameNum) {
                                rvmessage("%s: picture %d: duplicate frame_num in dpb: pos %d top-field == pos %d top-field: %d",
                                          filename, picno, i, j, dpb[j][0].FrameNum);
                                numerrors++;
                            }
                            if (dpb[j][1].ref_pic_type != UNUSED_FOR_REF && frame_num == dpb[j][1].FrameNum) {
                                rvmessage("%s: picture %d: duplicate frame_num in dpb: pos %d top-field == pos %d bot-field: %d",
                                          filename, picno, i, j, dpb[j][1].FrameNum);
                                numerrors++;
                            }
                            /* check picture order count */
                            if (dpb[j][0].ref_pic_type != UNUSED_FOR_REF && PicOrderCnt == dpb[j][0].PicOrderCnt) {
                                rvmessage("%s: picture %d: duplicate poc in dpb: pos %d top-field == pos %d top-field: %d",
                                          filename, picno, i, j, dpb[j][0].PicOrderCnt);
                                numerrors++;
                            }
                            if (dpb[j][1].ref_pic_type != UNUSED_FOR_REF && PicOrderCnt == dpb[j][1].PicOrderCnt) {
                                rvmessage("%s: picture %d: duplicate poc in dpb: pos %d top-field == pos %d bot-field: %d",
                                          filename, picno, i, j, dpb[j][1].PicOrderCnt);
                                numerrors++;
                            }
                        }
                    }
                }
                if (dpb[i][1].ref_pic_type != UNUSED_FOR_REF) {
                    int frame_num = dpb[i][1].FrameNum;
                    int PicOrderCnt = dpb[i][1].PicOrderCnt;
                    for (int j=i+1; j<MAX_DPB_SIZE; j++) {
                        if (j!=i) {
                            /* check frame_num */
                            if (dpb[j][0].ref_pic_type != UNUSED_FOR_REF && frame_num == dpb[j][0].FrameNum) {
                                rvmessage("%s: picture %d: duplicate frame_num in dpb: pos %d bot-field == pos %d top-field: %d",
                                          filename, picno, i, j, dpb[j][0].FrameNum);
                                numerrors++;
                            }
                            if (dpb[j][1].ref_pic_type != UNUSED_FOR_REF && frame_num == dpb[j][1].FrameNum) {
                                rvmessage("%s: picture %d: duplicate frame_num in dpb: pos %d bot-field == pos %d bot-field: %d",
                                          filename, picno, i, j, dpb[j][1].FrameNum);
                                numerrors++;
                            }
                            /* check picture order count */
                            if (dpb[j][0].ref_pic_type != UNUSED_FOR_REF && PicOrderCnt == dpb[j][0].PicOrderCnt) {
                                rvmessage("%s: picture %d: duplicate poc in dpb: pos %d bot-field == pos %d top-field: %d",
                                          filename, picno, i, j, dpb[j][0].PicOrderCnt);
                                numerrors++;
                            }
                            if (dpb[j][1].ref_pic_type != UNUSED_FOR_REF && PicOrderCnt == dpb[j][1].PicOrderCnt) {
                                rvmessage("%s: picture %d: duplicate poc in dpb: pos %d bot-field == pos %d bot-field: %d",
                                          filename, picno, i, j, dpb[j][1].PicOrderCnt);
                                numerrors++;
                            }
                        }
                    }
                }
            }
        }

        /* keep previous picture values */
        PrevPicNo = picno;
        PrevNalRefIdc = picture->nal_ref_idc;
        PrevPicOrderCnt = picture->PicOrderCnt;
        PrevFrameNum = picture->frame_num;
        if (sh->field_pic_flag)
            PrevFieldParity = sh->bottom_field_flag;
        PrevSecondComplementaryField = SecondComplementaryField;

        /* keep previous reference picture values */
        if (sps->gaps_in_frame_num_value_allowed_flag) {
        } else {
            if (picture->nal_ref_idc!=0) {
                //PrevNalUnitType = picture->nal_unit_type;
                PrevRefFrameNum = picture->frame_num;
                if (sh->field_pic_flag)
                    PrevRefFieldParity = sh->bottom_field_flag;
            }
        }

        if (numerrors>0 && verbose>=0)
            describe_picture_ref_params(picture, sh, fid, SecondComplementaryField);

        /* tidy up */
        free_264_picture(picture);

        if (numerrors>0) {
            if (!noexit) {
                dump_dpb(dpb);
                break;
            }
            toterrors += numerrors;
        }
    }

    /* tidy up */
    free_264_sequence(seq);

    if (verbose>=0)
        rvmessage("%s: checked %d pictures, %d errors", filename, picno, toterrors);

    return status;
}

void check_ts(struct bitbuf *bb, const char *filename, int checkpid, int timing_margin /* secs */, int noexit, int verbose)
{
    int i, j;

    /* initialise pid count check */
    int pid_count[8192] = {0};

    /* initialise continuity counter check */
    char continuity[8192];
    memset(continuity, -1, sizeof(continuity));

    /* initialise pcr timing check */
    char pcr_strike[8192] = {0};
    long long pcr_prev[8192];
    int packno_prev[8192] = {0};
    memset(pcr_prev, -1, sizeof(pcr_prev));

    /* initialise pcr accuracy check */
    long long pcr_ac_min = 0x7FFFFFFFFFFFFFFFll;
    long long pcr_ac_max = 0;
    long long pcr_ac_sum = 0;
    long long pcr_ac_cnt = 0;

    /* initialise pts and dts timing check */
    char pts_strike[8192] = {0};
    char dts_strike[8192] = {0};
    int vid_pid = checkpid>0 ? checkpid : 0;
#define MAX_AUD_PIDS 4
    int aud_pid[MAX_AUD_PIDS] = {0, 0, 0, 0};
    int pcr_pid = 0;
    int pmt_pid = 0;
    int num_aud_pids = 0;
    long long prev_pts[8192];
    long long prev_dts[8192];
    for (i=0; i<8192; i++)
        prev_pts[i] = prev_dts[i] = -1;
    long long max_vid_aud_diff[8192] = {0};
    long long max_arrival[8192] = {0};
    long long min_arrival[8192];
    for (i=0; i<8192; i++)
        min_arrival[i] = __INT64_MAX__;

    /* initialise running bitrate measure */
    long long bitrate_pcr_sum = 0;
    int bitrate_packet_sum = 0;

    /* initialise average bitrate measure */
    long long first_pcr = -1;
    long long last_pcr = -1;
    int first_pcr_packet = 0;
    int last_pcr_packet = 0;
    long long ts_rate_estimate = 0;

    /* initialise duplicate packet detection */
    crc32_t prev_header_crc32[8192] = {0};
    crc32_t prev_payload_crc32[8192] = {0};

    /* initialise program information tables */
    struct program_association_table *pat = (struct program_association_table *)rvalloc(NULL, sizeof(struct program_association_table), 1);
    struct program_map_table *pmt[MAX_PMTS] = {NULL};
    pat->version_displayed = -1;

    /* initialise section buffers */
    /* buffers include space for an extra transport packet of packet stuffing bytes */
    u_int8_t pat_section[SECTION_BUFFER_SIZE];
    u_int8_t *pmt_section[MAX_PMTS] = {NULL};
    int pat_pointer = 0, pmt_pointer[MAX_PMTS];

    /* initialise program information tables crc check */
    char pat_crc32_fail[8192] = {0};
    char pmt_crc32_fail[8192] = {0};

    /* initialise mpeg2 specific data */
    struct rvm2vsequence *sequence = NULL;

    /* initialise T-STD buffers */
    double TBn = 0.0;
    double MBn = 0.0;
    double EBn = 0.0;
    double TBsys = 0.0;
    double Bsys = 0.0;

    /* initialise T-STD buffer parameters */
    double Rxn = 0.0;
    double Rbxn = 0.0;
    double MBSn = 0.0;
    //double EBSn = 0.0;

    /* loop over transport stream packets */
    int packno = 0;
    while (!eofbits(bb)) {

        /* resynchronise */
        int search_bits = find_next_sync_code(bb);
        if (search_bits<0)
            break;
        if (verbose>=1 && search_bits)
            rvmessage("%s: packet %5d: searched %d bytes to find next sync byte", filename, packno, search_bits/8);

        /* read the next complete transport stream packet */
        u_int8_t packet[188];
        if (read_transport_packet(bb, verbose, packet)<0)
            break;
        packno++;

        /* check for synchronisation */
        if (showbits(bb, 8)!=0x47 && !eofbits(bb))
            rvmessage("%s: packet %5d: packet does not have end sync", filename, packno);

        /* prepare to parse transport stream packet */
        struct bitbuf *pb = initbits_memory(packet, 188);

        /* parse transport packet */
        int transport_error_indicator, payload_unit_start_indicator;
        int pid, adaptation_field_control, continuity_counter, pcr_flag;
        int discontinuity_indicator;
        long long pcr;
        int header_length = parse_transport_packet_header(pb, verbose, packno, &transport_error_indicator,
                        &payload_unit_start_indicator, &pid, &adaptation_field_control, &continuity_counter,
                        &discontinuity_indicator, &pcr_flag, &pcr);
        if (header_length<0) {
            rvmessage("%s: packet %5d: failed to parse transport packet header", filename, packno);
            break;
        }

        /* skip checks if requested */
        if (checkpid>=0 && pid!=checkpid)
            continue;

        /* give the memory bitbuf a better name */
        char bufname[16];
        snprintf(bufname, sizeof(bufname), "pid %d", pid);
        pb->filename = bufname;

        /* detect duplicate packets */
        /* NOTE the pcr's of duplicate packets will differ, so we need to skip them */
        crc32_t header_crc32  = crc32(packet, 4);
        crc32_t payload_crc32 = crc32(packet+header_length, 188-header_length);
        int duplicate_packet = (header_crc32==prev_header_crc32[pid]) && (payload_crc32==prev_payload_crc32[pid])? 1 : 0;
        prev_header_crc32[pid] = header_crc32;
        prev_payload_crc32[pid] = payload_crc32;

        /* check transport error */
        if (pid>=0 && pid<0x1FFF)
            if (transport_error_indicator && verbose>=1)
                rvmessage("%s: packet %5d: transport error has been indicated", filename, packno);

        /* check continuity_counter */
        if (pid>=0 && pid<0x1FFF) {
            if (continuity[pid]>=0) {
                int next_continuity = adaptation_field_control==0 || adaptation_field_control==2? continuity[pid] : (continuity[pid]+1)%16;
                if (next_continuity != continuity_counter) {
                    if (discontinuity_indicator) {
                        if (verbose>=1)
                            rvmessage("%s: packet %5d: discontinuity has been indicated (continuity goes from %d to %d)", filename, packno, continuity[pid], continuity_counter);
                    } else if ((continuity[pid] == continuity_counter) && duplicate_packet)
                        rvmessage("%s: packet %5d: duplicate packet", filename, packno);
                    else
                        rvmessage("%s: packet %5d: continuity error in pid %d, expected %d, packet has %d", filename, packno, pid, next_continuity, continuity_counter);
                }
            }
            continuity[pid] = continuity_counter;
        }

        /* check program clock recovery */
#define max_strikes 10
        if (pid>=0 && pid<0x1FFF) {
            if (pcr_flag && pcr_strike[pid]<max_strikes) {
                if (pcr_prev[pid]>=0) {
                    if (!discontinuity_indicator) {
                        if (pcr>pcr_prev[pid]) {
                            /* this is expected timing information */
                            if (pcr-pcr_prev[pid]>100ll*27000ll)
                                rvmessage("%s: packet %5d: pcr late in pid %d: %lld ms (%lld - %lld)", filename, packno, pid, (pcr-pcr_prev[pid])/27000ll, pcr, pcr_prev[pid]);

                            /* update running bitrate measure */
                            long long pcr_delta = pcr - pcr_prev[pid];
                            int packet_delta = packno - packno_prev[pid];
                            bitrate_pcr_sum += pcr_delta;
                            bitrate_packet_sum += packet_delta;
                            if (verbose>=3)
                                rvmessage("%s: latest transport stream bitrate estimate is %.2fMbps", filename, (bitrate_packet_sum*188.0*8.0*27.0) / (double)bitrate_pcr_sum);

                            /* check pcr accuracy */
                            long long expected_pcr = (long long)packet_delta * bitrate_pcr_sum / (long long)bitrate_packet_sum;
                            //long long expected_pcr = (long long)packet_delta * (last_pcr-first_pcr) / (long long)(last_pcr_packet-first_pcr_packet);
                            long long pcr_ac = pcr_delta - expected_pcr;
                            if (abs(pcr_ac)*1000ll > 27ll*500ll) { /* 500 ns */
                                rvmessage("%s: packet %5d: pid %4d: pcr_ac of %lldns, bitrate estimate is %.2fMbps", filename, packno, pid, pcr_ac*1000ll/27ll, (bitrate_packet_sum*188.0*8.0*27.0) / (double)bitrate_pcr_sum);
                                //rvmessage("expected %lld delta %lld pcr_ac %lld", expected_pcr, pcr_delta, pcr_ac);
                                //rvmessage("packet %d previous %d delta %d", packno, packno_prev[pid], packet_delta);
                                pcr_strike[pid]++;
                            } else if (verbose>=3)
                                rvmessage("%s: packet %5d: pid %4d: pcr_ac of %lldns", filename, packno, pid, pcr_ac*1000ll/27ll);

                            /* measure overall pcr accuracy */
                            if (pid==pcr_pid) {
                                if (pcr_ac<pcr_ac_min)
                                    pcr_ac_min = pcr_ac;
                                if (pcr_ac>pcr_ac_max)
                                    pcr_ac_max = pcr_ac;
                                pcr_ac_sum += pcr_ac;
                                pcr_ac_cnt++;
                            }

                            if (verbose>=2)
                                rvmessage("%s: packet %5d: pcr=%.2fms", filename, packno, pcr_to_ms(pcr));

                        } else {
                            /* this is degenerate pcr timing */
                            if (pcr==pcr_prev[pid])
                                rvmessage("%s: packet %5d: pcr in pid %d is not incrementing", filename, packno, pid);
                            if (pcr<pcr_prev[pid])
                                rvmessage("%s: packet %5d: pcr in pid %d jumps back in time: pcr=%.2fms previous=%.2fms",
                                          filename, packno, pid, pcr_to_ms(pcr), pcr_to_ms(pcr_prev[pid]));
                            pcr_strike[pid]++;
                        }
                    }
                }

                if (verbose>=4) {
                    /*long long clock = pcr/27000ll;
                    int msecs = clock % 1000ll;
                    clock /= 1000ll;
                    int secs = clock % 60ll;
                    clock /= 60ll;
                    int mins = clock % 60ll;
                    clock /= 60ll;
                    int hours = clock;*/
                    if (pcr_prev[pid]>=0)
                        rvmessage("%s: packet %5d: pcr in pid %d is %lld (increase of %lldmsecs after %dbytes)", filename, packno, pid, pcr, (pcr-pcr_prev[pid])/27000ll, (packno-packno_prev[pid])*188);
                    else
                        rvmessage("%s: packet %5d: pcr in pid %d is %lld (first pcr)", filename, packno, pid, pcr);
                }

                pcr_prev[pid] = pcr;
                packno_prev[pid] = packno;

                if (pcr_strike[pid]==max_strikes)
                    rvmessage("%s: packet %5d: %d strikes against pid %d, disabling pcr checks", filename, packno, pcr_strike[pid], pid);
            }

            /* record statistics */
            pid_count[pid]++;
        }

        /* measure average bitrate from pcr */
        if (pcr_flag && pid==pcr_pid) {
            /* find first and last pcrs in bitstream */
            if (first_pcr<0) {
                first_pcr = pcr;
                first_pcr_packet = packno;
            } else {
                last_pcr = pcr;
                last_pcr_packet = packno;
                ts_rate_estimate = (last_pcr_packet-first_pcr_packet) * (long long)(188*8) * 27000000ll / (last_pcr-first_pcr);
            }
        }

        /* calculate payload size and check sanity */
        int payload_size = 188-header_length;
        if (payload_size==0) {
            freebits(pb);
            continue;
        }
        if (payload_size<0) {
            if (verbose>=1)
                rvmessage("error parsing transport packet header, resyncing");
            freebits(pb);
            continue;
        }

        /* parse transport packet payload */
        int pes_packet_data_length = 0;
        if (adaptation_field_control==1 || adaptation_field_control==3) {
            if (pid==0x0000) {
                /* look for new pat section */
                if (payload_unit_start_indicator) {
                    payload_size -= parse_pointer_field(pb);
                    pat_pointer = 0;
                }

                /* copy payload to pat section */
                while (payload_size) {
                    if (pat_pointer==1024) {
                        rvmessage("%s: packet %5d: pat section is too large", filename, packno);
                        break;
                    }
                    pat_section[pat_pointer++] = getbits8(pb);
                    payload_size--;
                }

                /* try to parse pat section */
                struct bitbuf *sb = initbits_memory(pat_section, pat_pointer);
                int section_length = parse_program_association_section(sb, pat_pointer, verbose, pat);
                freebits(sb);

                if (section_length && pat->version_displayed!=pat->version_number) {
                    if (verbose>=1)
                        describe_program_association(pat);

                    pat->version_displayed = pat->version_number;
                }

                if (section_length) {
                    /* check the pat crc */
                    if (pat_crc32_fail[pid]==0) {
                        crc32_t pat_crc32 = crc32(pat_section, section_length-4);
                        if (pat_crc32 != pat->crc32) {
                            rvmessage("%s: packet %5d: crc mismatch in pat pid %d: calculated %08x, coded %08x", filename, packno, pid, pat_crc32, pat->crc32);
                            pat_crc32_fail[pid] = 1;
                        }
                    }
                }


            } else if (pid==vid_pid || is_in_list(pid, aud_pid, MAX_AUD_PIDS)) {

                /* look for a pes header */
                if (payload_unit_start_indicator) {
                    /* parse pes header */
                    int leading = 0;
                    while (showbits24(pb)!=0x000001 && !eofbits(pb))
                        leading += flushbits(pb, 8);
                    if (eofbits(pb)) {
                        freebits(pb);
                        continue;
                    }
                    if (leading && verbose>=1)
                        rvmessage("%s: %d bits of leading garbage before start code in pes packet", filename, leading);

                    flushbits(pb, 24);                                  /* packet_start_code_prefix */
                    int stream_id = getbits8(pb);                       /* stream_id */
                    int pes_packet_length = getbits(pb, 16);            /* PES_packet_length */
                    /*if (verbose>=2)
                        describe_pes_packet(stream_id, pes_packet_length);*/

                    /* parse pes packet */
                    long long pts = -1, dts = -1;
                    pes_packet_data_length = parse_pes_packet_header(pb, stream_id, pes_packet_length, &pts, &dts, verbose);
                    pes_packet_data_length = mmax(0, pes_packet_data_length); /* for unbounded pes packets */

                    if (verbose>=2) {
//                         const long long pts_step = 3003;
//                         if (pts>=0 && prev_pts>=0 && dts>=0 && prev_dts>=0 && pts!=dts)
//                             rvmessage("%s: packet %5d: pes: %d data bytes, pts=%lld dts=%lld pts_delta=%lld dts_delta=%lld dts_pts_delta=%lld",
//                                         filename, packno, pes_packet_data_length, pts/pts_step, dts/pts_step, (pts-prev_pts)/pts_step, (dts-prev_dts)/pts_step, (pts-dts)/pts_step);
//                         else if (pts>=0 && prev_pts>=0 && prev_dts>=0)
//                             rvmessage("%s: packet %5d: pes: %d data bytes, pts=%lld dts=%lld pts_delta=%lld dts_delta=%lld",
//                                         filename, packno, pes_packet_data_length, pts/pts_step, pts/pts_step, (pts-prev_pts)/pts_step, (pts-prev_dts)/pts_step);
//                         else if (pts>=0 && dts>=0 && pts!=dts)
//                             rvmessage("%s: packet %5d: pes: %d data bytes, pts=%lld dts=%lld                                     dts_pts_delta=%lld",
//                                         filename, packno, pes_packet_data_length, pts/pts_step, dts/pts_step, (pts-dts)/pts_step);
//                         else if (pts>=0)
//                             rvmessage("%s: packet %5d: pes: %d data bytes, pts=%lld",
//                                         filename, packno, pes_packet_data_length, pts/pts_step);
//                         else
//                             rvmessage("%s: packet %5d: pes: %d data bytes", filename, packno, pes_packet_data_length);
                        if (pts>=0 && prev_pts[pid]>=0 && dts>=0 && prev_dts[pid]>=0 && pts!=dts)
                            rvmessage("%s: packet %5d: pes: %d data bytes, pts=%.1fms dts=%.1fms pts_delta=%5.1fms dts_delta=%5.1fms dts_pts_delta=%5.1fms",
                                        filename, packno, pes_packet_data_length, pts_to_ms(pts), pts_to_ms(dts), pts_to_ms(pts-prev_pts[pid]), pts_to_ms(dts-prev_dts[pid]), pts_to_ms(pts-dts));
                        else if (pts>=0 && prev_pts[pid]>=0 && prev_dts[pid]>=0)
                            rvmessage("%s: packet %5d: pes: %d data bytes, pts=%.1fms dts=%.1fms pts_delta=%5.1fms dts_delta=%5.1fms",
                                        filename, packno, pes_packet_data_length, pts_to_ms(pts), pts_to_ms(pts), pts_to_ms(pts-prev_pts[pid]), pts_to_ms(pts-prev_dts[pid]));
                        else if (pts>=0 && dts>=0 && pts!=dts)
                            rvmessage("%s: packet %5d: pes: %d data bytes, pts=%.1fms dts=%.1fms                                     dts_pts_delta=%5.1fms",
                                        filename, packno, pes_packet_data_length, pts_to_ms(pts), pts_to_ms(dts), pts_to_ms(pts-dts));
                        else if (pts>=0)
                            rvmessage("%s: packet %5d: pes: %d data bytes, pts=%.1fms",
                                        filename, packno, pes_packet_data_length, pts_to_ms(pts));
                        else
                            rvmessage("%s: packet %5d: pes: %d data bytes", filename, packno, pes_packet_data_length);
                    }

                    /* calculate system time clock from pcr */
                    long long stc = -1;
                    if (bitrate_packet_sum>0 && pcr_prev[pcr_pid]>=0) {
                        stc = pcr_prev[pcr_pid];
                        stc += (long long)(packno - packno_prev[pcr_pid]) * bitrate_pcr_sum / (long long)bitrate_packet_sum;
                    }

                    /* check pts timing */
                    if (pts>=0 && pts_strike[pid]<max_strikes) {
                        if (prev_pts[pid]>=0 && pts-prev_pts[pid]>700*90) {
                            rvmessage("%s: packet %5d: pts late in pid %d: %.1fms", filename, packno, pid, pts_to_ms(pts-prev_pts[pid]));
                            pts_strike[pid]++;
                        }
                        if (pts<dts) {
                            rvmessage("%s: packet %5d: pts is earlier than dts in pid %d: pts=%.1fms dts=%.1fms", filename, packno, pid, pts_to_ms(pts), pts_to_ms(dts));
                            pts_strike[pid]++;
                        }

                        /* compare pts to system time */
                        if (stc>=0) {
                            if (pts*300ll<stc) {
                                rvmessage("%s: packet %5d: pts is in the past in pid %d: pts=%.2fms stc=%.1fms (%.2fms)", filename, packno, pid, pts_to_ms(pts), pcr_to_ms(stc), pts_to_ms(pts)-pcr_to_ms(stc));
                                pts_strike[pid]++;
                            }
                            if (pts*300ll>stc+timing_margin*27000000ll) {
                                rvmessage("%s: packet %5d: pts is %.1f second%s in the future in pid %d: pts=%.2fs stc=%.2fs", filename, packno, pcr_to_sec(pts*300ll-stc), timing_margin!=1? "s" : "", pid, pts_to_sec(pts), pcr_to_sec(stc));
                                pts_strike[pid]++;
                            }
                        }

                        if (pts_strike[pid]==max_strikes && !noexit)
                            rvmessage("%s: packet %5d: %d strikes against pid %d, disabling pts checks", filename, packno, pts_strike[pid], pid);

                        if (verbose>=2)
                            rvmessage("%s: packet %5d: pts-pcr is %.1fms", filename, packno, pcr_to_ms(pts*300ll-stc));
                    }

                    /* check dts timing */
                    if (dts>=0 && stc>=0 && dts_strike[pid]<max_strikes) {
                        if (dts*300ll<stc) {
                            rvmessage("%s: packet %5d: dts is in the past in pid %d: dts=%.2fms stc=%.1fms (%.2fms)", filename, packno, pid, pts_to_ms(dts), pcr_to_ms(stc), pts_to_ms(dts)-pcr_to_ms(stc));
                            dts_strike[pid]++;
                        }
                        if (dts*300ll>stc+timing_margin*27000000ll) {
                            rvmessage("%s: packet %5d: dts is %.1f second%s in the future in pid %d: dts=%.2fs stc=%.2fs", filename, packno, pcr_to_sec(dts*300ll-stc), timing_margin!=1? "s" : "", pid, pts_to_sec(dts), pcr_to_sec(stc));
                            dts_strike[pid]++;
                        }

                        /* compare video and audio dts */
                        for (i=0; i<MAX_AUD_PIDS; i++) {
                            if (pid==aud_pid[i] && vid_pid!=0 && prev_dts[vid_pid]>0) {
                                long long vid_aud_diff = llabs(dts-prev_dts[vid_pid]);
                                if (vid_aud_diff>timing_margin*90000ll) {
                                    rvmessage("%s: packet %5d: audio dts is %.1f second%s different from video in pid %d: pts=%.2fs stc=%.2fs", filename, packno, pts_to_sec(dts-prev_dts[vid_pid]), timing_margin!=1? "s" : "", pid, pts_to_sec(dts), pcr_to_sec(stc));
                                    dts_strike[pid]++;
                                }
                                if (vid_aud_diff>max_vid_aud_diff[pid])
                                    max_vid_aud_diff[pid] = vid_aud_diff;
                            }
                        }

                        /* monitor video and audio arrival times */
                        for (i=0; i<MAX_AUD_PIDS; i++) {
                            if (pid==aud_pid[i] || (i==0 && pid==vid_pid)) {
                                long long aud_arrival = mmax(dts*300ll - stc, 0);
                                if (aud_arrival>max_arrival[pid])
                                    max_arrival[pid] = aud_arrival;
                                if (aud_arrival<min_arrival[pid])
                                    min_arrival[pid] = aud_arrival;
                            }
                        }

                        if (dts_strike[pid]==max_strikes && !noexit)
                            rvmessage("%s: packet %5d: %d strikes against pid %d, disabling dts checks", filename, packno, dts_strike[pid], pid);
                    }

                    /* parse mpeg2 sequence header FIXME this assumes the entire sequence headers is in one ts packet */
                    if (showbits32(pb)==0x000001B3 && sequence==NULL) {
                        sequence = parse_m2v_headers(pb, verbose);
                        if (eofbits(pb))
                                rvmessage("warning: sequence header crosses multiple transport stream packets");
                        if (sequence) {
                            int profile = (sequence->profile_and_level >> 4) & 0x7;
                            int level   = sequence->profile_and_level & 0xF;

                            /* initialise T-STD parameters */
                            Rxn = max_bit_rate_profile_level(profile, level) * 5.0/4.0;
                            double BSmux = max_bit_rate_profile_level(profile, level) * 0.004;
                            double BSoh  = max_bit_rate_profile_level(profile, level) / 750.0;
                            if (level==10 || level==8) {
                                /* low and main level */
                                Rbxn = max_bit_rate_profile_level(profile, level);
                                MBSn = BSmux + BSoh + max_vbv_buffer_size_profile_level(profile, level) - sequence->vbv_buffer_size;
                            }
                            if (level==6 || level==4) {
                                /* high 1440 and high level */
                                Rbxn = mmin(1.05*sequence->bit_rate*400.0, max_bit_rate_profile_level(profile, level));
                                MBSn = BSmux + BSoh;
                            }
                            //EBSn = sequence->vbv_buffer_size;
                        }
                    }

                    if (pts>=0)
                        prev_pts[pid] = pts;
                    if (dts>=0)
                        prev_dts[pid] = dts;

                    /* tidy up */
                }

            } else if (pid==8191) {

                /* check null packets don't have an adaption field */
                if (payload_size!=184)
                    rvmessage("%s: packet %5d: unusual null packet header with adaption field: payload_size=%d", filename, packno, payload_size);

                /* look for a custom start code in null packet */
                /* (only when specifically checking null packets) */
                if (checkpid==8191) {
                    for (i=0; i<184; i++) {
                        if (showbits(pb, 24)==1) {
                            flushbits(pb, 24);
                            const int bytes = 19;
                            char secret[bytes*5];
                            for (int j=0, len=0; j<bytes; j++)
                                len += snprintf(secret+len, sizeof(secret)-len, "0x%02x ", getbits(pb, 8));
                            rvmessage("%s: packet %5d: start code hidden at pos %d in null packet: %s", filename, packno, i, secret);
                            break;
                        }
                    }
                }

            } else {
                for (i=0; i<pat->number_programs; i++)
                    if (pid==pat->program[i].program_map_pid) {
                        /* allocate pmt table and pmt section */
                        if (pmt[i]==NULL) {
                            pmt[i] = (struct program_map_table *)rvalloc(NULL, sizeof(struct program_map_table), 1);
                            pmt_section[i] = (unsigned char *)rvalloc(NULL, SECTION_BUFFER_SIZE, 1);
                            pmt[i]->version_displayed = -1;
                            pmt_pointer[i] = 0;
                        }

                        /* look for new pmt section */
                        if (payload_unit_start_indicator) {
                            payload_size -= parse_pointer_field(pb);
                            pmt_pointer[i] = 0;
                        }

                        /* copy payload to pmt section */
                        while (payload_size) {
                            if (pmt_pointer[i]==1024) {
                                rvmessage("%s: packet %5d: pmt section is too large", filename, packno);
                                break;
                            }
                            pmt_section[i][pmt_pointer[i]++] = getbits8(pb);
                            payload_size--;
                        }

                        /* try to parse pmt section */
                        struct bitbuf *sb = initbits_memory(pmt_section[i], pmt_pointer[i]);
                        int section_length = parse_program_map_section(sb, pmt_pointer[i], verbose, pmt[i]);
                        freebits(sb);

                        if (section_length>0) {
                            /* check the pmt crc */
                            if (pmt_crc32_fail[pid]==0) {
                                crc32_t pmt_crc32 = crc32(pmt_section[i], section_length-4);
                                if (pmt_crc32 != pmt[i]->crc32) {
                                    rvmessage("%s: packet %5d: crc mismatch in pmt pid %d: calculated %08x, coded %08x", filename, packno, pid, pmt_crc32, pmt[i]->crc32);
                                    pmt_crc32_fail[pid] = 1;
                                }
                            }

                            if (pmt[i]->version_displayed!=pmt[i]->version_number) {
                                /* display pmt */
                                if (verbose>=1)
                                    describe_program_map(pmt[i]);

                                pmt[i]->version_displayed = pmt[i]->version_number;
                            }

                            /* default to the first video and audio stream for timing analysis */
                            for (j=0; j<pmt[i]->number_streams; j++) {
                                switch (pmt[i]->stream[j].stream_type) {
                                    case STREAM_TYPE_VIDEO_MPEG1:
                                    case STREAM_TYPE_VIDEO_MPEG2:
                                    case STREAM_TYPE_VIDEO_MPEG4:
                                    case STREAM_TYPE_VIDEO_H264:
                                    case STREAM_TYPE_VIDEO_H264SVC:
                                    case STREAM_TYPE_VIDEO_H264MVC:
                                    case STREAM_TYPE_VIDEO_HEVC:
                                    case STREAM_TYPE_VIDEO_JPEG2000:
                                    case STREAM_TYPE_VIDEO_AVS:
                                    //case STREAM_TYPE_VIDEO_WMV:
                                    case STREAM_TYPE_VIDEO_DIRAC:
                                    case STREAM_TYPE_VIDEO_VC1:
                                        if (vid_pid==0) {
                                            vid_pid = pmt[i]->stream[j].elementary_pid;
                                            pmt_pid = pid;
                                            if (verbose>=1)
                                                rvmessage("%s: video pid is %d, stream %d in program %d, pcr_pid is %d", filename, vid_pid, j, pmt[i]->program_number, pmt[i]->pcr_pid);
                                        }
                                        break;

                                    case STREAM_TYPE_AUDIO_MPEG1:
                                    case STREAM_TYPE_AUDIO_MPEG2:
                                    case STREAM_TYPE_AUDIO_AAC:
                                    case STREAM_TYPE_AUDIO_LOAS:
                                    case STREAM_TYPE_AUDIO_MPEG4_RAW:
                                    case STREAM_TYPE_AUDIO_AC3:
                                    case STREAM_TYPE_AUDIO_DTS_6CHAN:
                                    case STREAM_TYPE_AUDIO_TRUEHD:
                                    case STREAM_TYPE_AUDIO_DTS_8CHAN:
                                    case STREAM_TYPE_AUDIO_DTS:
                                        if (aud_pid[num_aud_pids]==0) {
                                            aud_pid[num_aud_pids] = pmt[i]->stream[j].elementary_pid;
                                            pmt_pid = pid;
                                            if (verbose>=1)
                                                rvmessage("%s: audio pid is %d, stream %d in program %d, pcr_pid is %d", filename, aud_pid[num_aud_pids], j, pmt[i]->program_number, pmt[i]->pcr_pid);
                                            num_aud_pids = mmin(num_aud_pids+1, MAX_AUD_PIDS);
                                        }
                                        break;

                                    // also look for formats identified by the registration descriptor.
                                    case STREAM_TYPE_PRIVATE_DATA:
                                        switch (pmt[i]->stream[j].format_identifier) {
                                            case 0x56432d31: // vc-1 video
                                            case 0x56432d34: // vc-4 video
                                                if (vid_pid==0) {
                                                    vid_pid = pmt[i]->stream[j].elementary_pid;
                                                    pmt_pid = pid;
                                                    if (verbose>=1)
                                                        rvmessage("%s: video pid is %d, stream %d in program %d, pcr_pid is %d", filename, vid_pid, j, pmt[i]->program_number, pmt[i]->pcr_pid);
                                                }
                                                break;

                                            case 0x41432D33: // atsc ac-3 audio
                                            case 0x42535344: // smpte s302m pcm audio
                                            case 0x47413934: // atsc a/53
                                            case 0x6d6c7061: // dolby truehd audio
                                            case 0x4d54524d: // d-vhs
                                            case 0x4f707573: // opus audio
                                                if (aud_pid[num_aud_pids]==0) {
                                                    aud_pid[num_aud_pids] = pmt[i]->stream[j].elementary_pid;
                                                    pmt_pid = pid;
                                                    if (verbose>=1)
                                                        rvmessage("%s: audio pid is %d, stream %d in program %d, pcr_pid is %d", filename, aud_pid[num_aud_pids], j, pmt[i]->program_number, pmt[i]->pcr_pid);
                                                    num_aud_pids = mmin(num_aud_pids+1, MAX_AUD_PIDS);
                                                }
                                                break;
                                        }
                                        break;
                                }

                                /* note the pcr_pid for the video stream */
                                if (pcr_pid==0 && vid_pid==pmt[i]->stream[j].elementary_pid)
                                    pcr_pid = pmt[i]->pcr_pid;
                            }
                        }
                    }
            }
        }

        /* check T-STD buffering */
        if (pid>=0 && pid<0x1FFF && ts_rate_estimate>0 && sequence) {
            /* TODO these tests might only work with a given ts bitrate */
            long long ts_rate = ts_rate_estimate;
            double packet_time = 188.0*8.0 / ts_rate;

            /* calculate various bitrates */
            //int Rxn = max_bit_rate_profile_level(profile, level) * 5/4;
            static const int Rxsys = 1e6;
            int Rsys = mmax(80000, ts_rate/500);

            /* fill transport buffers at constant bitrate TODO use piecewise linear bitrate */
            double TBn_prev = TBn;
            if (pid==vid_pid)
                TBn += 188*8;

            double TBsys_prev = TBsys;
            if (pid==0 || pid==1 || pid==pmt_pid || (pid==pcr_pid && pcr_pid!=vid_pid)) /* include pcr packets which do not carry video data */
                TBsys += 188*8;

            /* empty transport buffers */
            double TBn_delta = mmin(TBn, Rxn * packet_time);
            double TBsys_delta = mmin(TBsys, Rxsys * packet_time);
            TBn -= TBn_delta;
            TBsys -= TBsys_delta;

            /* check for transport buffer overflow */
            if (TBn>512.0*8.0 && TBn_prev<=512.0*8.0)
                rvmessage("%s: packet %5d: T-STD: video transport buffer (TBn) overflow: %.1f bytes (+%d -%.1f)", filename, packno, TBn/8.0, pid==vid_pid? 188 : 0, TBn_delta);
            if (TBn<=512.0*8.0 && TBn_prev>512.0*8.0)
                rvmessage("%s: packet %5d: T-STD: video transport buffer (TBn) normal: %.1f bytes", filename, packno, TBn/8.0);
            if (TBsys>512.0*8.0 && TBsys_prev<=512.0*8.0)
                rvmessage("%s: packet %5d: T-STD: system transport buffer (TBsys) overflow: %.1f bytes", filename, packno, TBsys/8.0);
            if (TBsys<=512.0*8.0 && TBsys_prev>512.0*8.0)
                rvmessage("%s: packet %5d: T-STD: system transport buffer (TBsys) normal: %.1f bytes", filename, packno, TBsys/8.0);

            /* at some time in the future this data will fill main/multiplex buffers */
            double MBn_prev = MBn;
            double Bsys_prev = Bsys;
            if (!duplicate_packet) {
                if (pid==vid_pid)
                    MBn += (188-header_length)*8;

                if (pid==0 || pid==1 || pid==pmt_pid || (pid==pcr_pid && pcr_pid!=vid_pid)) /* include pcr packets which do not carry video data */
                    Bsys += (188-header_length)*8;
            }

            /* empty main/multiplex buffers */
            double MBn_delta = mmax(EBn, mmin(MBn, Rbxn * packet_time));
            double Bsys_delta = mmin(Bsys, Rsys * packet_time);
            MBn -= MBn_delta;
            Bsys -= Bsys_delta;

            /* check for main/multiplex buffer overflow */
            if (MBn>MBSn && MBn_prev<=MBSn)
                rvmessage("%s: packet %5d: T-STD: multiplex transport buffer (Bn) overflow: %.1f bytes", filename, packno, MBn/8.0);
            if (MBn<=MBSn && MBn_prev>MBSn)
                rvmessage("%s: packet %5d: T-STD: multiplex transport buffer (Bn) normal: %.1f bytes", filename, packno, MBn/8.0);
            if (Bsys>1536.0*8.0 && Bsys_prev<=1536.0*8.0)
                rvmessage("%s: packet %5d: T-STD: main transport buffer (Bsys) overflow: %.1f bytes", filename, packno, Bsys/8.0);
            if (Bsys<=1536.0*8.0 && Bsys_prev>1536.0*8.0)
                rvmessage("%s: packet %5d: T-STD: main transport buffer (Bsys) overflow: %.1f bytes", filename, packno, Bsys/8.0);

            /* at some further time in the future this data will fill the elementary stream buffer */
            if (!duplicate_packet) {
                if (pid==vid_pid) {
                    if (payload_unit_start_indicator)
                        EBn += pes_packet_data_length*8;
                    else
                        EBn += (188-header_length)*8;
                }
            }

            /* empty elementary stream buffer */
            // Hmmm.... need to know the delay in the previous buffers
            // to remove the AU at the time given by DTS
        }

        /* tidy up */
        freebits(pb);
    }

    /* check that all pcrs were present in the bitstream */
    if (pat) {
        int number_pcrs = 0;
        int pcr_pids[MAX_PMTS];

        for (i=0; i<pat->number_programs; i++) {
            if (pmt[i] && pmt[i]->number_streams>0) {
                int pcr_pid = pmt[i]->pcr_pid;
                if (pcr_pid!=0x1FFF) {
                    if (pcr_prev[pcr_pid]<0)
                        rvmessage("%s: no pcr was seen for pcr_pid %d in program %d", filename, pcr_pid, i);
                    pcr_pids[number_pcrs++] = pcr_pid;
                }
            }
        }
        for (i=0; i<8192; i++) {
            if (pcr_prev[i]>=0) {
                for (j=0; j<number_pcrs; j++) {
                    if (pcr_pids[j]==i)
                        break;
                }
                if (j==number_pcrs)
                    rvmessage("%s: pcr seen on pid %d, but this is not a pcr_pid for any program", filename, i);
            }
        }
    }

    /* calculate average pcr_ac */
    if (verbose>=1) {
        if (pcr_ac_cnt>0) {
            long long pcr_ac_avg = (pcr_ac_sum+pcr_ac_cnt/2) / pcr_ac_cnt;
            rvmessage("%s: pcr_ac for pid %d: range [%.1f,%.1f]ns, mean %.1fns", filename, pcr_pid, pcr_to_ns(pcr_ac_min), pcr_to_ns(pcr_ac_max), pcr_to_ns(pcr_ac_avg));
        }
    }

    /* display video and audio dts statistics */
    if (verbose>=1) {
        for (i=0; i<8192; i++) {
            if (max_vid_aud_diff[i]>0)
                rvmessage("%s: audio pid %d: dts arrival time window [%.1f,%.1f]ms, max dts delta to video is %.1fms", filename, i,
                          pcr_to_ms(min_arrival[i]), pcr_to_ms(max_arrival[i]), pts_to_ms(max_vid_aud_diff[i]));
            if (i==vid_pid)
                rvmessage("%s: video pid %d: dts arrival time window [%.1f,%.1f]ms", filename, i, pcr_to_ms(min_arrival[i]), pcr_to_ms(max_arrival[i]));
        }
    }

    /* calculate average bitrate */
    if (verbose>=1 && first_pcr>=0 && last_pcr>=0 && last_pcr!=first_pcr) {
        double bitrate = (last_pcr_packet-first_pcr_packet) * 188.0*8.0;
        bitrate *= 27.0;
        bitrate /= last_pcr-first_pcr;
        rvmessage("%s: average transport stream bitrate is %.2fMbps (from pcr in pid %d)", filename, bitrate, pcr_pid);
    }

    if (verbose>=1)
        rvmessage("%s: checked %d packets", filename, packno);

    /* tidy up */
    if (sequence)
        free(sequence);

    return;
}

int main(int argc, char *argv[])
{
    const char *filename[RV_MAXFILES] = {0};
    int fileindex = 0;
    int numfiles = 0;

    /* command line defaults */
    char *forcetype = NULL;
    int checkpid = -1;
    int timing_margin = 2; /* safe range for pts from system time in ts streams in seconds */
    int noexit = 0;
    int verbose = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"type",     1, NULL, 't'},
            {"pid",      1, NULL, 'p'},
            {"margin",   1, NULL, 'm'},
            {"noexit",   1, NULL, 'n'},
            {"quiet",    0, NULL, 'q'},
            {"verbose",  0, NULL, 'v'},
            {"usage",    0, NULL, 'h'},
            {"help",     0, NULL, 'h'},
            {NULL,       0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "t:p:m:nqvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 't':
                forcetype = optarg;
                break;

            case 'p':
                checkpid = atoi(optarg);
                break;

            case 'm':
                timing_margin = atoi(optarg);

            case 'n':
                noexit = 1;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < sizeof(filename)/sizeof(filename[0]))
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    /* use filein if no input filenames */
    if (numfiles==0)
        filename[0] = "-";

    /* loop over input files */
    int status = 0;
    do {

        /* prepare to parse file */
        struct bitbuf *bb = initbits_filename(filename[fileindex], B_GETBITS);
        if (bb==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex]);

        /* determine filetype */
        divine_t divine = divine_filetype(bb, filename[fileindex], forcetype, verbose);
        if (rewindbits(bb)<0)
            rverror("searched too far on non-seekable input file \"%s\"", filename[fileindex]);

        /* select what to do based on filetype */
        switch (divine.filetype) {
            case M2V:
                status = check_m2v(bb, filename[fileindex], verbose);
                break;

            case H264:
                status = check_264(bb, filename[fileindex], noexit, verbose);
                break;

            case TS:
                check_ts(bb, filename[fileindex], checkpid, timing_margin, noexit, verbose);
                break;

            case EMPTY:
                rvexit("empty file: \"%s\"", filename[fileindex]);
                break;

            default:
                rvmessage("filetype of file \"%s\" is not supported: %s", filename[fileindex], filetypename[divine.filetype]);
                break;
        }

        /* tidy up */
        freebits(bb);

    } while (++fileindex<numfiles);

    /* tidy up */

    return status;
}
