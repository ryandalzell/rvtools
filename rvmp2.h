
#ifndef _RVMP2_H_
#define _RVMP2_H_

#include "rvbits.h"

/* transport stream limits */
#define MAX_PMTS            253
#define SECTION_BUFFER_SIZE 1024+184
#define MAX_PCR_VALUE (0x1ffffffffll*300ll + 299ll)

/* program stream start codes */
#define MPEG_PROGRAM_END_CODE       0xb9
#define PACK_HEADER                 0xba
#define SYSTEM_HEADER               0xbb

/* pes stream_id codes */
#define PROGRAM_STREAM_MAP          0xbc
#define PRIVATE_STREAM_1            0xbd
#define PADDING_STREAM              0xbe
#define PRIVATE_STREAM_2            0xbf
#define AUDIO_STREAM_X              0xc0 /* ISO/IEC 13818-3 or ISO/IEC 11172-3 audio stream number x xxxx */
#define VIDEO_STREAM_X              0xe0 /* ITU-T Rec. H.262 | ISO/IEC 13818-2 or ISO/IEC 11172-2 video stream number xxxx */
#define ECM_STREAM                  0xf0
#define EMM_STREAM                  0xf1
#define DSMCC_STREAM                0xf2 /* ITU-T Rec. H.222.0 | ISO/IEC 13818-1 Annex A or ISO/IEC 13818-6_DSMCC_stream */
#define ISO_13522_STREAM            0xf3 /* ISO/IEC_13522_stream */
#define TYPE_A_STREAM               0xf4 /* ITU-T Rec. H.222.1 type A */
#define TYPE_B_STREAM               0xf5 /* ITU-T Rec. H.222.1 type B */
#define TYPE_C_STREAM               0xf6 /* ITU-T Rec. H.222.1 type C */
#define TYPE_D_STREAM               0xf7 /* ITU-T Rec. H.222.1 type D */
#define TYPE_E_STREAM               0xf8 /* ITU-T Rec. H.222.1 type E */
#define ANCILLARY_STREAM            0xf9
#define PROGRAM_STREAM_DIRECTORY    0xff

/* trick mode control values */
#define FAST_FORWARD 0
#define SLOW_MOTION  1
#define FREEZE_FRAME 2
#define FAST_REVERSE 3
#define SLOW_REVERSE 4

/* transport stream types TODO make this an enum */
typedef int stream_type_t;
#define STREAM_TYPE_RESERVED       0x0
#define STREAM_TYPE_VIDEO_MPEG1    0x1
#define STREAM_TYPE_VIDEO_MPEG2    0x2
#define STREAM_TYPE_AUDIO_MPEG1    0x3
#define STREAM_TYPE_AUDIO_MPEG2    0x4
#define STREAM_TYPE_PRIVATE_DATA   0x6
#define STREAM_TYPE_AUDIO_AAC      0xF
#define STREAM_TYPE_VIDEO_MPEG4    0x10
#define STREAM_TYPE_AUDIO_LOAS     0x11
#define STREAM_TYPE_VIDEO_H264     0x1B
#define STREAM_TYPE_AUDIO_MPEG4_RAW 0x1C
#define STREAM_TYPE_VIDEO_H264SVC  0x1F
#define STREAM_TYPE_VIDEO_H264MVC  0x20
#define STREAM_TYPE_VIDEO_JPEG2000 0x21
#define STREAM_TYPE_VIDEO_HEVC     0x24
#define STREAM_TYPE_VIDEO_AVS      0x42
#define STREAM_TYPE_AUDIO_AC3      0x81
#define STREAM_TYPE_AUDIO_DTS_6CHAN 0x82
#define STREAM_TYPE_AUDIO_TRUEHD   0x83
//#define STREAM_TYPE_VIDEO_WMV      0x83
#define STREAM_TYPE_AUDIO_DTS_8CHAN 0x85
#define STREAM_TYPE_AUDIO_DTS      0x8A
#define STREAM_TYPE_VIDEO_DIRAC    0xD1
#define STREAM_TYPE_VIDEO_VC1      0xEA
#define STREAM_TYPE_MIXED          0xFF     // metatype for mixed type transport streams for 'rvinfo'.

/* program association entry */
struct pat_entry {
    int program_number;
    int program_map_pid;
};

/* program association table */
struct program_association_table {
    int transport_stream_id;
    int version_displayed;
    int version_number;
    int number_programs;
    struct pat_entry program[253];
    crc32_t crc32;
};

/* program map entry */
struct pmt_entry {
    stream_type_t stream_type;
    int elementary_pid;
    int format_identifier;
};

/* program map table */
struct program_map_table {
    int version_displayed;
    int program_number;
    int version_number;
    int pcr_pid;
    int number_streams;
    struct pmt_entry stream[201];
    crc32_t crc32;
};

/* timing functions */
double pts_to_sec(long long pts);
double pts_to_ms(long long pts);
double pcr_to_sec(long long pcr);
double pcr_to_ms(long long pcr);
double pcr_to_ns(long long pcr);

/* transport stream functions*/
int find_next_sync_code(struct bitbuf *bb);
int read_transport_packet(struct bitbuf *bb, int verbose, unsigned char *packet);
int parse_adaptation_field(struct bitbuf *bb, int verbose, int packnum, int *discontinuity_indicator, int *pcr_flag, long long *pcr);
int parse_transport_packet_header(struct bitbuf *bb, int verbose, int packnum, int *transport_error_indicator,
                                  int *payload_unit_start_indicator, int *pid, int *adaptation_field_control, int *continuity_counter,
                                  int *discontinuity_indicator, int *pcr_flag, long long *pcr);
int parse_pointer_field(struct bitbuf *bb);
void describe_program_association(struct program_association_table *pat);
int parse_program_association_section(struct bitbuf *bb, int data_length, int verbose, struct program_association_table *pat);
extern const char *name_stream_type(stream_type_t stream_type, fourcc_t format_identifier);
extern const char *describe_stream_type(stream_type_t stream_type, int format_identifier);
void describe_program_map(struct program_map_table *pmt);
int parse_program_map_section(struct bitbuf *bb, int data_length, int verbose, struct program_map_table *pmt);
int parse_system_header(struct bitbuf *bb, int verbose);
int parse_pack_header(struct bitbuf *bb, int verbose);
int parse_pes_packet_header(struct bitbuf *bb, int stream_id, int PES_packet_length, long long *pts, long long *dts, int verbose);
void describe_pes_packet(int stream_id, int pes_packet_length);

int put_program_association_section(struct bitbuf *bb, int num_programs, int pat[][2]);
int put_program_map_section(struct bitbuf *bb, int program_number, int pcr_pid, int num_program_elements, int pmt[][2]);
int put_pes_packet(struct bitbuf *bb, int stream_id, int payload_start, long long pts, long long dts, unsigned char *es_data, int es_data_length);
int put_pes_packet_header(struct bitbuf *bb, int stream_id, int payload_start, long long pts, long long dts, int es_data_length);
int put_adaptation_field(struct bitbuf *bb, int required_field_length, int pcr_flag, long long pcr);
int put_ts_packet_pes_start(struct bitbuf *bb, int pid, int continuity_counter, int pcr_flag, long long pcr, int stream_number, long long pts, long long dts, unsigned char *pes_data, int pes_data_length);
int put_ts_packet_pes_data(struct bitbuf *bb, int pid, int continuity_counter, int pcr_flag, long long pcr, unsigned char *pes_data, int pes_data_length);
int put_ts_packet_psi(struct bitbuf *bb, int pid, int payload_start, int continuity_counter, unsigned char *psi_data, int psi_data_length);
void put_ts_packet_pcr(struct bitbuf *bb, int pid, int continuity_counter, long long pcr);
void put_ts_packet_null(struct bitbuf *bb);
int put_section(struct bitbuf *bb, int pid, int continuity_counter, unsigned char *section, int section_length);

#endif
