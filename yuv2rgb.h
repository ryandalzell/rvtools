
#ifndef _YUV2RGB_H_
#define _YUV2RGB_H_

/* yuv2rgb.c */
void InitColorDither(Display *display, Window window);
void Color16DitherImage(unsigned char *src[], unsigned char *out, int width, int height, int expand);
void YUVplanar_RGB32(unsigned char *src[], unsigned char *out, int width, int height, int expand);

void YUV422_RGB32(unsigned char *src[], unsigned char *dst, int width, int height, int expand);
void UYVY_RGB32(unsigned char *src, unsigned char *dst, int width, int height, int expand);
void YUY2_RGB32(unsigned char *src, unsigned char *dst, int width, int height, int expand);
void VYUY_RGB32(unsigned char *src, unsigned char *dst, int width, int height, int expand);
void YUVplanar444_RGB32(unsigned char *src[], unsigned char *dst, int width, int height, int expand, int bpp);
void RGB24_RGB32(unsigned char *src, unsigned char *dst, int width, int height, int expand, int bpp);
void RGBplanar_RGB32(unsigned char *src[], unsigned char *dst, int width, int height, int expand, int bpp);
void V210_RGB32(unsigned char *src, unsigned char *dst, int width, int height, int expand);
void YU15_RGB32(unsigned char *src[], unsigned char *dst, int width, int height, int expand);
void YU20_RGB32(unsigned char *src[], unsigned char *dst, int width, int height, int expand);
void NV12_RGB32(unsigned char *src[], unsigned char *dst, int width, int height, int expand);
void NV16_RGB32(unsigned char *src[], unsigned char *dst, int width, int height, int expand);
void NV20_RGB32(unsigned char *src[], unsigned char *dst, int width, int height, int expand);
void XV20_RGB32(unsigned char *src[], unsigned char *dst, int width, int height, int expand);

#endif
