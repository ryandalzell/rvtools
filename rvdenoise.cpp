/*
 * Description: Remove noise from raw video files.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-06-23 13:26:19 $
 * Revision   : $Revision: 1.25 $
 * Copyright  : (c) 2005,2011,2012 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <math.h>
#include <float.h>
#include "getopt.h"

#include "rvutil.h"

void ksvd_ipol(const double sigma, unsigned char *src, unsigned char *dst, const unsigned width, const unsigned height, const unsigned chnls, const int useT);

typedef enum {
    UNITY,
    MEAN3,
    MEAN3x3,
    MEAN1x3,
    MEAN1x1x3,
    MEAN3x3x3,  /* temporal */
    EDGE3x3,
    MEDIAN3,
    MEDIAN1x3,
    MEDIAN1x1x3,/* temporal */
    MEDIAN5,
    MEDIAN7,
    MEDIAN3x3,
    MEDIAN5x5,
    HYBRID5x5,
    NLMEANS,
    KSVD,
} denoise_t;

static const struct {
    const char *name;
    denoise_t algo;
} algos[] = {
    { "unity"      , UNITY       },
    { "mean3"      , MEAN3       },
    { "mean1x3"    , MEAN1x3     },
    { "mean1x1x3"  , MEAN1x1x3   },
    { "mean3x3"    , MEAN3x3     },
    { "mean3x3x3"  , MEAN3x3x3   },
    { "edge3x3"    , EDGE3x3     },
    { "median3"    , MEDIAN3     },
    { "median1x3"  , MEDIAN1x3   },
    { "median1x1x3", MEDIAN1x1x3 },
    { "median5"    , MEDIAN5     },
    { "median7"    , MEDIAN7     },
    { "median3x3"  , MEDIAN3x3   },
    { "median5x5"  , MEDIAN5x5   },
    { "hybrid5x5"  , HYBRID5x5   },
    { "nlmeans"    , NLMEANS     },
    { "ksvd"       , KSVD        },
    { "k-svd"      , KSVD        },
    { NULL         , UNITY       },
};

const char *appname;

void usage(int exitcode)
{
    /* prepare list of deinterlacing algorithms */
    int i, len = 0;
    char s[256];
    for (i=0; algos[i].name; i++) {
        if (i==0)
            len = snprintf(s, sizeof(s), "%s", algos[i].name);
        else
            len += snprintf(s+len, sizeof(s)-len, ", %s", algos[i].name);
    }
    fprintf(stderr, "%s: remove noise from raw video files\n", appname);
    fprintf(stderr, "usage: %s [options] [<file>] [<file>...]\n", appname);
    fprintf(stderr, "  -a, --algorithm     : denoise algorithm to use: %s (default: discard)\n", s);
    fprintf(stderr, "                      : (dimensions are: horiz x vert x temporal)\n");
    fprintf(stderr, "  -s, --size          : image size format\n");
    fprintf(stderr, "     (formats : %s)\n", rvformats);
    fprintf(stderr, "  -p, --fourcc        : image pixel format fourcc (default: I420)\n");
    fprintf(stderr, "     (formats : %s)\n", rvfourccs);
    fprintf(stderr, "  -n, --numframes     : number of frames (default: all)\n");
    fprintf(stderr, "  -o, --output        : write output to file\n");
    fprintf(stderr, "  -q, --quiet         : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "  -v, --verbose       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "  --                  : disable argument processing\n");
    fprintf(stderr, "  -h, --help, --usage : print this usage message\n");
    exit(exitcode);
}

typedef unsigned char pixelvalue;
#define min_pixelvalue 0
#define max_pixelvalue 255

pixelvalue mean3(pixelvalue a, pixelvalue b, pixelvalue c)
{
    // FIXME rounding.
    int sum = a+b+c+1;
    return sum/3;
}

pixelvalue mean3x3(pixelvalue a, pixelvalue b, pixelvalue c, pixelvalue d, pixelvalue e, pixelvalue f, pixelvalue g, pixelvalue h, pixelvalue i)
{
    // FIXME rounding.
    int sum = a+b+c+d+e+f+g+h+i+4;
    return sum/9;
}

pixelvalue mean3x3x3(pixelvalue *a, pixelvalue *b, pixelvalue *c, pixelvalue *d, pixelvalue *e, pixelvalue *f, pixelvalue *g, pixelvalue *h, pixelvalue *i)
{
    // FIXME rounding.
    int sum = a[-1]+a[0]+a[1]+b[-1]+b[0]+b[1]+c[-1]+c[0]+c[1]+
              d[-1]+d[0]+d[1]+e[-1]+e[0]+e[1]+f[-1]+f[0]+f[1]+
              g[-1]+g[0]+g[1]+h[-1]+h[0]+h[1]+i[-1]+i[0]+i[1]+13;
    return sum/27;
}

pixelvalue edge3x3(pixelvalue a, pixelvalue b, pixelvalue c, pixelvalue d, pixelvalue e, pixelvalue f, pixelvalue g, pixelvalue h, pixelvalue i)
{
    int conv = 0*a-1*b+0*c-1*d+5*e-1*f+0*g-1*h+0*i;
    /* this assumes the type pixelvalue is unsigned */
    return conv>max_pixelvalue? max_pixelvalue : (pixelvalue)conv;
}

/* begin optimised median functions from N. Devillard */

/*
 * The following routines have been built from knowledge gathered
 * around the Web. I am not aware of any copyright problem with
 * them, so use it as you want.
 * N. Devillard - 1998
 */

#define PIX_SORT(a,b) { if ((a)>(b)) { pixelvalue t=(a); (a)=(b); (b)=t; } }

/*----------------------------------------------------------------------------
   Function :   median3()
   In       :   pointer to array of 3 pixel values
   Out      :   a pixelvalue
   Job      :   optimized search of the median of 3 pixel values
   Notice   :   found on sci.image.processing
                cannot go faster unless assumptions are made
                on the nature of the input signal.
 ---------------------------------------------------------------------------*/

pixelvalue median3(pixelvalue a, pixelvalue b, pixelvalue c)
{
    pixelvalue p[3] = {a, b, c};
    PIX_SORT(p[0],p[1]); PIX_SORT(p[1],p[2]); PIX_SORT(p[0],p[1]);
    return p[1];
}

/*----------------------------------------------------------------------------
   Function :   median5()
   In       :   pointer to array of 5 pixel values
   Out      :   a pixelvalue
   Job      :   optimized search of the median of 5 pixel values
   Notice   :   found on sci.image.processing
                cannot go faster unless assumptions are made
                on the nature of the input signal.
 ---------------------------------------------------------------------------*/

pixelvalue median5(pixelvalue a, pixelvalue b, pixelvalue c, pixelvalue d, pixelvalue e)
{
    pixelvalue p[5] = {a, b, c, d, e};
    PIX_SORT(p[0],p[1]); PIX_SORT(p[3],p[4]); PIX_SORT(p[0],p[3]);
    PIX_SORT(p[1],p[4]); PIX_SORT(p[1],p[2]); PIX_SORT(p[2],p[3]);
    PIX_SORT(p[1],p[2]);
    return p[2];
}

/*----------------------------------------------------------------------------
   Function :   opt_med6()
   In       :   pointer to array of 6 pixel values
   Out      :   a pixelvalue
   Job      :   optimized search of the median of 6 pixel values
   Notice   :   from Christoph_John@gmx.de
                based on a selection network which was proposed in
                "FAST, EFFICIENT MEDIAN FILTERS WITH EVEN LENGTH WINDOWS"
                J.P. HAVLICEK, K.A. SAKADY, G.R.KATZ
                If you need larger even length kernels check the paper
 ---------------------------------------------------------------------------*/

pixelvalue opt_med6(pixelvalue * p)
{
    PIX_SORT(p[1], p[2]); PIX_SORT(p[3],p[4]);
    PIX_SORT(p[0], p[1]); PIX_SORT(p[2],p[3]); PIX_SORT(p[4],p[5]);
    PIX_SORT(p[1], p[2]); PIX_SORT(p[3],p[4]);
    PIX_SORT(p[0], p[1]); PIX_SORT(p[2],p[3]); PIX_SORT(p[4],p[5]);
    PIX_SORT(p[1], p[2]); PIX_SORT(p[3],p[4]);
    return ( p[2] + p[3] ) * 0.5;
    /* PIX_SORT(p[2], p[3]) results in lower median in p[2] and upper median in p[3] */
}

/*----------------------------------------------------------------------------
   Function :   median7()
   In       :   pointer to array of 7 pixel values
   Out      :   a pixelvalue
   Job      :   optimized search of the median of 7 pixel values
   Notice   :   found on sci.image.processing
                cannot go faster unless assumptions are made
                on the nature of the input signal.
 ---------------------------------------------------------------------------*/

pixelvalue median7(pixelvalue a, pixelvalue b, pixelvalue c, pixelvalue d, pixelvalue e, pixelvalue f, pixelvalue g)
{
    pixelvalue p[7] = {a, b, c, d, e, f, g};
    PIX_SORT(p[0], p[5]); PIX_SORT(p[0], p[3]); PIX_SORT(p[1], p[6]);
    PIX_SORT(p[2], p[4]); PIX_SORT(p[0], p[1]); PIX_SORT(p[3], p[5]);
    PIX_SORT(p[2], p[6]); PIX_SORT(p[2], p[3]); PIX_SORT(p[3], p[6]);
    PIX_SORT(p[4], p[5]); PIX_SORT(p[1], p[4]); PIX_SORT(p[1], p[3]);
    PIX_SORT(p[3], p[4]);
    return p[3];
}

/*----------------------------------------------------------------------------
   Function :   median9()
   In       :   pointer to an array of 9 pixelvalues
   Out      :   a pixelvalue
   Job      :   optimized search of the median of 9 pixelvalues
   Notice   :   in theory, cannot go faster without assumptions on the
                signal.
                Formula from:
                XILINX XCELL magazine, vol. 23 by John L. Smith

                The input array is modified in the process
                The result array is guaranteed to contain the median
                value
                in middle position, but other elements are NOT sorted.
 ---------------------------------------------------------------------------*/

pixelvalue median9(pixelvalue a, pixelvalue b, pixelvalue c, pixelvalue d, pixelvalue e, pixelvalue f, pixelvalue g, pixelvalue h, pixelvalue i)
{
    pixelvalue p[9] = {a, b, c, d, e, f, g, h, i};
    PIX_SORT(p[1], p[2]); PIX_SORT(p[4], p[5]); PIX_SORT(p[7], p[8]);
    PIX_SORT(p[0], p[1]); PIX_SORT(p[3], p[4]); PIX_SORT(p[6], p[7]);
    PIX_SORT(p[1], p[2]); PIX_SORT(p[4], p[5]); PIX_SORT(p[7], p[8]);
    PIX_SORT(p[0], p[3]); PIX_SORT(p[5], p[8]); PIX_SORT(p[4], p[7]);
    PIX_SORT(p[3], p[6]); PIX_SORT(p[1], p[4]); PIX_SORT(p[2], p[5]);
    PIX_SORT(p[4], p[7]); PIX_SORT(p[4], p[2]); PIX_SORT(p[6], p[4]);
    PIX_SORT(p[4], p[2]);
    return p[4];
}

/*----------------------------------------------------------------------------
   Function :   median25()
   In       :   pointer to an array of 25 pixelvalues
   Out      :   a pixelvalue
   Job      :   optimized search of the median of 25 pixelvalues
   Notice   :   in theory, cannot go faster without assumptions on the
                signal.
                Code taken from Graphic Gems.
 ---------------------------------------------------------------------------*/

pixelvalue median25(pixelvalue a, pixelvalue b, pixelvalue c, pixelvalue d, pixelvalue e, pixelvalue f, pixelvalue g, pixelvalue h, pixelvalue i, pixelvalue j, pixelvalue k, pixelvalue l, pixelvalue m, pixelvalue n, pixelvalue o, pixelvalue q, pixelvalue r, pixelvalue s, pixelvalue t, pixelvalue u, pixelvalue v, pixelvalue w, pixelvalue x, pixelvalue y, pixelvalue z)
{
    pixelvalue p[25] = {a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, q, r, s, t, u, v, w, x, y, z};

    PIX_SORT(p[0], p[1]) ;   PIX_SORT(p[3], p[4]) ;   PIX_SORT(p[2], p[4]) ;
    PIX_SORT(p[2], p[3]) ;   PIX_SORT(p[6], p[7]) ;   PIX_SORT(p[5], p[7]) ;
    PIX_SORT(p[5], p[6]) ;   PIX_SORT(p[9], p[10]) ;  PIX_SORT(p[8], p[10]) ;
    PIX_SORT(p[8], p[9]) ;   PIX_SORT(p[12], p[13]) ; PIX_SORT(p[11], p[13]) ;
    PIX_SORT(p[11], p[12]) ; PIX_SORT(p[15], p[16]) ; PIX_SORT(p[14], p[16]) ;
    PIX_SORT(p[14], p[15]) ; PIX_SORT(p[18], p[19]) ; PIX_SORT(p[17], p[19]) ;
    PIX_SORT(p[17], p[18]) ; PIX_SORT(p[21], p[22]) ; PIX_SORT(p[20], p[22]) ;
    PIX_SORT(p[20], p[21]) ; PIX_SORT(p[23], p[24]) ; PIX_SORT(p[2], p[5]) ;
    PIX_SORT(p[3], p[6]) ;   PIX_SORT(p[0], p[6]) ;   PIX_SORT(p[0], p[3]) ;
    PIX_SORT(p[4], p[7]) ;   PIX_SORT(p[1], p[7]) ;   PIX_SORT(p[1], p[4]) ;
    PIX_SORT(p[11], p[14]) ; PIX_SORT(p[8], p[14]) ;  PIX_SORT(p[8], p[11]) ;
    PIX_SORT(p[12], p[15]) ; PIX_SORT(p[9], p[15]) ;  PIX_SORT(p[9], p[12]) ;
    PIX_SORT(p[13], p[16]) ; PIX_SORT(p[10], p[16]) ; PIX_SORT(p[10], p[13]) ;
    PIX_SORT(p[20], p[23]) ; PIX_SORT(p[17], p[23]) ; PIX_SORT(p[17], p[20]) ;
    PIX_SORT(p[21], p[24]) ; PIX_SORT(p[18], p[24]) ; PIX_SORT(p[18], p[21]) ;
    PIX_SORT(p[19], p[22]) ; PIX_SORT(p[8], p[17]) ;  PIX_SORT(p[9], p[18]) ;
    PIX_SORT(p[0], p[18]) ;  PIX_SORT(p[0], p[9]) ;   PIX_SORT(p[10], p[19]) ;
    PIX_SORT(p[1], p[19]) ;  PIX_SORT(p[1], p[10]) ;  PIX_SORT(p[11], p[20]) ;
    PIX_SORT(p[2], p[20]) ;  PIX_SORT(p[2], p[11]) ;  PIX_SORT(p[12], p[21]) ;
    PIX_SORT(p[3], p[21]) ;  PIX_SORT(p[3], p[12]) ;  PIX_SORT(p[13], p[22]) ;
    PIX_SORT(p[4], p[22]) ;  PIX_SORT(p[4], p[13]) ;  PIX_SORT(p[14], p[23]) ;
    PIX_SORT(p[5], p[23]) ;  PIX_SORT(p[5], p[14]) ;  PIX_SORT(p[15], p[24]) ;
    PIX_SORT(p[6], p[24]) ;  PIX_SORT(p[6], p[15]) ;  PIX_SORT(p[7], p[16]) ;
    PIX_SORT(p[7], p[19]) ;  PIX_SORT(p[13], p[21]) ; PIX_SORT(p[15], p[23]) ;
    PIX_SORT(p[7], p[13]) ;  PIX_SORT(p[7], p[15]) ;  PIX_SORT(p[1], p[9]) ;
    PIX_SORT(p[3], p[11]) ;  PIX_SORT(p[5], p[17]) ;  PIX_SORT(p[11], p[17]) ;
    PIX_SORT(p[9], p[17]) ;  PIX_SORT(p[4], p[10]) ;  PIX_SORT(p[6], p[12]) ;
    PIX_SORT(p[7], p[14]) ;  PIX_SORT(p[4], p[6]) ;   PIX_SORT(p[4], p[7]) ;
    PIX_SORT(p[12], p[14]) ; PIX_SORT(p[10], p[14]) ; PIX_SORT(p[6], p[7]) ;
    PIX_SORT(p[10], p[12]) ; PIX_SORT(p[6], p[10]) ;  PIX_SORT(p[6], p[17]) ;
    PIX_SORT(p[12], p[17]) ; PIX_SORT(p[7], p[17]) ;  PIX_SORT(p[7], p[10]) ;
    PIX_SORT(p[12], p[18]) ; PIX_SORT(p[7], p[12]) ;  PIX_SORT(p[10], p[18]) ;
    PIX_SORT(p[12], p[20]) ; PIX_SORT(p[10], p[20]) ; PIX_SORT(p[10], p[12]) ;

    return (p[12]);
}



#undef PIX_SORT
#undef PIX_SWAP

/* end optimised median */
pixelvalue median5x5hybrid(pixelvalue a, pixelvalue b, pixelvalue c, pixelvalue d, pixelvalue e, pixelvalue f, pixelvalue g, pixelvalue h, pixelvalue i, pixelvalue j, pixelvalue k, pixelvalue l, pixelvalue m, pixelvalue n, pixelvalue o, pixelvalue q, pixelvalue r, pixelvalue s, pixelvalue t, pixelvalue u, pixelvalue v, pixelvalue w, pixelvalue x, pixelvalue y, pixelvalue z)
{
    pixelvalue sub1 = median9(a, e, g, i, m, q, s, u, y);
    pixelvalue sub2 = median9(c, h, k, l, m, n, o, r, w);
    return median3(sub1, m, sub2);
}

/* Copyright (c) 200? Andy Ray */

/* May be linked to the EP Patent:
 * A. Buades, T. Coll and J.M. Morel, Image data processing method by
 * reducing image noise, and camera integrating means for implementing
 * said method, EP Patent 1,749,278 (Feb. 7, 2007).
 */

/*
 *    These set the x-axis, y-axis, and z-axis radii of the search window.  These must be
 *    greater than or equal to 0.  The full window size will be:
 *
 *         (Ax*2+1) x (Ay*2+1) x (Az*2+1)
 *
 *    Generally, the larger the search window the better the result of the denoising. Of
 *    course, the larger the search window the longer the denoising takes.
 *
 *  NOTE: z-axis not supported
 *  NOTE: The original paper suggests a size of 21x21 which means Ax=Ay=10
 *
 */
int Ax=2, Ay=2;
/*
 *    These set the x-axis and y-axis radii of the support (similarity neighborhood) window.
 *    These must be greater than or equal to 0.  A larger similarity window will retain more
 *    detail/texture but will also cause less noise removal.  Typical values for Sx/Sy are 2
 *    or 3.  The full window size will be:
 *
 *         (Sx*2+1) x (Sy*2+1)
 *
 *  NOTE: The original paper suggests a size of 7x7 which means Sx=Sy=3
 */
int Sx=3, Sy=3;
/*
 *    Sets the standard deviation of the gaussian used for weighting the difference calculation
 *    used for computing neighborhood similarity.  Smaller values will result in less noise removal
 *    but will retain more detail/texture.
 *
 *    Default:  1.0 (float)
 */
double a = 1.0;
/*
 *    Controls the strength of the filtering (blurring).  Larger values will remove more noise
 *    but will also destroy more detail. 'h' should typically be set equal to the standard deviation
 *    of the noise in the image assuming the noise fits the zero mean, gaussian model.
 */
double h = 1.8;

/* nlmeans globals */
double *gw;
double *sums, *weights, *wmaxs;

void nlmeans_init(int width, int height)
{
    const double a2 = a * a;

    /* allocate memory for nlmeans filter */
    gw      = (double *)malloc(sizeof(double) * ((2*Sx)+1) * ((2*Sy)+1));
    sums    = (double *)malloc(sizeof(double) * width * height);
    weights = (double *)malloc(sizeof(double) * width * height);
    wmaxs   = (double *)malloc(sizeof(double) * width * height);

    /* fill in exp lookup */
    int j, k, m, n, w=0;
    for (j=-Sy; j<=Sy; ++j) {
        if (j < 0)
            m = mmin(j,0);
        else
            m = mmax(j,0);
        for (k=-Sx; k<=Sx; ++k) {
            if (k < 0)
                n = mmin(k,0);
            else
                n = mmax(k,0);
            gw[w++] = exp(-((m*m+n*n)/(2*a2)));
        }
    }
}

void nlmeans(unsigned char *src, int width, int height, int pitch, FILE *fileout)
{
    /* filter parameters */
    const int Sxd = (2*Sx)+1;
    //const int Syd = (2*Sy)+1;
    //const int Axd = (2*Ax)+1;
    //const int Ayd = (2*Ay)+1;
    const double h2in = -1.0 / (h * h);

    const int heightm1 = height-1;
    const int widthm1 = width-1;

    unsigned char *pfp = src;
    int x, y, u, v, j, k;

    memset(sums,0,height*width*sizeof(double));
    memset(weights,0,height*width*sizeof(double));
    memset(wmaxs,0,height*width*sizeof(double));

    /* loop over pixels */
    for (y=0; y<height; ++y)
    {
        const int stopy = mmin(y+Ay,heightm1);
        const int doffy = y*width;
        for (x=0; x<width; ++x)
        {
            const int startxt = mmax(x-Ax,0);
            const int stopx = mmin(x+Ax,widthm1);
            const int doff = doffy+x;
            double *dsum = &sums[doff];
            double *dweight = &weights[doff];
            double *dwmax = &wmaxs[doff];
            // loop over search window
            for (u=y; u<=stopy; ++u)
            {
                const int startx = u == y ? x+1 : startxt;
                const int yT = -mmin(mmin(Sy,u),y);
                const int yB = mmin(mmin(Sy,heightm1-u),heightm1-y);
                const unsigned char *s1_saved = pfp + (u+yT)*pitch;
                const unsigned char *s2_saved = pfp + (y+yT)*pitch + x;
                const double *gw_saved = gw+(yT+Sy)*Sxd+Sx;
                const int pfpl = u*pitch;
                const int coffy = u*width;
                for (v=startx; v<=stopx; ++v)
                {
                    const int coff = coffy+v;
                    double *csum = &sums[coff];
                    double *cweight = &weights[coff];
                    double *cwmax = &wmaxs[coff];
                    const int xL = -mmin(mmin(Sx,v),x);
                    const int xR = mmin(mmin(Sx,widthm1-v),widthm1-x);
                    const unsigned char *s1 = s1_saved + v;
                    const unsigned char *s2 = s2_saved;
                    const double *gwT = gw_saved;
                    double diff = 0.0, gweights = 0.0;

                    // loop over block
                    for (j=yT; j<=yB; ++j)
                    {
                        for (k=xL; k<=xR; ++k)
                        {
                            diff += (s1[k]-s2[k])*(s1[k]-s2[k])*gwT[k];
                            gweights += gwT[k];
                        }
                        s1 += pitch;
                        s2 += pitch;
                        gwT += Sxd;
                    }
                    const double weight = exp((diff/gweights)*h2in);
                    *cweight += weight;
                    *dweight += weight;
                    *csum += src[x]*weight;
                    *dsum += pfp[pfpl+v]*weight;
                    if (weight > *cwmax) *cwmax = weight;
                    if (weight > *dwmax) *dwmax = weight;
                }
            }
            const double wmax = *dwmax <= DBL_EPSILON ? 1.0 : *dwmax;
            *dsum += src[x]*wmax;
            *dweight += wmax;
            fputc (mmax(mmin((int)(((*dsum)/(*dweight))+0.5),255),0), fileout);
        }
        src += pitch;
    }
}

void nlmeans_free()
{
    if (wmaxs) free(wmaxs);
    if (weights) free(weights);
    if (sums) free(sums);
    if (gw) free(gw);
}

int main(int argc, char *argv[])
{
    FILE *filein = stdin;
    FILE *fileout = stdout;
    char *filename[RV_MAXFILES] = {0};
    char *outfile = NULL;
    int fileindex = 0;
    int numfiles = 0;
    int totframes = 0;

    /* data buffers - previous, current and next */
    int size = 0;
    unsigned char *data[3] = {NULL, NULL, NULL};

    /* command line defaults */
    denoise_t algorithm = UNITY;
    const char *format = NULL;
    const char *fccode = NULL;
    int numframes = -1;
    int verbose = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    int i;
    while (1) {
        static struct option long_options[] = {
            {"algorithm", 1, NULL, 'a'},
            {"size",      1, NULL, 's'},
            {"fourcc",    1, NULL, 'p'},
            {"numframes", 1, NULL, 'n'},
            {"output",    1, NULL, 'o'},
            {"quiet",     0, NULL, 'q'},
            {"verbose",   0, NULL, 'v'},
            {"usage",     0, NULL, 'h'},
            {"help",      0, NULL, 'h'},
            {NULL,        0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "a:s:p:n:o:qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 'a':
                for (i=0; ; i++) {
                    if (algos[i].name==NULL)
                        rvexit("unknown denoising algorithm: %s", optarg);
                    if (strcasecmp(optarg, algos[i].name)==0) {
                        algorithm = algos[i].algo;
                        break;
                    }
                }
                break;

            case 's':
                format = optarg;
                break;

            case 'p':
                fccode = optarg;
                break;

            case 'n':
                numframes = atoi(optarg);
                if (numframes<=0)
                    rvexit("invalid value for numframes: %d", numframes);
                break;

            case 'o':
                outfile = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < sizeof(filename)/sizeof(filename[0]))
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    /* open output file */
    if (outfile)
        fileout = fopen(outfile, "wb");
    if (fileout==NULL)
        rverror("failed to open output file \"%s\"", outfile);

    /* ignore SIGPIPE on output file */
    if (signal(SIGPIPE, SIG_IGN)==SIG_ERR)
        rverror("failed to set ignore on SIGPIPE");

    if (verbose>=2)
        rvmessage("denoising with algorithm \"%s\"", algos[algorithm].name);

    /* loop over input files */
    do {
        int width;
        int height;
        framerate_t framerate;
        fourcc_t fourcc;

        /* determine input file dimensions */
        divine_image_dims(&width, &height, NULL, &framerate, &fourcc, format, filename[fileindex]);

        /* override fourcc */
        if (fccode)
            fourcc = get_fourcc(fccode);

        /* get chroma subsampling from fourcc */
        int hsub, vsub;
        get_chromasub(fourcc, &hsub, &vsub);
        if (hsub==-1 || vsub==-1)
            rvexit("unknown pixel format: %s", fourccname(fourcc));
        int hsubs[3] = {1, hsub, hsub};
        int vsubs[3] = {1, vsub, vsub};

        /* (re-)allocate data buffers */
        size = get_framesize(width, height, hsub, vsub, 8);
        data[0] = (unsigned char *)rvalloc(data[0], size*sizeof(unsigned char), 0);
        data[1] = (unsigned char *)rvalloc(data[1], size*sizeof(unsigned char), 0);
        data[2] = (unsigned char *)rvalloc(data[2], size*sizeof(unsigned char), 0);

        /* open next input file, or use stdin */
        if (numfiles)
            filein = fopen(filename[fileindex], "rb");
        if (filein==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex]);

        /* pre-load first frame */
        int read = fread(data[0], size, 1, filein);
        if (read!=1)
            rverror("failed from read first frame from file \"%s\"", filename[fileindex]);

        /* first pass in loop will have past frame equal to current */
        memcpy(data[1], data[0], size);

        /* initialise algorithm */
        switch (algorithm) {
            case NLMEANS:
                nlmeans_init(width, height);
                break;

            default:
                break;
        }

        /* loop over frames in input file */
        int eof = 0;
        int frameno;
        for (frameno=0; !eof && (totframes<numframes || numframes<0) && !feof(filein) && !ferror(filein) && !ferror(fileout); frameno++)
        {
            /* shuffle frame pointers */
            unsigned char *swap = data[2];
            data[2] = data[1]; /* past frame */
            data[1] = data[0]; /* current frame */
            data[0] = swap;

            /* read next frame */
            read = fread(data[0], size, 1, filein); /* future frame */
            if (read!=1) {
                /* last pass in loop with have future frame equal to current */
                memcpy(data[0], data[1], size);
                eof = 1;
            }

            /* prepare pointers to chroma planes */
            unsigned char *past[3]    = {data[2], data[2]+width*height, data[2]+width*height+width/hsub*height/vsub};
            unsigned char *current[3] = {data[1], data[1]+width*height, data[1]+width*height+width/hsub*height/vsub};
            unsigned char *future[3]  = {data[0], data[0]+width*height, data[0]+width*height+width/hsub*height/vsub};

            int c, j;
            switch (algorithm) {
                case UNITY: /* ok, so this does nothing */
                    if (fwrite(current[0], size, 1, fileout) == 1)
                        totframes++;
                    break;

                case MEAN3:
                    for (c=0; c<3; c++) {
                        for (j=0; j<height/vsubs[c]; j++) {
                            unsigned char *p = current[c] + j*width/hsubs[c];
                            fputc(mean3(*p, *p, *(p+1)), fileout);
                            for (i=1, p++; i<width/hsubs[c]-1; i++, p++)
                                fputc(mean3(*(p-1), *p, *(p+1)), fileout);
                            fputc(mean3(*(p-1), *p, *p), fileout);
                        }
                    }
                    totframes++;
                    break;

                case MEAN1x3:
                    for (c=0; c<3; c++) {
                        for (j=0; j<height/vsubs[c]; j++) {
                            unsigned char *o = current[c] + mmax((j-1),0)                *width/hsubs[c];
                            unsigned char *p = current[c] +       j                      *width/hsubs[c];
                            unsigned char *q = current[c] + mmin((j+1),height/vsubs[c]-1)*width/hsubs[c];
                            for (i=0; i<width/hsubs[c]; i++, o++, p++, q++)
                                fputc(mean3(*o, *p, *q), fileout);
                        }
                    }
                    totframes++;
                    break;

                case MEAN1x1x3:
                    for (c=0; c<3; c++) {
                        for (j=0; j<height/vsubs[c]; j++) {
                            for (i=0; i<width/hsubs[c]; i++) {
                                int k = j*width/hsubs[c]+i;
                                fputc(mean3(past[c][k], current[c][k], future[c][k]), fileout);
                            }
                        }
                    }
                    totframes++;
                    break;

                case MEAN3x3:
                    for (c=0; c<3; c++) {
                        for (j=0; j<height/vsubs[c]; j++) {
                            unsigned char *o = current[c] + mmax((j-1),0)                *width/hsubs[c];
                            unsigned char *p = current[c] +       j                      *width/hsubs[c];
                            unsigned char *q = current[c] + mmin((j+1),height/vsubs[c]-1)*width/hsubs[c];
                            fputc(mean3x3(*o, *o, *(o+1),
                                          *p, *p, *(p+1),
                                          *q, *q, *(q+1)), fileout);
                            for (i=1, o++, p++, q++; i<width/hsubs[c]-1; i++, o++, p++, q++)
                                fputc(mean3x3(*(o-1), *o, *(o+1),
                                              *(p-1), *p, *(p+1),
                                              *(q-1), *q, *(q+1)), fileout);
                            fputc(mean3x3(*(o-1), *o, *o,
                                          *(p-1), *p, *p,
                                          *(q-1), *q, *q), fileout);
                        }
                    }
                    totframes++;
                    break;

                case MEAN3x3x3:
                    for (c=0; c<3; c++) {
                        for (i=0; i<width/hsubs[c]; i++)
                            fputc(current[c][i], fileout);
                        for (j=1; j<height/vsubs[c]-1; j++) {
                            unsigned char *l = past[c]    + (j-1)*width/hsubs[c];
                            unsigned char *m = past[c]    +  j   *width/hsubs[c];
                            unsigned char *n = past[c]    + (j+1)*width/hsubs[c];
                            unsigned char *o = current[c] + (j-1)*width/hsubs[c];
                            unsigned char *p = current[c] +  j   *width/hsubs[c];
                            unsigned char *q = current[c] + (j+1)*width/hsubs[c];
                            unsigned char *r = future[c]  + (j-1)*width/hsubs[c];
                            unsigned char *s = future[c]  +  j   *width/hsubs[c];
                            unsigned char *t = future[c]  + (j+1)*width/hsubs[c];
                            fputc(*p, fileout);
                            for (i=1, l++, m++, n++, o++, p++, q++, r++, s++, t++; i<width/hsubs[c]-1; i++, l++, m++, n++, o++, p++, q++, r++, s++, t++)
                                fputc(mean3x3x3(l, m, n,
                                                o, p, q,
                                                r, s, t), fileout);
                            fputc(*p, fileout);
                        }
                        for (i=0; i<width/hsubs[c]; i++)
                            fputc(current[c][j*width/hsubs[c]+i], fileout);
                    }
                    totframes++;
                    break;

                case EDGE3x3:
                    for (c=0; c<3; c++) {
                        for (j=0; j<height/vsubs[c]; j++) {
                            unsigned char *o = current[c] + mmax((j-1),0)                *width/hsubs[c];
                            unsigned char *p = current[c] +       j                      *width/hsubs[c];
                            unsigned char *q = current[c] + mmin((j+1),height/vsubs[c]-1)*width/hsubs[c];
                            fputc(edge3x3(*o, *o, *(o+1),
                                          *p, *p, *(p+1),
                                          *q, *q, *(q+1)), fileout);
                            for (i=1, o++, p++, q++; i<width/hsubs[c]-1; i++, o++, p++, q++)
                                fputc(edge3x3(*(o-1), *o, *(o+1),
                                              *(p-1), *p, *(p+1),
                                              *(q-1), *q, *(q+1)), fileout);
                            fputc(edge3x3(*(o-1), *o, *o,
                                          *(p-1), *p, *p,
                                          *(q-1), *q, *q), fileout);
                        }
                    }
                    totframes++;
                    break;

                case MEDIAN3:
                    for (c=0; c<3; c++) {
                        for (j=0; j<height/vsubs[c]; j++) {
                            unsigned char *p = current[c] + j*width/hsubs[c];
                            fputc(median3(*p, *p, *(p+1)), fileout);
                            for (i=1, p++; i<width/hsubs[c]-1; i++, p++)
                                fputc(median3(*(p-1), *p, *(p+1)), fileout);
                            fputc(median3(*(p-1), *p, *p), fileout);
                        }
                    }
                    totframes++;
                    break;

                case MEDIAN1x3:
                    for (c=0; c<3; c++) {
                        for (j=0; j<height/vsubs[c]; j++) {
                            unsigned char *o = current[c] + mmax((j-1),0)                *width/hsubs[c];
                            unsigned char *p = current[c] +       j                      *width/hsubs[c];
                            unsigned char *q = current[c] + mmin((j+1),height/vsubs[c]-1)*width/hsubs[c];
                            for (i=0; i<width/hsubs[c]; i++, o++, p++, q++)
                                fputc(median3(*o, *p, *q), fileout);
                        }
                    }
                    totframes++;
                    break;

                case MEDIAN1x1x3:
                    for (c=0; c<3; c++) {
                        for (j=0; j<height/vsubs[c]; j++) {
                            for (i=0; i<width/hsubs[c]; i++) {
                                int k = j*width/hsubs[c]+i;
                                fputc(median3(past[c][k], current[c][k], future[c][k]), fileout);
                            }
                        }
                    }
                    totframes++;
                    break;

                case MEDIAN5:
                    for (c=0; c<3; c++) {
                        for (j=0; j<height/vsubs[c]; j++) {
                            unsigned char *p = current[c] + j*width/hsubs[c];
                            fputc(median5(*p, *p, *p, *(p+1), *(p+2)), fileout);
                            p++;
                            fputc(median5(*(p-1), *(p-1), *p, *(p+1), *(p+2)), fileout);
                            p++;
                            for (i=2; i<width/hsubs[c]-2; i++, p++)
                                fputc(median5(*(p-2), *(p-1), *p, *(p+1), *(p+2)), fileout);
                            fputc(median5(*(p-2), *(p-1), *p, *(p+1), *(p+1)), fileout);
                            p++;
                            fputc(median5(*(p-2), *(p-1), *p, *p, *p), fileout);
                        }
                    }
                    totframes++;
                    break;

                case MEDIAN7:
                    for (c=0; c<3; c++) {
                        for (j=0; j<height/vsubs[c]; j++) {
                            unsigned char *p = current[c] + j*width/hsubs[c];
                            fputc(median7(*p, *p, *p, *p, *(p+1), *(p+2), *(p+3)), fileout);
                            p++;
                            fputc(median7(*(p-1), *(p-1), *(p-1), *p, *(p+1), *(p+2), *(p+3)), fileout);
                            p++;
                            fputc(median7(*(p-2), *(p-2), *(p-1), *p, *(p+1), *(p+2), *(p+3)), fileout);
                            p++;
                            for (i=3; i<width/hsubs[c]-3; i++, p++)
                                fputc(median7(*(p-3), *(p-2), *(p-1), *p, *(p+1), *(p+2), *(p+3)), fileout);
                            fputc(median7(*(p-3), *(p-2), *(p-1), *p, *(p+1), *(p+2), *(p+2)), fileout);
                            p++;
                            fputc(median7(*(p-3), *(p-2), *(p-1), *p, *(p+1), *(p+1), *(p+1)), fileout);
                            p++;
                            fputc(median7(*(p-3), *(p-2), *(p-1), *p, *p, *p, *p), fileout);
                        }
                    }
                    totframes++;
                    break;

                case MEDIAN3x3:
                    for (c=0; c<3; c++) {
                        for (j=0; j<height/vsubs[c]; j++) {
                            unsigned char *o = current[c] + mmax((j-1),0)                *width/hsubs[c];
                            unsigned char *p = current[c] +       j                      *width/hsubs[c];
                            unsigned char *q = current[c] + mmin((j+1),height/vsubs[c]-1)*width/hsubs[c];
                            fputc(median9(*o, *o, *(o+1),
                                          *p, *p, *(p+1),
                                          *q, *q, *(q+1)), fileout);
                            for (i=1, o++, p++, q++; i<width/hsubs[c]-1; i++, o++, p++, q++)
                                fputc(median9(*(o-1), *o, *(o+1),
                                              *(p-1), *p, *(p+1),
                                              *(q-1), *q, *(q+1)), fileout);
                            fputc(median9(*(o-1), *o, *o,
                                          *(p-1), *p, *p,
                                          *(q-1), *q, *q), fileout);
                        }
                    }
                    totframes++;
                    break;

                case MEDIAN5x5:
                    for (c=0; c<3; c++) {
                        for (j=0; j<height/vsubs[c]; j++) {
                            unsigned char *n = current[c] + mmax((j-2),0)                *width/hsubs[c];
                            unsigned char *o = current[c] + mmax((j-1),0)                *width/hsubs[c];
                            unsigned char *p = current[c] +       j                      *width/hsubs[c];
                            unsigned char *q = current[c] + mmin((j+1),height/vsubs[c]-1)*width/hsubs[c];
                            unsigned char *r = current[c] + mmin((j+2),height/vsubs[c]-1)*width/hsubs[c];
                            fputc(median25(*n, *n, *n, *(n+1), *(n+2),
                                           *o, *o, *o, *(o+1), *(o+2),
                                           *p, *p, *p, *(p+1), *(p+2),
                                           *q, *q, *q, *(q+1), *(q+2),
                                           *r, *r, *r, *(r+1), *(r+2)), fileout);
                            n++; o++; p++; q++; r++;
                            fputc(median25(*(n-1), *(n-1), *n, *(n+1), *(n+2),
                                           *(o-1), *(o-1), *o, *(o+1), *(o+2),
                                           *(p-1), *(p-1), *p, *(p+1), *(p+2),
                                           *(q-1), *(q-1), *q, *(q+1), *(q+2),
                                           *(r-1), *(r-1), *r, *(r+1), *(r+2)), fileout);
                            n++; o++; p++; q++; r++;
                            for (i=2; i<width/hsubs[c]-2; i++, n++, o++, p++, q++, r++)
                                fputc(median25(*(n-2), *(n-1), *n, *(n+1), *(n+2),
                                               *(o-2), *(o-1), *o, *(o+1), *(o+2),
                                               *(p-2), *(p-1), *p, *(p+1), *(p+2),
                                               *(q-2), *(q-1), *q, *(q+1), *(q+2),
                                               *(r-2), *(r-1), *r, *(r+1), *(r+2)), fileout);
                            fputc(median25(*(n-2), *(n-1), *n, *(n+1), *(n+1),
                                           *(o-2), *(o-1), *o, *(o+1), *(o+1),
                                           *(p-2), *(p-1), *p, *(p+1), *(p+1),
                                           *(q-2), *(q-1), *q, *(q+1), *(q+1),
                                           *(r-2), *(r-1), *r, *(r+1), *(r+1)), fileout);
                            n++; o++; p++; q++; r++;
                            fputc(median25(*(n-2), *(n-1), *n, *n, *n,
                                           *(o-2), *(o-1), *o, *o, *o,
                                           *(p-2), *(p-1), *p, *p, *p,
                                           *(q-2), *(q-1), *q, *q, *q,
                                           *(r-2), *(r-1), *r, *r, *r), fileout);
                        }
                    }
                    totframes++;
                    break;

                case HYBRID5x5:
                    for (c=0; c<3; c++) {
                        for (j=0; j<height/vsubs[c]; j++) {
                            unsigned char *n = current[c] + mmax((j-2),0)                *width/hsubs[c];
                            unsigned char *o = current[c] + mmax((j-1),0)                *width/hsubs[c];
                            unsigned char *p = current[c] +       j                      *width/hsubs[c];
                            unsigned char *q = current[c] + mmin((j+1),height/vsubs[c]-1)*width/hsubs[c];
                            unsigned char *r = current[c] + mmin((j+2),height/vsubs[c]-1)*width/hsubs[c];
                            fputc(median5x5hybrid(*n, *n, *n, *(n+1), *(n+2),
                                                  *o, *o, *o, *(o+1), *(o+2),
                                                  *p, *p, *p, *(p+1), *(p+2),
                                                  *q, *q, *q, *(q+1), *(q+2),
                                                  *r, *r, *r, *(r+1), *(r+2)), fileout);
                            n++; o++; p++; q++; r++;
                            fputc(median5x5hybrid(*(n-1), *(n-1), *n, *(n+1), *(n+2),
                                                  *(o-1), *(o-1), *o, *(o+1), *(o+2),
                                                  *(p-1), *(p-1), *p, *(p+1), *(p+2),
                                                  *(q-1), *(q-1), *q, *(q+1), *(q+2),
                                                  *(r-1), *(r-1), *r, *(r+1), *(r+2)), fileout);
                            n++; o++; p++; q++; r++;
                            for (i=2; i<width/hsubs[c]-2; i++, n++, o++, p++, q++, r++)
                                fputc(median5x5hybrid(*(n-2), *(n-1), *n, *(n+1), *(n+2),
                                                      *(o-2), *(o-1), *o, *(o+1), *(o+2),
                                                      *(p-2), *(p-1), *p, *(p+1), *(p+2),
                                                      *(q-2), *(q-1), *q, *(q+1), *(q+2),
                                                      *(r-2), *(r-1), *r, *(r+1), *(r+2)), fileout);
                            fputc(median5x5hybrid(*(n-2), *(n-1), *n, *(n+1), *(n+1),
                                                  *(o-2), *(o-1), *o, *(o+1), *(o+1),
                                                  *(p-2), *(p-1), *p, *(p+1), *(p+1),
                                                  *(q-2), *(q-1), *q, *(q+1), *(q+1),
                                                  *(r-2), *(r-1), *r, *(r+1), *(r+1)), fileout);
                            n++; o++; p++; q++; r++;
                            fputc(median5x5hybrid(*(n-2), *(n-1), *n, *n, *n,
                                                  *(o-2), *(o-1), *o, *o, *o,
                                                  *(p-2), *(p-1), *p, *p, *p,
                                                  *(q-2), *(q-1), *q, *q, *q,
                                                  *(r-2), *(r-1), *r, *r, *r), fileout);
                        }
                    }
                    totframes++;
                    break;

                case NLMEANS:
                {
                    nlmeans(current[0], width,      height,      width,      fileout);
                    nlmeans(current[1], width/hsub, height/vsub, width/hsub, fileout);
                    nlmeans(current[2], width/hsub, height/vsub, width/hsub, fileout);

                    /*for (c=0; c<3; c++) {
                        for (j=0; j<height/vsubs[c]; j++) {
                            unsigned char *p = current[c] + j*width/hsubs[c];
                            fputc(mean3(*p, *p, *(p+1)), fileout);
                            for (i=1, p++; i<width/hsubs[c]-1; i++, p++)
                                fputc(mean3(*(p-1), *p, *(p+1)), fileout);
                            fputc(mean3(*(p-1), *p, *p), fileout);
                        }
                    }*/

                    /* tidy up */

                    totframes++;
                    break;
                }

                case KSVD:
                {
                    const double sigma = 10.0;

                    /* filter luma only for time reasons */
                    ksvd_ipol(sigma, current[0], current[0], width, height, 1, 0);

                    if (fwrite(current[0], width*height, 1, fileout) != 1)
                        break;
                    if (fwrite(current[1], width*height/hsub/vsub, 1, fileout) != 1)
                        break;
                    if (fwrite(current[2], width*height/hsub/vsub, 1, fileout) != 1)
                        break;

                    /* this is a very slow algorithm */
                    if (verbose>=1)
                        rvmessage("frame %d", frameno);

                    totframes++;
                    break;
                }

            }
        }
        if (ferror(filein))
            rverror("failed to read from input");
        if (ferror(fileout) && errno!=EPIPE)
            rverror("failed to write to output");

        /* tidy up algorithm */
        switch (algorithm) {
            case NLMEANS:
                nlmeans_free();
                break;

            default:
                break;
        }

        /* tidy up */
        if (numfiles)
            fclose(filein);

    } while (++fileindex<numfiles && !ferror(fileout));

    if (verbose>=0)
        rvmessage("wrote %d frames", totframes);

    /* tidy up */
    rvfree(data[0]);
    rvfree(data[1]);
    rvfree(data[2]);

    return 0;
}
