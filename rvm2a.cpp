/*
 * Description: Functions related to the MPEG2 audio standard.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-10-28 16:07:45 $
 * Revision   : $Revision: 1.80 $
 * Copyright  : (c) 2011 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


#include "rvm2a.h"


/*
 * mpeg1 lookup tables
 */

/* lookup bitrate from index in frame header */
int m2a_bit_rate_table[2][3][16] = {
    { /* version 1 */
        { 0, 32, 64, 96, 128, 160, 192, 224, 256, 288, 320, 352, 384, 416, 448, 0 }, /* level 1 */
        { 0, 32, 48, 56,  64,  80,  96, 112, 128, 160, 192, 224, 256, 320, 384, 0 }, /* level 2 */
        { 0, 32, 40, 48,  56,  64,  80,  96, 112, 128, 160, 192, 224, 256, 320, 0 }  /* level 3 */
    },
    { /* version 2 */
        { 0, 32, 48, 56,  64,  80,  96, 112, 128, 144, 160, 176, 192, 224, 256, 0 }, /* level 1 */
        { 0,  8, 16, 24,  32,  40,  48,  56,  64,  80,  96, 112, 128, 144, 160, 0 }, /* level 2 */
        { 0,  8, 16, 24,  32,  40,  48,  56,  64,  80,  96, 112, 128, 144, 160, 0 }  /* level 3, same as level 2 */
    }
};

/* lookup sampling rate from index in frame header */
int m2a_sampling_rate_table[4][3] = {
    { 44100, 48000, 32000 },
    { 22050, 24000, 16000 },
    {     0,     0,     0 },
    { 11025, 12000,  8000 }
};

/*
 * id3 utility functions
 */

int unsyncsafe(int s)
{
    return ((s & 0xff000000)>>3) | ((s & 0x00ff0000)>>2) | ((s & 0x0000ff00)>>1) | (s & 0x000000ff);
}

int skip_id3(struct bitbuf *bb)
{
    flushbits(bb, 24);                              /* id3 file identifier */
    flushbits(bb, 16);                              /* id3 version */
    int flags = getbits8(bb);                       /* id3 flags */
    int size = unsyncsafe(getbits32(bb));           /* id3 size */
    int total = 80;
    total += flushbits(bb, size*8);
    if (flags & 0x10)
        total += flushbits(bb, 80);                 /* id3 footer */
    return total;
}
