
#ifndef _RVH264_H_
#define _RVH264_H_

#include "rvbits.h"
#include "rvcodec.h"

// type of a pixel. Needed because it could be eather 8 bits or 16bits if bit_depth_luma_minus8/chroma is not null...
#if 1
typedef uint8_t pixel_t;
#define PIXEL_T_IS_8_BITS
#else
typedef uint16_t pixel_t;
#define PIXEL_T_IS_16_BITS
#endif

/* h.264 nal unit type */
typedef enum {
    NAL_UNSPECIFIED = 0,
    NAL_CODED_SLICE_NON_IDR,
    NAL_CODED_SLICE_PARTITION_A,
    NAL_CODED_SLICE_PARTITION_B,
    NAL_CODED_SLICE_PARTITION_C,
    NAL_CODED_SLICE_IDR,
    NAL_SEI,
    NAL_SPS,
    NAL_PPS,
    NAL_AUD,
    NAL_EOQ,
    NAL_EOS,
    NAL_FILLER,
    NAL_SPS_EXT,
    NAL_PREFIX,
    NAL_SUBSET_SPS,
    NAL_RESERVED_16,
    NAL_RESERVED_17,
    NAL_RESERVED_18,
    NAL_CODED_AUX,
    NAL_CODED_EXT,
    NAL_RESERVED_21,
    NAL_RESERVED_22,
    NAL_RESERVED_23,
    NAL_UNSPECIFIED_24,
    NAL_UNSPECIFIED_25,
    NAL_UNSPECIFIED_26,
    NAL_UNSPECIFIED_27,
    NAL_UNSPECIFIED_28,
    NAL_UNSPECIFIED_29,
    NAL_UNSPECIFIED_30,
    NAL_UNSPECIFIED_31,
} nal_unit_type_t;

typedef struct nal_unit_s
{
    int ref_count; /* used by ludh264 */
    uint8_t         nal_ref_idc;        // All u(2)
    nal_unit_type_t nal_unit_type;      // All u(5)
    uint32_t        NumBytesInRBSP;     // Computed
    uint8_t*        rbsp_byte;          // Computed
    uint8_t         trailing_bits;      // Computed. It include the "stop_one_bit", so the range is 1-7 inclusive
    uint32_t        data_length_in_bits;// length of the RBSP in bits
    struct bitbuf  *bs;                 // bitstream buffer FIXME it is used by ludh264 to pass the bitstream between functions, make passing explicit
} nal_unit_t;

/* h.264 hrd parameters */
struct hrd_parameters {
    int cpb_cnt_minus1;
    int bit_rate_scale;
    int cpb_size_scale;
    int bit_rate_value_minus1[32];
    int cpb_size_value_minus1[32];
    int cbr_flag[32];
    int initial_cpb_removal_delay_length_minus1;
    int cpb_removal_delay_length_minus1;
    int dpb_output_delay_length_minus1;
    int time_offset_length;
    /* buffering period sei message parameters */
    int initial_cpb_removal_delay[32];
    int initial_cpb_removal_delay_offset[32];
};

/* h.264 vui parameters */
struct vui_parameters {
    uint8_t   aspect_ratio_info_present_flag;           // u(1)
    uint8_t   aspect_ratio_idc;                         // u(8)
    uint16_t  sar_width;                                // u(16)
    uint16_t  sar_height;                               // u(16)
    uint8_t   overscan_info_present_flag;               // u(1)
    uint8_t   overscan_appropriate_flag;                // u(1)
    uint8_t   video_signal_type_present_flag;           // u(1)
    uint8_t   video_format;                             // u(3)
    uint8_t   video_full_range_flag;                    // u(1)
    uint8_t   colour_description_present_flag;          // u(1)
    uint8_t   colour_primaries;                         // u(8)
    uint8_t   transfer_characteristics;                 // u(8)
    uint8_t   matrix_coefficients;                      // u(8)
    uint8_t   chroma_loc_info_present_flag;             // u(1)
    uint8_t   chroma_sample_loc_type_top_field;         // ue(v)
    uint8_t   chroma_sample_loc_type_bottom_field;      // ue(v)
    uint8_t   timing_info_present_flag;                 // u(1)
    uint32_t  num_units_in_tick;                        // u(32)
    uint32_t  time_scale;                               // u(32)
    uint8_t   fixed_frame_rate_flag;                    // u(1)
    uint8_t   nal_hrd_parameters_present_flag;          // u(1)
    struct hrd_parameters nal_hrd_parameters;
    uint8_t   vcl_hrd_parameters_present_flag;          // u(1)
    struct hrd_parameters vcl_hrd_parameters;
    uint8_t   low_delay_hrd_flag;                       // u(1)
    uint8_t   pic_struct_present_flag;                  // u(1)
    uint8_t   bitstream_restriction_flag;               // u(1)
    uint8_t   motion_vectors_over_pic_boundaries_flag;  // u(1)
    uint8_t   max_bytes_per_pic_denom;                  // ue(v)
    uint8_t   max_bits_per_mb_denom;                    // ue(v)
    uint8_t   log2_max_mv_length_horizontal;            // ue(v)
    uint8_t   log2_max_mv_length_vertical;              // ue(v)
    uint8_t   num_reorder_frames;                       // ue(v)
    uint8_t   max_dec_frame_buffering;                  // ue(v)
};

/* h.264 sequence parameter set */
struct rv264seq_parameter_set {
    int occupied;
    int occupied_warning;
    int profile_idc;
    uint8_t           constraint_set0_flag;     // 0 u(1)
    uint8_t           constraint_set1_flag;     // 0 u(1)
    uint8_t           constraint_set2_flag;     // 0 u(1)
    uint8_t           constraint_set3_flag;     // 0 u(1)
    int level_idc;
    int seq_parameter_set_id;
    int chroma_format_idc;
    int residual_colour_transform_flag;
    int separate_colour_plane_flag;
    int bit_depth_luma_minus8;
    int bit_depth_chroma_minus8;
    int qpprime_y_zero_transform_bypass_flag;
    uint8_t           seq_scaling_matrix_present_flag; // 0 u(1)
    uint8_t           seq_scaling_list_present_flag[8]; // 0 u(1)
    uint8_t           ScalingList4x4[6][16];
    uint8_t           ScalingList8x8[2][64];
    int log2_max_frame_num_minus4;
    int pic_order_cnt_type;
    int log2_max_pic_order_cnt_lsb_minus4;
    int delta_pic_order_always_zero_flag;
    int offset_for_non_ref_pic;
    int offset_for_top_to_bottom_field;
    int num_ref_frames_in_pic_order_cnt_cycle;
    int offset_for_ref_frame[255];
    int max_num_ref_frames;
    int gaps_in_frame_num_value_allowed_flag;
    int pic_width_in_mbs_minus1;
    int pic_height_in_map_units_minus1;
    int frame_mbs_only_flag;
    int mb_adaptive_frame_field_flag;
    int direct_8x8_inference_flag;
    uint8_t           frame_cropping_flag;      // 0 u(1)
    uint16_t          frame_crop_left_offset;   // 0 ue(v)
    uint16_t          frame_crop_right_offset;  // 0 ue(v)
    uint16_t          frame_crop_top_offset;    // 0 ue(v)
    uint16_t          frame_crop_bottom_offset; // 0 ue(v)
    int vui_parameters_present_flag;
    struct vui_parameters vui;
    /* derived parameters */
    unsigned char ChromaArrayType;
    unsigned char SubWidthC;
    unsigned char SubHeightC;
    unsigned char CropUnitX;
    unsigned char CropUnitY;
    unsigned char BitDepthY;
    unsigned char QpBdOffsetY;
    unsigned char BitDepthC;
    unsigned char QpBdOffsetC;
    int MbWidthC;
    int MbHeightC;
    int PicWidthInMbs;
    int PicWidthInSamplesl;
    int PicWidthInSamplesc;
    int FrameHeightInMbs;
    uint16_t          PicHeightInMapUnits;
    uint16_t          PicSizeInMapUnits;
    uint32_t          MaxFrameNum;
    int FrameCropWidthInSamples;
    int FrameCropHeightInSamples;
};

extern const double ChromaFormatFactor[4];

/* h.264 picture parameter set */
struct rv264pic_parameter_set {
    int occupied;
    int occupied_warning;
    int pic_parameter_set_id;
    int seq_parameter_set_id;
    struct rv264seq_parameter_set *sps;
    int entropy_coding_mode_flag;
    int pic_order_present_flag;
    int num_slice_groups_minus1;
    int slice_group_map_type;
    uint16_t*       run_length_minus1; // 1   ue(v)
    uint16_t*       top_left; // 1   ue(v)
    uint16_t*       bottom_right; // 1   ue(v)
    uint8_t         slice_group_change_direction_flag; // 1   u(1)
    uint16_t        slice_group_change_rate_minus1; // 1   ue(v)
    uint16_t        pic_size_in_map_units_minus1; //1   ue(v)
    uint8_t*        slice_group_id; // 1   u(v)
    int num_ref_idx_l0_active_minus1;
    int num_ref_idx_l1_active_minus1;
    uint8_t   num_ref_idx_active[2]; // 1   ue(v) // FIXME duplicate of above.
    int weighted_pred_flag;
    int weighted_bipred_idc;
    int pic_init_qp_minus26;
    int pic_init_qs_minus26;
    int chroma_qp_index_offset;
    int deblocking_filter_control_present_flag;
    int constrained_intra_pred_flag;
    int redundant_pic_cnt_present_flag;
    int transform_8x8_mode_flag;
    uint8_t         pic_scaling_matrix_present_flag; // 1   u(1)
    uint8_t         pic_scaling_list_present_flag[ 8 ]; // 1   u(1)
    uint8_t         ScalingList4x4[6][16];
    uint8_t         ScalingList8x8[2][64];
    int second_chroma_qp_index_offset;
    /* derived parameters */
    uint16_t  SliceGroupChangeRate;
    uint16_t  LevelScale4x4[6][6][16];
    uint16_t  LevelScale8x8[2][6][64];
};

/* h.264 sei message */
struct rv264pic_timing {
    int pic_struct;
    int seconds_flag;
    int minutes_flag;
    int hours_flag;
    uint8_t n_frames;
    uint8_t seconds_value;
    uint8_t minutes_value;
    uint8_t hours_value;
};

struct rv264user_data_registered_itu_t_t35 {
    int cc_valid;
};

struct rv264sei_message {
    struct rv264pic_timing pic_timing;
    struct rv264user_data_registered_itu_t_t35 user_data_registered_itu_t_t35;
};

/* h.264 slice type */
typedef enum {
    SLICE_P = 0,
    SLICE_B,
    SLICE_I,
    SLICE_SP,
    SLICE_SI,
    SLICE_CP,
    SLICE_CB,
    SLICE_CI,
    SLICE_CSP,
    SLICE_CSI
} slice_type_t;

/* h.264 reference picture reordering */
struct ref_pic_list_reordering_t
{
    uint32_t* pic_list_reordering_commands[2];
    uint8_t   ref_pic_list_reordering_flag[2];          // 2 u(1)
};

/* h.264 prediction weight table */
struct pred_weight_table_t
{
    uint8_t   luma_log2_weight_denom;
    uint8_t   chroma_log2_weight_denom;
    int8_t    *luma_weight_l[2];                        // [LX]   LX = 0 or 1
    int16_t   *luma_offset_l[2];                        // [LX]
    int8_t    *chroma_weight_l[2][2];                   // [iCbCr][LX] Cb=0, Cr=1
    int16_t   *chroma_offset_l[2][2];                   // [iCbCr][LX]
};

/* h.264 reference picture marking */
struct dec_ref_pic_marking_t
{
    uint8_t   no_output_of_prior_pics_flag;             // 2|5 u(1)
    uint8_t   long_term_reference_flag;                 // 2|5 u(1)
    uint8_t   adaptive_ref_pic_marking_mode_flag;       // 2|5 u(1)
    //uint32_t  difference_of_pic_nums_minus1; // 2|5 ue(v)
    //uint32_t  long_term_pic_num; // 2|5 ue(v)
    //uint32_t  long_term_frame_idx; // 2|5 ue(v)
    //uint32_t  max_long_term_frame_idx_plus1; // 2|5 ue(v)
    #define MAX_NB_OF_MMCO_COMMANDS 68
    uint8_t  nb_of_mmco_cmd;
    uint8_t  mmco_commands[MAX_NB_OF_MMCO_COMMANDS];
    uint8_t  difference_of_pic_nums_minus1[MAX_NB_OF_MMCO_COMMANDS];
    uint8_t  long_term_pic_num[MAX_NB_OF_MMCO_COMMANDS];
    uint8_t  long_term_frame_idx[MAX_NB_OF_MMCO_COMMANDS];
    uint8_t  max_long_term_frame_idx_plus1[MAX_NB_OF_MMCO_COMMANDS];
};

/* h.264 slice header */
struct slice_header {
    int ref_count; /* used by ludh264 */
    int first_mb_in_slice;
    slice_type_t slice_type;
    int pic_parameter_set_id;
    int frame_num;
    int field_pic_flag;
    int bottom_field_flag;
    int idr_pic_id;
    int pic_order_cnt_lsb;
    int delta_pic_order_cnt_bottom;
    int delta_pic_order_cnt[2];
    int redundant_pic_cnt;
    int direct_spatial_mv_pred_flag;
    int num_ref_idx_active_override_flag;
    int num_ref_idx_l0_active_minus1;
    int num_ref_idx_l1_active_minus1;
    struct ref_pic_list_reordering_t ref_pic_list_reordering;
    struct pred_weight_table_t *pred_weight_table;
    struct dec_ref_pic_marking_t dec_ref_pic_marking;
    int cabac_init_idc;
    int slice_qp_delta;
    int sp_for_switch_flag;
    int slice_qs_delta;
    int disable_deblocking_filter_idc;
    int slice_alpha_c0_offset_div2;
    int slice_beta_offset_div2;
    int slice_group_change_cycle;
    int MbaffFrameFlag;
    int PicHeightInMbs;
    int PicHeightInSamplesl;
    int PicHeightInSamplesc;
    int PicSizeInMbs;
    /* back pointers */
    struct rv264seq_parameter_set *sps;
    struct rv264pic_parameter_set *pps;
    /* derived parameters */
    int PicOrderCnt;
    struct RefListEntry_s* RefPicList[2]; // [LX][refPicIdx]
    slice_type_t slice_type_modulo5; // needed in order not to to the test for normal and _CONT slice_type
    uint16_t  original_frame_num; // this is equal to the original frame_num field but is not set to 0 with mmco equal to 5
    uint8_t   has_mmco5;
    uint32_t  MaxPicNum;
    uint32_t  CurrPicNum;
    int8_t    SliceQPY;
    int8_t    FilterOffsetA; // 2 se(v)
    int8_t    FilterOffsetB; // 2 se(v)
    uint8_t   nal_ref_idc; // copied from the nalu struct
    uint8_t   nal_unit_type; // copied from the nalu struct
    uint16_t  mb_nb; // number of macroblocks in that slice
    uint16_t  slice_num; // number of this slice in the picture
    nal_unit_t* nalu; // nal unit that hold the data for that slice
    struct rv264picture* pic; // back ref to the picture containing this slice
    uint8_t*  MbToSliceGroupMap;
    /* FIXME duplicated parameters */
    uint8_t   num_ref_idx_active[2]; // 2    ue(v)
};

extern const char *slice_type_name[];

/* h.264 partition types */
typedef enum {
    na          = -1,
    Intra_4x4   = 0,
    Intra_8x8,
    Intra_16x16,
    Direct,
    Pred_L0,
    Pred_L1,
    BiPred
} part_type_t;

/* h.264 macroblock type */
#define P_OFFSET 27
#define B_OFFSET 33
typedef enum {
    /* I macroblock types */
    I_NxN         =  0,
    I_16x16_0_0_0,
    I_16x16_1_0_0,
    I_16x16_2_0_0,
    I_16x16_3_0_0,
    I_16x16_0_1_0,
    I_16x16_1_1_0,
    I_16x16_2_1_0,
    I_16x16_3_1_0,
    I_16x16_0_2_0,
    I_16x16_1_2_0,
    I_16x16_2_2_0,
    I_16x16_3_2_0,
    I_16x16_0_0_1,
    I_16x16_1_0_1,
    I_16x16_2_0_1,
    I_16x16_3_0_1,
    I_16x16_0_1_1,
    I_16x16_1_1_1,
    I_16x16_2_1_1,
    I_16x16_3_1_1,
    I_16x16_0_2_1,
    I_16x16_1_2_1,
    I_16x16_2_2_1,
    I_16x16_3_2_1,
    I_PCM,

    /* from SI slices */
    SI            = 26,

    /* P macroblock types */
    P_L0_16x16    = P_OFFSET+ 0,
    P_L0_L0_16x8,
    P_L0_L0_8x16,
    P_8x8,
    P_8x8ref0,
    P_Skip,

    /* B macroblock types */
    B_Direct_16x16= B_OFFSET+ 0,
    B_L0_16x16,
    B_L1_16x16,
    B_Bi_16x16,
    B_L0_L0_16x8,
    B_L0_L0_8x16,
    B_L1_L1_16x8,
    B_L1_L1_8x16,
    B_L0_L1_16x8,
    B_L0_L1_8x16,
    B_L1_L0_16x8,
    B_L1_L0_8x16,
    B_L0_Bi_16x8,
    B_L0_Bi_8x16,
    B_L1_Bi_16x8,
    B_L1_Bi_8x16,
    B_Bi_L0_16x8,
    B_Bi_L0_8x16,
    B_Bi_L1_16x8,
    B_Bi_L1_8x16,
    B_Bi_Bi_16x8,
    B_Bi_Bi_8x16,
    B_8x8,
    B_Skip,

    NB_OF_MB_TYPE,

} mb_type_t;

/* h.264 sub macroblock types */
typedef enum {
    P_L0_8x8 = 0,
    P_L0_8x4,
    P_L0_4x8,
    P_L0_4x4,
    B_Direct_8x8 = 4, // FIXME changed from 0 to match ludh264, rvh264 code needs updating.
    B_L0_8x8,
    B_L1_8x8,
    B_Bi_8x8,
    B_L0_8x4,
    B_L0_4x8,
    B_L1_8x4,
    B_L1_4x8,
    B_Bi_8x4,
    B_Bi_4x8,
    B_L0_4x4,
    B_L1_4x4,
    B_Bi_4x4,

    NB_OF_SUB_MB_TYPE
} sub_mb_type_t;

/* h.264 intra 16x16 prediction types */
typedef enum {
    Intra_16x16_Vertical = 0,
    Intra_16x16_Horizontal,
    Intra_16x16_DC,
    Intra_16x16_Plane
} intra_16x16_pred_mode_t;

/* h.264 intra 4x4 prediction types */
typedef enum {
    Intra_4x4_Vertical = 0,
    Intra_4x4_Horizontal,
    Intra_4x4_DC,
    Intra_4x4_Diagonal_Down_Left,
    Intra_4x4_Diagonal_Down_Right,
    Intra_4x4_Vertical_Right,
    Intra_4x4_Horizontal_Down,
    Intra_4x4_Vertical_Left,
    Intra_4x4_Horizontal_Up
} intra_4x4_pred_mode_t;

/* h.264 intra chroma prediction types */
typedef enum {
    Intra_Chroma_DC = 0,
    Intra_Chroma_Horizontal,
    Intra_Chroma_Vertical,
    Intra_Chroma_Plane
} intra_chroma_pred_mode_t;

/* h.264 macroblock */
struct rv264macro {
    int idr_num;                    /* orders sequence into sub-sequences split by an idr picture */
    int pic_order_cnt;              /* picture order count within a sub-sequence */
    int slice_num;
    int mb_addr;
    slice_type_t slice_type;
    int occupied;
    struct slice_header *sh;
    int mb_skip_flag;
    int mb_field_decoding_flag;
    mb_type_t mb_type;
    int transform_size_8x8_flag;
    sub_mb_type_t sub_mb_type[4];
    intra_4x4_pred_mode_t rem_intra_pred_mode[16];
    intra_4x4_pred_mode_t Intra4x4PredMode[16];
    intra_chroma_pred_mode_t intra_chroma_pred_mode;
    int coded_block_pattern;
    int8_t ref_idx_l0[4];
    int8_t ref_idx_l1[4];
    int16_t mvd[2][4][4][2];
    int16_t mv[2][16][2];
    int mb_qp_delta;
    uint8_t TotalCoeffLuma[16];
    uint8_t TotalCoeffChroma[2][16];
    // TODO union these.
    coeff_t Intra16x16DCLevel[16];
    coeff_t Intra16x16ACLevel[16][15];
    coeff_t LumaLevel[16][16];
    coeff_t LumaLevel8x8[4][256];
    coeff_t ChromaDCLevel[2][16];
    coeff_t ChromaACLevel[2][16][15];
    /* derived parameters */
    int QPy;
    int QPc[2];
    int TransformBypassModeFlag;
    int CodedBlockPatternLuma;
    int CodedBlockPatternChroma;
    /* ludh264 parameters */
    uint32_t partWidths;   // 2bits per partWidth/Height_div4-1. bit01=> first (sub)partition, bit23 => second (sub)partition...
    uint32_t partHeights;  // an array [partwidth][partHeight][curr idx] gives the next idx (see implementation)
    // Before caching (syntax parsing)
    //    RefIdxL[L0-L1][mbPartIdx(<4)]
    //    MvL[L0-L1][mbPartIdx*4+subMbPartIdx][X-Y]
    // In the cache
    //     RefIdxL[L0-L1][block row*4+block col]   <= MUST BE ALIGNED on 32 bits...
    //     MvL[L0-L1][sub block row*4+sub block col][X-Y]   <= MUST BE ALIGNED on 32 bits...
    int8_t RefIdxL[2][16];
    //  it is not necessary to store directly predFlagLX, as it can be derived as predFlagLX = refIdxLX>=0
    int chroma_dc_coeffs_non_null;  // Bit0:Cb, Bit1:Cr. When set to 1=> at least one DC coeff is available, 0 => all DC coeffs are null
    int luma_dc_coeffs_non_null;    // only relevant for Intra16x16 mb. When set to 1=> at least one DC coeff is available, 0 => all DC coeffs are null
    /* derived flag parameters */
    unsigned int left_mb_is_available:1;
    unsigned int left_mb_is_field:1;
    unsigned int up_mb_is_available:1;
    unsigned int up_mb_is_field:1;
    unsigned int upleft_mb_is_available:1;
    unsigned int upleft_mb_is_field:1;
    unsigned int upright_mb_is_available:1;
    unsigned int upright_mb_is_field:1;
};

typedef enum {
    Intra16x16DCLevel,
    Intra16x16ACLevel,
    LumaLevel,
    LumaLevel8x8,
    ChromaDCLevel,
    ChromaACLevel
} residual_block_t;

struct coeff_token_t {
    int TrailingOnes;
    int TotalCoeff;
};

#define TotalCoeff(coeff_token) (coeff_token.TotalCoeff)
#define TrailingOnes(coeff_token) (coeff_token.TrailingOnes)

/* h.264 slice */
struct rv264slice {
    struct slice_header *sh;
    /* for each slice this is the number of
     * macroblocks in the entire picture */
    struct rv264macro *macro;
};

/* h.264 picture */
typedef struct
{
  struct rv264macro* mb_attr; // points to the global picture mb attributes
  int16_t* data;  // points to the global picture data buffer. Each mb has its data as: Y residual (or pcm data), Cb residual (or pcm data) and then Cr residual (or pcm data).
                  // the data inside a mb is in raster order
}
PictureData;

struct rv264picture {
    uint8_t nal_ref_idc;
    uint8_t nal_unit_type;

    uint8_t aud_present;
    uint8_t primary_pic_type;           /* optional field to indicate the type of slices in the primary coded picture, found in NAL_AUD */

    struct rv264sei_message sei_message;   /* copy of sei messages from before this picture */

    struct rv264pic_parameter_set *pps;
    size_t size_slice_array;
    int num_slices;
    struct slice_header **sh;
    struct rv264macro *macro;

    /* ludh264 */
    // The picture will be freed only if those 3 ref counter a set to -1 (the value is initialized to 0 for 1 user)
    int32_t field_use_count[2]; // ref counter for each fields of the picture
    int32_t dec_data_use_count; // ref counter for decoded data of this picture

    pixel_t* Y;      // contains decoded samples (fields are interleaved, when relevant)
    pixel_t* C[2];   //

    uint32_t image_buffer_size; // size of the image in bytes. Set and used in the image output module
    uint32_t width;
    uint32_t height;

    PictureData field_data[2]; // 0-Top Field, 1-Bot Field. For Frames, both points to the frame. Contains pre-decoded data
    struct slice_header** field_sh[2]; // array of all slices of this picture, for each fields. For frame, point to the same array
    unsigned int slice_num[2]; // number of slices for each fields

    unsigned int dec_num[2]; // picture number in decoding order
    int32_t   PicOrderCnt;
    int32_t   FieldOrderCnt[2]; //0-TopFieldOrderCnt,  1-BottomFieldOrderCnt
    int       pic_parameter_set_id;
    int       frame_num;
    int       field_pic_flag;
    int       bottom_field_flag;
    int       idr_pic_id;
    int       pic_order_cnt_lsb;
    uint8_t   ref_struct; /* bit 0 specify that top fields is reference fields, bit 1 is for bottom field, bit 2 specify that this is a frame picture
                                a frame with only the bottom field as a reference will have the bits2-0= 110
                                a complementary reference field pair will have the bits2-0= 011
                                a non paired reference top field will have the bits2-0= 001
                            */
    uint8_t   structure; /* same as ref_struct field, but not for reference. For instance, a Comp Field Pair has a structure=011 and could have a ref_struct=001 */
    uint8_t   non_existing_flag; // 1 when the ref pic is a non-existing frame.
};

/* h.264 frame */
struct rv264frame {
    int field_pic_flag;
    union {
        struct rv264picture *frame;
        struct rv264picture *topfield;
    }; /* anonymous unions require C11 */
    struct rv264picture *botfield;
};

typedef struct OUTPICLIST
{
  struct OUTPICLIST* next;
  struct rv264picture* pic;
} OUTPICLIST;

typedef struct
{
  struct rv264picture* images[16]; // this is a fifo like buffer (but entries are sorted by ascending poc value)
  struct rv264picture* last_added_image; // last added frame. Necessary when adding the second of a field pair.
  unsigned int first; // index of the 1st element in the buffer
  unsigned int last; // index of the last element in the buffer
  int nb_of_images;
  OUTPICLIST *head, *tail;
} DecodedPictureBuffer;

#define NB_OF_REF_PICS  32

typedef enum
{
  UNUSED_FOR_REF = 0,
  SHORT_TERM_REF = 1,
  LONG_TERM_REF  = 2,
} ref_pic_type_t;


typedef struct RefListEntry_s
{
  struct rv264picture *ref_pic;
  /* 0 = top field, 1 = bottom field
   * When field_pic_flag==1, parity overload ref_struct value
   * This is necessary when splitting frame into fields
   * */
  uint8_t   parity;
  ref_pic_type_t ref_pic_type;
}
RefListEntry;

typedef struct
{
  struct rv264picture* pic;
  struct slice_header* prev_sh;     // points to the previously decoded slice header.
  unsigned int max_num_of_slices;

  /* POC process variables */
  int32_t prevPicOrderCntMsb;
  int32_t prevPicOrderCntLsb;
  int32_t PicOrderCntMsb;
  int32_t FrameNumOffset;
  int32_t prevFrameNumOffset;
  uint16_t prevFrameNum;
  uint16_t PrevRefFrameNum;

  /* number of pictures decoded since the beginning, for debug purposes */
  unsigned int dec_num;
  unsigned int pic_not_finished;    // 1- The curr pic is not finished, 0-the curr has finished to decode
  unsigned int decoding_finished;   // set when no more nalu are available

  /* picture reference lists and picture marking */
  int           ref_list_nb_of_st;
  int           ref_list_nb_of_lt;
  struct rv264picture *ref_list_st[NB_OF_REF_PICS]; // holds short term reference pictures (frames and fields).
  struct rv264picture *ref_list_lt[16];             // holds long term reference pictures (frames and fields).
                                                    // the ref pic are stored at their index location and not consecutively.
  RefListEntry  RefPicList_d[2][NB_OF_REF_PICS];    // holds reordered/filtered reference pictures (frames and fields).
  RefListEntry* RefPicList[2][NB_OF_REF_PICS];      // pointers so it is easier to move/sort element of the list
                                                    // each element points to a real element of RefPicList_d

  int           MaxLongTermFrameIdx;
  uint8_t       is_second_field_of_a_pair;
}
PictureDecoderData;

/* h.264 sequence */
struct rv264sequence {
    int active_sps;
    int active_pps;
    struct rv264seq_parameter_set *sps[32];
    struct rv264pic_parameter_set *pps[256];
    struct rv264sei_message sei_message;           /* most recent sei message nal */
    size_t size_frame_array;
    struct rv264frame *frame;   /* this is a dynamically sized array of 'struct rv264frame' */

    // output reordering buffer.
    DecodedPictureBuffer dpb;

    // decoder state data.
    PictureDecoderData pdd;

    // parked nal unit.
    struct bitbuf *park;      /* in cases where a nal unit has been */
    int nal_unit_type;        /* extracted but not used, it can be */
    int nal_ref_idc;          /* parked here for the next iteration */
};

int slice_is_i(slice_type_t slice_type);
int slice_is_p(slice_type_t slice_type);
int slice_is_b(slice_type_t slice_type);

int macro_is_intra(struct rv264macro *macro);
int macro_is_intra4x4(struct rv264macro *macro);
int macro_is_intra8x8(struct rv264macro *macro);
int macro_is_intra16x16(struct rv264macro *macro);
int macro_is_inter(struct rv264macro *macro);
int macro_is_skip(struct rv264macro *macro);

int remove_emulation_prevention_three_byte(unsigned char *buf, int bytes);
struct bitbuf *extract_rbsp(struct bitbuf *bb, int verbose);
void free_rbsp(struct bitbuf *bb);

struct rv264picture *alloc_264_picture();
void realloc_264_slice_array(struct rv264picture *picture, int size);
void add_264_slice(struct rv264picture *picture, struct slice_header *sh);
void free_264_picture(struct rv264picture *picture);

struct rv264frame *alloc_264_frame();
void free_264_frame(struct rv264frame *frame);

struct rv264sequence *alloc_264_sequence();
void realloc_264_frame_array(struct rv264sequence *sequence, int size);
void realloc_264_picture_array(struct rv264sequence *sequence, int size);
void free_264_sequence(struct rv264sequence *sequence);

int find_next_nal_unit(struct bitbuf *bb, int *zerobyte);
const char *nal_unit_type_code(int nal_unit_type);
int parse_nal_unit_header(struct bitbuf *bb, int *nal_unit_type, int *nal_ref_idc);
int more_rbsp_data(struct bitbuf *bb);
int parse_rbsp_trailing_bits(struct bitbuf *bb, const char *where);
struct rv264seq_parameter_set *parse_sequence_param(struct bitbuf *bb);
int parse_sequence_param_ext(struct bitbuf *bb);
struct rv264pic_parameter_set *parse_picture_param(struct bitbuf *bb, struct rv264seq_parameter_set **spss, int verbose);
struct slice_header *parse_slice_header(struct bitbuf *bb, int nal_unit_type, int nal_ref_idc, struct rv264pic_parameter_set **pps, int verbose);
void parse_macroblock_layer(struct bitbuf *bb, struct rv264macro *mb);
struct rv264sequence *parse_264_params(struct bitbuf *bb, int verbose);
struct rv264picture *parse_264_picture(struct bitbuf *bb, struct rv264sequence *seq, int verbose);
struct rv264picture *parse_264_picture_headers(struct bitbuf *bb, struct rv264sequence *seq, int verbose);
void parse_sei(struct bitbuf *bb, struct rv264sequence *seq, int verbose);

/* TODO static */
void parse_mb_pred(struct bitbuf *bb, struct rv264macro *mb);
void parse_sub_mb_pred(struct bitbuf *bb, struct rv264macro *mb);
void parse_residual(struct bitbuf *bb, struct rv264macro *mb);
int parse_residual_block_cavlc(struct bitbuf *bb, coeff_t *coeffLevel, int maxNumCoeff, int nC);
int parse_residual_block_cabac(struct bitbuf *bb, coeff_t *coeffLevel, int maxNumCoeff, int nC);

int map_coded_block_pattern(int chroma_format_idc, mb_type_t mb_type, int codenum);
void decode_intra_pred_mode(int num_macros, struct rv264macro macro[], struct rv264seq_parameter_set *sps, struct rv264pic_parameter_set *pps);
void decode_mvd(int num_macros, struct rv264macro macro[], struct rv264seq_parameter_set *sps, struct rv264pic_parameter_set *pps);
sample_t *decode_264_picture(struct rv264picture *picture, struct rv264seq_parameter_set *sps, int picno);

#endif
