
#ifndef _RVUTIL_H_
#define _RVUTIL_H_

#include "rvtypes.h"
#include "rvbits.h"

/* Maximum number of characters in a file path */
#define RV_MAXPATH 256

/* Maximum number of input files */
#define RV_MAXFILES 1024

/* file types */
typedef enum {
    SKIP,
    MISSING,
    EMPTY,
    OTHER,
    YUV,
    YUV4MPEG,
    BMP,
    SGI,
    TS,
    PS,
    PES,
    RIFF,
    AVI,
    QT,
    ASF,
    H261,
    H263,
    H264,
    HEVC,
    M1V,
    M2V,
    M4V,
    JPEG,
    REAL,
    AVS,
    VC1,
    RCV,    /* VC-1 Annex L */
    AV1,
    IVF,
    WEBM,
    AU,
    WAV,
    MPA,
    AC3,
} filetype_t;

extern const char *filetypename[];

/* chroma formats */
typedef enum {
    UNKNOWN = 0,
    YUV400,
    YUV420,
    YUV420mpeg,
    YUV420jpeg,
    YUV420dv,
    YUV422,
    YUV444,
} chromaformat_t;

extern const char *chromaformatname[];

/* types */
typedef u_int32_t fourcc_t;
typedef float framerate_t;

typedef struct {
    filetype_t filetype;
    int leading;            /* number of bits of leading garbage */
} divine_t;

/* fourcc pixel formats */
#define FOURCC(a,b,c,d) (((fourcc_t)(u_int8_t)(a)      ) | \
                         ((fourcc_t)(u_int8_t)(b) << 8 ) | \
                         ((fourcc_t)(u_int8_t)(c) << 16) | \
                         ((fourcc_t)(u_int8_t)(d) << 24))

#define BIGFOURCC(a,b,c,d) (((fourcc_t)(u_int8_t)(a) << 24) | \
                            ((fourcc_t)(u_int8_t)(b) << 16) | \
                            ((fourcc_t)(u_int8_t)(c) << 8 ) | \
                            ((fourcc_t)(u_int8_t)(d)      ))

#define BTOH16(a) ((((u_int16_t)(a) & 0xff00) >> 8) | \
                   (((u_int16_t)(a) & 0x00ff) << 8))

#define BTOH32(a) ((((u_int32_t)(a) & 0xff000000) >> 24) | \
                   (((u_int32_t)(a) & 0x00ff0000) >> 8 ) | \
                   (((u_int32_t)(a) & 0x0000ff00) << 8 ) | \
                   (((u_int32_t)(a) & 0x000000ff) << 24))

#define BTOH64(a) ((((u_int64_t)(a) & 0xff00000000000000LL) >> 56) | \
                   (((u_int64_t)(a) & 0x00ff000000000000LL) >> 40) | \
                   (((u_int64_t)(a) & 0x0000ff0000000000LL) >> 24) | \
                   (((u_int64_t)(a) & 0x000000ff00000000LL) >> 8 ) | \
                   (((u_int64_t)(a) & 0x00000000ff000000LL) << 8 ) | \
                   (((u_int64_t)(a) & 0x0000000000ff0000LL) << 24) | \
                   (((u_int64_t)(a) & 0x000000000000ff00LL) << 40) | \
                   (((u_int64_t)(a) & 0x00000000000000ffLL) << 56))

#define I420   (fourcc_t)0x30323449
#define YU12   (fourcc_t)0x32315559
#define I422   (fourcc_t)0x32323449
#define YU16   (fourcc_t)0x36315559
#define IYUV   (fourcc_t)0x56555949
#define UYVY   (fourcc_t)0x59565955
#define YUY2   (fourcc_t)0x32595559
#define TWOVUY (fourcc_t)0x59555632
#define VYUY   (fourcc_t)0x59555956
#define Y800   (fourcc_t)0x30303859
#define RGB2   (fourcc_t)0x32424752
#define RGBP   (fourcc_t)0x50424752
#define I444   (fourcc_t)0x34343449
#define V210   (fourcc_t)0x30313256
#define YU15   (fourcc_t)0x35315559
#define YU20   (fourcc_t)0x30325559
#define NV12   (fourcc_t)0x3231564E
#define NV16   (fourcc_t)0x3631564E
#define NV15   (fourcc_t)0x3531564E /* 10-bit NV12 */
#define NV20   (fourcc_t)0x3032564E
#define XV20   (fourcc_t)0x30325658

#define RV10   FOURCC('R','V','1','0')
#define RV13   FOURCC('R','V','1','3')
#define RV20   FOURCC('R','V','2','0')
#define RV30   FOURCC('R','V','3','0')
#define RV40   FOURCC('R','V','4','0')

#define mmax(a, b) ((a) > (b) ? (a) : (b))
#define mmin(a, b) ((a) < (b) ? (a) : (b))
#define mmax3(a, b, c) (mmax((a), mmax((b), (c))))
#define mmin3(a, b, c) (mmin((a), mmin((b), (c))))
#define clip(x, a, b) ((x) < (a) ? (a) : (x) > (b) ? (b) : (x))
#define wrap(x, a, b, w) ((x) < (a) ? (x) + (w) : (x) > (b) ? (x) - (w) : (x))
#define sign(x) ((x)<0 ? -1 : (x)>0 ? 1 : 0)
#define rvswap(t, a, b) do {t x = a; a = b; b = x;} while (0)
unsigned int clog2(unsigned int x);
unsigned int flog2(unsigned int x);

#define stringify(x) #x

/* rvutil.c */
void rverror(const char *format, ...) __attribute__ ((format (printf, 1, 2)));
void rvexit(const char *format, ...) __attribute__ ((format (printf, 1, 2)));
void rvmessage(const char *format, ...) __attribute__ ((format (printf, 1, 2)));
void rvstatus(const char *format, ...) __attribute__ ((format (printf, 1, 2)));
void rvabort(const char *format, ...) __attribute__ ((format (printf, 1, 2)));

void *rvalloc(void *ptr, size_t size, int clear);
void rvfree(void *ptr);

const char *get_basename(const char *appname);
const char *get_extension(const char *filename);
const char *describe_fourcc(int fourcc);
char *describe_bitrate(char *string, int len, int bitrate, int precision, int extra);
char *describe_number(char *string, int len, long long int n, int precision);
char *describe_duration(char *string, int len, double duration);
char *describe_90kHz(char *string, int len, long long time);
char *describe_27MHz(char *string, int len, long long time);
char *describe_now(char *string, int len);

char *rvupper(char *s);
char *rvlower(char *s);

unsigned int rvmsec();
unsigned long long rvusec();
unsigned long long rv90kHz();
unsigned long long rv27MHz();

extern const char *rvformats;
int lookup_format(const char *format, int *width, int *height, int *interlaced, framerate_t *framerate);
extern const char *rvchromas;
int lookup_chroma(const char *chroma, int *hsub, int *vsub);
extern const char *rvfourccs;
fourcc_t get_fourcc(const char *fccode);
int fourcc_is_planar(const fourcc_t fourcc);
void get_chromasub(fourcc_t fourcc, int *hsub, int *vsub);
int get_bitsperpel(fourcc_t fourcc);
int get_numplanes(fourcc_t fourcc);
chromaformat_t get_chromaformat(fourcc_t fourcc);
const char *fourccname(const fourcc_t fourcc);
int get_framesize(int width, int height, int hsub, int vsub, int bpp);
int get_framesize(int width, int height, fourcc_t fourcc);
int divine_image_dims(int *width, int *height, int *interlaced, framerate_t *framerate, fourcc_t *fourcc, const char *format, const char *filename);
fourcc_t divine_pixel_format(const char *filename);
divine_t divine_filetype(struct bitbuf *bb, const char *filename, const char *format, int verbose);
int frame_match(unsigned int num, const char *format, int verbose);

#endif
