
#ifndef _RVSTAT_H_
#define _RVSTAT_H_

#include "rvimage.h"

typedef enum {
    ALL_PLANES   = 0,
    LUMA_PLANE   = 1,
    CHROMA_PLANE = 2,
    CB_PLANE     = 3,
    CR_PLANE     = 4,
} plane_t;

struct rvstats {
    int maxdiff;
    int maxdiffx;
    int maxdiffy;
    double mean;
    double var;
    double skew;
    /*double kurt;*/
    double mse;
    double psnr;
    double ssim;
};

struct rvstats calculate_stats(rvimage &src, rvimage &ref, int order, plane_t plane=ALL_PLANES);
double calc_entropy(struct rvimage &src, plane_t plane);
double calc_mean(struct rvimage &src, plane_t plane);

#endif
