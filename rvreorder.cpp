/*
 * Description: Reorder video data.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-06-23 13:26:19 $
 * Revision   : $Revision: 1.15 $
 * Copyright  : (c) 2005 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include "getopt.h"

#include "rvutil.h"

const char *appname;

const int rowcol[64] = {
     0,  8, 16, 24, 32, 40, 48, 56,
     1,  9, 17, 25, 33, 41, 49, 57,
     2, 10, 18, 26, 34, 42, 50, 58,
     3, 11, 19, 27, 35, 43, 51, 59,
     4, 12, 20, 28, 36, 44, 52, 60,
     5, 13, 21, 29, 37, 45, 53, 61,
     6, 14, 22, 30, 38, 46, 45, 62,
     7, 15, 23, 31, 39, 47, 55, 63,
};

const int zigzag[64] = {
    0,   1,  5,  6, 14, 15, 27, 28,
    2,   4,  7, 13, 16, 26, 29, 42,
    3,   8, 12, 17, 25, 30, 41, 43,
    9,  11, 18, 24, 31, 40, 44, 53,
    10, 19, 23, 32, 39, 45, 52, 54,
    20, 22, 33, 38, 46, 51, 55, 60,
    21, 34, 37, 47, 50, 56, 59, 61,
    35, 36, 48, 49, 57, 58, 62, 63
};

/* conversion types */
typedef enum {
    NONE,
    ROWCOL,
    ZIGZAG,
} conv_t;

void usage(int exitcode)
{
    fprintf(stderr, "%s: reorder lines in a text file\n", appname);
    fprintf(stderr, "usage: %s -f <format> [options] [<file>] [<file>...]\n", appname);
    fprintf(stderr, "\t-o <file>   : write output to file\n");
    fprintf(stderr, "\t-f <format> : conversion format\n");
    fprintf(stderr, "\t--          : disable argument processing\n");
    fprintf(stderr, "\t-h          : print this usage message\n");
    exit(exitcode);
}

int main(int argc, char *argv[])
{
    FILE *filein = stdin;
    FILE *fileout = stdout;
    char *filename[RV_MAXFILES] = {0};
    char *outfile = NULL;
    int fileindex = 0;
    int numfiles = 0;
    conv_t conv = NONE;

    /* line buffers */
    size_t size[64] = {0};
    char *line[64] = {NULL};

    /* command line defaults */
    int verbose = 0;
    char *format = NULL;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"format",  1, NULL, 'f'},
            {"output",  1, NULL, 'o'},
            {"quiet",   0, NULL, 'q'},
            {"verbose", 0, NULL, 'v'},
            {"usage",   0, NULL, 'h'},
            {"help",    0, NULL, 'h'},
            {NULL,      0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "f:o:qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 'f':
                format = optarg;
                break;

            case 'o':
                outfile = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < sizeof(filename)/sizeof(filename[0]))
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    /* determine format of conversion */
    if (format) {
        if (strcasecmp(format, "rowcol")==0 || strcasecmp(format, "rc")==0)
            conv = ROWCOL;
        if (strcasecmp(format, "zigzag")==0 || strcasecmp(format, "zz")==0)
            conv = ZIGZAG;
    }
    if (conv==NONE)
        rvexit("can't determine conversion to perform");

        /* open output file */
    if (outfile)
        fileout = fopen(outfile, "w");
    if (fileout==NULL)
        rverror("failed to open output file \"%s\"", outfile);

    /* ignore SIGPIPE on output file */
    if (signal(SIGPIPE, SIG_IGN)==SIG_ERR)
        rverror("failed to set ignore on SIGPIPE");

    /* loop over input files */
    do {
        if (numfiles)
            filein = fopen(filename[fileindex++], "r");
        if (filein==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex-1]);

        while (!feof(filein) && !ferror(filein))
        {
            int i, read[64];

            for (i=0; i<64; i++) {
#ifndef WIN32
                read[i] = getline(&line[i], &size[i], filein);
#else
                rvmessage("time to implement getline() on minghw32");
#endif
                if (read[i]<0) {
                    if (i>0)
                        rvmessage("input does not contain a whole number of blocks: %d/64", i);
                    break;
                }
            }

            if (read[i]<0)
                break;

            for (i=0; i<64; i++)
                if (fwrite(line[rowcol[i]], read[rowcol[i]], 1, fileout) != 1)
                    break;
        }
        if (ferror(filein))
            rverror("failed to read from input");
        if (ferror(fileout) && errno!=EPIPE)
            rverror("failed to write to output");

        if (numfiles)
            fclose(filein);

    } while (fileindex<numfiles && !ferror(fileout));

    /* tidy up */
    int i;
    for (i=0; i<64; i++)
        rvfree(line[i]);

    return 0;
}
