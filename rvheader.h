
#ifndef _RVHEADER_H_
#define _RVHEADER_H_

#include "rvbits.h"

/* video_object_shape */
#define RECTANGULAR 0
#define BINARY      1
#define BINARY_ONLY 2
#define GRAYSCALE   3

/* sprint_enable */
#define STATIC 1
#define GMC 2

/* vop_coding_type */
#define VOP_I 0
#define VOP_P 1
#define VOP_B 2
#define VOP_S 3

/* jpeg huffman table */
struct huffman_table {
    int l[16];
    int v[16][256];
};

/* mpeg4 complexity estimation header */
struct ce_header {
    int estimation_method;
    int opaque;
    int transparent;
    int intra_cae;
    int inter_cae;
    int no_update;
    int upsampling;
    int intra_blocks;
    int inter_blocks;
    int inter4v_blocks;
    int not_coded_blocks;
    int dct_coefs;
    int dct_lines;
    int vlc_symbols;
    int vlc_bits;
    int apm;
    int npm;
    int interpolate_mc_q;
    int forw_back_mc_q;
    int halfpel2;
    int halfpel4;
    int sadct;
    int quarterpel;
};

/* bitmap headers */
int parse_bmp(struct bitbuf *bb, int *width, int *height, int *bits, int *compression);
int parse_sgi(struct bitbuf *bb, int *width, int *height, int *channels, int *rle, int *bpc);

/* avi headers */
int parse_list(struct bitbuf *bb, int verbose, int *type, int size);
int parse_avih(struct bitbuf *bb, int verbose, int *frames, int *width, int *height);
int parse_strh(struct bitbuf *bb, int verbose, int index, int *type);
int parse_strf_vids(struct bitbuf *bb, int verbose);
int parse_strf_auds(struct bitbuf *bb, int verbose);
int parse_strn(struct bitbuf *bb, int verbose, int size);

/* jpeg headers */
int parse_sof(struct bitbuf *bb, int *width, int *height, chromaformat_t *format);
int parse_dht(struct bitbuf *bb, struct huffman_table (*t)[2][4]);
int parse_263(struct bitbuf *bb, int *width, int *height, int *pct, int *umv, int *sac, int *apm, int *pb,
              int *aic, int *df, int *ss, int *rps, int *isd, int *aiv, int *mq, int *ipb, int *rpr, int *rru);

/* mpeg 4 headers */
int parse_user_data(struct bitbuf *bb, int *bytes, char *user_data, int num_elements);
int parse_visual_object_sequence(struct bitbuf *bb, int *profile);
int parse_visual_object(struct bitbuf *bb, int *visual_object_verid, int *visual_object_type);
int parse_vo(struct bitbuf *bb);
int parse_vol(struct bitbuf *bb, int *video_object_type, int *video_object_verid, int *video_object_shape,
              int *vti_size, int *width, int *height, int *interlaced, int *obmc_disable, int *sprite_enable,
              int *quant_type, int *quarter_sample, int *data_partitioned, int *rvlc,
              int *scalability, int *enhancement_type, int *ce_disable, struct ce_header *ce);
int parse_group_of_vop(struct bitbuf *bb);
int parse_vop(struct bitbuf *bb, int vti_size, int video_object_shape, int sprite_enable, int scalability, int enhancement_type,
              int ce_disable, struct ce_header *ce, int interlaced, int *vop_coding_type, int *vti, int *fcode);
int parse_vp(struct bitbuf *bb, int video_object_shape, int sprite_enable, int vop_coding_type, int vti_size);

#endif
