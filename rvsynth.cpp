/*
 * Description: Synthesize a raw yuv data stream.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2010-01-22 15:52:40 $
 * Revision   : $Revision: 1.4 $
 * Copyright  : (c) 2005 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include "getopt.h"

#include "rvutil.h"

const char *images[] = {"black", "white", "grey", "blue", "red", "green", "rowcount", "colcount", "macrocount", "whitenoise", "colournoise", "colourbars"};

enum image_t {
    BLACK = 0,
    WHITE,
    GREY,
    BLUE,
    RED,
    GREEN,
    ROWCOUNT,
    COLCOUNT,
    MACROCOUNT,
    WHITENOISE,
    COLOURNOISE,
    COLOURBARS,
};


const char *appname;

void usage(int exitcode)
{
    fprintf(stderr, "%s: synthesize a raw yuv data stream\n", appname);
    fprintf(stderr, "usage: %s [options] [<file>] [<file>...]\n", appname);
    fprintf(stderr, "  -i, --image         : synthesis image format, \n");
    fprintf(stderr, "     (formats : black, white, grey, blue, red, green, rowcount, colcount, macrocount, whitenoise, colournoise, colourbars)\n");
    fprintf(stderr, "  -s, --size          : image size format\n");
    fprintf(stderr, "     (formats : %s)\n", rvformats);
    fprintf(stderr, "  -p, --fourcc        : image pixel format fourcc (default: I420)\n");
    fprintf(stderr, "     (formats : %s)\n", rvfourccs);
    fprintf(stderr, "  -n, --numframes     : number of frames (default: 1)\n");
    fprintf(stderr, "  -o, --output        : write output to file\n");
    fprintf(stderr, "  -q, --quiet         : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "  -v, --verbose       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "  --                  : disable argument processing\n");
    fprintf(stderr, "  -h, --help, --usage : print this usage message\n");
    exit(exitcode);
}

int main(int argc, char *argv[])
{
    int i;
    FILE *fileout = stdout;
    char *outfile = NULL;

    /* data buffer */
    int size = 1024;
    unsigned char *data = NULL;

    /* command line defaults */
    const char *image = images[0];
    int width = 0;
    int height = 0;
    const char *format = NULL;
    const char *fccode = NULL;
    int numframes = 1;
    int verbose = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"image",     1, NULL, 'f'},
            {"size",      1, NULL, 's'},
            {"fourcc",    1, NULL, 'p'},
            {"numframes", 1, NULL, 'n'},
            {"output",    1, NULL, 'o'},
            {"quiet",     0, NULL, 'q'},
            {"verbose",   0, NULL, 'v'},
            {"usage",     0, NULL, 'h'},
            {"help",      0, NULL, 'h'},
            {NULL,        0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "i:s:p:n:o:qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 'i':
                image = optarg;
                break;

            case 's':
                format = optarg;
                break;

            case 'p':
                fccode = optarg;
                break;

            case 'n':
                numframes = atoi(optarg);
                if (numframes<=0)
                    rvexit("invalid value for numframes: %d", numframes);
                break;

            case 'o':
                outfile = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* no non-options */
    if (optind<argc)
        usage(1);

    /* lookup image type */
    enum image_t type = (enum image_t)-1;
    for (i=0; i<sizeof(images)/sizeof(char *); i++)
        if (strcasecmp(image, images[i])==0)
            type = (enum image_t)i;
    if (type==-1)
        rvexit("unknown image type: %s", image);
    if (verbose>=2)
        rvmessage("synthesizing image type: %s", images[type]);

    /* determine output image dimensions */
    if (divine_image_dims(&width, &height, NULL, NULL, NULL, format, outfile)<0)
        rvexit("could not determine image size");

    /* convert fourcc string into integer */
    unsigned int fourcc = I420;
    if (fccode)
        fourcc = get_fourcc(fccode);
    if (fourcc!=I420 && fourcc!=I422 && fourcc!=I444)
        rvexit("unsupported fourcc, only planar formats are supported: %s", describe_fourcc(fourcc));

    /* get chroma subsampling from fourcc */
    int hsub, vsub;
    get_chromasub(fourcc, &hsub, &vsub);
    if (hsub==-1 || vsub==-1)
        rvexit("unknown pixel format: %s", fourccname(fourcc));

    /* open output file */
    if (outfile)
        fileout = fopen(outfile, "wb");
    if (fileout==NULL)
        rverror("failed to open output file \"%s\"", outfile);

    /* ignore SIGPIPE on output file */
    if (signal(SIGPIPE, SIG_IGN)==SIG_ERR)
        rverror("failed to set ignore on SIGPIPE");

    /* allocate data buffer */
    size = get_framesize(width, height, hsub, vsub, 8);
    data = (unsigned char *)rvalloc(NULL, size*sizeof(unsigned char), 0);

    /* use pointers to each plane */
    int length[3] = {width*height, width*height/hsub/vsub, width*height/hsub/vsub};
    unsigned char *plane[3] = {data, data+length[0], data+length[0]+length[1]};

    /* synthesize image data */
    switch (type) {
        case BLACK:
            memset(plane[0], 0  , length[0]);
            memset(plane[1], 128, length[1]);
            memset(plane[2], 128, length[2]);
            /* loop over frames */
            for (i=0; i<numframes; i++)
                if (fwrite(data, size, 1, fileout) != 1)
                    break;
            break;

        case WHITE:
            memset(plane[0], 255, length[0]);
            memset(plane[1], 128, length[1]);
            memset(plane[2], 128, length[2]);
            /* loop over frames */
            for (i=0; i<numframes; i++)
                if (fwrite(data, size, 1, fileout) != 1)
                    break;
            break;

        case GREY:
            memset(plane[0], 128, length[0]);
            memset(plane[1], 128, length[1]);
            memset(plane[2], 128, length[2]);
            /* loop over frames */
            for (i=0; i<numframes; i++)
                if (fwrite(data, size, 1, fileout) != 1)
                    break;
            break;

        case BLUE:
            memset(plane[0], 16 , length[0]);
            memset(plane[1], 240, length[1]);
            memset(plane[2], 128, length[2]);
            /* loop over frames */
            for (i=0; i<numframes; i++)
                if (fwrite(data, size, 1, fileout) != 1)
                    break;
            break;

        case RED:
            memset(plane[0], 16 , length[0]);
            memset(plane[1], 128, length[1]);
            memset(plane[2], 240, length[2]);
            /* loop over frames */
            for (i=0; i<numframes; i++)
                if (fwrite(data, size, 1, fileout) != 1)
                    break;
            break;

        case GREEN:
            memset(plane[0], 128, length[0]);
            memset(plane[1], 16 , length[1]);
            memset(plane[2], 16 , length[2]);
            /* loop over frames */
            for (i=0; i<numframes; i++)
                if (fwrite(data, size, 1, fileout) != 1)
                    break;
            break;

        case ROWCOUNT:
        {
            int x, y;
            for (y=0; y<height; y++)
                for (x=0; x<width; x++)
                    plane[0][y*width+x] = x & 0xff;
            for (y=0; y<height/vsub; y++)
                for (x=0; x<width/hsub; x++) {
                    plane[1][y*width/hsub+x] = (x*hsub) & 0xff;
                    plane[2][y*width/hsub+x] = (x*hsub) & 0xff;
                }
            /* loop over frames */
            for (i=0; i<numframes; i++)
                if (fwrite(data, size, 1, fileout) != 1)
                    break;
            break;
        }

        case COLCOUNT:
        {
            int x, y;
            for (y=0; y<height; y++)
                for (x=0; x<width; x++)
                    plane[0][y*width+x] = y & 0xff;
            for (y=0; y<height/vsub; y++)
                for (x=0; x<width/hsub; x++) {
                    plane[1][y*width/hsub+x] = (y*hsub) & 0xff;
                    plane[2][y*width/hsub+x] = (y*hsub) & 0xff;
                }
            /* loop over frames */
            for (i=0; i<numframes; i++)
                if (fwrite(data, size, 1, fileout) != 1)
                    break;
            break;
        }

        case MACROCOUNT:
        {
            int mbx, mby, x, y;
            for (mby=0; mby<height; mby+=16)
                for (mbx=0; mbx<width; mbx+=16)
                    for (y=0; y<16; y++)
                        if (mby+y<height)
                            for (x=0; x<16; x++)
                                if (mbx+x<width)
                                    plane[0][mby*width+mbx+y*width+x] = y*16+x;
            for (mby=0; mby<height/vsub; mby+=16/vsub)
                for (mbx=0; mbx<width/hsub; mbx+=16/hsub)
                    for (y=0; y<16/vsub; y++)
                        if (mby+y<height/vsub)
                            for (x=0; x<16/hsub; x++)
                                if (mbx+x<width/hsub) {
                                    plane[1][mby*width/hsub+mbx+y*width/hsub+x] = y*16+x*hsub;
                                    plane[2][mby*width/hsub+mbx+y*width/hsub+x] = y*16+x*hsub;
                                }
            /* loop over frames */
            for (i=0; i<numframes; i++)
                if (fwrite(data, size, 1, fileout) != 1)
                    break;
            break;
        }

        case WHITENOISE:
        {
            /* loop over frames */
            for (i=0; i<numframes; i++) {
                int j;
                for (j=0; j<length[0]; j++)
                    plane[0][j] = (rand()>>2) % 256;
                memset(plane[1], 128, length[1]);
                memset(plane[2], 128, length[2]);
                if (fwrite(data, size, 1, fileout) != 1)
                    break;
            }
            break;
        }

        case COLOURNOISE:
        {
            /* loop over frames */
            for (i=0; i<numframes; i++) {
                int j;
                for (j=0; j<length[0]; j++)
                    plane[0][j] = (rand()>>2) % 256;
                for (j=0; j<length[1]; j++)
                    plane[1][j] = (rand()>>2) % 256;
                for (j=0; j<length[2]; j++)
                    plane[2][j] = (rand()>>2) % 256;
                if (fwrite(data, size, 1, fileout) != 1)
                    break;
            }
            break;
        }

        /* 100% Colour Bars */
        case COLOURBARS:
        {
            int barwidth = width/8;
            for (i=0; i<height; i++) {
                memset(plane[0]+i*width+0*barwidth, 235, barwidth);
                memset(plane[0]+i*width+1*barwidth, 181, barwidth);
                memset(plane[0]+i*width+2*barwidth, 153, barwidth);
                memset(plane[0]+i*width+3*barwidth, 126, barwidth);
                memset(plane[0]+i*width+4*barwidth,  98, barwidth);
                memset(plane[0]+i*width+5*barwidth,  71, barwidth);
                memset(plane[0]+i*width+6*barwidth,  43, barwidth);
                memset(plane[0]+i*width+7*barwidth,  16, width-7*barwidth);
            }
            barwidth = width/hsub/8;
            for (i=0; i<height/vsub; i++) {
                memset(plane[1]+i*width/hsub+0*barwidth, 128, barwidth);
                memset(plane[1]+i*width/hsub+1*barwidth,  43, barwidth);
                memset(plane[1]+i*width/hsub+2*barwidth, 153, barwidth);
                memset(plane[1]+i*width/hsub+3*barwidth,  71, barwidth);
                memset(plane[1]+i*width/hsub+4*barwidth, 181, barwidth);
                memset(plane[1]+i*width/hsub+5*barwidth,  98, barwidth);
                memset(plane[1]+i*width/hsub+6*barwidth, 235, barwidth);
                memset(plane[1]+i*width/hsub+7*barwidth, 128, width-7*barwidth);
            }
            for (i=0; i<height/vsub; i++) {
                memset(plane[2]+i*width/hsub+0*barwidth, 128, barwidth);
                memset(plane[2]+i*width/hsub+1*barwidth, 153, barwidth);
                memset(plane[2]+i*width/hsub+2*barwidth,  43, barwidth);
                memset(plane[2]+i*width/hsub+3*barwidth,  43, barwidth);
                memset(plane[2]+i*width/hsub+4*barwidth, 181, barwidth);
                memset(plane[2]+i*width/hsub+5*barwidth, 181, barwidth);
                memset(plane[2]+i*width/hsub+6*barwidth, 128, barwidth);
                memset(plane[2]+i*width/hsub+7*barwidth, 128, width-7*barwidth);
            }
            /* loop over frames */
            for (i=0; i<numframes; i++)
                if (fwrite(data, size, 1, fileout) != 1)
                    break;
            break;
        }

        default:
            rvexit("unsupported image format: %s", images[type]);
            break;
    }
    if (ferror(fileout) && errno!=EPIPE)
        rverror("failed to write to output");

    /* tidy up */
    rvfree(data);

    if (verbose>=0)
        rvmessage("synthesized %d frames", numframes);

    return 0;
}
