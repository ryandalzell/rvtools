/*
 * Description: Functions for transforming to and from the frequency domain.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-06-23 13:26:19 $
 * Revision   : $Revision: 1.3 $
 * Copyright  : (c) 2005 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "rvcodec.h"

/*************************************************************
Copyright (C) 1990, 1991, 1993 Andy C. Hung, all rights reserved.
PUBLIC DOMAIN LICENSE: Stanford University Portable Video Research
Group. If you use this software, you agree to the following: This
program package is purely experimental, and is licensed "as is".
Permission is granted to use, modify, and distribute this program
without charge for any purpose, provided this license/ disclaimer
notice appears in the copies.  No warranty or maintenance is given,
either expressed or implied.  In no event shall the author(s) be
liable to you or a third party for any special, incidental,
consequential, or other damages, arising out of the use or inability
to use the program for any purpose (or the loss of data), even if we
have been advised of such possibilities.  Any public reference or
advertisement of this source code should refer to it as the Portable
Video Research Group (PVRG) code, and not by any author(s) (or
Stanford University) name.
*************************************************************/

static const double idct_matrix[] = {
    0.3535533905932737,
    0.4903926402016152,
    0.4619397662556434,
    0.4157348061512727,
    0.3535533905932738,
    0.2777851165098011,
    0.1913417161825449,
    0.0975451610080642,
    0.3535533905932737,
    0.4157348061512727,
    0.1913417161825449,
   -0.0975451610080641,
   -0.3535533905932737,
   -0.4903926402016152,
   -0.4619397662556434,
   -0.2777851165098011,
    0.3535533905932737,
    0.2777851165098011,
   -0.1913417161825449,
   -0.4903926402016152,
   -0.3535533905932738,
    0.0975451610080641,
    0.4619397662556432,
    0.4157348061512728,
    0.3535533905932737,
    0.0975451610080642,
   -0.4619397662556434,
   -0.2777851165098011,
    0.3535533905932737,
    0.4157348061512728,
   -0.1913417161825449,
   -0.4903926402016153,
    0.3535533905932737,
   -0.0975451610080641,
   -0.4619397662556434,
    0.2777851165098009,
    0.3535533905932738,
   -0.4157348061512726,
   -0.1913417161825453,
    0.4903926402016152,
    0.3535533905932737,
   -0.2777851165098010,
   -0.1913417161825452,
    0.4903926402016153,
   -0.3535533905932733,
   -0.0975451610080649,
    0.4619397662556437,
   -0.4157348061512720,
    0.3535533905932737,
   -0.4157348061512727,
    0.1913417161825450,
    0.0975451610080640,
   -0.3535533905932736,
    0.4903926402016152,
   -0.4619397662556435,
    0.2777851165098022,
    0.3535533905932737,
   -0.4903926402016152,
    0.4619397662556432,
   -0.4157348061512721,
    0.3535533905932733,
   -0.2777851165098008,
    0.1913417161825431,
   -0.0975451610080625
};

static int transpose_index[] = {
    0, 8,  16, 24, 32, 40, 48, 56,
    1, 9,  17, 25, 33, 41, 49, 57,
    2, 10, 18, 26, 34, 42, 50, 58,
    3, 11, 19, 27, 35, 43, 51, 59,
    4, 12, 20, 28, 36, 44, 52, 60,
    5, 13, 21, 29, 37, 45, 53, 61,
    6, 14, 22, 30, 38, 46, 54, 62,
    7, 15, 23, 31, 39, 47, 55, 63
};


static void reference_idct_1d(double *source, double *result)
{
    const double *m = idct_matrix;
    double *s, *r;

    for (r = result; r < result + 8; r++) {
        for (*r = 0, s = source; s < source + 8; s++, m++) {
            *r += (*s) * (*m);
        }
    }
}

static void transpose(double *source, double *result)
{
    int *p;

    for (p = transpose_index; p < transpose_index + 64; p++)
        *(result++) = source[*p];
}


void reference_idct(const coeff_t *coeff, diff_t *error)
{
    const coeff_t *cp;
    diff_t *mp;
    double *sp, *rp;
    double source[64], result[64];

    for (sp = source, cp = coeff; cp < coeff + 64; cp++, sp++)
        *sp = (double) *cp;

    for (rp = result, sp = source; sp < source + 64; sp += 8, rp += 8)
        reference_idct_1d(sp, rp);

    transpose(result, source);

    for (rp = result, sp = source; sp < source + 64; sp += 8, rp += 8)
        reference_idct_1d(sp, rp);

    transpose(result, source);

    for (sp = source, mp = error; mp < error + 64; sp++, mp++)
        *mp = (diff_t) (*sp > 0 ? (*(sp) + 0.5) : (*(sp) - 0.5));
}

/* chen macros */
#define NO_MULTIPLY
#ifdef NO_MULTIPLY
#define LS(r,s) ((r) << (s))
#define RS(r,s) ((r) >> (s))    /* Caution with rounding... */
#else
#define LS(r,s) ((r) * (1 << (s)))
#define RS(r,s) ((r) / (1 << (s)))  /* Correct rounding */
#endif
#define MSCALE(expr)  RS((expr),9)

/* cos constants */
#define c1d4 362L
#define c1d8 473L
#define c3d8 196L
#define c1d16 502L
#define c3d16 426L
#define c5d16 284L
#define c7d16 100L

/*
 * VECTOR_DEFINITION makes the temporary variables vectors.
 * Useful for machines with small register spaces.
 */
#define VECTOR_DEFINITION
#ifdef VECTOR_DEFINITION
#define a0 a[0]
#define a1 a[1]
#define a2 a[2]
#define a3 a[3]
#define b0 b[0]
#define b1 b[1]
#define b2 b[2]
#define b3 b[3]
#define c0 c[0]
#define c1 c[1]
#define c2 c[2]
#define c3 c[3]
#endif

/*
************************************************************
chendct.c

A simple DCT algorithm that seems to have fairly nice arithmetic
properties.

W. H. Chen, C. H. Smith and S. C. Fralick "A fast computational
algorithm for the discrete cosine transform," IEEE Trans. Commun.,
vol. COM-25, pp. 1004-1009, Sept 1977.

************************************************************
*/

void chen_idct(const coeff_t *coeff, diff_t *error)
{
    register int i;
    register int *aptr;
#ifdef VECTOR_DEFINITION
    int a[4];
    int b[4];
    int c[4];
#else
    register int a0, a1, a2, a3;
    register int b0, b1, b2, b3;
    register int c0, c1, c2, c3;
#endif
    int x[64], y[64];

    /* copy input to workspace */
    for (i=0; i<64; i++)
        x[i] = (int) coeff[i];

    /* loop over columns */
    for (i=0; i<8; i++) {
        aptr = x + i;
        b0 = LS(*aptr, 2);
        aptr += 8;
        a0 = LS(*aptr, 2);
        aptr += 8;
        b2 = LS(*aptr, 2);
        aptr += 8;
        a1 = LS(*aptr, 2);
        aptr += 8;
        b1 = LS(*aptr, 2);
        aptr += 8;
        a2 = LS(*aptr, 2);
        aptr += 8;
        b3 = LS(*aptr, 2);
        aptr += 8;
        a3 = LS(*aptr, 2);

        /* split into even mode  b0 = x0  b1 = x4  b2 = x2  b3 = x6.
           and the odd terms a0 = x1 a1 = x3 a2 = x5 a3 = x7
         */

        c0 = MSCALE((c7d16 * a0) - (c1d16 * a3));
        c1 = MSCALE((c3d16 * a2) - (c5d16 * a1));
        c2 = MSCALE((c3d16 * a1) + (c5d16 * a2));
        c3 = MSCALE((c1d16 * a0) + (c7d16 * a3));

        /* first Butterfly on even terms */

        a0 = MSCALE(c1d4 * (b0 + b1));
        a1 = MSCALE(c1d4 * (b0 - b1));

        a2 = MSCALE((c3d8 * b2) - (c1d8 * b3));
        a3 = MSCALE((c1d8 * b2) + (c3d8 * b3));

        b0 = a0 + a3;
        b1 = a1 + a2;
        b2 = a1 - a2;
        b3 = a0 - a3;

        /* second Butterfly */

        a0 = c0 + c1;
        a1 = c0 - c1;
        a2 = c3 - c2;
        a3 = c3 + c2;

        c0 = a0;
        c1 = MSCALE(c1d4 * (a2 - a1));
        c2 = MSCALE(c1d4 * (a2 + a1));
        c3 = a3;

        aptr = y + i;
        *aptr = b0 + c3;
        aptr += 8;
        *aptr = b1 + c2;
        aptr += 8;
        *aptr = b2 + c1;
        aptr += 8;
        *aptr = b3 + c0;
        aptr += 8;
        *aptr = b3 - c0;
        aptr += 8;
        *aptr = b2 - c1;
        aptr += 8;
        *aptr = b1 - c2;
        aptr += 8;
        *aptr = b0 - c3;
    }

    /* loop over rows */
    for (i = 0; i < 8; i++) {
        aptr = y + LS(i, 3);
        b0 = *(aptr++);
        a0 = *(aptr++);
        b2 = *(aptr++);
        a1 = *(aptr++);
        b1 = *(aptr++);
        a2 = *(aptr++);
        b3 = *(aptr++);
        a3 = *(aptr);

        /*
           split into even mode  b0 = x0  b1 = x4  b2 = x2  b3 = x6.
           and the odd terms a0 = x1 a1 = x3 a2 = x5 a3 = x7
         */

        c0 = MSCALE((c7d16 * a0) - (c1d16 * a3));
        c1 = MSCALE((c3d16 * a2) - (c5d16 * a1));
        c2 = MSCALE((c3d16 * a1) + (c5d16 * a2));
        c3 = MSCALE((c1d16 * a0) + (c7d16 * a3));

        /* first Butterfly on even terms */

        a0 = MSCALE(c1d4 * (b0 + b1));
        a1 = MSCALE(c1d4 * (b0 - b1));

        a2 = MSCALE((c3d8 * b2) - (c1d8 * b3));
        a3 = MSCALE((c1d8 * b2) + (c3d8 * b3));

        /* calculate last set of b's */

        b0 = a0 + a3;
        b1 = a1 + a2;
        b2 = a1 - a2;
        b3 = a0 - a3;

        /* second Butterfly */

        a0 = c0 + c1;
        a1 = c0 - c1;
        a2 = c3 - c2;
        a3 = c3 + c2;

        c0 = a0;
        c1 = MSCALE(c1d4 * (a2 - a1));
        c2 = MSCALE(c1d4 * (a2 + a1));
        c3 = a3;

        aptr = y + LS(i, 3);
        *(aptr++) = b0 + c3;
        *(aptr++) = b1 + c2;
        *(aptr++) = b2 + c1;
        *(aptr++) = b3 + c0;
        *(aptr++) = b3 - c0;
        *(aptr++) = b2 - c1;
        *(aptr++) = b1 - c2;
        *(aptr) = b0 - c3;
    }

    /* copy workspace to output with correct accuracy */
    /* we have additional factor of 16 that must be removed */
    for (i=0, aptr=y; i<64; i++, aptr++)
        error[i] = (diff_t) (((*aptr < 0) ? (*aptr - 8) : (*aptr + 8)) / 16);

}

/**********************************************************************
 * Software Copyright Licensing Disclaimer
 *
 * This software module was originally developed by contributors to the
 * course of the development of ISO/IEC 14496-10 for reference purposes
 * and its performance may not have been optimized.  This software
 * module is an implementation of one or more tools as specified by
 * ISO/IEC 14496-10.  ISO/IEC gives users free license to this software
 * module or modifications thereof. Those intending to use this software
 * module in products are advised that its use may infringe existing
 * patents.  ISO/IEC have no liability for use of this software module
 * or modifications thereof.  The original contributors retain full
 * rights to modify and use the code for their own purposes, and to
 * assign or donate the code to third-parties.
 *
 * This copyright notice must be included in all copies or derivative
 * works.  Copyright (c) ISO/IEC 2004, 2005, 2006, 2007, 2008.
 **********************************************************************/
/*
***********************************************************************
* COPYRIGHT AND WARRANTY INFORMATION
*
* Copyright 2001, International Telecommunications Union, Geneva
*
* DISCLAIMER OF WARRANTY
*
* These software programs are available to the user without any
* license fee or royalty on an "as is" basis. The ITU disclaims
* any and all warranties, whether express, implied, or
* statutory, including any implied warranties of merchantability
* or of fitness for a particular purpose.  In no event shall the
* contributor or the ITU be liable for any incidental, punitive, or
* consequential damages of any kind whatsoever arising from the
* use of these programs.
*
* This disclaimer of warranty extends to the user of these programs
* and user's customers, employees, agents, transferees, successors,
* and assigns.
*
* The ITU does not represent or warrant that the programs furnished
* hereunder are free of infringement of any third-party patents.
* Commercial implementations of ITU-T Recommendations, including
* shareware, may be subject to royalty fees to patent holders.
* Information regarding the ITU-T patent policy is available from
* the ITU Web site at http://www.itu.int.
*
* THIS IS NOT A GRANT OF PATENT RIGHTS - SEE THE ITU-T PATENT POLICY.
************************************************************************
*/
void ihadamard_4x4(coeff_t tblock[4][4], coeff_t block[4][4])
{
  int i;
  coeff_t p0,p1,p2,p3;
  coeff_t t[4][4];

  /* horizontal pass */
  for (i= 0; i<4; i++) {
    p0 = tblock[i][0] + tblock[i][2];
    p1 = tblock[i][0] - tblock[i][2];
    p2 = tblock[i][1] - tblock[i][3];
    p3 = tblock[i][1] + tblock[i][3];

    t[i][0] = p0 + p3;
    t[i][1] = p1 + p2;
    t[i][2] = p1 - p2;
    t[i][3] = p0 - p3;
  }

  /* vertical pass */
  for (i=0; i<4; i++) {
    p0 = t[0][i] + t[2][i];
    p1 = t[0][i] - t[2][i];
    p2 = t[1][i] - t[3][i];
    p3 = t[1][i] + t[3][i];

    block[0][i] = p0 + p3;
    block[1][i] = p1 + p2;
    block[2][i] = p1 - p2;
    block[3][i] = p0 - p3;
  }
}
