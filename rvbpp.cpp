/*
 * Description: Convert bits-per-pixel depths in raw video files.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-04-27 15:56:10 $
 * Revision   : $Revision: 1.5 $
 * Copyright  : (c) 2006 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include "getopt.h"

#include "rvutil.h"

const char *appname;

void usage(int exitcode)
{
    fprintf(stderr, "%s: convert bits-per-pixel depths\n", appname);
    fprintf(stderr, "usage: %s [options] [<file>] [<file>...]\n", appname);
    fprintf(stderr, "\t-f <num> : pixel depth to convert from (default: 8)\n");
    fprintf(stderr, "\t-t <num> : pixel depth to convert to (default: 8)\n");
    fprintf(stderr, "\t-o <num> : write output to file\n");
    fprintf(stderr, "\t-q       : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "\t-v       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "\t--       : disable argument processing\n");
    fprintf(stderr, "\t-h       : print this usage message\n");
    exit(exitcode);
}

int main(int argc, char *argv[])
{
    FILE *filein = stdin;
    FILE *fileout = stdout;
    char *filename[RV_MAXFILES] = {0};
    char *outfile = NULL;
    int fileindex = 0;
    int numfiles = 0;

    /* command line defaults */
    int inp_bpp = 8;
    int out_bpp = 8;
    int verbose = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"input-bpp",  1, NULL, 'f'},
            {"output-bpp", 1, NULL, 't'},
            {"output",     1, NULL, 'o'},
            {"quiet",      0, NULL, 'q'},
            {"verbose",    0, NULL, 'v'},
            {"usage",      0, NULL, 'h'},
            {"help",       0, NULL, 'h'},
            {NULL,         0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "f:t:o:qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 'f':
                inp_bpp = atoi(optarg);
                if (inp_bpp<8 || inp_bpp>16)
                    rvexit("invalid value for input bpp: %d", inp_bpp);
                break;

            case 't':
                out_bpp = atoi(optarg);
                if (out_bpp<8 || out_bpp>16)
                    rvexit("invalid value for output bpp: %d", out_bpp);
                break;

            case 'o':
                outfile = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < sizeof(filename)/sizeof(filename[0]))
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    /* open output file */
    if (outfile)
        fileout = fopen(outfile, "wb");
    if (fileout==NULL)
        rverror("failed to open output file \"%s\"", outfile);

    /* ignore SIGPIPE on output file */
    if (signal(SIGPIPE, SIG_IGN)==SIG_ERR)
        rverror("failed to set ignore on SIGPIPE");

    int inp_pelsize = (inp_bpp+7)/8;
    int out_pelsize = (out_bpp+7)/8;

    /* loop over input files */
    do {
        if (numfiles)
            filein = fopen(filename[fileindex], "rb");
        if (filein==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex]);

        while (!feof(filein) && !ferror(filein))
        {
            unsigned int data = 0;
            int read = fread(&data, inp_pelsize, 1, filein);
            if (read)
            {
                if (inp_bpp<out_bpp)
                    data <<= out_bpp-inp_bpp;
                if (inp_bpp>out_bpp)
                    data >>= inp_bpp-out_bpp;

                if (fwrite(&data, out_pelsize, 1, fileout) != 1)
                    break;
            }
        }
        if (ferror(filein))
            rverror("failed to read from input");
        if (ferror(fileout) && errno!=EPIPE)
            rverror("failed to write to output");

        if (numfiles)
            fclose(filein);

    } while (++fileindex<numfiles && !ferror(fileout));

    return 0;
}
