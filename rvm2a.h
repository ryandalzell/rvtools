
#ifndef _RVMPEG2A_H_
#define _RVMPEG2A_H_

#include "rvbits.h"

/* lookup bitrate from index in frame header */
extern int m2a_bit_rate_table[2][3][16];

/* lookup sampling rate from index in frame header */
extern int m2a_sampling_rate_table[4][3];

/* id3 utility functions */
int skip_id3(struct bitbuf *bb);

#endif
