# Makefile for rvtools
# Ryan Dalzell, 6th Oct 2004

# Build configuration
BINDIR = /usr/local/bin

ifeq ($(HOST),windows)
# MinGW32 build configuration
CC = i586-mingw32msvc-cc 
AR = i586-mingw32msvc-ar
EXE = .exe
else
ifeq ($(HOST),arm)
CC = arm-none-linux-gnueabi-gcc
CXX= arm-none-linux-gnueabi-g++
AR = arm-none-linux-gnueabi-ar
endif
endif

# Targets
CTARGETS=
CXXTARGETS=rvskel rverrors rvtxt rv8x4 rvreorder rvinfo rvsplice rvcrop rvconv rvscale rvquality rvvis rvfield rvbpp rvdecode rvdemux rvtrim rvcheck rvsynth rvstream rvnup rvencap rvrerate rvdeint rvdenoise rv10bit rvclean
ifeq ($(HOST),)
CXXTARGETS+=rvtsmon 
endif

# CFLAGS
CFLAGS=-std=gnu99 -pedantic -Wall -D_FILE_OFFSET_BITS=64 -g #-fgnu89-inline
#CFLAGS+=-march=nocona -ftree-vectorizer-verbose=1 -ftree-vectorize
CXXFLAGS=-std=c++0x -Wall -D_FILE_OFFSET_BITS=64 -D_POSIX_C_SOURCE=199309L -g -Wno-sign-compare

# Library files
LIBRARY=librvtools.a
LIBOBJS=rvutil.o rvy4m.o rvbits.o rvheader.o rvstat.o rvimage.o rvimage-scale.o rvlogfile.o rvh261.o rvm2v.o rvh264.o rvvc1.o rvendian.o rvcodec.o rvtransform.o rvmp2.o rvasf.o rvrecon.o my_getopt.o rvcrc.o rvm2a.o rvhevc.o rvdenoise-ksvd.o

LUDH264=ludh264/ludh264.a

# LFLAGS
LFLAGS=-lm -lrt
rvstream : LFLAGS+=-lpthread

# Conditional on installed packages
ifeq ($(HOST),)
ifneq ($(wildcard /usr/include/X11/Xlib.h),)
CXXTARGETS += rvplay
LIBOBJS += rvdisplay.o yuv2rgb.o 
rvplay   : LFLAGS+=-lX11 -L/usr/X11R6/lib
endif
ifneq ($(wildcard libde265/libde265/de265.h),)
CXXFLAGS += -DHAVE_LIBDE265 -Ilibde265
rvdecode : LFLAGS+=libde265/libde265/.libs/libde265.a -lpthread
rvplay   : LFLAGS+=libde265/libde265/.libs/libde265.a -lpthread
endif
endif
rvtsmon  : LFLAGS+=-lncurses

# Generic targets
all    : $(CTARGETS) $(CXXTARGETS)
all    : CFLAGS += -O2 -ffast-math -fomit-frame-pointer -DNDEBUG
all    : CXXFLAGS += -O2 -ffast-math -fomit-frame-pointer -DNDEBUG
debug  : $(CTARGETS) $(CXXTARGETS)
debug  : CFLAGS += -DDEBUG
debug  : CXXLFAGS += -DDEBUG

# Object files
OBJECTS=$(foreach target,$(CTARGETS) $(CXXTARGETS),$(target).o)

# Rules
$(CTARGETS): $(LIBRARY) $(LUDH264)
	$(CC) -o $@$(EXE) $@.o $(LIBRARY) $(LUDH264) $(LFLAGS)

$(CXXTARGETS): $(LIBRARY) $(LUDH264)
	$(CXX) -o $@$(EXE) $@.o $(LIBRARY) $(LUDH264) $(LFLAGS)

my_getopt.o: getopt/my_getopt.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(LIBRARY): $(LIBOBJS)
	$(AR) rcs $@ $?

$(LUDH264): ludh264/bitstream.cpp ludh264/bitstream.h ludh264/bitstream_types.h ludh264/cabac.cpp ludh264/cabac_data.h ludh264/cabac.h ludh264/common.h ludh264/decode.cpp ludh264/decode.h ludh264/decode_image_inter.h ludh264/decode_image_inter.cpp ludh264/decode_image_intra.h ludh264/decode_image_intra.cpp ludh264/decode_image_monothread.cpp ludh264/decode_slice_data.cpp ludh264/decode_slice_data.h ludh264/defaulttables.cpp ludh264/defaulttables.h ludh264/filter_image.cpp ludh264/intmath.cpp ludh264/intmath.h ludh264/inverse_transforms.h ludh264/inverse_transforms.cpp ludh264/ludh264.cpp ludh264/ludh264.h ludh264/motion_compensation.cpp ludh264/motion_compensation.h ludh264/motion_compensation_types.h ludh264/motion_vector.h ludh264/motion_vector.cpp ludh264/output.cpp ludh264/syntax.cpp ludh264/syntax.h ludh264/system.cpp ludh264/system.h ludh264/trace.h ludh264/trace.cpp rvh264.h
	$(MAKE) -C ludh264 $(MAKECMDGOALS)

library: $(LIBRARY)

depend: CXXFLAGS+=-MMD
depend: all

clean:
	$(MAKE) -C ludh264 $@
	rm -f $(CTARGETS) $(CXXTARGETS)
	rm -f $(OBJECTS) $(LIBOBJS)
	rm -f $(LIBRARY) *.d *.exe *~ core

install: all
	install -s $(filter-out rvskel,$(CTARGETS) $(CXXTARGETS)) $(BINDIR)

dist: rvtools.tar.gz

zip: rvtools.zip

rvtools.tar.gz: Makefile *.cpp *.h getopt/*.cpp getopt/*.h getopt/LICENSE ludh264/*.cpp ludh264/*.h ludh264/COPYING ludh264/Makefile BUGS COPYING TODO
	tar zcf $@ -C .. $(foreach i,$^,rvtools/$i)

rvtools.zip: *.exe BUGS COPYING TODO
	zip $@ $^

ChangeLog: Makefile *.cpp *.hpp BUGS TODO
	cvs2cl

# Linker dependencies
$(CTARGETS): % : %.o
$(CXXTARGETS): % : %.o

# Compiler dependencies
my_getopt.o: getopt/my_getopt.cpp getopt/my_getopt.h
rv8x4.o: rv8x4.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h rvy4m.h
rvasf.o: rvasf.cpp rvutil.h rvtypes.h rvbits.h rvcrc.h rvasf.h
rvbits.o: rvbits.cpp rvbits.h rvcrc.h rvutil.h rvtypes.h
rvbpp.o: rvbpp.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h
rvcheck.o: rvcheck.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h rvm2v.h rvcodec.h rvh264.h rvmp2.h
rvclean.o: rvclean.cpp
rvcodec.o: rvcodec.cpp rvutil.h rvtypes.h rvbits.h rvcrc.h rvcodec.h
rvconv.o: rvconv.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h rvheader.h rvendian.h
rvcrc.o: rvcrc.cpp rvcrc.h
rvcrop.o: rvcrop.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h rvy4m.h rvimage.h rvm2v.h rvcodec.h rvlogfile.h \
 rvh264.h rvhevc.h
rvdecode.o: rvdecode.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h rvh261.h rvcodec.h rvh264.h rvm2v.h rvy4m.h \
 ludh264/ludh264.h
rvdeint.o: rvdeint.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h
rvdemux.o: rvdemux.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h rvmp2.h rvheader.h rvasf.h
rvdenoise.o: rvdenoise.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h
rvdenoise-ksvd.o: rvdenoise-ksvd.cpp
rvdisplay.o: rvdisplay.cpp rvutil.h rvtypes.h rvbits.h rvcrc.h \
 rvdisplay.h rvlogfile.h rvh264.h rvcodec.h rvhevc.h rvstat.h rvimage.h \
 rvm2v.h yuv2rgb.h
rvencap.o: rvencap.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h rvm2v.h rvcodec.h rvh264.h rvmp2.h
rvendian.o: rvendian.cpp
rverrors.o: rverrors.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h rvheader.h rvh264.h rvcodec.h
rvfield.o: rvfield.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h rvy4m.h
rvh261.o: rvh261.cpp rvutil.h rvtypes.h rvbits.h rvcrc.h rvcodec.h \
 rvh261.h rvtransform.h
rvh264.o: rvh264.cpp rvutil.h rvtypes.h rvbits.h rvcrc.h rvh264.h \
 rvcodec.h rvtransform.h
rvheader.o: rvheader.cpp rvutil.h rvtypes.h rvbits.h rvcrc.h rvheader.h
rvhevc.o: rvhevc.cpp rvutil.h rvtypes.h rvbits.h rvcrc.h rvhevc.h \
 rvcodec.h rvh264.h
rvimage.o: rvimage.cpp rvutil.h rvtypes.h rvbits.h rvcrc.h rvimage.h \
 rvm2v.h rvcodec.h rvlogfile.h rvh264.h rvhevc.h rvy4m.h
rvimage-scale.o: rvimage-scale.cpp rvimage.h rvm2v.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h rvcodec.h rvlogfile.h rvh264.h rvhevc.h
rvinfo.o: rvinfo.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h rvheader.h rvy4m.h rvendian.h rvh261.h rvcodec.h \
 rvm2v.h rvmp2.h rvh264.h rvhevc.h rvvc1.h rvasf.h rvm2a.h
rvlogfile.o: rvlogfile.cpp rvutil.h rvtypes.h rvbits.h rvcrc.h \
 rvlogfile.h rvh264.h rvcodec.h rvhevc.h
rvm2a.o: rvm2a.cpp rvm2a.h rvbits.h rvcrc.h
rvm2v.o: rvm2v.cpp rvm2v.h rvutil.h rvtypes.h rvbits.h rvcrc.h rvcodec.h \
 rvtransform.h rvrecon.h
rvmp2.o: rvmp2.cpp rvutil.h rvtypes.h rvbits.h rvcrc.h rvmp2.h
rvnup.o: rvnup.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h rvy4m.h rvimage.h rvm2v.h rvcodec.h rvlogfile.h \
 rvh264.h rvhevc.h
rvplay.o: rvplay.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h rvdisplay.h rvlogfile.h rvh264.h rvcodec.h rvhevc.h \
 rvstat.h rvimage.h rvm2v.h rvy4m.h rvheader.h rvh261.h ludh264/ludh264.h
rvquality.o: rvquality.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h rvy4m.h rvstat.h rvimage.h rvm2v.h rvcodec.h \
 rvlogfile.h rvh264.h rvhevc.h
rvrecon.o: rvrecon.cpp rvutil.h rvtypes.h rvbits.h rvcrc.h rvcodec.h
rvreorder.o: rvreorder.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h
rvrerate.o: rvrerate.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h
rvscale.o: rvscale.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h rvimage.h rvm2v.h rvcodec.h rvlogfile.h rvh264.h \
 rvhevc.h rvy4m.h
rvskel.o: rvskel.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h
rvsplice.o: rvsplice.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h rvy4m.h rvm2v.h rvcodec.h rvstat.h rvimage.h \
 rvlogfile.h rvh264.h rvhevc.h
rvstat.o: rvstat.cpp rvstat.h rvimage.h rvm2v.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h rvcodec.h rvlogfile.h rvh264.h rvhevc.h
rvstream.o: rvstream.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h rvmp2.h
rvsynth.o: rvsynth.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h
rvtransform.o: rvtransform.cpp rvcodec.h
rvtrim.o: rvtrim.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h
rvtxt.o: rvtxt.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h rvendian.h
rvutil.o: rvutil.cpp rvutil.h rvtypes.h rvbits.h rvcrc.h rvm2a.h
rvvc1.o: rvvc1.cpp rvutil.h rvtypes.h rvbits.h rvcrc.h rvvc1.h
rvvis.o: rvvis.cpp getopt.h getopt/my_getopt.h rvutil.h rvtypes.h \
 rvbits.h rvcrc.h
rvy4m.o: rvy4m.cpp rvutil.h rvtypes.h rvbits.h rvcrc.h
yuv2rgb.o: yuv2rgb.cpp rvutil.h rvtypes.h rvbits.h rvcrc.h rvdisplay.h \
 rvlogfile.h rvh264.h rvcodec.h rvhevc.h rvstat.h rvimage.h rvm2v.h
ifneq ($(wildcard libde265/libde265/de265.h),)
rvdecode.o: libde265/libde265/de265.h \
 libde265/libde265/de265-version.h
rvdisplay.o: libde265/libde265/de265.h \
 libde265/libde265/de265-version.h libde265/libde265/image.h \
 libde265/libde265/de265.h libde265/libde265/sps.h \
 libde265/libde265/vps.h libde265/libde265/bitstream.h \
 libde265/libde265/refpic.h libde265/libde265/pps.h \
 libde265/libde265/motion.h libde265/libde265/threads.h \
 libde265/libde265/slice.h libde265/libde265/cabac.h \
 libde265/libde265/util.h libde265/libde265/nal.h
rvimage.o: libde265/libde265/de265.h libde265/libde265/de265-version.h \
 libde265/libde265/image.h libde265/libde265/de265.h \
 libde265/libde265/sps.h libde265/libde265/vps.h \
 libde265/libde265/bitstream.h libde265/libde265/refpic.h \
 libde265/libde265/pps.h libde265/libde265/motion.h \
 libde265/libde265/threads.h libde265/libde265/slice.h \
 libde265/libde265/cabac.h libde265/libde265/util.h \
 libde265/libde265/nal.h
rvplay.o: libde265/libde265/de265.h libde265/libde265/de265-version.h \
 libde265/libde265/image.h libde265/libde265/de265.h \
 libde265/libde265/sps.h libde265/libde265/vps.h \
 libde265/libde265/bitstream.h libde265/libde265/refpic.h \
 libde265/libde265/pps.h libde265/libde265/motion.h \
 libde265/libde265/threads.h libde265/libde265/slice.h \
 libde265/libde265/cabac.h libde265/libde265/util.h \
 libde265/libde265/nal.h
endif

