
#ifndef _RVCODEC_H_
#define _RVCODEC_H_

#include <stdint.h>

/* codec types */
typedef int8_t  tcoeff_t;
typedef int16_t coeff_t;
typedef int16_t diff_t;
typedef uint8_t sample_t;
typedef int16_t sampleandavail_t;

/* not available flag */
#define NOT_AVAILABLE (sampleandavail_t)-1
#define AVAILABLE(x) ((x)!=NOT_AVAILABLE)

/* luma or chroma type, true if chroma */
typedef enum {
    LUMA = 0,
    CHROMA
} chroma_t;

/* motion vector type */
struct mv_t {
    int16_t x;
    int16_t y;
};

/* coefficient type */
struct rvtcoeff {
    int run;
    int level;
    int sign;
};

/* matricies */
extern const int zigzag[64];
extern const int zigzag_inverse[64];
extern const int zigzag_alternate[64];
extern const int zigzag_alternate_inverse[64];

/* macros */
#define div2(x) ((x+(x>0))>>1)

/* functions */
void copy_macroblock_into_picture(unsigned char *frame, int width, int height, int chroma_format, int mbx, int mby, sample_t sample[][64]);
sample_t *copy_fields_into_frame(sample_t *topfield, sample_t *botfield, int width, int height, int chroma_format);
sample_t *copy_field_from_frame(sample_t *frame, int topfield, int width, int height, int chroma_format);

#endif
