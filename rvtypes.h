
#ifndef _RVTYPES_H_
#define _RVTYPES_H_

/* define types for specific sizes of integers */
#ifndef _WIN32

#include <inttypes.h>

#else

#ifndef __GNUC__
   /* MSVC/Borland */
   typedef __int8 s_int8_t;
   typedef unsigned __int8 u_int8_t;
   typedef __int16 s_int16_t;
   typedef unsigned __int16 u_int16_t;
   typedef __int32 s_int32_t;
   typedef unsigned __int32 u_int32_t;
   typedef __int64 s_int64_t;
   typedef unsigned __int64 u_int64_t;
#else
   /* MinGW32 */
   typedef char s_int8_t;
   typedef unsigned char u_int8_t;
   typedef short s_int16_t;
   typedef unsigned short u_int16_t;
   typedef int s_int32_t;
   typedef unsigned int u_int32_t;
   typedef long long s_int64_t;
   typedef unsigned long long u_int64_t;
#endif

#endif

#endif
