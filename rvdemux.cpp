/*
 * Description: Demultiplex video and audio container files.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2010-01-29 14:21:28 $
 * Revision   : $Revision: 1.47 $
 * Copyright  : (c) 2007 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <assert.h>
#include <signal.h>

#include "getopt.h"
#include "rvutil.h"
#include "rvbits.h"
#include "rvmp2.h"
#include "rvheader.h"
#include "rvasf.h"

struct streamstat {
    int pid;
    int packets;
    int bytes;
};

const char *appname;

void usage(int exitcode)
{
    fprintf(stderr, "%s: demultiplex video and audio container files\n", appname);
    fprintf(stderr, "usage: %s [options] [<file>] [<file>...]\n", appname);
    fprintf(stderr, "  -t, --type          : force type of file (default: autodetect)\n");
    fprintf(stderr, "  -p, --pid           : packet id of stream or track to output (default: 0)\n");
    fprintf(stderr, "  -s, --stream        : id number of stream or track to output (default: 0)\n");
    fprintf(stderr, "  -o, --output        : write output to file\n");
    fprintf(stderr, "  -q, --quiet         : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "  -v, --verbose       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "  --                  : disable argument processing\n");
    fprintf(stderr, "  -h, --help, --usage : print this usage message\n");
    exit(exitcode);
}

void resize(unsigned char **data, size_t *bufsize, size_t newsize) {
    if (newsize>*bufsize) {
        *bufsize = newsize;
        *data = (unsigned char *)rvalloc(*data, newsize, 0);
    }
}

const char *lookup_filetype(u_int32_t fourcc)
{
    const char *description;
    switch (fourcc) {
        /* file type descriptions taken from ftyps.com Rev 1.4 */
        case FOURCC('3','G','2','A'):   description = "3GPP2 Media (.3G2)"; break;
        case FOURCC('3','G','E','6'):   description = "3GPP (.3GP) Release 6 MBMS Extended Presentations"; break;
        case FOURCC('3','G','E','7'):   description = "3GPP (.3GP) Release 7 MBMS Extended Presentations"; break;
        case FOURCC('3','G','G','6'):   description = "3GPP Release 6 General Profile"; break;
        case FOURCC('3','G','P','1'):   description = "3GPP Media (.3GP) Release 1 ? (non-existent)"; break;
        case FOURCC('3','G','P','2'):   description = "3GPP Media (.3GP) Release 2 ? (non-existent)"; break;
        case FOURCC('3','G','P','3'):   description = "3GPP Media (.3GP) Release 3 ? (non-existent)"; break;
        case FOURCC('3','G','P','4'):   description = "3GPP Media (.3GP) Release 4"; break;
        case FOURCC('3','G','P','5'):   description = "3GPP Media (.3GP) Release 5"; break;
        case FOURCC('3','G','P','6'):   description = "3GPP Media (.3GP) Release 6 Basic Profile"; break;
        case FOURCC('3','G','R','6'):   description = "3GPP Media (.3GP) Release 6 Progressive Download"; break;
        case FOURCC('3','G','S','6'):   description = "3GPP Media (.3GP) Release 6 Streaming Servers"; break;
        case FOURCC('3','G','S','7'):   description = "3GPP Media (.3GP) Release 7 Streaming Servers"; break;
        case FOURCC('A','V','C','1'):   description = "MP4 Base w/ AVC ext [ISO 14496-12:2005]"; break;
        case FOURCC('C','A','Q','V'):   description = "Casio Digital Camera"; break;
        case FOURCC('I','S','O','2'):   description = "MP4 Base Media v2 [ISO 14496-12:2005]"; break;
        case FOURCC('I','S','O','M'):   description = "MP4  Base Media v1 [IS0 14496-12:2003]"; break;
        case FOURCC('J','P','2',' '):   description = "JPEG 2000 Image (.JP2) [ISO 15444-1 ?]"; break;
        case FOURCC('J','P','2','0'):   description = "Unknown, from GPAC samples (prob non-existent)"; break;
        case FOURCC('J','P','M',' '):   description = "JPEG 2000 Compound Image (.JPM) [ISO 15444-6]"; break;
        case FOURCC('J','P','X',' '):   description = "JPEG 2000 w/ extensions (.JPX) [ISO 15444-2]"; break;
        case FOURCC('K','D','D','I'):   description = "3GPP2 EZmovie for KDDI 3G Cellphones "; break;
        case FOURCC('M','4','A',' '):   description = "Apple iTunes AAC-LC (.M4A) Audio"; break;
        case FOURCC('M','4','B',' '):   description = "Apple iTunes AAC-LC (.M4B) Audio Book"; break;
        case FOURCC('M','4','P',' '):   description = "Apple iTunes AAC-LC (.M4P) AES Protected Audio"; break;
        case FOURCC('M','4','V',' '):   description = "Apple iTunes Video (.M4V) Video"; break;
        case FOURCC('M','J','2','S'):   description = "Motion JPEG 2000 [ISO 15444-3] Simple Profile"; break;
        case FOURCC('M','J','P','2'):   description = "Motion JPEG 2000 [ISO 15444-3] General Profile"; break;
        case FOURCC('M','M','P','4'):   description = "MPEG-4/3GPP Mobile Profile (.MP4 / .3GP) (for NTT)"; break;
        case FOURCC('M','P','2','1'):   description = "MPEG-21 [ISO/IEC 21000-9]"; break;
        case FOURCC('M','P','4','1'):   description = "MP4 v1 [ISO 14496-1:ch13]"; break;
        case FOURCC('M','P','4','2'):   description = "MP4 v2 [ISO 14496-14]"; break;
        case FOURCC('M','P','7','1'):   description = "MP4 w/ MPEG-7 Metadata [per ISO 14496-12]"; break;
        case FOURCC('M','P','P','I'):   description = "Photo Player, MAF [ISO/IEC 23000-3]"; break;
        case FOURCC('M','Q','T',' '):   description = "Sony / Mobile QuickTime (.MQV)"; break;
        case FOURCC('M','S','N','V'):   description = "MPEG-4 (.MP4) for SonyPSP"; break;
        case FOURCC('N','D','A','S'):   description = "MP4 v2 [ISO 14496-14] Nero Digital AAC Audio"; break;
        case FOURCC('N','D','S','C'):   description = "MPEG-4 (.MP4) Nero Cinema Profile"; break;
        case FOURCC('N','D','S','H'):   description = "MPEG-4 (.MP4) Nero HDTV Profile"; break;
        case FOURCC('N','D','S','M'):   description = "MPEG-4 (.MP4) Nero Mobile Profile"; break;
        case FOURCC('N','D','S','P'):   description = "MPEG-4 (.MP4) Nero Portable Profile"; break;
        case FOURCC('N','D','S','S'):   description = "MPEG-4 (.MP4) Nero Standard Profile"; break;
        case FOURCC('N','D','X','C'):   description = "H.264/MPEG-4 AVC (.MP4) Nero Cinema Profile"; break;
        case FOURCC('N','D','X','H'):   description = "H.264/MPEG-4 AVC (.MP4) Nero HDTV Profile"; break;
        case FOURCC('N','D','X','M'):   description = "H.264/MPEG-4 AVC (.MP4) Nero Mobile Profile"; break;
        case FOURCC('N','D','X','P'):   description = "H.264/MPEG-4 AVC (.MP4) Nero Portable Profile"; break;
        case FOURCC('N','D','X','S'):   description = "H.264/MPEG-4 AVC (.MP4) Nero Standard Profile"; break;
        case FOURCC('O','D','C','F'):   description = "OMA DCF DRM:OMA-TS-DRM-DCF-V2_0-20060303-A"; break;
        case FOURCC('Q','T',' ',' '):   description = "Apple QuickTime (.MOV/QT)"; break;
        case FOURCC('S','D','V',' '):   description = "SD Memory Card Video"; break;
        default                     :   description = "unknown file type"; break;
    }
    return description;
}

int demux_pes_packet(struct bitbuf *bb, int pes_packet_length, FILE *fileout)
{
    int i;

    unsigned char *data = (unsigned char *)rvalloc(NULL, pes_packet_length, 0);

    /* read in pes packet as byte stream */
    for (i=0; i<pes_packet_length; i++) {
        data[i] = getbits8(bb);                         /* PES_packet_data_byte */
        if (eofbits(bb))
            break;
    }

    /* demux video pes packets */
    fwrite(data, i, 1, fileout);

    /* tidy up */
    rvfree(data);

    return i;
}

int demux_unbounded_pes_packet(unsigned char *data, int length, FILE *fileout, int verbose)
{
    /* prepare to parse pes packet */
    struct bitbuf *eb = initbits_memory(data, length);

    /* parse pes header */
    int leading = 0;
    while (showbits24(eb)!=0x000001 && !eofbits(eb))
        leading += flushbits(eb, 8);
    if (leading>length*8) {
        if (verbose>=1)
            rvmessage("pes packet of %d bytes does not contain a start code", length);
        return 0;
    }
    if (leading && verbose>=1)
        rvmessage("%d bits of leading garbage before start code in pes packet", leading);

    flushbits(eb, 24);                                  /* packet_start_code_prefix */
    int stream_id = getbits8(eb);                       /* stream_id */
    flushbits(eb, 16);                                  /* PES_packet_length */
    int pes_packet_length = length - 6 - leading/8;
    if (verbose>=2)
        describe_pes_packet(stream_id, pes_packet_length);

    /* parse pes packet */
    int pes_packet_data_length = parse_pes_packet_header(eb, stream_id, pes_packet_length, NULL, NULL, verbose);

    if (verbose>=2)
        rvmessage("pes package: %d data bytes", pes_packet_data_length);

    /* demux pes packets */
    int demux_bytes = demux_pes_packet(eb, pes_packet_data_length, fileout);

    /* tidy up */
    freebits(eb);

    return demux_bytes;
}

int main(int argc, char *argv[])
{
    FILE *fileout = stdout;
    const char *filename[RV_MAXFILES] = {0};
    char *outfile = NULL;
    int fileindex = 0;
    int numfiles = 0;

    /* data buffer */
    size_t bufsize = 1024;
    unsigned char *data = NULL;

    /* command line defaults */
    char *forcetype = NULL;
    int stream = 0;
    int verbose = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"type",    1, NULL, 't'},
            {"pid",     1, NULL, 'p'},
            {"stream",  1, NULL, 's'},
            {"output",  1, NULL, 'o'},
            {"quiet",   0, NULL, 'q'},
            {"verbose", 0, NULL, 'v'},
            {"usage",   0, NULL, 'h'},
            {"help",    0, NULL, 'h'},
            {NULL,      0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "t:p:s:o:qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 't':
                forcetype = optarg;
                break;

            case 'p':
            case 's':
                stream = atoi(optarg);
                break;

            case 'o':
                outfile = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < sizeof(filename)/sizeof(filename[0]))
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    /* use filein if no input filenames */
    if (numfiles==0)
        filename[0] = "-";

    /* open output file */
    if (outfile)
        fileout = fopen(outfile, "wb");
    if (fileout==NULL)
        rverror("failed to open output file \"%s\"", outfile);

    /* ignore SIGPIPE on output file */
    if (signal(SIGPIPE, SIG_IGN)==SIG_ERR)
        rverror("failed to set ignore on SIGPIPE");

    /* allocate data buffer */
    data = (unsigned char *)rvalloc(NULL, bufsize*sizeof(unsigned char), 0);

    /* loop over input files */
    long long demux_bytes = 0;
    long long discard_bytes = 0;
    do {
        /* prepare to parse file */
        struct bitbuf *bb = initbits_filename(filename[fileindex],  B_GETBITS);
        if (bb==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex]);

        /* determine filetype */
        divine_t divine = divine_filetype(bb, filename[fileindex], forcetype, verbose);
        if (rewindbits(bb)<0)
            rverror("searched too far on non-seekable input file \"%s\"", filename[fileindex]);

        if (verbose>=2)
            rvmessage("file \"%s\" has type %s", filename[fileindex], filetypename[divine.filetype]);

        /* select what to do based on filetype */
        switch (divine.filetype) {
            case AVI:
            {

                /* skip past avi header */
                unsigned int fourcc = getdword(bb);
                unsigned int size   = getdword(bb);
                if (fourcc!=FOURCC('R','I','F','F'))
                    rvexit("input file is not a RIFF file: %s", fourccname(fourcc));
                fourcc = getdword(bb);
                if (fourcc!=FOURCC('A','V','I',' '))
                    rvexit("input file is not an AVI file: %s", fourccname(fourcc));

                /* initialise stream index */
                int index = -1;

                /* loop over lists and chunks */
                int type = FOURCC('n','u','l','l');
                while (!eofbits(bb) && !ferror(fileout)) {
                    int i;
                    int frames, width, height;

                    fourcc = getdword(bb);
                    size   = getdword(bb);

                    //rvmessage("%s with %d bytes", fourccname(fourcc), size);

                    switch (fourcc) {

                        case FOURCC('R','I','F','F'):
                            fourcc = getdword(bb);
                            if (fourcc!=FOURCC('A','V','I','X'))
                                rvexit("extended chunk is not an AVIX: %s", fourccname(fourcc));
                            if (verbose>=1)
                                rvmessage("parsing extended chunk");
                            break;

                        case FOURCC('L','I','S','T'):
                            parse_list(bb, verbose, (int *)&fourcc, size);
                            if (fourcc==FOURCC('s','t','r','l'))
                                index++;
                            break;

                        case FOURCC('a','v','i','h'):
                            parse_avih(bb, verbose, &frames, &width, &height);
                            break;

                        case FOURCC('s','t','r','h'):
                            parse_strh(bb, verbose, index, &type);
                            break;

                        case FOURCC('s','t','r','f'):
                            if (type==FOURCC('v','i','d','s'))
                                parse_strf_vids(bb, verbose);
                            else if (type==FOURCC('a','u','d','s'))
                                parse_strf_auds(bb, verbose);
                            else
                                rvmessage("unknown stream type: %s", describe_fourcc(type));
                            break;

                        case FOURCC('0','0','d','b'):
                        case FOURCC('0','1','d','b'):
                        case FOURCC('0','0','d','c'):
                        case FOURCC('0','1','d','c'):
                            /* calculate stream index from fourcc */
                            /* FIXME http://www.multimedia.cx/avistuff.txt tells me the stream id is in HEX */
                            fourcc = (u_int8_t)(fourcc>>8)-48 + ((u_int8_t)(fourcc)-48)*10;
                            resize(&data, &bufsize, size);
                            for (i=0; i<size; i++)
                                data[i] = getbyte(bb);
                            if (stream==fourcc)
                                demux_bytes += size * fwrite(data, size, 1, fileout);
                            /* align input to word boundary */
                            size = ((size + 1) & ~1) - size;
                            flushbits(bb, size*8);
                            break;

                        /* ascii string chunks */
                        case FOURCC('s','t','r','n'):
                        case FOURCC('I','S','F','T'):
                            parse_strn(bb, verbose, size);
                            break;

                        default:
                            if (verbose>=2)
                                rvmessage("skipping unknown chunk %s", fourccname(fourcc));
                            size = (size + 1) & ~1;
                            flushbits(bb, size*8);
                            break;
                    }

                }
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");

                break;
            }

            case QT:
            {
                u_int8_t byte;
                u_int16_t word;
                u_int32_t dword;
                u_int32_t timescale, duration;
                u_int32_t trackid;
                u_int32_t width, height;
                u_int32_t subtype;
                u_int32_t entries, fourcc;
                u_int32_t fixedsize = 0;

                /* tables */
                u_int32_t *stsc = NULL;
                u_int32_t *stsz = NULL;
                u_int32_t *stco = NULL;
                u_int64_t *co64 = NULL;

                /* track variables */
                int numsampchunk = 0;
                //int numsamps = 0;
                int numchunks = 0;

                /* prepare to parse quicktime file */
                FILE *file = fopen(filename[fileindex], "rb");
                if (file==NULL)
                    rverror("failed to open quicktime file \"%s\"", filename[fileindex]);

                /* loop over atoms */
                int i;
                while (!feof(file) && !ferror(file)) {
                    u_int32_t type, size;
                    if (fread(&size, 4, 1, file)!=1)
                        continue;
                    if (fread(&type, 4, 1, file)!=1)
                        continue;
                    size = BTOH32(size);

                    /* sanity check */
                    if (size==0)
                        /* could be null data at end of file */
                        continue;

                    /* look for extended size atoms */
                    u_int64_t esize = size;
                    if (size==1) {
                        if (fread(&esize, 8, 1, file)!=1)
                            continue;
                        esize = BTOH64(esize);
                    }

                    if (verbose>=2) {
                        if (size==1)
                            rvmessage("%s with extended size %lu bytes", describe_fourcc(type), esize);
                        else
                            rvmessage("%s with size %u bytes", describe_fourcc(type), size);
                    }

                    switch (type) {

                        /* file type atom */
                        case FOURCC('f','t','y','p'):
                            if (fread(&fourcc, 4, 1, file)!=1)     /* file type */
                                break;
                            if (fread(&dword, 4, 1, file)!=1)      /* version number */
                                break;
                            //dword = BTOH32(dword);
                            for (i=0; i<4; i++)
                                *((char *)&fourcc + i) = toupper(*((char *)&fourcc + i));
                            if (verbose>=1)
                                rvmessage("quicktime major file type is %s v%d: %s", fourccname(fourcc), dword, lookup_filetype(fourcc));
                            size -= 16;
                            while (size) {
                                if (fread(&fourcc, 4, 1, file)!=1)     /* file type */
                                    break;
                                for (i=0; i<4; i++)
                                    *((char *)&fourcc + i) = toupper(*((char *)&fourcc + i));
                                if (verbose>=1 && fourcc)
                                    rvmessage("quicktime compatible file type is %s: %s", fourccname(fourcc), lookup_filetype(fourcc));
                                size -= 4;
                            }
                            break;

                        /* movie atom */
                        case FOURCC('m','o','o','v'):
                            break;

                        /* movie header atom */
                        case FOURCC('m','v','h','d'):
                            if (fread(&byte, 1, 1, file)!=1)       /* version */
                                break;
                            if (fread(&dword, 3, 1, file)!=1)      /* flags */
                                break;
                            if (fread(&dword, 4, 1, file)!=1)      /* creation time */
                                break;
                            if (fread(&dword, 4, 1, file)!=1)      /* modification time */
                                break;
                            if (fread(&timescale, 4, 1, file)!=1)  /* time scale */
                                break;
                            if (fread(&duration, 4, 1, file)!=1)   /* duration */
                                break;
                            if (fread(&dword, 4, 1, file)!=1)      /* preferred rate */
                                break;
                            if (fread(&word, 2, 1, file)!=1)       /* preferred volume */
                                break;
                            fseeko(file, 10, SEEK_CUR);      /* reserved */
                            fseeko(file, 36, SEEK_CUR);      /* matrix structure */
                            if (fread(&dword, 4, 1, file)!=1)      /* preview time */
                                break;
                            if (fread(&dword, 4, 1, file)!=1)      /* preview duration */
                                break;
                            if (fread(&dword, 4, 1, file)!=1)      /* poster time */
                                break;
                            if (fread(&dword, 4, 1, file)!=1)      /* selection time */
                                break;
                            if (fread(&dword, 4, 1, file)!=1)      /* selection duration */
                                break;
                            if (fread(&dword, 4, 1, file)!=1)      /* current time */
                                break;
                            if (fread(&dword, 4, 1, file)!=1)      /* next track id */
                                break;
                            timescale = BTOH32(timescale);
                            duration = BTOH32(duration);
                            if (verbose>=1)
                                rvmessage("quicktime file has duration %.1f seconds", (double)duration/timescale);
                            break;

                        /* track atom */
                        case FOURCC('t','r','a','k'):
                            break;

                        /* track header atom */
                        case FOURCC('t','k','h','d'):
                            if (fread(&byte, 1, 1, file)!=1)       /* version */
                                break;
                            if (fread(&dword, 3, 1, file)!=1)      /* flags */
                                break;
                            if (fread(&dword, 4, 1, file)!=1)      /* creation time */
                                break;
                            if (fread(&dword, 4, 1, file)!=1)      /* modification time */
                                break;
                            if (fread(&trackid, 4, 1, file)!=1)    /* track id */
                                break;
                            if (fread(&dword, 4, 1, file)!=1)      /* reserved */
                                break;
                            if (fread(&duration, 4, 1, file)!=1)   /* duration */
                                break;
                            fseeko(file, 8, SEEK_CUR);       /* reserved */
                            if (fread(&word, 2, 1, file)!=1)       /* layer */
                                break;
                            if (fread(&word, 2, 1, file)!=1)       /* alternate group */
                                break;
                            if (fread(&word, 2, 1, file)!=1)       /* volume */
                                break;
                            if (fread(&word, 2, 1, file)!=1)       /* reserved */
                                break;
                            fseeko(file, 36, SEEK_CUR);      /* matrix structure */
                            if (fread(&width, 4, 1, file)!=1)      /* track width */
                                break;
                            if (fread(&height, 4, 1, file)!=1)     /* track height */
                                break;
                            duration = BTOH32(duration);
                            trackid = BTOH32(trackid);
                            width = BTOH32(width);
                            height = BTOH32(height);
                            if (verbose>=1)
                                rvmessage("track id %d is %.1fx%.1f with duration %.1f seconds", trackid, (double)width/(1<<16), (double)height/(1<<16), (double)duration/timescale);
                            break;

                        /* media atom */
                        case FOURCC('m','d','i','a'):
                            break;

                        /* media header atom */
                        case FOURCC('m','d','h','d'):
                            if (fread(&byte, 1, 1, file)!=1)       /* version */
                                break;
                            if (fread(&dword, 3, 1, file)!=1)      /* flags */
                                break;
                            if (fread(&dword, 4, 1, file)!=1)      /* creation time */
                                break;
                            if (fread(&dword, 4, 1, file)!=1)      /* modification time */
                                break;
                            if (fread(&timescale, 4, 1, file)!=1)  /* time scale */
                                break;
                            if (fread(&duration, 4, 1, file)!=1)   /* duration */
                                break;
                            if (fread(&word, 2, 1, file)!=1)       /* language */
                                break;
                            if (fread(&word, 2, 1, file)!=1)       /* quality */
                                break;
                            break;

                        /* media handler reference atom */
                        case FOURCC('h','d','l','r'):
                            if (fread(&byte, 1, 1, file)!=1)       /* version */
                                break;
                            if (fread(&dword, 3, 1, file)!=1)      /* flags */
                                break;
                            if (fread(&dword, 4, 1, file)!=1)      /* component type */
                                break;
                            if (fread(&subtype, 4, 1, file)!=1)    /* component subtype */
                                break;
                            if (fread(&dword, 4, 1, file)!=1)      /* component manufacturer */
                                break;
                            if (fread(&dword, 4, 1, file)!=1)      /* component flags */
                                break;
                            if (fread(&dword, 4, 1, file)!=1)      /* component flags mask */
                                break;
                            if (size-32) {
                                if (fread(data, size-32, 1, file)!=1)  /* component name */
                                    break;
                                data[size-32] = '\0';
                                if (verbose>=2)
                                    rvmessage("media handler with subtype %s and component name %s", describe_fourcc(subtype), data);
                            }
                            break;

                        /* video media information atom */
                        case FOURCC('m','i','n','f'):
                            break;

                        /* sample table atom */
                        case FOURCC('s','t','b','l'):
                            break;

                        /* sample description atom */
                        case FOURCC('s','t','s','d'):
                            if (fread(&byte, 1, 1, file)!=1)       /* version */
                                break;
                            if (fread(&dword, 3, 1, file)!=1)      /* flags */
                                break;
                            if (fread(&entries, 4, 1, file)!=1)    /* number of entries */
                                break;
                            entries = BTOH32(entries);
                            for (i=0; i<entries; i++) {
                                if (fread(&size, 4, 1, file)!=1)   /* sample description size */
                                    break;
                                if (fread(&fourcc, 4, 1, file)!=1) /* data format */
                                    break;
                                if (fread(data, 6, 1, file)!=1)    /* reserved */
                                    break;
                                if (fread(&word, 2, 1, file)!=1)   /* data reference index */
                                    break;
                                size = BTOH32(size);
                                word = BTOH16(word);
                                if (size-16) {
                                    resize(&data, &bufsize, size-16);
                                    if (fread(data, size-16, 1, file)!=1)
                                        break;
                                }
                                if (verbose>=1)
                                    rvmessage("sample description number %d with index %hd has fourcc %s", i, word, fourccname(fourcc));
                            }
                            break;

                        /* sample to chunk atom */
                        case FOURCC('s','t','s','c'):
                            if (fread(&byte, 1, 1, file)!=1)       /* version */
                                break;
                            if (fread(&dword, 3, 1, file)!=1)      /* flags */
                                break;
                            if (fread(&entries, 4, 1, file)!=1)    /* number of entries */
                                break;
                            entries = BTOH32(entries);
                            if (stream==trackid) {
                                stsc = (u_int32_t *)rvalloc(NULL, entries*3*sizeof(u_int32_t), 0);
                                if (fread(stsc, entries*3*sizeof(u_int32_t), 1, file)!=1)
                                    break;
                                for (i=0; i<entries*3; i++)
                                    stsc[i] = BTOH32(stsc[i]);
                                numsampchunk = entries;
                            } else {
                                resize(&data, &bufsize, entries*3*sizeof(u_int32_t));
                                if (fread(data, entries*3*sizeof(u_int32_t), 1, file)!=1)
                                    break;
                            }
                            break;

                        /* sample size atom */
                        case FOURCC('s','t','s','z'):
                            if (fread(&byte, 1, 1, file)!=1)       /* version */
                                break;
                            if (fread(&dword, 3, 1, file)!=1)      /* flags */
                                break;
                            if (fread(&dword, 4, 1, file)!=1)      /* sample size */
                                break;
                            if (fread(&entries, 4, 1, file)!=1)    /* number of entries */
                                break;
                            dword = BTOH32(dword);
                            entries = BTOH32(entries);
                            if (dword==0) {
                                /* read sample size table */
                                if (stream==trackid) {
                                    stsz = (u_int32_t *)rvalloc(NULL, entries*sizeof(u_int32_t), 0);
                                    if (fread(stsz, entries*sizeof(u_int32_t), 1, file)!=1)
                                        break;
                                    for (i=0; i<entries; i++)
                                        stsz[i] = BTOH32(stsz[i]);
                                } else {
                                    resize(&data, &bufsize, entries*sizeof(u_int32_t));
                                    if (fread(data, entries*sizeof(u_int32_t), 1, file)!=1)
                                        break;
                                }
                            }
                            if (stream==trackid) {
                                //numsamps = entries;
                                fixedsize = dword;
                            }
                            break;

                        /* chunk offset atom */
                        case FOURCC('s','t','c','o'):
                            if (fread(&byte, 1, 1, file)!=1)       /* version */
                                break;
                            if (fread(&dword, 3, 1, file)!=1)      /* flags */
                                break;
                            if (fread(&entries, 4, 1, file)!=1)    /* number of entries */
                                break;
                            entries = BTOH32(entries);
                            if (stream==trackid) {
                                stco = (u_int32_t *)rvalloc(NULL, entries*sizeof(u_int32_t), 0);
                                if (fread(stco, entries*sizeof(u_int32_t), 1, file)!=1)
                                    break;
                                for (i=0; i<entries; i++)
                                    stco[i] = BTOH32(stco[i]);
                                numchunks = entries;
                            } else {
                                resize(&data, &bufsize, entries*sizeof(u_int32_t));
                                if (fread(data, entries*sizeof(u_int32_t), 1, file)!=1)
                                    break;
                            }
                            break;

                        /* large chunk offset atom */
                        case FOURCC('c','o','6','4'):
                            if (fread(&byte, 1, 1, file)!=1)       /* version */
                                break;
                            if (fread(&dword, 3, 1, file)!=1)      /* flags */
                                break;
                            if (fread(&entries, 4, 1, file)!=1)    /* number of entries */
                                break;
                            entries = BTOH32(entries);
                            if (stream==trackid) {
                                co64 = (u_int64_t *)rvalloc(NULL, entries*sizeof(u_int64_t), 0);
                                if (fread(co64, entries*sizeof(u_int64_t), 1, file)!=1)
                                    break;
                                for (i=0; i<entries; i++)
                                    co64[i] = BTOH64(co64[i]);
                                numchunks = entries;
                            } else {
                                resize(&data, &bufsize, entries*sizeof(u_int64_t));
                                if (fread(data, entries*sizeof(u_int64_t), 1, file)!=1)
                                    break;
                            }
                            break;

                        /* data atom */
                        case FOURCC('m','d','a','t'):
                            /*
                             * skip atom now and parse later,
                             * this is to support files with
                             * data atom at the beginning
                             */
                            fseeko(file, esize-8, SEEK_CUR);
                            break;

                        case FOURCC('f','r','e','e'):
                        case FOURCC('s','k','i','p'):
                            size -= 8;
                            resize(&data, &bufsize, size);
                            if (fread(data, size, 1, file)!=1)
                                break;
                            break;

                        case FOURCC('w','i','d','e'):
                            if (size!=8)
                                rvmessage("warning: wide atom does not have a size of 8: %d", size);
                            break;

                        default:
                            if (verbose>=2)
                                rvmessage("skipping unknown atom %s", describe_fourcc(type));
                            size -= 8;
                            resize(&data, &bufsize, size);
                            if (fread(data, size, 1, file)!=1)
                                break;
                            break;
                    }

                }
                if (ferror(file))
                    rverror("error reading file \"%s\"", filename[fileindex]);

                /* extract track with requested id */
                int chunk, sample = 0;
                for (chunk=0; chunk<numchunks && !ferror(fileout); chunk++) {
                    /* position file at start of chunk */
                    if (co64) {
                        if (fseeko(file, co64[chunk], SEEK_SET)<0)
                            rvmessage("failed to seek to start of chunk at position %lu: %s", co64[chunk], strerror(errno));
                    } else {
                        if (fseek(file, stco[chunk], SEEK_SET)<0)
                            rvmessage("failed to seek to start of chunk at position %d: %s", stco[chunk], strerror(errno));
                    }

                    /* search stsc to find number of samples in chunk */
                    int numinchunk = 1;
                    for (i=0; i<numsampchunk; i++) {
                        if (stsc[i*3] > chunk+1)
                            break;
                        numinchunk = stsc[i*3+1];
                        /* TODO data reference index */
                    }
                    //fprintf(stderr, "number of samples in chunk %d: %d", chunk, numinchunk);

                    /* write out each sample */
                    for (i=0; i<numinchunk; i++) {
                        int samplesize = fixedsize? fixedsize : stsz[sample];
                        //fprintf(stderr, "size of sample %d: %d\n", sample, samplesize);
                        resize(&data, &bufsize, samplesize);
                        if (fread(data, samplesize, 1, file)!=1)
                            rverror("failed to read %d bytes from data atom", samplesize);
                        demux_bytes += samplesize * fwrite(data, samplesize, 1, fileout);
                        sample++;
                    }
                }
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");

                /* tidy up */
                rvfree(stsc);
                rvfree(stsz);
                rvfree(stco);
                rvfree(co64);
                fclose(file);

                break;
            }

            case TS:
            {
                int i;

                /* initialise program information tables */
                struct program_association_table *pat = (struct program_association_table *)rvalloc(NULL, sizeof(struct program_association_table), 1);
                struct program_map_table *pmt[MAX_PMTS] = {NULL};
                pat->version_displayed = -1;

                /* initialise section buffers */
                /* buffers include space for an extra transport packet of packet stuffing bytes */
                u_int8_t pat_section[SECTION_BUFFER_SIZE];
                u_int8_t *pmt_section[MAX_PMTS] = {NULL};
                int pat_pointer = 0, pmt_pointer[MAX_PMTS];

                /* loop over transport packets */
                int packnum = -1;
                u_int8_t packet[188];
                int found_pes_packet = 0;
                int pes_packet_length = -1;
                int data_pointer = 0;
                int pat_continuity = -1;
                int stream_continuity = -1;
                struct streamstat *stat = NULL;
                int numpids = 0;
                long long stc_start = -1, stc = -1, stc_last = 0;
                do {

                    /* read the next complete transport stream packet */
                    if (read_transport_packet(bb, verbose, packet)<0)
                        break;
                    packnum++;

                    /* check for synchronisation */
                    //if (showbits(bb, 8)!=0x47 && !eofbits(bb)) {
                    //    rvmessage("%s: packet %5d: packet does not have end sync, discarding and resyncing", filename[fileindex], packnum);
                    //    continue;
                    //}

                    /* prepare to parse transport stream packet */
                    struct bitbuf *pb = initbits_memory(packet, 188);

                    /* parse transport packet header */
                    int transport_error_indicator, payload_unit_start_indicator;
                    int pid, adaptation_field_control, continuity_counter, pcr_flag;
                    int discontinuity_indicator;
                    long long pcr;
                    int header_size = parse_transport_packet_header(pb, verbose, packnum, &transport_error_indicator,
                            &payload_unit_start_indicator, &pid, &adaptation_field_control, &continuity_counter,
                            &discontinuity_indicator, &pcr_flag, &pcr);
                    if (header_size<0) {
                        freebits(pb);
                        continue;
                    }
                    if (transport_error_indicator) {
                        if (verbose>=1)
                            rvmessage("transport packet %d: an uncorrectable error exists in the packet", packnum);
                        freebits(pb);
                        continue;
                    }

                    /* calculate payload size and check sanity */
                    int payload_size = 188-header_size;
                    if (payload_size==0) {
                        freebits(pb);
                        continue;
                    }
                    if (payload_size<0) {
                        if (verbose>=1)
                            rvmessage("error parsing transport packet header, resyncing");
                        freebits(pb);
                        continue;
                    }

                    /* update statistics */
                    for (i=0; i<numpids; i++) {
                        if (stat[i].pid==pid) {
                            stat[i].packets++;
                            stat[i].bytes += payload_size;
                            break;
                        }
                    }
                    if (i==numpids) {
                        numpids++;
                        stat = (struct streamstat *)rvalloc(stat, numpids*sizeof(struct streamstat), 0);
                        stat[i].pid = pid;
                        stat[i].packets = 1;
                        stat[i].bytes = payload_size;
                    }

                   /* parse transport packet payload */
                    if (adaptation_field_control==1 || adaptation_field_control==3) {
                        if (pid==0x0000) {
                            /* check continuity_counter */
                            if ((continuity_counter-pat_continuity+16)%16!=1 && pat_continuity!=-1)
                                if (verbose>=0)
                                    rvmessage("transport packet %d: discontinuous sequence for PAT, %d missing packets (%d-%d)", packnum, (continuity_counter-pat_continuity+16)%16-1,
                                             pat_continuity, continuity_counter);
                            pat_continuity = continuity_counter;

                            /* look for new pat section */
                            if (payload_unit_start_indicator) {
                                payload_size -= parse_pointer_field(pb);
                                pat_pointer = 0;
                            }

                            /* copy payload to pat section */
                            while (payload_size) {
                                if (pat_pointer==1024) {
                                    rvmessage("%s: packet %5d: pat section is too large", filename[fileindex], packnum);
                                    break;
                                }
                                pat_section[pat_pointer++] = getbits8(pb);
                                payload_size--;
                            }

                            /* try to parse pmt section */
                            struct bitbuf *sb = initbits_memory(pat_section, pat_pointer);
                            int ret = parse_program_association_section(sb, pat_pointer, verbose, pat);
                            freebits(sb);

                            if (ret>0 && pat->version_displayed!=pat->version_number) {
                                if (verbose>=0)
                                    describe_program_association(pat);

                                pat->version_displayed = pat->version_number;
                            }

                            /* allocate pmt table and pmt section for each program */
                            if (ret>0)
                                for (i=0; i<pat->number_programs; i++)
                                    if (pmt[i]==NULL) {
                                        pmt[i] = (struct program_map_table *)rvalloc(NULL, sizeof(struct program_map_table), 0);
                                        pmt_section[i] = (unsigned char *)rvalloc(NULL, SECTION_BUFFER_SIZE, 0);
                                        pmt[i]->version_displayed = -1;
                                        pmt_pointer[i] = 0;
                                    }

                        } else if (pid==8191 && stream==8191) {

                            /* just dump the null data bytes */
                            demux_bytes += payload_size;
                            while (payload_size--)
                                fputc(getbits8(pb), fileout);

                        } else if (pid==stream) {
                            static int start;
                            if ((continuity_counter-stream_continuity+16)%16!=1 && stream_continuity!=-1)
                                if (verbose>=0)
                                    rvmessage("transport packet %d: discontinuous sequence for PID %d, %d missing packets (%d-%d)", packnum, pid, (continuity_counter-stream_continuity+16)%16-1,
                                              stream_continuity, continuity_counter);
                            stream_continuity = continuity_counter;

                            /* demux unbounded pes packets */
                            if (found_pes_packet && pes_packet_length==0 && payload_unit_start_indicator) {
                                demux_bytes += demux_unbounded_pes_packet(data, data_pointer, fileout, verbose);
                                found_pes_packet = 0;
                            }

                            /* look for new pes packet */
                            if (payload_unit_start_indicator) {
                                found_pes_packet = 1;
                                data_pointer = 0;
                                /* read start of pes packet */
                                for (i=0; i<6; i++)
                                    data[data_pointer++] = getbits8(pb);
                                payload_size -= 6;
                                start = (int)data[3];                                   /* stream_id */
                                pes_packet_length = (int)data[4] << 8 | (int)data[5];   /* PES_packet_length */
                                //pes_packet_length = BTOH16(pes_packet_length);
                                /* describe pes packet */
                                if (verbose>=2 && pes_packet_length)
                                    describe_pes_packet(start, pes_packet_length);
                                if (pes_packet_length)
                                    resize(&data, &bufsize, pes_packet_length+6);
                            }

                            /* read pes packet data */
                            if (found_pes_packet) {

                                /* check for unbounded pes packets */
                                if (pes_packet_length==0) {
                                    /* read in pes packet data from transport packet */
                                    resize(&data, &bufsize, data_pointer+payload_size);
                                    while (payload_size) {
                                        data[data_pointer++] = getbits8(pb);                /* PES_packet_data_byte */
                                        payload_size--;
                                    }
                                } else {
                                    /* read in pes packet data from transport packet */
                                    while (payload_size && data_pointer<pes_packet_length+6) {
                                        data[data_pointer++] = getbits8(pb);                /* PES_packet_data_byte */
                                        payload_size--;
                                    }
                                }

                                /* demux normal pes packets */
                                if (pes_packet_length && data_pointer==pes_packet_length+6) {
                                    //demux_bytes += data_pointer * fwrite(data, data_pointer, 1, fileout);

                                    /* demux pes packets */
                                    demux_bytes += demux_unbounded_pes_packet(data, data_pointer, fileout, verbose);

                                    /* tidy up */
                                    found_pes_packet = 0;
                                }

                            } else {

                                /* discard junk data at start of transport stream */
                                flushbits(pb, payload_size*8);
                                discard_bytes += payload_size;
                                payload_size = 0;

                            }

                            if (verbose>=2)
                                if (payload_size)
                                    rvmessage("transport packet: unused payload after pes packet: %d bytes", payload_size);

                        } else {
                            for (i=0; i<pat->number_programs; i++)
                                if (pid==pat->program[i].program_map_pid) {
                                    /* TODO check continuity_counter */

                                    /* look for new pmt section */
                                    if (payload_unit_start_indicator) {
                                        payload_size -= parse_pointer_field(pb);
                                        pmt_pointer[i] = 0;
                                    }

                                    /* copy payload to pmt section */
                                    while (payload_size) {
                                        if (pmt_pointer[i]==1024) {
                                            rvmessage("%s: packet %5d: pmt section is too large", filename[fileindex], packnum);
                                            break;
                                        }
                                        pmt_section[i][pmt_pointer[i]++] = getbits8(pb);
                                        payload_size--;
                                    }

                                    /* try to parse pmt section */
                                    struct bitbuf *sb = initbits_memory(pmt_section[i], pmt_pointer[i]);
                                    int ret = parse_program_map_section(sb, pmt_pointer[i], verbose, pmt[i]);
                                    freebits(sb);

                                    if (ret>0 && pmt[i]->version_displayed!=pmt[i]->version_number) {
                                        /* display pmt */
                                        if (verbose>=0)
                                            describe_program_map(pmt[i]);
                                        pmt[i]->version_displayed = pmt[i]->version_number;
                                    }
                                }
                        }
                    }
                    assert(payload_size>=0);

                    /* removing packet stuffing bytes or unhandled packets */
                    //while (payload_size--)
                    //    flushbits(pb, 8);

                    /* decoder clock is just what is read from bitstream */
                    if (pcr_flag) {
                        stc = pcr;
                        if (stc_start<0)
                            stc_start = stc;
                    }

                    /* display statistics */
                    if (stc!=stc_start && stc-stc_last>=27000000ll && verbose>=1) {
                        rvmessage("+--------------------------------------------+");
                        for (i=0; i<numpids; i++) {
                            rvmessage("| %4d | %9d packets | %11lld kps |", stat[i].pid, stat[i].packets, (stat[i].bytes*8ll*27000ll)/(stc-stc_start));
                            stat[i].bytes = 0;
                        }
                        rvmessage("+--------------------------------------------+");
                        stc_last = stc;
                    }

                    /* tidy up */
                    freebits(pb);

                } while (!eofbits(bb) && !ferror(fileout));
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");

                /* demux last unbounded pes packet */
                if (found_pes_packet && pes_packet_length==0) {
                    demux_bytes += demux_unbounded_pes_packet(data, data_pointer, fileout, verbose);
                    found_pes_packet = 0;
                }

                /* tidy up */
                for (i=0; i<MAX_PMTS; i++) {
                    rvfree(pmt_section[i]);
                    rvfree(pmt[i]);
                }
                rvfree(pat);

                break;
            }

            case PS:
            {

                /* loop over start codes */
                do {
                    int i;
                    int pes_packet_length;

                    /* look for next start code */
                    int leading = 0;
                    while (showbits24(bb)!=0x000001)
                        leading += flushbits(bb, 8);
                    if (leading && verbose>=1)
                        rvmessage("%d bits of leading garbage before start code", leading);

                    flushbits(bb, 24);                                  /* packet_start_code_prefix */
                    int stream_id = getbits8(bb);                       /* stream_id */

                    /* lookup stream id */
                    switch (stream_id) {
                        case PACK_HEADER:
                            parse_pack_header(bb, verbose);
                            break;

                        case SYSTEM_HEADER:
                            parse_system_header(bb, verbose);
                            break;

                        case PROGRAM_STREAM_MAP:
                        case PRIVATE_STREAM_2:
                        case ECM_STREAM:
                        case EMM_STREAM:
                        case PROGRAM_STREAM_DIRECTORY:
                        case DSMCC_STREAM:
                        case TYPE_E_STREAM:
                            pes_packet_length = getbits(bb, 16);        /* PES_packet_length */
                            /* describe pes packet */
                            if (verbose>=2)
                                describe_pes_packet(stream_id, pes_packet_length);
                            for (i=0; i<pes_packet_length; i++)
                                flushbits(bb, 8);                       /* PES_packet_data_byte */
                            break;

                        case PADDING_STREAM:
                            pes_packet_length = getbits(bb, 16);        /* PES_packet_length */
                            /* describe pes packet */
                            if (verbose>=2)
                                describe_pes_packet(stream_id, pes_packet_length);
                            for (i=0; i<pes_packet_length; i++)
                                flushbits(bb, 8);                       /* padding_byte */
                            break;

                        default:
                            pes_packet_length = getbits(bb, 16);        /* PES_packet_length */
                            /* describe pes packet */
                            if (verbose>=2)
                                describe_pes_packet(stream_id, pes_packet_length);
                            pes_packet_length = parse_pes_packet_header(bb, stream_id, pes_packet_length, NULL, NULL, verbose);
                            /* demux video pes packets TODO support audio streams */
                            if (stream==stream_id-VIDEO_STREAM_X)
                                demux_pes_packet(bb, pes_packet_length, fileout);
                            break;

                    }
                } while (!eofbits(bb) && showbits32(bb)!=MPEG_PROGRAM_END_CODE && !ferror(fileout));
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");

                break;
            }

            case PES:
            {

                /* loop over pes packets */
                do {

                    /* look for next start code */
                    int leading = 0;
                    while (showbits24(bb)!=0x000001 && !eofbits(bb))
                        leading += flushbits(bb, 8);
                    if (eofbits(bb))
                        break;
                    if (leading && verbose>=1)
                        rvmessage("%d bits of leading garbage before start code in pes packet", leading);

                    flushbits(bb, 24);                                  /* packet_start_code_prefix */
                    int stream_id = getbits8(bb);                       /* stream_id */
                    int pes_packet_length = getbits(bb, 16);            /* PES_packet_length */
                    if (verbose>=2)
                        describe_pes_packet(stream_id, pes_packet_length);

                    /* parse pes packet */
                    int pes_packet_data_length = parse_pes_packet_header(bb, stream_id, pes_packet_length, NULL, NULL, verbose);

                    if (verbose>=2)
                        rvmessage("pes packet: %d data bytes", pes_packet_data_length);

                    /* demux video pes packets TODO support audio streams */
                    if (stream==stream_id-VIDEO_STREAM_X)
                        demux_bytes += demux_pes_packet(bb, pes_packet_data_length, fileout);

                } while (!eofbits(bb) && !ferror(fileout));
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");

                break;
            }

            case ASF:
            {

                int stream_properties_number = 0;

                /* loop over objects */
                do {
                    char string[37];
                    int data_packet_size;

                    /* read object id and size */
                    struct guid guid = getguid(bb);             /* object id */
                    unsigned long long size = getqword(bb);     /* object size */

                    //rvmessage("object with guid {%s} and size %lld", stringify_guid(guid, string), size);

                    if (strcmp(stringify_guid(guid, string), ASF_HEADER_OBJECT)==0) {
                        if (parse_header_object(bb, verbose)<0)
                            break;

                    } else if (strcmp(stringify_guid(guid, string), ASF_FILE_PROPERTIES_OBJECT)==0) {
                        int flags;
                        long long data_packets_count;
                        parse_file_properties_object(bb, verbose, &data_packets_count, &flags, &data_packet_size);

                    } else if (strcmp(stringify_guid(guid, string), ASF_STREAM_PROPERTIES_OBJECT)==0) {
                        int codec_specific_data_size;
                        unsigned char *codec_specific_data = NULL;

                        if (parse_stream_properties_object(bb, verbose, &codec_specific_data_size, &codec_specific_data)<0) {
                            rvmessage("failed to parse stream properties object");
                            break;
                        }

                        /* demux codec specific data */
                        if (++stream_properties_number==stream)
                            if (codec_specific_data_size) {
                                if (fwrite(codec_specific_data, codec_specific_data_size, 1, fileout)!=1)
                                    rverror("failed to write %d bytes to output", codec_specific_data_size);
                                else
                                    demux_bytes += codec_specific_data_size;
                                if (verbose>=1)
                                    rvmessage("writing %d bytes of codec specific data", codec_specific_data_size);
                            }

                        /* tidy up */
                        rvfree(codec_specific_data);

                    } else if (strcmp(stringify_guid(guid, string), ASF_HEADER_EXTENSION_OBJECT)==0) {
                        parse_header_extension_object(bb, verbose);

                    } else if (strcmp(stringify_guid(guid, string), ASF_CODEC_LIST_OBJECT)==0) {
                        parse_codec_list_object(bb, verbose);

                    } else if (strcmp(stringify_guid(guid, string), ASF_CONTENT_DESCRIPTION_OBJECT)==0) {
                        parse_content_description_object(bb, verbose);

                    } else if (strcmp(stringify_guid(guid, string), ASF_STREAM_BITRATE_PROPERTIES_OBJECT)==0) {
                        parse_stream_bitrate_properties_object(bb, verbose);

                    } else if (strcmp(stringify_guid(guid, string), ASF_DATA_OBJECT)==0) {
                        int i, j, k;
                        unsigned long long total_data_packets;
                        getguid(bb);                            /* File ID */
                        total_data_packets = getqword(bb);      /* Total Data Packets */
                        int reserved = getword(bb);             /* Reserved */

                        if (reserved!=0x0101)
                            rvmessage("asf data object: invalid value for reserved: 0x%04x", reserved);

                        /* loop over data packets */
                        for (i=0; i<total_data_packets; i++) {
                            if (eofbits(bb)) {
                                if (verbose>=0)
                                    rvmessage("out of data after %d/%lld packets", i, total_data_packets);
                                break;
                            }

                            /* look for error correction data */
                            int error_correction_present = getbit(bb);
                            int error_correction_data_length = 0;
                            if (error_correction_present) {
                                /* parse error correction data */
                                int error_correction_length_type = getbits(bb, 2);
                                int opaque_data_present = getbit(bb);
                                error_correction_data_length = getbits(bb, 4);
                                if (error_correction_length_type==0)
                                    flushbits(bb, error_correction_data_length*8);
                                else
                                    rvmessage("reserved value of error_correction_length_type: %d", error_correction_length_type);
                                flushbits(bb, 1);           /* Error Correction Present */
                                if (opaque_data_present) {
                                    rvmessage("asf: packet %d: opaque payload data", i);
                                    break;
                                }
                            }

                            /* parse payload parsing information */
                            int packet_length_type = getbits(bb, 2);
                            int padding_length_type = getbits(bb, 2);
                            int sequence_type = getbits(bb, 2);
                            int multiple_payloads_present = getbit(bb);
                            int stream_number_length_type = getbits(bb, 2);
                            int media_object_number_length_type = getbits(bb, 2);
                            int offset_into_media_object_number_length_type = getbits(bb, 2);
                            int replicated_data_length_type = getbits(bb, 2);
                            int packet_length = parse_length_with_type(bb, packet_length_type);
                            parse_length_with_type(bb, sequence_type);      /* Sequence */
                            int padding_length = parse_length_with_type(bb, padding_length_type);
                            getdword(bb);                                   /* Send Time */
                            getword(bb);                                    /* Duration */

                            /* use fixed data packet size */
                            if (packet_length==0)
                                packet_length = data_packet_size;

                            /* conformity check */
                            if (sequence_type!=0)
                                rvmessage("packet %d: sequence_type should be 0: %d", i, sequence_type);
                            if (replicated_data_length_type!=1)
                                rvmessage("packet %d: replicated_data_length_type should be 1: %d", i, replicated_data_length_type);
                            if (media_object_number_length_type!=1)
                                rvmessage("packet %d: media_object_number_length_type should be 1: %d", i, media_object_number_length_type);
                            if (stream_number_length_type!=1)
                                rvmessage("packet %d: stream_number_length_type should be 1: %d", i, stream_number_length_type);

                            if (verbose>=2)
                                rvmessage("packet %d: %s payload with length %d bytes and padding %d bytes", i, multiple_payloads_present? "multiple" : "single", packet_length, padding_length);

                            /* parse payload */
                            if (multiple_payloads_present) {

                                int payload_length_type = getbits(bb, 2);
                                int number_of_payloads = getbits(bb, 6);

                                /* loop over payloads */
                                for (j=0; j<number_of_payloads; j++) {

                                    flushbits(bb, 1);               /* Key Frame Bit */
                                    int stream_number = getbits(bb, 7);
                                    int media_object_number = parse_length_with_type(bb, media_object_number_length_type);
                                    int offset_into_media_object = parse_length_with_type(bb, offset_into_media_object_number_length_type);
                                    int replicated_data_length = parse_length_with_type(bb, replicated_data_length_type);
                                    flushbits(bb, replicated_data_length*8);
                                    int payload_length = parse_length_with_type(bb, payload_length_type);

                                    if (verbose>=2)
                                        rvmessage("payload %d: stream %d with replicated %d bytes and length %d bytes", j, stream_number, replicated_data_length, payload_length);
                                    if (verbose>=3)
                                        rvmessage("payload %d: offset %d into media object %d", j, offset_into_media_object, media_object_number);

                                    /* handle normal and compressed payloads */
                                    int length = 0;
                                    while (length<payload_length) {
                                        int sub_payload_data_length;
                                        if (replicated_data_length==1)
                                            sub_payload_data_length = getbyte(bb);
                                        else
                                            sub_payload_data_length = payload_length;
                                        //rvmessage("sub_payload_data_length=%d", sub_payload_data_length);

                                        /* demux sub-payload */
                                        if (stream_number==stream) {
                                            resize(&data, &bufsize, sub_payload_data_length);
                                            for (k=0; k<sub_payload_data_length; k++)
                                                data[k] = getbyte(bb);
                                            if (fwrite(data, sub_payload_data_length, 1, fileout)!=1)
                                                rverror("failed to write %d bytes to output", sub_payload_data_length);
                                            else
                                                demux_bytes += sub_payload_data_length;
                                        } else
                                            flushbits(bb, sub_payload_data_length*8);

                                        length += length_of_type(payload_length_type) + sub_payload_data_length;
                                    }

                                }

                            } else {

                                flushbits(bb, 1);                   /* Key Frame Bit */
                                int stream_number = getbits(bb, 7); /* Stream Number */
                                int media_object_number = parse_length_with_type(bb, media_object_number_length_type);
                                int offset_into_media_object = parse_length_with_type(bb, offset_into_media_object_number_length_type);
                                int replicated_data_length = parse_length_with_type(bb, replicated_data_length_type);
                                if (replicated_data_length==1)
                                    rvexit("time to implement multiple compressed payloads");
                                flushbits(bb, replicated_data_length*8);
                                int payload_length = packet_length
                                        - error_correction_present*(error_correction_data_length+1)
                                        - 8
                                        - length_of_type(packet_length_type)
                                        - length_of_type(sequence_type)
                                        - length_of_type(padding_length_type)
                                        - 1
                                        - length_of_type(media_object_number_length_type)
                                        - length_of_type(offset_into_media_object_number_length_type)
                                        - length_of_type(replicated_data_length_type)
                                        - replicated_data_length
                                        - padding_length;

                                if (verbose>=2)
                                    rvmessage("payload: stream %d with replicated %d bytes and length %d bytes", stream_number, replicated_data_length, payload_length);
                                if (verbose>=3)
                                    rvmessage("payload: offset %d into media object %d", offset_into_media_object, media_object_number);

                                /* demux payload */
                                if (stream_number==stream) {
                                    resize(&data, &bufsize, payload_length);
                                    for (k=0; k<payload_length; k++)
                                        data[k] = getbyte(bb);
                                    if (fwrite(data, payload_length, 1, fileout)!=1)
                                        rverror("failed to write %d bytes to output", payload_length);
                                    else
                                        demux_bytes += payload_length;
                                } else
                                    flushbits(bb, payload_length*8);

                            }

                            /* skip padding */
                            flushbits(bb, padding_length*8);

                        }

                    } else {
                        /* skip unknown object */
                        size -= 24;
                        flushbits(bb, size*8);
                        if (verbose>=1)
                            rvmessage("skipping unknown object: %s", stringify_guid(guid, string));
                    }

                } while (!eofbits(bb) && !ferror(fileout));
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");

                break;

            }

            case EMPTY:
                rvexit("empty file: \"%s\"", filename[fileindex]);
                break;

            default:
                rvmessage("filetype of file \"%s\" is not supported: %s", filename[fileindex], filetypename[divine.filetype]);
                break;
        }

        /* tidy up */
        freebits(bb);

    } while (++fileindex<numfiles);

    if (verbose>=1)
        rvmessage("discarded %lld bytes", discard_bytes);
    if (verbose>=0)
        rvmessage("demuxed %lld bytes", demux_bytes);

    /* tidy up */
    rvfree(data);

    return 0;
}
