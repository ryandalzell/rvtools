
#ifndef _RVH261_H_
#define _RVH261_H_

#include "rvbits.h"
#include "rvcodec.h"

/* macroblock type */
typedef enum {
    MTYPE_ERROR = -1,
    SKIPPED = 0,
    INTRA,
    INTRA_Q,
    INTER,
    INTER_Q,
    INTER_MC_SKIP,
    INTER_MC,
    INTER_MC_Q,
    INTER_FIL_SKIP,
    INTER_FIL,
    INTER_FIL_Q
} mtype_t;

/* h.261 picture */
struct rv261picture {
    int tempref;
    int ptype;
    int width;
    int height;
    struct rv261macro *macro;
};

/* h.261 macroblock */
struct rv261macro {
    char mba;
    mtype_t mtype;
    char mquant;
    struct mv_t mvd;
    struct mv_t mv;
    struct mv_t mvc;
    char cbp;
    tcoeff_t tcoeff[6][64];
    coeff_t coeff[6][64];
    diff_t error[6][64];
    sample_t sample[6][64];
};

/* gob number lookup tables for cif */
extern const int mbno_from_gobno[12];
extern const int mbno_from_ingob[33];

/* utility functions */
int mtype_is_intra(mtype_t m);
int mtype_is_quant(mtype_t m);
int mtype_is_mvd(mtype_t m);
int mtype_is_cbp(mtype_t m);
int mtype_is_fil(mtype_t m);

int parse_261_picture(struct bitbuf *bb, int verbose, int *width, int *height, int *hires);
int parse_261_gob(struct bitbuf *bb, int verbose, int *gquant);
int parse_261_cbp(struct bitbuf *bb);
struct rv261picture *parse_h261(struct bitbuf *bb, const char *filename, int verbose);
sample_t *decode_261(struct rv261picture *picture, sample_t *reference);

#endif
