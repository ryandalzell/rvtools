/*
 * Description: Skeleton of an rvtool application, read frames in and write frames out.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-06-23 13:26:19 $
 * Revision   : $Revision: 1.25 $
 * Copyright  : (c) 2005,2011 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include "getopt.h"

#include "rvutil.h"

const char *appname;

void usage(int exitcode)
{
    fprintf(stderr, "%s: another great rvtools program\n", appname);
    fprintf(stderr, "usage: %s [options] [<file>] [<file>...]\n", appname);
    fprintf(stderr, "  -s, --size          : image size format\n");
    fprintf(stderr, "     (formats : %s)\n", rvformats);
    fprintf(stderr, "  -p, --fourcc        : image pixel format fourcc (default: I420)\n");
    fprintf(stderr, "     (formats : %s)\n", rvfourccs);
    fprintf(stderr, "  -n, --numframes     : number of frames (default: all)\n");
    fprintf(stderr, "  -o, --output        : write output to file\n");
    fprintf(stderr, "  -q, --quiet         : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "  -v, --verbose       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "  --                  : disable argument processing\n");
    fprintf(stderr, "  -h, --help, --usage : print this usage message\n");
    exit(exitcode);
}

int main(int argc, char *argv[])
{
    FILE *filein = stdin;
    FILE *fileout = stdout;
    char *filename[RV_MAXFILES] = {0};
    char *outfile = NULL;
    int fileindex = 0;
    int numfiles = 0;
    int totframes = 0;

    /* data buffer */
    int size = 0;
    unsigned char *data = NULL;

    /* command line defaults */
    const char *format = NULL;
    const char *fccode = NULL;
    int numframes = -1;
    int verbose = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"size",      1, NULL, 's'},
            {"fourcc",    1, NULL, 'p'},
            {"numframes", 1, NULL, 'n'},
            {"output",    1, NULL, 'o'},
            {"quiet",     0, NULL, 'q'},
            {"verbose",   0, NULL, 'v'},
            {"usage",     0, NULL, 'h'},
            {"help",      0, NULL, 'h'},
            {NULL,        0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "s:p:n:o:qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 's':
                format = optarg;
                break;

            case 'p':
                fccode = optarg;
                break;

            case 'n':
                numframes = atoi(optarg);
                if (numframes<=0)
                    rvexit("invalid value for numframes: %d", numframes);
                break;

            case 'o':
                outfile = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < sizeof(filename)/sizeof(filename[0]))
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    /* open output file */
    if (outfile)
        fileout = fopen(outfile, "wb");
    if (fileout==NULL)
        rverror("failed to open output file \"%s\"", outfile);

    /* ignore SIGPIPE on output file */
    if (signal(SIGPIPE, SIG_IGN)==SIG_ERR)
        rverror("failed to set ignore on SIGPIPE");

    /* loop over input files */
    do {
        int width;
        int height;
        framerate_t framerate;
        fourcc_t fourcc;

        /* determine input file dimensions */
        divine_image_dims(&width, &height, NULL, &framerate, &fourcc, format, filename[fileindex]);

        /* override fourcc */
        if (fccode)
            fourcc = get_fourcc(fccode);

        /* get chroma subsampling from fourcc */
        int hsub, vsub;
        get_chromasub(fourcc, &hsub, &vsub);
        if (hsub==-1 || vsub==-1)
            rvexit("unknown pixel format: %s", fourccname(fourcc));

        /* (re-)allocate data buffer */
        size = get_framesize(width, height, hsub, vsub, 8);
        data = (unsigned char *)rvalloc(data, size*sizeof(unsigned char), 0);

        /* open next input file, or use stdin */
        if (numfiles)
            filein = fopen(filename[fileindex], "rb");
        if (filein==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex]);

        /* loop over frames in input file */
        int frameno;
        for (frameno=0; (totframes<numframes || numframes<0) && !feof(filein) && !ferror(filein) && !ferror(fileout); frameno++)
        {
            int read = fread(data, 1, size, filein);
            if (read)
            {
                if (fwrite(data, read, 1, fileout) == 1)
                    totframes++;
                else
                    break;

                if (verbose>=1)
                    rvmessage("frame %d", frameno);
            }
        }
        if (ferror(filein))
            rverror("failed to read from input");
        if (ferror(fileout) && errno!=EPIPE)
            rverror("failed to write to output");

        if (numfiles)
            fclose(filein);

    } while (++fileindex<numfiles && !ferror(fileout));

    if (verbose>=0)
        rvmessage("processed %d frames", totframes);

    /* tidy up */
    rvfree(data);

    return 0;
}
