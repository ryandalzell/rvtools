/*
 * Description: Common functions for rvtools codec applications.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-06-23 13:26:19 $
 * Revision   : $Revision: 1.6 $
 * Copyright  : (c) 2007 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdlib.h>
#include <string.h>

#include "rvutil.h"
#include "rvcodec.h"

/*
 * matricies
 */

const int zigzag[64] = {
    0 , 1 , 8 , 16, 9 , 2 , 3 , 10,
    17, 24, 32, 25, 18, 11, 4 , 5 ,
    12, 19, 26, 33, 40, 48, 41, 34,
    27, 20, 13, 6 , 7 , 14, 21, 28,
    35, 42, 49, 56, 57, 50, 43, 36,
    29, 22, 15, 23, 30, 37, 44, 51,
    58, 59, 52, 45, 38, 31, 39, 46,
    53, 60, 61, 54, 47, 55, 62, 63
};

const int zigzag_inverse[64] = {
    0 , 1 , 5 , 6 , 14, 15, 27, 28,
    2 , 4 , 7 , 13, 16, 26, 29, 42,
    3 , 8 , 12, 17, 25, 30, 41, 43,
    9 , 11, 18, 24, 31, 40, 44, 53,
    10, 19, 23, 32, 39, 45, 52, 54,
    20, 22, 33, 38, 46, 51, 55, 60,
    21, 34, 37, 47, 50, 56, 59, 61,
    35, 36, 48, 49, 57, 58, 62, 63
};

const int zigzag_alternate[64] = {
    0 , 8 , 16, 24, 1 , 9 , 2 , 10,
    17, 25, 32, 40, 48, 56, 57, 49,
    41, 33, 26, 18, 3 , 11, 4 , 12,
    19, 27, 34, 42, 50, 58, 35, 43,
    51, 59, 20, 28, 5 , 13, 6 , 14,
    21, 29, 36, 44, 52, 60, 37, 45,
    53, 61, 22, 30, 7 , 15, 23, 31,
    38, 46, 54, 62, 39, 47, 55, 63
};

const int zigzag_alternate_inverse[64] = {
    0 , 4 , 6 , 20, 22, 36, 38, 52,
    1 , 5 , 7 , 21, 23, 37, 39, 53,
    2 , 8 , 19, 24, 34, 40, 50, 54,
    3 , 9 , 18, 25, 35, 41, 51, 55,
    10, 17, 26, 30, 42, 46, 56, 60,
    11, 16, 27, 31, 43, 47, 57, 61,
    12, 15, 28, 32, 44, 48, 58, 62,
    13, 14, 29, 33, 45, 49, 59, 63
};

/*
 * common codec functions
 */

void copy_macroblock_into_picture(sample_t *frame, int width, int height, int chroma_format, int mbx, int mby, sample_t sample[][64])
{
    int block, i;
    unsigned char *p;

    /* copy luma sample data into frame */
    for (block=0; block<4; block++) {
        p = frame + 16*mby*width + 8*(block/2)*width + 16*mbx + 8*(block%2);
        for (i=0; i<8; i++)
            memcpy(p + i*width, sample[block] + 8*i, 8);
    }

    if (chroma_format==3) {
        /* 4:4:4 */

        /* copy chroma blue sample data into frame */
        p = frame + width*height + 16*mby*width + 8*mbx;
        for (i=0; i<8; i++)
            memcpy(p + i*width, sample[4] + 8*i, 8);
        p = frame + width*height + 16*mby*width + 8*width + 16*mbx;
        for (i=0; i<8; i++)
            memcpy(p + i*width, sample[6] + 8*i, 8);
        p = frame + width*height + 16*mby*width + 8*mbx + 8;
        for (i=0; i<8; i++)
            memcpy(p + i*width, sample[8] + 8*i, 8);
        p = frame + width*height + 16*mby*width + 8*width + 16*mbx + 8;
        for (i=0; i<8; i++)
            memcpy(p + i*width, sample[10] + 8*i, 8);

        /* copy chroma red sample data into frame */
        p = frame + width*height*8/4 + 16*mby*width + 16*mbx;
        for (i=0; i<8; i++)
            memcpy(p + i*width, sample[5] + 8*i, 8);
        p = frame + width*height*8/4 + 16*mby*width + 8*width + 16*mbx;
        for (i=0; i<8; i++)
            memcpy(p + i*width, sample[7] + 8*i, 8);
        p = frame + width*height*8/4 + 16*mby*width + 16*mbx + 8;
        for (i=0; i<8; i++)
            memcpy(p + i*width, sample[9] + 8*i, 8);
        p = frame + width*height*8/4 + 16*mby*width + 8*width + 16*mbx + 8;
        for (i=0; i<8; i++)
            memcpy(p + i*width, sample[11] + 8*i, 8);

    } else if (chroma_format==2) {
        /* 4:2:2 */

        /* copy chroma blue sample data into frame */
        p = frame + width*height + 16*mby*width/2 + 8*mbx;
        for (i=0; i<8; i++)
            memcpy(p + i*width/2, sample[4] + 8*i, 8);
        p = frame + width*height + 16*mby*width/2 + 8*width/2 + 8*mbx;
        for (i=0; i<8; i++)
            memcpy(p + i*width/2, sample[6] + 8*i, 8);

        /* copy chroma red sample data into frame */
        p = frame + width*height*6/4 + 16*mby*width/2 + 8*mbx;
        for (i=0; i<8; i++)
            memcpy(p + i*width/2, sample[5] + 8*i, 8);
        p = frame + width*height*6/4 + 16*mby*width/2 + 8*width/2 + 8*mbx;
        for (i=0; i<8; i++)
            memcpy(p + i*width/2, sample[7] + 8*i, 8);

    } else if (chroma_format==1) {
        /* 4:2:0 */

        /* copy chroma blue sample data into frame */
        p = frame + width*height + 8*mby*width/2 + 8*mbx;
        for (i=0; i<8; i++)
            memcpy(p + i*width/2, sample[4] + 8*i, 8);

        /* copy chroma red sample data into frame */
        p = frame + width*height*5/4 + 8*mby*width/2 + 8*mbx;
        for (i=0; i<8; i++)
            memcpy(p + i*width/2, sample[5] + 8*i, 8);

    }

}

sample_t *copy_fields_into_frame(sample_t *topfield, sample_t *botfield, int width, int height, int chroma_format)
{
    int j;

    /* allocate new frame picture */
    size_t size;
    switch (chroma_format) {
        case 3 : size = width*height*6/2; break;
        case 2 : size = width*height*4/2; break;
        default: size = width*height*3/2; break;
    }
    sample_t *frame = (sample_t *)rvalloc(NULL, size, 0);

    /* loop over luma lines in frame */
    sample_t *p = frame;
    for (j=0; j<height/2; j++) {
        memcpy(p, topfield, width);
        p += width;
        topfield += width;
        memcpy(p, botfield, width);
        p += width;
        botfield += width;
    }

    /* chroma subsampling */
    if (chroma_format==1 || chroma_format==2)
        width >>= 1;
    if (chroma_format==1)
        height >>= 1;

    /* loop over chroma lines in frame */
    for (j=0; j<height/2; j++) {
        memcpy(p, topfield, width);
        p += width;
        topfield += width;
        memcpy(p, botfield, width);
        p += width;
        botfield += width;
    }
    for (j=0; j<height/2; j++) {
        memcpy(p, topfield, width);
        p += width;
        topfield += width;
        memcpy(p, botfield, width);
        p += width;
        botfield += width;
    }

    return frame;
}

sample_t *copy_field_from_frame(sample_t *frame, int topfield, int width, int height, int chroma_format)
{
    int j;

    /* allocate new frame picture */
    size_t size;
    switch (chroma_format) {
        case 3 : size = width*height/2*6/2; break;
        case 2 : size = width*height/2*4/2; break;
        default: size = width*height/2*3/2; break;
    }
    sample_t *field= (sample_t *)rvalloc(NULL, size, 0);

    /* loop over luma lines in frame */
    sample_t *p = field;
    for (j=0; j<height/2; j++) {
        if (!topfield)
            frame += width;
        memcpy(p, frame, width);
        p += width;
        if (topfield)
            frame += width;
        frame += width;
    }

    /* chroma subsampling */
    if (chroma_format==1 || chroma_format==2)
        width >>= 1;
    if (chroma_format==1)
        height >>= 1;

    /* loop over chroma lines in frame */
    for (j=0; j<height/2; j++) {
        if (!topfield)
            frame += width;
        memcpy(p, frame, width);
        p += width;
        if (topfield)
            frame += width;
        frame += width;
    }
    for (j=0; j<height/2; j++) {
        if (!topfield)
            frame += width;
        memcpy(p, frame, width);
        p += width;
        if (topfield)
            frame += width;
        frame += width;
    }

    return field;
}
