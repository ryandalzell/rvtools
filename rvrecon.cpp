/*
 * Description: Functions for motion reconstruction.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-06-23 13:26:19 $
 * Revision   : $Revision: 1.7 $
 * Copyright  : (c) 2005 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/*
 * based on tmndecode (H.263 decoder) (C) 1996  Telenor R&D, Norway
 *        Karl Olav Lillevold <Karl.Lillevold@nta.no>
 */

/*
 * based on mpeg2decode, (C) 1994, MPEG Software Simulation Group
 * and mpeg2play, (C) 1994 Stefan Eckart
 *                         <stefan@lis.e-technik.tu-muenchen.de>
 */

#include "rvutil.h"
#include "rvcodec.h"

void make_edge_image(unsigned char *src, unsigned char *dst, int width, int height, int edge)
{
  int i,j;
  unsigned char *p1,*p2,*p3,*p4;
  unsigned char *o1,*o2,*o3,*o4;

  /* center image */
  p1 = dst;
  o1 = src;
  for (j = 0; j < height;j++) {
    for (i = 0; i < width; i++) {
      *(p1 + i) = *(o1 + i);
    }
    p1 += width + (edge<<1);
    o1 += width;
  }

  /* left and right edges */
  p1 = dst-1;
  o1 = src;
  for (j = 0; j < height;j++) {
    for (i = 0; i < edge; i++) {
      *(p1 - i) = *o1;
      *(p1 + width + i + 1) = *(o1 + width - 1);
    }
    p1 += width + (edge<<1);
    o1 += width;
  }

  /* top and bottom edges */
  p1 = dst;
  p2 = dst + (width + (edge<<1))*(height-1);
  o1 = src;
  o2 = src + width*(height-1);
  for (j = 0; j < edge;j++) {
    p1 = p1 - (width + (edge<<1));
    p2 = p2 + (width + (edge<<1));
    for (i = 0; i < width; i++) {
      *(p1 + i) = *(o1 + i);
      *(p2 + i) = *(o2 + i);
    }
  }

  /* corners */
  p1 = dst - (width+(edge<<1)) - 1;
  p2 = p1 + width + 1;
  p3 = dst + (width+(edge<<1))*(height)-1;
  p4 = p3 + width + 1;

  o1 = src;
  o2 = o1 + width - 1;
  o3 = src + width*(height-1);
  o4 = o3 + width - 1;
  for (j = 0; j < edge; j++) {
    for (i = 0; i < edge; i++) {
      *(p1 - i) = *o1;
      *(p2 + i) = *o2;
      *(p3 - i) = *o3;
      *(p4 + i) = *o4; 
    }
    p1 = p1 - (width + (edge<<1));
    p2 = p2 - (width + (edge<<1));
    p3 = p3 + width + (edge<<1);
    p4 = p4 + width + (edge<<1);
  }

}

static void rec(unsigned char *s, unsigned char *d, int dstride, int sstride, int h, int w)
{
    int i, j;

    for (j=0; j<h; j++) {
        for (i=0; i<w; i++)
            d[i] = s[i];
        s += sstride;
        d += dstride;
    }
}

static void rech(unsigned char *s, unsigned char *d, int dstride, int sstride, int h, int w)
{
    int i, j;

    for (j=0; j<h; j++) {
        for (i=0; i<w; i++)
            d[i] = (unsigned int)(s[i]+s[i+1]+1)>>1;
        s += sstride;
        d += dstride;
    }
}

static void recv(unsigned char *s, unsigned char *d, int dstride, int sstride, int h, int w)
{
  unsigned char *s1,*s2;
  int i, j;

  s1 = s;
  s2 = s+sstride;
    for (j=0; j<h; j++) {
        for (i=0; i<w; i++)
            d[i] = (unsigned int)(s1[i]+s2[i]+1)>>1;
        s1 += sstride;
        s2 += sstride;
        d += dstride;
    }
}

static void rec4(unsigned char *s, unsigned char *d, int dstride, int sstride, int h, int w)
{
    unsigned char *s1,*s2;
    int i, j;

    s1 = s;
    s2 = s+sstride;
    for (j=0; j<h; j++) {
        for (i=0; i<w; i++)
            d[i] = (unsigned int)(s1[i]+s1[i+1]+s2[i]+s2[i+1]+2)>>2;
        s1 += sstride;
        s2 += sstride;
        d += dstride;
    }
}

void recon_frame(unsigned char *src, int sstride, unsigned char *dst, int dstride, int w, int h, int x, int y, struct mv_t mv)
{
    int xint = mv.x>>1;     /* integer motion vector */
    int xh = mv.x & 1;      /* half-pel motion vector */
    int yint = mv.y>>1;     /* integer motion vector */
    int yh = mv.y & 1;      /* half-pel motion vector */

    /* origins */
    unsigned char *s = src + sstride*(y+yint) + x + xint;
    unsigned char *d = dst /*+ dstride*y + x*/;

    if (!xh && !yh)
        rec(s,d,dstride,sstride,h,w);
    else if (!xh && yh)
        recv(s,d,dstride,sstride,h,w);
    else if (xh && !yh)
        rech(s,d,dstride,sstride,h,w);
    else /* if (xh && yh) */
        rec4(s,d,dstride,sstride,h,w);
}

void recon_field(unsigned char *src, int sstride, unsigned char *dst, int dstride, int w, int h, int x, int y, struct mv_t mv)
{
    int xint = mv.x>>1;     /* integer motion vector */
    int xh = mv.x & 1;      /* half-pel motion vector */
    int yint = mv.y>>1;     /* integer motion vector */
    int yh = mv.y & 1;      /* half-pel motion vector */

    /* field adjustments */
    yint <<= 1;

    /* origins */
    unsigned char *s = src + sstride*(y+yint) + x + xint;
    unsigned char *d = dst /*+ dstride*y + x*/;

    /* field adjustments */
    sstride <<= 1;

    if (!xh && !yh)
        rec(s,d,dstride,sstride,h,w);
    else if (!xh && yh)
        recv(s,d,dstride,sstride,h,w);
    else if (xh && !yh)
        rech(s,d,dstride,sstride,h,w);
    else /* if (xh && yh) */
        rec4(s,d,dstride,sstride,h,w);
}

int recon_check(const char *filename, int picno, int macro, int width, int height, int w, int h, struct mv_t mv)
{
    int xint = mv.x>>1;     /* integer motion vector */
  //int xh = mv.x & 1;      /* half-pel motion vector */
    int yint = mv.y>>1;     /* integer motion vector */
  //int yh = mv.y & 1;      /* half-pel motion vector */

    int mbx = macro % (width/16);
    int mby = macro / (width/16);
    int x = mbx * 16;
    int y = mby * 16;

    /* sanity check integer motion vector */
    int pass = 1;
    if (x+xint<0 || x+xint>width-w) {
        rvmessage("%s: picture %d, macroblock %d: motion vector x points out of picture: %d", filename, picno, macro, mv.x);
        pass = 0;
    }
    if (y+yint<0 || y+yint>height-h) {
        rvmessage("%s: picture %d, macroblock %d: motion vector y points out of picture: %d", filename, picno, macro, mv.y);
        pass = 0;
    }

    return pass;
}
