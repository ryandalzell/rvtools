
#ifndef _RVHEVC_H_
#define _RVHEVC_H_

#include "rvbits.h"
#include "rvcodec.h"

/* hevc nal unit type */
/* *_N non-reference, *_R reference */
typedef enum {
    /* trailing non-IRAP pictures */
    TRAIL_N = 0,
    TRAIL_R,
    TSA_N,          /* temporal sub-layer access */
    TSA_R,
    STSA_N,         /* step-wise temporal sub-layer */
    STSA_R,
    /* leading pictures */
    RADL_N,         /* random-access decodable */
    RADL_R,
    RASL_N,         /* random-access skipped leading */
    RASL_R,
    RSV_VCL_N10,
    RSV_VCL_R11,
    RSV_VCL_N12,
    RSV_VCL_R13,
    RSV_VCL_N14,
    RSV_VCL_R15,
    /* intra random access point (IRAP) pictures */
    BLA_W_LP,       /* broken link access */
    BLA_W_RADL,
    BLA_N_LP,
    IDR_W_RADL,     /* instantaneous decoding refresh */
    IDR_N_LP,
    CRA_NUT,        /* clean random access */
    RSV_IRAP_VCL22,
    RSV_IRAP_VCL23,
    RSV_VCL24,
    RSV_VCL25,
    RSV_VCL26,
    RSV_VCL27,
    RSV_VCL28,
    RSV_VCL29,
    RSV_VCL30,
    RSV_VCL31,
    VPS_NUT,
    SPS_NUT,
    PPS_NUT,
    AUD_NUT,
    EOS_NUT,
    EOB_NUT,
    FD_NUT,
    PREFIX_SEI_NUT,
    SUFFIX_SEI_NUT,
    RSV_NVCL41,
    RSV_NVCL42,
    RSV_NVCL43,
    RSV_NVCL44,
    RSV_NVCL45,
    RSV_NVCL46,
    RSV_NVCL47,
    UNSPEC48,
    UNSPEC49,
    UNSPEC50,
    UNSPEC51,
    UNSPEC52,
    UNSPEC53,
    UNSPEC54,
    UNSPEC55,
    UNSPEC56,
    UNSPEC57,
    UNSPEC58,
    UNSPEC59,
    UNSPEC60,
    UNSPEC61,
    UNSPEC62,
    UNSPEC63
} hevc_nal_unit_type_t;

/* hevc hrd parameters */
struct hevc_hrd_parameters {
    int nal_hrd_parameters_present_flag;
    int vcl_hrd_parameters_present_flag;
    int sub_pic_hrd_params_present_flag;
    int du_cpb_removal_delay_increment_length_minus1;
    int sub_pic_cpb_params_in_pic_timing_sei_flag;
    int au_cpb_removal_delay_length_minus1;
    int dpb_output_delay_length_minus1;
};

/* hevc vui parameters */
struct hevc_vui_parameters {
    int field_seq_flag;
    int frame_field_info_present_flag;
    int timing_info_present_flag;
    int num_units_in_tick;
    int time_scale;
    int hrd_parameters_present_flag;
    struct hevc_hrd_parameters hrd;
};

/* hevc video parameter set */
struct hevc_vid_parameter_set {
    int occupied;
    int vid_parameter_set_id;
    int max_sub_layers_minus1;
    int num_hrd_parameters;
    int timing_info_present_flag;
    int num_units_in_tick;
    int time_scale;
    struct hevc_hrd_parameters hrd;
};

/* hevc short term reference picture set */
struct hevc_short_term_ref_pic_set {
    int num_negative_pics;
    int num_positive_pics;
    int DeltaPocS0[24];
    int DeltaPocS1[24];
    int used_by_curr_pic_s0_flag[24];
    int used_by_curr_pic_s1_flag[24];
};

/* hevc sequence parameter set */
struct hevc_seq_parameter_set {
    int occupied;
    int occupied_warning;
    int vid_parameter_set_id;
    int sps_max_sub_layers_minus1;
    int seq_parameter_set_id;
    int chroma_format_idc;
    int separate_colour_plane_flag;
    int pic_width_in_luma_samples;
    int pic_height_in_luma_samples;
    int bit_depth_luma_minus8;
    int bit_depth_chroma_minus8;
    int log2_max_pic_order_cnt_lsb_minus4;
    int log2_min_luma_coding_block_size_minus3;
    int log2_diff_max_min_luma_coding_block_size;
    int log2_min_transform_block_size_minus2;
    int log2_diff_max_min_transform_block_size;
    int max_transform_hierarchy_depth_inter;
    int max_transform_hierarchy_depth_intra;
    int scaling_list_enable_flag;
    int amp_enabled_flag;
    int sample_adaptive_offset_enabled_flag;
    int pcm_enabled_flag;
    struct hevc_short_term_ref_pic_set ref[64];
    int vui_parameters_present_flag;
    struct hevc_vui_parameters vui;
    /* derived parameters */
    int MaxPicOrderCntLsb;
    int SubWidthC;
    int SubHeightC;
    int MinCbLog2SizeY;
    int CtbLog2SizeY;
    int MinCbSizeY;
    int CtbSizeY;
    int PicWidthInMinCbsY;
    int PicWidthInCtbsY;
    int PicHeightInMinCbsY;
    int PicHeightInCtbsY;
    int PicSizeInMinCbsY;
    int PicSizeInCtbsY;
    int PicSizeInSamplesY;
    int PicWidthInSamplesC;
    int PicHeightInSamplesC;
    int CtbWidthC;
    int CtbHeightC;
    /* extra parameters, not in the spec */
    int MinTbSizeY;
    int PicWidthInMinTbsY;
    int PicHeightInMinTbsY;
    int PicSizeInMinTbsY;
};

/* hevc picture parameter set */
struct hevc_pic_parameter_set {
    int occupied;
    int occupied_warning;
    int pic_parameter_set_id;
    int seq_parameter_set_id;
    struct hevc_seq_parameter_set *sps;
    int dependent_slice_segments_enabled_flag;
    int output_flag_present_flag;
    int num_extra_slice_header_bits;
    int num_ref_idx_l0_default_active_minus1;
    int num_ref_idx_l1_default_active_minus1;
    int init_qp_minus26;
    int weighted_pred_flag;
    /* derived parameters */
};

/* hevc slice type */
typedef enum {
    B_SLICE = 0,
    P_SLICE,
    I_SLICE,
} hevc_slice_type_t;

/* hevc slice header */
struct hevc_slice_header {
    int first_slice_segment_in_pic_flag;
    int pic_parameter_set_id;
    struct hevc_pic_parameter_set *pps;
    hevc_slice_type_t slice_type;
    int slice_pic_order_cnt_lsb;
    /* derived parameters */
    int PicOrderCntMsb;
    int PicOrderCntVal;
    int num_ref_idx_l0_active;
    int num_ref_idx_l1_active;
    int MaxNumMergeCand;
    int SliceQPY;
};

/* hevc slice header previous state */
struct hevc_slice_header_state {
    int prevPicOrderCntLsb;
    int prevPicOrderCntMsb;
};

/* hevc prediction mode */
typedef enum {
    HEVC_MODE_INTRA,
    HEVC_MODE_INTER,
    HEVC_MODE_SKIP
} hevc_pred_mode_t;

typedef enum
{
    HEVC_INTRA_PLANAR = 0,
    HEVC_INTRA_DC = 1,
    HEVC_INTRA_ANGULAR_2 = 2,    HEVC_INTRA_ANGULAR_3 = 3,    HEVC_INTRA_ANGULAR_4 = 4,    HEVC_INTRA_ANGULAR_5 = 5,
    HEVC_INTRA_ANGULAR_6 = 6,    HEVC_INTRA_ANGULAR_7 = 7,    HEVC_INTRA_ANGULAR_8 = 8,    HEVC_INTRA_ANGULAR_9 = 9,
    HEVC_INTRA_ANGULAR_10 = 10,  HEVC_INTRA_ANGULAR_11 = 11,  HEVC_INTRA_ANGULAR_12 = 12,  HEVC_INTRA_ANGULAR_13 = 13,
    HEVC_INTRA_ANGULAR_14 = 14,  HEVC_INTRA_ANGULAR_15 = 15,  HEVC_INTRA_ANGULAR_16 = 16,  HEVC_INTRA_ANGULAR_17 = 17,
    HEVC_INTRA_ANGULAR_18 = 18,  HEVC_INTRA_ANGULAR_19 = 19,  HEVC_INTRA_ANGULAR_20 = 20,  HEVC_INTRA_ANGULAR_21 = 21,
    HEVC_INTRA_ANGULAR_22 = 22,  HEVC_INTRA_ANGULAR_23 = 23,  HEVC_INTRA_ANGULAR_24 = 24,  HEVC_INTRA_ANGULAR_25 = 25,
    HEVC_INTRA_ANGULAR_26 = 26,  HEVC_INTRA_ANGULAR_27 = 27,  HEVC_INTRA_ANGULAR_28 = 28,  HEVC_INTRA_ANGULAR_29 = 29,
    HEVC_INTRA_ANGULAR_30 = 30,  HEVC_INTRA_ANGULAR_31 = 31,  HEVC_INTRA_ANGULAR_32 = 32,  HEVC_INTRA_ANGULAR_33 = 33,
    HEVC_INTRA_ANGULAR_34 = 34
} hevc_intra_pred_mode_t;

typedef enum
{
    HEVC_PART_2Nx2N = 0,
    HEVC_PART_2NxN  = 1,
    HEVC_PART_Nx2N  = 2,
    HEVC_PART_NxN   = 3,
    HEVC_PART_2NxnU = 4,
    HEVC_PART_2NxnD = 5,
    HEVC_PART_nLx2N = 6,
    HEVC_PART_nRx2N = 7
} hevc_part_mode_t;

struct hevc_motion
{
    uint8_t predFlag[2];  // prediction list flags.
    int8_t  refIdx[2];    // index into reference picture list.
    mv_t    mv[2];        // absolute motion vectors.
};

/* hevc coding/prediction/transform unit array type */
template <class T> struct rvhevcarray {
    /* construction */
    rvhevcarray() { unitsize = width = height = 0; }
    ~rvhevcarray() { free(); }

    /* parameters */
    int unitsize;       /* min unit size */
    int width;          /* in min unit size units */
    int height;         /* in min unit size units */

    /* data array */
    T *data;

    /* initialisation */
    void init(int w, int h, int u) { width = w; height = h; unitsize = u; data = (T *) rvalloc(NULL, w*h*sizeof(T), 1); }
    void free() { rvfree(data); }

    /* access */
    /* find by min unit block size coordinates */
    T *get_in_units(int xu, int yu) { return &data[yu*width+xu]; }
    const T *get_in_units(int xu, int yu) const { return &data[yu*width+xu]; }

    /* find by luma pel coordinates */
    T *get(int xb, int yb) { xb/=unitsize; yb/=unitsize; return &data[yb*width+xb]; }
    const T *get(int xb, int yb) const { xb/=unitsize; yb/=unitsize; return &data[yb*width+xb]; }
};

/* hevc coding unit */
struct rvhevccodingunit {
    int idr_num;                    /* orders sequence into sub-sequences split by an idr picture */
    int pic_order_cnt;              /* picture order count within a sub-sequence */
    int slice_num;
    int cu_addr;
    //hevc_slice_type_t slice_type;
    struct hevc_slice_header *sh;
    int transquant_bypass;
    hevc_pred_mode_t pred_mode;
    int pcm_flag;

    /* derived parameters */
    int log2CbSize;
    hevc_intra_pred_mode_t IntraPredMode[4];
    hevc_part_mode_t PartMode;
    int qpy;
};

/* hevc prediction unit */
struct rvhevcpredictionunit {
    /* derived parameters */
    struct hevc_motion motion;
};

/* hevc transform unit */
struct rvhevctransformunit {
    int split_transform_flag;
    /* derived parameters */
};

/* hevc picture */
struct rvhevcpicture {
    struct hevc_slice_header sh;
    rvhevcarray <struct rvhevccodingunit>     cu;
    rvhevcarray <struct rvhevcpredictionunit> pu;
    rvhevcarray <struct rvhevctransformunit>  tu;

    /* derived parameters */
    int MinCbSizeY;
};

/* hevc video sequences */
struct rvhevcvideo {
    int active_vps;
    int active_sps;
    int active_pps;
    struct hevc_vid_parameter_set *vps[16];
    struct hevc_seq_parameter_set *sps[32];
    struct hevc_pic_parameter_set *pps[256];
    size_t size_pic_array;
    struct rvhevcpicture **picture;
};



const char *hevc_nal_unit_type_code(int hevc_nal_unit_type);

struct rvhevcvideo *alloc_hevc_video();
struct rvhevcpicture *alloc_hevc_picture(const struct hevc_seq_parameter_set* sps);
void free_hevc_picture(struct rvhevcpicture* picture);
void free_hevc_video(struct rvhevcvideo* video);

int find_next_hevc_nal_unit(struct bitbuf *bb, int *zerobyte);
int parse_hevc_nal_unit_header(struct bitbuf *bb, int *nal_unit_type);
struct hevc_vid_parameter_set *parse_hevc_vid_param(struct bitbuf *bb);
struct hevc_seq_parameter_set *parse_hevc_seq_param(struct bitbuf *bb);
struct hevc_pic_parameter_set *parse_hevc_pic_param(struct bitbuf *bb, struct hevc_seq_parameter_set **spss);
struct hevc_slice_header *parse_hevc_slice_header(struct bitbuf *bb, int nal_unit_type, struct hevc_pic_parameter_set **ppss, struct hevc_slice_header_state *prev);
void parse_hevc_sei_rbsp(struct bitbuf *bb, struct hevc_seq_parameter_set *sps, int nal_unit_type, int verbose);
void parse_hevc_access_unit_delimiter(struct bitbuf *bb, int verbose);

struct rvhevcvideo *parse_hevc_params(bitbuf *bb, int verbose);

#ifdef HAVE_LIBDE265
struct hevc_vid_parameter_set *extract_hevc_vid_param(const struct de265_image *image);
struct hevc_seq_parameter_set *extract_hevc_seq_param(const struct de265_image *image);
struct hevc_pic_parameter_set *extract_hevc_pic_param(const struct de265_image *image, struct hevc_seq_parameter_set **spss);
void extract_hevc_slice_header(const struct de265_image *image, struct hevc_slice_header &sh, struct hevc_pic_parameter_set **ppss);
void extract_hevc_ctus(const struct de265_image *image, struct rvhevcpicture &pic);
#endif

#endif
