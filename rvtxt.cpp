/*
 * Description: Convert text files in various formats to/from binary.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-04-27 15:56:10 $
 * Revision   : $Revision: 1.21 $
 * Copyright  : (c) 2005 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include "getopt.h"

#include "rvutil.h"
#include "rvendian.h"

/* list of conversions supported */
typedef enum {
    UNSUPPORTED,
    CHAR_BIN,
    CHAR_HEXCHAR,
    CHAR_HEXLONG,
    CHAR_HEXLONGLONG,
    BIN_CHAR,
    BIN_LONG,
    BIN_HEXCHAR,
    BIN_HEXLONG,
    BIN_HEXLONGLONG,
    HEX_BIN,
} conv_t;

/* conversion names struct */
static struct tag_names {
    conv_t  conv;
    const char *short_name;
    const char *long_name;
    const char *alt_name;
} names[] = {
    { UNSUPPORTED,      "usp", "unsupported",      "unsupported",     },
    { CHAR_BIN,         "c2b", "char_bin",         "char2bin",        },
    { CHAR_HEXCHAR,     "c2h", "char_hexchar",     "char2hexchar",    },
    { CHAR_HEXLONG,     "c2x", "char_hexlong",     "char2hexlong",    },
    { CHAR_HEXLONGLONG, "c2g", "char_hexlonglong", "char2hexlonglong",},
    { BIN_CHAR,         "b2c", "bin_char",         "bin2char",        },
    { BIN_LONG,         "b2l", "bin_long",         "bin2long",        },
    { BIN_HEXCHAR,      "b2h", "bin_hexchar",      "bin2hexchar",     },
    { BIN_HEXLONG,      "b2x", "bin_hexlong",      "bin2hexlong",     },
    { BIN_HEXLONGLONG,  "b2g", "bin_hexlonglong",  "bin2hexlonglong", },
    { HEX_BIN,          "h2b", "hex_bin",          "hex2bin",         },
};

const char *appname;

void usage(int exitcode)
{
    fprintf(stderr, "%s: convert video files to/from textio format for vhdl testbenches\n", appname);
    fprintf(stderr, "usage: %s -f <format> [options] [<file>] [<file>...]\n", appname);
    fprintf(stderr, "\t-f <format> : format for conversion\n");
    fprintf(stderr, "\t-l <num>    : skip num leading bytes of each file (default: 0)\n");
    fprintf(stderr, "\t-s          : swap endianness\n");
    fprintf(stderr, "\t-o <file>   : write output to file\n");
    fprintf(stderr, "\t--          : disable argument processing\n");
    fprintf(stderr, "\t-h          : print this usage message\n");
    fprintf(stderr, "\nSupported conversion formats (one per line):\n");
    for (unsigned i=1; i<sizeof(names)/sizeof(names[0]); i++)
        fprintf(stderr, "\t%3s  %15s  %15s\n", names[i].short_name, names[i].long_name, names[i].alt_name);
    exit(exitcode);
}

int main(int argc, char *argv[])
{
    FILE *filein = stdin;
    FILE *fileout = stdout;
    char *filename[RV_MAXFILES] = {0};
    char *outfile = NULL;
    unsigned fileindex = 0;
    unsigned numfiles = 0;
    int bytes = 0;
    conv_t conv = CHAR_BIN;

    /* command line defaults */
    int verbose = 0;
    char *format = NULL;
    int skip = 0;
    int swap = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"format",       1, NULL, 'f'},
            {"skip-leading", 1, NULL, 'l'},
            {"swap-endian",  0, NULL, 's'},
            {"output",       1, NULL, 'o'},
            {"quiet",        0, NULL, 'q'},
            {"verbose",      0, NULL, 'v'},
            {"usage",        0, NULL, 'h'},
            {"help",         0, NULL, 'h'},
            {NULL,           0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "f:l:so:qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 'f':
                format = optarg;
                break;

            case 'l':
                skip = atoi(optarg);
                break;

            case 's':
                swap = 1;
                break;

            case 'o':
                outfile = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < sizeof(filename)/sizeof(filename[0]))
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    if (format) {
        unsigned i;
        for (i=0; i<sizeof(names)/sizeof(names[0]); i++) {
            if (strcasecmp(format, names[i].short_name)==0 || strcasecmp(format, names[i].long_name)==0 || strcasecmp(format, names[i].alt_name)==0) {
                conv = names[i].conv;
                break;
            }
        }
        if (i==sizeof(names)/sizeof(names[0]))
            rvexit("conversion specification not recognised: %s", format);
    }
    if (conv==UNSUPPORTED)
        rvexit("can't determine conversion to perform");

    /* open output file */
    if (outfile)
        fileout = fopen(outfile, "w");
    if (fileout==NULL)
        rverror("failed to open output file \"%s\"", outfile);

    /* ignore SIGPIPE on output file */
    if (signal(SIGPIPE, SIG_IGN)==SIG_ERR)
        rverror("failed to set ignore on SIGPIPE");

    /* loop over input files */
    do {
        int warning = 0;

        if (numfiles)
            filein = fopen(filename[fileindex], "r");
        if (filein==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex]);

        /* skip leading bytes */
        if (skip)
            fseeko(filein, skip, SEEK_CUR);

        switch (conv) {
            /* convert decimal text to binary */
            case CHAR_BIN:
            {
                int d;
                do {
                    int n = fscanf(filein, " %d", &d);
                    if (n != 1) {
                        char junk[16];
                        if (feof(filein))
                            break;
                        n = fscanf(filein, " %16s", junk);
                        if (n && junk[0]=='x') {
                            warning++;
                            d = 0;
                        } else
                            rverror("input format error in file \"%s\": %s", filename[fileindex], junk);
                    }

                    n = fwrite(&d, 1, 1, fileout);
                    if (n != 1)
                        break;

                    bytes++;
                } while (!feof(filein) && !ferror(filein));
                if (ferror(filein)) {
                    if (filename[fileindex])
                        rverror("error reading input file \"%s\"", filename[fileindex]);
                    else
                        rverror("error reading from standard input");
                }
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");
                break;
            }

            /* convert decimal bytes to hexadecimal bytes */
            case CHAR_HEXCHAR:
            {
                int d;
                do {
                    int n = fscanf(filein, " %d", &d);
                    if (n != 1) {
                        char junk[16];
                        if (feof(filein))
                            break;
                        n = fscanf(filein, " %16s", junk);
                        if (n && junk[0]=='x')
                            d = 0;
                        else
                            rverror("input format error in file \"%s\": %s", filename[fileindex], junk);
                    }

                    n = fprintf(fileout, "%02hhx\n", d);
                    if (n < 0)
                        break;

                    bytes++;
                } while (!feof(filein) && !ferror(filein));
                if (ferror(filein)) {
                    if (filename[fileindex])
                        rverror("error reading input file \"%s\"", filename[fileindex]);
                    else
                        rverror("error reading from standard input");
                }
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");
                break;
            }

            /* convert decimal bytes to hexadecimal dwords */
            case CHAR_HEXLONG:
            {
                int i, d[4], n;
                do {
                    for (i=0; i<4; i++) {
                        n = fscanf(filein, " %d", &d[i]);
                        if (n != 1) {
                            char junk[16];
                            if (feof(filein))
                                break;
                            n = fscanf(filein, " %16s", junk);
                            if (n && junk[0]=='x')
                                d[i] = 0;
                            else
                                rverror("input format error in file \"%s\": %s", filename[fileindex], junk);
                        }
                    }

                    if (!swap)
                        n = fprintf(fileout, "%02hhx%02hhx%02hhx%02hhx\n", d[0], d[1], d[2], d[3]);
                    else
                        n = fprintf(fileout, "%02hhx%02hhx%02hhx%02hhx\n", d[3], d[2], d[1], d[0]);
                    if (n < 0)
                        break;

                    bytes++;
                } while (!feof(filein) && !ferror(filein));
                if (ferror(filein)) {
                    if (filename[fileindex])
                        rverror("error reading input file \"%s\"", filename[fileindex]);
                    else
                        rverror("error reading from standard input");
                }
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");
                break;
            }

            /* convert decimal bytes to hexadecimal double dwords */
            case CHAR_HEXLONGLONG:
            {
                int i, d[8], n;
                do {
                    for (i=0; i<8; i++) {
                        n = fscanf(filein, " %d", &d[i]);
                        if (n != 1) {
                            char junk[16];
                            n = fscanf(filein, " %16s", junk);
                            if (n && junk[0]=='x')
                                d[i] = 0;
                            else
                                rverror("input format error in file \"%s\": %s", filename[fileindex], junk);
                        }
                    }

                    if (!swap)
                        n = fprintf(fileout, "%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx\n", d[0], d[1], d[2], d[3], d[4], d[5], d[6], d[7]);
                    else
                        n = fprintf(fileout, "%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx\n", d[7], d[6], d[5], d[4], d[3], d[2], d[1], d[0]);
                    if (n < 0)
                        break;

                    bytes++;
                } while (!feof(filein) && !ferror(filein));
                if (ferror(filein)) {
                    if (filename[fileindex])
                        rverror("error reading input file \"%s\"", filename[fileindex]);
                    else
                        rverror("error reading from standard input");
                }
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");
                break;
            }

            /* convert binary to unsigned decimal bytes */
            case BIN_CHAR:
            {
                unsigned char d;
                do {
                    int n = fread(&d, 1, 1, filein);
                    if (n==0)
                        break;

                    n = fprintf(fileout, "%u\n", d);
                    if (n < 0)
                        break;

                    bytes++;
                } while (!feof(filein) && !ferror(filein));
                if (ferror(filein)) {
                    if (filename[fileindex])
                        rverror("error reading input file \"%s\"", filename[fileindex]);
                    else
                        rverror("error reading from standard input");
                }
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");
                break;
            }

            /* convert binary to unsigned decimal dwords */
            case BIN_LONG:
            {
                unsigned long d;
                do {
                    int n = fread(&d, 1, 4, filein);
                    if (n==0)
                        break;
                    if (n<4 && verbose>=0)
                        rvmessage("warning: input file is not a whole number of longs: %d bytes in last word", n);

                    if (swap)
                        d = swapendian(d);

                    n = fprintf(fileout, "%lu\n", d);
                    if (n < 0)
                        break;

                    bytes+=4;
                } while (!feof(filein) && !ferror(filein));
                if (ferror(filein)) {
                    if (filename[fileindex])
                        rverror("error reading input file \"%s\"", filename[fileindex]);
                    else
                        rverror("error reading from standard input");
                }
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");
                break;
            }

            /* convert binary to hexadecimal bytes */
            case BIN_HEXCHAR:
            {
                unsigned char d;
                do {
                    int n = fread(&d, 1, 1, filein);
                    if (n==0)
                        break;

                    n = fprintf(fileout, "%02hhx\n", d);
                    if (n < 0)
                        break;

                    bytes++;
                } while (!feof(filein) && !ferror(filein));
                if (ferror(filein)) {
                    if (filename[fileindex])
                        rverror("error reading input file \"%s\"", filename[fileindex]);
                    else
                        rverror("error reading from standard input");
                }
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");
                break;
            }

            /* convert binary to hexadecimal dwords */
            case BIN_HEXLONG:
            {
                unsigned long d;
                do {
                    int n = fread(&d, 1, 4, filein);
                    if (n==0)
                        break;
                    if (n<4 && verbose>=0)
                        rvmessage("warning: input file is not a whole number of longs: %d bytes in last word", n);

                    if (swap)
                        d = swapendian(d);

                    n = fprintf(fileout, "%08lx\n", d);
                    if (n < 0)
                        break;

                    bytes+=4;
                } while (!feof(filein) && !ferror(filein));
                if (ferror(filein)) {
                    if (filename[fileindex])
                        rverror("error reading input file \"%s\"", filename[fileindex]);
                    else
                        rverror("error reading from standard input");
                }
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");
                break;
            }

            /* convert binary to hexadecimal double dwords */
            case BIN_HEXLONGLONG:
            {
                unsigned long long d;
                do {
                    int n = fread(&d, 1, 8, filein);
                    if (n==0)
                        break;
                    if (n<8 && verbose>=0)
                        rvmessage("warning: input file is not a whole number of long longs: %d bytes in last word", n);

                    if (swap)
                        d = swapendianll(d);

                    n = fprintf(fileout, "%016llx\n", d);
                    if (n < 0)
                        break;

                    bytes+=8;
                } while (!feof(filein) && !ferror(filein));
                if (ferror(filein)) {
                    if (filename[fileindex])
                        rverror("error reading input file \"%s\"", filename[fileindex]);
                    else
                        rverror("error reading from standard input");
                }
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");
                break;
            }

            /* convert hexadecimal text to binary */
            case HEX_BIN:
            {
                int d;
                do {
                    int n = fscanf(filein, " %x", &d);
                    if (n != 1) {
                        char junk[16];
                        if (feof(filein))
                            break;
                        n = fscanf(filein, " %16s", junk);
                        if (n && junk[0]=='x') {
                            warning++;
                            d = 0;
                        } else
                            rverror("input format error in file \"%s\": %s", filename[fileindex], junk);
                    }

                    n = fwrite(&d, 1, 1, fileout);
                    if (n != 1)
                        break;

                    bytes++;
                } while (!feof(filein) && !ferror(filein));
                if (ferror(filein)) {
                    if (filename[fileindex])
                        rverror("error reading input file \"%s\"", filename[fileindex]);
                    else
                        rverror("error reading from standard input");
                }
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");
                break;
            }

            case UNSUPPORTED:
                ;
        }

        /* print warning message if x's found  */
        if (warning==1)
            rvmessage("warning: an x was found in file \"%s\"", filename[fileindex]);
        else if (warning)
            rvmessage("warning: %d x's were found in file \"%s\"", warning, filename[fileindex]);

    } while (++fileindex<numfiles);

    if (verbose>=1)
        rvmessage("converted %d bytes", bytes);

    return 0;
}
