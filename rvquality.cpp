/*
 * Description: Compare video sequences.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2010-01-22 15:52:40 $
 * Revision   : $Revision: 1.37 $
 * Copyright  : (c) 2005 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <signal.h>
#include "getopt.h"

#include "rvutil.h"
#include "rvy4m.h"
#include "rvstat.h"
#include "rvimage.h"

const char *appname;

void usage(int exitcode)
{
    fprintf(stderr, "%s: calculate image quality metrics\n", appname);
    fprintf(stderr, "usage: %s [options] [<file>] [<file>...] <reference>\n", appname);
    fprintf(stderr, " note: test image dimensions do not have to be the same as the reference image\n");
    fprintf(stderr, "  -t, --threshold     : threshold of absolute pel difference (default: 0)\n");
    fprintf(stderr, "  -m, --ssim          : include SSIM quality measure (default: don't calculate)\n");
    fprintf(stderr, "  -s, --size  : reference image size format\n");
    fprintf(stderr, "     (formats : %s)\n", rvformats);
    fprintf(stderr, "  -p, --fourcc        : image pixel format fourcc (default: I420)\n");
    fprintf(stderr, "     (formats : %s)\n", rvfourccs);
    fprintf(stderr, "  -a, --skip          : skip first number of frames in reference\n");
    fprintf(stderr, "  -n, --num           : only test number of frames in reference\n");
    fprintf(stderr, "  -o, --output        : write per-frame quality metrics to file (default: no per-frame metrics)\n");
    fprintf(stderr, "  -q, --quiet         : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "  -v, --verbose       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "  --                  : disable argument processing\n");
    fprintf(stderr, "  -h, --help, --usage : print this usage message\n");
    exit(exitcode);
}

int main(int argc, char *argv[])
{
    FILE *filein = stdin;
    FILE *fileout = NULL;
    char *filename[RV_MAXFILES] = {0};
    char *outfile = NULL;
    int fileindex = 0;
    int numfiles = 0;
    int totframes = 0;

    /* command line defaults */
    int threshold = 0;
    int calc_ssim = 0;
    int ref_width = 0;
    int ref_height = 0;
    const char *format = NULL;
    const char *fccode = NULL;
    int skipframes = 0;
    int testframes = -1;
    int raw_inp = 1;
    int raw_ref = 1;
    int basename = 0;
    int verbose = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"threshold",  1, NULL, 't'},
            {"ssim",       0, NULL, 'm'},
            {"size",       1, NULL, 's'},
            {"basename",   1, NULL, 'b'},
            {"skip",       1, NULL, 'a'},
            {"output",     1, NULL, 'o'},
            {"quiet",      0, NULL, 'q'},
            {"verbose",    0, NULL, 'v'},
            {"usage",      0, NULL, 'h'},
            {"help",       0, NULL, 'h'},
            {NULL,         0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "t:ms:p:a:n:bo:qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 't':
                threshold = atoi(optarg);
                if (threshold<0)
                    rvexit("invalid value for threshold: %d", threshold);
                break;

            case 'm':
                calc_ssim = 1;
                break;

            case 's':
                format = optarg;
                break;

            case 'p':
                fccode = optarg;
                break;

            case 'a':
                skipframes = atoi(optarg);
                if (skipframes<0)
                    rvexit("invalid value for number of frames to skip: %d", skipframes);
                break;

            case 'n':
                testframes = atoi(optarg);
                if (testframes<0)
                    rvexit("invalid value for number of frames to test: %d", testframes);
                break;

            case 'b':
                basename = 1;
                break;

            case 'o':
                outfile = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < RV_MAXFILES)
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    /* sanity check command line */
    if (numfiles==0)
        usage(1);

    /* convert fourcc string into integer */
    unsigned int ref_fourcc = 0;
    if (fccode)
        ref_fourcc = get_fourcc(fccode);

    /* open reference file */
    FILE* refin = fopen(filename[numfiles-1], "rb");
    if (refin==NULL)
        rverror("failed to open reference file \"%s\"", filename[numfiles-1]);
    if (verbose>=2)
        rvmessage("reference file: \"%s\"", filename[numfiles-1]);

    /* look for a pixel format fourcc in reference file name */
    ref_fourcc = divine_pixel_format(filename[numfiles-1]);
    if (ref_fourcc==0)
        ref_fourcc = I420;
    if (ref_fourcc==UYVY || ref_fourcc==YUY2 || ref_fourcc==TWOVUY)
        rvexit("unsupported pixel format: %s", fourccname(ref_fourcc));

    /* get chroma subsampling from fourcc */
    int hsub, vsub;
    get_chromasub(ref_fourcc, &hsub, &vsub);
    if (hsub==-1 || vsub==-1)
        rvexit("unknown pixel format: %s", fourccname(ref_fourcc));

    /* parse reference file for dimensions */
    if (raw_ref==0 || divine_image_dims(&ref_width, &ref_height, NULL, NULL, NULL, format, filename[numfiles-1])<0) {
        /* must be yuv4mpeg format file */
        raw_ref = 0;
        if (read_y4m_stream_header(&ref_width, &ref_height, NULL, refin)<0)
            rverror("failed to read yuv4mpeg stream header from reference file");
    }

    /* open output file */
    if (outfile) {
        fileout = fopen(outfile, "w");
        if (fileout==NULL)
            rverror("failed to open output file \"%s\"", outfile);
    }

    /* ignore SIGPIPE on output file */
    if (signal(SIGPIPE, SIG_IGN)==SIG_ERR)
        rverror("failed to set ignore on SIGPIPE");

    /* allocate data buffer */
    struct rvimage ref(ref_width, ref_width, ref_height, hsub, vsub, 8);

    /* loop over input files */
    int diffs = 0;
    do {
        int numframes = 0;

        /* initialise statistics */
        double mse  = 0.0;
        double psnr = 0.0;
        double ssim = 0.0;
        int maxdiff = 0;
        int errorframe = -1;
        int errormacro;

        /* set position in reference file */
        if (fseeko(refin, (off_t)skipframes*ref.size, SEEK_SET)<0)
            rverror("failed to seek in reference file");

        /* prepare to parse file */
        struct bitbuf *bb = initbits_filename(filename[fileindex], B_GETBITS);
        if (bb==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex]);

        /* determine filetype */
        divine_t divine = divine_filetype(bb, filename[fileindex], format, verbose);
        if (rewindbits(bb)<0)
            rverror("searched too far on non-seekable input file \"%s\"", filename[fileindex]);
        if (verbose>=2)
            rvmessage("input file %d: \"%s\" has type %s", fileindex, filename[fileindex], filetypename[divine.filetype]);

        /* select what to do based on filetype */
        switch (divine.filetype) {
            case YUV:
            case YUV4MPEG:
            {
                if (fileindex<numfiles-1)
                    filein = fopen(filename[fileindex], "rb");
                if (filein==NULL)
                    rverror("failed to open file \"%s\"", filename[fileindex]);

                /* parse input file for dimensions */
                int inp_width = ref_width;
                int inp_height = ref_height;
                if (raw_inp==0 || divine_image_dims(&inp_width, &inp_height, NULL, NULL, NULL, format, filename[fileindex])<0) {
                    /* must be yuv4mpeg format file */
                    raw_inp = 0;
                    if (read_y4m_stream_header(&inp_width, &inp_height, NULL, filein)<0)
                        rverror("failed to read yuv4mpeg stream header");
                } else
                    raw_inp = 1;

                /* check input file pixel format */
                unsigned int inp_fourcc = divine_pixel_format(filename[fileindex]);
                if (inp_fourcc==0)
                    inp_fourcc = ref_fourcc;
                if (inp_fourcc != ref_fourcc) {
                    rvmessage("warning: ignoring file \"%s\" with different pixel format from reference: %c%c%c%c", filename[fileindex],
                            inp_fourcc>>0, inp_fourcc>>8, inp_fourcc>>16, inp_fourcc>>24);
                    fclose(filein);
                    continue;
                }

                /* allocate image buffer */
                struct rvimage inp(inp_width, inp_width, inp_height, hsub, vsub, 8);

                /* main loop */
                int frameno;
                for (frameno=skipframes; (frameno<testframes+skipframes || testframes<0) && !feof(filein) && !ferror(filein) && !feof(refin) && !ferror(refin); frameno++)
                {
                    /* read a frame from input file */
                    if (inp.fread(filein, !raw_inp)!=1)
                        break;

                    /* read a frame from reference file */
                    if (ref.fread(refin, !raw_ref)!=1)
                        break;

                    /* compute quality statistics on luma plane only */
                    struct rvstats stats = calculate_stats(inp, ref, calc_ssim? 4 : 3, LUMA_PLANE);

                    /* accumulate quality statistics over each frame */
                    if (isfinite(stats.mse))
                        mse  += stats.mse;
                    if (isfinite(stats.psnr))
                        psnr += stats.psnr;
                    if (isfinite(stats.ssim))
                        ssim += stats.ssim;
                    if (stats.maxdiff>maxdiff)
                        maxdiff = stats.maxdiff;

                    /* record first frame with differences */
                    if (stats.maxdiff>threshold && errorframe<0) {
                        errorframe = frameno;
                        errormacro = (stats.maxdiffy/16)*(inp_width/16) + stats.maxdiffx/16;
                    }

                    if (fileout) {
                        fprintf(fileout, "%.2f", stats.psnr);
                        if (calc_ssim)
                            fprintf(fileout, " %.2f", stats.ssim);
                        fprintf(fileout, "\n");
                    }

                    numframes++;
                }
                if (ferror(filein))
                    rverror("failed to read from input file");
                if (ferror(refin))
                    rverror("failed to read from reference file");

                /* tidy up */
                if (fileindex<numfiles-1)
                    fclose(filein);

                break;
            }

            case M2V:
            {

                /* parse mpeg2 headers */
                struct rvm2vsequence *sequence = parse_m2v_headers(bb, verbose);
                if (sequence==NULL) {
                    rvmessage("failed to parse headers in mpeg2 sequence \"%s\"", filename[fileindex]);
                    break;
                }

                /* get frame parameters */
                int inp_width  = sequence->width_in_mbs*16;
                int inp_height = sequence->height_in_mbs*16;
                //chromaformat_t chromaformat = sequence->chroma_format==2? YUV422 : YUV420mpeg;

                /* TODO check pixel format */

                /* main loop */
                int frameno, picno;
                sample_t *reference[2] = {NULL, NULL};
                for (frameno=0, picno=0; (frameno<testframes || testframes<0) && !eofbits(bb) && !feof(refin) && !ferror(refin); frameno++, picno++)
                {
                    /* decode a frame from input file */
                    if (verbose>=1)
                        rvmessage("picture %d", picno);

                    /* find next picture start code */
                    int gop_flags;
                    if (find_m2v_next_picture(bb, sequence, picno, &gop_flags, verbose)<0)
                        break;

                    /* parse first picture */
                    struct rvm2vpicture *picture = parse_m2v_picture(bb, sequence, picno, verbose);
                    if (picture==NULL) {
                        rvmessage("failed to parse picture %d of mpeg2 sequence \"%s\"", picno, filename[fileindex]);
                        break;
                    }

                    /* decode first picture NOTE always use reference idct */
                    sample_t *firstpic = decode_m2v(picture, sequence, reference[0], reference[1], picno, 1);
                    if (firstpic==NULL) {
                        rvmessage("failed to decode picture %d of mpeg2 sequence \"%s\"", picno, filename[fileindex]);
                        break;
                    }

                    /* check if picture was field picture */
                    sample_t *newframe = NULL;
                    if (field_picture(picture)) {
                        picno++;

                        /* tidy up from first field picture */
                        free_m2v_picture(picture);

                        /* parse second picture */
                        picture = parse_m2v_picture(bb, sequence, picno, verbose);
                        if (picture==NULL) {
                            rvmessage("failed to parse second field picture %d of mpeg2 sequence \"%s\"", picno, filename[fileindex]);
                            break;
                        }

                        sample_t *secondpic;
                        if (picture->picture_coding_type==PCT_P) {

                            /* prepare reference frame from previous field pictures */
                            sample_t *reffield, *refframe;
                            if (picno==1) {
                                /* prepare reference frame from just first field picture */
                                reffield = NULL;
                                refframe = copy_fields_into_frame(firstpic, firstpic, inp_width, inp_height, sequence->chroma_format);
                            } else if (picture->extension.picture_structure==PST_BOTTOM) {
                                reffield = copy_field_from_frame(reference[1], 0, inp_width, inp_height, sequence->chroma_format);
                                refframe = copy_fields_into_frame(firstpic, reffield, inp_width, inp_height, sequence->chroma_format);
                            } else {
                                reffield = copy_field_from_frame(reference[1], 1, inp_width, inp_height, sequence->chroma_format);
                                refframe = copy_fields_into_frame(reffield, firstpic, inp_width, inp_height, sequence->chroma_format);
                            }

                            /* decode second picture NOTE always use reference idct */
                            secondpic = decode_m2v(picture, sequence, NULL, refframe, picno, 1);
                            if (secondpic==NULL) {
                                rvmessage("failed to decode second field picture %d of mpeg2 sequence \"%s\"", picno, filename[fileindex]);
                                break;
                            }

                            rvfree(reffield);
                            rvfree(refframe);

                        } else {

                            /* decode second picture NOTE always use reference idct */
                            secondpic = decode_m2v(picture, sequence, reference[0], reference[1], picno, 1);
                            if (firstpic==NULL) {
                                rvmessage("failed to decode second field picture %d of mpeg2 sequence \"%s\"", picno, filename[fileindex]);
                                break;
                            }

                        }

                        /* merge field pictures */
                        if (picture->extension.picture_structure==PST_BOTTOM)
                            newframe = copy_fields_into_frame(firstpic, secondpic, inp_width, inp_height, sequence->chroma_format);
                        else
                            newframe = copy_fields_into_frame(secondpic, firstpic, inp_width, inp_height, sequence->chroma_format);

                    } else
                        /* frame picture */
                        newframe = firstpic;

                    /* test decoded frame */
                    if (reference[1]) {
                        /* read a frame from reference file */
                        if (ref.fread(refin, !raw_ref)!=1)
                            break;

                        /* reorder output of frames */
                        struct rvimage inp(inp_width, inp_width, inp_height, 0, 0, 8);
                        if (picture->picture_coding_type==PCT_B)
                            /* test B-picture */
                            inp = newframe;
                        else
                            /* test previous I- or P-picture to output */
                            inp = reference[1];

                        /* compute quality statistics on luma plane only */
                        struct rvstats stats = calculate_stats(inp, ref, calc_ssim? 4 : 3, LUMA_PLANE);

                        /* accumulate quality statistics over each frame */
                        if (isfinite(stats.mse))
                            mse  += stats.mse;
                        if (isfinite(stats.psnr))
                            psnr += stats.psnr;
                        if (isfinite(stats.ssim))
                            ssim += stats.ssim;
                        if (stats.maxdiff>maxdiff)
                            maxdiff = stats.maxdiff;

                        /* record first frame with differences */
                        if (stats.maxdiff>threshold && errorframe<0) {
                            errorframe = frameno;
                            errormacro = (stats.maxdiffy/16)*(inp_width/16) + stats.maxdiffx/16;
                        }

                        if (fileout) {
                            fprintf(fileout, "%.2f", stats.psnr);
                            if (calc_ssim)
                                fprintf(fileout, " %.2f", stats.ssim);
                            fprintf(fileout, "\n");
                        }
                    }

                    /* reorder reference frames */
                    if (reference[1]) {
                        if (picture->picture_coding_type==PCT_B) {
                            rvfree(newframe);
                        } else {
                            rvfree(reference[0]);
                            reference[0] = reference[1];
                            reference[1] = newframe;
                        }
                    } else
                        reference[1] = newframe;

                    /* tidy up */
                    free_m2v_picture(picture);

                    numframes++;
                }
                if (ferror(refin))
                    rverror("failed to read from reference file");

                /* test last frame */
                do {
                    /* read a frame from reference file */
                    if (ref.fread(refin, !raw_ref)!=1)
                        break;

                    /* reorder output of frames */
                    struct rvimage inp(inp_width, inp_width, inp_height, 0, 0, 8);
                    inp = reference[1];

                    /* compute quality statistics on luma plane only */
                    struct rvstats stats = calculate_stats(inp, ref, calc_ssim? 4 : 3, LUMA_PLANE);

                    /* accumulate quality statistics over each frame */
                    if (isfinite(stats.mse))
                        mse  += stats.mse;
                    if (isfinite(stats.psnr))
                        psnr += stats.psnr;
                    if (isfinite(stats.ssim))
                        ssim += stats.ssim;
                    if (stats.maxdiff>maxdiff)
                        maxdiff = stats.maxdiff;

                    /* record first frame with differences */
                    if (stats.maxdiff>threshold && errorframe<0) {
                        errorframe = frameno;
                        errormacro = (stats.maxdiffy/16)*(inp_width/16) + stats.maxdiffx/16;
                    }

                    if (fileout) {
                        fprintf(fileout, "%.2f", stats.psnr);
                        if (calc_ssim)
                            fprintf(fileout, " %.2f", stats.ssim);
                        fprintf(fileout, "\n");
                    }
                } while (0);

                /* tidy up */
                rvfree(reference[0]);
                rvfree(reference[1]);
                free_m2v_sequence(sequence);

                break;
            }

            case EMPTY:
                rvexit("empty file: \"%s\"", filename[fileindex]);
                break;

            default:
                rvmessage("filetype of file \"%s\" is not supported: %s", filename[fileindex], filetypename[divine.filetype]);
                break;
        }

        /* display results */
        if (verbose>=0 && numframes) {
            const char *name = basename? get_basename(filename[fileindex]) : filename[fileindex];
            fprintf(stdout, "%s: overallPSNR=%.3f averagePSNR=%.3f", name, 20.0*log10(255.0/sqrt(mse/numframes)), psnr/numframes);
            if (calc_ssim)
                fprintf(stdout, " SSIM=%.3f", ssim/numframes);
            fprintf(stdout, " maxdiff=%d", maxdiff);
            if (errorframe>=0)
                fprintf(stdout, " (error in frame=%d/%d macroblock %d)", errorframe, numframes+skipframes, errormacro);
            fprintf(stdout, "\n");
        }

        /* count number of files which don't match */
        if (maxdiff>threshold)
            diffs++;

        totframes += numframes;

        /* tidy up */
        freebits(bb);

    } while (++fileindex<numfiles-1);

    /* tidy up */
    if (outfile)
        fclose(fileout);
    fclose(refin);

    if (totframes==0) {
        if (verbose>=0)
            rvmessage("warning: zero frames tested");
        return -1;
    }
    if (verbose>=1)
        rvmessage("tested %d frames", totframes);

    return diffs;
}
