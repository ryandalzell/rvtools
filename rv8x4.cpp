/*
 * Description: Convert tiled YUV data to planar YUV.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2010-01-22 15:52:39 $
 * Revision   : $Revision: 1.29 $
 * Copyright  : (c) 2005 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include "getopt.h"

#include "rvutil.h"
#include "rvy4m.h"

/* list of tiled formats supported */
enum format_t {
    UNSUPPORTED,
    PLANAR8x4,
    PACKED4x4,
    PACKED4x410,
    MBPLANAR,
    MBINTER,
    MBCOMPACT,
    MBCAPTURE,
    MBTILED,
    MBLUMA,
};

const char *appname;

void usage(int exitcode)
{
    fprintf(stderr, "%s: convert yuv data from tiled formats (8x4, 4x4, mbplanar, mbinterleaved) to planar\n", appname);
    fprintf(stderr, "usage: %s [options] [<file>] [<file>...]\n", appname);
    fprintf(stderr, "  -f <format>         : format of tiled data (default: 4x4)\n");
    fprintf(stderr, "     (formats : 8x4, 4x4, 4x410, macroblock, mbinterleaved, mbcompact, mbcapture, mbtiled, mbluma)\n");
    fprintf(stderr, "  -r, --reverse       : reverse direction, convert from planar to tiled format\n");
    fprintf(stderr, "  -s, --size          : image size format\n");
    fprintf(stderr, "     (formats : %s)\n", rvformats);
    fprintf(stderr, "  -p, --fourcc        : image pixel format fourcc (default: I420)\n");
    fprintf(stderr, "     (formats : %s)\n", rvfourccs);
    fprintf(stderr, "  -n, --numframes     : number of frames (default: all)\n");
    fprintf(stderr, "  -o, --output        : write output to file\n");
    fprintf(stderr, "  -4, --yuv4mpeg      : write yuv4mpeg format to output (default: raw yuv format)\n");
    fprintf(stderr, "  -q, --quiet         : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "  -v, --verbose       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "  --                  : disable argument processing\n");
    fprintf(stderr, "  -h, --help, --usage : print this usage message\n");
    exit(exitcode);
}

/* convert a single plane of yuv data from 8x4 format to planar */
void conv8x4toyuv(unsigned char *inp, unsigned char *out, int width, int height, int pitch)
{
    int i,j,k;
    unsigned char *r0,*r1,*r2,*r3;
    unsigned char *s0,*s1,*s2,*s3;

    for (j=0; j<height/4; j++) {
        for (i=0; i<width/8; i++) {
            r0 = inp + j * width * 4 + i*32;
            r1 = r0 + 8;
            r2 = r1 + 8;
            r3 = r2 + 8;
            s0 = out + j * pitch * 4 + i*8;
            s1 = s0 + pitch;
            s2 = s1 + pitch;
            s3 = s2 + pitch;

            for (k=0; k<8; k++) *s0++ = *r0++;
            for (k=0; k<8; k++) *s1++ = *r1++;
            for (k=0; k<8; k++) *s2++ = *r2++;
            for (k=0; k<8; k++) *s3++ = *r3++;
        }
    }
}

/* convert three planes of yuv data from 4x4 packed format to planar */
void conv4x4toyuv(unsigned char *inp, unsigned char *out, int width, int height)
{
    int i,j,k,l;

    for (j=0; j<height; j+=4) {
        for (i=0; i<width; i+=4) {
            int i_t = (j * width * 2) + (i * 8);
            int i_y = (j * width) + i;
            int i_u = (j * width / 4) + (i / 2) + (width * height);
            int i_v = (j * width / 4) + (i / 2) + (5 * width * height / 4);

            /* luma */
            for (k=0; k<4; k++)
                for (l=0; l<4; l++)
                    out[i_y + (k * width) + l] = inp[i_t + (k * 4) + l];

            /* chroma blue */
            for (k=0; k<2; k++)
                for (l=0; l<2; l++)
                    out[i_u + (k * width / 2) + l] = inp[i_t + (k * 4) + l + 16];

            /* chroma red */
            for (k=0; k<2; k++)
                for (l=0; l<2; l++)
                    out[i_v + (k * width / 2) + l] = inp[i_t + (k * 4) + l + 24];
        }
    }
}

void conv4x410toyuv(unsigned short *inp, unsigned char *out, int width, int height)
{
    int i,j,k,l;

    for (j=0; j<height; j+=4) {
        for (i=0; i<width; i+=4) {
            int i_t = (j * width * 2) + (i * 8);
            int i_y = (j * width) + i;
            int i_u = (j * width / 4) + (i / 2) + (width * height);
            int i_v = (j * width / 4) + (i / 2) + (5 * width * height / 4);

            /* luma */
            for (k=0; k<4; k++)
                for (l=0; l<4; l++)
                    out[i_y + (k * width) + l] = (unsigned char)(inp[i_t + (k * 4) + l] >> 0);

            /* chroma blue */
            for (k=0; k<2; k++)
                for (l=0; l<2; l++)
                    out[i_u + (k * width / 2) + l] = (unsigned char)(inp[i_t + (k * 4) + l + 16] >> 0);

            /* chroma red */
            for (k=0; k<2; k++)
                for (l=0; l<2; l++)
                    out[i_v + (k * width / 2) + l] = (unsigned char)(inp[i_t + (k * 4) + l + 24] >> 0);
        }
    }
}

/* convert three planes of yuv data from 4x4 packed format to planar */
void convyuvto4x4(unsigned char *inp, unsigned char *out, int width, int height, int pitch)
{
    int i,j,k,l;

    for (j=0; j<height; j+=4) {
        for (i=0; i<width; i+=4) {
            int i_t = (j * width * 2) + (i * 8);
            int i_y = (j * width) + i;
            int i_u = (j * width / 4) + (i / 2) + (width * height);
            int i_v = (j * width / 4) + (i / 2) + (5 * width * height / 4);

            /* luma */
            for (k=0; k<4; k++)
                for (l=0; l<4; l++)
                    out[i_t + (k * 4) + l] = inp[i_y + (k * width) + l];

            /* chroma blue */
            for (k=0; k<2; k++)
                for (l=0; l<2; l++)
                    out[i_t + (k * 4) + l + 16] = inp[i_u + (k * width / 2) + l];

            /* chroma red */
            for (k=0; k<2; k++)
                for (l=0; l<2; l++)
                    out[i_t + (k * 4) + l + 24] = inp[i_v + (k * width / 2) + l];
        }
    }
}

/* convert three planes of yuv data from macroblock format with planar chroma to planar */
void convmbplanartoyuv(unsigned char *inp, unsigned char *out, int width, int height, int pitch, int hsub, int vsub, int size)
{
    int i, j, k;
    unsigned char *o_y, *o_u, *o_v;

    /* start pointers of each output plane */
    unsigned char *p_y = out;
    unsigned char *p_u = p_y + pitch*height;
    unsigned char *p_v = p_u + pitch*height/hsub/vsub;

    /* loop over macroblocks */
    for (j=0; j<height/16; j++) {
        for (i=0; i<width/16; i++) {
            o_y = p_y + j*16*pitch + i*16;
            o_u = p_u + j*16/vsub*pitch/hsub + i*16/hsub;
            o_v = p_v + j*16/vsub*pitch/hsub + i*16/hsub;

            /* luma */
            for (k=0; k<16; k++) {
                memcpy(o_y, inp, 16*sizeof(unsigned char));
                o_y += pitch;
                inp += 16;
            }

            /* 4:2:0 chroma */
            if (vsub>=1) {
                /* chroma blue */
                for (k=0; k<8; k++) {
                    memcpy(o_u, inp, 16/hsub*sizeof(unsigned char));
                    o_u += pitch/hsub;
                    inp += 16/hsub;
                }

                /* chroma red */
                for (k=0; k<8; k++) {
                    memcpy(o_v, inp, 16/hsub*sizeof(unsigned char));
                    o_v += pitch/hsub;
                    inp += 16/hsub;
                }
            }

            /* 4:2:2 chroma */
            if (vsub==1) {
                /* chroma blue */
                for (k=0; k<8; k++) {
                    memcpy(o_u, inp, 16/hsub*sizeof(unsigned char));
                    o_u += pitch/hsub;
                    inp += 16/hsub;
                }

                /* chroma red */
                for (k=0; k<8; k++) {
                    memcpy(o_v, inp, 16/hsub*sizeof(unsigned char));
                    o_v += pitch/hsub;
                    inp += 16/hsub;
                }
            }

            inp += size - 256 - 2*256/hsub/vsub;
        }
    }
}

/* convert three planes of yuv data from planar to macroblock format with planar chroma */
void convyuvtombplanar(unsigned char *inp, unsigned char *out, int width, int height, int pitch, int hsub, int vsub, int size)
{
    int i, j, k;
    unsigned char *i_y, *i_u, *i_v;

    /* start pointers of each input plane */
    unsigned char *p_y = inp;
    unsigned char *p_u = p_y + pitch*height;
    unsigned char *p_v = p_u + pitch*height/hsub/vsub;

    /* loop over macroblocks */
    for (j=0; j<height/16; j++) {
        for (i=0; i<width/16; i++) {
            i_y = p_y + j*16*pitch + i*16;
            i_u = p_u + j*16/vsub*pitch/hsub + i*16/hsub;
            i_v = p_v + j*16/vsub*pitch/hsub + i*16/hsub;

            /* luma */
            for (k=0; k<16; k++) {
                memcpy(out, i_y, 16*sizeof(unsigned char));
                i_y += pitch;
                out += 16;
            }

            /* 4:2:0 chroma */
            if (vsub>=1) {
                /* chroma blue */
                for (k=0; k<8; k++) {
                    memcpy(out, i_u, 16/hsub*sizeof(unsigned char));
                    i_u += pitch/hsub;
                    out += 16/hsub;
                }

                /* chroma red */
                for (k=0; k<8; k++) {
                    memcpy(out, i_v, 16/hsub*sizeof(unsigned char));
                    i_v += pitch/hsub;
                    out += 16/hsub;
                }
            }

            /* 4:2:2 chroma */
            if (vsub==1) {
                /* chroma blue */
                for (k=0; k<8; k++) {
                    memcpy(out, i_u, 16/hsub*sizeof(unsigned char));
                    i_u += pitch/hsub;
                    out += 16/hsub;
                }

                /* chroma red */
                for (k=0; k<8; k++) {
                    memcpy(out, i_v, 16/hsub*sizeof(unsigned char));
                    i_v += pitch/hsub;
                    out += 16/hsub;
                }
            }

            out += size - 256 - 2*256/hsub/vsub;
        }
    }
}

/* convert three planes of yuv data from macroblock format with interleaved chroma to planar */
void convmbintertoyuv(unsigned char *inp, unsigned char *out, int width, int height, int pitch, int hsub, int vsub, int size)
{
    int i, j, k;
    unsigned char *o_y, *o_u, *o_v;

    /* start pointers of each output plane */
    unsigned char *p_y = out;
    unsigned char *p_u = p_y + pitch*height;
    unsigned char *p_v = p_u + pitch*height/hsub/vsub;

    /* loop over macroblocks */
    for (j=0; j<height/16; j++) {
        for (i=0; i<width/16; i++) {
            o_y = p_y + j*16*pitch + i*16;
            o_u = p_u + j*16/vsub*pitch/hsub + i*16/hsub;
            o_v = p_v + j*16/vsub*pitch/hsub + i*16/hsub;

            /* luma */
            for (k=0; k<16; k++) {
                memcpy(o_y, inp, 16*sizeof(unsigned char));
                o_y += pitch;
                inp += 16;
            }

            /* interleaved chroma */
            for (k=0; k<16/vsub; k++) {
                memcpy(o_u, inp, 16/hsub*sizeof(unsigned char));
                o_u += pitch/hsub;
                inp += 16/hsub;
                memcpy(o_v, inp, 16/hsub*sizeof(unsigned char));
                o_v += pitch/hsub;
                inp += 16/hsub;
            }

            inp += size - 256 - 2*256/hsub/vsub;
        }
    }
}

/* convert three planes of yuv data from planar to macroblock format with interleaved chroma */
void convyuvtombinter(unsigned char *inp, unsigned char *out, int width, int height, int pitch, int hsub, int vsub, int size)
{
    int i, j, k;
    unsigned char *i_y, *i_u, *i_v;

    /* start pointers of each input plane */
    unsigned char *p_y = inp;
    unsigned char *p_u = p_y + pitch*height;
    unsigned char *p_v = p_u + pitch*height/hsub/vsub;

    /* loop over macroblocks */
    for (j=0; j<height/16; j++) {
        for (i=0; i<width/16; i++) {
            i_y = p_y + j*16*pitch + i*16;
            i_u = p_u + j*16/vsub*pitch/hsub + i*16/hsub;
            i_v = p_v + j*16/vsub*pitch/hsub + i*16/hsub;

            /* luma */
            for (k=0; k<16; k++) {
                memcpy(out, i_y, 16*sizeof(unsigned char));
                i_y += pitch;
                out += 16;
            }

            /* interleaved chroma */
            for (k=0; k<16/vsub; k++) {
                memcpy(out, i_u, 16/hsub*sizeof(unsigned char));
                i_u += pitch/hsub;
                out += 16/hsub;
                memcpy(out, i_v, 16/hsub*sizeof(unsigned char));
                i_v += pitch/hsub;
                out += 16/hsub;
            }

            out += size - 256 - 2*256/hsub/vsub;
        }
    }
}

/* convert three planes of yuv data from macroblock format with interleaved chroma in capture order to planar */
void convmbcapturetoyuv(unsigned char *inp, unsigned char *out, int width, int height, int pitch, int hsub, int vsub, int size)
{
    int i, j, k, l;
    unsigned char *o_y, *o_u, *o_v;

    /* start pointers of each output plane */
    unsigned char *p_y = out;
    unsigned char *p_u = p_y + pitch*height;
    unsigned char *p_v = p_u + pitch*height/hsub/vsub;

    /* loop over macroblocks */
    for (j=0; j<height/16; j++) {
        for (i=0; i<width/16; i++) {
            o_y = p_y + j*16*pitch + i*16;
            o_u = p_u + j*16/vsub*pitch/hsub + i*16/hsub;
            o_v = p_v + j*16/vsub*pitch/hsub + i*16/hsub;

            /* luma */
            for (k=0; k<16; k++) {
                memcpy(o_y, inp, 16*sizeof(unsigned char));
                o_y += pitch;
                inp += 16;
            }

            /* interleaved chroma in capture order */
            for (k=0; k<16/vsub; k++) {
                static const int map[8]  = {2, 3, 6, 7, 0, 1, 4, 5};
                for (l=0; l<16/hsub; l++)
                    o_u[map[l]] = inp[l];
                o_u += pitch/hsub;
                inp += 16/hsub;
                for (l=0; l<16/hsub; l++)
                    o_v[map[l]] = inp[l];
                o_v += pitch/hsub;
                inp += 16/hsub;
            }

            inp += size - 256 - 2*256/hsub/vsub;
        }
    }
}

/* convert three planes of yuv data from planar to macroblock format with interleaved chroma in capture order */
void convyuvtombcapture(unsigned char *inp, unsigned char *out, int width, int height, int pitch, int hsub, int vsub, int size)
{
    int i, j, k, l;
    unsigned char *i_y, *i_u, *i_v;

    /* start pointers of each input plane */
    unsigned char *p_y = inp;
    unsigned char *p_u = p_y + pitch*height;
    unsigned char *p_v = p_u + pitch*height/hsub/vsub;

    /* loop over macroblocks */
    for (j=0; j<height/16; j++) {
        for (i=0; i<width/16; i++) {
            i_y = p_y + j*16*pitch + i*16;
            i_u = p_u + j*16/vsub*pitch/hsub + i*16/hsub;
            i_v = p_v + j*16/vsub*pitch/hsub + i*16/hsub;

            /* luma */
            for (k=0; k<16; k++) {
                memcpy(out, i_y, 16*sizeof(unsigned char));
                i_y += pitch;
                out += 16;
            }

            /* interleaved chroma */
            for (k=0; k<16/vsub; k++) {
                static const int map[8]  = {2, 3, 6, 7, 0, 1, 4, 5};
                for (l=0; l<16/hsub; l++)
                    out[l] = i_u[map[l]];
                i_u += pitch/hsub;
                out += 16/hsub;
                for (l=0; l<16/hsub; l++)
                    out[l] = i_v[map[l]];
                i_v += pitch/hsub;
                out += 16/hsub;
            }

            out += size - 256 - 2*256/hsub/vsub;
        }
    }
}

/* convert three planes of yuv data from macroblock format with 4x8 tiled chroma to planar */
void convmbtiledtoyuv(unsigned char *inp, unsigned char *out, int width, int height, int pitch, int hsub, int vsub)
{
    int i, j, k;
    unsigned char *o_y, *o_u, *o_v;

    /* start pointers of each output plane */
    unsigned char *p_y = out;
    unsigned char *p_u = p_y + pitch*height;
    unsigned char *p_v = p_u + pitch*height/hsub/vsub;

    /* loop over luma macroblocks */
    for (j=0; j<height/16; j++) {
        for (i=0; i<width/16; i++) {
            o_y = p_y + j*16*pitch + i*16;

            /* luma */
            for (k=0; k<16; k++) {
                memcpy(o_y, inp, 16*sizeof(unsigned char));
                o_y += pitch;
                inp += 16;
            }
        }
    }

    /* loop over chroma tiles */
    for (j=0; j<height/vsub; j+=8) {
        for (i=0; i<width/hsub; i+=4) {
            o_u = p_u + j*pitch/hsub + i;
            o_v = p_v + j*pitch/hsub + i;

            /* 4x8 top field tile */
            for (k=0; k<4; k++) {
                memcpy(o_u, inp, 4);
                o_u += 2*pitch/hsub;
                inp += 4;
                memcpy(o_v, inp, 4);
                o_v += 2*pitch/hsub;
                inp += 4;
            }
        }
        for (i=0; i<width/hsub; i+=4) {
            o_u = p_u + (j+1)*pitch/hsub + i;
            o_v = p_v + (j+1)*pitch/hsub + i;

            /* 4x8 bottom field tile */
            for (k=0; k<4; k++) {
                memcpy(o_u, inp, 4);
                o_u += 2*pitch/hsub;
                inp += 4;
                memcpy(o_v, inp, 4);
                o_v += 2*pitch/hsub;
                inp += 4;
            }
        }
    }
}

/* convert three planes of yuv data from planar to macroblock format with 4x8 tiled chroma */
void convyuvtombtiled(unsigned char *inp, unsigned char *out, int width, int height, int pitch, int hsub, int vsub, int size)
{
    int i, j, k, l;
    unsigned char *i_y, *i_u, *i_v;

    /* start pointers of each input plane */
    unsigned char *p_y = inp;
    unsigned char *p_u = p_y + pitch*height;
    unsigned char *p_v = p_u + pitch*height/hsub/vsub;

    /* loop over macroblocks */
    for (j=0; j<height/16; j++) {
        for (i=0; i<width/16; i++) {
            i_y = p_y + j*16*pitch + i*16;

            /* luma */
            for (k=0; k<16; k++) {
                memcpy(out, i_y, 16*sizeof(unsigned char));
                i_y += pitch;
                out += 16;
            }

            /* chroma */
            for (l=0; l<8; l+=4) {
                i_u = p_u + j*16/vsub*pitch/hsub + i*16/hsub + l;
                i_v = p_v + j*16/vsub*pitch/hsub + i*16/hsub + l;

                /* 4x8 top field tile */
                for (k=0; k<4; k++) {
                    memcpy(out, i_u, 4);
                    i_u += 2*pitch/hsub;
                    out += 4;
                    memcpy(out, i_v, 4);
                    i_v += 2*pitch/hsub;
                    out += 4;
                }

                i_u = p_u + (j+1)*16/vsub*pitch/hsub + i*16/hsub + l;
                i_v = p_v + (j+1)*16/vsub*pitch/hsub + i*16/hsub + l;

                /* 4x8 bottom field tile */
                for (k=0; k<4; k++) {
                    memcpy(out, i_u, 4);
                    i_u += 2*pitch/hsub;
                    out += 4;
                    memcpy(out, i_v, 4);
                    i_v += 2*pitch/hsub;
                    out += 4;
                }
            }

            out += size - 256 - 2*256/hsub/vsub;
        }
    }
}

/* convert one plane of yuv luma data from macroblock format to planar */
void convmblumatoyuv(unsigned char *inp, unsigned char *out, int width, int height, int pitch, int size)
{
    int i, j, k;
    unsigned char *o_y;

    /* start pointer of luma plane */
    unsigned char *p_y = out;

    /* loop over macroblocks */
    for (j=0; j<height/16; j++) {
        for (i=0; i<width/16; i++) {
            o_y = p_y + j*16*pitch + i*16;

            /* luma */
            for (k=0; k<16; k++) {
                memcpy(o_y, inp, 16*sizeof(unsigned char));
                o_y += pitch;
                inp += 16;
            }

            inp += size - 256;
        }
    }
}

int main(int argc, char *argv[])
{
    FILE *filein = stdin;
    FILE *fileout = stdout;
    char *filename[RV_MAXFILES] = {0};
    char *outfile = NULL;
    int fileindex = 0;
    int numfiles = 0;
    int totframes = 0;
    enum format_t format = PACKED4x4;

    /* data buffers */
    size_t inp_size, out_size;
    unsigned char* inp = NULL;
    unsigned char* out = NULL;

    /* command line defaults */
    const char *formatname = NULL;
    int reverse = 0;
    int width = 0;
    int height = 0;
    const char *fccode = NULL;
    unsigned int fourcc = I420;
    const char *formatsize = NULL;
    int numframes = -1;
    int yuv4mpeg = 0;
    int verbose = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"format",     1, NULL, 'f'},
            {"reverse",    1, NULL, 'r'},
            {"size",       1, NULL, 's'},
            {"output",     1, NULL, 'o'},
            {"yuv4mpeg",   0, NULL, '4'},
            {"quiet",      0, NULL, 'q'},
            {"verbose",    0, NULL, 'v'},
            {"usage",      0, NULL, 'h'},
            {"help",       0, NULL, 'h'},
            {NULL,         0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "f:rs:p:n:o:4qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 'f':
                formatname = optarg;
                break;

            case 'r':
                reverse = 1;
                break;

            case 's':
                formatsize = optarg;
                break;

            case 'p':
                fccode = optarg;
                break;

            case 'n':
                numframes = atoi(optarg);
                if (numframes<=0)
                    rvexit("invalid value for number of frames: %d", numframes);
                break;

            case '4':
                yuv4mpeg = 1;
                break;

            case 'o':
                outfile = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < RV_MAXFILES)
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    /* convert fourcc string into integer */
    if (fccode)
        fourcc = get_fourcc(fccode);

    /* get chroma subsampling from fourcc */
    int hsub, vsub;
    get_chromasub(fourcc, &hsub, &vsub);
    if (hsub==-1 || vsub==-1)
        rvexit("unknown pixel format: %s", fourccname(fourcc));

    /* determine format of tiled data */
    if (formatname) {
        if (strcasecmp(formatname, "8x4")==0 || strcasecmp(formatname, "planar8x4")==0)
            format = PLANAR8x4;
        else if (strcasecmp(formatname, "4x4")==0 || strcasecmp(formatname, "packed4x4")==0)
            format = PACKED4x4;
        else if (strcasecmp(formatname, "4x410")==0 || strcasecmp(formatname, "4x410bit")==0 || strcasecmp(formatname, "packed4x410")==0 || strcasecmp(formatname, "packed4x410bit")==0)
            format = PACKED4x410;
        else if (strcasecmp(formatname, "mbplanar")==0 || strcasecmp(formatname, "macroblock")==0 || strcasecmp(formatname, "macro")==0)
            format = MBPLANAR;
        else if (strcasecmp(formatname, "mbinterleaved")==0 || strcasecmp(formatname, "mbinter")==0 || strcasecmp(formatname, "interleaved")==0)
            format = MBINTER;
        else if (strcasecmp(formatname, "mbcompact")==0 || strcasecmp(formatname, "mbcomp")==0 || strcasecmp(formatname, "compact")==0)
            format = MBCOMPACT;
        else if (strcasecmp(formatname, "mbcapture")==0 || strcasecmp(formatname, "mbcap")==0 || strcasecmp(formatname, "capture")==0)
            format = MBCAPTURE;
        else if (strcasecmp(formatname, "mbtiled")==0 || strcasecmp(formatname, "macroblocktiled")==0 || strcasecmp(formatname, "mpeg2tiled")==0)
            format = MBTILED;
        else if (strcasecmp(formatname, "mbluma")==0 || strcasecmp(formatname, "macroblockluma")==0) {
            format = MBLUMA;
            hsub = vsub = 0;
        } else
            rvexit("unknown format: %s", formatname);
    }
    if (verbose>=1) rvmessage("hsub=%d vsub=%d", hsub, vsub);

    /* sanity check command line */

    /* get image dimensions */
    if (divine_image_dims(&width, &height, NULL, NULL, NULL, formatsize, filename[0])<0)
        rvexit("unable to determine input image dimensions");
    if (verbose>=1) rvmessage("width=%d height=%d", width, height);

     /* open output file */
    if (outfile)
        fileout = fopen(outfile, "wb");
    if (fileout==NULL)
        rverror("failed to open output file \"%s\"", outfile);

    /* ignore SIGPIPE on output file */
    if (signal(SIGPIPE, SIG_IGN)==SIG_ERR)
        rverror("failed to set ignore on SIGPIPE");

    /* write yuv4mpeg stream header */
    if (yuv4mpeg)
        if (write_y4m_stream_header(width, height, get_chromaformat(fourcc), fileout)<0)
            rverror("failed to write stream header to output");

    /* determine size of data buffers based on format conversion */
    if (format==PACKED4x4 || format==MBPLANAR || format==MBINTER || format==MBCAPTURE || format==MBTILED)
        inp_size = height*width*2;
    else if (format==PACKED4x410)
        inp_size = height*width*4;
    else if (format==MBLUMA)
        inp_size = height*width;
    else
        inp_size = height*width*3/2;
    out_size = width*height + ((hsub && vsub)? 2*width*height/hsub/vsub : 0);
    if (reverse)
        rvswap(size_t, inp_size, out_size);

    /* allocate data buffers */
    inp = (unsigned char*) rvalloc(NULL, inp_size, 0);
    out = (unsigned char*) rvalloc(NULL, out_size, 0);

    /* loop over input files */
    do {
        int frameno;

        if (numfiles)
            filein = fopen(filename[fileindex], "rb");
        if (filein==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex]);

        for (frameno=0; (frameno<numframes || numframes==-1) && !feof(filein) && !ferror(filein) && !ferror(fileout); frameno++)
        {
            size_t read = fread(inp, 1, inp_size, filein);
            if (read==inp_size)
            {
                if (!reverse) {
                    switch (format) {
                        case PLANAR8x4:
                            /* convert all three planes of data from 8x4 format */
                            conv8x4toyuv(inp,                  out,                  width,   height,   width);
                            conv8x4toyuv(inp+width*height,     out+width*height,     width/2, height/2, width/2);
                            conv8x4toyuv(inp+width*height*5/4, out+width*height*5/4, width/2, height/2, width/2);
                            break;

                        case PACKED4x4:
                            conv4x4toyuv(inp, out, width, height);
                            break;

                        case PACKED4x410:
                            conv4x410toyuv((unsigned short *)inp, out, width, height);
                            break;

                        case MBPLANAR:
                            convmbplanartoyuv(inp, out, width, height, width, hsub, vsub, 512);
                            break;

                        case MBINTER:
                            convmbintertoyuv(inp, out, width, height, width, hsub, vsub, 512);
                            break;

                        case MBCOMPACT:
                            convmbcapturetoyuv(inp, out, width, height, width, hsub, vsub, 384);
                            break;

                        case MBTILED:
                            convmbtiledtoyuv(inp, out, width, height, width, hsub, vsub);
                            break;

                        case MBCAPTURE:
                            convmbcapturetoyuv(inp, out, width, height, width, hsub, vsub, 512);
                            break;

                        case MBLUMA:
                            convmblumatoyuv(inp, out, width, height, width, 256);
                            break;

                        default:
                            rvexit("format not supported: %s", formatname);
                    }
                } else {
                    switch (format) {
                        case PACKED4x4:
                            convyuvto4x4(inp, out, width, height, height);
                            break;

                        case MBPLANAR:
                            convyuvtombplanar(inp, out, width, height, width, hsub, vsub, 512);
                            break;

                        case MBINTER:
                            convyuvtombinter(inp, out, width, height, width, hsub, vsub, 512);
                            break;

                        case MBTILED:
                            convyuvtombtiled(inp, out, width, height, width, hsub, vsub, 512);
                            break;

                        case MBCAPTURE:
                            convyuvtombcapture(inp, out, width, height, width, hsub, vsub, 512);
                            break;

                        default:
                            rvexit("format not supported in reverse: %s", formatname);
                    }
                }

                /* write yuv4mpeg frame header */
                if (yuv4mpeg)
                    if (write_y4m_frame_header(fileout)<0)
                        break;

                if (fwrite(out, out_size, 1, fileout) != 1)
                    break;
                else
                    totframes++;
            }
            else if (read!=0) {
                if (read<inp_size/2)
                    rvmessage("input is not a whole number of frames: %zd bytes extra", read);
                else
                    rvmessage("input is not a whole number of frames: %zd bytes remaining", inp_size-read);
            }
        }
        if (ferror(filein))
            rverror("failed to read from input");
        if (ferror(fileout) && errno!=EPIPE)
            rverror("failed to write to output");

        if (numfiles)
            fclose(filein);

    } while (++fileindex<numfiles && !ferror(fileout));

    if (verbose>=0)
        rvmessage("%s %d frames", reverse? "tiled" : "untiled", totframes);

    /* tidy up */
    rvfree(inp);
    rvfree(out);

    return 0;
}
