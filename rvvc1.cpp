/*
 * Description: Functions related to the VC-1 standard.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-06-23 13:26:19 $
 * Revision   : $Revision: 1.4 $
 * Copyright  : (c) 2006 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>

#include "rvutil.h"
#include "rvvc1.h"

static int parse_hrd_param(struct bitbuf *bb)
{
    int num_leaky_buckets = getbits(bb, 5);
    flushbits(bb, 4);               /* bit_rate_exponent */
    flushbits(bb, 4);               /* buffer_size_exponent */
    int n;
    for (n=1; n<=num_leaky_buckets; n++)
    {
        flushbits(bb, 16);          /* hrd_rate[n] */
        flushbits(bb, 16);          /* hrd_buffer[n] */
    }
    return 0;
}

int parse_sequence_layer(struct bitbuf *bb, int *profile, int *level, int *width, int *height)
{
    *profile = getbits(bb, 2);      /* profile */
    *level = getbits(bb, 3);        /* level */
    flushbits(bb, 2);               /* colordiff_format */
    flushbits(bb, 3);               /* frmrtq_postproc */
    flushbits(bb, 5);               /* bitrtq_postproc */
    flushbits(bb, 1);               /* postprocflag */
    *width = getbits(bb, 12);       /* max_coded_width */
    *width = (*width)*2 + 2;
    *height = getbits(bb, 12);      /* max_coded_height */
    *height = (*height)*2 + 2;
    flushbits(bb, 1);               /* pulldown */
    flushbits(bb, 1);               /* interlace */
    flushbits(bb, 1);               /* tfcntrflag */
    flushbits(bb, 1);               /* finterpflag */
    flushbits(bb, 1);               /* reserved */
    flushbits(bb, 1);               /* psf */
    flushbits(bb, 1);
    if (getbit(bb)) {               /* display_ext */
        flushbits(bb, 14);          /* disp_horiz_size */
        flushbits(bb, 14);          /* disp_vert_size */
        if (getbit(bb)) {           /* aspect_ratio_flag */
            int aspect_ratio = getbits(bb, 4);
            if (aspect_ratio == 15) {
                flushbits(bb, 8);   /* aspect_horiz_size */
                flushbits(bb, 8);   /* aspect_vert_size */
            }
        }
        if (getbit(bb)) {           /* framerate_flag */
            if (!getbit(bb)) {      /* framerateind */
                flushbits(bb, 8);   /* frameratenr */
                flushbits(bb, 4);   /* frameratedr */
            } else {
                flushbits(bb, 16);  /* framerateexp */
            }
        }
        if (getbit(bb)) {           /* color_format_flag */
            flushbits(bb, 8);       /* color_prim */
            flushbits(bb, 8);       /* transfer_char */
            flushbits(bb, 8);       /* matrix_coef */
        }
    }
    if (getbit(bb)) {               /* hrd_param_flag */
        return parse_hrd_param(bb);
    }
    return 0;
}
