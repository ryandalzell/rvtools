/*
 * based on tmndecode (H.263 decoder) (C) 1996  Telenor R&D, Norway
 *        Karl Olav Lillevold <Karl.Lillevold@nta.no>
 */

/*
 * based on mpeg2decode, (C) 1994, MPEG Software Simulation Group
 * and mpeg2play, (C) 1994 Stefan Eckart
 *                         <stefan@lis.e-technik.tu-muenchen.de>
 *
 */

#ifndef _RVBITS_H_
#define _RVBITS_H_

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

#include "rvcrc.h"

#define SE_CODE 31

#define B_GETBITS 0x1
#define B_PUTBITS 0x2

struct bitbuf {
  /* file input or output */
  int infile;               /* input file descriptor */
  FILE *outfile;            /* output file handle */
  const char *filename;     /* file name */
  /* buffer input */
  unsigned char *buffer;    /* memory buffer */
  unsigned char *membuf;    /* memory buffer pointer */
  unsigned int membytes;    /* size of memory buffer remaining */
  off_t numbytes;           /* size of memory buffer */
  /* read buffer */
  unsigned char rdbfr[2052];/* internal IO buffer */
  unsigned int bufbytes;    /* bytes in IO buffer */
  unsigned char *rdptr;     /* read pointer in IO buffer */
  int bufbits;              /* number of bits read into buffer */
  off_t bitbits;            /* number of bits read by user */
  off_t readbytes;          /* number of bytes read from input */
  int eof;
  /* write buffer */
  unsigned char outbfr;
  int outcnt;
  off_t bytecnt;
  /* copy flag */
  int copybits;
};

struct bitbuf *initbits_filename(const char *bitfile, int flags);
struct bitbuf *initbits_memory(unsigned char *buf, int bytes);
int openbits(struct bitbuf *bb, const char *bitfile, int flags);
off_t tellbits(struct bitbuf *bb);
int rewindbits(struct bitbuf *bb);
off_t seekbits(struct bitbuf *bb, off_t pos);
int eofbits(struct bitbuf *bb);
int errorbits(struct bitbuf *bb);
void truncbits(struct bitbuf *bb);
struct stat statbits(struct bitbuf *bb);
off_t sizebits(struct bitbuf *bb);
void freebits(struct bitbuf *bb);
void putbits(struct bitbuf *bb, int n, int val);
void llputbits(struct bitbuf *bb, int n, unsigned long long val);
unsigned int showbits(struct bitbuf *bb, unsigned int n);
unsigned int showbit(struct bitbuf *bb);
unsigned int showbits8(struct bitbuf *bb);
unsigned int showbits16(struct bitbuf *bb);
unsigned int showbits24(struct bitbuf *bb);
unsigned int showbits32(struct bitbuf *bb);
unsigned long long llshowbits(struct bitbuf *bb, unsigned int n);
unsigned int flushbits(struct bitbuf *bb, unsigned int n);
unsigned int flushbit(struct bitbuf *bb);
unsigned int unflushbits(struct bitbuf *bb, unsigned int n);
unsigned int getbits(struct bitbuf *bb, unsigned int n);
unsigned int getbits8(struct bitbuf *bb);
unsigned int getbits32(struct bitbuf *bb);
unsigned long long llgetbits(struct bitbuf *bb, unsigned int n);
unsigned int getbit(struct bitbuf *bb);
unsigned int getbyte(struct bitbuf *bb);
unsigned int getword(struct bitbuf *bb);
unsigned int getdword(struct bitbuf *bb);
unsigned long long getqword(struct bitbuf *bb);
int readbits(struct bitbuf *bb, void *data, int size);
unsigned int alignbits(struct bitbuf *bb);
off_t numbits(struct bitbuf *bb);
int numalignbits(struct bitbuf *bb);
int numbitsleft(struct bitbuf *bb);
unsigned int tracebits(struct bitbuf *bb);
int tracelen(struct bitbuf *bb);
unsigned int uexpbits(struct bitbuf *bb);
signed   int sexpbits(struct bitbuf *bb);
unsigned int texpbits(struct bitbuf *bb, int range);
crc32_t crc32bits(struct bitbuf *bb);
unsigned ebmlid(struct bitbuf *bb);
unsigned long long ebmlsize(struct bitbuf *bb);
unsigned long long ebmluinteger(struct bitbuf *bb);

#endif
