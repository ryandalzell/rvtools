
#ifndef _RVRECON_H_
#define _RVRECON_H_

#include "rvcodec.h"

void make_edge_image(unsigned char *src, unsigned char *dst, int width, int height, int edge);
void recon_frame(unsigned char *src, int sstride, unsigned char *dst, int dstride, int w, int h, int x, int y, struct mv_t mv);
void recon_field(unsigned char *src, int sstride, unsigned char *dst, int dstride, int w, int h, int x, int y, struct mv_t mv);
int recon_check(const char *filename, int picno, int macro, int width, int height, int w, int h, struct mv_t mv);

#endif
