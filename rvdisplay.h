
#ifndef _RVDISPLAY_H_
#define _RVDISPLAY_H_

#include <X11/X.h>

#ifdef SHMEM
#include <sys/ipc.h>
#include <sys/shm.h>
#include <X11/extensions/XShm.h>
#endif

#include "rvlogfile.h"
#include "rvstat.h"

/* overlay draw modes */
#define DRAWMODE_INT 0x1    /* display intra mode */
#define DRAWMODE_MVS 0x2    /* display motion vectors */
#define DRAWMODE_VEC 0x4    /* draw motion vector lines */
#define DRAWMODE_REF 0x8    /* display motion reference */
#define DRAWMODE_BAR 0x10   /* display status bar*/
#define DRAWMODE_DCT 0x20   /* display dct mode/transform split */
#define DRAWMODE_NUM 0x40   /* display number of bits */
#define DRAWMODE_COL 0x100  /* display macroblock type in colour */
#define DRAWMODE_DEC 0x8000 /* display decoded values */

struct rvwin {
    Display        *display;
    Window          window;
    GC              gc;
    XImage         *ximage;
    Pixmap          pixmap;
    const char     *fontname[2][2];
    XFontStruct    *font[2];
    unsigned int    width;
    unsigned int    height;
    char            winname[256];
#ifdef SHMEM
    int             shmem;
    XShmSegmentInfo shminfo;
#endif
};

/* rvdisplay.c */
void init_clip_table();
void free_clip_table();
void init_dither_tab();

int create_window(struct rvwin *win, Display *display, int width, int height, int verbose);
void resize_window(struct rvwin *win, int width, int height);
void resize_pixmap(struct rvwin *win, int width, int height);
XImage *create_ximage(struct rvwin *win, int width, int height);
void resize_ximage(struct rvwin *win, int width, int height);
void free_ximage(struct rvwin *win);

void dither_image(struct rvwin *win, unsigned char *data[3], fourcc_t fourcc, int bpp, int zoom);
void display_ximage(struct rvwin *win, int x, int y);
void display_grid(struct rvwin *win, int x, int y, int width, int height, int gauge, int line);
void display_mark(struct rvwin *win, int x, int y, int width, int zoom, int markx, int marky);
void display_pel(struct rvwin *win, int x, int y, int width, int height, int zoom, int xpel, int ypel, int chroma, int value);
void display_numbers(struct rvwin *win, int x, int y, int width, int height, int zoom, int grid, int field, int mbaff, int coord);
void display_info(struct rvwin *win, int x, int y, int framenum, int lastframe, int time, int gain, int threshold);
void display_name(struct rvwin *win, int x, int y, const char *name, int numimages);
void display_psnr(struct rvwin *win, int x, int y, struct rvstats stats);
void display_stat(struct rvwin *win, int x, int y, struct rvstats stats);
void display_prob(struct rvwin *win, int x, int y, double entropy, double mean);
void display_timecode(struct rvwin *win, int x, int y, int hours, int mins, int secs, int frames);
void display_m2v_params(struct rvwin *win, int x, int y, struct rvm2vpicture *picture);
void display_m2v_ratios(struct rvwin *win, int x, int y, struct rvm2vsequence *sequence, struct rvm2vpicture *picture);
void display_m2v_quantmat(struct rvwin *win, int x, int y, struct rvm2vpicture *picture);
void display_m2v_coeff(struct rvwin *win, int x, int y, struct rvm2vmacro *macro);
void display_m2v_macro(struct rvwin *win, int x, int y, struct rvm2vmacro *macro);
void display_m2v_overlay(struct rvwin *win, int x, int y, int width, int height, int zoom, int mode, struct rvm2vpicture *toppicture, struct rvm2vpicture *botpicture);
void display_264_params(struct rvwin *win, int x, int y, const struct rv264picture *pic);
void display_264_overlay(struct rvwin *win, int x, int y, int width, int height, int zoom, int mode, struct rv264macro macro[], int mbaff, int field_pic_flag, int bottom_field_flag);
void display_265_params(struct rvwin *win, int x, int y, const struct rvhevcpicture *pic);
void display_265_overlay(struct rvwin *win, int x, int y, int width, int height, int zoom, int drawmode, const struct rvhevcpicture *pic);
void free_display(struct rvwin *window);

#endif
