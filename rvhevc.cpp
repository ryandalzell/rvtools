/*
 * Description: Functions related to the HEVC standard.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2010-01-28 18:07:39 $
 * Revision   : $Revision: 1.29 $
 * Copyright  : (c) 2012 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>

#include "rvutil.h"
#include "rvhevc.h"
#include "rvh264.h"

#ifdef HAVE_LIBDE265
#include "libde265/libde265/de265.h"
#include "libde265/libde265/image.h"
#endif

/*
 * hevc lookups.
 */
extern char *nal_start[2];

/*
 * hevc slice namess.
 */

/*
 * hevc utility functions.
 */

struct rvhevcvideo *alloc_hevc_video()
{
    size_t size = sizeof(struct rvhevcvideo);
    struct rvhevcvideo *cvs = (struct rvhevcvideo *)rvalloc(NULL, size, 1);
    /* allocate array of pictures in sequence */
    cvs->size_pic_array = 1024*128; /* FIXME how big should this be? each 1024 is a 4k page */
    cvs->picture = (struct rvhevcpicture **)rvalloc(NULL, cvs->size_pic_array*sizeof(struct rvhevcpicture *), 1);
    return cvs;
}

struct rvhevcpicture *alloc_hevc_picture(const struct hevc_seq_parameter_set *sps)
{
    size_t size = sizeof(struct rvhevcpicture);
    struct rvhevcpicture *picture = (struct rvhevcpicture *)rvalloc(NULL, size, 1);
    picture->cu.init(sps->PicWidthInMinCbsY, sps->PicHeightInMinCbsY, sps->MinCbSizeY);
    picture->pu.init(sps->PicWidthInMinCbsY*4, sps->PicHeightInMinCbsY*4, sps->MinCbSizeY/4);
    picture->tu.init(sps->PicWidthInMinTbsY, sps->PicHeightInMinTbsY, sps->MinTbSizeY);
    return picture;
}

void free_hevc_picture(struct rvhevcpicture* picture)
{
    if (picture) {
        picture->cu.free();
        picture->pu.free();
        picture->tu.free();
        free(picture);
    }
}

void free_hevc_video(struct rvhevcvideo* cvs)
{
    if (cvs) {
        for (int i=0; i<sizeof(cvs->vps)/sizeof(struct hevc_vid_parameter_set *); i++)
            rvfree(cvs->vps[i]);
        for (int i=0; i<sizeof(cvs->sps)/sizeof(struct hevc_seq_parameter_set *); i++)
            rvfree(cvs->sps[i]);
        for (int i=0; i<sizeof(cvs->pps)/sizeof(struct hevc_pic_parameter_set *); i++)
            rvfree(cvs->pps[i]);
        for (int i=0; i<cvs->size_pic_array; i++) {
            if (cvs->picture[i])
                free_hevc_picture(cvs->picture[i]);
        }
        rvfree(cvs->picture);
        free(cvs);
    }
}

/*
 * hevc nal unit types.
 */

const char *hevc_nal_unit_type_code(int nal_unit_type)
{
    switch (nal_unit_type) {
        case 0 : return "TRAIL_N: Coded slice segment of a trailing picture"; break;
        case 1 : return "TRAIL_R: Coded slice segment of a trailing picture"; break;
        case 2 : return "TSA_N: Coded slice segment of a TSA picture"; break;
        case 3 : return "TSA_R: Coded slice segment of a TSA picture"; break;
        case 4 : return "STSA_N: Coded slice segment of a STSA picture"; break;
        case 5 : return "STSA_R: Coded slice segment of a STSA picture"; break;
        case 6 : return "RADL_N: Coded slice segment of a RADL picture"; break;
        case 7 : return "RADL_R: Coded slice segment of a RADL picture"; break;
        case 8 : return "RASL_N: Coded slice segment of a RASL picture"; break;
        case 9 : return "RASL_R: Coded slice segment of a RASL picture"; break;
        case 10: return "RSV_VCL_N10: Reserved"; break;
        case 11: return "RSV_VCL_R11: Reserved"; break;
        case 12: return "RSV_VCL_N12: Reserved"; break;
        case 13: return "RSV_VCL_R13: Reserved"; break;
        case 14: return "RSV_VCL_N14: Reserved"; break;
        case 15: return "RSV_VCL_R15: Reserved"; break;
        case 16: return "BLA_W_LP: Coded slice segment of a BLA picture"; break;
        case 17: return "BLA_W_RADL: Coded slice segment of a BLA picture"; break;
        case 18: return "BLA_N_LP: Coded slice segment of a BLA picture"; break;
        case 19: return "IDR_W_RADL: Coded slice segment of an IDR picture "; break;
        case 20: return "IDR_N_LP: Coded slice segment of an IDR picture "; break;
        case 21: return "CRA_NUT: Coded slice segment of a CRA picture"; break;
        case 22: return "RSV_IRAP_VCL22: Reserved"; break;
        case 23: return "RSV_IRAP_VCL23: Reserved"; break;
        case 24: return "RSV_VCL24: Reserved"; break;
        case 25: return "RSV_VCL25: Reserved"; break;
        case 26: return "RSV_VCL26: Reserved"; break;
        case 27: return "RSV_VCL27: Reserved"; break;
        case 28: return "RSV_VCL28: Reserved"; break;
        case 29: return "RSV_VCL29: Reserved"; break;
        case 30: return "RSV_VCL30: Reserved"; break;
        case 31: return "RSV_VCL31: Reserved"; break;
        case 32: return "VPS_NUT: Video parameter set"; break;
        case 33: return "SPS_NUT: Sequence parameter set"; break;
        case 34: return "PPS_NUT: Picture parameter set"; break;
        case 35: return "AUD_NUT: Access unit delimiter"; break;
        case 36: return "EOS_NUT: End of sequence"; break;
        case 37: return "EOB_NUT: End of bitstream"; break;
        case 38: return "FD_NUT: Filler data"; break;
        case 39: return "PREFIX_SEI_NUT: Supplemental enhancement information (prefix)"; break;
        case 40: return "SUFFIX_SEI_NUT: Supplemental enhancement information (suffix)"; break;
        case 41: return "RSV_NVCL41: Reserved"; break;
        case 42: return "RSV_NVCL42: Reserved"; break;
        case 43: return "RSV_NVCL43: Reserved"; break;
        case 44: return "RSV_NVCL44: Reserved"; break;
        case 45: return "RSV_NVCL45: Reserved"; break;
        case 46: return "RSV_NVCL46: Reserved"; break;
        case 47: return "RSV_NVCL47: Reserved"; break;
        case 48: return "UNSPEC48: Unspecified"; break;
        case 49: return "UNSPEC49: Unspecified"; break;
        case 50: return "UNSPEC50: Unspecified"; break;
        case 51: return "UNSPEC51: Unspecified"; break;
        case 52: return "UNSPEC52: Unspecified"; break;
        case 53: return "UNSPEC53: Unspecified"; break;
        case 54: return "UNSPEC54: Unspecified"; break;
        case 55: return "UNSPEC55: Unspecified"; break;
        case 56: return "UNSPEC56: Unspecified"; break;
        case 57: return "UNSPEC57: Unspecified"; break;
        case 58: return "UNSPEC58: Unspecified"; break;
        case 59: return "UNSPEC59: Unspecified"; break;
        case 60: return "UNSPEC60: Unspecified"; break;
        case 61: return "UNSPEC61: Unspecified"; break;
        case 62: return "UNSPEC62: Unspecified"; break;
        case 63: return "UNSPEC63: Unspecified"; break;
        default: return "UNKNOWN: Unknown NAL unit type"; break;
    }
}

/*
 * hevc parsing functions.
 */
int find_next_hevc_nal_unit(struct bitbuf *bb, int *zerobyte)
{
    if (zerobyte)
        *zerobyte = 0;
    alignbits(bb);
    /* look for a start code prefix */
    while (showbits24(bb)!=0x000001 && showbits32(bb)!=0x00000001) {
        /* move along by one byte */
        flushbits(bb, 8);               /* leading_zero_8bits */
        if (eofbits(bb))
            return -1;
    }
    if (showbits24(bb)!=0x000001) {
        flushbits(bb, 8);               /* zero_byte */
        if (zerobyte)
            *zerobyte = 1;
    }
    /* return nal_unit_type */
    return (showbits32(bb) & 0x7E) >> 1;
}

int parse_hevc_nal_unit_header(struct bitbuf *bb, int *nal_unit_type)
{
    if (getbit(bb)!=0) {                                                /* forbidden_zero_bit */
        rvmessage("hevc nal unit: forbidden_zero_bit: 1");
            return -1;
    }
    *nal_unit_type = getbits(bb, 6);                                    /* nal_unit_type */
    flushbits(bb, 6);                                                   /* nuh_reserved_zero_6bits */
    flushbits(bb, 3);                                                   /* nuh_temporal_id_plus1 */
    return 0;
}

void parse_hevc_profile_tier_level(struct bitbuf *bb, int MaxNumSubLayersMinus1) {
    int i, j;
    int sub_layer_profile_present_flag[32] = {0};
    int sub_layer_level_present_flag[32] = {0};
    flushbits(bb, 2);                                                   /* general_profile_space */
    flushbits(bb, 1);                                                   /* general_tier_flag */
    flushbits(bb, 5);                                                   /* general_profile_idc */
    for (i=0; i<32; i++)
        flushbits(bb, 1);                                               /* general_profile_compatibility_flag[i] */
    flushbits(bb, 1);                                                   /* general_progressive_source_flag */
    flushbits(bb, 1);                                                   /* general_interlaced_source_flag */
    flushbits(bb, 1);                                                   /* general_non_packed_constraint_flag */
    flushbits(bb, 1);                                                   /* general_frame_only_constraint_flag */
    flushbits(bb, 44);                                                  /* general_reserved_zero_44bits */
    flushbits(bb, 8);                                                   /* general_level_idc */
    for (i=0; i<MaxNumSubLayersMinus1; i++) {
        sub_layer_profile_present_flag[i] = getbit(bb);                 /* sub_layer_profile_present_flag[i] */
        sub_layer_level_present_flag[i] = getbit(bb);                   /* sub_layer_level_present_flag[i] */
    }
    if (MaxNumSubLayersMinus1>0)
        for (i=MaxNumSubLayersMinus1; i<8; i++)
            flushbits(bb, 2);                                           /* reserved_zero_2bits[i] */
    for (i=0; i<MaxNumSubLayersMinus1; i++) {
        if (sub_layer_profile_present_flag[i]) {
            flushbits(bb, 2);                                           /* sub_layer_profile_space[i]  */
            flushbits(bb, 1);                                           /* sub_layer_tier_flag */
            flushbits(bb, 5);                                           /* sub_layer_profile_idc[i] */
            for (j=0; j<32; j++)
                flushbits(bb, 1);                                       /* sub_layer_profile_compatibility_flag[i][j] */
            flushbits(bb, 1);                                           /* sub_layer_progressive_source_flag */
            flushbits(bb, 1);                                           /* sub_layer_interlaced_source_flag */
            flushbits(bb, 1);                                           /* sub_layer_non_packed_constraint_flag */
            flushbits(bb, 1);                                           /* sub_layer_frame_only_constraint_flag */
            flushbits(bb, 44);                                          /* sub_layer_reserved_zero_44bits[i] */
        }
        if (sub_layer_level_present_flag[i])
            flushbits(bb, 8);                                           /* sub_layer_level_idc[i] */
    }
}

void parse_hevc_op_point(struct bitbuf *bb, int opIdx)
{
    int i;
    int op_num_layer_id_values_minus1 = uexpbits(bb);                   /* op_num_layer_id_values_minus1[opIdx] */
    for (i=0; i<=op_num_layer_id_values_minus1; i++)
        flushbits(bb, 6);                                               /* op_layer_id[opIdx][i] */
}

static void parse_hevc_sub_layer_hrd_parameters(struct bitbuf *bb, int cpb_cnt_minus1, int sub_pic_hrd_params_present_flag)
{
    int i, CpbCnt = cpb_cnt_minus1;
    for (i=0; i<=CpbCnt; i++) {
        uexpbits(bb);                                                   /* bit_rate_value_minus1[i] */
        uexpbits(bb);                                                   /* cpb_size_value_minus1[i] */
        if (sub_pic_hrd_params_present_flag) {
            uexpbits(bb);                                               /* cpb_size_du_value_minus1[i] */
            uexpbits(bb);                                               /* bit_rate_du_value_minus1[i] */
        }
        flushbits(bb, 1);                                               /* cbr_flag[i] */
    }
}

void parse_hevc_hrd_params(struct bitbuf *bb, int commonInfPresentFlag, int MaxNumSubLayersMinus1, struct hevc_hrd_parameters *hrd)
{
    int i;
    hrd->nal_hrd_parameters_present_flag = 0;
    hrd->vcl_hrd_parameters_present_flag = 0;
    hrd->sub_pic_hrd_params_present_flag = 0;
    int cpb_cnt_minus1[MaxNumSubLayersMinus1];
    if (commonInfPresentFlag) {
        hrd->nal_hrd_parameters_present_flag = getbits(bb, 1);          /* nal_hrd_parameters_present_flag */
        hrd->vcl_hrd_parameters_present_flag = getbits(bb, 1);          /* vcl_hrd_parameters_present_flag */
        if (hrd->nal_hrd_parameters_present_flag || hrd->vcl_hrd_parameters_present_flag) {
            hrd->sub_pic_hrd_params_present_flag = getbit(bb);          /* sub_pic_hrd_params_present_flag */
            if (hrd->sub_pic_hrd_params_present_flag) {
                flushbits(bb, 8);                                       /* tick_divisor_minus2 */
                hrd->du_cpb_removal_delay_increment_length_minus1 = getbits(bb, 5); /* du_cpb_removal_delay_increment_length_minus1 */
                hrd->sub_pic_cpb_params_in_pic_timing_sei_flag = getbit(bb);    /* sub_pic_cpb_params_in_pic_timing_sei_flag */
                flushbits(bb, 5);                                       /* dpb_output_delay_du_length_minus1 */
            }
            flushbits(bb, 4);                                           /* bit_rate_scale */
            flushbits(bb, 4);                                           /* cpb_size_scale */
            if (hrd->sub_pic_hrd_params_present_flag)
                flushbits(bb, 4);                                       /* cpb_size_du_scale */
            flushbits(bb, 5);                                           /* initial_cpb_removal_delay_length_minus1 */
            hrd->au_cpb_removal_delay_length_minus1 = getbits(bb, 5);   /* au_cpb_removal_delay_length_minus1 */
            hrd->dpb_output_delay_length_minus1 = getbits(bb, 5);       /* dpb_output_delay_length_minus1 */
        }
    }
    for (i=0; i<=MaxNumSubLayersMinus1; i++) {
        int fixed_pic_rate_within_cvs_flag = 0;
        int low_delay_hrd_flag = 0;
        int fixed_pic_rate_general_flag = getbit(bb);                   /* fixed_pic_rate_general_flag[i] */
        if (!fixed_pic_rate_general_flag)
            fixed_pic_rate_within_cvs_flag = getbit(bb);                /* fixed_pic_rate_within_cvs_flag[i] */
        else
            fixed_pic_rate_within_cvs_flag = 1;
        if (fixed_pic_rate_within_cvs_flag)
            uexpbits(bb);                                               /* elemental_duration_in_tc_minus1[i] */
        else
            low_delay_hrd_flag = getbit(bb);                            /* low_delay_hrd_flag[i] */
        if (!low_delay_hrd_flag)
            cpb_cnt_minus1[i] = uexpbits(bb);                           /* cpb_cnt_minus1[i] */
        else
            cpb_cnt_minus1[i] = 0;
        if (cpb_cnt_minus1[i]>31) {
            rvmessage("hevc hrd params: cpb_cnt_minus1 must be in the range [0:31]: %d", cpb_cnt_minus1[i]);
            return;
        }
        if (hrd->nal_hrd_parameters_present_flag)
            parse_hevc_sub_layer_hrd_parameters(bb, cpb_cnt_minus1[i], hrd->sub_pic_hrd_params_present_flag);
        if (hrd->vcl_hrd_parameters_present_flag)
            parse_hevc_sub_layer_hrd_parameters(bb, cpb_cnt_minus1[i], hrd->sub_pic_hrd_params_present_flag);
    }
}

void parse_hevc_vui_params(struct bitbuf *bb, int sps_max_sub_layers_minus1, struct hevc_vui_parameters *vui) {
    int aspect_ratio_info_present_flag = getbit(bb);                    /* aspect_ratio_info_present_flag */
    if (aspect_ratio_info_present_flag) {
        int aspect_ratio_idc = getbits8(bb);                            /* aspect_ratio_idc */
        if (aspect_ratio_idc==255) { /* Extended_SAR */
            flushbits(bb, 16);                                          /* sar_width */
            flushbits(bb, 16);                                          /* sar_height */
        }
    }
    int overscan_info_present_flag = getbits(bb, 1);                    /* overscan_info_present_flag */
    if (overscan_info_present_flag)
        flushbits(bb, 1);                                               /* overscan_appropriate_flag */
    int video_signal_type_present_flag = getbits(bb, 1);                /* video_signal_type_present_flag */
    if (video_signal_type_present_flag) {
        flushbits(bb, 3);                                               /* video_format */
        flushbits(bb, 1);                                               /* video_full_range_flag */
        int colour_description_present_flag = getbits(bb, 1);           /* colour_description_present_flag */
        if (colour_description_present_flag) {
            flushbits(bb, 8);                                           /* colour_primaries */
            flushbits(bb, 8);                                           /* transfer_characteristics */
            flushbits(bb, 8);                                           /* matrix_coefficients */
        }
    }
    int chroma_loc_info_present_flag = getbits(bb, 1);                  /* chroma_loc_info_present_flag */
    if (chroma_loc_info_present_flag) {
        uexpbits(bb);                                                   /* chroma_sample_loc_type_top_field */
        uexpbits(bb);                                                   /* chroma_sample_loc_type_bottom_field */
    }
    flushbits(bb, 1);                                                   /* neutral_chroma_indication_flag */
    vui->field_seq_flag = getbit(bb);                                   /* field_seq_flag */
    vui->frame_field_info_present_flag = getbit(bb);                    /* frame_field_info_present_flag */
    int default_display_window_flag = getbit(bb);                       /* default_display_window_flag */
    if (default_display_window_flag) {
        uexpbits(bb);                                                   /* def_disp_win_left_offset */
        uexpbits(bb);                                                   /* def_disp_win_right_offset */
        uexpbits(bb);                                                   /* def_disp_win_top_offset */
        uexpbits(bb);                                                   /* def_disp_win_bottom_offset */
    }
    vui->timing_info_present_flag = getbit(bb);                         /* vui_timing_info_present_flag */
    if (vui->timing_info_present_flag) {
        vui->num_units_in_tick = getbits32(bb);                         /* vui_num_units_in_tick */
        vui->time_scale = getbits32(bb);                                /* vui_time_scale */
        int vui_poc_proportional_to_timing_flag = getbit(bb);           /* vui_poc_proportional_to_timing_flag */
        if (vui_poc_proportional_to_timing_flag)
            uexpbits(bb);                                               /* vui_num_ticks_poc_diff_one_minus1 */
        vui->hrd_parameters_present_flag = getbits(bb, 1);              /* hrd_parameters_present_flag */
        if (vui->hrd_parameters_present_flag)
            parse_hevc_hrd_params(bb, 1, sps_max_sub_layers_minus1, &vui->hrd);
    }
    int bitstream_restriction_flag = getbits(bb, 1);                    /* bitstream_restriction_flag */
    if (bitstream_restriction_flag) {
        flushbits(bb, 1);                                               /* tiles_fixed_structure_flag */
        flushbits(bb, 1);                                               /* motion_vectors_over_pic_boundaries_flag */
        flushbits(bb, 1);                                               /* restricted_ref_pic_lists_flag */
        uexpbits(bb);                                                   /* min_spatial_segmentation_idc */
        uexpbits(bb);                                                   /* max_bytes_per_pic_denom */
        uexpbits(bb);                                                   /* max_bits_per_min_cu_denom */
        uexpbits(bb);                                                   /* log2_max_mv_length_horizontal */
        uexpbits(bb);                                                   /* log2_max_mv_length_vertical */
    }
}

struct hevc_vid_parameter_set *parse_hevc_vid_param(struct bitbuf *bb)
{
    const char *where = "hevc video param. set";
    int i, j;

    /* allocate a video parameter set */
    struct hevc_vid_parameter_set *vps = (struct hevc_vid_parameter_set *)rvalloc(NULL, sizeof(struct hevc_vid_parameter_set), 1);
    vps->occupied = 1;

    /* parse video parameter set */
    vps->vid_parameter_set_id = getbits(bb, 4);                         /* vps_video_parameter_set_id */
    int three = getbits(bb, 2);                                         /* vps_reserved_three_2bits */
    if (three!=3)
        rvmessage("%s: vps_reserved_three_2bits: %d", where, three);
    flushbits(bb, 6);                                                   /* vps_max_layers_minus1 */
    vps->max_sub_layers_minus1 = getbits(bb, 3);                        /* vps_max_sub_layers_minus1 */
    flushbits(bb, 1);                                                   /* vps_temporal_id_nesting_flag */
    int ffff = getbits(bb, 16);                                         /* vps_reserved_0xffff_16bits */
    if (ffff!=0xffff)
        rvmessage("%s: vps_reserved_0xffff_16bits: %x", where, ffff);
    parse_hevc_profile_tier_level(bb, vps->max_sub_layers_minus1);
    int vps_sub_layer_ordering_info_present_flag = getbit(bb);          /* vps_sub_layer_ordering_info_present_flag */
    for (i=(vps_sub_layer_ordering_info_present_flag?0:vps->max_sub_layers_minus1); i<=vps->max_sub_layers_minus1; i++) {
        uexpbits(bb);                                                   /* vps_max_dec_pic_buffering[i] */
        uexpbits(bb);                                                   /* vps_max_num_reorder_pics[i] */
        uexpbits(bb);                                                   /* vps_max_latency_increase[i] */
    }
    int vps_max_layer_id = getbits(bb, 6);
    int vps_num_layer_sets_minus1 = uexpbits(bb);
    for (i=1; i<=vps_num_layer_sets_minus1; i++)
        for (j=0; j<=vps_max_layer_id; j++)
            flushbits(bb, 1);                                           /* layer_id_included_flag[i][j] */
    vps->timing_info_present_flag = getbit(bb);                         /* vps_timing_info_present_flag */
    if (vps->timing_info_present_flag) {
        vps->num_units_in_tick = getbits32(bb);                         /* vps_num_units_in_tick */
        vps->time_scale = getbits32(bb);                                /* vps_time_scale */
        int vps_poc_proportional_to_timing_flag = getbit(bb);           /* vps_poc_proportional_to_timing_flag */
        if (vps_poc_proportional_to_timing_flag)
            uexpbits(bb);                                               /* vps_num_ticks_poc_diff_one_minus1 */
        int vps_num_hrd_parameters = uexpbits(bb);                      /* vps_num_hrd_parameters */
        for (i=0; i<vps_num_hrd_parameters; i++) {
            int cprms_present_flag = 1;
            uexpbits(bb);                                               /* hrd_layer_set_idx[i] */
            if (i>0)
                cprms_present_flag = getbit(bb);                        /* cprms_present_flag */
            parse_hevc_hrd_params(bb, cprms_present_flag, vps->max_sub_layers_minus1, &vps->hrd);
        }
    }
    int vps_extension_flag = getbit(bb);                                /* vps_extension_flag */
    if (vps_extension_flag)
        while (more_rbsp_data(bb))
            flushbits(bb, 1);                                           /* vps_extension_data_flag */
    parse_rbsp_trailing_bits(bb, where);

    /* calculate derived parameters */

    return vps;
}

static void parse_hevc_scaling_list_for_matrix(struct bitbuf *bb, int sizeID,int matrixID)
{
    int i;
    int nextCoef = 8;
    int coefNum = mmin(64, (1 << (4 + (sizeID << 1))));
    if (sizeID > 1) {
        int scaling_list_dc_coef_minus8 = sexpbits(bb);                 /* scaling_list_dc_coef_minus8[ sizeID - 2 ][ matrixID ] */
        nextCoef = scaling_list_dc_coef_minus8 + 8;
    }
    for (i=0; i<coefNum; i++) {
        int scaling_list_delta_coef = sexpbits(bb);                     /* scaling_list_delta_coef */
        nextCoef = ( nextCoef + scaling_list_delta_coef + 256 ) % 256;
        //ScalingList[ sizeID ][ matrixID ][i] = nextCoef;
    }
}

static void parse_hevc_scaling_list_data(struct bitbuf *bb)
{
    int sizeID, matrixID;
    for (sizeID=0; sizeID<4; sizeID++)
        for (matrixID=0; matrixID<(sizeID==3? 2 : 6); matrixID++) {
            int scaling_list_pred_mode_flag = getbits(bb, 1);           /* scaling_list_pred_mode_flag */
            if (!scaling_list_pred_mode_flag)
                uexpbits(bb);                                           /* scaling_list_pred_matrix_id_delta */
            else
                parse_hevc_scaling_list_for_matrix(bb, sizeID, matrixID);
        }
}

static struct hevc_short_term_ref_pic_set *parse_short_term_ref_pic_set(struct bitbuf *bb, struct hevc_short_term_ref_pic_set ref[], int stRpsIdx, int num_short_term_ref_pic_sets)
{
    const char *where = "hevc short-term reference picture set";
    int i, inter_ref_pic_set_prediction_flag = 0;
    if (stRpsIdx!=0)
        inter_ref_pic_set_prediction_flag = getbit(bb);
    if (inter_ref_pic_set_prediction_flag) {
        int delta_idx_minus1 = 0;
        if (stRpsIdx==num_short_term_ref_pic_sets) {
            delta_idx_minus1 = uexpbits(bb);                            /* delta_idx_minus1 */
            if (delta_idx_minus1>stRpsIdx-1) {
                rvmessage("%s: delta_idx_minus1 should be smaller than idx-1=%d-1: %d", where, stRpsIdx, delta_idx_minus1);
                return NULL;
            }
        }
        int RIdx = stRpsIdx - (delta_idx_minus1+1);
        int delta_rps_sign = getbit(bb);                                /* delta_rps_sign */
        int abs_delta_rps_minus1 = uexpbits(bb);                        /* abs_delta_rps_minus1 */
        int DeltaRPS = (1-2*delta_rps_sign) * (abs_delta_rps_minus1+1);
        int NumDeltaPocs = ref[RIdx].num_negative_pics + ref[RIdx].num_positive_pics;
        int used_by_curr_pic_flag[24+24];
        int use_delta_flag[24+24];
        int j;
        for (j=0; j<=NumDeltaPocs; j++) {
            used_by_curr_pic_flag[j] = getbit(bb);                      /* used_by_curr_pic_flag[j] */
            if (!used_by_curr_pic_flag[j])
                use_delta_flag[j] = getbit(bb);                         /* use_delta_flag[j] */
            else
                use_delta_flag[j] = 1;
        }
        for (i=0, j=ref[RIdx].num_positive_pics-1; j>=0; j--) {
            int dPoc = ref[RIdx].DeltaPocS1[j] + DeltaRPS;
            if (dPoc < 0 && use_delta_flag[ref[RIdx].num_negative_pics+j] ) {
                ref[stRpsIdx].DeltaPocS0[i] = dPoc;
                ref[stRpsIdx].used_by_curr_pic_s0_flag[i++] = used_by_curr_pic_flag[ref[RIdx].num_negative_pics+j];
            }
        }
        if( DeltaRPS < 0 && use_delta_flag[NumDeltaPocs] ) {
            ref[stRpsIdx].DeltaPocS0[i] = DeltaRPS;
            ref[stRpsIdx].used_by_curr_pic_s0_flag[i++] = used_by_curr_pic_flag[NumDeltaPocs];
        }
        for (j=0; j<ref[RIdx].num_negative_pics; j++) {
            int dPoc = ref[RIdx].DeltaPocS0[j] + DeltaRPS;
            if (dPoc<0 && use_delta_flag[j]) {
                ref[stRpsIdx].DeltaPocS0[i] = dPoc;
                ref[stRpsIdx].used_by_curr_pic_s0_flag[i++] = used_by_curr_pic_flag[j];
            }
        }
        ref[stRpsIdx].num_negative_pics = i;
        for (i=0, j=ref[RIdx].num_negative_pics-1; j>=0; j--) {
            int dPoc = ref[RIdx].DeltaPocS0[j] + DeltaRPS;
            if( dPoc > 0 && use_delta_flag[j] ) {
                ref[stRpsIdx].DeltaPocS1[i] = dPoc;
                ref[stRpsIdx].used_by_curr_pic_s1_flag[i++] = used_by_curr_pic_flag[j];
            }
        }
        if( DeltaRPS > 0 && use_delta_flag[NumDeltaPocs] ) {
            ref[stRpsIdx].DeltaPocS1[i] = DeltaRPS;
            ref[stRpsIdx].used_by_curr_pic_s1_flag[i++] = used_by_curr_pic_flag[NumDeltaPocs];
        }
        for (j=0; j<ref[RIdx].num_positive_pics; j++) {
            int dPoc = ref[RIdx].DeltaPocS1[j] + DeltaRPS;
            if (dPoc > 0 && use_delta_flag[ref[RIdx].num_negative_pics+j] ) {
                ref[stRpsIdx].DeltaPocS1[i] = dPoc;
                ref[stRpsIdx].used_by_curr_pic_s1_flag[i++] = used_by_curr_pic_flag[ref[RIdx].num_negative_pics+j];
            }
        }
        ref[stRpsIdx].num_positive_pics = i;

    } else {
        ref[stRpsIdx].num_negative_pics = uexpbits(bb);                 /* num_negative_pics */
        ref[stRpsIdx].num_positive_pics = uexpbits(bb);                 /* num_positive_pics */
        for (i=0; i<ref[stRpsIdx].num_negative_pics; i++) {
            int delta_poc_s0_minus1 = uexpbits(bb);                     /* delta_poc_s0_minus1[i] */
            ref[stRpsIdx].used_by_curr_pic_s0_flag[i] = getbit(bb);     /* used_by_curr_pic_s0_flag[i] */
            ref[stRpsIdx].DeltaPocS0[i] = i==0? -(delta_poc_s0_minus1+1) : ref[stRpsIdx].DeltaPocS0[i-1] - (delta_poc_s0_minus1+1);
        }
        for (i=0; i<ref[stRpsIdx].num_positive_pics; i++) {
            int delta_poc_s1_minus1 = uexpbits(bb);                     /* delta_poc_s1_minus1[i] */
            ref[stRpsIdx].used_by_curr_pic_s1_flag[i] = getbit(bb);     /* used_by_curr_pic_s1_flag[i] */
            ref[stRpsIdx].DeltaPocS1[i] = i==0? delta_poc_s1_minus1 + 1 : ref[stRpsIdx].DeltaPocS1[i-1] + (delta_poc_s1_minus1+1);
        }
    }
    return &ref[stRpsIdx];
}

struct hevc_seq_parameter_set *parse_hevc_seq_param(struct bitbuf *bb)
{
    const char *where = "hevc sequence param. set";
    int i;

    /* allocate a sequence parameter set */
    struct hevc_seq_parameter_set *sps = (struct hevc_seq_parameter_set *)rvalloc(NULL, sizeof(struct hevc_seq_parameter_set), 1);
    sps->occupied = 1;

    /* parse sequence parameter set */
    sps->vid_parameter_set_id = getbits(bb, 4);                         /* video_parameter_set_id */
    sps->sps_max_sub_layers_minus1 = getbits(bb, 3);                    /* sps_max_sub_layers_minus1 */
    if (sps->sps_max_sub_layers_minus1>6) {
        rvmessage("%s: sps_max_sub_layers_minus1 is greater than 6: %d", where, sps->sps_max_sub_layers_minus1);
        rvfree(sps);
        return NULL;
    }
    flushbits(bb, 1);                                                   /* sps_temporal_id_nesting_flag */
    parse_hevc_profile_tier_level(bb, sps->sps_max_sub_layers_minus1);
    sps->seq_parameter_set_id = uexpbits(bb);                           /* seq_parameter_set_id */
    if (sps->seq_parameter_set_id>31) {
        rvmessage("%s: seq_parameter_set_id is greater than 31: %d", where, sps->seq_parameter_set_id);
        rvfree(sps);
        return NULL;
    }
    sps->chroma_format_idc = uexpbits(bb);                              /* chroma_format_idc */
    if (sps->chroma_format_idc>3) {
        rvmessage("%s: chroma_format_idc is greater than 3: %d", where, sps->chroma_format_idc);
        rvfree(sps);
        return NULL;
    }
    if (sps->chroma_format_idc==3)
        sps->separate_colour_plane_flag = getbit(bb);                   /* separate_colour_plane_flag */
    sps->pic_width_in_luma_samples = uexpbits(bb);                      /* pic_width_in_luma_samples */
    sps->pic_height_in_luma_samples = uexpbits(bb);                     /* pic_height_in_luma_samples */
    int conformance_window_flag = getbits(bb, 1);                       /* conformance_window_flag */
    if (conformance_window_flag) {
        uexpbits(bb);                                                   /* conf_win_left_offset */
        uexpbits(bb);                                                   /* conf_win_right_offset */
        uexpbits(bb);                                                   /* conf_win_top_offset */
        uexpbits(bb);                                                   /* conf_win_bottom_offset */
    }
    sps->bit_depth_luma_minus8 = uexpbits(bb);                          /* bit_depth_luma_minus8 */
    sps->bit_depth_chroma_minus8 = uexpbits(bb);                        /* bit_depth_chroma_minus8 */
    sps->log2_max_pic_order_cnt_lsb_minus4 = uexpbits(bb);              /* log2_max_pic_order_cnt_lsb_minus4 */
    int sps_sub_layer_ordering_info_present_flag = getbit(bb);          /* sps_sub_layer_ordering_info_present_flag */
    for (i=(sps_sub_layer_ordering_info_present_flag?0:sps->sps_max_sub_layers_minus1); i<=sps->sps_max_sub_layers_minus1; i++) {
        uexpbits(bb);                                                   /* sps_max_dec_pic_buffering_minus1[i] */
        uexpbits(bb);                                                   /* sps_max_num_reorder_pics[i] */
        uexpbits(bb);                                                   /* sps_max_latency_increase_plus1[i] */
    }
    sps->log2_min_luma_coding_block_size_minus3 = uexpbits(bb);         /* log2_min_luma_coding_block_size_minus3 */
    sps->log2_diff_max_min_luma_coding_block_size = uexpbits(bb);       /* log2_diff_max_min_luma_coding_block_size */
    sps->log2_min_transform_block_size_minus2 = uexpbits(bb);           /* log2_min_transform_block_size_minus2 */
    sps->log2_diff_max_min_transform_block_size = uexpbits(bb);         /* log2_diff_max_min_transform_block_size */
    sps->max_transform_hierarchy_depth_inter = uexpbits(bb);            /* max_transform_hierarchy_depth_inter */
    sps->max_transform_hierarchy_depth_intra = uexpbits(bb);            /* max_transform_hierarchy_depth_intra */
    sps->scaling_list_enable_flag = getbit(bb);                         /* scaling_list_enable_flag */
    if (sps->scaling_list_enable_flag) {
        int sps_scaling_list_data_present_flag = getbits(bb, 1);        /* sps_scaling_list_data_present_flag */
        if (sps_scaling_list_data_present_flag)
            parse_hevc_scaling_list_data(bb);
    }
    sps->amp_enabled_flag = getbit(bb);                                 /* amp_enabled_flag */
    sps->sample_adaptive_offset_enabled_flag = getbit(bb);              /* sample_adaptive_offset_enabled_flag */
    sps->pcm_enabled_flag = getbit(bb);                                 /* pcm_enabled_flag */
    if (sps->pcm_enabled_flag) {
        flushbits(bb, 4);                                               /* pcm_sample_bit_depth_luma_minus1 */
        flushbits(bb, 4);                                               /* pcm_sample_bit_depth_chroma_minus1 */
        uexpbits(bb);                                                   /* log2_min_pcm_luma_coding_block_size_minus3 */
        uexpbits(bb);                                                   /* log2_diff_max_min_pcm_luma_coding_block_size */
        flushbits(bb, 1);                                               /* pcm_loop_filter_disable_flag */
    }
    int num_short_term_ref_pic_sets = uexpbits(bb);                     /* num_short_term_ref_pic_sets */
    for (i=0; i<num_short_term_ref_pic_sets; i++)
        parse_short_term_ref_pic_set(bb, sps->ref, i, num_short_term_ref_pic_sets);
    int long_term_ref_pics_present_flag = getbits(bb, 1);               /* long_term_ref_pics_present_flag */
    if (long_term_ref_pics_present_flag) {
        int num_long_term_ref_pics_sps = uexpbits(bb);                  /* num_long_term_ref_pics_sps */
        for (i=0; i<num_long_term_ref_pics_sps; i++) {
            flushbits(bb, sps->log2_max_pic_order_cnt_lsb_minus4+4);    /* lt_ref_pic_poc_lsb_sps[i] */
            flushbits(bb, 1);                                           /* used_by_curr_pic_lt_sps_flag[i] */
        }
    }
    flushbits(bb, 1);                                                   /* sps_temporal_mvp_enable_flag */
    flushbits(bb, 1);                                                   /* strong_intra_smoothing_enabled_flag */
    sps->vui_parameters_present_flag = getbits(bb, 1);                  /* vui_parameters_present_flag */
    if (sps->vui_parameters_present_flag)
        parse_hevc_vui_params(bb, sps->sps_max_sub_layers_minus1, &sps->vui);
    int sps_extension_flag = flushbits(bb, 1);                          /* sps_extension_flag */
    if (sps_extension_flag)
        while (more_rbsp_data(bb))
            flushbits(bb, 1);                                           /* sps_extension_data_flag */
    parse_rbsp_trailing_bits(bb, where);

    /* derived parameters */
    sps->MaxPicOrderCntLsb = 1 << (sps->log2_max_pic_order_cnt_lsb_minus4+4);
    sps->SubWidthC = sps->chroma_format_idc==1 || sps->chroma_format_idc==2? 2 : 1;
    sps->SubHeightC = sps->chroma_format_idc==1? 2 : 1;
    sps->MinCbLog2SizeY = sps->log2_min_luma_coding_block_size_minus3 + 3;
    sps->CtbLog2SizeY = sps->MinCbLog2SizeY + sps->log2_diff_max_min_luma_coding_block_size;
    sps->MinCbSizeY = 1 << sps->MinCbLog2SizeY;
    sps->CtbSizeY = 1 << sps->CtbLog2SizeY;
    sps->PicWidthInMinCbsY = sps->pic_width_in_luma_samples / sps->MinCbSizeY;
    sps->PicWidthInCtbsY = (sps->pic_width_in_luma_samples + sps->CtbSizeY - 1) / sps->CtbSizeY;
    sps->PicHeightInMinCbsY = sps->pic_height_in_luma_samples / sps->MinCbSizeY;
    sps->PicHeightInCtbsY = (sps->pic_height_in_luma_samples + sps->CtbSizeY - 1) / sps->CtbSizeY;
    sps->PicSizeInMinCbsY = sps->PicWidthInMinCbsY * sps->PicHeightInMinCbsY;
    sps->PicSizeInCtbsY = sps->PicWidthInCtbsY * sps->PicHeightInCtbsY;
    sps->PicSizeInSamplesY = sps->pic_width_in_luma_samples * sps->pic_height_in_luma_samples;
    sps->PicWidthInSamplesC = sps->pic_width_in_luma_samples / sps->SubWidthC;
    sps->PicHeightInSamplesC = sps->pic_height_in_luma_samples / sps->SubHeightC;
    if (sps->chroma_format_idc==0 || sps->separate_colour_plane_flag) {
        sps->CtbWidthC  = 0;
        sps->CtbHeightC = 0;
    } else {
        sps->CtbWidthC  = sps->CtbSizeY / sps->SubWidthC;
        sps->CtbHeightC = sps->CtbSizeY / sps->SubHeightC;
    }

    /* extra parameters */
    sps->MinTbSizeY = 1 << (sps->log2_min_transform_block_size_minus2+2);
    sps->PicWidthInMinTbsY  = sps->PicWidthInCtbsY  << (sps->CtbLog2SizeY - (sps->log2_min_transform_block_size_minus2+2));
    sps->PicHeightInMinTbsY = sps->PicHeightInCtbsY << (sps->CtbLog2SizeY - (sps->log2_min_transform_block_size_minus2+2));
    sps->PicSizeInMinTbsY = sps->PicWidthInMinTbsY * sps->PicHeightInMinTbsY;

    return sps;
}

struct hevc_pic_parameter_set *parse_hevc_pic_param(struct bitbuf *bb, struct hevc_seq_parameter_set **spss)
{
    const char *where = "hevc picture param. set";
    int i;

    /* allocate a picture parameter set */
    struct hevc_pic_parameter_set *pps = (struct hevc_pic_parameter_set *)rvalloc(NULL, sizeof(struct hevc_pic_parameter_set), 1);
    pps->occupied = 1;

    /* parse picture parameter set */
    pps->pic_parameter_set_id = uexpbits(bb);                           /* pic_parameter_set_id */
    if (pps->pic_parameter_set_id>255) {
        rvmessage("%s: pic_parameter_set_id is greater than 31: %d", where, pps->pic_parameter_set_id);
        rvfree(pps);
        return NULL;
    }
    pps->seq_parameter_set_id = uexpbits(bb);                           /* seq_parameter_set_id */
    if (pps->seq_parameter_set_id>31) {
        rvmessage("%s: seq_parameter_set_id is greater than 31: %d", where, pps->seq_parameter_set_id);
        rvfree(pps);
        return NULL;
    }
    pps->sps = spss[pps->seq_parameter_set_id]; // NOTE the sps can be unoccupied at this stage.
    pps->dependent_slice_segments_enabled_flag = getbit(bb);            /* dependent_slice_segments_enabled_flag */
    pps->output_flag_present_flag = getbit(bb);                         /* output_flag_present_flag */
    pps->num_extra_slice_header_bits = getbits(bb, 3);                  /* num_extra_slice_header_bits */
    getbit(bb);                                                         /* sign_data_hiding_enabled_flag */
    getbit(bb);                                                         /* cabac_init_present_flag */
    uexpbits(bb);                                                       /* num_ref_idx_l0_default_active_minus1 */
    uexpbits(bb);                                                       /* num_ref_idx_l1_default_active_minus1 */
    sexpbits(bb);                                                       /* init_qp_minus26 */
    getbit(bb);                                                         /* constrained_intra_pred_flag */
    getbit(bb);                                                         /* transform_skip_enabled_flag */
    int cu_qp_delta_enabled_flag= getbit(bb);                           /* cu_qp_delta_enabled_flag */
    if (cu_qp_delta_enabled_flag)
        uexpbits(bb);                                                   /* diff_cu_qp_delta_depth */
    sexpbits(bb);                                                       /* pps_cb_qp_offset */
    sexpbits(bb);                                                       /* pps_cr_qp_offset */
    getbit(bb);                                                         /* pps_slice_chroma_qp_offsets_present_flag */
    getbit(bb);                                                         /* weighted_pred_flag */
    getbit(bb);                                                         /* weighted_bipred_flag */
    getbit(bb);                                                         /* transquant_bypass_enabled_flag */
    int tiles_enabled_flag = getbit(bb);                                /* tiles_enabled_flag */
    getbit(bb);                                                         /* entropy_coding_sync_enabled_flag */
    if (tiles_enabled_flag) {
        int num_tile_columns_minus1 = uexpbits(bb);                     /* num_tile_columns_minus1 */
        int num_tile_rows_minus1 = uexpbits(bb);                        /* num_tile_rows_minus1 */
        int uniform_spacing_flag = getbit(bb);                          /* uniform_spacing_flag */
        if (!uniform_spacing_flag) {
            for (i=0; i<num_tile_columns_minus1; i++)
                uexpbits(bb);                                           /* column_width_minus1[ i ] */
            for (i=0; i<num_tile_rows_minus1; i++)
                uexpbits(bb);                                           /* row_height_minus1[ i ] */
        }
        getbit(bb);                                                     /* loop_filter_across_tiles_enabled_flag */
    }
    getbit(bb);                                                         /* pps_loop_filter_across_slices_enabled_flag */
    int deblocking_filter_control_present_flag = getbit(bb);            /* deblocking_filter_control_present_flag */
    if (deblocking_filter_control_present_flag) {
        getbit(bb);                                                     /* deblocking_filter_override_enabled_flag */
        int pps_deblocking_filter_disabled_flag = getbit(bb);           /* pps_deblocking_filter_disabled_flag */
        if (!pps_deblocking_filter_disabled_flag) {
           sexpbits(bb);                                                /* pps_beta_offset_div2 */
           sexpbits(bb);                                                /* pps_tc_offset_div2 */
       }
    }
    int pps_scaling_list_data_present_flag = getbit(bb);                /* pps_scaling_list_data_present_flag */
    if (pps_scaling_list_data_present_flag)
        parse_hevc_scaling_list_data(bb);                               /* scaling_list_data( ) */
    getbit(bb);                                                         /* lists_modification_present_flag */
    uexpbits(bb);                                                       /* log2_parallel_merge_level_minus2 */
    getbit(bb);                                                         /* slice_segment_header_extension_present_flag */
    int pps_extension_flag = getbit(bb);                                /* pps_extension_flag */
    if (pps_extension_flag)
        while (more_rbsp_data(bb))
            getbit(bb);                                                 /* pps_extension_data_flag */
    parse_rbsp_trailing_bits(bb, where);

    return pps;
}

struct hevc_slice_header *parse_hevc_slice_header(struct bitbuf *bb, int nal_unit_type, struct hevc_pic_parameter_set **ppss, struct hevc_slice_header_state *prev)
{
    const char *where = "hevc slice header";

    /* allocate a slice header */
    struct hevc_slice_header *sh = (struct hevc_slice_header *)rvalloc(NULL, sizeof(struct hevc_slice_header), 1);

    /* parse slice header */
    sh->first_slice_segment_in_pic_flag = getbits(bb, 1);               /* first_slice_segment_in_pic_flag */
    if (nal_unit_type >= BLA_W_LP && nal_unit_type <= RSV_IRAP_VCL23)
        flushbits(bb, 1);                                               /* no_output_of_prior_pics_flag */
    sh->pic_parameter_set_id = uexpbits(bb);                            /* pic_parameter_set_id */
    if (sh->pic_parameter_set_id>255) {
        rvmessage("%s: pic_parameter_set_id is greater than 255: %d", where, sh->pic_parameter_set_id);
        rvfree(sh);
        return NULL;
    }
    sh->pps = ppss[sh->pic_parameter_set_id];
    if (sh->pps==NULL || !sh->pps->occupied) {
        if (sh->pps)
            rvmessage("sh->pps->occupied_warning=%d", sh->pps->occupied_warning);
        if (sh->pps && !sh->pps->occupied_warning) {
            rvmessage("%s: pic_parameter_set_id is not occupied: %d", where, sh->pic_parameter_set_id);
            sh->pps->occupied_warning = 1;
        }
        rvfree(sh);
        return NULL;
    }
    if (!sh->pps->sps->occupied) {
        if (!sh->pps->sps->occupied_warning) {
            rvmessage("%s: seq_parameter_set_id is not occupied: %d", where, sh->pps->seq_parameter_set_id);
            sh->pps->sps->occupied_warning = 1;
        }
        rvfree(sh);
        return NULL;
    }
    const struct hevc_pic_parameter_set *pps = sh->pps;
    struct hevc_seq_parameter_set *sps = pps->sps;
    int dependent_slice_segment_flag = 0;
    if (!sh->first_slice_segment_in_pic_flag) {
        if (pps->dependent_slice_segments_enabled_flag)
            dependent_slice_segment_flag = getbit(bb);                  /* dependent_slice_segment_flag */
        flushbits(bb, clog2(sps->PicSizeInCtbsY));                      /* slice_segment_address */
    }
    if (!dependent_slice_segment_flag) {
        if (pps->num_extra_slice_header_bits)
            flushbits(bb, pps->num_extra_slice_header_bits);            /* slice_reserved_flag[ i ] */
        sh->slice_type = (hevc_slice_type_t)uexpbits(bb);               /* slice_type */
        if (pps->output_flag_present_flag)
            flushbits(bb, 1);                                           /* pic_output_flag */
        if (sps->separate_colour_plane_flag==1)
            flushbits(bb, 2);                                           /* colour_plane_id */
        sh->slice_pic_order_cnt_lsb = 0;
        if (nal_unit_type!=IDR_W_RADL && nal_unit_type!=IDR_N_LP) {
            sh->slice_pic_order_cnt_lsb = getbits(bb, sps-> log2_max_pic_order_cnt_lsb_minus4+4);
#if 0
            int short_term_ref_pic_set_sps_flag = getbit(bb);
            if( !short_term_ref_pic_set_sps_flag )
                short_term_ref_pic_set( num_short_term_ref_pic_sets )
            else if( num_short_term_ref_pic_sets > 1 )
                short_term_ref_pic_set_idx
            if( long_term_ref_pics_present_flag ) {
                if( num_long_term_ref_pics_sps > 0 )
                    num_long_term_sps
                num_long_term_pics
                for( i = 0; i < num_long_term_sps + num_long_term_pics; i++ ) {
                    if( i < num_long_term_sps ) {
                        if( num_long_term_ref_pics_sps > 1 )
                            lt_idx_sps[ i ]
                    } else {
                        poc_lsb_lt[ i ]
                        used_by_curr_pic_lt_flag[ i ]
                    }
                    delta_poc_msb_present_flag[ i ]
                    if( delta_poc_msb_present_flag[ i ] )
                        delta_poc_msb_cycle_lt[ i ]
                }
            }
            if( sps_temporal_mvp_enabled_flag )
                slice_temporal_mvp_enabled_flag
        }
        if( sample_adaptive_offset_enabled_flag ) {
            slice_sao_luma_flag
            slice_sao_chroma_flag
        }
        if( slice_type == P | | slice_type == B ) {
            num_ref_idx_active_override_flag
            if( num_ref_idx_active_override_flag ) {
                num_ref_idx_l0_active_minus1
                if( slice_type == B )
                    num_ref_idx_l1_active_minus1
            }
            if( lists_modification_present_flag && NumPocTotalCurr > 1 )
                ref_pic_lists_modification( )
            if( slice_type == B )
                mvd_l1_zero_flag
            if( cabac_init_present_flag )
                cabac_init_flag
                if( slice_temporal_mvp_enabled_flag ) {
                    if( slice_type == B )
                        collocated_from_l0_flag
                        if( ( collocated_from_l0_flag && num_ref_idx_l0_active_minus1 > 0 ) | |
                            ( !collocated_from_l0_flag && num_ref_idx_l1_active_minus1 > 0 ) )
                            collocated_ref_idx
                }
                if( ( weighted_pred_flag && slice_type == P ) | |
                    ( weighted_bipred_flag &&  == B ) )
                    pred_weight_table( )
                    five_minus_max_num_merge_cand
        }
        slice_qp_delta
        if( pps_slice_chroma_qp_offsets_present_flag ) {
            slice_cb_qp_offset
            slice_cr_qp_offset
        }
        if( deblocking_filter_override_enabled_flag )
            deblocking_filter_override_flag
            if( deblocking_filter_override_flag ) {
                slice_deblocking_filter_disabled_flag
                if( !slice_deblocking_filter_disabled_flag ) {
                    slice_beta_offset_div2
                    slice_tc_offset_div2
                }
            }
            if( pps_loop_filter_across_slices_enabled_flag && ( slice_sao_luma_flag | | slice_sao_chroma_flag | | !slice_deblocking_filter_disabled_flag ) )
                slice_loop_filter_across_slices_enabled_flag
#endif
    }
#if 0
    if (tiles_enabled_flag || entropy_coding_sync_enabled_flag) {
        num_entry_point_offsets
        if( num_entry_point_offsets > 0 ) {
            offset_len_minus1
            for( i = 0; i < num_entry_point_offsets; i++ )
                entry_point_offset_minus1[ i ]
        }
    }
    if( slice_segment_header_extension_present_flag ) {
        slice_segment_header_extension_length
        for( i = 0; i < slice_segment_header_extension_length; i++)
            slice_segment_header_extension_data_byte[ i ]
#endif
    }
    flushbits(bb, numalignbits(bb));                                    /* byte_alignment() */

    /* derived parameters */
    if (prev) {
        if (nal_unit_type>=BLA_W_LP && nal_unit_type<=IDR_N_LP /* TODO || first picture in bitstream */) {
            //NoRaslOutputFlag = 1;
            sh->PicOrderCntMsb = 0;
        } else {
            if ((sh->slice_pic_order_cnt_lsb < prev->prevPicOrderCntLsb) && ((prev->prevPicOrderCntLsb - sh->slice_pic_order_cnt_lsb) >= (sps->MaxPicOrderCntLsb/2) ))
                sh->PicOrderCntMsb = prev->prevPicOrderCntMsb + sps->MaxPicOrderCntLsb;
            else if ((sh->slice_pic_order_cnt_lsb > prev->prevPicOrderCntLsb) && ((sh->slice_pic_order_cnt_lsb - prev->prevPicOrderCntLsb) > (sps->MaxPicOrderCntLsb/2) ))
                sh->PicOrderCntMsb = prev->prevPicOrderCntMsb - sps->MaxPicOrderCntLsb;
            else
                sh->PicOrderCntMsb = prev->prevPicOrderCntMsb;

        }
        sh->PicOrderCntVal = sh->PicOrderCntMsb + sh->slice_pic_order_cnt_lsb;
    }
    return sh;
}

void parse_hevc_sei_pic_timing(struct bitbuf *bb, struct hevc_seq_parameter_set *sps, int payloadSize, int verbose)
{
    if (sps) {
        int frame_field_info_present_flag = 0;
        if (sps->vui_parameters_present_flag) {
            if (sps->vui.frame_field_info_present_flag)
                frame_field_info_present_flag = 1;
        } else {
            // if (general_progressive_source_flag==1 && general_interlaced_source_flag==1)
            //     frame_field_info_present_flag = 1;
        }
        if (frame_field_info_present_flag) {
            int pic_struct       = getbits(bb, 4);                              /* pic_struct       */
            int source_scan_type = getbits(bb, 2);                              /* source_scan_type */
            int duplicate_flag   = getbits(bb, 1);                              /* duplicate_flag   */
            if (verbose>=1)
                rvmessage("sei: picture timing: pic_struct=%d source_scan_type=%d duplicate_flag=%d", pic_struct, source_scan_type, duplicate_flag);
        }
        int CpbDpbDelaysPresentFlag = 0;
        if (sps->vui_parameters_present_flag && sps->vui.hrd.nal_hrd_parameters_present_flag)
            CpbDpbDelaysPresentFlag = 1;
        if (sps->vui_parameters_present_flag && sps->vui.hrd.vcl_hrd_parameters_present_flag)
            CpbDpbDelaysPresentFlag = 1;
        if (CpbDpbDelaysPresentFlag) {
            flushbits(bb, sps->vui.hrd.au_cpb_removal_delay_length_minus1+1);   /* au_cpb_removal_delay_minus1 */
            flushbits(bb, sps->vui.hrd.dpb_output_delay_length_minus1+1);       /* pic_dpb_output_delay */
            if (sps->vui.hrd.sub_pic_hrd_params_present_flag)
                flushbits(bb, sps->vui.hrd.dpb_output_delay_length_minus1+1);   /* pic_dpb_output_du_delay */
            if (sps->vui.hrd.sub_pic_hrd_params_present_flag && sps->vui.hrd.sub_pic_cpb_params_in_pic_timing_sei_flag) {
                int num_decoding_units_minus1 = uexpbits(bb);                   /* num_decoding_units_minus1 */
                int du_common_cpb_removal_delay_flag = getbit(bb);              /* du_common_cpb_removal_delay_flag */
                if (du_common_cpb_removal_delay_flag)
                    flushbits(bb, sps->vui.hrd.du_cpb_removal_delay_increment_length_minus1+1); /* du_common_cpb_removal_delay_increment_minus1 */
                for (int i=0; i<=num_decoding_units_minus1; i++) {
                    /*int num_nalus_in_du_minus1 =*/ uexpbits(bb);              /* num_nalus_in_du_minus1[i] */
                    if (!du_common_cpb_removal_delay_flag && i < num_decoding_units_minus1)
                        flushbits(bb, sps->vui.hrd.du_cpb_removal_delay_increment_length_minus1+1); /* du_cpb_removal_delay_increment_minus1[i] */
                }
            }
        }
    } else {
        /* handle SEI before SPS in stream */
        flushbits(bb, 8*payloadSize);
    }
}

void parse_hevc_sei_frame_field_info(struct bitbuf *bb, int verbose) {
    int pic_struct       = getbits(bb, 4);               /* ffinfo_pic_struct       */
    int source_scan_type = getbits(bb, 2);               /* ffinfo_source_scan_type */
    int duplicate_flag   = getbits(bb, 1);               /* ffinfo_duplicate_flag   */
    if (verbose>=1)
        rvmessage("sei: frame field info: pic_struct=%d source_scan_type=%d duplicate_flag=%d", pic_struct, source_scan_type, duplicate_flag);
}

void parse_hevc_sei_payload(struct bitbuf *bb, struct hevc_seq_parameter_set *sps, int nal_unit_type, int payloadType, int payloadSize, int verbose)
{
    int pos = 0;
    if (nal_unit_type == PREFIX_SEI_NUT) {
        if( payloadType == 0 ) {
            //buffering_period(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: buffering period");
        } else if( payloadType == 1 ) {
            parse_hevc_sei_pic_timing(bb, sps, payloadSize, verbose);
            pos += 8*payloadSize;
        } else if( payloadType == 2 ) {
            //pan_scan_rect(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: pan scan rectangle");
        } else if( payloadType == 3 ) {
            //filler_payload(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: filler payload");
        } else if( payloadType == 4 ) {
            //user_data_registered_itu_t_t35(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: user data registered prefix");
        } else if( payloadType == 5 ) {
            //user_data_unregistered(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: user data unregistered prefix");
        } else if( payloadType == 6 ) {
            //recovery_point(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: recovery point");
        } else if( payloadType == 9 ) {
            //scene_info(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: scene info");
        } else if( payloadType == 15 ) {
            //picture_snapshot(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: picture snapshot");
        } else if( payloadType == 16 ) {
            //progressive_refinement_segment_start(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: progressive refinement segment start");
        } else if( payloadType == 17 ) {
            //progressive_refinement_segment_end(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: progressive refinement segment end");
        } else if( payloadType == 19 ) {
            //film_grain_characteristics(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: film grain characteristics");
        } else if( payloadType == 22 ) {
            //post_filter_hint(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: post filter hint");
        } else if( payloadType == 23 ) {
            //tone_mapping_info(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: tone mapping info");
        } else if( payloadType == 45 ) {
            //frame_packing_arrangement(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: frame packing arrangement");
        } else if( payloadType == 47 ) {
            //display_orientation(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: display orientation");
        } else if( payloadType == 128 ) {
            //structure_of_pictures_info(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: structure of pictures info");
        } else if( payloadType == 129 ) {
            //active_parameter_sets(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: active parameter set");
#if 0
        else if( payloadType == 130 )
            decoding_unit_info(payloadSize)
        else if( payloadType == 131 )
            temporal_sub_layer_zero_index(payloadSize)
        else if( payloadType == 133 )
            scalable_nesting(payloadSize)
        else if( payloadType == 134 )
            region_refresh_info(payloadSize)
        else if( payloadType == 135 )
            no_display(payloadSize)
        else if( payloadType == 136 )
            time_code(payloadSize)
        else if( payloadType == 137 )
            mastering_display_colour_volume(payloadSize)
        else if( payloadType == 138 )
            segmented_rect_frame_packing_arrangement(payloadSize)
        else if( payloadType == 139 )
            temporal_motion_constrained_tile_sets(payloadSize)
        else if( payloadType == 140 )
            chroma_resampling_filter_hint(payloadSize)
        else if( payloadType == 141 )
            knee_function_info(payloadSize)
        else if( payloadType == 142 )
            colour_remapping_info(payloadSize)
        else if( payloadType == 143 )
            deinterlaced_field_identification(payloadSize)
        else if( payloadType == 160 )
            layers_not_present(payloadSize) /* specified in Annex F */
        else if( payloadType == 161 )
            inter_layer_constrained_tile_sets(payloadSize) /* specified in Annex F */
        else if( payloadType == 162 )
            bsp_nesting(payloadSize) /* specified in Annex F */
        else if( payloadType == 163 )
            bsp_initial_arrival_time(payloadSize) /* specified in Annex F */
        else if( payloadType == 164 )
            sub_bitstream_property(payloadSize) /* specified in Annex F */
        else if( payloadType == 165 )
            alpha_channel_info(payloadSize) /* specified in Annex F */
        else if( payloadType == 166 )
            overlay_info(payloadSize) /* specified in Annex F */
        else if( payloadType == 167 )
            temporal_mv_prediction_constraints(payloadSize) /* specified in Annex F */
#endif
        } else if( payloadType == 168 ) {
            //frame_field_info(payloadSize) /* specified in Annex F */
            parse_hevc_sei_frame_field_info(bb, verbose);
            pos += payloadSize*8;
#if 0
        else if( payloadType == 176 )
            three_dimensional_reference_displays_info(payloadSize) /* specified in Annex G */
        else if( payloadType == 177 )
            depth_representation_info(payloadSize) /* specified in Annex G */
        else if( payloadType == 178 )
            multiview_scene_info(payloadSize) /* specified in Annex G */
        else if( payloadType == 179 )
            multiview_acquisition_info(payloadSize) /* specified in Annex G */
        else if( payloadType == 180 )
            multiview_view_position(payloadSize) /* specified in Annex G */
#endif
        } else {
            //reserved_sei_message(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: unknown: %d", payloadType);
        }
    } else { /* nal_unit_type == SUFFIX_SEI_NUT */
        if( payloadType == 3 ) {
            //filler_payload(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: filler payload");
        } else if( payloadType == 4 ) {
            //user_data_registered_itu_t_t35(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: user data registered (suffix)");
        } else if( payloadType == 5 ) {
            //user_data_unregistered(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: user data unregistered (suffix)");
        } else if( payloadType == 17 ) {
            //progressive_refinement_segment_end(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: progressive refinement segment end");
        } else if( payloadType == 22 ) {
            //post_filter_hint(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: post filter hint");
        } else if( payloadType == 132 ) {
            //decoded_picture_hash(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: decoded picture hash");
        } else {
            //reserved_sei_message(payloadSize)
            pos += flushbits(bb, 8*payloadSize);
            if (verbose>=1) rvmessage("sei message: reserved (suffix)");
        }
    }
    if ( !(numalignbits(bb)==0 && pos==8*payloadSize) ) {
        //if( payload_extension_present( ) )
        //    reserved_payload_extension_data
        parse_rbsp_trailing_bits(bb, "hevc sei payload");
    }
}

void parse_hevc_sei_message(struct bitbuf *bb, struct hevc_seq_parameter_set *sps, int nal_unit_type, int verbose)
{
    int payloadType = 0;
    while (showbits(bb, 8 ) == 0xFF) {
        flushbits(bb, 88);                              /* ff_byte equal to 0xFF */
        payloadType += 255;
    }
    int last_payload_type_byte = getbits(bb, 8);        /* last_payload_type_byte */
    payloadType += last_payload_type_byte;
    int payloadSize = 0;
    while (showbits(bb, 8) == 0xFF) {
        flushbits(bb, 8);                               /* ff_byte equal to 0xFF */
        payloadSize += 255;
    }
    int last_payload_size_byte = getbits(bb, 8);
    payloadSize += last_payload_size_byte;
    parse_hevc_sei_payload(bb, sps, nal_unit_type, payloadType, payloadSize, verbose);
}

void parse_hevc_sei_rbsp(struct bitbuf *bb, struct hevc_seq_parameter_set *sps, int nal_unit_type, int verbose)
{
    do
        parse_hevc_sei_message(bb, sps, nal_unit_type, verbose);
    while (more_rbsp_data(bb));
    parse_rbsp_trailing_bits(bb, "hevc sei message");
}

void parse_hevc_access_unit_delimiter(struct bitbuf *bb, int verbose)
{
    const char *where = "hevc access unit delimiter";
    int pic_type = getbits(bb, 3);                      /* pic_type */
    if (verbose>=2)
        rvmessage("picture type in access unit is an %s picture", pic_type==0? "I" : pic_type==1? "I or P" : pic_type==2? "I or P or B" : "unknown");
    parse_rbsp_trailing_bits(bb, where);
}

/* find a single parameter set that might decode a video,
 * if multiple picture parameter sets are required, these
 * are assumed to become available later in the nal stream */
int find_hevc_complete_parameter_set(struct rvhevcvideo *vid)
{
    int i, j;

    if (vid==NULL)
        return -1;
    for (i=0; i<256; i++) {
        if (!vid->pps[i])
            continue;
        if (!vid->pps[i]->occupied)
            continue;
        struct hevc_pic_parameter_set *pps = vid->pps[i];
        for (j=0; j<32; j++)
            if (vid->sps[j])
                if (vid->sps[j]->seq_parameter_set_id==pps->seq_parameter_set_id) {
                    /* initialise vid with a possible parameter set,
                     * this could easily be overridden by the application */
                    vid->active_sps = j;
                    vid->active_pps = i;
                    return j;
                }
    }
    return -1;
}

struct rvhevcvideo *parse_hevc_params(bitbuf *bb, int verbose)
{
    const char *where = "hevc nal unit stream";
    int numnals = 0;

    /* allocate hevc video struct */
    size_t size = sizeof(struct rvhevcvideo);
    struct rvhevcvideo *vid = (struct rvhevcvideo *)rvalloc(NULL, size, 1);

    /* loop over nal units until sufficient parameter nals have been parsed */
    int firstnal = 1; /* first nal in access unit */
    do {

        /* search for the next nal unit */
        int zerobyte, pos = numbits(bb);
        int nal_unit_type = find_next_hevc_nal_unit(bb, &zerobyte);
        if (verbose>=1 && numbits(bb)-pos>7+8*zerobyte)
            rvmessage("%jd bits of leading garbage before first video parameter set nal", numbits(bb)-pos);
        if (verbose>=1 && !eofbits(bb))
            rvmessage("nal_unit %3d: type=%s", numnals, hevc_nal_unit_type_code(nal_unit_type));
        numnals++;
        if (nal_unit_type<0) {
            rvfree(vid);
            break;
        }

        /* parse nal unit header */
        flushbits(bb, 24);              /* start_code_prefix_one_3bytes */
        int ret = parse_hevc_nal_unit_header(bb, &nal_unit_type);
        if (ret<0)
            rvexit("%s: failed to parse nal unit header in file \"%s\"", where, bb->filename);

        /* prepare to parse rbsp */
        struct bitbuf *rbsp = extract_rbsp(bb, verbose);
        if (verbose>=1)
            rvmessage("%s: nal: %s start: length=%d unit_type=%s", where, nal_start[zerobyte], numbitsleft(rbsp)/8+1, hevc_nal_unit_type_code(nal_unit_type));

        /* select nal unit type */
        switch (nal_unit_type) {

            case PREFIX_SEI_NUT: /* supplemental enhancement information */
                /* check for long start code (Annex B.1.2) */
                if (verbose>=0 && firstnal && zerobyte==0)
                    rvmessage("%s: sei nal unit which is first in access unit should have a long start code", where);

                /* parse supplemental enhancement information */
                parse_hevc_sei_rbsp(rbsp, vid->sps[vid->active_sps], nal_unit_type, verbose);
                break;

            case VPS_NUT: /* video parameter set */
            {
                /* check for long start code (Annex B.2.2) */
                if (verbose>=0 && zerobyte==0)
                    rvmessage("%s: vps nal unit should have a long start code", where);

                /* parse video parameter set */
                struct hevc_vid_parameter_set *vps = parse_hevc_vid_param(rbsp);
                if (vps==NULL)
                    rvexit("%s: failed to parse video parameter set header in file \"%s\"", where, bb->filename);
                if (verbose>=2)
                    rvmessage("%s: vid_parameter_set_id=%d", where, vps->vid_parameter_set_id);

                /* store sequence parameter set */
                rvfree(vid->vps[vps->vid_parameter_set_id]);
                vid->vps[vps->vid_parameter_set_id] = vps;

                break;
            }

            case SPS_NUT: /* sequence parameter set */
            {
                /* check for long start code (Annex B.2.2) */
                if (verbose>=0 && zerobyte==0)
                    rvmessage("%s: sps nal unit should have a long start code", where);

                /* parse sequence parameter set */
                struct hevc_seq_parameter_set *sps = parse_hevc_seq_param(rbsp);
                if (sps==NULL)
                    rvexit("%s: failed to parse sequence parameter set header in file \"%s\"", where, bb->filename);
                if (verbose>=2)
                    rvmessage("%s: seq_parameter_set_id=%d", where, sps->seq_parameter_set_id);

                /* store sequence parameter set */
                rvfree(vid->sps[sps->seq_parameter_set_id]);
                vid->sps[sps->seq_parameter_set_id] = sps;

                break;
            }

            case PPS_NUT: /* picture parameter set */
            {
                /* check for long start code (Annex B.2.2) */
                if (verbose>=0 && zerobyte==0)
                    rvmessage("%s: pps nal unit should have a long start code", where);

                /* parse picture parameter set */
                struct hevc_pic_parameter_set *pps = parse_hevc_pic_param(rbsp, vid->sps);
                if (pps==NULL) /* TODO handle this more gracefully */
                    rvexit("%s: failed to parse picture parameter set header in file \"%s\"", where, bb->filename);
                if (verbose>=2)
                    rvmessage("%s: pic_parameter_set_id=%d seq_parameter_set_id=%d", where, pps->pic_parameter_set_id, pps->seq_parameter_set_id);

                /* store sequence parameter set */
                rvfree(vid->pps[pps->pic_parameter_set_id]);
                vid->pps[pps->pic_parameter_set_id] = pps;

                break;
            }

            case EOS_NUT: /* end of sequence */
                if (verbose>=3)
                    rvmessage("%s: found end of sequence nal", where);
                rvfree(vid);
                return NULL;

            case EOB_NUT: /* end of bitstream */
                if (verbose>=3)
                    rvmessage("%s: found end of stream nal", where);
                rvfree(vid);
                return NULL;

            default:
                rvmessage("%s: skipping nal_unit_type=%d", where, nal_unit_type);
                break;

        }
        firstnal = 0;

        /* tidy up */
        free_rbsp(rbsp);

    } while (find_hevc_complete_parameter_set(vid)<0);

    /* report sequence parameters
    if (verbose>=1) {
        char *p, *l;
        describe_profile(sequence->profile_and_level, &p, &l);
        rvmessage("%s: %s@%s %dx%d@%.2ffps", get_basename(bb->filename), p, l,
                  sequence->horizontal_size_value, sequence->vertical_size_value,
                  sequence->frame_rate);
    }*/

    /* sufficient information has now been obtained from sequence to parse first picture */
    return vid;
}

#ifdef HAVE_LIBDE265

/*
 * extract information from libde265 data structures.
 */

struct hevc_vid_parameter_set *extract_hevc_vid_param(const struct de265_image *image)
{
    const video_parameter_set* src = &image->get_vps();

    /* allocate a video parameter set */
    struct hevc_vid_parameter_set *vps = (struct hevc_vid_parameter_set *)rvalloc(NULL, sizeof(struct hevc_vid_parameter_set), 1);
    vps->occupied = 1;

    /* parse video parameter set */
    vps->vid_parameter_set_id = src->video_parameter_set_id;
    vps->max_sub_layers_minus1 = src->vps_max_sub_layers - 1;
    //parse_hevc_profile_tier_level(bb, vps->max_sub_layers_minus1);
    vps->timing_info_present_flag = src->vps_timing_info_present_flag;
    if (vps->timing_info_present_flag) {
        vps->num_units_in_tick = src->vps_num_units_in_tick;
        vps->time_scale = src->vps_time_scale;
    }

    /* calculate derived parameters */

    return vps;
}

struct hevc_seq_parameter_set *extract_hevc_seq_param(const struct de265_image *image)
{
    const seq_parameter_set* src = &image->get_sps();

    /* allocate a sequence parameter set */
    struct hevc_seq_parameter_set *sps = (struct hevc_seq_parameter_set *)rvalloc(NULL, sizeof(struct hevc_seq_parameter_set), 1);
    sps->occupied = 1;

    /* parse sequence parameter set */
    sps->vid_parameter_set_id = src->video_parameter_set_id;
    sps->sps_max_sub_layers_minus1 = src->sps_max_sub_layers - 1;
    //parse_hevc_profile_tier_level(bb, sps->sps_max_sub_layers_minus1);
    sps->seq_parameter_set_id = src->seq_parameter_set_id;
    sps->chroma_format_idc = src->chroma_format_idc;
    if (sps->chroma_format_idc==3)
        sps->separate_colour_plane_flag = src->separate_colour_plane_flag;
    sps->pic_width_in_luma_samples = src->pic_width_in_luma_samples;
    sps->pic_height_in_luma_samples = src->pic_height_in_luma_samples;
    sps->bit_depth_luma_minus8 = src->bit_depth_luma - 8;
    sps->bit_depth_chroma_minus8 = src->bit_depth_chroma - 8;
    sps->log2_max_pic_order_cnt_lsb_minus4 = src->log2_max_pic_order_cnt_lsb - 4;
    sps->log2_min_luma_coding_block_size_minus3 = src->log2_min_luma_coding_block_size - 3;
    sps->log2_diff_max_min_luma_coding_block_size = src->log2_diff_max_min_luma_coding_block_size;
    sps->log2_min_transform_block_size_minus2 = src->log2_min_transform_block_size - 2;
    sps->log2_diff_max_min_transform_block_size = src->log2_diff_max_min_transform_block_size - 2;
    sps->max_transform_hierarchy_depth_inter = src->max_transform_hierarchy_depth_inter;
    sps->max_transform_hierarchy_depth_intra = src->max_transform_hierarchy_depth_intra;
    sps->scaling_list_enable_flag = src->scaling_list_enable_flag;
    sps->amp_enabled_flag = src->amp_enabled_flag;
    sps->sample_adaptive_offset_enabled_flag = src->sample_adaptive_offset_enabled_flag;
    sps->pcm_enabled_flag = src->pcm_enabled_flag;
    sps->vui_parameters_present_flag = src->vui_parameters_present_flag;
    //if (sps->vui_parameters_present_flag)
    //    parse_hevc_vui_params(bb, sps->sps_max_sub_layers_minus1, &sps->vui);

    /* derived parameters */
    sps->MaxPicOrderCntLsb = 1 << (sps->log2_max_pic_order_cnt_lsb_minus4+4);
    sps->SubWidthC = sps->chroma_format_idc==1 || sps->chroma_format_idc==2? 2 : 1;
    sps->SubHeightC = sps->chroma_format_idc==1? 2 : 1;
    sps->MinCbLog2SizeY = sps->log2_min_luma_coding_block_size_minus3 + 3;
    sps->CtbLog2SizeY = sps->MinCbLog2SizeY + sps->log2_diff_max_min_luma_coding_block_size;
    sps->MinCbSizeY = 1 << sps->MinCbLog2SizeY;
    sps->CtbSizeY = 1 << sps->CtbLog2SizeY;
    sps->PicWidthInMinCbsY = sps->pic_width_in_luma_samples / sps->MinCbSizeY;
    sps->PicWidthInCtbsY = (sps->pic_width_in_luma_samples + sps->CtbSizeY - 1) / sps->CtbSizeY;
    sps->PicHeightInMinCbsY = sps->pic_height_in_luma_samples / sps->MinCbSizeY;
    sps->PicHeightInCtbsY = (sps->pic_height_in_luma_samples + sps->CtbSizeY - 1) / sps->CtbSizeY;
    sps->PicSizeInMinCbsY = sps->PicWidthInMinCbsY * sps->PicHeightInMinCbsY;
    sps->PicSizeInCtbsY = sps->PicWidthInCtbsY * sps->PicHeightInCtbsY;
    sps->PicSizeInSamplesY = sps->pic_width_in_luma_samples * sps->pic_height_in_luma_samples;
    sps->PicWidthInSamplesC = sps->pic_width_in_luma_samples / sps->SubWidthC;
    sps->PicHeightInSamplesC = sps->pic_height_in_luma_samples / sps->SubHeightC;
    if (sps->chroma_format_idc==0 || sps->separate_colour_plane_flag) {
        sps->CtbWidthC  = 0;
        sps->CtbHeightC = 0;
    } else {
        sps->CtbWidthC  = sps->CtbSizeY / sps->SubWidthC;
        sps->CtbHeightC = sps->CtbSizeY / sps->SubHeightC;
    }

    /* extra parameters */
    sps->MinTbSizeY = 1 << (sps->log2_min_transform_block_size_minus2+2);
    sps->PicWidthInMinTbsY  = sps->PicWidthInCtbsY  << (sps->CtbLog2SizeY - (sps->log2_min_transform_block_size_minus2+2));
    sps->PicHeightInMinTbsY = sps->PicHeightInCtbsY << (sps->CtbLog2SizeY - (sps->log2_min_transform_block_size_minus2+2));
    sps->PicSizeInMinTbsY = sps->PicWidthInMinTbsY * sps->PicHeightInMinTbsY;

    return sps;
}

struct hevc_pic_parameter_set *extract_hevc_pic_param(const struct de265_image *image, struct hevc_seq_parameter_set **spss)
{
    const pic_parameter_set* src = &image->get_pps();

    /* allocate a picture parameter set */
    struct hevc_pic_parameter_set *pps = (struct hevc_pic_parameter_set *)rvalloc(NULL, sizeof(struct hevc_pic_parameter_set), 1);
    pps->occupied = 1;

    /* parse picture parameter set */
    pps->pic_parameter_set_id = src->pic_parameter_set_id;
    pps->seq_parameter_set_id = src->seq_parameter_set_id;
    pps->sps = spss[pps->seq_parameter_set_id]; // NOTE the sps can be unoccupied at this stage.
    pps->dependent_slice_segments_enabled_flag = src->dependent_slice_segments_enabled_flag;
    pps->output_flag_present_flag = src->output_flag_present_flag;
    pps->num_extra_slice_header_bits = src->num_extra_slice_header_bits;
    pps->num_ref_idx_l0_default_active_minus1 = src->num_ref_idx_l0_default_active - 1;
    pps->num_ref_idx_l1_default_active_minus1 = src->num_ref_idx_l1_default_active - 1;
    pps->init_qp_minus26 = src->pic_init_qp - 26;

    return pps;
}

void extract_hevc_slice_header(const struct de265_image *image, struct hevc_slice_header &sh, struct hevc_pic_parameter_set **ppss)
{
    // TODO support multiple slices
    const slice_segment_header *src = image->get_SliceHeaderCtb(0, 0);

    sh.first_slice_segment_in_pic_flag = src->first_slice_segment_in_pic_flag;
    sh.pic_parameter_set_id = src->slice_pic_parameter_set_id;
    sh.pps = ppss[sh.pic_parameter_set_id];
    sh.slice_type = (hevc_slice_type_t)src->slice_type;
    sh.slice_pic_order_cnt_lsb = src->slice_pic_order_cnt_lsb;
    sh.PicOrderCntVal = image->PicOrderCntVal;
    sh.num_ref_idx_l0_active = src->num_ref_idx_l0_active;
    sh.num_ref_idx_l1_active = src->num_ref_idx_l1_active;
    sh.MaxNumMergeCand = src->MaxNumMergeCand;
    sh.SliceQPY = src->SliceQPY;
}

void extract_hevc_ctus(const struct de265_image *image, struct rvhevcpicture &pic)
{
    hevc_pic_parameter_set *pps = pic.sh.pps;
    hevc_seq_parameter_set *sps = pps->sps;

    /* derived parameters */
    pic.MinCbSizeY = sps->MinCbSizeY;

    /* loop over minimum size coding blocks in frame */
    for (int ycb=0; ycb<sps->PicHeightInMinCbsY; ycb++) {
        for (int xcb=0; xcb<sps->PicWidthInMinCbsY; xcb++) {
            struct rvhevccodingunit *cu = pic.cu.get_in_units(xcb, ycb);

            cu->log2CbSize = image->get_log2CbSize_cbUnits(xcb,ycb);
            if (cu->log2CbSize==0) {
                continue;
            }
            int CbSize = 1<<cu->log2CbSize;
            int HalfCbSize = 1<<(cu->log2CbSize-1);

            /* calculate coordinates of coding block */
            int xb = xcb*pic.MinCbSizeY;
            int yb = ycb*pic.MinCbSizeY;

            /* extract cu level coding parameters */
            cu->transquant_bypass = image->get_cu_transquant_bypass(xb, yb);
            cu->pred_mode = (hevc_pred_mode_t) image->get_pred_mode(xb, yb);
            cu->PartMode = (hevc_part_mode_t) image->get_PartMode(xb, yb);
            cu->pcm_flag = image->get_pcm_flag(xb, yb);
            if (cu->pred_mode==HEVC_MODE_INTRA) {
                hevc_part_mode_t partMode = cu->PartMode;

                switch (partMode) {
                    case HEVC_PART_2Nx2N:
                        cu->IntraPredMode[0] = (hevc_intra_pred_mode_t) image->get_IntraPredMode(xb, yb);
                        break;
                    case HEVC_PART_NxN:
                        cu->IntraPredMode[0] = (hevc_intra_pred_mode_t) image->get_IntraPredMode(xb           , yb           );
                        cu->IntraPredMode[1] = (hevc_intra_pred_mode_t) image->get_IntraPredMode(xb+HalfCbSize, yb           );
                        cu->IntraPredMode[2] = (hevc_intra_pred_mode_t) image->get_IntraPredMode(xb           , yb+HalfCbSize);
                        cu->IntraPredMode[3] = (hevc_intra_pred_mode_t) image->get_IntraPredMode(xb+HalfCbSize, yb+HalfCbSize);
                        break;
                    default:
                        assert(false);
                        break;
                }
            } else {

                /* loop over prediction blocks in coding unit */
                for (int ypb=0; ypb<4; ypb++) {
                    for (int xpb=0; xpb<4; xpb++) {
                        struct rvhevcpredictionunit *pu = pic.pu.get_in_units(xcb*4+xpb*CbSize/pic.MinCbSizeY, ycb*4+ypb*CbSize/pic.MinCbSizeY);

                        /* calculate coordinates of prediction block */
                        int xp = xb + xpb*CbSize/4;
                        int yp = yb + ypb*CbSize/4;

                        const PBMotion mv_info = image->get_mv_info(xp, yp);
                        memcpy(&pu->motion, &mv_info, sizeof(hevc_motion));
                    }
                }
            }

            cu->qpy = image->get_QPY(xb, yb);

        }
    }

    /* derived parameters */
    int max_transform_hierarchy_depth = mmax(sps->max_transform_hierarchy_depth_intra, sps->max_transform_hierarchy_depth_inter);

    /* loop over minimum size coding blocks in frame */
    for (int ytb=0; ytb<sps->PicHeightInMinTbsY; ytb++) {
        for (int xtb=0; xtb<sps->PicWidthInMinTbsY; xtb++) {
            struct rvhevctransformunit *tu = pic.tu.get_in_units(xtb, ytb);

            /* calculate coordinates of transform block */
            int xt = xtb*sps->MinTbSizeY;
            int yt = ytb*sps->MinTbSizeY;

            /* extract tu level coding parameters */
            tu->split_transform_flag = 0;
            for (int depth=0; depth < max_transform_hierarchy_depth ; depth++)
                tu->split_transform_flag |= image->get_split_transform_flag(xt, yt, depth);
        }
    }
}

#endif
