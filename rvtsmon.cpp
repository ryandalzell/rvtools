/*
 * Description: Send or receive video data on a network.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-09-24 15:04:44 $
 * Revision   : $Revision: 1.8 $
 * Copyright  : (c) 2009 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>
#include <errno.h>
#include <fcntl.h>
#include "getopt.h"
#include <time.h>
#include <sys/time.h>
#include <map>
#include <vector>
#include <array>
#include <ncurses.h>

#include "rvutil.h"
#include "rvmp2.h"

using std::map;
using std::vector;
using std::array;

#define MAX_DATAGRAM_SIZE (8*1024)

const char *appname;

/* per-pid data struct */
struct perpid_t {
    perpid_t() { init(); }

    void init () {
        count = count_restart = frames = discontinuity = 0;
        stream_type = STREAM_TYPE_RESERVED;
        continuity = -1;
        first_pcr = last_pcr = -1ll;
        pcr_pid = pcr_wraparound = first_pcr_packet = last_pcr_packet = 0;
        last_pcr_obs = -1ll;
        pcr_ac = pcr_ac_max = 0ll;
        pcr_ac_error = pcr_incr_error = 0;
        pcr_ac_obs = pcr_obs_error = 0;
        pcr_obs_min = pcr_obs_max = 0ll;
    };

    unsigned int count;
    unsigned int count_restart;
    unsigned int frames;
    stream_type_t stream_type;
    // continuity.
    char continuity;
    unsigned int discontinuity;
    // pcr.
    int pcr_pid;
    long long first_pcr;
    long long last_pcr;
    int pcr_wraparound;
    int first_pcr_packet;
    int last_pcr_packet;
    long long last_pcr_obs;
    // pcr accuracy.
    long long pcr_ac;
    int pcr_ac_error;
    int pcr_incr_error;
    long long pcr_ac_max;
    // pcr observed.
    int pcr_ac_obs;
    int pcr_obs_error;
    long long pcr_obs_min;
    long long pcr_obs_max;
};

void usage(int exitcode)
{
    fprintf(stderr, "%s: monitor a transport stream for errors on a network\n", appname);
    fprintf(stderr, "usage: %s [options]\n", appname);
    fprintf(stderr, "  -a, --address       : ip address to send to or listen from (default: any)\n");
    fprintf(stderr, "  -p, --port          : port number to send to or listen from (default: 1234)\n");
    fprintf(stderr, "  -i, --interface     : ip address of interface to listen on for multicast (default: any)\n");
    fprintf(stderr, "  -q, --quiet         : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "  -v, --verbose       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "  --                  : disable argument processing\n");
    fprintf(stderr, "  -h, --help, --usage : print this usage message\n");
    exit(exitcode);
}

bool multicast_address(const char *address)
{
    if (address)
        if (strtol(address, NULL, 10)>=224 && strtol(address, NULL, 10)<=239)
            return true;
    return false;
}

/* utility function for consistency in message window */
static WINDOW *message;
static bool new_messages;

int wprint_message(const char *format, ...) __attribute__ ((format (printf, 1, 2)));
int wprint_message(const char *format, ...)
{
    va_list ap;
    char string[256];

    /* start output with current time */
    const time_t t = time(NULL);
    struct tm *tm = localtime(&t);

    /* format time */
    int len = snprintf(string, sizeof(string), "%02d:%02d:%02d: ", tm->tm_hour, tm->tm_min, tm->tm_sec);

    /* print message */
    va_start(ap, format);
    vsnprintf(string+len, sizeof(string)-len, format, ap);
    int ret = wprintw(message, "%s", string);
    va_end(ap);

    new_messages = true;
    return ret;
}

int main(int argc, char *argv[])
{
    /* command line defaults */
    char *address = NULL;
    int port = 1234;
    const char *interface = "0.0.0.0";
    int verbose = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"address",    1, NULL, 'a'},
            {"port",       1, NULL, 'p'},
            {"interface",  1, NULL, 'i'},
            {"quiet",      0, NULL, 'q'},
            {"verbose",    0, NULL, 'v'},
            {"usage",      0, NULL, 'h'},
            {"help",       0, NULL, 'h'},
            {NULL,         0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "sa:p:i:qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 'a':
                address = optarg;
                break;

            case 'p':
                port = atoi(optarg);
                if (port<=0 || port>65535)
                    rvexit("invalid value for port number: %d", port);
                if (port<1024 && geteuid()!=0)
                    rvexit("can't use privledged port number: %d", port);
                break;

            case 'i':
                interface = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* sanity check command line */
    if (optind<argc) {
        usage (1);
    }

    /* create a udp socket */
    int sock = socket(PF_INET, SOCK_DGRAM, 0);
    if (sock<0)
        rverror("failed to create socket");

    /* assign a name to the socket */
    struct sockaddr_in sa_addr;
    memset(&sa_addr, 0, sizeof(sa_addr));
    sa_addr.sin_family = AF_INET;
    sa_addr.sin_port = htons(port);
    sa_addr.sin_addr.s_addr = multicast_address(address)? inet_addr(address) : htonl(INADDR_ANY);
    if (bind(sock, (struct sockaddr *)&sa_addr, sizeof(sa_addr)))
        rverror("failed to bind to socket");

    /* join a multicast group */
    struct ip_mreqn mc_req;
    memset(&mc_req, 0, sizeof(mc_req));
    if (multicast_address(address)) {
        /* construct an IGMP join request structure */
        mc_req.imr_multiaddr.s_addr = inet_addr(address);
        mc_req.imr_address.s_addr = inet_addr(interface); //htonl(INADDR_ANY);

        /* send an ADD MEMBERSHIP message via setsockopt */
        if ((setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, (void*) &mc_req, sizeof(mc_req))) < 0)
            rverror("failed to send add membership message");
    }

    /* init curses  */
    WINDOW *root = initscr();
    int width = getmaxx(root);
    int height = getmaxy(root);
    const int mwh = mmax(6, height/4); /* message window height */

    /* create the windows */
    WINDOW *left  = newwin(height-mwh, width/2, 0, 0);
    WINDOW *right = newwin(height-mwh, width/2, 0, width/2);
          message = newwin(mwh, width, height-mwh, 0);
    scrollok(message, true);

    /* init the input */
    clear();
    noecho();
    cbreak();           /* disable line buffering */
    timeout(0);         /* non-blocking input */

    /* let the user know something is happening before potentially blocking */
    wprint_message("press 'h' for help\n");
    wprint_message("listening to %s address %s on port %d...\n",
            multicast_address(address)? "multicast" : "unicast", address? address : "localinterface", port);

    /* performance metrics */
    long long totbytes = 0;
    long long totpacks = 0;
    long long starttime = 0;

    /* main application loop */
    char string0[32], string1[32], string2[32];
    long long last = rvmsec();
    int restarts = 0;
    int exit = 0;
    do {
        int packnum = 0;

        /* per-pid data */
        map<int, perpid_t> pids;

        /* global pcr is used for measuring the total bitrate */
        int global_pcr_pid = 0;

        /* initialise running bitrate measure */
        long long bitrate_pcr_sum = 0;
        int bitrate_packet_sum = 0;

        /* initialise program information tables */
        program_association_table pat;
        vector<program_map_table> pmt;

        /* initialise section buffers */
        /* buffers include space for an extra transport packet of packet stuffing bytes */
        u_int8_t pat_section[SECTION_BUFFER_SIZE];
        int pat_pointer = 0;
        vector<array<u_int8_t, SECTION_BUFFER_SIZE> > pmt_section;
        vector<int> pmt_pointer;

        bool refresh = false;
        long long last_pcr_obs = -1ll;
        do {
            unsigned char data[MAX_DATAGRAM_SIZE];
            struct sockaddr_in sa_from;
            socklen_t from_size = sizeof(sa_from);

            /* read from socket */
            int read = recvfrom(sock, data, MAX_DATAGRAM_SIZE, 0, (struct sockaddr *)&sa_from, &from_size);
            long long pcr_obs = rv27MHz(); /* take the observed pcr at this moment, should really be earlier */
            if (read<0)
                wprint_message("failed to receive from socket\n");
            else if (read%188!=0)
                wprint_message("unusual: possibly not a transport stream: read %d bytes\n", read);
            else if (read==MAX_DATAGRAM_SIZE)
                wprint_message("need to increase MAX_DATAGRAM_SIZE: %d\n", MAX_DATAGRAM_SIZE);

            /* look for gaps in reception */
            if (last_pcr_obs>=0) {
                if (pcr_obs-last_pcr_obs > 27ll*15000ll) { // 15 ms
                    wprint_message("no data received for %s, %d stream restarts\n", describe_27MHz(string0, sizeof(string0), pcr_obs-last_pcr_obs), restarts+1);

                    break;
                }
            }
            last_pcr_obs = pcr_obs;

            if (starttime==0) {
                /* start measuring receive rate */
                starttime = rvmsec();
                if (verbose>=0)
                    wprint_message("receiving data from %s:%d, packet length=%d bytes\n", inet_ntoa(sa_from.sin_addr), ntohs(sa_from.sin_port), read);
            }
            if (verbose>=4)
                wprint_message("received packet from %s:%d length=%d bytes data[0]=0x%02x\n", inet_ntoa(sa_from.sin_addr), ntohs(sa_from.sin_port), read, data[0]);

            /* prepare to parse transport stream datagram */
            struct bitbuf *bb = initbits_memory(data, read);

            /* parse transport stream datagram for transport stream packets */
            while (showbits(bb, 8)==0x47) {
                /* parse transport packet header */
                int transport_error_indicator, payload_unit_start_indicator, pid, adaptation_field_control, continuity_counter, discontinuity_indicator, pcr_flag;
                long long pcr;
                int header = parse_transport_packet_header(bb, verbose, packnum, &transport_error_indicator,
                                                        &payload_unit_start_indicator, &pid, &adaptation_field_control, &continuity_counter,
                                                        &discontinuity_indicator, &pcr_flag, &pcr);

                /* calculate payload size and check sanity */
                int payload_size = 188-header;
                if (header<0 || payload_size<0) {
                    wprint_message("packet %5d: error parsing transport packet header", packnum);
                    break;
                }

                /* TODO transport_error_indicator */

                /* count pid frequency */
                pids[pid].count++;
                if (pid!=0)
                    if (payload_unit_start_indicator)
                        pids[pid].frames++;

                /* check continuity_counter */
                if (pid>=0 && pid<0x1FFF) {
                    if (pids[pid].continuity>=0) {
                        int next_continuity = adaptation_field_control==0 || adaptation_field_control==2? pids[pid].continuity : (pids[pid].continuity+1)%16;
                        if (next_continuity != continuity_counter) {
                            if (discontinuity_indicator) {
                                if (verbose>=1)
                                    wprint_message("packet %5d: discontinuity has been indicated (continuity goes from %d to %d)\n",
                                            packnum, pids[pid].continuity, continuity_counter);
                            } else {
                                if (verbose>=0)
                                    wprint_message("packet %5d: continuity error in pid %d, expected %d, packet has %d\n",
                                            packnum, pid, next_continuity, continuity_counter);
                                pids[pid].discontinuity++;
                            }
                        }
                    }
                    pids[pid].continuity = continuity_counter;
                }

                /* measure bitrate for each pid */
                if ((pid==0 || pid==1 || (pid>=16 && pid<0x1FFF)) && pcr_flag) {

                    /* use first pid with a pcr to measure bitrate of pids without their own pcr */
                    if (global_pcr_pid==0 && pcr!=0)
                        global_pcr_pid = pid;

                    /* find first and last pcrs in bitstream */
                    if (pids[pid].first_pcr<0 || pcr<pids[pid].last_pcr) {
                        pids[pid].first_pcr = pcr;
                        pids[pid].first_pcr_packet = packnum;
                        if (pcr<pids[pid].last_pcr) {
                            pids[pid].pcr_wraparound++;
                            /* selective reset */
                            for (auto& n : pids) {
                                n.second.last_pcr = -1ll;
                                n.second.last_pcr_obs = -1ll;
                                n.second.count_restart = n.second.count;
                            }
                            bitrate_pcr_sum = 0;
                            bitrate_packet_sum = 0;
                            wprint_message("packet %5d: pcr wraparound in pid %d: %d restart%s\n", packnum, pid, pids[pid].pcr_wraparound, pids[pid].pcr_wraparound>1?"s":"");
                        }
                    } else {
                        if (pids[pid].last_pcr>=0) {
                            /* update running bitrate measure */
                            long long pcr_delta = pcr - pids[pid].last_pcr;
                            int packet_delta = packnum - pids[pid].last_pcr_packet;
                            bitrate_pcr_sum += pcr_delta;
                            bitrate_packet_sum += packet_delta;

                            /* calculate pcr accuracy */
                            long long expected_pcr_delta = (long long)packet_delta * bitrate_pcr_sum / (long long)bitrate_packet_sum;
                            pids[pid].pcr_ac = pcr_delta - expected_pcr_delta;

                            /* log pcr events */
                            if (llabs(pids[pid].pcr_ac)*1000ll > 27ll*500ll) { /* 500 ns */
                                pids[pid].pcr_ac_error++;
                                if (verbose>=1)
                                    wprint_message("packet %5d: pcr accuracy in pid %d, expected delta %s, packet has delta %s\n",
                                            packnum, pid, describe_27MHz(string0, sizeof(string0), pcr_delta), describe_27MHz(string1, sizeof(string1), expected_pcr_delta));
                            }
                            if (pcr==pids[pid].last_pcr)
                                pids[pid].pcr_incr_error++;
                            pids[pid].pcr_ac_max = mmax(pids[pid].pcr_ac_max, llabs(pids[pid].pcr_ac));

                            if (pids[pid].last_pcr_obs>=0) {
                                /* calculate observed pcr accuracy */
                                long long pcr_obs_delta = pcr_obs - pids[pid].last_pcr_obs;
                                pids[pid].pcr_ac_obs = pcr_obs_delta - expected_pcr_delta;

                                /* log pcr_obs events */
                                if (llabs(pids[pid].pcr_ac_obs) > 27ll*50ll) { /* +/-25 us = 50us */
                                    pids[pid].pcr_obs_error++;
                                }
                                pids[pid].pcr_obs_min = mmin(pids[pid].pcr_obs_min, pids[pid].pcr_ac_obs);
                                pids[pid].pcr_obs_max = mmax(pids[pid].pcr_obs_max, pids[pid].pcr_ac_obs);
                            }
                        }

                        pids[pid].last_pcr = pcr;
                        pids[pid].last_pcr_obs = pcr_obs;
                        pids[pid].last_pcr_packet = packnum;
                    }
                }

                /* parse transport packet payload */
                if (adaptation_field_control==1 || adaptation_field_control==3) {
                    if (pid==0x0000) {
                        /* look for new pat section */
                        if (payload_unit_start_indicator) {
                            payload_size -= parse_pointer_field(bb);
                            pat_pointer = 0;
                        }

                        /* copy payload to pat section */
                        while (payload_size) {
                            if (pat_pointer==1024) {
                                wprint_message("packet %5d: pat section is too large", packnum);
                                break;
                            }
                            pat_section[pat_pointer++] = getbits8(bb);
                            payload_size--;
                        }

                        /* try to parse pmt section */
                        struct bitbuf *sb = initbits_memory(pat_section, pat_pointer);
                        int ret = parse_program_association_section(sb, pat_pointer, verbose, &pat);
                        freebits(sb);

                        /* allocate pmt table and pmt section for each program */
                        if (ret>0) {
                            pmt_section.resize(pat.number_programs);
                            pmt_pointer.resize(pat.number_programs);
                        }
                    } else {
                        for (int i=0; i<pat.number_programs; i++)
                            if (pid==pat.program[i].program_map_pid) {
                                /* TODO check continuity_counter */

                                /* look for new pmt section */
                                if (payload_unit_start_indicator) {
                                    payload_size -= parse_pointer_field(bb);
                                    pmt_pointer[i] = 0;
                                }

                                /* copy payload to pmt section */
                                while (payload_size) {
                                    if (pmt_pointer[i]==1024) {
                                        wprint_message("packet %5d: pmt section is too large", packnum);
                                        break;
                                    }
                                    pmt_section[i][pmt_pointer[i]++] = getbits8(bb);
                                    payload_size--;
                                }

                                /* make sure vector is large enough */
                                if (pmt.size()<i+1)
                                    pmt.resize(i+1);

                                /* try to parse pmt section */
                                struct bitbuf *sb = initbits_memory(pmt_section[i].data(), pmt_pointer[i]);
                                int ret = parse_program_map_section(sb, pmt_pointer[i], verbose, &pmt[i]);
                                freebits(sb);

                                /* annotate pids with their pcr_pid */
                                if (ret>0) {
                                    for (int j=0; j<pmt[i].number_streams; j++) {
                                        int pmt_pid = pmt[i].stream[j].elementary_pid;
                                        pids[pmt_pid].pcr_pid = pmt[i].pcr_pid;
                                        pids[pmt_pid].stream_type = pmt[i].stream[j].stream_type;
                                    }
                                }
                            }
                    }
                }

                /* drop remaining data */
                flushbits(bb, 8*payload_size);

                packnum++;
                totpacks++;
            }
            freebits(bb);

            totbytes += read;

            /* real-time receive bitrate */
            long long now = rvmsec();
            if (now-last>=1000 || refresh) {
                werase(left);
                wmove(left, 0,0);
                wprintw(left, "+--------------------------------------------------------+\n");
                wprintw(left, "| monitored %6sB and %6s packets in %14s |\n",
                            describe_number(string0, sizeof(string0), totbytes, 1),
                            describe_number(string1, sizeof(string1), totpacks, 1),
                            describe_duration(string2, sizeof(string2), (now-starttime)/1000.0));

                /* calculate average bitrate
                    * long long bitrate = 1;
                    * if (pcr_pid>0 && first_pcr[pcr_pid]>=0 && last_pcr[pcr_pid]>=0 && last_pcr[pcr_pid]!=first_pcr[pcr_pid]) {
                    *     bitrate = (last_pcr_packet[pcr_pid]-first_pcr_packet[pcr_pid]) * 188ll*8ll;
                    *     bitrate *= 27000000ll;
                    *     bitrate /= pcr_wraparound[pcr_pid]*MAX_PCR_VALUE+last_pcr[pcr_pid]-first_pcr[pcr_pid];
                    * }
                    */

                /* display per-pid statistics */
                wprintw(left, "+------+---------+--------+-------+----------+-----------+\n");
                wprintw(left, "|  pid | packets | frames | ratio |  discon  |  bitrate  +\n");
                wprintw(left, "+------+---------+--------+-------+----------+-----------+\n");
                double totrate = 0.0;
                //for (auto const &it : pids) {
                map<int, perpid_t>::const_iterator it;
                for (it = pids.begin(); it != pids.end(); ++it) {
                    int pid = it->first;
                    const perpid_t &p = it->second;
                    if (p.count) {
                        /* find a suitable pcr for this pid */
                        int pcr_pid = pids[pid].pcr_pid; /* use the pcr for this pid */
                        if (pcr_pid==0)
                            pcr_pid = global_pcr_pid; /* use the global pcr if pid doesn't have its own pcr */

                        double bitrate = 0.0;
                        if (pids[pcr_pid].last_pcr>pids[pcr_pid].first_pcr) {
                            /* pid has a timebase */
                            bitrate = (p.count-p.count_restart) * 188.0*8.0;
                            bitrate *= 27000000.0;
                            bitrate /= /*pids[pcr_pid].pcr_wraparound*MAX_PCR_VALUE+*/pids[pcr_pid].last_pcr-pids[pcr_pid].first_pcr;
                            totrate += bitrate;
                        }
                        wprintw(left, "| %4d | %7s | %6s | %4.1f%% | %8u | %9s |\n", pid,
                                    describe_number(string1, sizeof(string1), p.count, 0),
                                    // TODO a better way of not counting frames for PSI.
                                    describe_number(string2, sizeof(string2), p.frames==p.count? 0 : p.frames, 0),
                                    100.0*p.count/packnum, p.discontinuity,
                                    bitrate>0.01? describe_bitrate(string0, sizeof(string0), bitrate, 1, 1) : "         ");
                    }
                }
                wprintw(left, "+------+---------+--------+-------+----------+-----------+\n");
                wprintw(left, "|      |         |        |       |          | %9s |\n",
                        describe_bitrate(string0, sizeof(string0), totrate, 1, 1));
                wprintw(left, "+------+---------+--------+-------+----------+-----------+\n");

                /* display pcr statistics */
                wprintw(left, "+------+--------+-------+-------+--------+---------+-------------+-------------+\n");
                wprintw(left, "|  pid | pcr_ac | _errs | _incr |   _max | pcr_obs |        _min |        _max |\n");
                wprintw(left, "+------+--------+-------+-------+--------+---------+-------------+-------------+\n");
                //for (auto const &it : pids) {
                for (it = pids.begin(); it != pids.end(); ++it) {
                    int pid = it->first;
                    const perpid_t &p = it->second;
                    if (p.last_pcr>p.first_pcr) {
                        wprintw(left, "| %4d | %4lldns | %5u | %5u | %4lldns | %7s | %11s | %11s |\n", pid,
                                    p.pcr_ac*1000ll/27ll, p.pcr_ac_error, p.pcr_incr_error, p.pcr_ac_max*1000ll/27ll,
                                    describe_27MHz(string0, sizeof(string0), p.pcr_ac_obs),
                                    describe_27MHz(string1, sizeof(string1), p.pcr_obs_min),
                                    describe_27MHz(string2, sizeof(string2), p.pcr_obs_max));
                    }
                }
                wprintw(left, "+------+--------+-------+-------+--------+---------+-------------+-------------+\n");
                wrefresh(left);

                /* display pat */
                werase(right);
                wmove(right, 0,0);
                wprintw(right, "+-------+-------+-----+\n");
                wprintw(right, "|pat v%-2d|program| pid |\n", pat.version_number);
                wprintw(right, "+-------+-------+-----+\n");
                for (int i=0; i<pat.number_programs; i++)
                    wprintw(right, "|       |% 7d|% 5d|\n", pat.program[i].program_number, pat.program[i].program_map_pid);
                wprintw(right, "+-------+-------+-----+\n");

                /* display pmt */
                for (int i=0; i<pat.number_programs; i++) {
                    int pid = pat.program[i].program_map_pid;
                    if (pids[pid].count) {
                        wprintw(right, "+-------+-----+---+------+------------------------------------+\n");
                        wprintw(right, "|pmt v%-2d|  program %-5d | pcr_pid=%-4d                       |\n", pmt[i].version_number, pmt[i].program_number, pmt[i].pcr_pid);
                        wprintw(right, "+-------+-----+---+------+------------------------------------+\n");
                        wprintw(right, "|       | pid |pcr|stream|              description           |\n");
                        wprintw(right, "+-------+-----+---+------+------------------------------------+\n");
                        for (int j=0; j<pmt[i].number_streams; j++) {
                            wprintw(right, "|       |% 5d| %1c |% 6d|%36s|\n", pmt[i].stream[j].elementary_pid, pmt[i].stream[j].elementary_pid==pmt[i].pcr_pid? 'y': ' ', pmt[i].stream[j].stream_type, describe_stream_type(pmt[i].stream[j].stream_type, pmt[i].stream[j].format_identifier));
                        }
                        wprintw(right, "+-------+-----+---+------+------------------------------------+\n");
                    }
                }
                wrefresh(right);

                last = now;
                refresh = false;
            }

            /* look for any user input */
            int c = getch();
            if (c!=ERR) {
                switch (c) {
                    case 'e':
                        /* erase and refresh screen in case of visual corruption */
                        erase();
                        new_messages = true;
                        break;

                    case 'r':
                        /* reset statistics */
                        wprint_message("resetting long term statistics\n");
                        for (auto& n : pids)
                            n.second.init();
                        bitrate_pcr_sum = 0;
                        bitrate_packet_sum = 0;
                        refresh = true;
                        break;

                    case 's':
                        wprint_message("receiving data from %s:%d, packet length=%d bytes\n", inet_ntoa(sa_from.sin_addr), ntohs(sa_from.sin_port), read);
                        break;

                    case 'h':
                        wprint_message("\te: erase screen\n\t\tr: reset statistics\n\t\ts: show stream info\n\t\tv: increase verbosity\n\t\tV: decrease verbosity\n\t\te: erase screen\n\t\tq: quit\n");
                        break;

                    case 'q':
                        //wprint_message("exit by user request\n");
                        exit = 1;
                        break;

                    case 'v':
                        verbose++;
                        wprint_message("changing verbosity level to %d\n", verbose);
                        break;

                    case 'V':
                        verbose--;
                        wprint_message("changing verbosity level to %d\n", verbose);
                        break;
                }
            }

            /* update message window if needed */
            if (new_messages) {
                wrefresh(message);
                new_messages = false;
            }
        } while (!exit);
        restarts++;
    } while (!exit);

    /* leave multicast group */
    if (multicast_address(address)) {
        /* send a DROP MEMBERSHIP message via setsockopt */
        if ((setsockopt(sock, IPPROTO_IP, IP_DROP_MEMBERSHIP, (void*) &mc_req, sizeof(mc_req))) < 0)
            rverror("failed to send drop membership message");
    }

    /* tidy up */
    delwin(left);
    delwin(right);
    delwin(message);
    endwin();

    if (verbose>=0)
        rvmessage("monitored %6sB and %6s packets in %14s over %d stream restarts\n",
                            describe_number(string0, sizeof(string0), totbytes, 1),
                            describe_number(string1, sizeof(string1), totpacks, 1),
                            describe_duration(string2, sizeof(string2), (rvmsec()-starttime)/1000.0),
                            restarts);

    return 0;
}
