/*
 * Description: YUV to RGB conversion functions.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-06-23 13:26:19 $
 * Revision   : $Revision: 1.17 $
 * Copyright  : (c) 2005,2013 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/* This software was written by Erik Corry, who agreed to place this
 * file under the same licensing terms as the rest of the tmndec
 * software.  See the accompanying COPYING file for details.  The
 * original copyright notice from Erik Corry is included below for
 * completeness.  */
/*
 * Copyright (c) 1995 Erik Corry
 * All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without written agreement is
 * hereby granted, provided that the above copyright notice and the following
 * two paragraphs appear in all copies of this software.
 *
 * IN NO EVENT SHALL ERIK CORRY BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
 * SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF
 * THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF ERIK CORRY HAS BEEN ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ERIK CORRY SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS"
 * BASIS, AND ERIK CORRY HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include "rvutil.h"
#include "rvdisplay.h"

#undef INTERPOLATE

/*
 * Erik Corry's multi-byte dither routines.
 *
 * The basic idea is that the Init generates all the necessary tables.
 * The tables incorporate the information about the layout of pixels
 * in the XImage, so that it should be able to cope with 15-bit, 16-bit
 * 24-bit (non-packed) and 32-bit (10-11 bits per color!) screens.
 * At present it cannot cope with 24-bit packed mode, since this involves
 * getting down to byte level again. It is assumed that the bits for each
 * color are contiguous in the longword.
 *
 * Writing to memory is done in shorts or ints. (Unfortunately, short is not
 * very fast on Alpha, so there is room for improvement here). There is no
 * dither time check for overflow - instead the tables have slack at
 * each end. This is likely to be faster than an 'if' test as many modern
 * architectures are really bad at ifs. Potentially, each '&&' causes a
 * pipeline flush!
 *
 * There is no shifting and fixed point arithmetic, as I really doubt you
 * can see the difference, and it costs. This may be just my bias, since I
 * heard that Intel is really bad at shifting.
 */

/* Gamma correction stuff */

#define GAMMA_CORRECTION(x) ((int)(pow((x) / 255.0, 1.0 / gammaCorrect) * 255.0))
#define CHROMA_CORRECTION256(x) ((x) >= 128 \
                    ? 128 + mmin(127, (int)(((x) - 128.0) * chromaCorrect)) \
                    : 128 - mmin(128, (int)((128.0 - (x)) * chromaCorrect)))
#define CHROMA_CORRECTION128(x) ((x) >= 0 \
                    ? mmin(127,  (int)(((x) * chromaCorrect))) \
                    : mmax(-128, (int)(((x) * chromaCorrect))))
#define CHROMA_CORRECTION256D(x) ((x) >= 128 \
                    ? 128.0 + mmin(127.0, (((x) - 128.0) * chromaCorrect)) \
                    : 128.0 - mmin(128.0, (((128.0 - (x)) * chromaCorrect))))
#define CHROMA_CORRECTION128D(x) ((x) >= 0 \
                    ? mmin(127.0,  ((x) * chromaCorrect)) \
                    : mmax(-128.0, ((x) * chromaCorrect)))

#define GAMMA_CORRECTION10(x) ((int)(pow((x) / 1023.0, 1.0 / gammaCorrect) * 1023.0))
#define CHROMA_CORRECTION1024(x) ((x) >= 512 \
                    ? 512 + mmin(511, (int)(((x) - 512.0) * chromaCorrect)) \
                    : 512 - mmin(512, (int)((512.0 - (x)) * chromaCorrect)))
#define CHROMA_CORRECTION512(x) ((x) >= 0 \
                    ? mmin(511,  (int)(((x) * chromaCorrect))) \
                    : mmax(-512, (int)(((x) * chromaCorrect))))
#define CHROMA_CORRECTION1024D(x) ((x) >= 512 \
                    ? 512.0 + mmin(511.0, (((x) - 512.0) * chromaCorrect)) \
                    : 512.0 - mmin(512.0, (((512.0 - (x)) * chromaCorrect))))
#define CHROMA_CORRECTION512D(x) ((x) >= 0 \
                    ? mmin(511.0,  ((x) * chromaCorrect)) \
                    : mmax(-512.0, ((x) * chromaCorrect)))

/* Flag for gamma correction */
int gammaCorrectFlag = 0;
double gammaCorrect = 1.0;

/* Flag for chroma correction */
int chromaCorrectFlag = 0;
double chromaCorrect = 1.0;

/*
 * How many 1 bits are there in the longword.
 * Low performance, do not call often.
 */
static int number_of_bits_set(unsigned long a)
{
    if(!a) return 0;
    if(a & 1) return 1 + number_of_bits_set(a >> 1);
    return(number_of_bits_set(a >> 1));
}

/*
 * Shift the 0s in the least significant end out of the longword.
 * Low performance, do not call often.
 */
#if 0
static unsigned long shifted_down(unsigned long a)
{
    if(!a) return 0;
    if(a & 1) return a;
    return a >> 1;
}
#endif

/*
 * How many 0 bits are there at most significant end of longword.
 * Low performance, do not call often.
 */
#if 0
static int free_bits_at_top(unsigned long a)
{
    /* assume char is 8 bits */
    if(!a) return sizeof(unsigned long) * 8;
        /* assume twos complement */
    if(((long)a) < 0l) return 0;
    return 1 + free_bits_at_top ( a << 1);
}
#endif

/*
 * How many 0 bits are there at least significant end of longword.
 * Low performance, do not call often.
 */
static int free_bits_at_bottom(unsigned long a)
{
    /* assume char is 8 bits */
    if(!a) return sizeof(unsigned long) * 8;
    if(((long)a) & 1l) return 0;
    return 1 + free_bits_at_bottom ( a >> 1);
}

static int *L_tab, *Cr_r_tab, *Cr_g_tab, *Cb_g_tab, *Cb_b_tab;
static int *L_tab10, *Cr_r_tab10, *Cr_g_tab10, *Cb_g_tab10, *Cb_b_tab10;

/*
 * We define tables that convert a color value between -256 and 512
 * into the R, G and B parts of the pixel. The normal range is 0-255.
 */

static long *r_2_pix;
static long *g_2_pix;
static long *b_2_pix;
static long *r_2_pix_alloc;
static long *g_2_pix_alloc;
static long *b_2_pix_alloc;

/*
 *--------------------------------------------------------------
 *
 * InitColorDither
 *
 *  To get rid of the multiply and other conversions in color
 *  dither, we use a lookup table.
 *
 *--------------------------------------------------------------
 */
void InitColorDither(Display *display, Window window)
{
    int CR, CB, i;
    int depth = DefaultDepth(display, DefaultScreen(display));

    /*
     * note that this implies that the window is
     * created before this routine is called
     */
    XWindowAttributes xwa;
    XGetWindowAttributes(display, window, &xwa);
    unsigned long red_mask   = xwa.visual->red_mask;
    unsigned long green_mask = xwa.visual->green_mask;
    unsigned long blue_mask  = xwa.visual->blue_mask;

    init_dither_tab();

    L_tab    = (int *)rvalloc(NULL, 256*sizeof(int), 0);
    Cr_r_tab = (int *)rvalloc(NULL, 256*sizeof(int), 0);
    Cr_g_tab = (int *)rvalloc(NULL, 256*sizeof(int), 0);
    Cb_g_tab = (int *)rvalloc(NULL, 256*sizeof(int), 0);
    Cb_b_tab = (int *)rvalloc(NULL, 256*sizeof(int), 0);

    L_tab10    = (int *)rvalloc(NULL, 1024*sizeof(int), 0);
    Cr_r_tab10 = (int *)rvalloc(NULL, 1024*sizeof(int), 0);
    Cr_g_tab10 = (int *)rvalloc(NULL, 1024*sizeof(int), 0);
    Cb_g_tab10 = (int *)rvalloc(NULL, 1024*sizeof(int), 0);
    Cb_b_tab10 = (int *)rvalloc(NULL, 1024*sizeof(int), 0);

    r_2_pix_alloc = (long *)rvalloc(NULL, 768*sizeof(long), 0);
    g_2_pix_alloc = (long *)rvalloc(NULL, 768*sizeof(long), 0);
    b_2_pix_alloc = (long *)rvalloc(NULL, 768*sizeof(long), 0);

    for (i=0; i<256; i++) {
      L_tab[i] = i;
      if (gammaCorrectFlag) {
        L_tab[i] = GAMMA_CORRECTION(i);
      }

      CB = CR = i;

      if (chromaCorrectFlag) {
        CB -= 128;
        CB = CHROMA_CORRECTION128(CB);
        CR -= 128;
        CR = CHROMA_CORRECTION128(CR);
      } else {
        CB -= 128; CR -= 128;
      }
/* was
      Cr_r_tab[i] =  1.596 * CR;
      Cr_g_tab[i] = -0.813 * CR;
      Cb_g_tab[i] = -0.391 * CB;
      Cb_b_tab[i] =  2.018 * CB;
  but they were just messed up.
  Then was (_Video Deymstified_):
      Cr_r_tab[i] =  1.366 * CR;
      Cr_g_tab[i] = -0.700 * CR;
      Cb_g_tab[i] = -0.334 * CB;
      Cb_b_tab[i] =  1.732 * CB;
  but really should be:
   (from ITU-R BT.470-2 System B, G and SMPTE 170M )
*/
      Cr_r_tab[i] =  (0.419/0.299) * CR;
      Cr_g_tab[i] = -(0.299/0.419) * CR;
      Cb_g_tab[i] = -(0.114/0.331) * CB;
      Cb_b_tab[i] =  (0.587/0.331) * CB;

/*
  though you could argue for:
    SMPTE 240M
      Cr_r_tab[i] =  (0.445/0.212) * CR;
      Cr_g_tab[i] = -(0.212/0.445) * CR;
      Cb_g_tab[i] = -(0.087/0.384) * CB;
      Cb_b_tab[i] =  (0.701/0.384) * CB;
    FCC
      Cr_r_tab[i] =  (0.421/0.30) * CR;
      Cr_g_tab[i] = -(0.30/0.421) * CR;
      Cb_g_tab[i] = -(0.11/0.331) * CB;
      Cb_b_tab[i] =  (0.59/0.331) * CB;
    ITU-R BT.709
      Cr_r_tab[i] =  (0.454/0.2125) * CR;
      Cr_g_tab[i] = -(0.2125/0.454) * CR;
      Cb_g_tab[i] = -(0.0721/0.386) * CB;
      Cb_b_tab[i] =  (0.7154/0.386) * CB;
*/
    }

    for (i=0; i<1024; i++) {
        L_tab10[i] = i;
        if (gammaCorrectFlag) {
            L_tab10[i] = GAMMA_CORRECTION10(i);
        }

        CB = CR = i;

        if (chromaCorrectFlag) {
            CB -= 512;
            CB = CHROMA_CORRECTION512(CB);
            CR -= 512;
            CR = CHROMA_CORRECTION512(CR);
        } else {
            CB -= 512; CR -= 512;
        }
        /* was
         *      Cr_r_tab[i] =  1.596 * CR;
         *      Cr_g_tab[i] = -0.813 * CR;
         *      Cb_g_tab[i] = -0.391 * CB;
         *      Cb_b_tab[i] =  2.018 * CB;
         *  but they were just messed up.
         *  Then was (_Video Deymstified_):
         *      Cr_r_tab[i] =  1.366 * CR;
         *      Cr_g_tab[i] = -0.700 * CR;
         *      Cb_g_tab[i] = -0.334 * CB;
         *      Cb_b_tab[i] =  1.732 * CB;
         *  but really should be:
         *   (from ITU-R BT.470-2 System B, G and SMPTE 170M )
         */
        Cr_r_tab10[i] =  (0.419/0.299) * CR;
        Cr_g_tab10[i] = -(0.299/0.419) * CR;
        Cb_g_tab10[i] = -(0.114/0.331) * CB;
        Cb_b_tab10[i] =  (0.587/0.331) * CB;

        /*
         *  though you could argue for:
         *    SMPTE 240M
         *      Cr_r_tab[i] =  (0.445/0.212) * CR;
         *      Cr_g_tab[i] = -(0.212/0.445) * CR;
         *      Cb_g_tab[i] = -(0.087/0.384) * CB;
         *      Cb_b_tab[i] =  (0.701/0.384) * CB;
         *    FCC
         *      Cr_r_tab[i] =  (0.421/0.30) * CR;
         *      Cr_g_tab[i] = -(0.30/0.421) * CR;
         *      Cb_g_tab[i] = -(0.11/0.331) * CB;
         *      Cb_b_tab[i] =  (0.59/0.331) * CB;
         *    ITU-R BT.709
         *      Cr_r_tab[i] =  (0.454/0.2125) * CR;
         *      Cr_g_tab[i] = -(0.2125/0.454) * CR;
         *      Cb_g_tab[i] = -(0.0721/0.386) * CB;
         *      Cb_b_tab[i] =  (0.7154/0.386) * CB;
         */
    }

    /*
     * Set up entries 0-255 in rgb-to-pixel value tables.
     */
    for (i = 0; i < 256; i++) {
      r_2_pix_alloc[i + 256] = i >> (8 - number_of_bits_set(red_mask));
      r_2_pix_alloc[i + 256] <<= free_bits_at_bottom(red_mask);
      g_2_pix_alloc[i + 256] = i >> (8 - number_of_bits_set(green_mask));
      g_2_pix_alloc[i + 256] <<= free_bits_at_bottom(green_mask);
      b_2_pix_alloc[i + 256] = i >> (8 - number_of_bits_set(blue_mask));
      b_2_pix_alloc[i + 256] <<= free_bits_at_bottom(blue_mask);
      /*
       * If we have 16-bit output depth, then we double the value
       * in the top word. This means that we can write out both
       * pixels in the pixel doubling mode with one op. It is
       * harmless in the normal case as storing a 32-bit value
       * through a short pointer will lose the top bits anyway.
       * A similar optimisation for Alpha for 64 bit has been
       * prepared for, but is not yet implemented.
       */
      if(!(depth == 24 || depth == 32)) {

        r_2_pix_alloc[i + 256] |= (r_2_pix_alloc[i + 256]) << 16;
        g_2_pix_alloc[i + 256] |= (g_2_pix_alloc[i + 256]) << 16;
        b_2_pix_alloc[i + 256] |= (b_2_pix_alloc[i + 256]) << 16;

      }
#ifdef SIXTYFOUR_BIT
      if(depth == 24 || depth == 32) {

        r_2_pix_alloc[i + 256] |= (r_2_pix_alloc[i + 256]) << 32;
        g_2_pix_alloc[i + 256] |= (g_2_pix_alloc[i + 256]) << 32;
        b_2_pix_alloc[i + 256] |= (b_2_pix_alloc[i + 256]) << 32;

      }
#endif
    }

    /*
     * Spread out the values we have to the rest of the array so that
     * we do not need to check for overflow.
     */
    for (i = 0; i < 256; i++) {
      r_2_pix_alloc[i] = r_2_pix_alloc[256];
      r_2_pix_alloc[i+ 512] = r_2_pix_alloc[511];
      g_2_pix_alloc[i] = g_2_pix_alloc[256];
      g_2_pix_alloc[i+ 512] = g_2_pix_alloc[511];
      b_2_pix_alloc[i] = b_2_pix_alloc[256];
      b_2_pix_alloc[i+ 512] = b_2_pix_alloc[511];
    }

    r_2_pix = r_2_pix_alloc + 256;
    g_2_pix = g_2_pix_alloc + 256;
    b_2_pix = b_2_pix_alloc + 256;

}

/*
 *--------------------------------------------------------------
 *
 * Color16DitherImage
 *
 *  Converts image into 16 bit color.
 *
 *--------------------------------------------------------------
 */
void Color16DitherImage(unsigned char *src[], unsigned char *out, int width, int height, int expand)
{
    unsigned char *lum = src[0];
    unsigned char *cb = src[1];
    unsigned char *cr = src[2];
    int cols;
    int rows;

    int L, CR, CB;
    unsigned short *row1, *row2;
    unsigned char *lum2;
    int x, y;
    int cr_r;
    int cr_g;
    int cb_g;
    int cb_b;
    int cols_2;

    cols = width;
    rows = height;
    cols_2 = cols/2;

    row1 = (unsigned short *)out;
    row2 = row1 + cols_2 + cols_2;
    lum2 = lum + cols_2 + cols_2;

    for (y=0; y<rows; y+=2) {
        for (x=0; x<cols_2; x++) {
            int R, G, B;

            CR = *cr++;
            CB = *cb++;
            cr_r = Cr_r_tab[CR];
            cr_g = Cr_g_tab[CR];
            cb_g = Cb_g_tab[CB];
            cb_b = Cb_b_tab[CB];

            L = L_tab[(int) *lum++];

            R = L + cr_r;
            G = L + cr_g + cb_g;
            B = L + cb_b;

            *row1++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

#ifdef INTERPOLATE
            if(x != cols_2 - 1) {
              CR = (CR + *cr) >> 1;
              CB = (CB + *cb) >> 1;
              cr_r = Cr_r_tab[CR];
              cr_g = Cr_g_tab[CR];
              cb_g = Cb_g_tab[CB];
              cb_b = Cb_b_tab[CB];
            }
#endif

            L = L_tab[(int) *lum++];

            R = L + cr_r;
            G = L + cr_g + cb_g;
            B = L + cb_b;

            *row1++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

            /*
             * Now, do second row.
             */
#ifdef INTERPOLATE
            if(y != rows - 2) {
              CR = (CR + *(cr + cols_2 - 1)) >> 1;
              CB = (CB + *(cb + cols_2 - 1)) >> 1;
              cr_r = Cr_r_tab[CR];
              cr_g = Cr_g_tab[CR];
              cb_g = Cb_g_tab[CB];
              cb_b = Cb_b_tab[CB];
            }
#endif

            L = L_tab[(int) *lum2++];
            R = L + cr_r;
            G = L + cr_g + cb_g;
            B = L + cb_b;

            *row2++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

            L = L_tab[(int) *lum2++];
            R = L + cr_r;
            G = L + cr_g + cb_g;
            B = L + cb_b;

            *row2++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);
        }
        /*
         * These values are at the start of the next line, (due
         * to the ++'s above),but they need to be at the start
         * of the line after that.
         */
        lum += cols_2 + cols_2;
        lum2 += cols_2 + cols_2;
        row1 += cols_2 + cols_2;
        row2 += cols_2 + cols_2;
    }
}

/*
 *--------------------------------------------------------------
 *
 * YUVplanar_RGB32
 *
 *  Converts YUV planar 4:2:0 into 32-bit packed RGB.
 *
 *  This is a copysoft version of the function above with ints instead
 *  of shorts to cause a 4-byte pixel size
 *
 *--------------------------------------------------------------
 */
void YUVplanar_RGB32(unsigned char *src[], unsigned char *dst, int width, int height, int expand)
{
    int L, CR, CB;
    int x, y;
    int i, j;
    int cr_r;
    int cr_g;
    int cb_g;
    int cb_b;

    const int cols = width/expand/2;
    const int rows = height/expand/2;

    /* source pointers */
    unsigned char *lum1 = src[0];
    unsigned char *lum2 = src[0] + width/expand;
    unsigned char *cb   = src[1];
    unsigned char *cr   = src[2];

    /* destination pointers */
    unsigned int *row1 = (unsigned int *)dst;
    unsigned int *row2 = row1 + width*expand;

    for (y=0; y<rows; y++) {
        for (i=0; i<expand; i++) {
            for (x=0; x<cols; x++) {
                int R, G, B;

                /*
                 * Do first row.
                 */

                CR = *cr++;
                CB = *cb++;
                cr_r = Cr_r_tab[CR];
                cr_g = Cr_g_tab[CR];
                cb_g = Cb_g_tab[CB];
                cb_b = Cb_b_tab[CB];

                L = L_tab[(int) *lum1++];

                R = L + cr_r;
                G = L + cr_g + cb_g;
                B = L + cb_b;

                for (j=0; j<expand; j++)
                    *row1++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

#ifdef INTERPOLATE
                if(x != cols*expand - 1) {
                    CR = (CR + *cr) >> 1;
                    CB = (CB + *cb) >> 1;
                    cr_r = Cr_r_tab[CR];
                    cr_g = Cr_g_tab[CR];
                    cb_g = Cb_g_tab[CB];
                    cb_b = Cb_b_tab[CB];
                }
#endif

                L = L_tab[(int) *lum1++];

                R = L + cr_r;
                G = L + cr_g + cb_g;
                B = L + cb_b;

                for (j=0; j<expand; j++)
                    *row1++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

                /*
                 * Now, do second row.
                 */

#ifdef INTERPOLATE
                if(y != rows*expand - 1) {
                    CR = (CR + *(cr + cols_2 - 1)) >> 1;
                    CB = (CB + *(cb + cols_2 - 1)) >> 1;
                    cr_r = Cr_r_tab[CR];
                    cr_g = Cr_g_tab[CR];
                    cb_g = Cb_g_tab[CB];
                    cb_b = Cb_b_tab[CB];
                }
#endif

                L = L_tab [(int) *lum2++];
                R = L + cr_r;
                G = L + cr_g + cb_g;
                B = L + cb_b;

                for (j=0; j<expand; j++)
                    *row2++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

                L = L_tab [(int) *lum2++];
                R = L + cr_r;
                G = L + cr_g + cb_g;
                B = L + cb_b;

                for (j=0; j<expand; j++)
                    *row2++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);
            }

            /* repeat line in source */
            lum1 -= 2*cols;
            lum2 -= 2*cols;
            cb   -= cols;
            cr   -= cols;

        }

        /* go to next group of lines in destination */
        row1 += width*expand;
        row2 += width*expand;

        /* go to next line in source */
        lum1 += 4*cols;
        lum2 += 4*cols;
        cb   += cols;
        cr   += cols;
    }
}

/*
 * The following code was not written by Erik Corry, but by me (Ryan Dalzell)
 * It does however borrow heavily from Erik's code
 */

/*
 *--------------------------------------------------------------
 *
 * YUV422_RGB32
 *
 *  Converts YUV planar 4:2:2 into 32-bit packed RGB.
 *
 *--------------------------------------------------------------
 */
void YUV422_RGB32(unsigned char *src[], unsigned char *dst, int width, int height, int expand)
{
    int L, CR, CB;
    int x, y;
    int i, j;
    int cr_r;
    int cr_g;
    int cb_g;
    int cb_b;

    const int cols = width/expand/2;
    const int rows = height/expand;

    /* source pointers */
    unsigned char *lum = src[0];
    unsigned char *cb  = src[1];
    unsigned char *cr  = src[2];

    /* destination pointers */
    unsigned int *row = (unsigned int *)dst;

    for (y=0; y<rows; y++) {
        for (i=0; i<expand; i++) {
            for (x=0; x<cols; x++) {
                int R, G, B;

                /*
                 * Do first row.
                 */

                CR = *cr++;
                CB = *cb++;
                cr_r = Cr_r_tab[CR];
                cr_g = Cr_g_tab[CR];
                cb_g = Cb_g_tab[CB];
                cb_b = Cb_b_tab[CB];

                L = L_tab[(int) *lum++];

                R = L + cr_r;
                G = L + cr_g + cb_g;
                B = L + cb_b;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

#ifdef INTERPOLATE
                if(x != cols*expand - 1) {
                    CR = (CR + *cr) >> 1;
                    CB = (CB + *cb) >> 1;
                    cr_r = Cr_r_tab[CR];
                    cr_g = Cr_g_tab[CR];
                    cb_g = Cb_g_tab[CB];
                    cb_b = Cb_b_tab[CB];
                }
#endif

                L = L_tab[(int) *lum++];

                R = L + cr_r;
                G = L + cr_g + cb_g;
                B = L + cb_b;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

            }

            /* repeat line in source */
            lum -= 2*cols;
            cb  -= cols;
            cr  -= cols;

        }

        /* go to next line in source */
        lum += 2*cols;
        cb  += cols;
        cr  += cols;
    }
}

/*
 *--------------------------------------------------------------
 *
 * UYVY_RGB32
 *
 *  Converts YUV packed 4:2:2 into 32-bit packed RGB.
 *
 *--------------------------------------------------------------
 */
void UYVY_RGB32(unsigned char *src, unsigned char *dst, int width, int height, int expand)
{
    int y;
    int i, j;

    const int cols = width/expand;
    const int rows = height/expand;

    /* source pointer */
    unsigned char *p;

    /* destination pointer */
    unsigned int *row = (unsigned int *)dst;

    for (y=0; y<rows; y++) {
        for (i=0; i<expand; i++) {
            for (p=src+(y*cols*2); p<src+(y*cols*2)+cols*2; ) {
                int R, G, B;

                int CB = *p++;
                int L1 = *p++;
                int CR = *p++;
                int L2 = *p++;

                /* apply gamma correction */
                L1 = L_tab[L1];
                L2 = L_tab[L2];

                /* lookup chroma */
                int cr_r = Cr_r_tab[CR];
                int cr_g = Cr_g_tab[CR];
                int cb_g = Cb_g_tab[CB];
                int cb_b = Cb_b_tab[CB];

                /* convert first pixel */
                R = L1 + cr_r;
                G = L1 + cr_g + cb_g;
                B = L1 + cb_b;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

                /* convert second pixel */
                R = L2 + cr_r;
                G = L2 + cr_g + cb_g;
                B = L2 + cb_b;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

            }
        }
    }
}

/*
 *--------------------------------------------------------------
 *
 * YUY2_RGB32
 *
 *  Converts YUV packed 4:2:2 into 32-bit packed RGB.
 *
 *--------------------------------------------------------------
 */
void YUY2_RGB32(unsigned char *src, unsigned char *dst, int width, int height, int expand)
{
    int y;
    int i, j;

    const int cols = width/expand;
    const int rows = height/expand;

    /* source pointer */
    unsigned char *p;

    /* destination pointer */
    unsigned int *row = (unsigned int *)dst;

    for (y=0; y<rows; y++) {
        for (i=0; i<expand; i++) {
            for (p=src+(y*cols*2); p<src+(y*cols*2)+cols*2; ) {
                int R, G, B;

                int L1 = *p++;
                int CB = *p++;
                int L2 = *p++;
                int CR = *p++;

                /* apply gamma correction */
                L1 = L_tab[L1];
                L2 = L_tab[L2];

                /* lookup chroma */
                int cr_r = Cr_r_tab[CR];
                int cr_g = Cr_g_tab[CR];
                int cb_g = Cb_g_tab[CB];
                int cb_b = Cb_b_tab[CB];

                /* convert first pixel */
                R = L1 + cr_r;
                G = L1 + cr_g + cb_g;
                B = L1 + cb_b;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

                /* convert second pixel */
                R = L2 + cr_r;
                G = L2 + cr_g + cb_g;
                B = L2 + cb_b;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

            }
        }
    }
}

/*
 *--------------------------------------------------------------
 *
 * UYVY_RGB32
 *
 *  Converts YUV packed 4:2:2 into 32-bit packed RGB.
 *
 *--------------------------------------------------------------
 */
void VYUY_RGB32(unsigned char *src, unsigned char *dst, int width, int height, int expand)
{
    int y;
    int i, j;

    const int cols = width/expand;
    const int rows = height/expand;

    /* source pointer */
    unsigned char *p;

    /* destination pointer */
    unsigned int *row = (unsigned int *)dst;

    for (y=0; y<rows; y++) {
        for (i=0; i<expand; i++) {
            for (p=src+(y*cols*2); p<src+(y*cols*2)+cols*2; ) {
                int R, G, B;

                int CR = *p++;
                int L1 = *p++;
                int CB = *p++;
                int L2 = *p++;

                /* apply gamma correction */
                L1 = L_tab[L1];
                L2 = L_tab[L2];

                /* lookup chroma */
                int cr_r = Cr_r_tab[CR];
                int cr_g = Cr_g_tab[CR];
                int cb_g = Cb_g_tab[CB];
                int cb_b = Cb_b_tab[CB];

                /* convert first pixel */
                R = L1 + cr_r;
                G = L1 + cr_g + cb_g;
                B = L1 + cb_b;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

                /* convert second pixel */
                R = L2 + cr_r;
                G = L2 + cr_g + cb_g;
                B = L2 + cb_b;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

            }
        }
    }
}

/*
 *--------------------------------------------------------------
 *
 * YUVplanar444_RGB32
 *
 *  Converts YUV planar 4:4:4 into 32-bit packed RGB.
 *
 *--------------------------------------------------------------
 */
void YUVplanar444_RGB32(unsigned char *src[], unsigned char *dst, int width, int height, int expand, int bpp)
{
    int x, y;
    int i, j;

    const int cols = width/expand;
    const int rows = height/expand;
    const int pelsize = (bpp+7)/8;

    /* source pointers */
    unsigned char *lu = src[0];
    unsigned char *cb = src[1];
    unsigned char *cr = src[2];

    /* destination pointer */
    unsigned int *row = (unsigned int *)dst;

    for (y=0; y<rows; y++) {
        for (i=0; i<expand; i++) {
            for (x=0; x<cols; x++) {
                unsigned int LU, CB, CR;

                if (pelsize==1) {
                    LU = *lu++;
                    CB = *cb++;
                    CR = *cr++;
                } else if (pelsize==2) {
                    LU = *(unsigned short *)lu >> (bpp-8);
                    lu += 2;
                    CB = *(unsigned short *)cb >> (bpp-8);
                    cb += 2;
                    CR = *(unsigned short *)cr >> (bpp-8);
                    cr += 2;
                } else {
                    LU = 128;
                    CB = 128;
                    CR = 128;
                }

                /* apply gamma correction */
                LU = L_tab[LU];

                /* lookup chroma */
                int cr_r = Cr_r_tab[CR];
                int cr_g = Cr_g_tab[CR];
                int cb_g = Cb_g_tab[CB];
                int cb_b = Cb_b_tab[CB];

                /* convert to rgb */
                int R = LU + cr_r;
                int G = LU + cr_g + cb_g;
                int B = LU + cb_b;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);
            }
        }
    }
}

/*
 *--------------------------------------------------------------
 *
 * RGB24_RGB32
 *
 *  Converts 24-bit RGB into 32-bit RGB.
 *
 *--------------------------------------------------------------
 */
void RGB24_RGB32(unsigned char *src, unsigned char *dst, int width, int height, int expand, int bpp)
{
    int y;
    int i, j;

    const int cols = width/expand;
    const int rows = height/expand;
    const int pelsize = (bpp+7)/8;

    /* source pointer */
    unsigned char *p;

    /* destination pointer */
    unsigned int *row = (unsigned int *)dst;

    for (y=0; y<rows; y++) {
        for (i=0; i<expand; i++) {
            for (p=src+(y*cols*3*pelsize); p<src+(y+1)*cols*3*pelsize; ) {
                unsigned int R, G, B;

                if (pelsize==1) {
                    B = *p++;
                    G = *p++;
                    R = *p++;
                } else if (pelsize==2) {
                    B = *(unsigned short *)p >> (bpp-8);
                    p += 2;
                    G = *(unsigned short *)p >> (bpp-8);
                    p += 2;
                    R = *(unsigned short *)p >> (bpp-8);
                    p += 2;
                } else {
                    B = 255;
                    G = 0;
                    R = 0;
                }

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);
            }
        }
    }
}

/*
 *--------------------------------------------------------------
 *
 * RGBplanar_RGB32
 *
 *  Converts RGB planar 4:4:4 into 32-bit packed RGB.
 *
 *--------------------------------------------------------------
 */
void RGBplanar_RGB32(unsigned char *src[], unsigned char *dst, int width, int height, int expand, int bpp)
{
    int x, y;
    int i, j;

    const int cols = width/expand;
    const int rows = height/expand;
    const int pelsize = (bpp+7)/8;

    /* source pointers */
    unsigned char *r = src[0];
    unsigned char *g = src[1];
    unsigned char *b = src[2];

    /* destination pointer */
    unsigned int *row = (unsigned int *)dst;

    for (y=0; y<rows; y++) {
        for (i=0; i<expand; i++) {
            for (x=0; x<cols; x++) {
                unsigned int R, G, B;

                if (pelsize==1) {
                    R = *r++;
                    G = *g++;
                    B = *b++;
                } else if (pelsize==2) {
                    R = *(unsigned short *)r >> (bpp-8);
                    r += 2;
                    G = *(unsigned short *)g >> (bpp-8);
                    g += 2;
                    B = *(unsigned short *)b >> (bpp-8);
                    b += 2;
                } else {
                    R = 255;
                    G = 0;
                    B = 0;
                }

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);
            }
        }
    }
}

/*
 * --------------------------------------------------------------
 *
 * V210_RGB32
 *
 *  Converts YUV packed 4:2:2 10-bit into 32-bit packed RGB.
 *
 *--------------------------------------------------------------
 */
void V210_RGB32(unsigned char *src, unsigned char *dst, int width, int height, int expand)
{
    int x, y;
    int i, j;

    const int cols = width/expand;
    const int rows = height/expand;
    const int stride = 8*(((width+47)/48)*48)/3;

    for (y=0; y<rows; y++) {
        for (i=0; i<expand; i++) {
            for (x=0; x<cols; x+=6) {
                int R, G, B;
                int cr_r, cr_g, cb_g, cb_b;

                /* source pointer */
                unsigned char *p = src + (y*stride) + x*8/3;

                /* destination pointer */
                unsigned int *row = (unsigned int *)dst + y*cols*expand + x*expand;

                /* read four 32-bit blocks */
                unsigned int *block = (unsigned int *)p;
#if 1
                int V0 = (block[0]      ) & 0x3ff;
                int Y0 = (block[0] >> 10) & 0x3ff;
                int U0 = (block[0] >> 20) & 0x3ff;
                int Y1 = (block[1]      ) & 0x3ff;
                int V2 = (block[1] >> 10) & 0x3ff;
                int Y2 = (block[1] >> 20) & 0x3ff;
                int U2 = (block[2]      ) & 0x3ff;
                int Y3 = (block[2] >> 10) & 0x3ff;
                int V4 = (block[2] >> 20) & 0x3ff;
                int Y4 = (block[3]      ) & 0x3ff;
                int U4 = (block[3] >> 10) & 0x3ff;
                int Y5 = (block[3] >> 20) & 0x3ff;
#else
                int V0 = (block[0] >>  2) & 0xff;
                int Y0 = (block[0] >> 12) & 0xff;
                int U0 = (block[0] >> 22) & 0xff;
                int Y1 = (block[1] >>  2) & 0xff;
                int V2 = (block[1] >> 12) & 0xff;
                int Y2 = (block[1] >> 22) & 0xff;
                int U2 = (block[2] >>  2) & 0xff;
                int Y3 = (block[2] >> 12) & 0xff;
                int V4 = (block[2] >> 22) & 0xff;
                int Y4 = (block[3] >>  2) & 0xff;
                int U4 = (block[3] >> 12) & 0xff;
                int Y5 = (block[3] >> 22) & 0xff;
#endif

                /* apply gamma correction */
                Y0 = L_tab10[Y0];
                Y1 = L_tab10[Y1];
                Y2 = L_tab10[Y2];
                Y3 = L_tab10[Y3];
                Y4 = L_tab10[Y4];
                Y5 = L_tab10[Y5];

                /* lookup chroma */
                cr_r = Cr_r_tab10[U0];
                cr_g = Cr_g_tab10[U0];
                cb_g = Cb_g_tab10[V0];
                cb_b = Cb_b_tab10[V0];

                /* convert first pixel */
                R = Y0 + cr_r;
                G = Y0 + cr_g + cb_g;
                B = Y0 + cb_b;
                R >>= 2; G >>= 2; B >>= 2;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

                if (x+1==cols)
                    continue;

                /* convert second pixel */
                R = Y1 + cr_r;
                G = Y1 + cr_g + cb_g;
                B = Y1 + cb_b;
                R >>= 2; G >>= 2; B >>= 2;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

                if (x+2==cols)
                    continue;

                /* lookup chroma */
                cr_r = Cr_r_tab10[U2];
                cr_g = Cr_g_tab10[U2];
                cb_g = Cb_g_tab10[V2];
                cb_b = Cb_b_tab10[V2];

                /* convert third pixel */
                R = Y2 + cr_r;
                G = Y2 + cr_g + cb_g;
                B = Y2 + cb_b;
                R >>= 2; G >>= 2; B >>= 2;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

                if (x+3==cols)
                    continue;

                /* convert fourth pixel */
                R = Y3 + cr_r;
                G = Y3 + cr_g + cb_g;
                B = Y3 + cb_b;
                R >>= 2; G >>= 2; B >>= 2;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

                if (x+4==cols)
                    continue;

                /* lookup chroma */
                cr_r = Cr_r_tab10[U4];
                cr_g = Cr_g_tab10[U4];
                cb_g = Cb_g_tab10[V4];
                cb_b = Cb_b_tab10[V4];

                /* convert fifth pixel */
                R = Y4 + cr_r;
                G = Y4 + cr_g + cb_g;
                B = Y4 + cb_b;
                R >>= 2; G >>= 2; B >>= 2;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

                if (x+5==cols)
                    continue;

                /* convert sixth pixel */
                R = Y5 + cr_r;
                G = Y5 + cr_g + cb_g;
                B = Y5 + cb_b;
                R >>= 2; G >>= 2; B >>= 2;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

            }
        }
    }
}

/*
 * --------------------------------------------------------------
 *
 * YU15_RGB32
 *
 *  Converts YUV planar 4:2:0 10-bit into 32-bit packed RGB.
 *
 *--------------------------------------------------------------
 */
void YU15_RGB32(unsigned char *src[], unsigned char *dst, int width, int height, int expand)
{
    int L, CR, CB;
    int x, y;
    int i, j;
    int cr_r;
    int cr_g;
    int cb_g;
    int cb_b;

    const int cols = width/expand/2;
    const int rows = height/expand;

    /* source pointers */
    unsigned short *lum = (unsigned short *) src[0];
    unsigned short *cb  = (unsigned short *) src[1];
    unsigned short *cr  = (unsigned short *) src[2];

    /* destination pointers */
    unsigned int *row = (unsigned int *)dst;

    for (y=0; y<rows; y++) {
        for (i=0; i<expand; i++) {
            for (x=0; x<cols; x++) {
                int R, G, B;

                /*
                 * Do first row.
                 */

                CR = *cr++;
                CB = *cb++;
                cr_r = Cr_r_tab10[CR];
                cr_g = Cr_g_tab10[CR];
                cb_g = Cb_g_tab10[CB];
                cb_b = Cb_b_tab10[CB];

                L = L_tab10[*lum++];

                R = L + cr_r;
                G = L + cr_g + cb_g;
                B = L + cb_b;
                R >>= 2; G >>= 2; B >>= 2;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

#ifdef INTERPOLATE
                if(x != cols*expand - 1) {
                    CR = (CR + *cr) >> 1;
                    CB = (CB + *cb) >> 1;
                    cr_r = Cr_r_tab10[CR];
                    cr_g = Cr_g_tab10[CR];
                    cb_g = Cb_g_tab10[CB];
                    cb_b = Cb_b_tab10[CB];
                }
#endif

                L = L_tab10[*lum++];

                R = L + cr_r;
                G = L + cr_g + cb_g;
                B = L + cb_b;
                R >>= 2; G >>= 2; B >>= 2;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

            }

            /* repeat line in source */
            lum -= 2*cols;
            cb  -= cols;
            cr  -= cols;

        }

        /* go to next line in source */
        lum += 2*cols;
        if (y&1) {
            cb  += cols;
            cr  += cols;
        }
    }
}

/*
 * --------------------------------------------------------------
 *
 * YU20_RGB32
 *
 *  Converts YUV planar 4:2:2 10-bit into 32-bit packed RGB.
 *
 *--------------------------------------------------------------
 */
void YU20_RGB32(unsigned char *src[], unsigned char *dst, int width, int height, int expand)
{
    int L, CR, CB;
    int x, y;
    int i, j;
    int cr_r;
    int cr_g;
    int cb_g;
    int cb_b;

    const int cols = width/expand/2;
    const int rows = height/expand;

    /* source pointers */
    unsigned short *lum = (unsigned short *) src[0];
    unsigned short *cb  = (unsigned short *) src[1];
    unsigned short *cr  = (unsigned short *) src[2];

    /* destination pointers */
    unsigned int *row = (unsigned int *)dst;

    for (y=0; y<rows; y++) {
        for (i=0; i<expand; i++) {
            for (x=0; x<cols; x++) {
                int R, G, B;

                /*
                 * Do first row.
                 */

                CR = *cr++;
                CB = *cb++;
                cr_r = Cr_r_tab10[CR];
                cr_g = Cr_g_tab10[CR];
                cb_g = Cb_g_tab10[CB];
                cb_b = Cb_b_tab10[CB];

                L = L_tab10[*lum++];

                R = L + cr_r;
                G = L + cr_g + cb_g;
                B = L + cb_b;
                R >>= 2; G >>= 2; B >>= 2;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

#ifdef INTERPOLATE
                if(x != cols*expand - 1) {
                    CR = (CR + *cr) >> 1;
                    CB = (CB + *cb) >> 1;
                    cr_r = Cr_r_tab10[CR];
                    cr_g = Cr_g_tab10[CR];
                    cb_g = Cb_g_tab10[CB];
                    cb_b = Cb_b_tab10[CB];
                }
#endif

                L = L_tab10[*lum++];

                R = L + cr_r;
                G = L + cr_g + cb_g;
                B = L + cb_b;
                R >>= 2; G >>= 2; B >>= 2;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

            }

            /* repeat line in source */
            lum -= 2*cols;
            cb  -= cols;
            cr  -= cols;

        }

        /* go to next line in source */
        lum += 2*cols;
        cb  += cols;
        cr  += cols;
    }
}

/*
 *--------------------------------------------------------------
 *
 * NV12_RGB32
 *
 *  Converts NV12 planar 4:0:0 into 32-bit packed RGB.
 *
 *--------------------------------------------------------------
 */
void NV12_RGB32(unsigned char *src[], unsigned char *dst, int width, int height, int expand)
{
    int L, CR, CB;
    int x, y;
    int i, j;
    int cr_r;
    int cr_g;
    int cb_g;
    int cb_b;

    const int cols = width/expand/2;
    const int rows = height/expand;

    /* source pointers */
    unsigned char *lum = src[0];
    unsigned char *uv  = src[1];

    /* destination pointers */
    unsigned int *row = (unsigned int *)dst;

    for (y=0; y<rows; y++) {
        for (i=0; i<expand; i++) {
            for (x=0; x<cols; x++) {
                int R, G, B;

                /*
                 * Do first row.
                 */

                CB = *uv++;
                CR = *uv++;
                cr_r = Cr_r_tab[CR];
                cr_g = Cr_g_tab[CR];
                cb_g = Cb_g_tab[CB];
                cb_b = Cb_b_tab[CB];

                L = L_tab[(int) *lum++];

                R = L + cr_r;
                G = L + cr_g + cb_g;
                B = L + cb_b;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

#ifdef INTERPOLATE
                if(x != cols*expand - 1) {
                    CR = (CR + *cr) >> 1;
                    CB = (CB + *cb) >> 1;
                    cr_r = Cr_r_tab[CR];
                    cr_g = Cr_g_tab[CR];
                    cb_g = Cb_g_tab[CB];
                    cb_b = Cb_b_tab[CB];
                }
#endif

                L = L_tab[(int) *lum++];

                R = L + cr_r;
                G = L + cr_g + cb_g;
                B = L + cb_b;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

            }

            /* repeat line in source */
            lum -= 2*cols;
            uv  -= 2*cols;

        }

        /* go to next line in source */
        lum += 2*cols;
        if (y&1)
            uv += 2*cols;
    }
}

/*
 *--------------------------------------------------------------
 *
 * NV16_RGB32
 *
 *  Converts NV16 planar 4:2:2 into 32-bit packed RGB.
 *
 *--------------------------------------------------------------
 */
void NV16_RGB32(unsigned char *src[], unsigned char *dst, int width, int height, int expand)
{
    int L, CR, CB;
    int x, y;
    int i, j;
    int cr_r;
    int cr_g;
    int cb_g;
    int cb_b;

    const int cols = width/expand/2;
    const int rows = height/expand;

    /* source pointers */
    unsigned char *lum = src[0];
    unsigned char *uv  = src[1];

    /* destination pointers */
    unsigned int *row = (unsigned int *)dst;

    for (y=0; y<rows; y++) {
        for (i=0; i<expand; i++) {
            for (x=0; x<cols; x++) {
                int R, G, B;

                /*
                 * Do first row.
                 */

                CB = *uv++;
                CR = *uv++;
                cr_r = Cr_r_tab[CR];
                cr_g = Cr_g_tab[CR];
                cb_g = Cb_g_tab[CB];
                cb_b = Cb_b_tab[CB];

                L = L_tab[(int) *lum++];

                R = L + cr_r;
                G = L + cr_g + cb_g;
                B = L + cb_b;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

#ifdef INTERPOLATE
                if(x != cols*expand - 1) {
                    CR = (CR + *cr) >> 1;
                    CB = (CB + *cb) >> 1;
                    cr_r = Cr_r_tab[CR];
                    cr_g = Cr_g_tab[CR];
                    cb_g = Cb_g_tab[CB];
                    cb_b = Cb_b_tab[CB];
                }
#endif

                L = L_tab[(int) *lum++];

                R = L + cr_r;
                G = L + cr_g + cb_g;
                B = L + cb_b;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

            }

            /* repeat line in source */
            lum -= 2*cols;
            uv  -= 2*cols;

        }

        /* go to next line in source */
        lum += 2*cols;
        uv  += 2*cols;
    }
}

/*
 * --------------------------------------------------------------
 *
 * NV20_RGB32
 *
 *  Converts NV20 planar 4:2:2 10-bit into 32-bit packed RGB.
 *
 *--------------------------------------------------------------
 */
void NV20_RGB32(unsigned char *src[], unsigned char *dst, int width, int height, int expand)
{
    int L, CR, CB;
    int x, y;
    int i, j;
    int cr_r;
    int cr_g;
    int cb_g;
    int cb_b;

    const int cols = width/expand/2;
    const int rows = height/expand;

    /* source pointers */
    unsigned short *lum = (unsigned short *) src[0];
    unsigned short *uv  = (unsigned short *) src[1];

    /* destination pointers */
    unsigned int *row = (unsigned int *)dst;

    for (y=0; y<rows; y++) {
        for (i=0; i<expand; i++) {
            for (x=0; x<cols; x++) {
                int R, G, B;

                /*
                 * Do first row.
                 */

                CB = *uv++;
                CR = *uv++;
                cr_r = Cr_r_tab10[CR];
                cr_g = Cr_g_tab10[CR];
                cb_g = Cb_g_tab10[CB];
                cb_b = Cb_b_tab10[CB];

                L = L_tab10[*lum++];

                R = L + cr_r;
                G = L + cr_g + cb_g;
                B = L + cb_b;
                R >>= 2; G >>= 2; B >>= 2;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

#ifdef INTERPOLATE
                if(x != cols*expand - 1) {
                    CR = (CR + *cr) >> 1;
                    CB = (CB + *cb) >> 1;
                    cr_r = Cr_r_tab10[CR];
                    cr_g = Cr_g_tab10[CR];
                    cb_g = Cb_g_tab10[CB];
                    cb_b = Cb_b_tab10[CB];
                }
#endif

                L = L_tab10[*lum++];

                R = L + cr_r;
                G = L + cr_g + cb_g;
                B = L + cb_b;
                R >>= 2; G >>= 2; B >>= 2;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

            }

            /* repeat line in source */
            lum -= 2*cols;
            uv  -= 2*cols;

        }

        /* go to next line in source */
        lum += 2*cols;
        uv  += 2*cols;
    }
}

/*
 * --------------------------------------------------------------
 *
 * XV20_RGB32
 *
 *  Converts XV20 planar 4:2:2 10-bit packed into 32-bit words
 *    into 32-bit packed RGB.
 *
 *--------------------------------------------------------------
 */

/* unpack the xv20 32-bit word, each data plane has an associated index i */
int unpack_10le32(uint32_t *&data, int &i)
{
    int r = 0;
    switch (i) {
        case 0: r = (*data    ) & 0x3ff; i=1; break;
        case 1: r = (*data>>10) & 0x3ff; i=2; break;
        case 2: r = (*data>>20) & 0x3ff; i=0; data++; break;
    }
    return r;
}

void XV20_RGB32(unsigned char *src[], unsigned char *dst, int width, int height, int expand)
{
    int L, CR, CB;
    int x, y;
    int l, c;
    int i, j;
    int cr_r;
    int cr_g;
    int cb_g;
    int cb_b;

    const int cols = width/expand/2;
    const int rows = height/expand;

    /* source pointers */
    uint32_t *lum = (uint32_t *) src[0];
    uint32_t *uv  = (uint32_t *) src[0] + 4*((width+2)/3)*height/sizeof(uint32_t);

    /* destination pointers */
    unsigned int *row = (unsigned int *)dst;

    for (y=0; y<rows; y++) {
        for (i=0; i<expand; i++) {
            for (x=0, l=0, c=0; x<cols; x++) {
                int R, G, B;

                /*
                 * Do first row.
                 */

                CB = unpack_10le32(uv, c);
                CR = unpack_10le32(uv, c);
                cr_r = Cr_r_tab10[CR];
                cr_g = Cr_g_tab10[CR];
                cb_g = Cb_g_tab10[CB];
                cb_b = Cb_b_tab10[CB];

                //switch (l) {
                //    case 0: L = L_tab10[(*lum & 0x3ff)]; l=1; break;
                //    case 1: L = L_tab10[((*lum>>10) & 0x3ff)]; l=2; break;
                //    case 2: L = L_tab10[((*lum>>20) & 0x3ff)]; lum++; l=0; break;
                //}
                L = L_tab10[unpack_10le32(lum, l)];

                R = L + cr_r;
                G = L + cr_g + cb_g;
                B = L + cb_b;
                R >>= 2; G >>= 2; B >>= 2;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

#ifdef INTERPOLATE
                if(x != cols*expand - 1) {
                    CR = (CR + *cr) >> 1;
                    CB = (CB + *cb) >> 1;
                    cr_r = Cr_r_tab10[CR];
                    cr_g = Cr_g_tab10[CR];
                    cb_g = Cb_g_tab10[CB];
                    cb_b = Cb_b_tab10[CB];
                }
#endif

                //switch (l) {
                //    case 0: L = L_tab10[(*lum & 0x3ff)]; l=1; break;
                //    case 1: L = L_tab10[((*lum>>10) & 0x3ff)]; l=2; break;
                //    case 2: L = L_tab10[((*lum>>20) & 0x3ff)]; lum++; l=0; break;
                //}
                L = L_tab10[unpack_10le32(lum, l)];

                R = L + cr_r;
                G = L + cr_g + cb_g;
                B = L + cb_b;
                R >>= 2; G >>= 2; B >>= 2;

                for (j=0; j<expand; j++)
                    *row++ = (r_2_pix[R] | g_2_pix[G] | b_2_pix[B]);

            }

            /* repeat line in source */
            lum -= (2*cols)/4;
            uv  -= (2*cols)/4;

        }

        /* go to next line in source */
        lum += (2*cols)/4;
        uv  += (2*cols)/4;
    }
}
