/*
 * Description: Demultiplex video and audio container files.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-04-20 15:33:48 $
 * Revision   : $Revision: 1.10 $
 * Copyright  : (c) 2007 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <string.h>

#include "rvutil.h"
#include "rvmp2.h"

/*
 * timing functions
 */
double pts_to_sec(long long pts)
{
    return pts/90000.0;
}

double pts_to_ms(long long pts)
{
    return pts/90.0;
}

double pcr_to_sec(long long pcr)
{
    return pcr/27000000.0;
}

double pcr_to_ms(long long pcr)
{
    return pcr/27000.0;
}

double pcr_to_ns(long long pcr)
{
    return pcr*1000.0/27.0;
}

/*
 * transport stream functions
 */

int find_next_sync_code(struct bitbuf *bb)
{
    int leading = 0;
    while (showbits8(bb)!=0x47 && !eofbits(bb))
        leading += flushbits(bb, 8);
    if (eofbits(bb))
        return -1;

    return leading;
}

int read_transport_packet(struct bitbuf *bb, int verbose, unsigned char *packet)
{
    int i, numbits = find_next_sync_code(bb);
    if (numbits<0)
        return -1;
    if (numbits>0 && verbose>=1)
        rvmessage("searched %d bits (%d bytes) to find next transport packet", numbits, numbits/8);
    for (i=0; i<188; i++)
        packet[i] = getbits8(bb);
    if (eofbits(bb))
        return -1;
    return 0;
}

int parse_adaptation_field(struct bitbuf *bb, int verbose, int packnum, int *discontinuity_indicator, int *pcr_flag, long long *pcr)
{
    *discontinuity_indicator = 0;
    *pcr_flag = 0;
    *pcr = -1;

    int i;
    int adaptation_field_length = getbits8(bb);                         /* adaptation_field_length */
    if (adaptation_field_length>183) {
        if (verbose>=1)
            rvmessage("transport packet %d: adaptation field: adaptation_field_length is too large: %d", packnum, adaptation_field_length);
        return -1;
    }
    int header_length = adaptation_field_length + 1;
    if (adaptation_field_length > 0) {
        *discontinuity_indicator = getbit(bb);                          /* discontinuity_indicator */
        flushbits(bb, 1);                                               /* random_access_indicator */
        flushbits(bb, 1);                                               /* elementary_stream_priority_indicator */
        *pcr_flag = getbit(bb);                                         /* PCR_flag */
        int OPCR_flag = getbit(bb);                                     /* OPCR_flag */
        int splicing_point_flag = getbit(bb);                           /* splicing_point_flag */
        int transport_private_data_flag = getbit(bb);                   /* transport_private_data_flag */
        int adaptation_field_extension_flag = getbit(bb);               /* adaptation_field_extension_flag */
        adaptation_field_length -= 1;
        if (*pcr_flag) {
            long long pcr_base = llgetbits(bb, 33);                     /* program_clock_reference_base */
            flushbits(bb, 6);                                           /* reserved */
            long long pcr_ext = getbits(bb, 9);                         /* program_clock_reference_extension */
            if (pcr)
                *pcr = pcr_base*300 + pcr_ext;
            adaptation_field_length -= 6;
        }
        if (OPCR_flag) {
            flushbits(bb, 33);                                          /* original_program_clock_reference_base */
            flushbits(bb, 6);                                           /* reserved */
            flushbits(bb, 9);                                           /* original_program_clock_reference_extension */
            adaptation_field_length -= 6;
        }
        if (splicing_point_flag) {
            flushbits(bb, 8);                                           /* splice_countdown */
            adaptation_field_length -= 1;
        }
        if (transport_private_data_flag) {
            int transport_private_data_length = getbits8(bb);           /* transport_private_data_length */
            if (transport_private_data_length>adaptation_field_length) {
                if (verbose>=1)
                    rvmessage("transport packet %d: adaptation field: transport_private_data_length is too large: %d > %d", packnum, transport_private_data_length, adaptation_field_length);
                return -1;
            }
            adaptation_field_length -= transport_private_data_length+1;
            for (i=0; i<transport_private_data_length; i++)
                flushbits(bb, 8);                                       /* private_data_byte */
        }
        if (adaptation_field_extension_flag) {
            int adaptation_field_extension_length = getbits8(bb);       /* adaptation_field_extension_length */
            if (adaptation_field_extension_length>adaptation_field_length) {
                if (verbose>=1)
                    rvmessage("transport packet %d: adaptation field: adaptation_field_extension_length is too large: %d > %d", packnum, adaptation_field_extension_length, adaptation_field_length);
                return -1;
            }
            adaptation_field_length -= adaptation_field_extension_length+1;
            int ltw_flag = getbit(bb);                                  /* ltw_flag             */
            int piecewise_rate_flag = getbit(bb);                       /* piecewise_rate_flag */
            int seamless_splice_flag = getbit(bb);                      /* seamless_splice_flag */
            flushbits(bb, 5);                                           /* reserved */
            adaptation_field_extension_length -= 1;
            if (ltw_flag) {
                flushbits(bb, 1);                                       /* ltw_valid_flag */
                flushbits(bb, 15);                                      /* ltw_offset */
                adaptation_field_extension_length -= 2;
            }
            if (piecewise_rate_flag) {
                flushbits(bb, 2);                                       /* reserved */
                flushbits(bb, 22);                                      /* piecewise_rate */
                adaptation_field_extension_length -= 3;
            }
            if (seamless_splice_flag){
                flushbits(bb, 4);                                       /* splice_type */
                flushbits(bb, 3);                                       /* DTS_next_AU[32..30] */
                flushbits(bb, 1);                                       /* marker_bit */
                flushbits(bb, 15);                                      /* DTS_next_AU[29..15] */
                flushbits(bb, 1);                                       /* marker_bit */
                flushbits(bb, 15);                                      /* DTS_next_AU[14..0] */
                flushbits(bb, 1);                                       /* marker_bit */
                adaptation_field_extension_length -= 5;
            }
            if (adaptation_field_extension_length<0) {
                if (verbose>=1)
                    rvmessage("transport packet %d: adaptation field: adaption field did not parse correctly: adaptation_field_extension_length=%d", packnum, adaptation_field_extension_length);
                return -1;
            }
            while (adaptation_field_extension_length-- && !eofbits(bb)) {
                flushbits(bb, 8);                                       /* reserved */
            }
        }
        if (adaptation_field_length<0)
            rvmessage("transport packet %d: adaptation field: adaption field did not parse correctly: adaptation_field_length=%d", packnum, adaptation_field_length);
        else while (adaptation_field_length--) {
            int stuffing_byte = getbits8(bb);                           /* stuffing_byte */
            if (verbose>=2)
                if (stuffing_byte!=0xff)
                    rvmessage("transport packet %d: adaptation field: invalid value of stuffing_byte: %d", packnum, stuffing_byte);
        }
    }
    return header_length;
}

int parse_transport_packet_header(struct bitbuf *bb, int verbose, int packnum, int *transport_error_indicator,
                                  int *payload_unit_start_indicator, int *pid, int *adaptation_field_control, int *continuity_counter,
                                  int *discontinuity_indicator, int *pcr_flag, long long *pcr)
{
    flushbits(bb, 8);                                       /* sync_byte */
    *transport_error_indicator = getbit(bb);                /* transport_error_indicator */
    *payload_unit_start_indicator = getbit(bb);             /* payload_unit_start_indicator */
    flushbits(bb, 1);                                       /* transport_priority */
    *pid = getbits(bb, 13);                                 /* PID */
    if (*pid>=0x2 && *pid<=0xf) {
        if (verbose>=1)
            rvmessage("transport packet %d: reserved value of pid: %d", packnum, *pid);
        return -1;
    }
    flushbits(bb, 2);                                       /* transport_scrambling_control */
    *adaptation_field_control = getbits(bb, 2);             /* adaptation_field_control */
    if (*adaptation_field_control==0) {
        if (verbose>=1)
            rvmessage("transport packet %d: reserved value of adaptation_field_control: 0", packnum);
        return -1;
    }
    *continuity_counter = getbits(bb, 4);                   /* continuity_counter */
    if (verbose>=3)
        rvmessage("transport packet %d: pid %d, %s%scontinuity %d", packnum, *pid, *payload_unit_start_indicator? "start indicator, " : "",
                  *adaptation_field_control==1? "" : *adaptation_field_control==2? "just adaptation field, " : "adaptation field, ", *continuity_counter);
    if (*adaptation_field_control==2 || *adaptation_field_control==3) {
        int adaptation_field_length = parse_adaptation_field(bb, verbose, packnum, discontinuity_indicator, pcr_flag, pcr);
        if (adaptation_field_length<0)
            return -1;
        else
            return 4 + adaptation_field_length;
    } else {
        *discontinuity_indicator = 0;
        *pcr_flag = 0;
    }
    return 4;
}

int parse_pointer_field(struct bitbuf *bb)
{
    int pointer_field = getbits8(bb);
    int i;
    for (i=0; i<pointer_field; i++)
        flushbits(bb, 8);
    return pointer_field+1;
}

void describe_program_association(struct program_association_table *pat)
{
    int i;
    rvmessage("+-------+-------+-----+-----------------+");
    rvmessage("|pat v%-2d|program| pid | id %-5d 0x%-4x |", pat->version_number, pat->transport_stream_id, pat->transport_stream_id);
    rvmessage("+-------+-------+-----+-----------------+");
    for (i=0; i<pat->number_programs; i++)
        rvmessage("|       |% 7d|% 5d|", pat->program[i].program_number, pat->program[i].program_map_pid);
    rvmessage("+-------+-------+-----+");
}

int parse_program_association_section(struct bitbuf *bb, int data_length, int verbose, struct program_association_table *pat)
{
    int i, n;
    flushbits(bb, 8);                                       /* table_id */
    flushbits(bb, 1);                                       /* section_syntax_indicator */
    flushbits(bb, 1);                                       /* '0' */
    flushbits(bb, 2);                                       /* reserved */
    int section_length = getbits(bb, 12);                   /* section_length */
    if (section_length>1021) {
        rvmessage("pat: section_length is too large: %d", section_length);
        return -1;
    }
    if (verbose>=3)
        rvmessage("pat: section_length=%d data_length=%d", section_length, data_length);
    if (section_length>data_length)
        /* defer parsing until more data is available */
        return 0;
    pat->transport_stream_id = getbits(bb, 16);             /* transport_stream_id */
    flushbits(bb, 2);                                       /* reserved */
    pat->version_number = getbits(bb, 5);                   /* version_number */
    flushbits(bb, 1);                                       /* current_next_indicator */
    flushbits(bb, 8);                                       /* section_number */
    flushbits(bb, 8);                                       /* last_section_number */
    for (i=5, n=0; i<section_length-4; i+=4, n++) {
        pat->program[n].program_number = getbits(bb, 16);   /* program_number */
        flushbits(bb, 3);                                   /* reserved */
        pat->program[n].program_map_pid = getbits(bb, 13);  /* network_PID / program_map_PID */
    }
    pat->number_programs = n;
    pat->crc32 = getbits32(bb);                             /* CRC_32 */
    return section_length+3;
}

int parse_video_stream_descriptor(struct bitbuf *bb)
{
    flushbits(bb, 1);                                       /* multiple_frame_rate_flag */
    flushbits(bb, 4);                                       /* frame_rate_code */
    int MPEG_1_only_flag = getbits(bb, 1);                  /* MPEG_1_only_flag */
    flushbits(bb, 1);                                       /* constrained_parameter_flag */
    flushbits(bb, 1);                                       /* still_picture_flag */
    if (MPEG_1_only_flag == 0) {
        flushbits(bb, 8);                                   /* profile_and_level_indication */
        flushbits(bb, 2);                                   /* chroma_format */
        flushbits(bb, 1);                                   /* frame_rate_extension_flag */
        flushbits(bb, 5);                                   /* reserved */
    }
    return MPEG_1_only_flag? 1 : 3;
}

int parse_descriptor(struct bitbuf *bb, struct pmt_entry *pmt, const char *where)
{
    int i;
    int descriptor_tag = getbits8(bb);                      /* descriptor_tag */
    int descriptor_length = getbits8(bb);                   /* descriptor_length */
    switch (descriptor_tag) {
        case 2:
            if (parse_video_stream_descriptor(bb)!=descriptor_length)
                rvmessage("%s: video stream descriptor is not %d bytes", where, descriptor_length);
            break;

        case 5: {
            int format_identifier = getbits(bb, 32);                /* format_identifier */
            flushbits(bb, descriptor_length*8-32);
            //rvmessage("%s: registration descriptor format_identifier is 0x%x", where, format_identifier);
            if (pmt)
                pmt->format_identifier = format_identifier;
            break;
        }

        default:
            for (i=0; i<descriptor_length; i++)
                flushbits(bb, 8);
            break;
    }
    return descriptor_length+2;
}

/* short name for stream type */
const char *name_stream_type(stream_type_t stream_type, fourcc_t format_identifier)
{
    switch (stream_type) {
        case 0x00 : return ""; break;
        case 0x01 : return "mpeg1"; break;
        case 0x02 : return "mpeg2"; break;
        case 0x03 : return "mp3"; break;
        case 0x04 : return "m2a"; break;
        case 0x06 : {
            switch (format_identifier) {
                case 0x41432D33: return "ac-3"; break;
                case 0x42535344: return "pcm"; break;
                case 0x47413934: return "a/53"; break;
                case 0x49443320: return "id3"; break;
                case 0x4b4c5641: return "klv"; break;
                case 0x4c495053: return "st2064"; break;
                case 0x6d6c7061: return "truehd"; break;
                case 0x4d54524d: return "d-vhs"; break;
                case 0x4f707573: return "opus"; break;
                case 0x53504c43: return "312m"; break;
                case 0x56432d31: return "vc-1"; break;
                case 0x56432d34: return "vc-4"; break;
                case 0x56414e43: return "vanc"; break; /* not on smpte registry */
                case 0x00000000: return "priv"; break;
                default        : return "????"; break;
            }
        }
        case 0x07 : return "mheg"; break;
        case 0x09 : return "h.222.1"; break;
        case 0x0a : return "type A"; break;
        case 0x0b : return "type B"; break;
        case 0x0c : return "type C"; break;
        case 0x0d : return "type D"; break;
        case 0x0f : return "aac"; break;
        case 0x10 : return "mpeg4"; break;
        case 0x11 : return "loas"; break;
        case 0x1b : return "avc"; break;
        case 0x1f : return "svc"; break;
        case 0x21 : return "jpeg2k"; break;
        case 0x24 : return "hevc"; break;
        case 0x81 : return "ac3"; break;
        case 0x8a : return "dts"; break;
        case 0xd1 : return "dirac"; break;
        case 0xea : return "vc1"; break;
        case 0xff : return "mixed"; break;      // metatype for mixed video or mixed audio transport streams.
        default   : return "unknown";  break;
    }
}

/* longer description for stream type */
const char *describe_stream_type(stream_type_t stream_type, int format_identifier)
{
    switch (stream_type) {
        case 0x00 : return "reserved"; break;
        case 0x01 : return "mpeg1 video"; break;
        case 0x02 : return "mpeg2 video"; break;
        case 0x03 : return "mpeg1 audio"; break;
        case 0x04 : return "mpeg2 audio"; break;
        case 0x05 : return "mpeg2 private sections"; break;
        case 0x06 : {
            switch (format_identifier) {
                case 0x41432D33: return "atsc ac-3 audio"; break;
                case 0x42535344: return "smpte s302m pcm audio"; break;
                case 0x47413934: return "atsc a/53"; break;
                case 0x49443320: return "id3 data"; break;
                case 0x4b4c5641: return "smpte 271 klv data"; break;
                case 0x4c495053: return "smpte st 2064"; break;
                case 0x6d6c7061: return "dolby truehd audio"; break;
                case 0x4d54524d: return "d-vhs"; break;
                case 0x4f707573: return "opus audio"; break;
                case 0x53504c43: return "smpte 312m data"; break;
                case 0x56432d31: return "vc-1 video"; break;
                case 0x56432d34: return "vc-4 video"; break;
                case 0x56414e43: return "vanc data"; break; /* not on smpte registry */
                case 0x00000000: return "mpeg2 private data"; break;
                /* TODO fourcc translation for unknown identifiers */
                default        : {
                    rvmessage("unknown mpeg2 private data format identifier: 0x%08x", format_identifier);
                    return "mpeg2 private data"; break;
                }
            }
        }
        case 0x07 : return "mheg data"; break;
        case 0x08 : return "mpeg2 dsm cc"; break;
        case 0x09 : return "h.222.1"; break;
        case 0x0a : return "DSM CC multiprotocol encapsulation"; break;
        case 0x0b : return "DSM CC U-N messages"; break;
        case 0x0c : return "DSM CC stream descriptors"; break;
        case 0x0d : return "DSM CC tabled data "; break;
        case 0x0e : return "mpeg2 auxiliary"; break;
        case 0x0f : return "mpeg2 aac audio"; break;
        case 0x10 : return "mpeg4 h.263 video"; break;
        case 0x11 : return "mpeg4 loas audio"; break;
        case 0x12 : return "mpeg4 systems in pes packets"; break;
        case 0x13 : return "mpeg4 systems in sections"; break;
        case 0x14 : return "mpeg2 synchronised download protocol"; break;
        case 0x15 : return "packetised metadata"; break;
        case 0x16 : return "sectioned metadata"; break;
        case 0x1a : return "ipmp data"; break;
        case 0x1b : return "h.264 video"; break;
        case 0x1c : return "mpeg4 raw audio"; break;
        case 0x1d : return "mpeg4 text"; break;
        case 0x1e : return "mpeg4 auxiliary video"; break;
        case 0x1f : return "h.264 scalable video"; break;
        case 0x20 : return "h.264 multiview video"; break;
        case 0x21 : return "jpeg 2000"; break;
        case 0x24 : return "hevc video"; break;
        case 0x42 : return "avs video"; break;
        case 0x7f : return "ipmp drm data"; break;
        case 0x81 : return "ac3 audio"; break;
        case 0x82 : return "dts 6 channel audio"; break;
        case 0x83 : return "dolby truehd lossless audio"; break;
        case 0x84 : return "dolby digital plus audio"; break;
        case 0x85 : return "dts 8 channel audio"; break;
        case 0x86 : return "dts 8 channel lossless audio"; break;
        case 0x87 : return "dolby digital plus audio"; break;
        case 0x8a : return "dts audio"; break;
        case 0xc1 : return "ac-3 audio with aes-128-cbc"; break;
        case 0xc2 : return "dolby digital plus with aes-128-cbc"; break;
        case 0xcf : return "aac audio with aes-128-cbc"; break;
        case 0xd1 : return "dirac video"; break;
        case 0xdb : return "h.264 video with aes-128-cbc"; break;
        case 0xea : return "vc1 video"; break;
        case 0x100: return "dvb subtitle"; break;
        default   : return "unknown";  break;
    }
}

void describe_program_map(struct program_map_table *pmt)
{
    int i;
    rvmessage("+-------+-----+---+------+------------------------------------+");
    rvmessage("|pmt v%-2d|  program %-5d | pcr_pid=%-4d                       |", pmt->version_number, pmt->program_number, pmt->pcr_pid);
    rvmessage("+-------+-----+---+------+------------------------------------+");
    rvmessage("|       | pid |pcr|stream|              description           |");
    rvmessage("+-------+-----+---+------+------------------------------------+");
    for (i=0; i<pmt->number_streams; i++) {
        rvmessage("|       |% 5d| %1c |% 6d|%36s|", pmt->stream[i].elementary_pid, pmt->stream[i].elementary_pid==pmt->pcr_pid? 'y': ' ', pmt->stream[i].stream_type, describe_stream_type(pmt->stream[i].stream_type, pmt->stream[i].format_identifier));
    }
    rvmessage("+-------+-----+---+------+------------------------------------+");
}

int parse_program_map_section(struct bitbuf *bb, int data_length, int verbose, struct program_map_table *pmt)
{
    int i, j, n;
    flushbits(bb, 8);                                       /* table_id */
    flushbits(bb, 1);                                       /* section_syntax_indicator */
    flushbits(bb, 1);                                       /* '0' */
    flushbits(bb, 2);                                       /* reserved */
    int section_length = getbits(bb, 12);                   /* section_length */
    if (section_length>1021) {
        if (verbose>=1)
            rvmessage("pmt: section_length is too large: %d", section_length);
        return -1;
    }
    if (verbose>=3)
        rvmessage("pmt: section_length=%d data_length=%d", section_length, data_length);
    if (section_length>data_length)
        /* defer parsing until more data is available */
        return 0;
    pmt->program_number = getbits(bb, 16);                  /* program_number */
    flushbits(bb, 2);                                       /* reserved */
    pmt->version_number = getbits(bb, 5);                   /* version_number */
    flushbits(bb, 1);                                       /* current_next_indicator */
    flushbits(bb, 8);                                       /* section_number */
    flushbits(bb, 8);                                       /* last_section_number */
    flushbits(bb, 3);                                       /* reserved */
    pmt->pcr_pid = getbits(bb, 13);                         /* PCR_PID */
    flushbits(bb, 4);                                       /* reserved */
    int program_info_length = getbits(bb, 12);              /* program_info_length */
    if (program_info_length>section_length-9) {
        if (verbose>=1)
            rvmessage("pmt: program_info_length is too large: %d", program_info_length);
        return -1;
    }
    for (i=0; i<program_info_length; )
        i += parse_descriptor(bb, NULL, "pmt");
    for (i=9+program_info_length, n=0; i<section_length-4; i+=5, n++) {
        pmt->stream[n].stream_type = getbits8(bb);          /* stream_type */
        flushbits(bb, 3);                                   /* reserved */
        pmt->stream[n].elementary_pid = getbits(bb, 13);    /* elementary_PID */
        flushbits(bb, 4);                                   /* reserved */
        int es_info_length = getbits(bb, 12);               /* ES_info_length */
        if (es_info_length>section_length-4-i) {
            if (verbose>=1)
                rvmessage("pmt: es_info_length is too large compared to %d: %d", section_length-4-i, es_info_length);
            return -1;
        }
        for (j=0; j<es_info_length; )
            j += parse_descriptor(bb, &pmt->stream[n], "pmt");
        i += es_info_length;
    }
    pmt->crc32 = getbits32(bb);                             /* CRC_32 */
    pmt->number_streams = n;
    return section_length+3;
}

/*
 * packetised elementary stream functions
 */

int parse_pes_packet_header(struct bitbuf *bb, int stream_id, int PES_packet_length, long long *pts, long long *dts, int verbose)
{
    int i;
    if (pts)
        *pts = -1ll;
    if (dts)
        *dts = -1ll;
    /* remember position in stream */
    if( stream_id != PROGRAM_STREAM_MAP
     && stream_id != PADDING_STREAM
     && stream_id != PRIVATE_STREAM_2
     && stream_id != ECM_STREAM
     && stream_id != EMM_STREAM
     && stream_id != PROGRAM_STREAM_DIRECTORY
     && stream_id != DSMCC_STREAM
     && stream_id != TYPE_E_STREAM) {
        flushbits(bb, 2);                                       /* '10' */
        flushbits(bb, 2);                                       /* PES_scrambling_control */
        flushbits(bb, 1);                                       /* PES_priority */
        flushbits(bb, 1);                                       /* data_alignment_indicator */
        flushbits(bb, 1);                                       /* copyright */
        flushbits(bb, 1);                                       /* original_or_copy */
        int PTS_DTS_flags = getbits(bb, 2);                     /* PTS_DTS_flags */
        int ESCR_flag = getbits(bb, 1);                         /* ESCR_flag */
        int ES_rate_flag = getbits(bb, 1);                      /* ES_rate_flag */
        int DSM_trick_mode_flag = getbits(bb, 1);               /* DSM_trick_mode_flag */
        int additional_copy_info_flag = getbits(bb, 1);         /* additional_copy_info_flag */
        int PES_CRC_flag = getbits(bb, 1);                      /* PES_CRC_flag */
        int PES_extension_flag = getbits(bb, 1);                /* PES_extension_flag */
        /*if (verbose>=2) {
            char string[256], *p=string;
            *p = '\0';
            if (PTS_DTS_flags==2)
                p += snprintf(p, sizeof(string)-(p-string), " PTS");
            else if (PTS_DTS_flags==3)
                p += snprintf(p, sizeof(string)-(p-string), " PTS_DTS");
            if (ESCR_flag)
                p += snprintf(p, sizeof(string)-(p-string), " ESCR");
            if (ES_rate_flag)
                p += snprintf(p, sizeof(string)-(p-string), " ES_rate");
            if (DSM_trick_mode_flag)
                p += snprintf(p, sizeof(string)-(p-string), " DSM_trick_mode");
            if (additional_copy_info_flag)
                p += snprintf(p, sizeof(string)-(p-string), " additional_copy_info");
            if (PES_CRC_flag)
                p += snprintf(p, sizeof(string)-(p-string), " PES_CRC");
            if (PES_extension_flag)
                p += snprintf(p, sizeof(string)-(p-string), " PES_extension");
            rvmessage("pes packet flags: %s", string);
        }*/
        int PES_header_data_length = getbits8(bb);              /* PES_header_data_length */
        PES_packet_length -= 3;
        if (PTS_DTS_flags==2) {
            long long ts;
            flushbits(bb, 4);                                   /* '0010' */
            ts = getbits(bb, 3);                                /* PTS[32..30] */
            ts <<= 15;
            flushbits(bb, 1);                                   /* marker_bit */
            ts |= getbits(bb, 15);                              /* PTS[29..15] */
            ts <<= 15;
            flushbits(bb, 1);                                   /* marker_bit */
            ts |= getbits(bb, 15);                              /* PTS[14..0] */
            flushbits(bb, 1);                                   /* marker_bit */
            if (pts)
                *pts = ts;
            if (dts)
                *dts = ts;
            PES_packet_length -= 5;
            PES_header_data_length -= 5;
        }
        if (PTS_DTS_flags==3)  {
            long long ts;
            flushbits(bb, 4);                                   /* '0011' */
            ts = getbits(bb, 3);                                /* PTS[32..30] */
            ts <<= 15;
            flushbits(bb, 1);                                   /* marker_bit */
            ts |= getbits(bb, 15);                              /* PTS[29..15] */
            ts <<= 15;
            flushbits(bb, 1);                                   /* marker_bit */
            ts |= getbits(bb, 15);                              /* PTS[14..0] */
            flushbits(bb, 1);                                   /* marker_bit */
            if (pts)
                *pts = ts;
            flushbits(bb, 4);                                   /* '0001' */
            ts = getbits(bb, 3);                                /* DTS[32..30] */
            ts <<= 15;
            flushbits(bb, 1);                                   /* marker_bit */
            ts |= getbits(bb, 15);                              /* DTS[29..15] */
            ts <<= 15;
            flushbits(bb, 1);                                   /* marker_bit */
            ts |= getbits(bb, 15);                              /* DTS[14..0] */
            flushbits(bb, 1);                                   /* marker_bit */
            if (dts)
                *dts = ts;
            PES_packet_length -= 10;
            PES_header_data_length -= 10;
        }
        if (ESCR_flag==1) {
            flushbits(bb, 2);                                   /* reserved */
            flushbits(bb, 3);                                   /* ESCR_base[32..30] */
            flushbits(bb, 1);                                   /* marker_bit */
            flushbits(bb, 15);                                  /* ESCR_base[29..15] */
            flushbits(bb, 1);                                   /* marker_bit */
            flushbits(bb, 15);                                  /* ESCR_base[14..0] */
            flushbits(bb, 1);                                   /* marker_bit */
            flushbits(bb, 9);                                   /* ESCR_extension */
            flushbits(bb, 1);                                   /* marker_bit */
            PES_packet_length -= 6;
            PES_header_data_length -= 6;
        }
        if (ES_rate_flag==1) {
            flushbits(bb, 1);                                   /* marker_bit */
            flushbits(bb, 22);                                  /* ES_rate */
            flushbits(bb, 1);                                   /* marker_bit */
            PES_packet_length -= 3;
            PES_header_data_length -= 3;
        }
        if (DSM_trick_mode_flag==1) {
            int trick_mode_control = getbits(bb, 3);            /* trick_mode_control */
            if ( trick_mode_control == FAST_FORWARD ) {
                flushbits(bb, 2);                               /* field_id */
                flushbits(bb, 1);                               /* intra_slice_refresh */
                flushbits(bb, 2);                               /* frequency_truncation */
            } else if ( trick_mode_control == SLOW_MOTION ) {
                flushbits(bb, 5);                               /* rep_cntrl */
            } else if ( trick_mode_control == FREEZE_FRAME) {
                flushbits(bb, 2);                               /* field_id */
                flushbits(bb, 3);                               /* reserved */
            } else if ( trick_mode_control == FAST_REVERSE ) {
                flushbits(bb, 2);                               /* field_id */
                flushbits(bb, 1);                               /* intra_slice_refresh */
                flushbits(bb, 2);                               /* frequency_truncation */
            } else if ( trick_mode_control == SLOW_REVERSE ) {
                flushbits(bb, 5);                               /* rep_cntrl */
            } else
                flushbits(bb, 5);                               /* reserved */
            PES_packet_length -= 1;
            PES_header_data_length -= 1;
        }
        if (additional_copy_info_flag==1) {
            flushbits(bb, 1);                                   /* marker_bit */
            flushbits(bb, 7);                                   /* additional_copy_info */
            PES_packet_length -= 1;
            PES_header_data_length -= 1;
        }
        if (PES_CRC_flag==1) {
            flushbits(bb, 16);                                  /* previous_PES_packet_CRC */
            PES_packet_length -= 2;
            PES_header_data_length -= 2;
        }
        if (PES_extension_flag==1) {
            int PES_private_data_flag = getbits(bb, 1);         /* PES_private_data_flag */
            int pack_header_field_flag = getbits(bb, 1);        /* pack_header_field_flag */
            int program_packet_sequence_counter_flag = getbits(bb, 1);/* program_packet_sequence_counter_flag */
            int P_STD_buffer_flag = getbits(bb, 1);             /* P-STD_buffer_flag */
            flushbits(bb, 3);                                   /* reserved */
            int PES_extension_flag_2 = getbits(bb, 1);          /* PES_extension_flag_2 */
            PES_packet_length -= 1;
            PES_header_data_length -= 1;
            if (PES_private_data_flag == 1) {
                for (i=0; i<16; i++)
                    flushbits(bb, 8);                           /* PES_private_data */
                PES_packet_length -= 16;
                PES_header_data_length -= 16;
            }
            if (pack_header_field_flag==1) {
                flushbits(bb, 8);                               /* pack_field_length */
                flushbits(bb, 32);                              /* pack_start_code */
                PES_packet_length -= 5;
                PES_header_data_length -= 5;
                int pack_header_length = parse_pack_header(bb, verbose);
                PES_packet_length -= pack_header_length;
                PES_header_data_length -= pack_header_length;
            }
            if (program_packet_sequence_counter_flag==1) {
                flushbits(bb, 1);                               /* marker_bit */
                flushbits(bb, 7);                               /* program_packet_sequence_counter */
                flushbits(bb, 1);                               /* marker_bit */
                flushbits(bb, 1);                               /* MPEG1_MPEG2_identifier */
                flushbits(bb, 6);                               /* original_stuff_length */
                PES_packet_length -= 2;
                PES_header_data_length -= 2;
            }
            if (P_STD_buffer_flag==1) {
                flushbits(bb, 2);                               /* '01' */
                flushbits(bb, 1);                               /* P-STD_buffer_scale */
                flushbits(bb, 13);                              /* P-STD_buffer_size */
                PES_packet_length -= 2;
                PES_header_data_length -= 2;
            }
            if (PES_extension_flag_2==1) {
                flushbits(bb, 1);                               /* marker_bit */
                int PES_extension_field_length = getbits(bb, 7);/* PES_extension_field_length */
                for (i=0; i<PES_extension_field_length; i++) {
                    flushbits(bb, 8);                           /* reserved */
                }
                PES_packet_length -= PES_extension_field_length+1;
                PES_header_data_length -= PES_extension_field_length+1;
            }
        }
        for (i=0; i<PES_header_data_length; i++) {
            flushbits(bb, 8);                                   /* stuffing_byte */
        }
        PES_packet_length -= PES_header_data_length;
    }

    else if ( stream_id == PROGRAM_STREAM_MAP
           || stream_id == PRIVATE_STREAM_2
           || stream_id == ECM_STREAM
           || stream_id == EMM_STREAM
           || stream_id == PROGRAM_STREAM_DIRECTORY
           || stream_id == DSMCC_STREAM
           || stream_id == TYPE_E_STREAM ) {
        for (i=0; i<PES_packet_length; i++) {
            flushbits(bb, 8);                                   /* PES_packet_data_byte */
        }
        PES_packet_length = 0;
    }
    else if (stream_id == PADDING_STREAM) {
        for (i=0; i<PES_packet_length; i++) {
            flushbits(bb, 8);                                   /* padding_byte */
        }
        PES_packet_length = 0;
    }
    return PES_packet_length;
}

void describe_pes_packet(int stream_id, int pes_packet_length)
{
    char string[64];
    if (stream_id==PROGRAM_STREAM_MAP)
        strncpy(string, "program stream map", sizeof(string));
    else if (stream_id==PRIVATE_STREAM_2)
        strncpy(string, "private steam 2", sizeof(string));
    else if (stream_id==ECM_STREAM)
        strncpy(string, "ECM stream", sizeof(string));
    else if (stream_id==EMM_STREAM)
        strncpy(string, "EMM stream", sizeof(string));
    else if (stream_id==PROGRAM_STREAM_DIRECTORY)
        strncpy(string, "program stream directory", sizeof(string));
    else if (stream_id==DSMCC_STREAM)
        strncpy(string, "DSMCC stream", sizeof(string));
    else if (stream_id==TYPE_E_STREAM)
        strncpy(string, "type E stream", sizeof(string));
    else if (stream_id==PROGRAM_STREAM_DIRECTORY)
        strncpy(string, "program stream directory", sizeof(string));
    else if (stream_id==PADDING_STREAM)
        strncpy(string, "padding stream", sizeof(string));
    else if (stream_id>=VIDEO_STREAM_X && stream_id<VIDEO_STREAM_X+16)
        snprintf(string, sizeof(string), "video stream %d", stream_id-VIDEO_STREAM_X);
    else if (stream_id>=AUDIO_STREAM_X && stream_id<AUDIO_STREAM_X+32)
        snprintf(string, sizeof(string), "audio stream %d", stream_id-AUDIO_STREAM_X);
    else
        strncpy(string, "unknown stream type", sizeof(string));
    rvmessage("pes packet: stream id 0x%.2x (%s) and size %d bytes", stream_id, string, pes_packet_length);
}

/*
 * program stream functions
 */

int parse_system_header(struct bitbuf *bb, int verbose) {
    int bytecount = 0;
    int header_length = getbits(bb, 16);                    /* header_length */
    flushbits(bb, 1);                                       /* marker_bit */
    int rate_bound = getbits(bb, 22);                       /* rate_bound */
    flushbits(bb, 1);                                       /* marker_bit */
    int audio_bound = getbits(bb, 6);                       /* audio_bound */
    flushbits(bb, 1);                                       /* fixed_flag */
    flushbits(bb, 1);                                       /* CSPS_flag */
    flushbits(bb, 1);                                       /* system_audio_lock_flag */
    flushbits(bb, 1);                                       /* system_video_lock_flag */
    flushbits(bb, 1);                                       /* marker_bit */
    int video_bound = getbits(bb, 5);                       /* video_bound */
    flushbits(bb, 1);                                       /* packet_rate_restriction_flag */
    flushbits(bb, 7);                                       /* reserved_byte */
    if (verbose>=1)
        rvmessage("program stream has max %d video streams, max %d audio streams and max %.2f Mbits/secs rate", video_bound, audio_bound, (double)rate_bound*50.0*8.0/1000000.0);
    header_length -= 6;
    bytecount += 8;
    while (showbits(bb, 1) == 1) {
        flushbits(bb, 8);                                   /* stream_id */
        flushbits(bb, 2);                                   /* '11' */
        flushbits(bb, 1);                                   /* P-STD_buffer_bound_scale */
        flushbits(bb, 13);                                  /* P-STD_buffer_size_bound */
        header_length -= 3;
        bytecount += 3;
    }
    while (header_length) {
        flushbits(bb, 8);
        header_length--;
        bytecount++;
    }
    return bytecount;
}

int parse_pack_header(struct bitbuf *bb, int verbose)
{
    int bytecount = 0;
    flushbits(bb, 2);                                   /* '01' */
    flushbits(bb, 3);                                   /* system_clock_reference_base[32..30] */
    flushbits(bb, 1);                                   /* marker_bit */
    flushbits(bb, 15);                                  /* system_clock_reference_base[29..15] */
    flushbits(bb, 1);                                   /* marker_bit */
    flushbits(bb, 15);                                  /* system_clock_reference_base[14..0] */
    flushbits(bb, 1);                                   /* marker_bit */
    flushbits(bb, 9);                                   /* system_clock_reference_extension */
    flushbits(bb, 1);                                   /* marker_bit */
    int program_mux_rate = getbits(bb, 22);             /* program_mux_rate */
    if (verbose>=2)
        rvmessage("new program stream pack has rate of %.2f Mbits/sec", (double)program_mux_rate*50.0*8.0/1000000.0);
    flushbits(bb, 1);                                   /* marker_bit */
    flushbits(bb, 1);                                   /* marker_bit */
    flushbits(bb, 5);                                   /* reserved */
    int pack_stuffing_length = getbits(bb, 3);          /* pack_stuffing_length */ 
    int i;
    bytecount += 10;
    for (i=0; i<pack_stuffing_length; i++) {
        flushbits(bb, 8);                               /* stuffing_byte */
        bytecount++;
    }
    if (showbits32(bb) == 0x000001bb) {
        flushbits(bb, 32);                              /* system_header_start_code */
        bytecount += 4;
        return bytecount + parse_system_header(bb, verbose);
    } else
        return bytecount;
}

int parse_program_stream_map(struct bitbuf *bb)
{
    flushbits(bb, 24);                                  /* packet_start_code_prefix */
    flushbits(bb, 8);                                   /* map_stream_id */
    int program_stream_map_length = getbits(bb, 16);    /* program_stream_map_length */
    flushbits(bb, 1);                                   /* current_next_indicator */
    flushbits(bb, 2);                                   /* reserved */
    flushbits(bb, 5);                                   /* program_stream_map_version */
    flushbits(bb, 7);                                   /* reserved */
    flushbits(bb, 1);                                   /* marker_bit */
    flushbits(bb, 16);                                  /* program_stream_info_length */
    program_stream_map_length -= 4;
    while (program_stream_map_length) {
        program_stream_map_length -= parse_descriptor(bb, NULL, "psm");
    }
    int elementary_stream_map_length = getbits(bb, 16); /* elementary_stream_map_length */
    while (elementary_stream_map_length) {
        flushbits(bb, 8);                                   /* stream_type */
        flushbits(bb, 8);                                   /* elementary_stream_id */
        int elementary_stream_info_length = getbits(bb, 16);/* elementary_stream_info_length */
        elementary_stream_map_length -= 4 + elementary_stream_info_length;
        while (elementary_stream_info_length) {
            elementary_stream_info_length -= parse_descriptor(bb, NULL, "psm");
        }
    }
    flushbits(bb, 32);                                  /* CRC_32 */
    return 0;
}

int parse_directory_PES_packet(struct bitbuf *bb)
{
    flushbits(bb, 24);                                  /* packet_start_code_prefix */
    flushbits(bb, 8);                                   /* directory_stream_id */
    flushbits(bb, 16);                                  /* PES_packet_length */
    int number_of_access_units = getbits(bb, 15);                                  /* number_of_access_units */
    flushbits(bb, 1);                                   /* marker_bit */
    flushbits(bb, 15);                                  /* prev_directory_offset[44..30] */
    flushbits(bb, 1);                                   /* marker_bit */
    flushbits(bb, 15);                                  /* prev_directory_offset[29..15] */
    flushbits(bb, 1);                                   /* marker_bit */
    flushbits(bb, 15);                                  /* prev_directory_offset[14..0] */
    flushbits(bb, 1);                                   /* marker_bit */
    flushbits(bb, 15);                                  /* next_directory_offset[44..30] */
    flushbits(bb, 1);                                   /* marker_bit */
    flushbits(bb, 15);                                  /* next_directory_offset[29..15] */
    flushbits(bb, 1);                                   /* marker_bit */
    flushbits(bb, 15);                                  /* next_directory_offset[14..0] */
    flushbits(bb, 1);                                   /* marker_bit */
    int i;
    for (i=0; i<number_of_access_units; i++) {
    flushbits(bb, 8);                                   /* packet_stream_id */
    flushbits(bb, 1);                                   /* PES_header_position_offset_sign */
    flushbits(bb, 14);                                  /* PES_header_position_offset[43..30] */
    flushbits(bb, 1);                                   /* marker_bit */
    flushbits(bb, 15);                                  /* PES_header_position_offset[29..15] */
    flushbits(bb, 1);                                   /* marker_bit */
    flushbits(bb, 15);                                  /* PES_header_position_offset[14..0] */
    flushbits(bb, 1);                                   /* marker_bit */
    flushbits(bb, 16);                                  /* reference_offset */
    flushbits(bb, 1);                                   /* marker_bit */
    flushbits(bb, 3);                                   /* reserved */
    flushbits(bb, 3);                                   /* PTS[32..30] */
    flushbits(bb, 1);                                   /* marker_bit */
    flushbits(bb, 15);                                  /* PTS[29..15] */
    flushbits(bb, 1);                                   /* marker_bit */
    flushbits(bb, 15);                                  /* PTS[14..0] */
    flushbits(bb, 1);                                   /* marker_bit */
    flushbits(bb, 15);                                  /* bytes_to_read[22..8] */
    flushbits(bb, 1);                                   /* marker_bit */
    flushbits(bb, 8);                                   /* bytes_to_read[7..0] */
    flushbits(bb, 1);                                   /* marker_bit */
    flushbits(bb, 1);                                   /* intra_coded_indicator */
    flushbits(bb, 2);                                   /* coding_parameters_indicator */
    flushbits(bb, 4);                                   /* reserved */
    }
    return 0;
}

int put_program_association_section(struct bitbuf *bb, int num_programs, int pat[][2])
{
    int i, section_length = 4 + num_programs*4 + 5;

    putbits(bb, 8, 0x00);                   /* table_id */
    putbits(bb, 1, 1);                      /* section_syntax_indicator */
    putbits(bb, 1, 0);                      /* '0' */
    putbits(bb, 2, 0x3);                    /* reserved */
    putbits(bb, 12, section_length);        /* section_length */
    putbits(bb, 16, ('r'<<8) |'v');         /* transport_stream_id */
    putbits(bb, 2, 0x3);                    /* reserved */
    putbits(bb, 5, 0);                      /* version_number */
    putbits(bb, 1, 1);                      /* current_next_indicator */
    putbits(bb, 8, 0x00);                   /* section_number */
    putbits(bb, 8, 0x00);                   /* last_section_number */
    for (i=0; i<num_programs; i++) {
        putbits(bb, 16, pat[i][0]);         /* program_number */
        putbits(bb, 3, 0x7);                /* reserved */
        putbits(bb, 13, pat[i][1]);         /* network_PID / program_map_PID */
    }
    crc32_t crc32 = crc32bits(bb);
    putbits(bb, 32, crc32);                 /* CRC_32 */

    return section_length + 3;
}

int put_program_map_section(struct bitbuf *bb, int program_number, int pcr_pid, int num_program_elements, int pmt[][2])
{
    const int num_program_element_descriptors = 0;
    const int es_info_length = 0;
    const int num_program_descriptors = 0;
    const int program_info_length = 0;
    int i, section_length = 4 + num_program_elements*5 + 9;

    putbits(bb, 8, 0x2);                    /* table_id */
    putbits(bb, 1, 1);                      /* section_syntax_indicator */
    putbits(bb, 1, 0);                      /* '0' */
    putbits(bb, 2, 0x3);                    /* reserved */
    putbits(bb, 12, section_length);        /* section_length */
    putbits(bb, 16, program_number);        /* program_number */
    putbits(bb, 2, 0x3);                    /* reserved */
    putbits(bb, 5, 0);                      /* version_number */
    putbits(bb, 1, 1);                      /* current_next_indicator */
    putbits(bb, 8, 0x00);                   /* section_number */
    putbits(bb, 8, 0x00);                   /* last_section_number */
    putbits(bb, 3, 0x7);                    /* reserved */
    putbits(bb, 13, pcr_pid);                /* PCR_PID */
    putbits(bb, 4, 0xF);                    /* reserved */
    putbits(bb, 12, program_info_length);   /* program_info_length */
    for (i=0; i<num_program_descriptors; i++) {
        //descriptor()
    }
    for (i=0; i<num_program_elements; i++) {
        putbits(bb, 8, pmt[i][0]);          /* stream_type */
        putbits(bb, 3, 0x7);                /* reserved */
        putbits(bb, 13, pmt[i][1]);         /* elementary_PID */
        putbits(bb, 4, 0xF);                /* reserved */
        putbits(bb, 12, es_info_length);    /* ES_info_length */
        for (i=0; i<num_program_element_descriptors; i++) {
            //descriptor()
        }
    }
    crc32_t crc32 = crc32bits(bb);
    putbits(bb, 32, crc32);                 /* CRC_32 */

    return section_length + 3;
}

int put_pes_packet(struct bitbuf *bb, int stream_id, int payload_start, long long pts, long long dts, unsigned char *es_data, int es_data_length)
{
    int pts_dts_flags = pts>=0? (pts==dts? 2 : 3) : 0;
    const int escr_flag = 0;
    const int es_rate_flag = 0;
    const int dsm_trick_mode_flag = 0;
    const int additional_copy_info_flag = 0;
    const int pes_crc_flag = 0;
    const int pes_extension_flag = 0;

    long long escr_base = 0;
    int escr_extension = 0;
    int es_rate = 0;

    int i;
    int pes_packet_length = 0;

    putbits(bb, 24, 0x1);                   /* packet_start_code_prefix */
    putbits(bb, 8, stream_id);              /* stream_id */
    if (stream_id != 0xBC &&
        stream_id != 0xBE &&
        stream_id != 0xBF &&
        stream_id != 0xF0 &&
        stream_id != 0xF1 &&
        stream_id != 0xFF &&
        stream_id != 0xF2 &&
        stream_id != 0xF8) {
        int pes_stuffing_bytes = 0;
        int pes_header_data_length = pes_stuffing_bytes;
        if (pts_dts_flags == 0x2)
            pes_header_data_length += 5;
        if (pts_dts_flags == 0x3)
            pes_header_data_length += 10;
        if (escr_flag)
            pes_header_data_length += 6;
        if (es_rate_flag)
            pes_header_data_length += 3;
        if (dsm_trick_mode_flag)
            pes_header_data_length += 0;
        if (additional_copy_info_flag)
            pes_header_data_length += 1;
        if (pes_crc_flag)
            pes_header_data_length += 2;
        if (pes_extension_flag)
            pes_header_data_length += 0;
        pes_packet_length = es_data_length + 3 + pes_header_data_length;

        putbits(bb, 16, pes_packet_length); /* pes_packet_length */
        putbits(bb, 2, 0x2);                /* '10' */
        putbits(bb, 2, 0x0);                /* PES_scrambling_control */
        putbits(bb, 1, 0);                  /* PES_priority */
        putbits(bb, 1, payload_start);      /* data_alignment_indicator */
        putbits(bb, 1, 0);                  /* copyright */
        putbits(bb, 1, 0);                  /* original_or_copy */
        putbits(bb, 2, pts_dts_flags);      /* PTS_DTS_flags */
        putbits(bb, 1, escr_flag);          /* ESCR_flag */
        putbits(bb, 1, es_rate_flag);       /* ES_rate_flag */
        putbits(bb, 1, dsm_trick_mode_flag);/* DSM_trick_mode_flag */
        putbits(bb, 1, additional_copy_info_flag);/* additional_copy_info_flag */
        putbits(bb, 1, pes_crc_flag);       /* PES_CRC_flag */
        putbits(bb, 1, pes_extension_flag); /* PES_extension_flag */
        putbits(bb, 8, pes_header_data_length);/* PES_header_data_length */
        if (pts_dts_flags == 0x2) {
            putbits(bb, 4, 0x2);            /* '0010' */
            putbits(bb, 3, pts>>30);        /* PTS [32..30] */
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 15, pts>>15);       /* PTS [29..15] */
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 15, pts);           /* PTS [14..0] */
            putbits(bb, 1, 1);              /* marker_bit */
        }
        if (pts_dts_flags == 0x3) {
            putbits(bb, 4, 0x3);            /* '0011' */
            putbits(bb, 3, pts>>30);        /* PTS [32..30] */
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 15, pts>>15);       /* PTS [29..15] */
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 15, pts);           /* PTS [14..0] */
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 4, 0x1);            /* '0001' */
            putbits(bb, 3, dts>>30);        /* DTS [32..30] */
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 15, dts>>15);       /* DTS [29..15] */
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 15, dts);           /* DTS [14..0] */
            putbits(bb, 1, 1);              /* marker_bit */
        }
        if (escr_flag == 1) {
            putbits(bb, 2, 0x3);            /* reserved */
            putbits(bb, 3, escr_base>>30);  /* ESCR_base[32..30] */
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 15, escr_base>>15); /* ESCR_base[29..15] */
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 15, escr_base);     /* ESCR_base[14..0] */
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 9, escr_extension); /* ESCR_extension */
            putbits(bb, 1, 1);              /* marker_bit */
        }
        if (es_rate_flag == 1) {
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 22, es_rate);       /* ES_rate */
            putbits(bb, 1, 1);              /* marker_bit */
        }
        if (dsm_trick_mode_flag == 1) {
            /*
            trick_mode_control
            if ( trick_mode_control = = fast_forward ) {
                field_id
                intra_slice_refresh
                frequency_truncation
            }
            else if ( trick_mode_control = = slow_motion ) {
                rep_cntrl
            }
            else if ( trick_mode_control = = freeze_frame ) {
                field_id
                reserved
            }
            else if ( trick_mode_control = = fast_reverse ) {
                field_id
                intra_slice_refresh
                frequency_truncation
            else if ( trick_mode_control = = slow_reverse ) {
                rep_cntrl
            }
            else
                reserved
            */
        }
        if (additional_copy_info_flag == 1) {
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 7, 0);              /* additional_copy_info */
        }
        if (pes_crc_flag == 1) {
            putbits(bb, 16, 0);             /* previous_PES_packet_CRC */
        }
        if (pes_extension_flag == 1) {
            /*
            PES_private_data_flag
            pack_header_field_flag
            program_packet_sequence_counter_flag
            P-STD_buffer_flag
            reserved
            PES_extension_flag_2
            if (PES_private_data_flag == 1) {
                PES_private_data
            }
            if (pack_header_field_flag == 1) {
                pack_field_length
                pack_header()
            }
            if (program_packet_sequence_counter_flag == 1) {
                marker_bit
                program_packet_sequence_counter
                marker_bit
                MPEG1_MPEG2_identifier
                original_stuff_length
            }
            if ( P-STD_buffer_flag == 1) {
                '01'
                P-STD_buffer_scale
                P-STD_buffer_size
            }
            if (PES_extension_flag_2 == 1) {
                marker_bit
                PES_extension_field_length
                for (i = 0; i < PES_extension_field_length; i++) {
                    reserved
                }
            }
            */
        }
        for (i=0; i<pes_stuffing_bytes; i++) {
            putbits(bb, 8, 0xff);           /* stuffing_byte */
        }
        for (i=0; i<es_data_length; i++) {
            putbits(bb, 8, es_data[i]);     /* PES_packet_data_byte */
        }
    }
    else if (stream_id == 0xBC ||
        stream_id == 0xBF ||
        stream_id == 0xF0 ||
        stream_id == 0xF1 ||
        stream_id == 0xFF ||
        stream_id == 0xF2 ||
        stream_id == 0xF8) {
        pes_packet_length = es_data_length;
        putbits(bb, 16, pes_packet_length); /* pes_packet_length */
        for (i=0; i<es_data_length; i++) {
            putbits(bb, 8, es_data[i]);     /* PES_packet_data_byte */
        }
    }
    else if (stream_id == 0xBE) {
        pes_packet_length = es_data_length;
        putbits(bb, 16, pes_packet_length); /* pes_packet_length */
        for (i=0; i<es_data_length; i++) {
            putbits(bb, 8, 0xff);           /* padding_byte */
        }
    }
    /* return size of pes packet */
    return pes_packet_length + 6;
}

int put_pes_packet_header(struct bitbuf *bb, int stream_id, int payload_start, long long pts, long long dts, int es_data_length)
{
    int pts_dts_flags = pts>=0? (pts==dts? 2 : 3) : 0;
    const int escr_flag = 0;
    const int es_rate_flag = 0;
    const int dsm_trick_mode_flag = 0;
    const int additional_copy_info_flag = 0;
    const int pes_crc_flag = 0;
    const int pes_extension_flag = 0;

    long long escr_base = 0;
    int escr_extension = 0;
    int es_rate = 0;

    int i;
    int pes_packet_length = 0;
    int pes_header_length = 0;

    putbits(bb, 24, 0x1);                   /* packet_start_code_prefix */
    putbits(bb, 8, stream_id);              /* stream_id */
    if (stream_id != 0xBC &&
        stream_id != 0xBE &&
        stream_id != 0xBF &&
        stream_id != 0xF0 &&
        stream_id != 0xF1 &&
        stream_id != 0xFF &&
        stream_id != 0xF2 &&
        stream_id != 0xF8) {
        int pes_stuffing_bytes = 0;
        int pes_header_data_length = pes_stuffing_bytes;
        if (pts_dts_flags == 0x2)
            pes_header_data_length += 5;
        if (pts_dts_flags == 0x3)
            pes_header_data_length += 10;
        if (escr_flag)
            pes_header_data_length += 6;
        if (es_rate_flag)
            pes_header_data_length += 3;
        if (dsm_trick_mode_flag)
            pes_header_data_length += 0;
        if (additional_copy_info_flag)
            pes_header_data_length += 1;
        if (pes_crc_flag)
            pes_header_data_length += 2;
        if (pes_extension_flag)
            pes_header_data_length += 0;

        if (es_data_length)
            pes_packet_length = es_data_length + 3 + pes_header_data_length;
        /* else use unbounded pes packets */

        putbits(bb, 16, pes_packet_length); /* pes_packet_length */
        putbits(bb, 2, 0x2);                /* '10' */
        putbits(bb, 2, 0x0);                /* PES_scrambling_control */
        putbits(bb, 1, 0);                  /* PES_priority */
        putbits(bb, 1, payload_start);      /* data_alignment_indicator */
        putbits(bb, 1, 0);                  /* copyright */
        putbits(bb, 1, 0);                  /* original_or_copy */
        putbits(bb, 2, pts_dts_flags);      /* PTS_DTS_flags */
        putbits(bb, 1, escr_flag);          /* ESCR_flag */
        putbits(bb, 1, es_rate_flag);       /* ES_rate_flag */
        putbits(bb, 1, dsm_trick_mode_flag);/* DSM_trick_mode_flag */
        putbits(bb, 1, additional_copy_info_flag);/* additional_copy_info_flag */
        putbits(bb, 1, pes_crc_flag);       /* PES_CRC_flag */
        putbits(bb, 1, pes_extension_flag); /* PES_extension_flag */
        putbits(bb, 8, pes_header_data_length);/* PES_header_data_length */
        if (pts_dts_flags == 0x2) {
            putbits(bb, 4, 0x2);            /* '0010' */
            putbits(bb, 3, pts>>30);        /* PTS [32..30] */
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 15, pts>>15);       /* PTS [29..15] */
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 15, pts);           /* PTS [14..0] */
            putbits(bb, 1, 1);              /* marker_bit */
        }
        if (pts_dts_flags == 0x3) {
            putbits(bb, 4, 0x3);            /* '0011' */
            putbits(bb, 3, pts>>30);        /* PTS [32..30] */
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 15, pts>>15);       /* PTS [29..15] */
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 15, pts);           /* PTS [14..0] */
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 4, 0x1);            /* '0001' */
            putbits(bb, 3, dts>>30);        /* DTS [32..30] */
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 15, dts>>15);       /* DTS [29..15] */
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 15, dts);           /* DTS [14..0] */
            putbits(bb, 1, 1);              /* marker_bit */
        }
        if (escr_flag == 1) {
            putbits(bb, 2, 0x3);            /* reserved */
            putbits(bb, 3, escr_base>>30);  /* ESCR_base[32..30] */
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 15, escr_base>>15); /* ESCR_base[29..15] */
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 15, escr_base);     /* ESCR_base[14..0] */
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 9, escr_extension); /* ESCR_extension */
            putbits(bb, 1, 1);              /* marker_bit */
        }
        if (es_rate_flag == 1) {
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 22, es_rate);       /* ES_rate */
            putbits(bb, 1, 1);              /* marker_bit */
        }
        if (dsm_trick_mode_flag == 1) {
            /*
            trick_mode_control
            if ( trick_mode_control = = fast_forward ) {
                field_id
                intra_slice_refresh
                frequency_truncation
            }
            else if ( trick_mode_control = = slow_motion ) {
                rep_cntrl
            }
            else if ( trick_mode_control = = freeze_frame ) {
                field_id
                reserved
            }
            else if ( trick_mode_control = = fast_reverse ) {
                field_id
                intra_slice_refresh
                frequency_truncation
            else if ( trick_mode_control = = slow_reverse ) {
                rep_cntrl
            }
            else
                reserved
            */
        }
        if (additional_copy_info_flag == 1) {
            putbits(bb, 1, 1);              /* marker_bit */
            putbits(bb, 7, 0);              /* additional_copy_info */
        }
        if (pes_crc_flag == 1) {
            putbits(bb, 16, 0);             /* previous_PES_packet_CRC */
        }
        if (pes_extension_flag == 1) {
            /*
            PES_private_data_flag
            pack_header_field_flag
            program_packet_sequence_counter_flag
            P-STD_buffer_flag
            reserved
            PES_extension_flag_2
            if (PES_private_data_flag == 1) {
                PES_private_data
            }
            if (pack_header_field_flag == 1) {
                pack_field_length
                pack_header()
            }
            if (program_packet_sequence_counter_flag == 1) {
                marker_bit
                program_packet_sequence_counter
                marker_bit
                MPEG1_MPEG2_identifier
                original_stuff_length
            }
            if ( P-STD_buffer_flag == 1) {
                '01'
                P-STD_buffer_scale
                P-STD_buffer_size
            }
            if (PES_extension_flag_2 == 1) {
                marker_bit
                PES_extension_field_length
                for (i = 0; i < PES_extension_field_length; i++) {
                    reserved
                }
            }
            */
        }
        for (i=0; i<pes_stuffing_bytes; i++) {
            putbits(bb, 8, 0xff);           /* stuffing_byte */
        }
        pes_header_length = pes_header_data_length + 9;
    }
    else if (stream_id == 0xBC ||
        stream_id == 0xBF ||
        stream_id == 0xF0 ||
        stream_id == 0xF1 ||
        stream_id == 0xFF ||
        stream_id == 0xF2 ||
        stream_id == 0xF8) {
        pes_packet_length = es_data_length;
        putbits(bb, 16, pes_packet_length); /* pes_packet_length */
        pes_header_length = 2;
    }
    else if (stream_id == 0xBE) {
        pes_packet_length = es_data_length;
        putbits(bb, 16, pes_packet_length); /* pes_packet_length */
        pes_header_length = 2;
    }
    /* return size of pes header */
    return pes_header_length;
}

int put_adaptation_field(struct bitbuf *bb, int required_field_length, int pcr_flag, long long pcr)
{
    const int opcr_flag = 0;
    const int splicing_point_flag = 0;
    const int transport_private_data_flag = 0;
    const int adaptation_field_extension_flag = 0;
    const int ltw_flag = 0;
    const int piecewise_rate_flag = 0;
    const int seamless_splice_flag = 0;

    long long pcr_base = (pcr / 300ll) % 0x2000000ll;
    long long pcr_ext = pcr % 300ll;

    int reserved_bytes = 0;
    int transport_private_data_length = 0;
    int adaptation_field_extension_length = 0;

    if (adaptation_field_extension_flag) {
        adaptation_field_extension_length = 1 + reserved_bytes;
        if (seamless_splice_flag)
            adaptation_field_extension_length += 5;
        if (piecewise_rate_flag)
            adaptation_field_extension_length += 3;
        if (ltw_flag)
            adaptation_field_extension_length += 2;
    }

    int adaptation_field_length = 1;
    if (adaptation_field_extension_flag)
        adaptation_field_length += 1 + adaptation_field_extension_length;
    if (transport_private_data_flag)
        adaptation_field_length += 1 + transport_private_data_length;
    if (splicing_point_flag)
        adaptation_field_length += 1;
    if (opcr_flag)
        adaptation_field_length += 6;
    if (pcr_flag)
        adaptation_field_length += 6;

    /* calculate amount of stuffing needed to make adaptation field the required size */
    int stuffing_bytes = 0;
    if (required_field_length)
        stuffing_bytes = required_field_length - 1 - adaptation_field_length;
    adaptation_field_length += stuffing_bytes;

    putbits(bb, 8, adaptation_field_length);/* adaptation_field_length */
    if (adaptation_field_length > 0) {
        int i;
        putbits(bb, 1, 0);                  /* discontinuity_indicator */
        putbits(bb, 1, 0);                  /* random_access_indicator */
        putbits(bb, 1, 0);                  /* elementary_stream_priority_indicator */
        putbits(bb, 1, pcr_flag);           /* PCR_flag */
        putbits(bb, 1, opcr_flag);          /* OPCR_flag */
        putbits(bb, 1, splicing_point_flag);/* splicing_point_flag */
        putbits(bb, 1, transport_private_data_flag);/* transport_private_data_flag */
        putbits(bb, 1, adaptation_field_extension_flag);/* adaptation_field_extension_flag */
        if (pcr_flag == 1) {
            llputbits(bb, 33, pcr_base);    /* program_clock_reference_base */
            putbits(bb, 6, 0x3F);           /* reserved */
            putbits(bb, 9, (int)pcr_ext);   /* program_clock_reference_extension */
        }
        if (opcr_flag == 1) {
            putbits(bb, 33, 0);             /* original_program_clock_reference_base */
            putbits(bb, 6, 0x3F);           /* reserved */
            putbits(bb, 9, 0);              /* original_program_clock_reference_extension */
        }
        if (splicing_point_flag == 1) {
            putbits(bb, 8, 0);              /* splice_countdown */
        }
        if (transport_private_data_flag == 1) {
            putbits(bb, 8, transport_private_data_length);/* transport_private_data_length */
            for (i=0; i<transport_private_data_length; i++) {
                putbits(bb, 8, 0);          /* private_data_byte */
            }
        }
        if (adaptation_field_extension_flag == 1) {
            putbits(bb, 8, adaptation_field_extension_length);/* adaptation_field_extension_length */
            putbits(bb, 1, ltw_flag);       /* ltw_flag */
            putbits(bb, 1, piecewise_rate_flag);/* piecewise_rate_flag */
            putbits(bb, 1, seamless_splice_flag);/* seamless_splice_flag */
            putbits(bb, 5, 0x1F);           /* reserved */
            if (ltw_flag == 1) {
                putbits(bb, 1, 0);          /* ltw_valid_flag */
                putbits(bb, 15, 0);         /* ltw_offset */
            }
            if (piecewise_rate_flag == 1) {
                putbits(bb, 2, 0x3);        /* reserved */
                putbits(bb, 22, 0);         /* piecewise_rate */
            }
            if (seamless_splice_flag == 1) {
                putbits(bb, 4, 0);          /* splice_type */
                putbits(bb, 3, 0);          /* DTS_next_AU[32..30] */
                putbits(bb, 1, 1);          /* marker_bit */
                putbits(bb, 15, 1);         /* DTS_next_AU[29..15] */
                putbits(bb, 1, 1);          /* marker_bit */
                putbits(bb, 15, 1);         /* DTS_next_AU[14..0] */
                putbits(bb, 1, 1);          /* marker_bit */
            }
            for (i=0; i<reserved_bytes; i++) {
                putbits(bb, 8, 0xFF);       /* reserved */
            }
        }
        for (i=0; i<stuffing_bytes; i++) {
            putbits(bb, 8, 0xFF);           /* stuffing_byte */
        }
    }
    return adaptation_field_length + 1;
}

int put_ts_packet_pes_start(struct bitbuf *bb, int pid, int continuity_counter, int pcr_flag, long long pcr, int stream_number, long long pts, long long dts, unsigned char *pes_data, int pes_data_length)
{
    /* calculate amount of stuffing to add */
    int packet_size = 188 - 4;
    int adaptation_field_required = pcr_flag? 8 : 0;
    int stuffing_bytes = mmax(0, packet_size-adaptation_field_required-pes_data_length);
    int adaptation_field_control = stuffing_bytes || pcr_flag? 3 : 1;

    /* put ts packet header */
    putbits(bb, 8, 0x47);                   /* sync_byte */
    putbits(bb, 1, 0);                      /* transport_error_indicator */
    putbits(bb, 1, 1);                      /* payload_unit_start_indicator */
    putbits(bb, 1, 0);                      /* transport_priority */
    putbits(bb, 13, pid);                   /* PID */
    putbits(bb, 2, 0);                      /* transport_scrambling_control */
    putbits(bb, 2, adaptation_field_control);/* adaptation_field_control */
    putbits(bb, 4, continuity_counter);     /* continuity_counter */

    /* stuffing is inserted in adaptation field */
    if (adaptation_field_control == 2 || adaptation_field_control == 3) {
        packet_size -= put_adaptation_field(bb, adaptation_field_required+stuffing_bytes, pcr_flag, pcr);
    }

    /* put pes packet header */
    packet_size -= put_pes_packet_header(bb, VIDEO_STREAM_X + stream_number, 1, pts, dts, 0);

    /* fill remainder of packet with pes data */
    int data_bytes = 0;
    for (data_bytes=0; data_bytes<packet_size && data_bytes<pes_data_length; data_bytes++) {
        putbits(bb, 8, pes_data[data_bytes]); /* data_byte */
    }
    packet_size -= data_bytes;

    /* return number of pes bytes used in this packet */
    return data_bytes;
}

int put_ts_packet_pes_data(struct bitbuf *bb, int pid, int continuity_counter, int pcr_flag, long long pcr, unsigned char *pes_data, int pes_data_length)
{
    /* calculate amount of stuffing to add */
    int packet_size = 188 - 4;
    int adaptation_field_required = pcr_flag? 8 : 0;
    int stuffing_bytes = mmax(0, packet_size-adaptation_field_required-pes_data_length);
    int adaptation_field_control = stuffing_bytes || pcr_flag? 3 : 1;

    /* put ts packet header */
    putbits(bb, 8, 0x47);                   /* sync_byte */
    putbits(bb, 1, 0);                      /* transport_error_indicator */
    putbits(bb, 1, 0);                      /* payload_unit_start_indicator */
    putbits(bb, 1, 0);                      /* transport_priority */
    putbits(bb, 13, pid);                   /* PID */
    putbits(bb, 2, 0);                      /* transport_scrambling_control */
    putbits(bb, 2, adaptation_field_control);/* adaptation_field_control */
    putbits(bb, 4, continuity_counter);     /* continuity_counter */

    /* stuffing is inserted in adaptation field */
    if (adaptation_field_control == 2 || adaptation_field_control == 3) {
        packet_size -= put_adaptation_field(bb, adaptation_field_required+stuffing_bytes, pcr_flag, pcr);
    }

    /* fill remainder of packet with pes data */
    int data_bytes = 0;
    for (data_bytes=0; data_bytes<packet_size && data_bytes<pes_data_length; data_bytes++) {
        putbits(bb, 8, pes_data[data_bytes]); /* data_byte */
    }
    packet_size -= data_bytes;

    /* return number of pes bytes used in this packet */
    return data_bytes;
}

int put_ts_packet_psi(struct bitbuf *bb, int pid, int payload_start, int continuity_counter, unsigned char *psi_data, int psi_data_length)
{
    /* calculate amount of stuffing to add */
    int packet_size = 188 - 4;
    int adaptation_field_control = 1;

    /* put ts packet header */
    putbits(bb, 8, 0x47);                   /* sync_byte */
    putbits(bb, 1, 0);                      /* transport_error_indicator */
    putbits(bb, 1, payload_start);          /* payload_unit_start_indicator */
    putbits(bb, 1, 0);                      /* transport_priority */
    putbits(bb, 13, pid);                   /* PID */
    putbits(bb, 2, 0);                      /* transport_scrambling_control */
    putbits(bb, 2, adaptation_field_control);/* adaptation_field_control */
    putbits(bb, 4, continuity_counter);     /* continuity_counter */

    /* put pointer field (what is this for anyway?) */
    if (payload_start) {
        putbits(bb, 8, 0x00);               /* pointer_field */
        packet_size--;
    }

    /* fill remainder of packet with psi data */
    int data_bytes;
    for (data_bytes=0; data_bytes<packet_size && data_bytes<psi_data_length; data_bytes++) {
        putbits(bb, 8, psi_data[data_bytes]); /* data_byte */
    }
    packet_size -= data_bytes;

    /* add psi stuffing if necessary */
    if (packet_size) {
        int stuffing_bytes;
        for (stuffing_bytes=0; stuffing_bytes<packet_size; stuffing_bytes++) {
            putbits(bb, 8, 0xFF);           /* stuffing_byte */
        }
    }

    /* return number of pes bytes used in this packet */
    return data_bytes;
}

void put_ts_packet_pcr(struct bitbuf *bb, int pid, int continuity_counter, long long pcr)
{
    /* whole packet is adaptation field */
    int adaptation_field_length = 188 - 4;

    /* put ts packet header */
    putbits(bb, 8, 0x47);                   /* sync_byte */
    putbits(bb, 1, 0);                      /* transport_error_indicator */
    putbits(bb, 1, 0);                      /* payload_unit_start_indicator */
    putbits(bb, 1, 0);                      /* transport_priority */
    putbits(bb, 13, pid);                   /* PID */
    putbits(bb, 2, 0);                      /* transport_scrambling_control */
    putbits(bb, 2, 2);                      /* adaptation_field_control */
    putbits(bb, 4, continuity_counter);     /* continuity_counter */

    /* put adaptation field */
    put_adaptation_field(bb, adaptation_field_length, 1, pcr);
}

void put_ts_packet_null(struct bitbuf *bb)
{
    /* calculate amount of stuffing to add */
    int packet_size = 188 - 4;

    /* put ts packet header */
    putbits(bb, 8, 0x47);                   /* sync_byte */
    putbits(bb, 1, 0);                      /* transport_error_indicator */
    putbits(bb, 1, 0);                      /* payload_unit_start_indicator */
    putbits(bb, 1, 0);                      /* transport_priority */
    putbits(bb, 13, 0x1FFF);                /* PID */
    putbits(bb, 2, 0);                      /* transport_scrambling_control */
    putbits(bb, 2, 1);                      /* adaptation_field_control */
    putbits(bb, 4, 0);                      /* continuity_counter */

    /* fill remainder of packet with null data */
    int data_bytes;
    for (data_bytes=0; data_bytes<packet_size; data_bytes++) {
        putbits(bb, 8, 0x55);               /* data_byte */
    }
}

int put_section(struct bitbuf *bb, int pid, int continuity_counter, unsigned char *section, int section_length)
{
    int i, payload_start = 1;
    for (i=0; section_length>0; i++) {
        section_length -= put_ts_packet_psi(bb, pid, payload_start, continuity_counter++, section, section_length);
        payload_start = 0;
    }
    return i;
}
