/*
 * Description: Split frame pictures into field pictures, or vice versa.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2010-01-22 15:52:40 $
 * Revision   : $Revision: 1.11 $
 * Copyright  : (c) 2005 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include "getopt.h"

#include "rvutil.h"
#include "rvy4m.h"

const char *appname;

void usage(int exitcode)
{
    fprintf(stderr, "%s: split frame pictures into field pictures\n", appname);
    fprintf(stderr, "usage: %s [options] [<file>] [<file>...]\n", appname);
    fprintf(stderr, "  -i, --invert        : invert operation - weave field pictures into frame\n");
    fprintf(stderr, "  -t, --topfieldfirst : temporally first field, 1: top field first, 0: bottom field first (default: 1)\n");
    fprintf(stderr, "  -s, --size          : image size format\n");
    fprintf(stderr, "     (formats : %s)\n", rvformats);
    fprintf(stderr, "  -o, --output <file> : write output to file\n");
    fprintf(stderr, "  -4. --yuv4mpeg      : write yuv4mpeg format to output (default: raw yuv format)\n");
    fprintf(stderr, "  -q, --quiet         : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "  -v, --verbose       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "  --                  : disable argument processing\n");
    fprintf(stderr, "  -h, --help, --usage : print this usage message\n");
    exit(exitcode);
}

int main(int argc, char *argv[])
{
    FILE *filein = stdin;
    FILE *fileout = stdout;
    char *filename[RV_MAXFILES] = {0};
    char *outfile = NULL;
    int fileindex = 0;
    int numfiles = 0;
    int numframes = 0;
    int numfields = 0;

    /* data buffer */
    int size = 0;
    unsigned char *data[2] = {NULL, NULL};

    /* command line defaults */
    int invert = 0;
    int topfirst = 1;
    int width = 0;
    int height = 0;
    const char *format = NULL;
    int yuv4mpeg = 0;
    int verbose = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"invert",        1, NULL, 'i'},
            {"topfieldfirst", 1, NULL, 't'},
            {"size",          1, NULL, 's'},
            {"output",        1, NULL, 'o'},
            {"yuv4mpeg",      0, NULL, '4'},
            {"quiet",         0, NULL, 'q'},
            {"verbose",       0, NULL, 'v'},
            {"usage",         0, NULL, 'h'},
            {"help",          0, NULL, 'h'},
            {NULL,            0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "it:s:o:4qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 'i':
                invert = 1;
                break;

            case 't':
                topfirst = atoi(optarg);
                if (topfirst<0 || topfirst>1)
                    rvexit("invalid value for top field first: %d", topfirst);
                break;

            case 's':
                format = optarg;
                break;

            case '4':
                yuv4mpeg = 1;
                break;

            case 'o':
                outfile = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < sizeof(filename)/sizeof(filename[0]))
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    /* open output file */
    if (outfile)
        fileout = fopen(outfile, "wb");
    if (fileout==NULL)
        rverror("failed to open output file \"%s\"", outfile);

    /* ignore SIGPIPE on output file */
    if (signal(SIGPIPE, SIG_IGN)==SIG_ERR)
        rverror("failed to set ignore on SIGPIPE");

    /* loop over input files */
    do {
        if (numfiles)
            filein = fopen(filename[fileindex], "rb");
        if (filein==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex]);

        /* parse yuv4mpeg stream header */
        int file_width  = 0;
        int file_height = 0;
        int file_raw = 1;
        if (divine_image_dims(&file_width, &file_height, NULL, NULL, NULL, format, filename[fileindex])<0) {
            /* must be yuv4mpeg format file */
            file_raw = 0;
            if (read_y4m_stream_header(&file_width, &file_height, NULL, filein)<0)
                rverror("failed to read yuv4mpeg stream header");
        }
        if (!width)
            width = file_width;
        if (!height)
            height = file_height;

        /* check all input files are of the same dimensions */
        if (width!=file_width || height!=file_height)
            rvexit("input files are not isometric: %dx%-d != %dx%-d", width, height, file_width, file_height);

        /* allocate data buffer */
        size = width*height*3/2;
        if (data[0]==NULL)
            data[0] = (unsigned char *)rvalloc(NULL, size*sizeof(unsigned char), 0);

        /* allocate second data buffer */
        if (invert)
            if (data[1]==NULL)
                data[1] = (unsigned char *)rvalloc(NULL, size*sizeof(unsigned char), 0);

        /* write yuv4mpeg stream header */
        if (yuv4mpeg && fileindex==0)
            if (write_y4m_stream_header(width, invert? height*2 : height/2, get_chromaformat(I420), fileout)<0)
                rverror("failed to write stream header to output");

        /* main loop */
        while (!feof(filein) && !ferror(filein) && !ferror(fileout)) {
            int read[2];

            /* read frame or first field */
            if (!file_raw)
                read_y4m_frame_header(filein);
            read[0] = fread(data[0], 1, size, filein);

            /* read second field */
            if (invert) {
                if (!file_raw)
                    read_y4m_frame_header(filein);
                read[1] = fread(data[1], 1, size, filein);
            } else
                read[1] = 0;

            if (read[0] || read[1]) {
                unsigned char *p[2];
                const int frame = width*height;

                /* write yuv4mpeg frame header */
                if (yuv4mpeg)
                    if (write_y4m_frame_header(fileout)<0)
                        rverror("failed to write frame header to output");

                if (!invert) {
                    /* write first field */
                    int field = topfirst? 0 : width;
                    for (p[0]=data[0]+field; p[0]<data[0]+frame; p[0]+=2*width)
                        fwrite(p[0], width, 1, fileout);
                    for (p[0]=data[0]+frame+field/2; p[0]<data[0]+frame*5/4; p[0]+=width)
                        fwrite(p[0], width/2, 1, fileout);
                    for (p[0]=data[0]+frame*5/4+field/2; p[0]<data[0]+frame*3/2; p[0]+=width)
                        fwrite(p[0], width/2, 1, fileout);

                    if (ferror(fileout) && errno!=EPIPE)
                        rverror("failed to write first field to output");

                    /* write yuv4mpeg frame header */
                    if (yuv4mpeg)
                        if (write_y4m_frame_header(fileout)<0)
                            rverror("failed to write frame header to output");

                    /* write second field */
                    field = topfirst? width : 0;
                    for (p[0]=data[0]+field; p[0]<data[0]+frame; p[0]+=2*width)
                        fwrite(p[0], width, 1, fileout);
                    for (p[0]=data[0]+frame+field/2; p[0]<data[0]+frame*5/4; p[0]+=width)
                        fwrite(p[0], width/2, 1, fileout);
                    for (p[0]=data[0]+frame*5/4+field/2; p[0]<data[0]+frame*3/2; p[0]+=width)
                        fwrite(p[0], width/2, 1, fileout);

                    if (ferror(fileout) && errno!=EPIPE)
                        rverror("failed to write second field to output");

                } else {
                    int top = topfirst? 0 : 1;
                    /* write whole frame */
                    for (p[0]=data[top], p[1]=data[!top]; p[0]<data[top]+frame; p[0]+=width, p[1]+=width) {
                        fwrite(p[0], width, 1, fileout);
                        fwrite(p[1], width, 1, fileout);
                    }
                    for (p[0]=data[top]+frame, p[1]=data[!top]+frame; p[0]<data[top]+frame*5/4; p[0]+=width/2, p[1]+=width/2) {
                        fwrite(p[0], width/2, 1, fileout);
                        fwrite(p[1], width/2, 1, fileout);
                    }
                    for (p[0]=data[top]+frame*5/4, p[1]=data[!top]+frame*5/4; p[0]<data[top]+frame*3/2; p[0]+=width/2, p[1]+=width/2) {
                        fwrite(p[0], width/2, 1, fileout);
                        fwrite(p[1], width/2, 1, fileout);
                    }

                    if (ferror(fileout) && errno!=EPIPE)
                        rverror("failed to write frame to output");
                }

                /* update statistics */
                numframes++;
                numfields+=2;
            }
        }
        if (ferror(filein))
            rverror("failed to read from input");
        if (ferror(fileout) && errno!=EPIPE)
            rverror("failed to write to output");

        if (numfiles)
            fclose(filein);

    } while (++fileindex<numfiles);

    if (verbose>=0) {
        if (!invert)
            rvmessage("read %d frames, wrote %d fields", numframes, numfields);
        else
            rvmessage("read %d fields, wrote %d frames", numfields, numframes);
    }

    /* tidy up */
    rvfree(data[0]);
    rvfree(data[1]);

    return 0;
}
