/*
 * Description: Demultiplex video and audio container files.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-06-23 13:26:19 $
 * Revision   : $Revision: 1.5 $
 * Copyright  : (c) 2007 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "rvutil.h"
#include "rvasf.h"
#include "rvbits.h"

/*
 * guid utility functions
 */

/* return next guid (only works when byte aligned) */
struct guid getguid(struct bitbuf *bb)
{
    int i;
    struct guid guid;

    if (bb->bufbits & 7)
        rverror("bitstream not byte aligned: %d", bb->bufbits);

    guid.data1 = getdword(bb);
    guid.data2 = getword(bb);
    guid.data3 = getword(bb);
    for (i=0; i<8; i++)
        guid.data4[i] = getbyte(bb);

    return guid;
}

char *stringify_guid(struct guid guid, char *string)
{
    snprintf(string, 37, "%08lX-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X", guid.data1, guid.data2, guid.data3,
                  guid.data4[0], guid.data4[1], guid.data4[2], guid.data4[3], guid.data4[4], guid.data4[5], guid.data4[6], guid.data4[7]);
    return string;
}

/*
 * parsing utility functions
 */

int parse_length_with_type(struct bitbuf *bb, int length_type)
{
    switch (length_type) {
        case 0 : return 0;
        case 1 : return getbyte(bb);
        case 2 : return getword(bb);
        case 3 : return getdword(bb);
        default: return -1;
    }
}

int length_of_type(int length_type)
{
    return length_type>=3? 4 : length_type;
}

/*
 * header object functions
 */

int parse_header_object(struct bitbuf *bb, int verbose)
{
    int number_of_header_objects = getdword(bb);    /* number of header objects */
    getbyte(bb);                                    /* reserved1 */
    int reserved2 = getbyte(bb);                    /* reserved2 */
    if (reserved2!=0x02) {
        rvmessage("asf header object: reserved2 is not 0x02: %d", reserved2);
        return -1;
    }
    if (verbose>=2)
        rvmessage("asf: %d objects in header object", number_of_header_objects);
    return 0;
}

int parse_file_properties_object(struct bitbuf *bb, int verbose, long long *data_packets_count, int *flags, int *data_packet_size)
{
    getguid(bb);                                    /* File ID */
    getqword(bb);                                   /* File Size */
    getqword(bb);                                   /* Creation Date */
    *data_packets_count = getqword(bb);             /* Data Packets Count */
    getqword(bb);                                   /* Play Duration */
    getqword(bb);                                   /* Send Duration */
    getqword(bb);                                   /* Preroll */
    *flags = getdword(bb);                          /* Flags */
    *data_packet_size = getdword(bb);               /* Minimum Data Packet Size */
    int maximum_data_packet_size = getdword(bb);    /* Maximum Data Packet Size */
    getdword(bb);                                   /* Maximum Bitrate */
    if (verbose>=2)
        rvmessage("asf: %lld data packets of size %d", *data_packets_count, *data_packet_size);
    if (*data_packet_size!=maximum_data_packet_size)
        rvmessage("asf: min and max data packet sizes do not match: %d, %d", *data_packet_size, maximum_data_packet_size);
    return 0;
}

int parse_stream_properties_object(struct bitbuf *bb, int verbose, int *codec_specific_data_size, unsigned char **codec_specific_data)
{
    int i;
    char string[37];

    struct guid stream_type = getguid(bb);          /* Stream Type */
    getguid(bb);                                    /* Error Correction Type */
    getqword(bb);                                   /* Time Offset */
    int type_specific_data_length = getdword(bb);   /* Type-Specific Data Length */
    int error_correction_data_length = getdword(bb);/* Error Correction Data Length */
    int flags = getword(bb);                        /* Flags */
    getdword(bb);                                   /* Reserved */
    if (strcmp(stringify_guid(stream_type, string), ASF_AUDIO_MEDIA)==0) {
        getword(bb);                                /* Codec ID / Format Tag */
        int number_of_channels = getword(bb);       /* Number of Channels */
        int samples_per_second = getdword(bb);      /* Samples Per Second */
        getdword(bb);                               /* Average Number of Bytes Per Second */
        getword(bb);                                /* Block Alignment */
        int bits_per_sample = getword(bb);          /* Bits Per Sample */
        *codec_specific_data_size = getword(bb);    /* Codec Specific Data Size */
        *codec_specific_data = (unsigned char *)rvalloc(*codec_specific_data, *codec_specific_data_size*sizeof(unsigned char), 1);
        for (i=0; i<*codec_specific_data_size; i++)
            (*codec_specific_data)[i] = getbyte(bb);/* Codec Specific Data */
        if (verbose>=1)
            rvmessage("asf: stream %d is type audio: %d channels %d-bit %d samples/sec", flags & 0x7f, number_of_channels, bits_per_sample, samples_per_second);
    } else if (strcmp(stringify_guid(stream_type, string), ASF_VIDEO_MEDIA)==0) {
        int encoded_image_width = getdword(bb);     /* Encoded Image Width */
        int encoded_image_height = getdword(bb);    /* Encoded Image Height */
        getbyte(bb);                                /* Reserved Flags */
        int format_data_size = getword(bb);         /* Format Data Size */
        //flushbits(bb, format_data_size*8);        /* Format Data */
        getdword(bb);                               /* Format Data Size */
        getdword(bb);                               /* Image Width */
        getdword(bb);                               /* Image Height */
        getword(bb);                                /* Reserved */
        int bits_per_pixel = getword(bb);           /* Bits Per Pixel Count */
        int compression_id = getdword(bb);          /* Compression ID */
        getdword(bb);                               /* Image Size */
        getdword(bb);                               /* Horizontal Pixels Per Meter */
        getdword(bb);                               /* Vertical Pixels Per Meter */
        getdword(bb);                               /* Colors Used Count */
        getdword(bb);                               /* Important Colors Count */
        *codec_specific_data_size = format_data_size - 40;
        assert(*codec_specific_data_size>=0);
        *codec_specific_data = (unsigned char *)rvalloc(*codec_specific_data, *codec_specific_data_size*sizeof(unsigned char), 1);
        for (i=0; i<*codec_specific_data_size; i++)
            (*codec_specific_data)[i] = getbyte(bb);/* Codec Specific Data */
        if (verbose>=1)
            rvmessage("asf: stream %d is type video, %dx%d %d-bit with fourcc %s", flags & 0x7f, encoded_image_width, encoded_image_height, bits_per_pixel, fourccname(compression_id));
    } else if (strcmp(stringify_guid(stream_type, string), ASF_COMMAND_MEDIA)==0) {
        flushbits(bb, type_specific_data_length*8);     /* Type-Specific Data */
        if (verbose>=1)
            rvmessage("asf: stream %d is type command", flags & 0x7f);
    } else if (strcmp(stringify_guid(stream_type, string), ASF_JFIF_MEDIA)==0) {
        flushbits(bb, type_specific_data_length*8);     /* Type-Specific Data */
        if (verbose>=1)
            rvmessage("asf: stream %d is type jfif", flags & 0x7f);
    } else if (strcmp(stringify_guid(stream_type, string), ASF_DEGRADABLE_JPEG_MEDIA)==0) {
        flushbits(bb, type_specific_data_length*8);     /* Type-Specific Data */
        if (verbose>=1)
            rvmessage("asf: stream %d is type degradable jpeg", flags & 0x7f);
    } else if (strcmp(stringify_guid(stream_type, string), ASF_FILE_TRANSFER_MEDIA)==0) {
        flushbits(bb, type_specific_data_length*8);     /* Type-Specific Data */
        if (verbose>=1)
            rvmessage("asf: stream %d is type file transfer", flags & 0x7f);
    } else if (strcmp(stringify_guid(stream_type, string), ASF_BINARY_MEDIA)==0) {
        flushbits(bb, type_specific_data_length*8);     /* Type-Specific Data */
        if (verbose>=1)
            rvmessage("asf: stream %d is type binary", flags & 0x7f);
    } else {
        flushbits(bb, type_specific_data_length*8);     /* Type-Specific Data */
        if (verbose>=1)
            rvmessage("asf: stream %d is type unknown: %s", flags & 0x7f, stringify_guid(stream_type, string));
    }
    flushbits(bb, error_correction_data_length*8);  /* Error Correction Data */
    return 0;
}

int parse_header_extension_object(struct bitbuf *bb, int verbose)
{
    getguid(bb);                                    /* Reserved Field 1 */
    getword(bb);                                    /* Reserved Field 2 */
    int header_extension_data_size = getdword(bb);  /* Header Extension Data Size */
    flushbits(bb, header_extension_data_size*8);
    if (verbose>=2)
        rvmessage("asf: %d bytes of header extension data", header_extension_data_size);
    return 0;
}

int parse_codec_list_object(struct bitbuf *bb, int verbose)
{
    int i, j;
    getguid(bb);                                    /* Reserved */
    int codec_entries_count = getdword(bb);         /* Codec Entries Count */
    for (i=1; i<=codec_entries_count; i++) {
        int type = getword(bb);
        int codec_name_length = getword(bb);        /* Codec Name Length */
        wchar_t *codec_name = (wchar_t *)rvalloc(NULL, codec_name_length+1*sizeof(wchar_t), 1);
        for (j=0; j<codec_name_length; j++)
            codec_name[j] = getword(bb);
        int codec_description_length = getword(bb); /* Codec Description Length */
        wchar_t *codec_description = (wchar_t *)rvalloc(NULL, codec_description_length+1*sizeof(wchar_t), 1);
        for (j=0; j<codec_description_length; j++)
            codec_description[j] = getword(bb);
        int codec_information_length = getword(bb); /* Codec Information Length */
        flushbits(bb, codec_information_length*8);
        if (verbose>=1)
            rvmessage("asf: %s stream is: %ls (%ls)", type==0x0001? "video" : type==0x0002? "audio" : "unknown", codec_name, codec_description);
        rvfree(codec_name);
        rvfree(codec_description);
    }
    return 0;
}

int parse_content_description_object(struct bitbuf *bb, int verbose)
{
    int i;
    int title_length = getword(bb);                 /* Title Length */
    int author_length = getword(bb);                /* Author Length */
    int copyright_length = getword(bb);             /* Copyright Length */
    int description_length = getword(bb);           /* Description Length */
    int rating_length = getword(bb);                /* Rating Length */
    wchar_t *title = (wchar_t *)rvalloc(NULL, title_length+1*sizeof(wchar_t), 1);
    wchar_t *author = (wchar_t *)rvalloc(NULL, author_length+1*sizeof(wchar_t), 1);
    wchar_t *copyright  = (wchar_t *)rvalloc(NULL, copyright_length+1*sizeof(wchar_t), 1);
    wchar_t *description = (wchar_t *)rvalloc(NULL, description_length+1*sizeof(wchar_t), 1);
    wchar_t *rating = (wchar_t *)rvalloc(NULL, rating_length+1*sizeof(wchar_t), 1);
    for (i=0; i<title_length/2; i++)
        title[i] = getword(bb);
    for (i=0; i<author_length/2; i++)
        author[i] = getword(bb);
    for (i=0; i<copyright_length/2; i++)
        copyright[i] = getword(bb);
    for (i=0; i<description_length/2; i++)
        description[i] = getword(bb);
    for (i=0; i<rating_length/2; i++)
        rating[i] = getword(bb);
    if (verbose>=1)
        rvmessage("title: %ls\nauthor: %ls\ncopyright: %ls\ndescription: %ls\nrating: %ls", title, author, copyright, description, rating);
    rvfree(title);
    rvfree(author);
    rvfree(copyright);
    rvfree(description);
    rvfree(rating);
    return 0;
}

int parse_stream_bitrate_properties_object(struct bitbuf *bb, int verbose)
{
    int i;
    int bitrate_records_count = getword(bb);        /* Bitrate Records Count */
    for (i=0; i<bitrate_records_count; i++) {
        int flags = getword(bb);                    /* Flags */
        int average_bitrate = getdword(bb);         /* Average Bitrate */
        if (verbose>=1)
            rvmessage("average bitrate for stream %d is %d bits/sec", flags & 0x7f, average_bitrate);
    }
    return 0;
}
