/*
 * Description: Functions related to the H.261 standard.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-06-23 13:26:19 $
 * Revision   : $Revision: 1.10 $
 * Copyright  : (c) 2007 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdlib.h>
#include <string.h>

#include "rvutil.h"
#include "rvcodec.h"
#include "rvh261.h"
#include "rvtransform.h"

static void decode_261_mvd(struct rv261macro *macro, int mbingob);

/*
 * h.261 lookups.
 */

/* gob number lookup tables for cif */
const int mbno_from_gobno[12] = { 0, 11, 66, 77, 132, 143, 198, 209, 264, 275, 330, 341 };
const int mbno_from_ingob[33] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54 };


/*
 * h.261 utility functions.
 */
int mtype_is_intra(mtype_t m)
{
    return (m==INTRA) || (m==INTRA_Q);
}

int mtype_is_quant(mtype_t m)
{
    return (m==INTRA_Q) || (m==INTER_Q) || (m==INTER_MC_Q) || (m==INTER_FIL_Q);
}

int mtype_is_mvd(mtype_t m)
{
    return (m>=INTER_MC_SKIP && m<=INTER_FIL_Q);
}

int mtype_is_cbp(mtype_t m)
{
    return (m==INTER) || (m==INTER_Q) || (m==INTER_MC) || (m==INTER_MC_Q) || (m==INTER_FIL) || (m==INTER_FIL_Q);
}

int mtype_is_fil(mtype_t m)
{
    return (m==INTER_FIL_SKIP) || (m==INTER_FIL) || (m==INTER_FIL_Q);
}

/*
 * h.261 header parsing functions.
 */

int parse_261_picture(struct bitbuf *bb, int verbose, int *width, int *height, int *hires)
{
    flushbits(bb, 5);                                                   /* temporal reference */
    int ptype = getbits(bb, 6);                                         /* type information */
    if (ptype&0x4) {
        *width = 352;
        *height = 288;
    } else {
        *width = 176;
        *height = 144;
    }
    *hires = ptype&0x2? 0 : 1;
    int pei = getbit(bb);                                               /* pei */
    while (pei) {
        int pspare = getbits8(bb);                                      /* pspare */
        if (verbose>=1)
            rvmessage("h.261 pspare byte: 0x%02x '%c'", pspare, pspare);
        /* undocumented feature found in pvrg reference software */
        /*if (pspare==0x8c) {
            *width = 352;
            *height = 240;
        }*/
        pei = getbit(bb);                                               /* pei */
    }
    return 0;
}

int parse_261_gob(struct bitbuf *bb, int verbose, int *gquant)
{
    flushbits(bb, 4);                                                   /* group number */
    *gquant = getbits(bb, 5);                                           /* gob quant */
    int gei = getbit(bb);                                               /* gei */
    while (gei) {
        int gspare = getbits8(bb);                                      /* gspare */
        if (verbose>=1)
            rvmessage("h.261 gspare byte: 0x%02x '%c'", gspare, gspare);
        gei = getbit(bb);                                               /* gei */
    }
    return 0;
}

/*
 * h.261 macroblock parsing functions.
 */

static int parse_261_mba(struct bitbuf *bb)
{
    if (showbits(bb, 1 ) == 1 ) /* 1           */ { flushbits(bb, 1 ); return 1;  }
    if (showbits(bb, 3 ) == 3 ) /* 011         */ { flushbits(bb, 3 ); return 2;  }
    if (showbits(bb, 3 ) == 2 ) /* 010         */ { flushbits(bb, 3 ); return 3;  }
    if (showbits(bb, 4 ) == 3 ) /* 0011        */ { flushbits(bb, 4 ); return 4;  }
    if (showbits(bb, 4 ) == 2 ) /* 0010        */ { flushbits(bb, 4 ); return 5;  }
    if (showbits(bb, 5 ) == 3 ) /* 00011       */ { flushbits(bb, 5 ); return 6;  }
    if (showbits(bb, 5 ) == 2 ) /* 00010       */ { flushbits(bb, 5 ); return 7;  }
    if (showbits(bb, 7 ) == 7 ) /* 0000111     */ { flushbits(bb, 7 ); return 8;  }
    if (showbits(bb, 7 ) == 6 ) /* 0000110     */ { flushbits(bb, 7 ); return 9;  }
    if (showbits(bb, 8 ) == 11) /* 00001011    */ { flushbits(bb, 8 ); return 10; }
    if (showbits(bb, 8 ) == 10) /* 00001010    */ { flushbits(bb, 8 ); return 11; }
    if (showbits(bb, 8 ) == 9 ) /* 00001001    */ { flushbits(bb, 8 ); return 12; }
    if (showbits(bb, 8 ) == 8 ) /* 00001000    */ { flushbits(bb, 8 ); return 13; }
    if (showbits(bb, 8 ) == 7 ) /* 00000111    */ { flushbits(bb, 8 ); return 14; }
    if (showbits(bb, 8 ) == 6 ) /* 00000110    */ { flushbits(bb, 8 ); return 15; }
    if (showbits(bb, 10) == 23) /* 0000010111  */ { flushbits(bb, 10); return 16; }
    if (showbits(bb, 10) == 22) /* 0000010110  */ { flushbits(bb, 10); return 17; }
    if (showbits(bb, 10) == 21) /* 0000010101  */ { flushbits(bb, 10); return 18; }
    if (showbits(bb, 10) == 20) /* 0000010100  */ { flushbits(bb, 10); return 19; }
    if (showbits(bb, 10) == 19) /* 0000010011  */ { flushbits(bb, 10); return 20; }
    if (showbits(bb, 10) == 18) /* 0000010010  */ { flushbits(bb, 10); return 21; }
    if (showbits(bb, 11) == 35) /* 00000100011 */ { flushbits(bb, 11); return 22; }
    if (showbits(bb, 11) == 34) /* 00000100010 */ { flushbits(bb, 11); return 23; }
    if (showbits(bb, 11) == 33) /* 00000100001 */ { flushbits(bb, 11); return 24; }
    if (showbits(bb, 11) == 32) /* 00000100000 */ { flushbits(bb, 11); return 25; }
    if (showbits(bb, 11) == 31) /* 00000011111 */ { flushbits(bb, 11); return 26; }
    if (showbits(bb, 11) == 30) /* 00000011110 */ { flushbits(bb, 11); return 27; }
    if (showbits(bb, 11) == 29) /* 00000011101 */ { flushbits(bb, 11); return 28; }
    if (showbits(bb, 11) == 28) /* 00000011100 */ { flushbits(bb, 11); return 29; }
    if (showbits(bb, 11) == 27) /* 00000011011 */ { flushbits(bb, 11); return 30; }
    if (showbits(bb, 11) == 26) /* 00000011010 */ { flushbits(bb, 11); return 31; }
    if (showbits(bb, 11) == 25) /* 00000011001 */ { flushbits(bb, 11); return 32; }
    if (showbits(bb, 11) == 24) /* 00000011000 */ { flushbits(bb, 11); return 33; }
    if (showbits(bb, 11) == 15) /* 00000001111 */ { flushbits(bb, 11); return 34; } /* mba stuffing */
    if (showbits16(bb) == 1 ) /* 0000000000000001 */               { return 35; } /* start code */
    return -1;
}

static mtype_t parse_261_mtype(struct bitbuf *bb)
{
    if (showbits(bb, 4 ) == 1 ) /* 0001       */ { flushbits(bb, 4 ); return INTRA;          }
    if (showbits(bb, 7 ) == 1 ) /* 0000001    */ { flushbits(bb, 7 ); return INTRA_Q;        }
    if (showbits(bb, 1 ) == 1 ) /* 1          */ { flushbits(bb, 1 ); return INTER;          }
    if (showbits(bb, 5 ) == 1 ) /* 00001      */ { flushbits(bb, 5 ); return INTER_Q;        }
    if (showbits(bb, 9 ) == 1 ) /* 000000001  */ { flushbits(bb, 9 ); return INTER_MC_SKIP;  }
    if (showbits(bb, 8 ) == 1 ) /* 00000001   */ { flushbits(bb, 8 ); return INTER_MC;       }
    if (showbits(bb, 10) == 1 ) /* 0000000001 */ { flushbits(bb, 10); return INTER_MC_Q;     }
    if (showbits(bb, 3 ) == 1 ) /* 001        */ { flushbits(bb, 3 ); return INTER_FIL_SKIP; }
    if (showbits(bb, 2 ) == 1 ) /* 01         */ { flushbits(bb, 2 ); return INTER_FIL;      }
    if (showbits(bb, 6 ) == 1 ) /* 000001     */ { flushbits(bb, 6 ); return INTER_FIL_Q;    }
    return MTYPE_ERROR;
}

static int parse_261_mvd(struct bitbuf *bb)
{
    if (showbits(bb, 11) == 25 ) /* 00000011001 */ { flushbits(bb, 11); return -16; }
    if (showbits(bb, 11) == 27 ) /* 00000011011 */ { flushbits(bb, 11); return -15; }
    if (showbits(bb, 11) == 29 ) /* 00000011101 */ { flushbits(bb, 11); return -14; }
    if (showbits(bb, 11) == 31 ) /* 00000011111 */ { flushbits(bb, 11); return -13; }
    if (showbits(bb, 11) == 33 ) /* 00000100001 */ { flushbits(bb, 11); return -12; }
    if (showbits(bb, 11) == 35 ) /* 00000100011 */ { flushbits(bb, 11); return -11; }
    if (showbits(bb, 10) == 19 ) /* 0000010011  */ { flushbits(bb, 10); return -10; }
    if (showbits(bb, 10) == 21 ) /* 0000010101  */ { flushbits(bb, 10); return -9;  }
    if (showbits(bb, 10) == 23 ) /* 0000010111  */ { flushbits(bb, 10); return -8;  }
    if (showbits(bb, 8 ) == 7  ) /* 00000111    */ { flushbits(bb, 8 ); return -7;  }
    if (showbits(bb, 8 ) == 9  ) /* 00001001    */ { flushbits(bb, 8 ); return -6;  }
    if (showbits(bb, 8 ) == 11 ) /* 00001011    */ { flushbits(bb, 8 ); return -5;  }
    if (showbits(bb, 7 ) == 7  ) /* 0000111     */ { flushbits(bb, 7 ); return -4;  }
    if (showbits(bb, 5 ) == 3  ) /* 00011       */ { flushbits(bb, 5 ); return -3;  }
    if (showbits(bb, 4 ) == 3  ) /* 0011        */ { flushbits(bb, 4 ); return -2;  }
    if (showbits(bb, 3 ) == 3  ) /* 011         */ { flushbits(bb, 3 ); return -1;  }
    if (showbits(bb, 1 ) == 1  ) /* 1           */ { flushbits(bb, 1 ); return 0;   }
    if (showbits(bb, 3 ) == 2  ) /* 010         */ { flushbits(bb, 3 ); return 1;   }
    if (showbits(bb, 4 ) == 2  ) /* 0010        */ { flushbits(bb, 4 ); return 2;   }
    if (showbits(bb, 5 ) == 2  ) /* 00010       */ { flushbits(bb, 5 ); return 3;   }
    if (showbits(bb, 7 ) == 6  ) /* 0000110     */ { flushbits(bb, 7 ); return 4;   }
    if (showbits(bb, 8 ) == 10 ) /* 00001010    */ { flushbits(bb, 8 ); return 5;   }
    if (showbits(bb, 8 ) == 8  ) /* 00001000    */ { flushbits(bb, 8 ); return 6;   }
    if (showbits(bb, 8 ) == 6  ) /* 00000110    */ { flushbits(bb, 8 ); return 7;   }
    if (showbits(bb, 10) == 22 ) /* 0000010110  */ { flushbits(bb, 10); return 8;   }
    if (showbits(bb, 10) == 20 ) /* 0000010100  */ { flushbits(bb, 10); return 9;   }
    if (showbits(bb, 10) == 18 ) /* 0000010010  */ { flushbits(bb, 10); return 10;  }
    if (showbits(bb, 11) == 34 ) /* 00000100010 */ { flushbits(bb, 11); return 11;  }
    if (showbits(bb, 11) == 32 ) /* 00000100000 */ { flushbits(bb, 11); return 12;  }
    if (showbits(bb, 11) == 30 ) /* 00000011110 */ { flushbits(bb, 11); return 13;  }
    if (showbits(bb, 11) == 28 ) /* 00000011100 */ { flushbits(bb, 11); return 14;  }
    if (showbits(bb, 11) == 26 ) /* 00000011010 */ { flushbits(bb, 11); return 15;  }
    return -255;
}

int parse_261_cbp(struct bitbuf *bb)
{
   if (showbits(bb, 3) == 7  ) /* 111       */ { flushbits(bb, 3); return 60; }
   if (showbits(bb, 4) == 13 ) /* 1101      */ { flushbits(bb, 4); return 4 ; }
   if (showbits(bb, 4) == 12 ) /* 1100      */ { flushbits(bb, 4); return 8 ; }
   if (showbits(bb, 4) == 11 ) /* 1011      */ { flushbits(bb, 4); return 16; }
   if (showbits(bb, 4) == 10 ) /* 1010      */ { flushbits(bb, 4); return 32; }
   if (showbits(bb, 5) == 19 ) /* 10011     */ { flushbits(bb, 5); return 12; }
   if (showbits(bb, 5) == 18 ) /* 10010     */ { flushbits(bb, 5); return 48; }
   if (showbits(bb, 5) == 17 ) /* 10001     */ { flushbits(bb, 5); return 20; }
   if (showbits(bb, 5) == 16 ) /* 10000     */ { flushbits(bb, 5); return 40; }
   if (showbits(bb, 5) == 15 ) /* 01111     */ { flushbits(bb, 5); return 28; }
   if (showbits(bb, 5) == 14 ) /* 01110     */ { flushbits(bb, 5); return 44; }
   if (showbits(bb, 5) == 13 ) /* 01101     */ { flushbits(bb, 5); return 52; }
   if (showbits(bb, 5) == 12 ) /* 01100     */ { flushbits(bb, 5); return 56; }
   if (showbits(bb, 5) == 11 ) /* 01011     */ { flushbits(bb, 5); return 1 ; }
   if (showbits(bb, 5) == 10 ) /* 01010     */ { flushbits(bb, 5); return 61; }
   if (showbits(bb, 5) == 9  ) /* 01001     */ { flushbits(bb, 5); return 2 ; }
   if (showbits(bb, 5) == 8  ) /* 01000     */ { flushbits(bb, 5); return 62; }
   if (showbits(bb, 6) == 15 ) /* 001111    */ { flushbits(bb, 6); return 24; }
   if (showbits(bb, 6) == 14 ) /* 001110    */ { flushbits(bb, 6); return 36; }
   if (showbits(bb, 6) == 13 ) /* 001101    */ { flushbits(bb, 6); return 3 ; }
   if (showbits(bb, 6) == 12 ) /* 001100    */ { flushbits(bb, 6); return 63; }
   if (showbits(bb, 7) == 23 ) /* 0010111   */ { flushbits(bb, 7); return 5 ; }
   if (showbits(bb, 7) == 22 ) /* 0010110   */ { flushbits(bb, 7); return 9 ; }
   if (showbits(bb, 7) == 21 ) /* 0010101   */ { flushbits(bb, 7); return 17; }
   if (showbits(bb, 7) == 20 ) /* 0010100   */ { flushbits(bb, 7); return 33; }
   if (showbits(bb, 7) == 19 ) /* 0010011   */ { flushbits(bb, 7); return 6 ; }
   if (showbits(bb, 7) == 18 ) /* 0010010   */ { flushbits(bb, 7); return 10; }
   if (showbits(bb, 7) == 17 ) /* 0010001   */ { flushbits(bb, 7); return 18; }
   if (showbits(bb, 7) == 16 ) /* 0010000   */ { flushbits(bb, 7); return 34; }
   if (showbits(bb, 8) == 31 ) /* 00011111  */ { flushbits(bb, 8); return 7 ; }
   if (showbits(bb, 8) == 30 ) /* 00011110  */ { flushbits(bb, 8); return 11; }
   if (showbits(bb, 8) == 29 ) /* 00011101  */ { flushbits(bb, 8); return 19; }
   if (showbits(bb, 8) == 28 ) /* 00011100  */ { flushbits(bb, 8); return 35; }
   if (showbits(bb, 8) == 27 ) /* 00011011  */ { flushbits(bb, 8); return 13; }
   if (showbits(bb, 8) == 26 ) /* 00011010  */ { flushbits(bb, 8); return 49; }
   if (showbits(bb, 8) == 25 ) /* 00011001  */ { flushbits(bb, 8); return 21; }
   if (showbits(bb, 8) == 24 ) /* 00011000  */ { flushbits(bb, 8); return 41; }
   if (showbits(bb, 8) == 23 ) /* 00010111  */ { flushbits(bb, 8); return 14; }
   if (showbits(bb, 8) == 22 ) /* 00010110  */ { flushbits(bb, 8); return 50; }
   if (showbits(bb, 8) == 21 ) /* 00010101  */ { flushbits(bb, 8); return 22; }
   if (showbits(bb, 8) == 20 ) /* 00010100  */ { flushbits(bb, 8); return 42; }
   if (showbits(bb, 8) == 19 ) /* 00010011  */ { flushbits(bb, 8); return 15; }
   if (showbits(bb, 8) == 18 ) /* 00010010  */ { flushbits(bb, 8); return 51; }
   if (showbits(bb, 8) == 17 ) /* 00010001  */ { flushbits(bb, 8); return 23; }
   if (showbits(bb, 8) == 16 ) /* 00010000  */ { flushbits(bb, 8); return 43; }
   if (showbits(bb, 8) == 15 ) /* 00001111  */ { flushbits(bb, 8); return 25; }
   if (showbits(bb, 8) == 14 ) /* 00001110  */ { flushbits(bb, 8); return 37; }
   if (showbits(bb, 8) == 13 ) /* 00001101  */ { flushbits(bb, 8); return 26; }
   if (showbits(bb, 8) == 12 ) /* 00001100  */ { flushbits(bb, 8); return 38; }
   if (showbits(bb, 8) == 11 ) /* 00001011  */ { flushbits(bb, 8); return 29; }
   if (showbits(bb, 8) == 10 ) /* 00001010  */ { flushbits(bb, 8); return 45; }
   if (showbits(bb, 8) == 9  ) /* 00001001  */ { flushbits(bb, 8); return 53; }
   if (showbits(bb, 8) == 8  ) /* 00001000  */ { flushbits(bb, 8); return 57; }
   if (showbits(bb, 8) == 7  ) /* 00000111  */ { flushbits(bb, 8); return 30; }
   if (showbits(bb, 8) == 6  ) /* 00000110  */ { flushbits(bb, 8); return 46; }
   if (showbits(bb, 8) == 5  ) /* 00000101  */ { flushbits(bb, 8); return 54; }
   if (showbits(bb, 8) == 4  ) /* 00000100  */ { flushbits(bb, 8); return 58; }
   if (showbits(bb, 9) == 7  ) /* 000000111 */ { flushbits(bb, 9); return 31; }
   if (showbits(bb, 9) == 6  ) /* 000000110 */ { flushbits(bb, 9); return 47; }
   if (showbits(bb, 9) == 5  ) /* 000000101 */ { flushbits(bb, 9); return 55; }
   if (showbits(bb, 9) == 4  ) /* 000000100 */ { flushbits(bb, 9); return 59; }
   if (showbits(bb, 9) == 3  ) /* 000000011 */ { flushbits(bb, 9); return 27; }
   if (showbits(bb, 9) == 2  ) /* 000000010 */ { flushbits(bb, 9); return 39; }
   return -1;
}

static struct rvtcoeff parse_261_tcoeff(struct bitbuf *bb, int first)
{
    struct rvtcoeff tcoeff;
    tcoeff.run   = -1;
    tcoeff.level = 0;
    tcoeff.sign  = 0;

if (first) {
    if (showbits(bb, 1 ) == 1  ) /* 1s             */ { flushbits(bb, 1 ); tcoeff.run =  0 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
} else {
    if (showbits(bb, 2 ) == 2  ) /* 10             */ { flushbits(bb, 2 ); tcoeff.run =  0 ; tcoeff.level =  0; return tcoeff; } /* EOB */
    if (showbits(bb, 2 ) == 3  ) /* 11s            */ { flushbits(bb, 2 ); tcoeff.run =  0 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
}
    if (showbits(bb, 4 ) == 4  ) /* 0100s          */ { flushbits(bb, 4 ); tcoeff.run =  0 ; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 5 ) == 5  ) /* 00101s         */ { flushbits(bb, 5 ); tcoeff.run =  0 ; tcoeff.level =  3; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 7 ) == 6  ) /* 0000110s       */ { flushbits(bb, 7 ); tcoeff.run =  0 ; tcoeff.level =  4; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 38 ) /* 00100110s      */ { flushbits(bb, 8 ); tcoeff.run =  0 ; tcoeff.level =  5; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 33 ) /* 00100001s      */ { flushbits(bb, 8 ); tcoeff.run =  0 ; tcoeff.level =  6; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 10) == 10 ) /* 0000001010s    */ { flushbits(bb, 10); tcoeff.run =  0 ; tcoeff.level =  7; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 29 ) /* 000000011101s  */ { flushbits(bb, 12); tcoeff.run =  0 ; tcoeff.level =  8; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 24 ) /* 000000011000s  */ { flushbits(bb, 12); tcoeff.run =  0 ; tcoeff.level =  9; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 19 ) /* 000000010011s  */ { flushbits(bb, 12); tcoeff.run =  0 ; tcoeff.level = 10; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 16 ) /* 000000010000s  */ { flushbits(bb, 12); tcoeff.run =  0 ; tcoeff.level = 11; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 26 ) /* 0000000011010s */ { flushbits(bb, 13); tcoeff.run =  0 ; tcoeff.level = 12; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 25 ) /* 0000000011001s */ { flushbits(bb, 13); tcoeff.run =  0 ; tcoeff.level = 13; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 24 ) /* 0000000011000s */ { flushbits(bb, 13); tcoeff.run =  0 ; tcoeff.level = 14; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 23 ) /* 0000000010111s */ { flushbits(bb, 13); tcoeff.run =  0 ; tcoeff.level = 15; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 3 ) == 3  ) /* 011s           */ { flushbits(bb, 3 ); tcoeff.run =  1 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 6 ) == 6  ) /* 000110s        */ { flushbits(bb, 6 ); tcoeff.run =  1 ; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 37 ) /* 00100101s      */ { flushbits(bb, 8 ); tcoeff.run =  1 ; tcoeff.level =  3; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 10) == 12 ) /* 0000001100s    */ { flushbits(bb, 10); tcoeff.run =  1 ; tcoeff.level =  4; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 27 ) /* 000000011011s  */ { flushbits(bb, 12); tcoeff.run =  1 ; tcoeff.level =  5; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 22 ) /* 0000000010110s */ { flushbits(bb, 13); tcoeff.run =  1 ; tcoeff.level =  6; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 21 ) /* 0000000010101s */ { flushbits(bb, 13); tcoeff.run =  1 ; tcoeff.level =  7; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 4 ) == 5  ) /* 0101s          */ { flushbits(bb, 4 ); tcoeff.run =  2 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 7 ) == 4  ) /* 0000100s       */ { flushbits(bb, 7 ); tcoeff.run =  2 ; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 10) == 11 ) /* 0000001011s    */ { flushbits(bb, 10); tcoeff.run =  2 ; tcoeff.level =  3; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 20 ) /* 000000010100s  */ { flushbits(bb, 12); tcoeff.run =  2 ; tcoeff.level =  4; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 20 ) /* 0000000010100s */ { flushbits(bb, 13); tcoeff.run =  2 ; tcoeff.level =  5; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 5 ) == 7  ) /* 00111s         */ { flushbits(bb, 5 ); tcoeff.run =  3 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 36 ) /* 00100100s      */ { flushbits(bb, 8 ); tcoeff.run =  3 ; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 28 ) /* 000000011100s  */ { flushbits(bb, 12); tcoeff.run =  3 ; tcoeff.level =  3; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 19 ) /* 0000000010011s */ { flushbits(bb, 13); tcoeff.run =  3 ; tcoeff.level =  4; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 5 ) == 6  ) /* 00110s         */ { flushbits(bb, 5 ); tcoeff.run =  4 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 10) == 15 ) /* 0000001111s    */ { flushbits(bb, 10); tcoeff.run =  4 ; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 18 ) /* 000000010010s  */ { flushbits(bb, 12); tcoeff.run =  4 ; tcoeff.level =  3; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 6 ) == 7  ) /* 000111s        */ { flushbits(bb, 6 ); tcoeff.run =  5 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 10) == 9  ) /* 0000001001s    */ { flushbits(bb, 10); tcoeff.run =  5 ; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 18 ) /* 0000000010010s */ { flushbits(bb, 13); tcoeff.run =  5 ; tcoeff.level =  3; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 6 ) == 5  ) /* 000101s        */ { flushbits(bb, 6 ); tcoeff.run =  6 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 30 ) /* 000000011110s  */ { flushbits(bb, 12); tcoeff.run =  6 ; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 6 ) == 4  ) /* 000100s        */ { flushbits(bb, 6 ); tcoeff.run =  7 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 21 ) /* 000000010101s  */ { flushbits(bb, 12); tcoeff.run =  7 ; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 7 ) == 7  ) /* 0000111s       */ { flushbits(bb, 7 ); tcoeff.run =  8 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 17 ) /* 000000010001s  */ { flushbits(bb, 12); tcoeff.run =  8 ; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 7 ) == 5  ) /* 0000101s       */ { flushbits(bb, 7 ); tcoeff.run =  9 ; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 17 ) /* 0000000010001s */ { flushbits(bb, 13); tcoeff.run =  9 ; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 39 ) /* 00100111s      */ { flushbits(bb, 8 ); tcoeff.run =  10; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 16 ) /* 0000000010000s */ { flushbits(bb, 13); tcoeff.run =  10; tcoeff.level =  2; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 35 ) /* 00100011s      */ { flushbits(bb, 8 ); tcoeff.run =  11; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 34 ) /* 00100010s      */ { flushbits(bb, 8 ); tcoeff.run =  12; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 8 ) == 32 ) /* 00100000s      */ { flushbits(bb, 8 ); tcoeff.run =  13; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 10) == 14 ) /* 0000001110s    */ { flushbits(bb, 10); tcoeff.run =  14; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 10) == 13 ) /* 0000001101s    */ { flushbits(bb, 10); tcoeff.run =  15; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 10) == 8  ) /* 0000001000s    */ { flushbits(bb, 10); tcoeff.run =  16; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 31 ) /* 000000011111s  */ { flushbits(bb, 12); tcoeff.run =  17; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 26 ) /* 000000011010s  */ { flushbits(bb, 12); tcoeff.run =  18; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 25 ) /* 000000011001s  */ { flushbits(bb, 12); tcoeff.run =  19; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 23 ) /* 000000010111s  */ { flushbits(bb, 12); tcoeff.run =  20; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 12) == 22 ) /* 000000010110s  */ { flushbits(bb, 12); tcoeff.run =  21; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 31 ) /* 0000000011111s */ { flushbits(bb, 13); tcoeff.run =  22; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 30 ) /* 0000000011110s */ { flushbits(bb, 13); tcoeff.run =  23; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 29 ) /* 0000000011101s */ { flushbits(bb, 13); tcoeff.run =  24; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 28 ) /* 0000000011100s */ { flushbits(bb, 13); tcoeff.run =  25; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 13) == 27 ) /* 0000000011011s */ { flushbits(bb, 13); tcoeff.run =  26; tcoeff.level =  1; tcoeff.sign = getbit(bb); return tcoeff; }
    if (showbits(bb, 6 ) == 1  ) /* 000001         */ { /* escape */
        flushbits(bb, 6 );
        tcoeff.run = getbits(bb, 6);
        tcoeff.level = getbits8(bb);
        if (tcoeff.level>127)
            tcoeff.level -= 256;
    }
    return tcoeff;
}

static int parse_261_macroblock(struct bitbuf *bb, struct rv261macro *macro, int picno, int mbno)
{
    /* parse macroblock layer */
    macro->mtype = parse_261_mtype(bb);
    if (macro->mtype<0) {
        rvmessage("failed to parse mtype in picture %d, macroblock %d", picno, mbno);
        return -1;
    }

    if (mtype_is_quant(macro->mtype))
        macro->mquant = getbits(bb, 5);

    if (mtype_is_mvd(macro->mtype)) {
        macro->mvd.x = parse_261_mvd(bb);
        macro->mvd.y = parse_261_mvd(bb);
    }
    if (macro->mvd.x<-16) {
        rvmessage("failed to parse mvdx in picture %d, macroblock %d", picno, mbno);
        return -1;
    }
    if (macro->mvd.y<-16) {
        rvmessage("failed to parse mvdy in picture %d, macroblock %d", picno, mbno);
        return -1;
    }

    if (mtype_is_cbp(macro->mtype))
        macro->cbp = parse_261_cbp(bb);
    else if (mtype_is_intra(macro->mtype))
        macro->cbp = 63;
    else
        macro->cbp = 0;
    if (macro->cbp<0) {
        rvmessage("failed to parse cbp in picture %d, macroblock %d", picno, mbno);
        return -1;
    }

    /* parse block layer */
    int block;
    for (block=0; block<6; block++) {
        if ((0x20>>block) & macro->cbp) {
            struct rvtcoeff tcoeff;
            int coeff=0;

            /* parse intra dc tcoeff */
            if (mtype_is_intra(macro->mtype)) {
                int intradc = getbits8(bb);
                if (intradc==255)
                    intradc = 128;
                macro->tcoeff[block][coeff++] = intradc;
            }

            /* parse remaining tcoeffs */
            do {
                /* read next tcoeff */
                tcoeff = parse_261_tcoeff(bb, !coeff);
                if (tcoeff.run<0) {
                    rvmessage("failed to parse tcoeff in picture %d, macroblock %d, block %d, coeff %d", picno, mbno, block, coeff);
                    rvmessage("showbits=%06x", showbits24(bb));
                    return -1;
                }

                /* exit loop if eob NOTE h.261 requires an eob */
                if (tcoeff.run==0 && tcoeff.level==0)
                    break;

                /* process tcoeff */
                coeff += tcoeff.run;
                if (coeff>=64) {
                    rvmessage("too many coefficients in picture %d, macroblock %d, block %d: %d", picno, mbno, block, coeff);
                    return -1;
                }
                macro->tcoeff[block][coeff++] = tcoeff.sign ? -tcoeff.level : tcoeff.level;
            } while (1);
        }
    }
    return 0;
}

struct rv261picture *parse_h261(struct bitbuf *bb, const char *filename, int verbose)
{
    /* allocate array of pictures */
    size_t size = 1024*sizeof(struct rv261picture);
    struct rv261picture *picture = (struct rv261picture *)rvalloc(NULL, size, 1);

    /* loop over pictures in sequence */
    int picno = 0;
    do {
        /* search for a picture start code */
        int start = showbits(bb, 20);
        while (start!=0x00010) {
            /* move along by one bit */
            flushbits(bb, 1);
            if (eofbits(bb))
                break;
            start = showbits(bb, 20);
        }
        flushbits(bb, 20);

        /* check for end of bitstream */
        if (eofbits(bb))
            break;

        /* parse picture layer */
        int hires;
        if (parse_261_picture(bb, verbose, &picture[picno].width, &picture[picno].height, &hires)<0)
            rvexit("failed to parse h.261 picture layer in file \"%s\"", filename);

        /* allocate array of macroblocks */
        size = picture[picno].width*picture[picno].height/256*sizeof(struct rv261macro);
        picture[picno].macro = (struct rv261macro *)rvalloc(NULL, size, 1);

        /* loop over gobs in picture */
        int gobno;
        int numgobs = picture[picno].width==176? 3 : 12;
        for (gobno=0; gobno<numgobs; gobno++) {
            int mbno;
            int prev_quant;

            /* check for early end of bistream */
            if (eofbits(bb))
                break;

            /* calculate macroblock number */
            if (picture[picno].width==176)
                mbno = gobno*33;
            else
                mbno = mbno_from_gobno[gobno];

            /* search for a gob start code (in the event of a parsing error) */
            int searchbits = 0;
            start = showbits16(bb);
            if (start!=0x0001) {
                /* move along by one bit */
                searchbits += flushbits(bb, 1);
                start = showbits16(bb);
                break;
            }
            flushbits(bb, 16);
            if (searchbits)
                rvmessage("searched %d bits to find gob start code in file \"%s\", picture %d, gob %d", searchbits, filename, picno, gobno);

            /* TODO check gob number */

            /* parse gob layer */
            int gquant;
            if (parse_261_gob(bb, verbose, &gquant)<0) {
                rvexit("failed to parse h.261 gob layer in file \"%s\", picture %d, gob %d", filename, picno, gobno);
                break;
            }
            prev_quant = gquant;

            /* loop over macroblocks in gob */
            int mbingob = -1;
            while (mbingob<32) {
                int mba;
                struct rv261macro *macro;

                /* read next macroblock mba */
                do {
                    mba = parse_261_mba(bb);
                } while (mba==34); /* discard mba stuffing */
                if (mba==35)
                    /* end of gob */
                    break;
                if (mba<0) {
                    rvmessage("failed to parse mba in picture %d, after macroblock %d", picno, mbno);
                    break;
                }

                /* move over skipped macroblocks */
                mbingob += mba;

                /* calculate macroblock number */
                if (picture[picno].width==176)
                    mbno = gobno*33 + mbingob;
                else
                    mbno = mbno_from_gobno[gobno] + mbno_from_ingob[mbingob];
                macro = picture[picno].macro + mbno;

                /* initialise macroblock */
                macro->mba = mba;
                macro->mquant = prev_quant;

                /* parse macroblock layer */
                if (parse_261_macroblock(bb, macro, picno, mbno)<0) {
                    rvmessage("failed to parse h.261 macroblock in file \"%s\", picture %d, gob number %d, macroblock in gob %d, macroblock %d", filename, picno, gobno, mbingob, mbno);
                    break;
                }

                /* perform motion vector inverse prediction */
                decode_261_mvd(macro, mbingob);

                /* remember previous macroblock quant */
                prev_quant = picture[picno].macro[mbno].mquant;
            }
        }

        /* end of picture */
        picno++;

    } while (!eofbits(bb));

    return picture;
}

/*
 * h.261 decoding functions.
 */

static void decode_261_mvd(struct rv261macro *macro, int mbingob)
{
    static struct mv_t mvp;

    /* reset motion vector predictors */
    if (mbingob==0 || mbingob==11 || mbingob==22 || macro->mba!=1)
        mvp.x = mvp.y = 0;

    if (mtype_is_mvd(macro->mtype)) {
        /* inverse predict motion vectors */
        macro->mv.x = macro->mvd.x + mvp.x;
        macro->mv.y = macro->mvd.y + mvp.y;

        /* wrap around motion vectors */
        if (macro->mv.x > 15)
            macro->mv.x -= 32;
        if (macro->mv.x < -16)
            macro->mv.x += 32;
        if (macro->mv.y > 15)
            macro->mv.y -= 32;
        if (macro->mv.y < -16)
            macro->mv.y += 32;
        mvp.x = macro->mv.x;
        mvp.y = macro->mv.y;
    } else
        mvp.x = mvp.y = 0;

    /* calculate chroma motion vector */
    macro->mvc.x = (abs(macro->mv.x) >> 1) * sign(macro->mv.x);
    macro->mvc.y = (abs(macro->mv.y) >> 1) * sign(macro->mv.y);
}

static void decode_261_iquant(struct rv261macro *macro, int quant)
{
    int block;

    /* loop over blocks */
    for (block=0; block<6; block++) {
        /* skip not coded blocks */
        if ((0x20>>block) & macro->cbp) {
            int index;

            /* reconstruct coeffs TODO remember EOB to shorten loop */
            if (quant&1) {
                /* quant odd */
                for (index=0; index<64; index++) {
                    int level = macro->tcoeff[block][zigzag_inverse[index]];
                    int sign  = level > 0? 1 : level < 0? -1 : 0;
                    macro->coeff[block][index] = quant * (2*level + sign);
                }
            } else {
                /* quant even */
                for (index=0; index<64; index++) {
                    int level = macro->tcoeff[block][zigzag_inverse[index]];
                    int sign  = level > 0? 1 : level < 0? -1 : 0;
                    macro->coeff[block][index] = quant * (2*level + sign) - sign;
                }
            }

            /* reconstruct intra dc coeff */
            if (mtype_is_intra(macro->mtype))
                macro->coeff[block][0] = (uint8_t) macro->tcoeff[block][0] * 8;

            /* clip coeffs */
            for (index=0; index<64; index++) {
                if (macro->coeff[block][index] > 2047)
                    macro->coeff[block][index] = 2047;
                else if (macro->coeff[block][index] < -2048)
                    macro->coeff[block][index] = -2048;
            }

        }
    }
}

static void decode_261_idct(struct rv261macro *macro)
{
    int block;

    /* loop over blocks */
    for (block=0; block<6; block++)
        /* skip not coded blocks */
        if ((0x20>>block) & macro->cbp)
            reference_idct(macro->coeff[block], macro->error[block]);
            //chen_idct(macro->coeff[block], macro->error[block]);
}

static void decode_261_fetch(struct rv261macro *macro, int mbx, int mby, int width, int height, sample_t *reference)
{
    int block, i;
    sample_t *p;

    /* fetch luma motion compensated reference data */
    for (block=0; block<4; block++) {
        p = reference + 16*mby*width + 16*mbx + 8*(block/2)*width + 8*(block%2) + macro->mv.y*width + macro->mv.x;
        for (i=0; i<64; i++)
            macro->sample[block][i] = *(p + (i/8)*width + i%8);
    }

    /* fetch chroma motion compensated reference data */
    p = reference + width*height + 8*mby*width/2 + 8*mbx + macro->mvc.y*width/2 + macro->mvc.x;
    for (i=0; i<64; i++)
        macro->sample[4][i] = *(p + (i/8)*width/2 + i%8);
    p = reference + width*height*5/4 + 8*mby*width/2 + 8*mbx + macro->mvc.y*width/2 + macro->mvc.x;
    for (i=0; i<64; i++)
        macro->sample[5][i] = *(p + (i/8)*width/2 + i%8);
}

static void decode_261_filter(struct rv261macro *macro)
{
    int block;
    int x, y;
    int sample[64];
    int output[64];

    for (block=0; block<6; block++) {
        /* apply horizontal filter */
        for (y=0; y<8; y++) {
            sample[8*y+0] = macro->sample[block][8*y+0] * 4;
            for (x=1; x<7; x++) {
                sample[8*y+x]  = macro->sample[block][8*y+x-1];
                sample[8*y+x] += macro->sample[block][8*y+x] * 2;
                sample[8*y+x] += macro->sample[block][8*y+x+1];
            }
            sample[8*y+7] = macro->sample[block][8*y+7] * 4;
        }
        /* apply vertical filter */
        for (x=0; x<8; x++) {
            output[8*0+x] = sample[8*0+x] * 4;
            for (y=1; y<7; y++) {
                output[8*y+x]  = sample[8*y+x-8];
                output[8*y+x] += sample[8*y+x] * 2;
                output[8*y+x] += sample[8*y+x+8];
            }
            output[8*7+x] = sample[8*7+x] * 4;
        }
        /* renormalise and round */
        for (x=0; x<64; x++)
            macro->sample[block][x] = (sample_t) ((output[x] + 8) >> 4);
    }
}

static void decode_261_sample(struct rv261macro *macro)
{
    int block, i;

    for (block=0; block<6; block++)
        /* no point checking cbp */
        for (i=0; i<64; i++)
            macro->sample[block][i] = (sample_t) clip(macro->error[block][i], 0, 255);

}

static void decode_261_recon(struct rv261macro *macro)
{
    int block, i;

    for (block=0; block<6; block++)
        /* skip not coded blocks */
        if ((0x20>>block) & macro->cbp)
            for (i=0; i<64; i++) {
                int sample = (int)macro->error[block][i] + (int)macro->sample[block][i];
                macro->sample[block][i] = (sample_t) clip(sample, 0, 255);
            }

}

sample_t *decode_261(struct rv261picture *picture, sample_t *reference)
{
    int mbx, mby;

    int width = picture->width;
    int height = picture->height;

    /* allocate picture */
    size_t size = width*height*3/2;
    unsigned char *frame = (unsigned char *)rvalloc(NULL, size*sizeof(sample_t), 0);

    /* loop over macroblocks */
    for (mby=0; mby<height/16; mby++) {
        for (mbx=0; mbx<width/16; mbx++) {
            struct rv261macro *macro = picture->macro + mby*width/16 + mbx;

            /* TODO optimise for skipped macroblocks */

            /* inverse quantise macroblock */
            decode_261_iquant(macro, macro->mquant);

            /* inverse transform macroblock */
            decode_261_idct(macro);

            /* motion compensate macroblock */
            if (!mtype_is_intra(macro->mtype) && reference!=NULL)
                decode_261_fetch(macro, mbx, mby, width, height, reference);

            /* filter macroblock */
            if (mtype_is_fil(macro->mtype))
                decode_261_filter(macro);

            /* reconstruct macroblock */
            if (mtype_is_intra(macro->mtype))
                decode_261_sample(macro);
            else
                decode_261_recon(macro);

            /* copy macroblock into frame */
            copy_macroblock_into_picture(frame, width, height, 1, mbx, mby, macro->sample);
        }
    }

    return frame;
}
