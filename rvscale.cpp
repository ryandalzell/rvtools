/*
 * Description: Scale video sequences with various resampling algorithms.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2010-01-22 15:52:40 $
 * Revision   : $Revision: 1.29 $
 * Copyright  : (c) 2005 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include "getopt.h"

#include "rvutil.h"
#include "rvimage.h"
#include "rvy4m.h"

const char *appname;

void usage(int exitcode)
{
    fprintf(stderr, "%s: arbitrary up- or down-scale raw yuv video\n", appname);
    fprintf(stderr, "usage: %s [options] [<file>] [<file>...]\n", appname);
    fprintf(stderr, "  -a, --algorithm     : resampling algorithm (default: blackman)\n");
    fprintf(stderr, "  (algorithms : nearest, bilinear, bicubic, bartlett, vonhann, hamming, blackman, blackmanharris, blackmannuttall, kaiser)\n");
    fprintf(stderr, "  -p, --phases        : number of phases in polyphase filter\n");
    fprintf(stderr, "  -t, --taps          : number of taps in polyphase filter\n");
    fprintf(stderr, "  -w, --input-width   : width of input image in pels\n");
    fprintf(stderr, "  -h, --input-height  : height of input image in lines\n");
    fprintf(stderr, "  -s, --input-size    : input image size format\n");
    fprintf(stderr, "     (formats : %s)\n", rvformats);
    fprintf(stderr, "  -c, --input-chroma  : input image chroma format (default: 420)\n");
    fprintf(stderr, "     (formats : %s)\n", rvchromas);
    fprintf(stderr, "  -f, --fourcc        : image pixel format fourcc (default: I420)\n");
    fprintf(stderr, "     (formats : %s)\n", rvfourccs);
    fprintf(stderr, "  -W, --output-width  : width of output image in pels\n");
    fprintf(stderr, "  -H, --output-height : height of output image in lines\n");
    fprintf(stderr, "  -S, --output-size   : output image size format (default: same as input)\n");
    fprintf(stderr, "  -C, --output-chroma : output image chroma format (default: 420)\n");
    fprintf(stderr, "  -n, --numframes     : number of frames (default: all)\n");
    fprintf(stderr, "  -o, --output        : write output to file\n");
    fprintf(stderr, "  -4, --yuv4mpeg      : write yuv4mpeg format to output (default: raw yuv format)\n");
    fprintf(stderr, "  -q, --quiet         : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "  -v, --verbose       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "  --                  : disable argument processing\n");
    fprintf(stderr, "  -h, --usage, --help : print this usage message\n");
    exit(exitcode);
}

int main(int argc, char *argv[])
{
    FILE *filein = stdin;
    FILE *fileout = stdout;
    char *filename[RV_MAXFILES] = {0};
    char *outfile = NULL;
    int fileindex = 0;
    int numfiles = 0;
    int totframes = 0;

    /* command line defaults */
    int inp_width = 0;
    int inp_height = 0;
    const char *inp_format = NULL;
    const char *inp_chroma = "420";
    int out_width = 0;
    int out_height = 0;
    const char *out_format = NULL;
    const char *out_chroma = "420";
    const char *algorithm = "blackman";
    enum algo_t algo = NOALGO;
    const char *fccode = NULL;
    unsigned int inp_fourcc = I420;
    unsigned int out_fourcc = 0;
    int phases = 8;
    int taps = 8;
    int numframes = -1;
    int raw_inp = 1;
    int yuv4mpeg = 0;
    int verbose = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"output-width",  1, NULL, 'W'},
            {"output-height", 1, NULL, 'H'},
            {"input-size",    1, NULL, 's'},
            {"output-size",   1, NULL, 'S'},
            {"input-chroma",  1, NULL, 'c'},
            {"output-chroma", 1, NULL, 'C'},
            {"fourcc",        1, NULL, 'f'},
            {"algorithm",     1, NULL, 'a'},
            {"phases",        1, NULL, 'p'},
            {"taps",          1, NULL, 't'},
            {"numframes",     1, NULL, 'n'},
            {"output",        1, NULL, 'o'},
            {"yuv4mpeg",      0, NULL, '4'},
            {"quiet",         0, NULL, 'q'},
            {"verbose",       0, NULL, 'v'},
            {"usage",         0, NULL, 'h'},
            {"help",          0, NULL, 'h'},
            {NULL,            0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "W:H:s:S:c:C:f:a:p:t:n:o:4qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 'W':
                out_width = atoi(optarg);
                if (out_width<=0)
                    rvexit("invalid value for output width: %d", out_width);
                break;

            case 'H':
                out_height = atoi(optarg);
                if (out_height<=0)
                    rvexit("invalid value for output height: %d", out_height);
                break;

            case 's':
                inp_format = optarg;
                break;

            case 'S':
                out_format = optarg;
                break;

            case 'c':
                inp_chroma = optarg;
                break;

            case 'C':
                out_chroma = optarg;
                break;

            case 'f':
                fccode = optarg;
                break;

            case 'a':
                algorithm = optarg;
                break;

            case 'p':
                phases = atoi(optarg);
                if (phases<=0)
                    rvexit("invalid number of phases: %d", phases);
                break;

            case 't':
                taps = atoi(optarg);
                if (taps<=0)
                    rvexit("invalid number of taps: %d", taps);
                break;

            case 'n':
                numframes = atoi(optarg);
                if (numframes<=0)
                    rvexit("invalid number of frames: %d", numframes);
                break;

            case '4':
                yuv4mpeg = 1;
                break;

            case 'o':
                outfile = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < RV_MAXFILES)
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    /* determine resampling algorithm */
    if (strcasecmp(algorithm,"nearest")==0)
        algo = NEAREST;
    else if (strcasecmp(algorithm,"bilinear")==0)
        algo = BILINEAR;
    else if (strcasecmp(algorithm,"bicubic")==0)
        algo = BICUBIC;
    else if (strcasecmp(algorithm,"bartlett")==0)
        algo = BARTLETT;
    else if (strcasecmp(algorithm,"vonhann")==0)
        algo = VONHANN;
    else if (strcasecmp(algorithm,"hamming")==0)
        algo = HAMMING;
    else if (strcasecmp(algorithm,"blackman")==0)
        algo = BLACKMAN;
    else if (strcasecmp(algorithm,"blackmanharris")==0)
        algo = BLACKMANHARRIS;
    else if (strcasecmp(algorithm,"blackmannuttall")==0)
        algo = BLACKMANNUTTALL;
    else if (strcasecmp(algorithm,"kaiser")==0)
        algo = KAISER;
    else
        rvexit("unknown resampling algorithm: %s", algorithm);

    /* sanity check resampling parameters */
    if (algo==BICUBIC)
        taps = 4;
    if ((algo>=BARTLETT || algo<=KAISER) && taps&1)
        rvexit("cannot have an odd number of taps in the polyphase resampling filter: %d", taps);

    /* determine output image dimensions */
    if (divine_image_dims(&out_width, &out_height, NULL, NULL, NULL, out_format, outfile)<0) {
        rvmessage("using image dimensions of first input file as output");
        if (divine_image_dims(&out_width, &out_height, NULL, NULL, NULL, inp_format, filename[0])<0)
            rvexit("could not determine image size to scale to");
    }

    /* convert input fourcc string into integer */
    if (fccode)
        inp_fourcc = get_fourcc(fccode);

    /* change default input chroma format based on fourcc */
    int hsub, vsub;
    get_chromasub(inp_fourcc, &hsub, &vsub);
    if (hsub==-1 || vsub==-1)
        rvexit("unknown pixel format: %s", fourccname(inp_fourcc));

    /* determine input and output chroma formats */
    int out_hsub, out_vsub;
    if (lookup_chroma(out_chroma, &out_hsub, &out_vsub)==-1)
        rvexit("unknown output chroma format: %s", out_chroma);
    hsub *= out_hsub; vsub *= out_vsub;
    int inp_hsub, inp_vsub;
    if (lookup_chroma(inp_chroma, &inp_hsub, &inp_vsub)==-1)
        rvexit("unknown input chroma format: %s", inp_chroma);
    hsub /= inp_hsub; vsub /= inp_vsub;

    /* determine output fourcc */
    out_fourcc = inp_fourcc;
    if ((out_hsub != inp_hsub) || (out_vsub != inp_vsub)) {
        if (out_hsub==2 && out_vsub==2)
            out_fourcc = I420;
        if (out_hsub==2 && out_vsub==1)
            out_fourcc = I422;
        if (out_hsub==1 && out_vsub==1)
            out_fourcc = I444;
    }

    if (verbose>=1)
        rvmessage("out: %dx%d %s (%s)", out_width, out_height, fourccname(out_fourcc), yuv4mpeg? "y4m" : "raw");

    /* sanity check fourcc */
    if (inp_fourcc==UYVY && !(inp_hsub==2 && inp_vsub==1))
        rvexit("input chroma must be 4:2:2 for pixel format UYVY: %s", inp_chroma);
    if (inp_fourcc==UYVY && !(out_hsub==2 && out_vsub==1))
        rvexit("output chroma must be 4:2:2 for pixel format UYVY: %s", out_chroma);

    /* open output file */
    if (outfile)
        fileout = fopen(outfile, "wb");
    if (fileout==NULL)
        rverror("failed to open output file \"%s\"", outfile);

    /* ignore SIGPIPE on output file */
    if (signal(SIGPIPE, SIG_IGN)==SIG_ERR)
        rverror("failed to set ignore on SIGPIPE");

    /* allocate output data buffer */
    rvimage out(out_width, out_width, out_height, out_hsub, out_vsub, 8);

    /* configure resampling parameters */
    out.scale_config(algo, taps, phases);

    /* loop over input files */
    do {
        int frameno = 0;

        if (numfiles)
            filein = fopen(filename[fileindex], "rb");
        if (filein==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex]);

        /* parse yuv4mpeg stream header */
        if (raw_inp==0 || divine_image_dims(&inp_width, &inp_height, NULL, NULL, NULL, inp_format, filename[fileindex])<0) {
            /* must be yuv4mpeg input file */
            raw_inp = 0;
            if (read_y4m_stream_header(&inp_width, &inp_height, NULL, filein)<0)
                rverror("failed to read yuv4mpeg stream header");
        }
        else
            raw_inp = 1;

        if (inp_width==out_width && inp_height==out_height && inp_hsub==out_hsub && inp_vsub==out_vsub)
            rvexit("input and output images have the same dimensions and chroma subsampling");
        if (verbose>=1)
            rvmessage("inp: %dx%d %s (%s)", inp_width, inp_height, fourccname(inp_fourcc), raw_inp? "raw" : "y4m");

        /* allocate input data buffer */
        rvimage inp(inp_width, inp_width, inp_height, inp_hsub, inp_vsub, 8);

        /* write yuv4mpeg stream header */
        if (yuv4mpeg && fileindex==0)
            if (write_y4m_stream_header(out_width, out_height, get_chromaformat(out_fourcc), fileout)<0)
                rverror("failed to write stream header to output");

        /* main loop */
        for (frameno=0; !feof(filein) && !ferror(filein) && !ferror(fileout) && (frameno<numframes || numframes<0); frameno++) {
            /* read a frame */
            if (inp.fread(filein, !raw_inp)) {

                /* scale input image to output image */
                out.scale(inp);

                if (out.fwrite(fileout, yuv4mpeg) != 1)
                    break;
                else
                    totframes++;

            }
        }
        if (ferror(filein))
            rverror("failed to read from input");
        if (ferror(fileout) && errno!=EPIPE)
            rverror("failed to write to output");

        if (numfiles)
            fclose(filein);

    } while (++fileindex<numfiles);

    if (verbose>=0)
        rvmessage("scaled %d frames with algorithm: %s", totframes, algorithm);

    /* tidy up */

    return 0;
}
