/*
 * Description: Split and join video sequences.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2010-03-02 23:53:25 $
 * Revision   : $Revision: 1.42 $
 * Copyright  : (c) 2005 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include "getopt.h"

#include "rvutil.h"
#include "rvy4m.h"
#include "rvm2v.h"
#include "rvstat.h"
#include "rvhevc.h"

const char *appname;

void usage(int exitcode)
{
    fprintf(stderr, "%s: cut and join video files at picture boundaries\n", appname);
    fprintf(stderr, "usage: %s [options] [<file>[:<frames>]] [<file>[:<frames>]...]\n", appname);
    fprintf(stderr, "  <frames>    : list of frames to output, e.g. 2 or 2,4,6 or 8-\n");
    fprintf(stderr, "  -f, --frames        : list of frames for stdin (default: 0-)\n");
    fprintf(stderr, "  -a, --auto          : automatically splice to match reference sequence (default: disabled)\n");
    fprintf(stderr, "  -t, --type          : force type of file (default: autodetect)\n");
    fprintf(stderr, "  -s, --size          : image size format\n");
    fprintf(stderr, "     (formats : %s)\n", rvformats);
    fprintf(stderr, "  -p, --fourcc        : image pixel format fourcc (default: I420)\n");
    fprintf(stderr, "     (formats : %s)\n", rvfourccs);
    fprintf(stderr, "  -o, --output <file> : write output to file\n");
    fprintf(stderr, "  -4. --yuv4mpeg      : write yuv4mpeg format to output (default: raw yuv format)\n");
    fprintf(stderr, "  -q, --quiet         : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "  -v, --verbose       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "  --                  : disable argument processing\n");
    fprintf(stderr, "  -h, --help, --usage : print this usage message\n");
    exit(exitcode);
}

int main(int argc, char *argv[])
{
    FILE *fileout = stdout;
    FILE *fileref = NULL;
    const char *filename[RV_MAXFILES] = {0};
    char *outfile = NULL;
    int fileindex = 0;
    int numfiles = 0;
    int numframes = 0;

    /* command line defaults */
    const char *frames[RV_MAXFILES];
    char *reffile = NULL;
    char *forcetype = NULL;
    int width = 0;
    int height = 0;
    const char *format = NULL;
    const char *fccode = NULL;
    int raw_inp = 1;
    int yuv4mpeg = 0;
    int verbose = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"frames",     1, NULL, 'f'},
            {"auto",       1, NULL, 'a'},
            {"size",       1, NULL, 's'},
            {"fourcc",     1, NULL, 'p'},
            {"output",     1, NULL, 'o'},
            {"yuv4mpeg",   0, NULL, '4'},
            {"quiet",      0, NULL, 'q'},
            {"verbose",    0, NULL, 'v'},
            {"usage",      0, NULL, 'h'},
            {"help",       0, NULL, 'h'},
            {NULL,         0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "f:a:t:s:p:o:4qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 'f':
                frames[0] = optarg;
                break;

            case 'a':
                reffile = optarg;
                break;

            case 't':
                forcetype = optarg;
                break;

            case 's':
                format = optarg;
                break;

            case 'p':
                fccode = optarg;
                break;

            case '4':
                yuv4mpeg = 1;
                break;

            case 'o':
                outfile = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < RV_MAXFILES) {
            char *c = strrchr(argv[optind], ':');
            if (c==NULL)
                frames[numfiles] = "0-";
            else {
                *c = '\0';
                frames[numfiles] = c + 1;
            }
            filename[numfiles] = argv[optind++];
            numfiles++;
        } else
            rvexit("more than %d input files", numfiles);
    }

    /* use stdin if no input filenames */
    if (numfiles==0) {
        filename[0] = "-";
        frames[0] = "0-";
    }

    /* sanity check frame specification */
    for (int i=0; i<numfiles; i++) {
        const char *p;
        int len = strlen(frames[i]);
        for (p=frames[i]; p<frames[i]+len; p++)
            if (!((*p>='0' && *p<='9') || *p=='-' || *p=='+' || *p==','))
                rvexit("illegal characters in frame specification \"%s\": %c", frames[i], *p);
    }

    /* convert fourcc string into integer */
    unsigned int fourcc = 0;
    if (fccode)
        fourcc = get_fourcc(fccode);

    /* open reference file */
    int ref_width = 0;
    int ref_height = 0;
    if (reffile) {
        fileref = fopen(reffile, "rb");
        if (reffile==NULL)
            rverror("failed to open reference file \"%s\"", reffile);

        /* get dimensions of reference file */
        if (raw_inp==0 || divine_image_dims(&ref_width, &ref_height, NULL, NULL, NULL, format, reffile)<0) {
            /* must be yuv4mpeg format file */
            if (read_y4m_stream_header(&ref_width, &ref_height, NULL, fileref)<0)
                rverror("failed to read yuv4mpeg stream header");
        }
        if (ref_width==0 || ref_height==0)
            rvexit("failed to determine dimensions of reference file \"%s\"", reffile);
    }

    /* open output file */
    if (outfile)
        fileout = fopen(outfile, "wb");
    if (fileout==NULL)
        rverror("failed to open output file \"%s\"", outfile);

    /* ignore SIGPIPE on output file */
    if (signal(SIGPIPE, SIG_IGN)==SIG_ERR)
        rverror("failed to set ignore on SIGPIPE");

    /* loop over input files */
    do {
        unsigned int frameno = 0;
        char autoframes[64];

        /* prepare to parse file */
        struct bitbuf *bb = initbits_filename(filename[fileindex], B_GETBITS);
        if (bb==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex]);

        /* determine filetype */
        divine_t divine = divine_filetype(bb, filename[fileindex], forcetype? forcetype : format, verbose);
        if (rewindbits(bb)<0)
            rverror("searched too far on non-seekable input file \"%s\"", filename[fileindex]);

        /* determine input file chroma format */
        if (fourcc==0)
            fourcc = divine_pixel_format(filename[fileindex]);
        if (fourcc==0)
            fourcc = I420;

        if (verbose>=2)
            rvmessage("file \"%s\" has type %s and fourcc %s", filename[fileindex], filetypename[divine.filetype], fourccname(fourcc));

        /* select what to do based on filetype */
        switch (divine.filetype) {
            case YUV:
            case YUV4MPEG:
            {
                /* parse yuv4mpeg stream header */
                int inp_width  = 0;
                int inp_height = 0;
                int inp_raw = 1;
                if (raw_inp==0 || divine_image_dims(&inp_width, &inp_height, NULL, NULL, NULL, format, filename[fileindex])<0) {
                    /* must be yuv4mpeg format file */
                    inp_raw = 0;
                    if (read_y4m_stream_header_bits(bb, &inp_width, &inp_height, NULL, &fourcc)<0)
                        rverror("failed to read yuv4mpeg stream header");
                }
                if (!width)
                    width = inp_width;
                if (!height)
                    height = inp_height;

                /* check all input files are of the same dimensions */
                if (inp_width!=width || inp_height!=height)
                    rvexit("input files are not isometric: %dx%-d != %dx%-d", width, height, inp_width, inp_height);
                if (fileref)
                    if (inp_width!=ref_width || inp_height!=ref_height)
                        rvexit("reference file has different dimensions to input file");

                /* allocate data buffer */
                struct rvimage inp(inp_width, inp_width, inp_height, fourcc);

                /* find frames specification in auto-splice mode */
                if (fileref) {
#define window 1
                    /* allocate reference data buffers */
                    struct rvimage refframe[window];
                    for (unsigned i=0; i<window; i++)
                        refframe[i].init(ref_width, ref_width, ref_height, fourcc);

                    /* read first frames of reference sequence */
                    for (unsigned i=0; i<window; i++)
                        if (refframe[i].fread(fileref)!=1)
                            rverror("failed to read frame %d from reference file \"%s\"", i, reffile);

                    /* find index of last frame in reference sequence */
                    if (fseeko(fileref, 0, SEEK_END)<0)
                        rverror("failed to seek in reference file");
                    unsigned int refno = ftello(fileref)/refframe[0].size - 1;

                    /* compute statistics of every frame in input wrt first frames of reference */
                    struct rvstats *stats[window] = {NULL};
                    for (frameno=0; !eofbits(bb); frameno++)
                    {
                        /* realloc arrays */
                        for (unsigned i=0; i<window; i++)
                            stats[i] = (rvstats *)rvalloc(stats[i], (frameno+1)*sizeof(struct rvstats), 0);

                        /* test each frame against reference */
                        if (inp.readbits(bb, !inp_raw)) {
                            for (unsigned i=0; i<window; i++) {
                                stats[i][frameno] = calculate_stats(inp, refframe[i], 1, LUMA_PLANE);
                            }
                        } else
                            break;
                    }
                    rewindbits(bb);

                    /* sanity check */
                    if (frameno<refno)
                        rvexit("automatic splice: input file is shorter than reference file: %d<%d", frameno, refno);

                    /* find best match */
                    double best = 1e9;
                    int index = 0;
                    for (unsigned i=0; i<frameno-1; i++) {
                        int j;
                        double cost = 0.0;
                        for (j=0; j<window; j++)
                            cost += stats[j][i+j].mse;
                        if (cost < best) {
                            index = i;
                            best = cost;
                        }
                    }

                    /* prepare frame specification */
                    snprintf(autoframes, sizeof(autoframes), "%d-%d", index, index+refno);
                    if (verbose>=1)
                        rvmessage("automatic splice for \"%s\": %s", filename[fileindex], autoframes);
                    frames[fileindex] = autoframes;

                    /* tidy up */
                    for (unsigned i=0; i<window; i++) {
                        rvfree(stats[i]);
                    }
                }

                /* write yuv4mpeg stream header */
                if (yuv4mpeg && fileindex==0)
                    if (write_y4m_stream_header(inp_width, inp_height, get_chromaformat(fourcc), fileout)<0)
                        break;

                /* main loop */
                for (frameno=0; !eofbits(bb); frameno++)
                {
                    int match = frame_match(frameno, frames[fileindex], verbose);
                    /* look for early exit */
                    if (match<0)
                        break;

                    if (inp.readbits(bb, !inp_raw) && match>0)
                    {
                        if (inp.fwrite(fileout, yuv4mpeg) != 1)
                            break;
                        else
                            numframes++;
                    }
                }
                if (ferror(fileout) && errno!=EPIPE)
                    rverror("failed to write to output");
                break;
            }

            case JPEG:
                /* main loop */
                for (frameno=0; !eofbits(bb); frameno++)
                {
                    int match = frame_match(frameno, frames[fileindex], verbose);
                    /* look for early exit */
                    if (match<0)
                        break;

                    int marker = 0;
                    int output = 0;
                    while (1) {
                        int byte = getbyte(bb);
                        if (byte!=EOF) {
                            if (match>0)
                                if (output || byte) {
                                    output = 1;
                                    if (fputc(byte, fileout)==EOF)
                                        break;
                                }

                            /* look for EOI marker */
                            if (marker)
                                if (byte==0xD9) {
                                    if (match>0)
                                        numframes++;
                                    break;
                                }
                            if (byte==0xFF)
                                marker = 1;
                            else
                                marker = 0;
                        }
                    }
                    if (ferror(fileout) && errno!=EPIPE)
                        rverror("failed to write to output");
                }
                break;

            case M2V:
            {
                /* find sequence header */
                int pos = numbits(bb);
                while (!eofbits(bb)) {
                    int start = showbits32(bb);
                    /* look for a sequence start code */
                    if (start==0x1B3)
                        break;
                    /* move along */
                    flushbits(bb, 8);
                }
                if (eofbits(bb))
                    rvexit("failed to find sequence header in m2v sequence \"%s\"", filename[fileindex]);
                if (verbose>=1 && numbits(bb)-pos>7)
                    rvmessage("%jd bits of leading garbage before sequence start code", numbits(bb)-pos);

                /* start copy of sequence header to output */
                bb->outfile = fileout;
                bb->copybits = 1;

                /* parse sequence header */
                struct rvm2vsequence *sequence = parse_m2v_headers(bb, verbose);
                if (sequence==NULL)
                    rvexit("failed to parse m2v sequence headers");

                /* main loop */
                int picno;
                for (frameno=0, picno=0; !eofbits(bb); picno++)
                {
                    int match = frame_match(frameno, frames[fileindex], verbose);
                    /* look for early exit */
                    if (match<0)
                        break;
                    else
                        bb->copybits = match>0;

                    /* find start of next picture */
                    pos = numbits(bb);
                    if (find_m2v_next_picture(bb, sequence, frameno, NULL, verbose)<0)
                        break;

                    /* parse picture header */
                    struct rvm2vpicture *picture = parse_m2v_picture_header_and_extensions(bb, sequence, picno, verbose);
                    if (picture==NULL)
                        rvexit("failed to parse picture header");
                    struct rvm2vextension *extension = &picture->extension;

                    /* find end of next picture */
                    if (find_m2v_picture_end(bb, sequence, frameno)<0)
                        break;

                    /* increment frame number */
                    if ((extension->picture_structure==PST_FRAME) ||
                       (extension->picture_structure==PST_TOP && !extension->top_field_first) ||
                       (extension->picture_structure==PST_BOTTOM && extension->top_field_first)) {
                        frameno++;
                        if (match>0)
                            numframes++;
                    }

                    /* tidy up */
                    rvfree(picture);
                }

                /* copy a sequence end code */
                if (showbits32(bb)==0x1B7)
                    getbits32(bb);
                break;
            }

            case H264:
            {
                /* find first sequence parameter set nal */
                int pos = numbits(bb);
                while (!eofbits(bb)) {
                    int nal_unit_type = find_next_nal_unit(bb, NULL);
                    /* look for a sequence parameter set */
                    if (nal_unit_type==NAL_SPS)
                        break;
                    /* move along */
                    flushbits(bb, 8);
                }
                if (eofbits(bb))
                    rvexit("failed to find sequence parameter set nal in h.264 sequence \"%s\"", filename[fileindex]);
                if (verbose>=1 && numbits(bb)-pos>7)
                    rvmessage("%jd bits of leading garbage before first sequence parameter set nal", numbits(bb)-pos);

                /* start copy of nals to output */
                bb->outfile = fileout;
                bb->copybits = 1;

                /* parse sequence and picture parameter sets */
                struct rv264sequence *seq = parse_264_params(bb, verbose);
                if (seq==NULL)
                    rvexit("failed to parse h.264 sequence and picture parameter sets");

                /* main loop */
                int picno;
                for (frameno=0, picno=0; !eofbits(bb); picno++)
                {
                    int match = frame_match(frameno, frames[fileindex], verbose);
                    /* look for early exit */
                    if (match<0)
                        break;
                    else
                        bb->copybits = match>0;

                    /* find start of next picture */
                    while (!eofbits(bb)) {
                        int zerobyte, nal_unit_type = find_next_nal_unit(bb, &zerobyte);
                        if (nal_unit_type<0)
                            break;
                        /* look for a VLC NAL unit */
                        if (nal_unit_type==NAL_CODED_SLICE_IDR || nal_unit_type==NAL_CODED_SLICE_NON_IDR) {
                            flushbits(bb, zerobyte? 32 : 24);
                            /* find end of next picture */
                            find_next_nal_unit(bb, NULL);
                            /* TODO look for post VLC NAL units, e.g. SEI */
                            break;
                        }
                        /* move along */
                        flushbits(bb, 8);
                    }


                    /* increment frame/picture number */
                    if (1/*picture->sh[0]->field_pic_flag==0  TODO field pictures */) {
                        frameno++;
                        if (match>0)
                            numframes++;
                    }

                    /* tidy up */
                }

                break;
            }

            case HEVC:
            {
                int numnals = 0;

                /* find first video parameter set nal */
                int pos = numbits(bb), zerobyte;
                while (!eofbits(bb)) {
                    int nal_unit_type = find_next_hevc_nal_unit(bb, &zerobyte);
                    if (verbose>=1 && !eofbits(bb))
                        rvmessage("nal_unit %3d: type=%s", numnals, hevc_nal_unit_type_code(nal_unit_type));
                    numnals++;
                    /* look for a video parameter set */
                    if (nal_unit_type==VPS_NUT || nal_unit_type==SPS_NUT)
                        break;
                    /* move along */
                    flushbits(bb, 8);
                }
                if (eofbits(bb))
                    rvexit("failed to find video parameter set nal in hevc sequence \"%s\"", filename[fileindex]);
                if (verbose>=1 && numbits(bb)-pos>7+8*zerobyte)
                    rvmessage("%jd bits of leading garbage before first video parameter set nal", numbits(bb)-pos);

                /* start copy of nals to output */
                bb->outfile = fileout;
                bb->copybits = 1;

                /* find start of first picture, copying parameter set nals to output */
                pos = numbits(bb);
                while (!eofbits(bb)) {
                    int nal_unit_type = find_next_hevc_nal_unit(bb, NULL);
                    if (verbose>=1 && !eofbits(bb))
                        rvmessage("nal_unit %3d: type=%s", numnals, hevc_nal_unit_type_code(nal_unit_type));
                    numnals++;
                    /* look for an access unit delimiter */
                    if (nal_unit_type==AUD_NUT)
                        break;
                    /* move along */
                    flushbits(bb, 8);
                }

                /* main loop */
                int picno;
                for (frameno=0, picno=0; !eofbits(bb); picno++)
                {
                    int match = frame_match(frameno, frames[fileindex], verbose);
                    /* look for early exit */
                    if (match<0)
                        break;
                    else
                        bb->copybits = match>0;

                    /* find start of next picture */
                    pos = numbits(bb);
                    while (!eofbits(bb)) {
                        int nal_unit_type = find_next_hevc_nal_unit(bb, NULL);
                        if (verbose>=1 && !eofbits(bb))
                            rvmessage("nal_unit %3d: type=%s", numnals, hevc_nal_unit_type_code(nal_unit_type));
                        numnals++;
                        /* look for an access unit delimiter */
                        if (nal_unit_type==AUD_NUT)
                            break;
                        /* move along */
                        flushbits(bb, 8);
                    }

                    /* parse access unit */
                    int nal_unit_type;
                    parse_hevc_nal_unit_header(bb, &nal_unit_type);
                    //parse_hevc_access_unit_delimiter(bb, verbose);

                    /* increment frame number */
                    frameno++;
                    if (match>0)
                        numframes++;

                    /* tidy up */
                }

                break;
            }

            case EMPTY:
                rvexit("empty file: \"%s\"", filename[fileindex]);
                break;

            default:
                rvmessage("file format not supported: %s", filetypename[divine.filetype]);
                break;
        }

        /* tidy up */
        freebits(bb);

    } while (++fileindex<numfiles);

    if (verbose>=0)
        rvmessage("wrote %d pictures", numframes);

    /* tidy up */

    return 0;
}
