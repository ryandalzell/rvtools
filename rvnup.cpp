/*
 * Description: Merge two raw video sequences into one split screen view.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2010-01-22 15:52:40 $
 * Revision   : $Revision: 1.2 $
 * Copyright  : (c) 2009 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include "getopt.h"

#include "rvutil.h"
#include "rvy4m.h"
#include "rvimage.h"

#define MAXFILES 4

const char *appname;

void usage(int exitcode)
{
    fprintf(stderr, "%s: merge two raw video sequences into one split screen view\n", appname);
    fprintf(stderr, "usage: %s [options] <file> <file>\n", appname);
    fprintf(stderr, "  -s, --size          : output image size format\n");
    fprintf(stderr, "     (formats : %s)\n", rvformats);
    fprintf(stderr, "  -p, --fourcc        : output image pixel format fourcc (default: I420)\n");
    fprintf(stderr, "     (formats : %s)\n", rvfourccs);
    fprintf(stderr, "  -n, --numframes     : number of frames (default: all)\n");
    fprintf(stderr, "  -o, --output        : write output to file\n");
    fprintf(stderr, "  -q, --quiet         : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "  -v, --verbose       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "  --                  : disable argument processing\n");
    fprintf(stderr, "  -h, --help, --usage : print this usage message\n");
    exit(exitcode);
}

int main(int argc, char *argv[])
{
    int i;
    FILE *fileout = stdout;
    char *outfile = NULL;
    int numfiles = 0;
    int totframes = 0;

    /* input file parameters */
    FILE *filein[MAXFILES] = {NULL, stdin, NULL, NULL};
    char *filename[MAXFILES] = {NULL};
    int width[MAXFILES] = {0};
    int height[MAXFILES] = {0};
    int raw[MAXFILES];

    /* image buffers */
    struct rvimage inp[MAXFILES];

    /* command line defaults */
    int out_width = 0;
    int out_height = 0;
    const char *format = NULL;
    const char *fccode = NULL;
    int numframes = -1;
    int verbose = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"size",      1, NULL, 's'},
            {"fourcc",    1, NULL, 'p'},
            {"numframes", 1, NULL, 'n'},
            {"output",    1, NULL, 'o'},
            {"quiet",     0, NULL, 'q'},
            {"verbose",   0, NULL, 'v'},
            {"usage",     0, NULL, 'h'},
            {"help",      0, NULL, 'h'},
            {NULL,        0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "s:p:n:o:qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 's':
                format = optarg;
                break;

            case 'p':
                fccode = optarg;
                break;

            case 'n':
                numframes = atoi(optarg);
                if (numframes<=0)
                    rvexit("invalid value for numframes: %d", numframes);
                break;

            case 'o':
                outfile = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < MAXFILES)
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    /* sanity check command line */
    if (numfiles>MAXFILES)
        rvexit("only suppports %d input files or less", MAXFILES);

    /* convert fourcc string into integer */
    unsigned int fourcc = I420;
    if (fccode)
        fourcc = get_fourcc(fccode);

    /* get chroma subsampling from fourcc */
    int hsub, vsub;
    get_chromasub(fourcc, &hsub, &vsub);
    if (hsub==-1 || vsub==-1)
        rvexit("unknown pixel format: %s", fourccname(fourcc));

    /* open input files */
    for (i=0; i<numfiles; i++) {
        filein[i] = fopen(filename[i], "rb");
        if (filein[i]==NULL)
            rverror("failed to open file \"%s\"", filename[i]);
    }

    /* get input file dimensions */
    for (i=0; i<numfiles; i++) {
        if (divine_image_dims(&width[i], &height[i], NULL, NULL, NULL, NULL, filename[i])<0) {
            /* must be yuv4mpeg format file */
            raw[i] = 0;
            if (read_y4m_stream_header(&width[i], &height[i], NULL, filein[i])<0)
                rverror("failed to read yuv4mpeg stream header");
        } else
            raw[i] = 1;
        if (width[i]==0 || height[i]==0)
            rvexit("failed to determine dimensions of input file \"%s\"", filename[i]);
    }

    /* initialise input images */
    for (i=0; i<numfiles; i++)
        inp[i].init(width[i], width[i], height[i], hsub, vsub, 8);

    /* open output file */
    if (outfile)
        fileout = fopen(outfile, "wb");
    if (fileout==NULL)
        rverror("failed to open output file \"%s\"", outfile);

    /* get output file dimensions */
    if (divine_image_dims(&out_width, &out_height, NULL, NULL, NULL, format, outfile)<0)
        rvexit("failed to determine dimensions of output file");
    if (verbose>=1)
        rvmessage("output image: %dx%d", out_width, out_height);

    /* ignore SIGPIPE on output file */
    if (signal(SIGPIPE, SIG_IGN)==SIG_ERR)
        rverror("failed to set ignore on SIGPIPE");

    /* allocate output image */
    rvimage out(out_width, out_width, out_height, hsub, vsub, 8);

    /* loop over input frames */
    int frameno;
    for (frameno=0; (totframes<numframes || numframes<0) && !ferror(fileout); frameno++)
    {
        /* read next frame from each input file */
        int read = 0;
        for (i=0; i<numfiles; i++) {
            read += inp[i].fread(filein[i], !raw[i]);
        }
        if (read!=numfiles)
            break;

        /* clear output frame */
        out.clear();

        if (numfiles==2) {
            /* place two frames side-by-side */
            out.copy(inp[0],                                         0, 0,                                 0, 0, mmin(inp[0].width, out.width/2), inp[0].height);
            out.copy(inp[1], mmax(out.width/2, out.width-inp[1].width), 0, mmax(0, inp[1].width-out.width/2), 0, mmin(inp[1].width, out.width/2), inp[1].height);
            out.draw_line(out.width/2-1, 0, 0, out.height, BLACK);
        } else if (numfiles==4) {
            /* place four frames in each corner */
            out.copy(inp[0],                                         0, 0,                                 0, 0, mmin(inp[0].width, out.width/2), inp[0].height);
            out.copy(inp[1], mmax(out.width/2, out.width-inp[1].width), 0, mmax(0, inp[1].width-out.width/2), 0, mmin(inp[1].width, out.width/2), inp[1].height);
            out.copy(inp[2],                                         0, mmax(out.height/2, out.height-inp[1].height),                                 0, mmax(0, inp[1].height-out.height/2), mmin(inp[0].width, out.width/2), inp[0].height);
            out.copy(inp[3], mmax(out.width/2, out.width-inp[1].width), mmax(out.height/2, out.height-inp[1].height), mmax(0, inp[1].width-out.width/2), mmax(0, inp[1].height-out.height/2), mmin(inp[1].width, out.width/2), inp[1].height);
            out.draw_line(out.width/2-1, 0, 0, out.height, BLACK);
            out.draw_line(0, out.height/2-1, out.width, 0, BLACK);
        } else {
            rvmessage("unsupported mode: %d input files", numfiles);
            break;
        }

        /* write output frame */
        if (out.fwrite(fileout)!=1)
            break;

        totframes++;
    }
    if (ferror(filein[0]))
        rverror("failed to read from input");
    if (ferror(filein[1]))
        rverror("failed to read from input");
    if (ferror(fileout) && errno!=EPIPE)
        rverror("failed to write to output");

    if (verbose>=0)
        rvmessage("processed %d frames", totframes);

    /* tidy up */
    for (i=0; i<numfiles; i++) {
        fclose(filein[i]);
    }
    fclose(fileout);

    return 0;
}
