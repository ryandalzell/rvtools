
#ifndef _RVASF_H_
#define _RVASF_H_

/* guid type */
struct guid {
    unsigned long  data1;
    unsigned short data2;
    unsigned short data3;
    unsigned char  data4[8];
};

/* top level asf object guids */
#define ASF_HEADER_OBJECT                       "75B22630-668E-11CF-A6D9-00AA0062CE6C"
#define ASF_DATA_OBJECT                         "75B22636-668E-11CF-A6D9-00AA0062CE6C"
#define ASF_SIMPLE_INDEX_OBJECT                 "33000890-E5B1-11CF-89F4-00A0C90349CB"
#define ASF_INDEX_OBJECT                        "D6E229D3-35DA-11D1-9034-00A0C90349BE"
#define ASF_MEDIA_OBJECT_INDEX_OBJECT           "FEB103F8-12AD-4C64-840F-2A1D2F7AD48C"
#define ASF_TIMECODE_INDEX_OBJECT               "3CB73FD0-0C4A-4803-953D-EDF7B6228F0C"

/* header object guids */

#define ASF_FILE_PROPERTIES_OBJECT              "8CABDCA1-A947-11CF-8EE4-00C00C205365"
#define ASF_STREAM_PROPERTIES_OBJECT            "B7DC0791-A9B7-11CF-8EE6-00C00C205365"
#define ASF_HEADER_EXTENSION_OBJECT             "5FBF03B5-A92E-11CF-8EE3-00C00C205365"
#define ASF_CODEC_LIST_OBJECT                   "86D15240-311D-11D0-A3A4-00A0C90348F6"
#define ASF_SCRIPT_COMMAND_OBJECT               "1EFB1A30-0B62-11D0-A39B-00A0C90348F6"
#define ASF_MARKER_OBJECT                       "F487CD01-A951-11CF-8EE6-00C00C205365"
#define ASF_BITRATE_MUTUAL_EXCLUSION_OBJECT     "D6E229DC-35DA-11D1-9034-00A0C90349BE"
#define ASF_ERROR_CORRECTION_OBJECT             "75B22635-668E-11CF-A6D9-00AA0062CE6C"
#define ASF_CONTENT_DESCRIPTION_OBJECT          "75B22633-668E-11CF-A6D9-00AA0062CE6C"
#define ASF_EXTENDED_CONTENT_DESCRIPTION_OBJECT "D2D0A440-E307-11D2-97F0-00A0C95EA850"
#define ASF_CONTENT_BRANDING_OBJECT             "2211B3FA-BD23-11D2-B4B7-00A0C955FC6E"
#define ASF_STREAM_BITRATE_PROPERTIES_OBJECT    "7BF875CE-468D-11D1-8D82-006097C9A2B2"
#define ASF_CONTENT_ENCRYPTION_OBJECT           "2211B3FB-BD23-11D2-B4B7-00A0C955FC6E"
#define ASF_EXTENDED_CONTENT_ENCRYPTION_OBJECT  "298AE614-2622-4C17-B935-DAE07EE9289C"
#define ASF_DIGITAL_SIGNATURE_OBJECT            "2211B3FC-BD23-11D2-B4B7-00A0C955FC6E"
#define ASF_PADDING_OBJECT                      "1806D474-CADF-4509-A4BA-9AABCB96AAE8"

/* header extension object guids */

#define ASF_EXTENDED_STREAM_PROPERTIES_OBJECT       "14E6A5CB-C672-4332-8399-A96952065B5A"
#define ASF_ADVANCED_MUTUAL_EXCLUSION_OBJECT        "A08649CF-4775-4670-8A16-6E35357566CD"
#define ASF_GROUP_MUTUAL_EXCLUSION_OBJECT           "D1465A40-5A79-4338-B71B-E36B8FD6C249"
#define ASF_STREAM_PRIORITIZATION_OBJECT            "D4FED15B-88D3-454F-81F0-ED5C45999E24"
#define ASF_BANDWIDTH_SHARING_OBJECT                "A69609E6-517B-11D2-B6AF-00C04FD908E9"
#define ASF_LANGUAGE_LIST_OBJECT                    "7C4346A9-EFE0-4BFC-B229-393EDE415C85"
#define ASF_METADATA_OBJECT                         "C5F8CBEA-5BAF-4877-8467-AA8C44FA4CCA"
#define ASF_METADATA_LIBRARY_OBJECT                 "44231C94-9498-49D1-A141-1D134E457054"
#define ASF_INDEX_PARAMETERS_OBJECT                 "D6E229DF-35DA-11D1-9034-00A0C90349BE"
#define ASF_MEDIA_OBJECT_INDEX_PARAMETERS_OBJECT    "6B203BAD-3F11-48E4-ACA8-D7613DE2CFA7"
#define ASF_TIMECODE_INDEX_PARAMETERS_OBJECT        "F55E496D-9797-4B5D-8C8B-604DFE9BFB24"
#define ASF_COMPATIBILITY_OBJECT                    "75B22630-668E-11CF-A6D9-00AA0062CE6C"
#define ASF_ADVANCED_CONTENT_ENCRYPTION_OBJECT      "43058533-6981-49E6-9B74-AD12CB86D58C"

/* stream properties object stream type guids */

#define ASF_AUDIO_MEDIA             "F8699E40-5B4D-11CF-A8FD-00805F5C442B"
#define ASF_VIDEO_MEDIA             "BC19EFC0-5B4D-11CF-A8FD-00805F5C442B"
#define ASF_COMMAND_MEDIA           "59DACFC0-59E6-11D0-A3AC-00A0C90348F6"
#define ASF_JFIF_MEDIA              "B61BE100-5B4E-11CF-A8FD-00805F5C442B"
#define ASF_DEGRADABLE_JPEG_MEDIA   "35907DE0-E415-11CF-A917-00805F5C442B"
#define ASF_FILE_TRANSFER_MEDIA     "91BD222C-F21C-497A-8B6D-5AA86BFC0185"
#define ASF_BINARY_MEDIA            "3AFB65E2-47EF-40F2-AC2C-70A90D71D343"

/* web stream type-specific data guids */

#define ASF_WEB_STREAM_MEDIA_SUBTYPE    "776257D4-C627-41CB-8F81-7AC7FF1C40CC"
#define ASF_WEB_STREAM_FORMAT           "DA1E6B13-8359-4050-B398-388E965BF00C"


/* stream properties object error correction type guids  */

#define ASF_NO_ERROR_CORRECTION     "20FB5700-5B55-11CF-A8FD-00805F5C442B"
#define ASF_AUDIO_SPREAD            "BFC3CD50-618F-11CF-8BB2-00AA00B4E220"


/* header extension object guids */

#define ASF_RESERVED_1              "ABD3D211-A9BA-11cf-8EE6-00C00C205365"

/* advanced content encryption object system id guids */

#define ASF_CONTENT_ENCRYPTION_SYSTEM_WINDOWS_MEDIA_DRM_NETWORK_DEVICES "7A079BB6-DAA4-4e12-A5CA-91D38DC11A8D"

/* codec list object guids */

#define ASF_RESERVED_2              "86D15241-311D-11D0-A3A4-00A0C90348F6"

/* script command object guids */

#define ASF_RESERVED_3              "4B1ACBE3-100B-11D0-A39B-00A0C90348F6"

/* marker object guids */

#define ASF_RESERVED_4              "4CFEDB20-75F6-11CF-9C0F-00A0C90349CB"

/* mutual exclusion object exclusion type guids */

#define ASF_MUTEX_LANGUAGE          "D6E22A00-35DA-11D1-9034-00A0C90349BE"
#define ASF_MUTEX_BITRATE           "D6E22A01-35DA-11D1-9034-00A0C90349BE"
#define ASF_MUTEX_UNKNOWN           "D6E22A02-35DA-11D1-9034-00A0C90349BE"

/* bandwidth sharing object guids */

#define ASF_BANDWIDTH_SHARING_EXCLUSIVE "AF6060AA-5197-11D2-B6AF-00C04FD908E9"
#define ASF_BANDWIDTH_SHARING_PARTIAL   "AF6060AB-5197-11D2-B6AF-00C04FD908E9"

/* standard payload extension system guids */

#define ASF_PAYLOAD_EXTENSION_SYSTEM_TIMECODE               "399595EC-8667-4E2D-8FDB-98814CE76C1E"
#define ASF_PAYLOAD_EXTENSION_SYSTEM_FILE_NAME              "E165EC0E-19ED-45D7-B4A7-25CBD1E28E9B"
#define ASF_PAYLOAD_EXTENSION_SYSTEM_CONTENT_TYPE           "D590DC20-07BC-436C-9CF7-F3BBFBF1A4DC"
#define ASF_PAYLOAD_EXTENSION_SYSTEM_PIXEL_ASPECT_RATIO     "1B1EE554-F9EA-4BC8-821A-376B74E4C4B8"
#define ASF_PAYLOAD_EXTENSION_SYSTEM_SAMPLE_DURATION        "C6BD9450-867F-4907-83A3-C77921B733AD"
#define ASF_PAYLOAD_EXTENSION_SYSTEM_ENCRYPTION_SAMPLE_ID   "6698B84E-0AFA-4330-AEB2-1C0A98D7A44D"

/* guid utility functions */
struct guid getguid(struct bitbuf *bb);
char *stringify_guid(struct guid guid, char *string);

/* parsing utility functions */
int parse_length_with_type(struct bitbuf *bb, int length_type);
int length_of_type(int length_type);

/* header object functions */
int parse_header_object(struct bitbuf *bb, int verbose);
int parse_file_properties_object(struct bitbuf *bb, int verbose, long long *data_packets_count, int *flags, int *data_packet_size);
int parse_stream_properties_object(struct bitbuf *bb, int verbose, int *codec_specific_data_size, unsigned char **codec_specific_data);
int parse_header_extension_object(struct bitbuf *bb, int verbose);
int parse_codec_list_object(struct bitbuf *bb, int verbose);
int parse_content_description_object(struct bitbuf *bb, int verbose);
int parse_stream_bitrate_properties_object(struct bitbuf *bb, int verbose);

#endif
