/*
 * Description: Resampling/scaling method of an rvimage.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-10-01 16:00:14 $
 * Revision   : $Revision: 1.27 $
 * Copyright  : (c) 2015 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <string.h>
#include <math.h>

#include "rvimage.h"

/*
 * nearest neighbour scaling, also known as coincident resampling
 * this algorithm is the modified Bresenham line drawing algorithm, as described in:
 * Robert Ulichney. Bresenham-style scaling. In Proc. of IS&T 46th Annual Conf, pages 101-103, May 1993
 */
static int scale_nearest_line(const unsigned char *src, int src_width, int src_step,
                       unsigned char *dst, int dst_width, int dst_step)
{
    const unsigned char *pel;
    /* horizontal scaling */
    if (dst_width >= src_width) {
        /* up-scaling */
        int a = 0;
        for (pel=src; pel<src+src_width*src_step; pel+=src_step) {
            /* use this new pel */
            *dst = *pel;
            dst += dst_step;
            a += src_width - dst_width;
            while (a<0) {
                /* repeat pel */
                *dst = *pel;
                dst += dst_step;
                a += src_width;
            }
        }
    } else {
        /* down-scaling */
        int a = 0;
        for (pel=src; pel<src+src_width*src_step; pel+=src_step) {
            if (a<0) {
                /* skip this pel */
                a += dst_width;
            } else {
                /* use this pel */
                *dst = *pel;
                dst += dst_step;
                a += dst_width - src_width;
            }
        }
    }
    return dst_width;
}

static void scale_nearest(const unsigned char *src, int src_width, int src_height, int src_step,
                   unsigned char *dst, int dst_width, int dst_height, int dst_step)
{
    /* check for nop */
    if (src_width==dst_width && src_height==dst_height) {
        memcpy(dst, src, src_width*src_height*sizeof(unsigned char));
        return;
    }

    const unsigned char *line;
    /* vertical scaling */
    if (dst_height >= src_height) {
        /* up-scaling */
        int a = 0;
        for (line=src; line<src+src_width*src_step*src_height; line+=src_width*src_step) {
            dst += scale_nearest_line(line, src_width, src_step, dst, dst_width, dst_step);
            a += src_height - dst_height;
            while (a<0) {
                dst += scale_nearest_line(line, src_width, src_step, dst, dst_width, dst_step);
                a += src_height;
            }
        }
    } else {
        /* down-scaling */
        int a = 0;
        for (line=src; line<src+src_width*src_step*src_height; line+=src_width*src_step) {
            if (a<0) {
                /* skip this line */
                a += dst_height;
            } else {
                /* use this line */
                dst += scale_nearest_line(line, src_width, src_step, dst, dst_width, dst_step);
                a += dst_height - src_height;
            }
        }
    }
}

/*
 * bilinear interpolation scaling
 * this algorithm is taken from Numerical Recipes in C, Second Edition, p123.
 */
static void scale_bilinear(const unsigned char *src, int src_width, int src_height, unsigned char *dst, int dst_width, int dst_height)
{
    int x, y;

    /* check for nop */
    if (src_width==dst_width && src_height==dst_height) {
        memcpy(dst, src, src_width*src_height*sizeof(unsigned char));
        return;
    }

    /* loop over output lines */
    for (y=0; y<dst_height; y++) {
        /* project destination line onto source line grid */
        double y1 = (double)y * (double)src_height / (double)dst_height;
        double u  = y1 - floor(y1);

        /* check for out of bounds samples */
        int yoob = (int)y1==src_height-1;

        /* pointer to start of top line */
        const unsigned char *py = src + (int)y1*src_width;

        /* loop over output pels */
        for (x=0; x<dst_width; x++) {
            /* project destination pel onto source pel grid */
            double x1 = (double)x * (double)src_width / (double)dst_width;
            double t  = x1 - floor(x1);

            /* check for out of bounds samples */
            int xoob = (int)x1==src_width-1;

            /* pointer to top left of source pel grid */
            const unsigned char *px = py + (int)x1;

            /* bilinear interpolation */
            int p1 = *px;
            int p2 = xoob? p1 : *(px + 1);
            int p3 = yoob? p1 : *(px + src_width);
            int p4 = xoob? p3 : yoob? p2 : *(px + src_width + 1);
            *(dst + y*dst_width + x) = (1-t)*(1-u)*p1 + t*(1-u)*p2 + (1-t)*u*p3 + t*u*p4;
        }
    }
}

/*
 * polyphase interpolation scaling
 * this algorithm has been developed after reading:
 * Charles Poynton, "Digital Video and HDTV: Algorithms and Interfaces", Morgan Kaufmann, San Fransico
 * Fredric J. Harris, "On the Use of Windows for Harmonic Analysis with the Discrete Fourier Transform," Proc. of the IEEE, Vol. 66, No. 1, January 1978
 */

static double sinc(double x)
{
    return x==0? 1.0 : sin(x) / x;
}

static double vonhann(double x, double a)
{
    return a + (1.0-a)*cos(x);
}

static double blackman(double x, double a0, double a1, double a2, double a3)
{
    return a0 - a1*cos(x) + a2*cos(2*x) - a3*cos(2*x);
}

static void build_polyphase(double *filter, double factor, int phases, int taps, double scale, enum algo_t algo)
{
    const int centre = (taps-1)/2; /* left-centre */
    double h[taps];
    int p, t;

    /* only need to filter at original fs if interpolating */
    factor = mmin(1.0, factor);

    for (p=0; p<phases; p++) {
        double sum = 0;
        for (t=0; t<taps; t++) {
            double x = (double)(t-centre) - (double)p/(double)phases;  /* (-taps/2, taps/2] */
            x *= factor;                                               /* (-taps*factor/2, taps*factor/2] */
            double w = 2.0*x / taps / factor;                          /* (-1, 1] */
            switch (algo) {
                // FIXME need BICUBIC and KAISER coefficients also.
                case BARTLETT:
                    h[t] = sinc(M_PI*x) * (1.0-fabs(w));
                    break;
                case VONHANN:
                    h[t] = sinc(M_PI*x) * vonhann(w, 0.5);
                    break;
                case HAMMING:
                    h[t] = sinc(M_PI*x) * vonhann(w, 25.0/46.0);
                    break;
                case BLACKMAN:
                    h[t] = sinc(M_PI*x) * blackman(w, 0.42, -0.5, 0.08, 0.0);
                    break;
                case BLACKMANHARRIS:
                    h[t] = sinc(M_PI*x) * blackman(w, 0.35875, -0.48829, 0.14128, -0.01168);
                    break;
                case BLACKMANNUTTALL:
                    h[t] = sinc(M_PI*x) * blackman(w, 0.3635819, -0.4891775, 0.1365995, -0.0106411);
                    break;
                default:
                    rvexit("filter algorithm not implemented: %d", algo);
            }

            sum += h[t];
        }

        /* normalise */
        for (t=0; t<taps; t++)
            filter[p*taps + t] = h[t] * scale / sum;
    }
}

static int get_phase(double d, int phases)
{
    int i;

    /* get fractional part */
    double frac = d - floor(d);
    for (i=1; i<=phases; i++)
        if ((double)i/(double)phases > frac)
            break;
    return i-1;
}

static void scale_polyphase_single(const unsigned char *src, const unsigned char *max, int src_step, int src_length,
                            unsigned char *dst, int dst_step, int dst_length, double *h, int phases, int taps)
{
    int x, i;

    /* (left)centre tap of the filter */
    const int centre = (taps-1)/2;

    /* loop over output pels */
    for (x=0; x<dst_length; x++) {
        /* project destination pel onto source pel grid */
        double x1 = (double)x * (double)src_length / (double)dst_length;

        /* determine phase */
        int phase = get_phase(x1, phases);
        double *hp = h + phase*taps;

        /* pointer to topmost sample covered by filter */
        const unsigned char *p = src + ((int)x1 - centre)*src_step;

        /* finite impulse response filter */
        double g = 0.5; /* rounding away from zero */
        for (i=0; i<taps; i++, p+=src_step) {
            int sample;
            if (p < src)
                sample = *src;
            else if (p > max)
                sample = *max;
            else
                sample = *p;
            g += sample * hp[i];
        }

        /* store new pel */
        *(dst + x*dst_step) = mmax(0, mmin(255, (int)g));
    }
}

static void scale_polyphase(const unsigned char *src, int src_width, int src_height,
                     unsigned char *dst, int dst_width, int dst_height, int phases, int taps, enum algo_t algo)
{
    int x, y;

    /* check for nop */
    if (src_width==dst_width && src_height==dst_height) {
        memcpy(dst, src, src_width*src_height*sizeof(unsigned char));
        return;
    }

    /* allocate storage for filter coefficients */
    double *h = (double *)rvalloc(NULL, phases*taps*sizeof(double), 0);
    double *v = (double *)rvalloc(NULL, phases*taps*sizeof(double), 0);

    /* allocate storage for horizontally scaled image */
    unsigned char *buf = (unsigned char *)rvalloc(NULL, src_height*dst_width*sizeof(unsigned char), 0);

    /* calculate horizontal polyphase filter coefficients */
    double factor = (double)dst_width/(double)src_width;
    build_polyphase(h, factor, phases, taps, 1.0, algo);

    /* calculate vertical polyphase filter coefficients */
    factor = (double)dst_height/(double)src_height;
    build_polyphase(v, factor, phases, taps, 1.0, algo);

    /* horizontally resample (loop over input lines) */
    for (y=0; y<src_height; y++) {
        /* pointer to the last sample in line */
        const unsigned char *maxx = src + y*src_width + src_width - 1;
        scale_polyphase_single(src+y*src_width, maxx, 1, src_width, buf+y*dst_width, 1, dst_width, h, phases, taps);
    }

    /* pointer to the last line in image */
    const unsigned char *maxy = buf + dst_width*(src_height-1);

    /* vertically resample (loop over output columns) */
    for (x=0; x<dst_width; x++)
        scale_polyphase_single(buf+x, maxy+x, dst_width, src_height, dst+x, dst_width, dst_height, v, phases, taps);

    rvfree(buf);
    rvfree(h);
    rvfree(v);
}

/*
 * rvimage image resampling
 */
void rvimage::scale_config(algo_t a, int t, int p)
{
    /* modify resampling parameters */
    algo = a;
    taps = t;
    phases = p;

    /* sanity check resampling parameters */
    if (algo==BICUBIC)
        taps = 4;
    if ((algo>=BARTLETT || algo<=KAISER) && taps&1)
        rvexit("cannot have an odd number of taps in the polyphase resampling filter: %d", taps);
}

void rvimage::scale(const rvimage &src)
{
    /* TODO resample packed data formats */
    const int step = 1;

    /* sanity check */
    if (width==0 || height==0) {
        rvmessage("cannot scale into an uninitialised image");
        return;
    }

    /* check for nop */
    if (src.width==width && src.height==height && src.hsub==hsub && src.vsub==vsub)
        return copy(src);

#if 0
    /* prepare pointers to each plane of the images */
    unsigned char *src[3] = {NULL, NULL, NULL};
    unsigned char *dst[3] = {NULL, NULL, NULL};
    switch (inp_fourcc) {
        case I420:
        case IYUV:
            src[0] = inp_data;
            src[1] = inp_data+inp_width*inp_height;
            src[2] = inp_data+inp_width*inp_height+inp_width*inp_height/inp_hsub/inp_vsub;
            dst[0] = out_data;
            dst[1] = out_data+out_width*out_height;
            dst[2] = out_data+out_width*out_height+out_width*out_height/out_hsub/out_vsub;
            break;

        case UYVY:
            src[0] = inp_data+1;
            src[1] = inp_data;
            src[2] = inp_data+2;
            dst[0] = out_data+1;
            dst[1] = out_data;
            dst[2] = out_data+2;
            break;
    }

    /* prepare step size */
    int step=0;
    switch (inp_fourcc) {
        case I420:
        case IYUV:
            step = 1;
            break;

        case UYVY:
            step = 4;
            break;
    }
#endif

    /* scale input image to output image */
    /* note the chroma resampling currently takes no account of resiteing */
    switch (algo) {
        case NEAREST:
            scale_nearest(src.plane[0], src.width,          src.height,          step, plane[0], width,      height,      step);
            scale_nearest(src.plane[1], src.width/src.hsub, src.height/src.vsub, step, plane[1], width/hsub, height/vsub, step);
            scale_nearest(src.plane[2], src.width/src.hsub, src.height/src.vsub, step, plane[2], width/hsub, height/vsub, step);
            break;

        case BILINEAR:
            scale_bilinear(src.plane[0], src.width,          src.height,          plane[0], width,      height);
            scale_bilinear(src.plane[1], src.width/src.hsub, src.height/src.vsub, plane[1], width/hsub, height/vsub);
            scale_bilinear(src.plane[2], src.width/src.hsub, src.height/src.vsub, plane[2], width/hsub, height/vsub);
            break;

        case BICUBIC:
        case BARTLETT:
        case VONHANN:
        case HAMMING:
        case BLACKMAN:
        case BLACKMANHARRIS:
        case BLACKMANNUTTALL:
        case KAISER:
            scale_polyphase(src.plane[0], src.width,          src.height,          plane[0], width,      height,      phases, taps, algo);
            scale_polyphase(src.plane[1], src.width/src.hsub, src.height/src.vsub, plane[1], width/hsub, height/vsub, phases, taps, algo);
            scale_polyphase(src.plane[2], src.width/src.hsub, src.height/src.vsub, plane[2], width/hsub, height/vsub, phases, taps, algo);
            break;

        default:
            copy(src);
            break;
    }
}
