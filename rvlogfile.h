
#ifndef _RVLOGFILE_H_
#define _RVLOGFILE_H_

#include <stdio.h>
#include "rvh264.h"
#include "rvhevc.h"

/* rvlogfile.c */
int seqsort(const void *a, const void *b);
int parse_jmlog_header(FILE *trace, struct rv264seq_parameter_set *sps, struct rv264pic_parameter_set *pps);
int parse_jmlog_macros(FILE *trace, int num_macros, struct rv264macro macro[], struct rv264seq_parameter_set *sps, struct rv264pic_parameter_set *pps);

int parse_hmlog_header(FILE *trace, struct hevc_vid_parameter_set *vps, struct hevc_seq_parameter_set *sps, struct hevc_pic_parameter_set *pps);
int parse_hmlog_macros(FILE *trace, int num_macros, struct rvhevccodingunit macro[], struct hevc_vid_parameter_set *vps, struct hevc_seq_parameter_set *sps, struct hevc_pic_parameter_set *pps);

#endif
