
#ifndef _RVY4M_H_
#define _RVY4M_H_

#include "rvbits.h"

/* rvutil.c */
int write_y4m_stream_header(int width, int height, chromaformat_t chromaformat, FILE *stream);
int write_y4m_frame_header(FILE *stream);
int read_y4m_stream_header(int *width, int *height, fourcc_t *fourcc, FILE *stream);
int read_y4m_frame_header(FILE *stream);
int read_y4m_stream_header_bits(struct bitbuf *bb, int *width, int *height, framerate_t *framerate, fourcc_t *fourcc);
int read_y4m_frame_header_bits(struct bitbuf *bb);
int write_y4m_frame_header_bits(struct bitbuf *bb);

#endif
