/*
 * Description: Crop bitstream to have sensible start and end points.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-12-21 10:47:33 $
 * Revision   : $Revision: 1.45 $
 * Copyright  : (c) 2005-2008 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "getopt.h"
#include <math.h>

#include "rvutil.h"
#include "rvm2v.h"
#include "rvh264.h"
#include "rvmp2.h"
#include "rvcrc.h"
#include "rvhevc.h"

#define STATUS_PARSE_ERROR 2

/* different ts clean algorithms */
#define ALGO_FIRST_SPS  0x1
#define ALGO_FIRST_PUSI 0x2
#define ALGO_PCR_RESET  0x4

const char *appname;

extern char *nal_start[2];

void usage(int exitcode)
{
    fprintf(stderr, "%s: crop bitstream to have sensible start and end points\n", appname);
    fprintf(stderr, "usage: %s [options] [<file>] [<file>...]\n", appname);
    fprintf(stderr, "  -t, --type          : force type of file (default: autodetect)\n");
    fprintf(stderr, "  -a, --algorithm     : clean algorithm for transport streams, one or more of firstsps|firstpusi|pcrreset (default: firstsps|firstpusi)\n");
    fprintf(stderr, "  -p, --pid           : only check given pid in transport stream\n");
    fprintf(stderr, "  -o, --output        : write output to file\n");
    fprintf(stderr, "  -q, --quiet         : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "  -v, --verbose       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "  --                  : disable argument processing\n");
    fprintf(stderr, "  -h, --help, --usage : print this usage message\n");
    exit(exitcode);
}

int clean_m2v(struct bitbuf *bb, const char *filename, int verbose)
{
    int status = 0;

    /* parse mpeg2 headers */
    struct rvm2vsequence *seq = parse_m2v_headers(bb, verbose);
    if (seq==NULL) {
        rvmessage("failed to parse headers in mpeg2 sequence \"%s\"", filename);
        status = STATUS_PARSE_ERROR;
        return status;
    }

    /* get frame height and width */
    //int width  = seq->width_in_mbs*16;
    //int height = seq->height_in_mbs*16;

    /* decode each picture */
    int picno;
    for (picno=0; ; picno++) {

        /* look for end of sequence code */
        if (showbits32(bb)==0x1B7)
            break;

        /* find next picture start code */
        int gop_flags = 0;
        if (find_m2v_next_picture(bb, seq, picno, &gop_flags, verbose)<0) {
            rvmessage("%s: sequence does not end with a sequence end code", filename);
            break;
        }

        /* parse next picture */
        struct rvm2vpicture *picture = parse_m2v_picture(bb, seq, picno, verbose);
        if (picture==NULL) {
            rvmessage("failed to parse picture %d of mpeg2 sequence \"%s\"", picno, filename);
            status = STATUS_PARSE_ERROR;
            break;
        }

        /* measure size of picture in bits */
        //int picture_size = numbits(bb)-vbv_bits;

        /* tidy up */
        free_m2v_picture(picture);
    }

    /* tidy up */
    free_m2v_sequence(seq);

    return status;
}

int clean_264(struct bitbuf *bb, const char *filename, int verbose)
{
    const char *where = "h.264 nal unit stream";
    int status = 0;
    int numnals = 0, numpics = 0;

    /* copy nal units to output file until it becomes apparent that the
     * coded slice is not decodable, then truncate output and wait for
     * a possible restart point */
    bb->copybits = 1;

    /* allocate h.264 sequence struct */
    size_t size = sizeof(struct rv264sequence);
    struct rv264sequence *seq = (struct rv264sequence *)rvalloc(NULL, size, 1);

    /* loop over nal units until sufficient parameter nals have been parsed */
    int firstnal = 1; /* first nal in access unit */
    do {

        /* search for the next nal unit */
        int zerobyte, pos = numbits(bb);
        int nal_unit_type = find_next_nal_unit(bb, &zerobyte);
        if (verbose>=1 && numbits(bb)-pos>7+zerobyte*8)
            rvmessage("%s: %jd bits of leading garbage before nal unit", where, numbits(bb)-pos);
        if (nal_unit_type<0) {
            rvfree(seq);
            break;
        }

        /* look for possible restart in output copy NOTE zerobyte will be missing as consumed in find_next_nal_unit() */
        if (nal_unit_type==NAL_AUD || nal_unit_type==NAL_SPS) {
            bb->copybits = 1;
        }

        /* parse nal unit header */
        if (zerobyte)
            flushbits(bb, 8);           /* zero_byte */
        flushbits(bb, 24);              /* start_code_prefix_one_3bytes */
        int nal_ref_idc;
        int ret = parse_nal_unit_header(bb, &nal_unit_type, &nal_ref_idc);
        if (ret<0)
            rvexit("%s: failed to parse nal unit header in file \"%s\"", where, bb->filename);

        /* prepare to parse rbsp */
        struct bitbuf *rbsp = extract_rbsp(bb, verbose);
        if (verbose>=1)
            rvmessage("%s: nal: %s start: length=%d ref_idc=%d unit_type=%s", where, nal_start[zerobyte], numbitsleft(rbsp)/8+1, nal_ref_idc, nal_unit_type_code(nal_unit_type));

        /* select nal unit type */
        switch (nal_unit_type) {

            case NAL_CODED_SLICE_NON_IDR: /* coded slice of a non-IDR picture */
            case NAL_CODED_SLICE_IDR: /* coded slice of an IDR picture */
            {
                /* parse slice header */
                struct slice_header *sh = parse_slice_header(rbsp, nal_unit_type, nal_ref_idc, seq->pps, verbose);
                if (sh==NULL)
                    rvmessage("%s: failed to parse slice header", where);
                if (verbose>=2)
                    rvmessage("%s: first_mb_in_slice=%d", where, sh->first_mb_in_slice);

                /* check that the parameter sets exist to decode this slice */
                if (sh &&
                    seq->pps[sh->pic_parameter_set_id] && seq->pps[sh->pic_parameter_set_id]->occupied &&
                    seq->sps[seq->pps[sh->pic_parameter_set_id]->seq_parameter_set_id] && seq->sps[seq->pps[sh->pic_parameter_set_id]->seq_parameter_set_id]->occupied)
                {
                    break;
                } else {
                    truncbits(bb);
                    bb->copybits = 0;
                    numnals = 0;
                    numpics = 0;
                }
                break;
            }

            case NAL_SEI: /* supplemental enhancement information */
                /* check for long start code (Annex B.1.2) */
                if (verbose>=0 && firstnal && zerobyte==0)
                    rvmessage("%s: sei nal unit which is first in access unit should have a long start code", where);

                /* parse supplemental enhancement information */
                parse_sei(rbsp, seq, verbose);
                break;

            case NAL_SPS: /* sequence parameter set */
            {
                /* check for long start code (Annex B.1.2) */
                if (verbose>=0 && zerobyte==0)
                    rvmessage("%s: sps nal unit should have a long start code", where);

                /* parse sequence parameter set */
                struct rv264seq_parameter_set *sps = parse_sequence_param(rbsp);
                if (sps==NULL)
                    rvexit("%s: failed to parse sequence parameter set header in file \"%s\"", where, bb->filename);
                if (verbose>=2)
                    rvmessage("%s: seq_parameter_set_id=%d", where, sps->seq_parameter_set_id);

                /* store sequence parameter set */
                rvfree(seq->sps[sps->seq_parameter_set_id]);
                seq->sps[sps->seq_parameter_set_id] = sps;

                break;
            }

            case NAL_PPS: /* picture parameter set */
            {
                /* check for long start code (Annex B.1.2) */
                if (verbose>=0 && zerobyte==0)
                    rvmessage("%s: pps nal unit should have a long start code", where);

                /* parse picture parameter set */
                struct rv264pic_parameter_set *pps = parse_picture_param(rbsp, seq->sps, verbose);
                if (pps==NULL) /* TODO handle this more gracefully */
                    rvexit("%s: failed to parse picture parameter set header in file \"%s\"", where, bb->filename);
                if (verbose>=2)
                    rvmessage("%s: pic_parameter_set_id=%d seq_parameter_set_id=%d", where, pps->pic_parameter_set_id, pps->seq_parameter_set_id);

                /* store sequence parameter set */
                rvfree(seq->pps[pps->pic_parameter_set_id]);
                seq->pps[pps->pic_parameter_set_id] = pps;

                break;
            }

            case NAL_AUD:
                if (bb->copybits)
                    numpics++;  // TODO detect start of AU without AUD_NUT
                break;

            case NAL_EOQ: /* end of sequence */
                if (verbose>=3)
                    rvmessage("%s: found end of sequence nal", where);
                rvfree(seq);
                return status;

            case NAL_EOS: /* end of stream */
                if (verbose>=3)
                    rvmessage("%s: found end of stream nal", where);
                rvfree(seq);
                return status;

            default:
                //rvmessage("%s: skipping nal_unit_type=%d", where, nal_unit_type);
                break;

        }
        firstnal = 0;

        /* count nal units copied to output */
        if (bb->copybits)
            numnals++;

        /* tidy up */
        free_rbsp(rbsp);

    //} while (find_complete_parameter_set(seq)<0);
    } while (!eofbits(bb));

    if (verbose>=0)
        rvmessage("wrote %d nal units, %d pictures", numnals, numpics);

    /* tidy up */
    free_264_sequence(seq);

    return status;
}

int clean_hevc(struct bitbuf *bb, const char *filename, int verbose)
{
    const char *where = "hevc nal unit stream";
    int status = 0;
    int numnals = 0, numpics = 0;

    /* copy nal units to output file until it becomes apparent that the
     * coded slice is not decodable, then truncate output and wait for
     * a possible restart point */
    bb->copybits = 1;

    /* allocate hevc video struct */
    size_t size = sizeof(struct rvhevcvideo);
    struct rvhevcvideo *vid = (struct rvhevcvideo *)rvalloc(NULL, size, 1);

    /* loop over nal units until sufficient parameter nals have been parsed */
    do {

        /* search for the next nal unit */
        int zerobyte, pos = numbits(bb);
        int nal_unit_type = find_next_hevc_nal_unit(bb, &zerobyte);
        if (verbose>=1 && numbits(bb)-pos>7+8*zerobyte)
            rvmessage("%jd bits of leading garbage before first nal in access unit", numbits(bb)-pos);
        if (nal_unit_type<0) {
            break;
        }

        /* look for possible restart in output copy NOTE zerobyte will be missing as consumed in find_next_hevc_nal_unit() */
        if (nal_unit_type==AUD_NUT || nal_unit_type==VPS_NUT || nal_unit_type==SPS_NUT) {
            bb->copybits = 1;
        }

        /* parse nal unit header */
        flushbits(bb, 24);              /* start_code_prefix_one_3bytes */
        int ret = parse_hevc_nal_unit_header(bb, &nal_unit_type);
        if (ret<0)
            rvexit("%s: failed to parse nal unit header in file \"%s\"", where, bb->filename);

        /* prepare to parse rbsp */
        struct bitbuf *rbsp = extract_rbsp(bb, verbose);
        if (verbose>=1)
            rvmessage("nal_unit %3d: nal: %s start: length=%d unit_type=%s", numnals, nal_start[zerobyte], numbitsleft(rbsp)/8+1, hevc_nal_unit_type_code(nal_unit_type));

        /* select nal unit type */
        switch (nal_unit_type) {

            case PREFIX_SEI_NUT: /* supplemental enhancement information */
                /* check for long start code (Annex B.1.2) */
                if (verbose>=0 && numnals==0 && zerobyte==0)
                    rvmessage("%s: sei nal unit which is first in access unit should have a long start code", where);

                /* parse supplemental enhancement information */
                parse_hevc_sei_rbsp(rbsp, vid->sps[vid->active_sps], nal_unit_type, verbose);
                break;

            case VPS_NUT: /* video parameter set */
            {
                /* check for long start code (Annex B.2.2) */
                if (verbose>=0 && zerobyte==0)
                    rvmessage("%s: vps nal unit should have a long start code", where);

                /* parse video parameter set */
                struct hevc_vid_parameter_set *vps = parse_hevc_vid_param(rbsp);
                if (vps==NULL)
                    rvexit("%s: failed to parse video parameter set header in file \"%s\"", where, bb->filename);
                if (verbose>=2)
                    rvmessage("%s: vid_parameter_set_id=%d", where, vps->vid_parameter_set_id);

                /* store sequence parameter set */
                rvfree(vid->vps[vps->vid_parameter_set_id]);
                vid->vps[vps->vid_parameter_set_id] = vps;

                break;
            }

            case SPS_NUT: /* sequence parameter set */
            {
                /* check for long start code (Annex B.2.2) */
                if (verbose>=0 && zerobyte==0)
                    rvmessage("%s: sps nal unit should have a long start code", where);

                /* parse sequence parameter set */
                struct hevc_seq_parameter_set *sps = parse_hevc_seq_param(rbsp);
                if (sps==NULL)
                    rvexit("%s: failed to parse sequence parameter set header in file \"%s\"", where, bb->filename);
                if (verbose>=2)
                    rvmessage("%s: seq_parameter_set_id=%d", where, sps->seq_parameter_set_id);

                /* store sequence parameter set */
                rvfree(vid->sps[sps->seq_parameter_set_id]);
                vid->sps[sps->seq_parameter_set_id] = sps;

                break;
            }

            case PPS_NUT: /* picture parameter set */
            {
                /* check for long start code (Annex B.2.2) */
                if (verbose>=0 && zerobyte==0)
                    rvmessage("%s: pps nal unit should have a long start code", where);

                /* parse picture parameter set */
                struct hevc_pic_parameter_set *pps = parse_hevc_pic_param(rbsp, vid->sps);
                if (pps==NULL) /* TODO handle this more gracefully */
                    rvexit("%s: failed to parse picture parameter set header in file \"%s\"", where, bb->filename);
                if (verbose>=2)
                    rvmessage("%s: pic_parameter_set_id=%d seq_parameter_set_id=%d", where, pps->pic_parameter_set_id, pps->seq_parameter_set_id);

                /* store sequence parameter set */
                rvfree(vid->pps[pps->pic_parameter_set_id]);
                vid->pps[pps->pic_parameter_set_id] = pps;

                break;
            }

            case TRAIL_N:
            case TRAIL_R:
            case TSA_N:          /* temporal sub-layer access */
            case TSA_R:
            case STSA_N:         /* step-wise temporal sub-layer */
            case STSA_R:
            case RADL_N:         /* random-access decodable */
            case RADL_R:
            case RASL_N:         /* random-access skipped leading */
            case RASL_R:
            case RSV_VCL_N10:
            case RSV_VCL_R11:
            case RSV_VCL_N12:
            case RSV_VCL_R13:
            case RSV_VCL_N14:
            case RSV_VCL_R15:
            case BLA_W_LP:       /* broken link access */
            case BLA_W_RADL:
            case BLA_N_LP:
            case IDR_W_RADL:     /* instantaneous decoding refresh */
            case IDR_N_LP:
            case CRA_NUT:        /* clean random access */
            {
                /* parse slice header */
                const char *where = "hevc slice header";

                /* parse slice header */
                int first_slice_segment_in_pic_flag = getbits(rbsp, 1);               /* first_slice_segment_in_pic_flag */
                if (nal_unit_type >= BLA_W_LP && nal_unit_type <= RSV_IRAP_VCL23)
                    flushbits(rbsp, 1);                                               /* no_output_of_prior_pics_flag */
                int pic_parameter_set_id = uexpbits(rbsp);                            /* pic_parameter_set_id */
                if (pic_parameter_set_id>255) {
                    rvmessage("%s: pic_parameter_set_id is greater than 255: %d", where, pic_parameter_set_id);
                    break;
                }
                if (verbose>=2)
                    rvmessage("%s: first_slice_segment_in_pic_flag=%d slice_pic_parameter_set_id=%d", where, first_slice_segment_in_pic_flag, pic_parameter_set_id);

                /* check that the parameter sets exist to decode this slice */
                if (vid->pps[pic_parameter_set_id] && vid->pps[pic_parameter_set_id]->occupied &&
                    vid->sps[vid->pps[pic_parameter_set_id]->seq_parameter_set_id] && vid->sps[vid->pps[pic_parameter_set_id]->seq_parameter_set_id]->occupied)
                {
                    break;
                } else {
                    truncbits(bb);
                    bb->copybits = 0;
                    numnals = 0;
                    numpics = 0;
                }
                break;
            }

            case EOS_NUT: /* end of sequence */
                if (verbose>=3)
                    rvmessage("%s: found end of sequence nal", where);
                rvfree(vid);
                return status;

            case EOB_NUT: /* end of bitstream */
                if (verbose>=3)
                    rvmessage("%s: found end of stream nal", where);
                rvfree(vid);
                return status;

            case AUD_NUT:
                if (bb->copybits)
                    numpics++;  // TODO detect start of AU without AUD_NUT
                break;

            case FD_NUT:
                break;

            default:
                rvmessage("%s: skipping nal_unit_type=%d", where, nal_unit_type);
                break;

        }

        /* count nal units copied to output */
        if (bb->copybits)
            numnals++;

        /* tidy up */
        free_rbsp(rbsp);

    } while (!eofbits(bb));

    if (verbose>=0)
        rvmessage("wrote %d nal units, %d pictures", numnals, numpics);

    rvfree(vid);
    return status;
}

void clean_ts(struct bitbuf *bb, const char *filename, FILE *outfile, int checkpid, int algo, int verbose)
{
    int i, j;

    /* initialise copy buffer */
    int copy_flag = 0;
    const int COPY_CHUNK_SIZE = 1024;
    int copy_index = 0, copy_packets = 0;
    unsigned char *copy_buffer = NULL;

    /* initialise continuity counter check */
    char continuity[8192];
    memset(continuity, -1, sizeof(continuity));

    /* initialise pcr timing check */
    long long pcr_prev[8192];
    int packno_prev[8192] = {0};
    memset(pcr_prev, -1, sizeof(pcr_prev));

    /* initialise pcr accuracy check */
    long long pcr_ac_min = 0x7FFFFFFFFFFFFFFFll;
    long long pcr_ac_max = 0;
    long long pcr_ac_sum = 0;
    long long pcr_ac_cnt = 0;

    /* initialise running bitrate measure */
    long long bitrate_pcr_sum = 0;
    int bitrate_packet_sum = 0;

    /* initialise video stream variables */
    stream_type_t stream_type = STREAM_TYPE_RESERVED;
    int vid_pid = checkpid>0 ? checkpid : 0;
    //int pmt_pid = 0;
    long long prev_pts = -1;
    long long prev_dts = -1;

    /* initialise duplicate packet detection */
    crc32_t prev_header_crc32[8192] = {0};
    crc32_t prev_payload_crc32[8192] = {0};

    /* initialise program information tables */
    struct program_association_table *pat = (struct program_association_table *)rvalloc(NULL, sizeof(struct program_association_table), 1);
    struct program_map_table *pmt[MAX_PMTS] = {NULL};
    pat->version_displayed = -1;

    /* initialise section buffers */
    /* buffers include space for an extra transport packet of packet stuffing bytes */
    u_int8_t pat_section[SECTION_BUFFER_SIZE];
    u_int8_t *pmt_section[MAX_PMTS] = {NULL};
    int pat_pointer = 0, pmt_pointer[MAX_PMTS];

    /* initialise bitrate measure */
    int pcr_pid = 0;
    long long first_pcr[8192];
    long long last_pcr[8192];
    int pcr_wraparound[8192];
    int first_pcr_packet[8192];
    int last_pcr_packet[8192];
    memset(first_pcr, -1, sizeof(first_pcr));
    memset(last_pcr, -1, sizeof(last_pcr));
    memset(pcr_wraparound, 0, sizeof(pcr_wraparound));
    memset(first_pcr_packet, 0, sizeof(first_pcr_packet));
    memset(last_pcr_packet, 0, sizeof(last_pcr_packet));

    /* initialise pid statistics */
    int pid_count[8192];
    memset(pid_count, 0, sizeof(pid_count));

    /* initialise h.264 specific parameters */
    struct rv264sequence *seq = (struct rv264sequence *) rvalloc(NULL, sizeof(struct rv264sequence), 1);

    /* loop over transport stream packets */
    int packno = 0, copyno = 0;
    while (!eofbits(bb)) {

        /* resynchronise */
        int search_bits = find_next_sync_code(bb);
        if (search_bits<0)
            break;
        if (verbose>=1 && search_bits)
            rvmessage("%s: packet %5d: searched %d bytes to find next sync byte", filename, packno, search_bits/8);

        /* read the next complete transport stream packet */
        u_int8_t packet[188];
        if (read_transport_packet(bb, verbose, packet)<0)
            break;
        packno++;

        /* check for synchronisation */
        if (showbits(bb, 8)!=0x47 && !eofbits(bb))
            rvmessage("%s: packet %5d: packet does not have end sync", filename, packno);

        /* prepare to parse transport stream packet */
        struct bitbuf *pb = initbits_memory(packet, 188);

        /* parse transport packet */
        int transport_error_indicator, payload_unit_start_indicator;
        int pid, adaptation_field_control, continuity_counter, pcr_flag;
        int discontinuity_indicator;
        long long pcr;
        int header_length = parse_transport_packet_header(pb, verbose, packno, &transport_error_indicator,
                        &payload_unit_start_indicator, &pid, &adaptation_field_control, &continuity_counter,
                        &discontinuity_indicator, &pcr_flag, &pcr);
        if (header_length<0) {
            rvmessage("%s: packet %5d: failed to parse transport packet header", filename, packno);
            break;
        }

        /* skip checks if requested */
        if (checkpid>=0 && pid!=checkpid)
            continue;

        /* give the memory bitbuf a better name */
        char bufname[16];
        snprintf(bufname, sizeof(bufname), "pid %d", pid);
        pb->filename = bufname;

        /* detect duplicate packets */
        /* NOTE the pcr's of duplicate packets will differ, so we need to skip them */
        crc32_t header_crc32  = crc32(packet, 4);
        crc32_t payload_crc32 = crc32(packet+header_length, 188-header_length);
        int duplicate_packet = (header_crc32==prev_header_crc32[pid]) && (payload_crc32==prev_payload_crc32[pid])? 1 : 0;
        prev_header_crc32[pid] = header_crc32;
        prev_payload_crc32[pid] = payload_crc32;

        /* check transport error */
        if (pid>=0 && pid<0x1FFF)
            if (transport_error_indicator && verbose>=0)
                rvmessage("%s: packet %5d: transport error has been indicated", filename, packno);

        /* check continuity_counter */
        if (pid>=0 && pid<0x1FFF) {
            if (continuity[pid]>=0) {
                int next_continuity = adaptation_field_control==0 || adaptation_field_control==2? continuity[pid] : (continuity[pid]+1)%16;
                if (next_continuity != continuity_counter) {
                    if (discontinuity_indicator) {
                        if (verbose>=0)
                            rvmessage("%s: packet %5d: discontinuity has been indicated (continuity goes from %d to %d)", filename, packno, continuity[pid], continuity_counter);
                    } else if ((continuity[pid] == continuity_counter) && duplicate_packet)
                        rvmessage("%s: packet %5d: duplicate packet", filename, packno);
                    else
                        rvmessage("%s: packet %5d: continuity error in pid %d, expected %d, packet has %d", filename, packno, pid, next_continuity, continuity_counter);
                }
            }
            continuity[pid] = continuity_counter;
        }

        /* check program clock recovery */
        if (pid>=0 && pid<0x1FFF) {
            if (pcr_flag) {
                if (pcr_prev[pid]>=0) {
                    if (!discontinuity_indicator) {
                        if (pcr>pcr_prev[pid]) {
                            /* this is expected timing information */
                            if (pcr-pcr_prev[pid]>100ll*27000ll)
                                rvmessage("%s: packet %5d: pcr late in pid %d: %lld ms (%lld - %lld)", filename, packno, pid, (pcr-pcr_prev[pid])/27000ll, pcr, pcr_prev[pid]);

                            /* update running bitrate measure */
                            long long pcr_delta = pcr - pcr_prev[pid];
                            int packet_delta = packno - packno_prev[pid];
                            bitrate_pcr_sum += pcr_delta;
                            bitrate_packet_sum += packet_delta;
                            if (verbose>=3)
                                rvmessage("%s: latest transport stream bitrate estimate is %.2fMbps", filename, (bitrate_packet_sum*188.0*8.0*27.0) / (double)bitrate_pcr_sum);

                            /* check pcr accuracy */
                            long long expected_pcr = (long long)packet_delta * bitrate_pcr_sum / (long long)bitrate_packet_sum;
                            //long long expected_pcr = (long long)packet_delta * (last_pcr-first_pcr) / (long long)(last_pcr_packet-first_pcr_packet);
                            long long pcr_ac = pcr_delta - expected_pcr;
                            if (abs(pcr_ac)*1000ll > 27ll*500ll) { /* 500 ns */
                                rvmessage("%s: packet %5d: pid %4d: pcr_ac of %lldns, bitrate estimate is %.2fMbps", filename, packno, pid, pcr_ac*1000ll/27ll, (bitrate_packet_sum*188.0*8.0*27.0) / (double)bitrate_pcr_sum);
                                //rvmessage("expected %lld delta %lld pcr_ac %lld", expected_pcr, pcr_delta, pcr_ac);
                                //rvmessage("packet %d previous %d delta %d", packno, packno_prev[pid], packet_delta);
                            } else if (verbose>=3)
                                rvmessage("%s: packet %5d: pid %4d: pcr_ac of %lldns", filename, packno, pid, pcr_ac*1000ll/27ll);

                            /* measure overall pcr accuracy */
                            if (pid==pcr_pid) {
                                if (pcr_ac<pcr_ac_min)
                                    pcr_ac_min = pcr_ac;
                                if (pcr_ac>pcr_ac_max)
                                    pcr_ac_max = pcr_ac;
                                pcr_ac_sum += pcr_ac;
                                pcr_ac_cnt++;
                            }

                            if (verbose>=2)
                                rvmessage("%s: packet %5d: pcr=%.2fms", filename, packno, pcr_to_ms(pcr));

                        } else {
                            /* this is degenerate pcr timing */
                            if (pcr==pcr_prev[pid])
                                rvmessage("%s: packet %5d: pcr in pid %d is not incrementing", filename, packno, pid);
                            if (pcr<pcr_prev[pid]) {
                                rvmessage("%s: packet %5d: pcr in pid %d jumps back in time: pcr=%.2fms previous=%.2fms",
                                          filename, packno, pid, pcr_to_ms(pcr), pcr_to_ms(pcr_prev[pid]));
                                // this was used to clean up a specific transport stream (ORTG).
                                if ( !copy_flag && (algo&ALGO_PCR_RESET) ) {
                                    if (verbose>=1)
                                        rvmessage("%s: packet %5d: starting copy to output at pcr=%.2fms", filename, packno, pcr_to_ms(pcr));
                                    copy_flag = 1;
                                }
                            }
                        }
                    }
                }

                if (verbose>=4) {
                    /*long long clock = pcr/27000ll;
                    int msecs = clock % 1000ll;
                    clock /= 1000ll;
                    int secs = clock % 60ll;
                    clock /= 60ll;
                    int mins = clock % 60ll;
                    clock /= 60ll;
                    int hours = clock;*/
                    if (pcr_prev[pid]>=0)
                        rvmessage("%s: packet %5d: pcr in pid %d is %lld (increase of %lldmsecs after %dbytes)", filename, packno, pid, pcr, (pcr-pcr_prev[pid])/27000ll, (packno-packno_prev[pid])*188);
                    else
                        rvmessage("%s: packet %5d: pcr in pid %d is %lld (first pcr)", filename, packno, pid, pcr);
                }

                pcr_prev[pid] = pcr;
                packno_prev[pid] = packno;
            }

            /* record statistics */
            pid_count[pid]++;
        }

        /* measure bitrate for each pid */
        if (pcr_flag) {
            if (pcr_pid==0 && pcr!=0)
                pcr_pid = pid; /* use this pid to measure bitrate */
            /* find first and last pcrs in bitstream */
            if (first_pcr[pid]<0) {
                first_pcr[pid] = pcr;
                first_pcr_packet[pid] = packno-1;
            } else {
                if (pcr < (last_pcr[pid]<0? first_pcr[pid] : last_pcr[pid]))
                    pcr_wraparound[pid]++;
                last_pcr[pid] = pcr;
                last_pcr_packet[pid] = packno-1;
            }
            if (verbose>=2)
                rvmessage("pcr: pid=%d pcr=%lld", pid, pcr);
        }

        /* calculate payload size and check sanity */
        int payload_size = 188-header_length;
        if (payload_size<0) {
            if (verbose>=1)
                rvmessage("error parsing transport packet header, resyncing");
            freebits(pb);
            continue;
        }

        /* parse transport packet payload */
        int pes_packet_data_length = 0;
        if (adaptation_field_control==1 || adaptation_field_control==3) {
            if (pid==0x0000) {
                /* look for new pat section */
                if (payload_unit_start_indicator) {
                    payload_size -= parse_pointer_field(pb);
                    pat_pointer = 0;
                }

                /* copy payload to pat section */
                while (payload_size) {
                    if (pat_pointer==1024) {
                        rvmessage("%s: packet %5d: pat section is too large", filename, packno);
                        break;
                    }
                    pat_section[pat_pointer++] = getbits8(pb);
                    payload_size--;
                }

                /* try to parse pat section */
                struct bitbuf *sb = initbits_memory(pat_section, pat_pointer);
                int section_length = parse_program_association_section(sb, pat_pointer, verbose, pat);
                freebits(sb);

                if (section_length && pat->version_displayed!=pat->version_number) {
                    if (verbose>=1)
                        describe_program_association(pat);

                    pat->version_displayed = pat->version_number;
                }

            } else if (pid==vid_pid) {

                /* look for a pes header */
                if (payload_unit_start_indicator) {
                    /* parse pes header */
                    int leading = 0;
                    while (showbits24(pb)!=0x000001 && !eofbits(pb))
                        leading += flushbits(pb, 8);
                    if (eofbits(pb)) {
                        freebits(pb);
                        continue;
                    }
                    if (leading && verbose>=1)
                        rvmessage("%s: packet %5d: %d bytes of leading garbage before start code in pes packet", filename, packno, leading/8);

                    flushbits(pb, 24);                                  /* packet_start_code_prefix */
                    int stream_id = getbits8(pb);                       /* stream_id */
                    int pes_packet_length = getbits(pb, 16);            /* PES_packet_length */
                    /*if (verbose>=2)
                        describe_pes_packet(stream_id, pes_packet_length);*/

                    /* parse pes packet */
                    long long pts = -1, dts = -1;
                    pes_packet_data_length = parse_pes_packet_header(pb, stream_id, pes_packet_length, &pts, &dts, verbose);
                    pes_packet_data_length = mmax(0, pes_packet_data_length); /* for unbounded pes packets */

                    if (verbose>=2) {
                        if (pts>=0 && prev_pts>=0 && dts>=0 && prev_dts>=0 && pts!=dts)
                            rvmessage("%s: packet %5d: pes: %d data bytes, pts=%.1fms dts=%.1fms pts_delta=%5.1fms dts_delta=%5.1fms dts_pts_delta=%5.1fms",
                                        filename, packno, pes_packet_data_length, pts_to_ms(pts), pts_to_ms(dts), pts_to_ms(pts-prev_pts), pts_to_ms(dts-prev_dts), pts_to_ms(pts-dts));
                        else if (pts>=0 && prev_pts>=0 && prev_dts>=0)
                            rvmessage("%s: packet %5d: pes: %d data bytes, pts=%.1fms dts=%.1fms pts_delta=%5.1fms dts_delta=%5.1fms",
                                        filename, packno, pes_packet_data_length, pts_to_ms(pts), pts_to_ms(pts), pts_to_ms(pts-prev_pts), pts_to_ms(pts-prev_dts));
                        else if (pts>=0 && dts>=0 && pts!=dts)
                            rvmessage("%s: packet %5d: pes: %d data bytes, pts=%.1fms dts=%.1fms                                     dts_pts_delta=%5.1fms",
                                        filename, packno, pes_packet_data_length, pts_to_ms(pts), pts_to_ms(dts), pts_to_ms(pts-dts));
                        else if (pts>=0)
                            rvmessage("%s: packet %5d: pes: %d data bytes, pts=%.1fms",
                                        filename, packno, pes_packet_data_length, pts_to_ms(pts));
                        else
                            rvmessage("%s: packet %5d: pes: %d data bytes", filename, packno, pes_packet_data_length);
                    }

                    if (pts>=0)
                        prev_pts = pts;
                    if (dts>=0)
                        prev_dts = dts;

                    /* parse elementary stream headers */
                    switch ( stream_type ) {
                        case STREAM_TYPE_RESERVED:
                            /* shouldn't really happen */
                            rvmessage("unknown stream type in video pid parsing");
                            break;

                        case STREAM_TYPE_VIDEO_H264:
                        {
                            /* loop over nal units until we hopefully find what we need inside this ts packet */
                            int nal_unit_type, found_slice = 0;
                            do {

                                /* search for the next nal unit */
                                int zerobyte, pos = numbits(pb);
                                nal_unit_type = find_next_nal_unit(pb, &zerobyte);
                                if (verbose>=1 && numbits(pb)-pos>7+zerobyte*8 && !eofbits(pb))
                                    rvmessage("%s: packet %5d: %jd bytes of leading garbage before nal unit", filename, packno, (numbits(pb)-pos)/8);
                                if (nal_unit_type<0) {
                                    break;
                                }

                                /* parse nal unit header */
                                if (zerobyte)
                                    flushbits(pb, 8);           /* zero_byte */
                                flushbits(pb, 24);              /* start_code_prefix_one_3bytes */
                                int nal_ref_idc;
                                int ret = parse_nal_unit_header(pb, &nal_unit_type, &nal_ref_idc);
                                if (ret<0)
                                    rvexit("%s: failed to parse nal unit header", filename);

                                /* prepare to parse rbsp */
                                struct bitbuf *rbsp = extract_rbsp(pb, verbose);
                                if (rbsp==NULL)
                                    break;
                                if (verbose>=2)
                                    rvmessage("%s: nal: %s start: length=%d ref_idc=%d unit_type=%s", filename, zerobyte? "long" : "short", numbitsleft(rbsp)/8+1, nal_ref_idc, nal_unit_type_code(nal_unit_type));

                                /* select nal unit type */
                                switch (nal_unit_type) {

                                    case NAL_CODED_SLICE_NON_IDR:   /* coded slice of a non-IDR picture */
                                    case NAL_CODED_SLICE_IDR:       /* coded slice of an IDR picture */
                                    {
                                        /* parse slice header */
                                        if ( seq->pps ) {
                                            struct slice_header *sh = parse_slice_header(rbsp, nal_unit_type, nal_ref_idc, seq->pps, verbose-1);
                                            if (sh==NULL) {
                                                //rvmessage("%s: failed to parse slice header", filename);
                                                break;
                                            }
                                            if (verbose>=3)
                                                rvmessage("%s: slice header: first_mb_in_slice=%d", filename, sh->first_mb_in_slice);

                                            if ( seq->pps[sh->pic_parameter_set_id] ) {
                                                seq->active_pps = sh->pic_parameter_set_id;
                                                if ( seq->sps[seq->pps[sh->pic_parameter_set_id]->seq_parameter_set_id] )
                                                    seq->active_sps = seq->pps[sh->pic_parameter_set_id]->seq_parameter_set_id;
                                            }
                                        }

                                        /* no need to search further in this packet */
                                        found_slice = 1;

                                        break;
                                    }

                                    case NAL_SEI: /* supplemental enhancement information */
                                        /* parse supplemental enhancement information */
                                        parse_sei(rbsp, seq, verbose);
                                        break;

                                    case NAL_SPS: /* sequence parameter set */
                                    {
                                        /* parse sequence parameter set */
                                        struct rv264seq_parameter_set *sps = parse_sequence_param(rbsp);
                                        if (sps==NULL)
                                            rvexit("%s: failed to parse sequence parameter set header", filename);
                                        if (verbose>=2)
                                            rvmessage("%s: seq_parameter_set_id=%d", filename, sps->seq_parameter_set_id);

                                        /* store sequence parameter set */
                                        rvfree(seq->sps[sps->seq_parameter_set_id]);
                                        seq->sps[sps->seq_parameter_set_id] = sps;

                                        /* start copy to output on first sps of video pid */
                                        if ( !copy_flag && (algo&ALGO_FIRST_SPS) ) {
                                            if (verbose>=1)
                                                rvmessage("%s: packet %5d: starting copy to output at pts=0x%09llx with a %s", filename, packno, pts, nal_unit_type_code(nal_unit_type));
                                            copy_flag = 1;
                                        }

                                        break;
                                    }

                                    case NAL_PPS: /* picture parameter set */
                                    {
                                        /* parse picture parameter set */
                                        struct rv264pic_parameter_set *pps = parse_picture_param(rbsp, seq->sps, verbose);
                                        if (pps==NULL) /* TODO handle this more gracefully */
                                            rvexit("%s: failed to parse picture parameter set header", filename);
                                        if (verbose>=2)
                                            rvmessage("%s: pic_parameter_set_id=%d seq_parameter_set_id=%d", filename, pps->pic_parameter_set_id, pps->seq_parameter_set_id);

                                        /* store sequence parameter set */
                                        rvfree(seq->pps[pps->pic_parameter_set_id]);
                                        seq->pps[pps->pic_parameter_set_id] = pps;

                                        break;
                                    }

                                    case NAL_AUD: /* access unit delimiter */
                                        break;

                                    case NAL_EOQ: /* end of sequence */
                                        if (verbose>=3)
                                            rvmessage("%s: found end of sequence nal", filename);
                                        break;

                                    case NAL_EOS: /* end of stream */
                                        if (verbose>=3)
                                            rvmessage("%s: found end of stream nal", filename);
                                        break;

                                    default:
                                        rvmessage("%s: skipping nal_unit_type=%s", filename, nal_unit_type_code(nal_unit_type));
                                        break;

                                }

                                /* tidy up */
                                free_rbsp(rbsp);

                            } while ( numbits(pb)>0 && !found_slice );

                            /* start copy to output on first payload_unit_start_indicator of video pid */
                            if ( !copy_flag && (algo&ALGO_FIRST_PUSI) ) {
                                if ( seq->sps[seq->active_sps] && seq->pps[seq->active_pps] ) {
                                    if (verbose>=1)
                                        rvmessage("%s: packet %5d: starting copy to output at pts=0x%09llx with a %s", filename, packno, pts, nal_unit_type_code(nal_unit_type));
                                    copy_flag = 1;
                                }
                            }

                            break;
                        }
                    }

                    /* flush copy buffer to file at start of new payload_unit */
                    if (copy_flag && copy_index>0) {
                        if (fwrite(copy_buffer, copy_index*188, 1, outfile)!=1)
                            rverror("failed to write to output");
                        copyno += copy_index;
                        copy_index = 0;
                    }

                    /* tidy up */
                }

            } else if (pid==8191) {

            } else {
                for (i=0; i<pat->number_programs; i++)
                    if (pid==pat->program[i].program_map_pid) {
                        /* allocate pmt table and pmt section */
                        if (pmt[i]==NULL) {
                            pmt[i] = (struct program_map_table *)rvalloc(NULL, sizeof(struct program_map_table), 1);
                            pmt_section[i] = (unsigned char *)rvalloc(NULL, SECTION_BUFFER_SIZE, 1);
                            pmt[i]->version_displayed = -1;
                            pmt_pointer[i] = 0;
                        }

                        /* look for new pmt section */
                        if (payload_unit_start_indicator) {
                            payload_size -= parse_pointer_field(pb);
                            pmt_pointer[i] = 0;
                        }

                        /* copy payload to pmt section */
                        while (payload_size) {
                            if (pmt_pointer[i]==1024) {
                                rvmessage("%s: packet %5d: pmt section is too large", filename, packno);
                                break;
                            }
                            pmt_section[i][pmt_pointer[i]++] = getbits8(pb);
                            payload_size--;
                        }

                        /* try to parse pmt section */
                        struct bitbuf *sb = initbits_memory(pmt_section[i], pmt_pointer[i]);
                        int section_length = parse_program_map_section(sb, pmt_pointer[i], verbose, pmt[i]);
                        freebits(sb);

                        if (section_length>0) {

                            if (pmt[i]->version_displayed!=pmt[i]->version_number) {
                                /* display pmt */
                                if (verbose>=1)
                                    describe_program_map(pmt[i]);

                                pmt[i]->version_displayed = pmt[i]->version_number;
                            }

                            /* default to the first video stream for timing analysis */
                            for (j=0; j<pmt[i]->number_streams && vid_pid==0; j++)
                                switch (pmt[i]->stream[j].stream_type) {
                                    case STREAM_TYPE_VIDEO_MPEG1:
                                    case STREAM_TYPE_VIDEO_MPEG2:
                                    case STREAM_TYPE_VIDEO_MPEG4:
                                    case STREAM_TYPE_VIDEO_H264:
                                    case STREAM_TYPE_VIDEO_HEVC:
                                        stream_type = pmt[i]->stream[j].stream_type;
                                        vid_pid = pmt[i]->stream[j].elementary_pid;
                                        //pmt_pid = pid;
                                        if (verbose>=1)
                                            rvmessage("%s: video pid is %d, stream %d in program %d, pcr_pid is %d", filename, vid_pid, j, pmt[i]->program_number, pmt[i]->pcr_pid);
                                        break;
                                }

                            /* note the pcr_pid for the video stream */
                            if (pcr_pid==0 && vid_pid==pmt[i]->stream[j].elementary_pid)
                                pcr_pid = pmt[i]->pcr_pid;
                        }
                    }
            }
        }

        /* copy transport stream packet to output if required */
        if (copy_flag) {
            /* record statistics */
            if (pid>=0 && pid<0x1FFF)
                pid_count[pid]++;

            /* resize copy buffer if necessary */
            if (copy_index==copy_packets) {
                copy_packets += COPY_CHUNK_SIZE;
                copy_buffer = (unsigned char *) rvalloc(copy_buffer, copy_packets*188, 0);
            }

            /* copy ts packet to copy buffer */
            memcpy(&copy_buffer[copy_index*188], packet, 188);
            copy_index++;
        }

        /* tidy up */
        freebits(pb);
    }

    /* display per-pid statistics */
    if (verbose>=1) {
        rvmessage("+------+----------+-------+----------+");
        rvmessage("|  pid |  packets | ratio |  bitrate +");
        rvmessage("+------+----------+-------+----------+");
        for (i=0; i<8192; i++)
            if (pid_count[i]) {
                if (last_pcr[i]>first_pcr[i]) {
                    /* pid has a timebase */
                    double bitrate = pid_count[i] * 188.0*8.0;
                    bitrate *= 27.0;
                    bitrate /= pcr_wraparound[i]*MAX_PCR_VALUE+last_pcr[i]-first_pcr[i];
                    rvmessage("| %4d | %8d | %4.1f%% | %4.1fMbps |", i, pid_count[i], 100.0*pid_count[i]/copyno, bitrate);
                } else
                    rvmessage("| %4d | %8d | %4.1f%% |          |", i, pid_count[i], 100.0*pid_count[i]/copyno);
            }
        rvmessage("+------+----------+-------+----------+");
    }

    if (verbose>=0)
        rvmessage("%s: copied %d out of %d packets", filename, copyno, packno);

    /* tidy up */
    rvfree(seq);

    return;
}

int main(int argc, char *argv[])
{
    FILE *fileout = stdout;
    const char *filename[RV_MAXFILES] = {0};
    char *outfile = NULL;
    int fileindex = 0;
    int numfiles = 0;

    /* command line defaults */
    char *forcetype = NULL;
    char *algotype = NULL;
    int algo = ALGO_FIRST_SPS|ALGO_FIRST_PUSI;
    int checkpid = -1;
    int verbose = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"type",     1, NULL, 't'},
            {"pid",      1, NULL, 'p'},
            {"output",   1, NULL, 'o'},
            {"quiet",    0, NULL, 'q'},
            {"verbose",  0, NULL, 'v'},
            {"usage",    0, NULL, 'h'},
            {"help",     0, NULL, 'h'},
            {NULL,       0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "t:a:p:o:qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 't':
                forcetype = optarg;
                break;

            case 'a':
                algotype = optarg;
                break;

            case 'p':
                checkpid = atoi(optarg);
                if (checkpid==0 || checkpid>8191)
                    rvexit("invalid pid: %s", optarg);
                break;

            case 'o':
                outfile = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < sizeof(filename)/sizeof(filename[0]))
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    /* use filein if no input filenames */
    if (numfiles==0)
        filename[0] = "-";

    /* open output file */
    if (outfile)
        fileout = fopen(outfile, "wb");
    if (fileout==NULL)
        rverror("failed to open output file \"%s\"", outfile);

    /* loop over input files */
    int status = 0;
    do {

        /* prepare to parse file */
        struct bitbuf *bb = initbits_filename(filename[fileindex], B_GETBITS);
        if (bb==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex]);
        bb->outfile = fileout;

        /* determine filetype */
        divine_t divine = divine_filetype(bb, filename[fileindex], forcetype, verbose);
        if (rewindbits(bb)<0)
            rverror("searched too far on non-seekable input file \"%s\"", filename[fileindex]);

        /* select what to do based on filetype */
        switch (divine.filetype) {
            case M2V:
                status = clean_m2v(bb, filename[fileindex], verbose);
                break;

            case H264:
                status = clean_264(bb, filename[fileindex], verbose);
                break;

            case HEVC:
                status = clean_hevc(bb, filename[fileindex], verbose);
                break;

            case TS:
                if (algotype) {
                    algo = 0;
                    /* parse algorithm type(s) */
                    if (strstr(algotype, "firstsps"))
                        algo |= ALGO_FIRST_SPS;
                    if (strstr(algotype, "firstpusi"))
                        algo |= ALGO_FIRST_PUSI;
                    if (strstr(algotype, "pcrreset"))
                        algo |= ALGO_PCR_RESET;
                    if (algo==0)
                        rvexit("unknown algorithm list: %s", algotype);
                }
                clean_ts(bb, filename[fileindex], fileout, checkpid, algo, verbose);
                break;

            case EMPTY:
                rvexit("empty file: \"%s\"", filename[fileindex]);
                break;

            default:
                rvmessage("filetype of file \"%s\" is not supported: %s", filename[fileindex], filetypename[divine.filetype]);
                break;
        }

        /* tidy up */
        freebits(bb);

    } while (++fileindex<numfiles);

    /* tidy up */

    return status;
}
