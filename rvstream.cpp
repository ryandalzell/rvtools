/*
 * Description: Send or receive video data on a network.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-09-24 15:04:44 $
 * Revision   : $Revision: 1.8 $
 * Copyright  : (c) 2009 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>
#include <errno.h>
#include <fcntl.h>
#include "getopt.h"
#include <sys/time.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <pthread.h>
#include <queue>
#include <map>

#include "rvutil.h"
#include "rvmp2.h"

using std::queue;
using std::map;

#define MAX_DATAGRAM_SIZE (8*1024)
#define MAX_CAPTURES 4

const char *appname;

/* per-pid data struct */
struct perpid_t {
    perpid_t() {
        count = discontinuity = 0;
        continuity = -1;
        first_pcr = last_pcr = -1ll;
        pcr_wraparound = first_pcr_packet = last_pcr_packet = 0;
    };

    unsigned int count;
    // continuity.
    char continuity;
    unsigned int discontinuity;
    // pcr.
    long long first_pcr;
    long long last_pcr;
    int pcr_wraparound;
    int first_pcr_packet;
    int last_pcr_packet;
};

/* data buffer pool struct */
struct pool_t {
    unsigned char data[MAX_DATAGRAM_SIZE];
    ssize_t size;
};

void usage(int exitcode)
{
    fprintf(stderr, "%s: send or receive video data on a network\n", appname);
    fprintf(stderr, "usage: %s [options] [<file>] [<file>...]\n", appname);
    fprintf(stderr, "  -s, --send          : send data (default: receive)\n");
    fprintf(stderr, "  -a, --address       : ip address to send to or listen from (default: any)\n");
    fprintf(stderr, "  -p, --port          : port number to send to or listen from (default: 1234)\n");
    fprintf(stderr, "  -i, --interface     : interface name or address to use for multicast (default: any)\n");
    fprintf(stderr, "  -b, -n, --bytes     : amount of data in bytes to capture or send, can use suffixes K,M,G,Ki,Mi,Gi (default: all)\n");
    fprintf(stderr, "  -t, --time          : length of time in seconds to capture or send, can use suffixes m,h,d (default: datasize)\n");
    fprintf(stderr, "  -l, --loop          : loop over input file when sending (default: stop at end of file)\n");
    fprintf(stderr, "  -o, --output        : write output to file (default: stdout)\n");
    fprintf(stderr, "  -q, --quiet         : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "  -v, --verbose       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "  --                  : disable argument processing\n");
    fprintf(stderr, "  -h, --help, --usage : print this usage message\n");
    exit(exitcode);
}

/* chose high precision clock type globally */;
static const clockid_t clk_id = CLOCK_REALTIME;

long long get_time_us_old()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    long long time = 1000000ll * (long long)tv.tv_sec;
    time += (long long)tv.tv_usec;
    return time;
}

long long get_time_us()
{
    struct timespec tv;
    clock_gettime(clk_id, &tv);
    long long time = 1000000ll * (long long)tv.tv_sec;
    time += (long long)tv.tv_nsec/1000ll;
    return time;
}

long long get_time_ns()
{
    struct timespec tv;
    clock_gettime(clk_id, &tv);
    long long time = 1000000000ll * (long long)tv.tv_sec;
    time += (long long)tv.tv_nsec;
    return time;
}

struct timespec ts_init(long long ticks, long long ticks_per_sec)
{
    struct timespec b;
    b.tv_sec = (time_t) (ticks / ticks_per_sec);
    b.tv_nsec = (long) ((ticks % ticks_per_sec) * 1000000000ll / ticks_per_sec);
    return b;
}

double ts_double(const struct timespec &a)
{
    double b = a.tv_sec;
    b += a.tv_nsec/1e9;
    return b;
}

struct timespec ts_add(const struct timespec &a, const struct timespec &b)
{
    struct timespec c;
    c.tv_sec = a.tv_sec + b.tv_sec;
    c.tv_nsec = a.tv_nsec + b.tv_nsec;
    while (c.tv_nsec>999999999) {
        c.tv_sec++;
        c.tv_nsec -= 1000000000l;
    }
    return c;
}

struct timespec ts_sub(const struct timespec &a, const struct timespec &b)
{
    struct timespec c;
    c.tv_sec = a.tv_sec - b.tv_sec;
    c.tv_nsec = a.tv_nsec - b.tv_nsec;
    while (c.tv_nsec<0) {
        c.tv_sec--;
        c.tv_nsec += 1000000000l;
    }
    return c;
}

int ts_gt(const struct timespec &a, const struct timespec &b)
{
    if (a.tv_sec > b.tv_sec)
        return 1;
    if (a.tv_sec < b.tv_sec)
        return 0;
    return a.tv_nsec > b.tv_nsec;
}

int multicast_address(const char *address)
{
    if (address)
        if (strtol(address, NULL, 10)>=224 && strtol(address, NULL, 10)<=239)
            return 1;
    return 0;
}

int capture(const char *address, int port, const char *interface, long long numbytes, int numsecs, int verbose, FILE *fileout)
{
    long long totbytes = 0;
    int totsecs = 0;

    /* performance metrics */
    long long starttime = 0;
    int packnum = 0;

    /* data buffer pool */
    queue<struct pool_t *> pool, fifo;

    /* per-pid data */
    map<int, perpid_t> pids;

    /* initialise bitrate measure */
    int pcr_pid = 0;

    /* set non-blocking write on output file */
    if (fcntl(fileno(fileout), F_SETFL, O_NONBLOCK | fcntl(fileno(fileout), F_GETFL, 0))<0)
        rverror("failed to set O_NONBLOCK on output");

    /* create a udp socket */
    int sock = socket(PF_INET, SOCK_DGRAM, 0);
    if (sock<0)
        rverror("failed to create socket");

    /* assign a name to the socket */
    struct sockaddr_in sa_addr;
    memset(&sa_addr, 0, sizeof(sa_addr));
    sa_addr.sin_family = AF_INET;
    sa_addr.sin_port = htons(port);
    sa_addr.sin_addr.s_addr = multicast_address(address)? inet_addr(address) : htonl(INADDR_ANY);
    if (bind(sock, (struct sockaddr *)&sa_addr, sizeof(sa_addr)))
        rverror("failed to bind to socket: %s", address);

    /* join a multicast group */
    struct ip_mreqn mc_req;
    memset(&mc_req, 0, sizeof(mc_req));
    if (multicast_address(address)) {
        /* construct an IGMP join request structure */
        mc_req.imr_multiaddr.s_addr = inet_addr(address);
        mc_req.imr_address.s_addr = inet_addr(interface); //htonl(INADDR_ANY);
        if (mc_req.imr_address.s_addr==INADDR_NONE) {
            /* try looking up as interface name instead */
            struct ifreq ifr;
            ifr.ifr_addr.sa_family = AF_INET;
            strncpy(ifr.ifr_name, interface, IFNAMSIZ-1);
            if (ioctl(sock, SIOCGIFADDR, &ifr)<0)
                rverror("failed to get interface address: %s", interface);
            struct sockaddr_in *so = (struct sockaddr_in *)&ifr.ifr_addr;
            mc_req.imr_address.s_addr = so->sin_addr.s_addr;
        }

        /* send an ADD MEMBERSHIP message via setsockopt */
        if ((setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, (void*) &mc_req, sizeof(mc_req))) < 0)
            rverror("failed to send add membership message");
    }

    /* receive loop */
    long long start = get_time_us(), last = start;
    do {
        struct sockaddr_in sa_from;
        socklen_t from_size = sizeof(sa_from);

        /* fetch data buffers from the pool, or create a new one */
        if (pool.empty())
            /* the pool reuses buffers so they don't have to be allocate on every loop */
            pool.push(new pool_t);
        struct pool_t *p = pool.front();

        /* read from socket */
        int read = recvfrom(sock, p->data, MAX_DATAGRAM_SIZE, 0, (struct sockaddr *)&sa_from, &from_size);
        if (read<0)
            rverror("failed to receive from socket");
        else if (read%188!=0)
            rvmessage("unusual: possibly not a transport stream: read %d bytes", read);
        else if (read==MAX_DATAGRAM_SIZE)
            rverror("need to increase MAX_DATAGRAM_SIZE: %d", MAX_DATAGRAM_SIZE);

        if (totbytes==0) {
            /* start measuring receive rate */
            starttime = get_time_us();
            if (verbose>=0)
                rvmessage("%s data capture started from address %s", multicast_address(address)? "multicast" : "unicast", address);
        }
        if (verbose>=2)
            rvmessage("received packet from %s:%d length=%d bytes data[0]=0x%02x", inet_ntoa(sa_from.sin_addr), ntohs(sa_from.sin_port), read, p->data[0]);

        /* prepare to parse transport stream datagram */
        struct bitbuf *bb = initbits_memory(p->data, read);

        /* parse transport stream datagram for transport stream packets */
        while (showbits(bb, 8)==0x47) {
            /* parse transport packet header */
            int transport_error_indicator, payload_unit_start_indicator, pid, adaptation_field_control, continuity_counter, discontinuity_indicator, pcr_flag;
            long long pcr;
            int header = parse_transport_packet_header(bb, verbose, packnum, &transport_error_indicator,
                                                       &payload_unit_start_indicator, &pid, &adaptation_field_control, &continuity_counter,
                                                       &discontinuity_indicator, &pcr_flag, &pcr);
            flushbits(bb, 8*(188-header));

            /* count pid frequency */
            pids[pid].count++;

            /* check continuity_counter */
            if (pid>=0 && pid<0x1FFF) {
                if (pids[pid].continuity>=0) {
                    int next_continuity = adaptation_field_control==0 || adaptation_field_control==2? pids[pid].continuity : (pids[pid].continuity+1)%16;
                    if (next_continuity != continuity_counter) {
                        if (discontinuity_indicator) {
                            if (verbose>=1)
                                rvmessage("%s: packet %5d: discontinuity has been indicated (continuity goes from %d to %d)", address, packnum, pids[pid].continuity, continuity_counter);
                        } else {
                            if (verbose>=0)
                                rvmessage("%s: packet %5d: continuity error in pid %d, expected %d, packet has %d", address, packnum, pid, next_continuity, continuity_counter);
                            pids[pid].discontinuity++;
                        }
                    }
                }
                pids[pid].continuity = continuity_counter;
            }

            /* measure bitrate for each pid */
            if (pcr_flag) {
                if (pcr_pid==0 && pcr!=0)
                    pcr_pid = pid; /* use this pid to measure bitrate */
                /* find first and last pcrs in bitstream */
                if (pids[pid].first_pcr<0) {
                    pids[pid].first_pcr = pcr;
                    pids[pid].first_pcr_packet = packnum;
                } else {
                    if (pcr<pids[pid].last_pcr)
                        pids[pid].pcr_wraparound++;
                    pids[pid].last_pcr = pcr;
                    pids[pid].last_pcr_packet = packnum;
                }
            }
            packnum++;
        }
        freebits(bb);

        /* remove buffer from pool */
        p->size = read;
        fifo.push(p);
        pool.pop();

        /* check for buffers to output */
        p = fifo.front();
        unsigned char *buf = p->data;
        ssize_t len = p->size;

        /* write to output */
        if (len==0)
            break; /* orderly shutdown of socket */
        int write = fwrite(buf, len, 1, fileout);
        if (write!=1)
            break;

        /* return buffer to pool */
        if (write==1) {
            pool.push(fifo.front());
            fifo.pop();
        }

        totbytes += read;

        /* real-time receive bitrate */
        long long now = get_time_us();
        if (verbose>=-1) {
            char time[16] = "";
            describe_duration(time, sizeof(time), (now-starttime)/1000000.0);
            if (now-last>=1000000) {
                if (numbytes==0 && numsecs==0)
                    rvstatus("%s: wrote %.1f MB in %s (%.1fMbps)", address, totbytes/1000000.0, time, (double)totbytes*8.0/(double)(now-starttime));
                else if (numsecs>0)
                    rvstatus("%s: wrote %.1f MB in %s (%d%%) (%.1fMbps)", address, totbytes/1000000.0,
                             time, 100*totsecs/numsecs, (double)totbytes*8.0/(double)(now-starttime));
                else
                    rvstatus("%s: wrote %.1f MB (%lld%%) in %s (%.1fMbps)", address, totbytes/1000000.0, 100*totbytes/numbytes,
                             time, (double)totbytes*8.0/(double)(now-starttime));
                last = now;
            }
        }

        /* measure catpure time */
        totsecs = (int) (now - start)/1000000;

    } while ((totbytes<numbytes || numbytes<0) && (totsecs<numsecs || totsecs<0));
    if (ferror(fileout) && errno!=EPIPE && errno!=EAGAIN)
        rverror("failed to write to output");

    /* leave multicast group */
    if (multicast_address(address)) {
        /* send a DROP MEMBERSHIP message via setsockopt */
        if ((setsockopt(sock, IPPROTO_IP, IP_DROP_MEMBERSHIP, (void*) &mc_req, sizeof(mc_req))) < 0)
            rverror("failed to send drop membership message");
    }

    /* reset blocking write on output file */
    if (fcntl(fileno(fileout), F_SETFL, ~O_NONBLOCK & fcntl(fileno(fileout), F_GETFL, 0))<0)
        rverror("failed to set O_NONBLOCK on output");

    /* output remaining buffers */
    while (!fifo.empty()) {
        struct pool_t *p = fifo.front();
        unsigned char *buf = p->data;
        ssize_t len = p->size;
        if (len==0)
            break; /* orderly shutdown of socket */
        int write = fwrite(buf, len, 1, fileout);
        if (write!=1)
            break;
        else
            pool.push(fifo.front());
        fifo.pop();
    }
    if (ferror(fileout) && errno!=EPIPE && errno!=EAGAIN)
        rverror("failed to write to output");

    if (verbose>=0) {
        long long endtime = get_time_us();
        char time[16] = "";
        describe_duration(time, sizeof(time), (endtime-starttime)/1000000.0);
        rvmessage("%s: wrote %.1f MB and %d packets in %s (%.1fMbps)", address, totbytes/1000000.0, packnum, time, (double)totbytes*8.0/(double)(endtime-starttime));

        /* display list of unique pids */
        char string[1024];
        int n = 0;
        int num_unique_pids = 0;
        //for (auto const &it : pids) {
        map<int, perpid_t>::const_iterator it;
        for (it = pids.begin(); it != pids.end(); ++it) {
            int pid = it->first;
            const perpid_t &p = it->second;
            if (p.count) {
                n += snprintf(string+n, sizeof(string)-n, "%d ", pid);
                num_unique_pids++;
            }
        }
        rvmessage("%d unique pids: %s", num_unique_pids, string);

        /* calculate average bitrate
            *            long long bitrate = 1;
            *            if (pcr_pid>0 && first_pcr[pcr_pid]>=0 && last_pcr[pcr_pid]>=0 && last_pcr[pcr_pid]!=first_pcr[pcr_pid]) {
            *                bitrate = (last_pcr_packet[pcr_pid]-first_pcr_packet[pcr_pid]) * 188ll*8ll;
            *                bitrate *= 27000000ll;
            *                bitrate /= pcr_wraparound[pcr_pid]*MAX_PCR_VALUE+last_pcr[pcr_pid]-first_pcr[pcr_pid];
            } */

        /* display per-pid statistics */
        rvmessage("+------+----------+-------+----------+----------+");
        rvmessage("|  pid |  packets | ratio |  discon  |  bitrate +");
        rvmessage("+------+----------+-------+----------+----------+");
        //for (auto const &it : pids) {
        for (it = pids.begin(); it != pids.end(); ++it) {
            int pid = it->first;
            const perpid_t &p = it->second;
            if (p.count) {
                if (p.last_pcr>p.first_pcr) {
                    /* pid has a timebase */
                    double bitrate = p.count * 188.0*8.0;
                    bitrate *= 27.0;
                    bitrate /= p.pcr_wraparound*MAX_PCR_VALUE+p.last_pcr-p.first_pcr;
                    rvmessage("| %4d | %8u | %4.1f%% | %8u | %4.1fMbps |", pid, p.count, 100.0*p.count/packnum, p.discontinuity, bitrate);
                } else
                    rvmessage("| %4d | %8u | %4.1f%% | %8u |          |", pid, p.count, 100.0*p.count/packnum, p.discontinuity);
            }
        }
        rvmessage("+------+----------+-------+----------+----------+");
    }

    /* tidy up */
    while (!pool.empty()) {
        delete pool.front();
        pool.pop();
    }

    return totbytes;
}

struct capture_args {
    const char *address;
    int port;
    const char *interface;
    long long numbytes;
    int numsecs;
    int verbose;
    FILE *fileout;
};

void *capture_thread(void *a)
{
    struct capture_args *args = (struct capture_args *)a;
    capture(args->address, args->port, args->interface, args->numbytes, args->numsecs, args->verbose, args->fileout);
    return NULL;
}

int main(int argc, char *argv[])
{
    int i;
    FILE *filein = stdin;
    FILE *fileout[MAX_CAPTURES] = {stdout, stdout, stdout, stdout};
    char *filename[RV_MAXFILES] = {0};
    char *outfile[MAX_CAPTURES] = {NULL, NULL, NULL, NULL};
    int fileindex = 0;
    int numfiles = 0;
    int numoutfiles = 0;

    /* command line defaults */
    int numaddresses = 0;
    const char *address[MAX_CAPTURES] = {"localhost", NULL, NULL, NULL};
    int numports = 0;
    int port[MAX_CAPTURES] = {1234, 1234, 1234, 1234};
    int send = 0;
    const char *interface = "0.0.0.0";
    long long numbytes = -1;
    int numsecs = -1;
    int looped = 0;
    int verbose = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"send",       0, NULL, 's'},
            {"address",    1, NULL, 'a'},
            {"port",       1, NULL, 'p'},
            {"interface",  1, NULL, 'i'},
            {"bytes",      1, NULL, 'b'},
            {"time",       1, NULL, 't'},
            {"loop",       0, NULL, 'l'},
            {"output",     1, NULL, 'o'},
            {"quiet",      0, NULL, 'q'},
            {"verbose",    0, NULL, 'v'},
            {"usage",      0, NULL, 'h'},
            {"help",       0, NULL, 'h'},
            {NULL,         0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "sa:p:i:b:t:n:lo:qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 's':
                send = 1;
                break;

            case 'a':
                address[numaddresses++] = optarg;
                break;

            case 'p':
                port[numports] = atoi(optarg);
                if (port[numports]<=0 || port[numports]>65535)
                    rvexit("invalid value for port number: %d", port[numports]);
                if (port[numports]<1024 && geteuid()!=0)
                    rvexit("can't use privledged port number: %d", port[numports]);
                numports++;
                break;

            case 'i':
                interface = optarg;
                break;

            case 'b':
            case 'n':
            {
                const char *arg = optarg;
                numbytes = atoll(optarg);
                rvupper(optarg);
                if (strstr(optarg, "KI"))
                    numbytes <<= 10;
                else if (strstr(optarg, "MI"))
                    numbytes <<= 20;
                else if (strstr(optarg, "GI"))
                    numbytes <<= 30;
                else if (strchr(optarg, 'K'))
                    numbytes *= 1000;
                else if (strchr(optarg, 'M'))
                    numbytes *= 1000000;
                else if (strchr(optarg, 'G'))
                    numbytes *= 1000000000;
                else if (strncmp(optarg, "inf", 3)==0)
                    numbytes = -1;
                else if (strncmp(optarg, "INF", 3)==0)
                    numbytes = -1;
                if (numbytes==0)
                    rvexit("invalid value for amount of data: %s (%lld)", arg, numbytes);
                break;
            }

            case 't':
            {
                const char *arg = optarg;
                numsecs = atoll(optarg);
                rvupper(optarg);
                if (strstr(optarg, "S"))
                    ;
                else if (strstr(optarg, "M"))
                    numsecs *= 60;
                else if (strstr(optarg, "H"))
                    numsecs *= 60*60;
                else if (strstr(optarg, "D"))
                    numsecs *= 60*60*24;
                else if (strncmp(optarg, "inf", 3)==0)
                    numsecs = -1;
                else if (strncmp(optarg, "INF", 3)==0)
                    numsecs = -1;
                if (numsecs==0)
                    rvexit("invalid value for length of time to capture: %s (%lld)", arg, numbytes);
                break;
            }

            case 'l':
                looped = 1;
                break;

            case 'o':
                outfile[numoutfiles++] = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < sizeof(filename)/sizeof(filename[0]))
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    /* sanity check command line */
    if (send && !address[0])
        rvexit("need to supply a destination address");

    /* send or receive */
    long long totbytes = 0;
    if (send) {

        if (numsecs!=-1)
            rvexit("time restricted sending not supported yet");

        /* create a udp socket */
        int sock = socket(PF_INET, SOCK_DGRAM, 0);
        if (sock<0)
            rverror("failed to create socket");

        /* assign a name to the socket */
        struct sockaddr_in sa_addr;
        memset(&sa_addr, 0, sizeof(sa_addr));
        sa_addr.sin_family = AF_INET;
        sa_addr.sin_port = htons(port[0]);
        sa_addr.sin_addr.s_addr = htonl(INADDR_ANY);
        if (address[0]) {
            if (inet_aton(address[0], &sa_addr.sin_addr)==0)
                rverror("failed to translate address %s", address[0]);
        }

        /* select interface to send multicast from */
        if (multicast_address(address[0])) {
            struct in_addr interface_addr;
            interface_addr.s_addr = inet_addr(interface);
            if (interface_addr.s_addr==INADDR_NONE) {
                /* try looking up as interface name instead */
                struct ifreq ifr;
                ifr.ifr_addr.sa_family = AF_INET;
                strncpy(ifr.ifr_name, interface, IFNAMSIZ-1);
                if (ioctl(sock, SIOCGIFADDR, &ifr)<0)
                    rverror("failed to get interface address: %s", interface);
                struct sockaddr_in *so = (struct sockaddr_in *)&ifr.ifr_addr;
                interface_addr.s_addr = so->sin_addr.s_addr;
            }
            setsockopt (sock, IPPROTO_IP, IP_MULTICAST_IF, &interface_addr, sizeof(interface_addr));
        }

        /* loop over input files */
        do {
            if (numfiles)
                filein = fopen(filename[fileindex], "rb");
            if (filein==NULL)
                rverror("failed to open file \"%s\"", filename[fileindex]);

            /* prepare to parse file */
            struct bitbuf *bb = initbits_filename(filename[fileindex], B_GETBITS);
            if (bb==NULL)
                rverror("failed to open file \"%s\"", filename[fileindex]);

            /* determine filetype */
            divine_t divine = divine_filetype(bb, filename[fileindex], NULL, verbose);
            if (rewindbits(bb)<0)
                rverror("searched too far on non-seekable input file \"%s\"", filename[fileindex]);

            /* start measuring send rate*/
            long long starttime = get_time_us();
            long long sendbytes = 0;

            /* select what to do based on filetype */
            switch (divine.filetype) {
                case TS:
                {
                    const int pack_per_frame = 7;

                    /* allocate network frame buffer */
                    size_t framesize = pack_per_frame*188;
                    unsigned char *frame = (unsigned char *)rvalloc(NULL, framesize, 0);

                    /* initialise timing */
                    long long pcr_start_packet = 0;
                    long long pcr_latest_packet = 0;
                    struct timespec start;
                    clock_gettime(clk_id, &start);
                    struct timespec displaytime = {1, 0};

                    /* loop over input file */
                    long long packno = 0, loopno = 0;
                    do {
                        /* send loop */
                        long long loopbytes = 0;

                        /* initialise loop timing */
                        long long pcr_start = -1;
                        long long pcr_latest = -1;
                        struct timespec loopstart;
                        clock_gettime(clk_id, &loopstart);

                        while (!eofbits(bb) && !errorbits(bb) && (loopbytes<numbytes || numbytes<0)) {
                            /* construct next frame */
                            for (i=0; i<pack_per_frame; i++) {
                                unsigned char *packet = frame+i*188;

                                /* read the next complete transport stream packet */
                                if (find_next_sync_code(bb)<0)
                                    break;
                                read_transport_packet(bb, verbose, packet);
                                packno++;

                                /* prepare to parse transport stream packet */
                                struct bitbuf *pb = initbits_memory(packet, 188);

                                /* parse transport packet */
                                int transport_error_indicator, payload_unit_start_indicator;
                                int pid, adaptation_field_control, continuity_counter, pcr_flag;
                                int discontinuity_indicator;
                                long long pcr;
                                int header_length = parse_transport_packet_header(pb, verbose, packno, &transport_error_indicator,
                                                &payload_unit_start_indicator, &pid, &adaptation_field_control, &continuity_counter,
                                                &discontinuity_indicator, &pcr_flag, &pcr);
                                if (header_length<0) {
                                    rvmessage("%s: packet %5lld: failed to parse transport packet header", filename[fileindex], packno);
                                    break;
                                }

                                /* look for a pcr to synchronise against */
                                if (pcr_flag) {
                                    if (pcr_start<0) {
                                        pcr_start = pcr;
                                        pcr_start_packet = packno;
                                    } else {
                                        pcr_latest = pcr;
                                        pcr_latest_packet = packno;
                                    }
                                }

                                /* tidy up */
                                freebits(pb);
                            }

                            /* begin synchronisation between time in bitstream
                             * and real clock time of sending packets */
                            struct timespec now;
                            clock_gettime(clk_id, &now);

                            /* smooth packet sending (except at start) */
                            if (pcr_latest>pcr_start) {
                                struct timespec streamtime = ts_init(packno*(pcr_latest-pcr_start)/27ll/(pcr_latest_packet-pcr_start_packet), 1000000ll);
                                struct timespec walltime = ts_sub(now, start);
                                if (ts_gt(streamtime, walltime)) {
                                    struct timespec tv = ts_sub(streamtime, walltime);
                                    //rvmessage("%ld.%09ld - %ld.%09ld = %ld.%09ld", streamtime.tv_sec, streamtime.tv_nsec, walltime.tv_sec, walltime.tv_nsec, tv.tv_sec, tv.tv_nsec);
                                    nanosleep(&tv, NULL);
                                }
                            }

                            /* burst packet sending between each pcr
                            if (pcr_latest>pcr_start) {
                                long long streamtime = (pcr_latest-pcr_start)*1000ll/27ll;
                                long long walltime = now-start;
                                if (streamtime>walltime) {
                                    unsigned int nsecs = streamtime - walltime;
                                    nanosleep(nsecs);
                                }
                            } */

                            /* send frame */
                            int sent = sendto(sock, frame, framesize, 0, (struct sockaddr *)&sa_addr, sizeof(sa_addr));
                            if (sent<0)
                                rverror("failed to send packet");
                            if (sent!=framesize)
                                rvmessage("failed to send whole packet");
                            loopbytes += sent;

                            /* display approx timings */
                            if (verbose>=0) {
                                struct timespec runtime = ts_sub(now, start);
                                if (ts_gt(runtime, displaytime)) {
                                    struct timespec looptime = ts_sub(now, loopstart);
                                    double rt = ts_double(runtime);
                                    double lt = ts_double(looptime);
                                    rvstatus("%7.2fs: streamtime=%7.2fs loops=%3lld bitrate=%7.2fMbps", rt, packno*(pcr_latest-pcr_start)/(pcr_latest_packet-pcr_start_packet)/27e6, loopno, (double)loopbytes*8.0/lt/1e6);
                                    //rvstatus("%7.2fs: streamtime=%7.2fs bitrate=%7.2fMbps", rt, lt, (double)loopbytes*8.0/lt/1e6);
                                    displaytime.tv_sec++;
                                }
                            }
                        }
                        if (errorbits(bb))
                            rvmessage("%s: %s", filename[fileindex], strerror(errno));

                        /* handle looped mode */
                        if (looped)
                            rewindbits(bb);

                        sendbytes += loopbytes;
                        loopno++;
                    } while (looped);

                    if (verbose>=0) {
                        long long endtime = get_time_us();
                        char time[16] = "";
                        describe_duration(time, sizeof(time), (endtime-starttime)/1000000.0);
                        rvmessage("sent %lld bytes and %lld packets in %s (%.1fMbps)", sendbytes, packno, time, (double)sendbytes*8.0/(double)(endtime-starttime));
                    }

                    /* tidy up */
                    rvfree(frame);

                    break;
                }

                case EMPTY:
                    rvexit("empty file: \"%s\"", filename[fileindex]);
                    break;

                default:
                    rvmessage("filetype of file \"%s\" is not supported: %s", filename[fileindex], filetypename[divine.filetype]);
                    break;
            }
            if (ferror(filein))
                rverror("failed to read from input");

            totbytes += sendbytes;

            /* tidy up */
            if (numfiles)
                fclose(filein);
            freebits(bb);

        } while (++fileindex<numfiles);

        if (verbose>=0 && numfiles>1)
            rvmessage("sent a total of %lld bytes over %d input files", totbytes, numfiles);

    } else {
        /* receive */

        pthread_t thread[MAX_CAPTURES];
        struct capture_args args[MAX_CAPTURES];

        /* ignore SIGPIPE on output file */
        if (signal(SIGPIPE, SIG_IGN)==SIG_ERR)
            rverror("failed to set ignore on SIGPIPE");

        /* launch each capture thread */
        for (i=0; i==0 || i<numaddresses; i++) {

            /* sanity check */
            if (address[i]==NULL)
                rvmessage("no address specified for capture no. %d to file \"%s\"", i, outfile[i]);

            /* open output file */
            if (outfile[i])
                fileout[i] = fopen(outfile[i], "wb");
            if (fileout[i]==NULL)
                rverror("failed to open output file \"%s\"", outfile[i]);

            /* run capture in a thread */
            args[i].address = address[i];
            args[i].port = numports>=i+1? port[i] : port[0];
            args[i].interface = interface;
            args[i].numbytes = numbytes;
            args[i].numsecs = numsecs;
            args[i].verbose = verbose;
            args[i].fileout = fileout[i];

            pthread_create(&thread[i], NULL, capture_thread, (void *)&args[i]);
        }

        /* wait for each capture thread to finish */
        for (i=0; i==0 || i<numaddresses; i++)
            pthread_join(thread[i], NULL);

        /* tidy up */
        for (i=0; i==0 || i<numaddresses; i++)
            if (outfile[i])
                fclose(fileout[i]);
    }

    return 0;
}
