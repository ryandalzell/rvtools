/*
 * Description: Base class of a raw or decoded yuv image.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-10-01 16:00:14 $
 * Revision   : $Revision: 1.27 $
 * Copyright  : (c) 2005,2015 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "rvutil.h"
#include "rvimage.h"
#include "rvh264.h"
#include "rvy4m.h"

/*
 * rvimage class
 */

rvimage::rvimage()
{
    init();
}

rvimage::rvimage(int width, int pitch, int height, int hsub, int vsub, int bpp)
{
    init();
    init(width, pitch, height, hsub, vsub, bpp);
}

rvimage::rvimage(int width, int pitch, int height, fourcc_t fourcc)
{
    init();
    init(width, pitch, height,fourcc);
}

/* copy constructor */
rvimage::rvimage(const struct rvimage &image)
{
    init();
    init(image.width, image.pitch, image.height, image.hsub, image.vsub, image.bpp);
}

rvimage::~rvimage()
{
    rvfree(buf);
}

/*
 * memory management
 */

void rvimage::init()
{
    plane[0] = plane[1] = plane[2] = buf = NULL;
    width = pitch = height = 0;

    /* default image resampling parameters */
    algo = BLACKMAN;
    phases = taps = 8;
}

void rvimage::init(unsigned char *base, int w, int p, int h, int hs, int vs, int b)
{
    const int pelsize = (b+7)/8;
    const int planesize = p*h*pelsize;

    /* fill in rvimage details */
    plane[0] = base;
    if (hs && vs) {
        plane[1] = base+planesize;
        plane[2] = base+planesize+planesize/hs/vs;
    } else
        plane[1] = plane[2] = NULL;
    width = w;
    pitch = p;
    height = h;
    hsub = hs;
    vsub = vs;
    bpp = b;
    size = get_framesize(pitch, height, hsub, vsub, bpp);
}

void rvimage::init(int width, int pitch, int height, int hsub, int vsub, int bpp)
{
    /* free old rvimage data, if any */
    rvfree(buf);

    /* allocate rvimage data */
    size_t framesize = get_framesize(pitch, height, hsub, vsub, bpp);
    buf = (unsigned char *)rvalloc(NULL, framesize*sizeof(unsigned char), 0);

    /* prepare rvimage structure */
    init(buf, width, pitch, height, hsub, vsub, bpp);
}

void rvimage::init(int width, int pitch, int height, fourcc_t fourcc)
{
    /* free old rvimage data, if any */
    rvfree(buf);

    /* allocate rvimage data */
    size_t framesize = get_framesize(pitch, height, fourcc);
    buf = (unsigned char *)rvalloc(NULL, framesize*sizeof(unsigned char), 0);

    /* prepare rvimage structure */
    int hsub, vsub;
    get_chromasub(fourcc, &hsub, &vsub);
    init(buf, width, pitch, height, hsub, vsub, get_bitsperpel(fourcc));

    /* override size for packed formats TODO packed formats need better support */
    size = framesize;
}

void rvimage::init(const struct rvimage &image)
{
    /* call basic method */
    init(image.width, image.pitch, image.height, image.hsub, image.vsub, image.bpp);
}

/*
 * file io
 */

/* returns number of images read (0 or 1) */
size_t rvimage::fread(FILE *file, bool yuv4mpeg)
{
    /* read yuv4mpeg frame header */
    if (yuv4mpeg) {
        int read = read_y4m_frame_header(file);
        if (read<0)
            rverror("failed to read frame header from input");
        else if (read==0)
            return 0;
    }

    return ::fread(plane[0], size, 1, file);
}

/* returns number of images written (0 or 1) */
size_t rvimage::fwrite(FILE *file, bool yuv4mpeg)
{
    /* write yuv4mpeg frame header */
    if (yuv4mpeg)
        if (write_y4m_frame_header(file)<0)
            rverror("failed to write frame header to output");

    return ::fwrite(plane[0], size, 1, file);
}

/* returns number of images read (0 or 1) */
size_t rvimage::readbits(struct bitbuf *bb, bool yuv4mpeg)
{
    /* read yuv4mpeg frame header */
    if (yuv4mpeg)
        if (read_y4m_frame_header_bits(bb)<0)
            rverror("failed to read frame header from input");

    return ::readbits(bb, plane[0], size);
}

/* returns number of images written (0 or 1) */
size_t rvimage::writebits(struct bitbuf *bb, bool yuv4mpeg)
{
    /* write yuv4mpeg frame header */
    if (yuv4mpeg)
        if (write_y4m_frame_header_bits(bb)<0)
            rverror("failed to write frame header to output");

    for (size_t i=0; i<size; i++)
        ::putbits(bb, plane[0][i], 8);

    return ::errorbits(bb);
}

/*
 * image drawing
 */

void rvimage::clear()
{
    const int pelsize = (bpp+7)/8;
    const size_t framesize = pitch*height*pelsize;
    memset(plane[0], 0x00, framesize);
    clear_chroma();
}

void rvimage::clear_chroma()
{
    if (hsub && vsub)  {
        const int pelsize = (bpp+7)/8;
        const size_t framesize = pitch*height*pelsize;
        const int mid = 1<<(bpp-1);
        switch (pelsize) {
            case 1:
                memset(plane[1], mid, framesize/hsub/vsub);
                memset(plane[2], mid, framesize/hsub/vsub);
                break;

            case 2:
                for (int i=0; i<pitch*height/hsub/vsub; i++) {
                    ((unsigned short *)plane[1])[i] = mid;
                    ((unsigned short *)plane[2])[i] = mid;
                }
                break;
        }
    }
}

/*
 * rvimage translation functions
 */

/* copy image */
void rvimage::copy(const struct rvimage &src)
{
    memcpy(plane[0], src.plane[0], size);
}

/* copy image in two dimensions with boundary clipping */
void rvimage::copy(const struct rvimage &src, int dstx, int dsty, int srcx, int srcy, int width, int height)
{
    int i;

    // TODO optimise for source and this of same size.

    /* clip copy area */
    int lines = mmin(mmin(height, src.height-srcy), this->height-dsty);
    int pels  = mmin(mmin(width,  src.width -srcx), this->width -dstx);

    //rvmessage("copying %dx%d from %d,%d ->%d,%d", pels, lines, srcx, srcy, dstx, dsty);

    /* copy luma */
    for (i=0; i<lines; i++)
        memcpy(plane[0]+(dsty+i)*pitch+dstx, src.plane[0]+(srcy+i)*src.pitch+srcx, pels);

    /* shift for subsampled chroma */
    lines /= vsub;
    pels  /= hsub;
    dstx  /= hsub;
    dsty  /= vsub;
    srcx  /= src.hsub;
    srcy  /= src.vsub;

    /* copy chroma */
    for (i=0; i<lines; i++)
        memcpy(plane[1]+(dsty+i)*pitch/hsub+dstx, src.plane[1]+(srcy+i)*src.pitch/src.hsub+srcx, pels);
    for (i=0; i<lines; i++)
        memcpy(plane[2]+(dsty+i)*pitch/hsub+dstx, src.plane[2]+(srcy+i)*src.pitch/src.hsub+srcx, pels);
}

void rvimage::copy_luma(const struct rvimage &src, int dstx, int dsty, int srcx, int srcy, int width, int height)
{
    int i;

    /* clip copy area */
    int lines = mmin(mmin(height, src.height-srcy), this->height-dsty);
    int pels  = mmin(mmin(width,  src.width -srcx), this->width -dstx);

    /* copy luma */
    for (i=0; i<lines; i++)
        memcpy(plane[0]+(dsty+i)*pitch+dstx, src.plane[0]+(srcy+i)*src.pitch+srcx, pels);
}

void rvimage::copy_chroma(const struct rvimage &src, int dstx, int dsty, int srcx, int srcy, int width, int height)
{
    int i;

    /* clip copy area */
    int lines = mmin(mmin(height, src.height-srcy), this->height-dsty);
    int pels  = mmin(mmin(width,  src.width -srcx), this->width -dstx);

    /* shift for subsampled chroma */
    lines /= vsub;
    pels  /= hsub;
    dstx  /= hsub;
    dsty  /= vsub;
    srcx  /= src.hsub;
    srcy  /= src.vsub;

    /* copy chroma */
    for (i=0; i<lines; i++)
        memcpy(plane[1]+(dsty+i)*pitch/hsub+dstx, src.plane[1]+(srcy+i)*src.pitch/src.hsub+srcx, pels);
    for (i=0; i<lines; i++)
        memcpy(plane[2]+(dsty+i)*pitch/hsub+dstx, src.plane[2]+(srcy+i)*src.pitch/src.hsub+srcx, pels);
}

static void shift_plane(unsigned char *dst, unsigned char *src, int width, int pitch, int height, int xshift, int yshift)
{
    if (yshift<0) {
        /* shift up */
        int y, ypos = -yshift;
        for (y=0; y<height+yshift; y++, ypos++) {
            if (xshift<0)
                /* shift left */
                memcpy(dst+y*pitch, src+ypos*pitch-xshift, width+xshift);
            else
                /* shift right */
                memcpy(dst+y*pitch+xshift, src+ypos*pitch, width-xshift);
        }
    } else {
        /* shift down */
        int y, ypos = 0;
        for (y=yshift; y<height; y++, ypos++) {
            if (xshift<0)
                /* shift left */
                memcpy(dst+y*pitch, src+ypos*pitch-xshift, width+xshift);
            else
                /* shift right */
                memcpy(dst+y*pitch+xshift, src+ypos*pitch, width-xshift);
        }
    }
}

/*
 * rvimage drawing primitives
 */

/* this is for luma only */
void rvimage::brighten_image(int x, int y, int w, int h, const int offset)
{
    int i, j;
    unsigned char *p;

    p = plane[0] + y*pitch + x;
    for (i=0; i<h; i++) {
        for (j=0; j<w; j++)
            *(p+j) = clip(*(p+j)+offset, 0, 255);
        p += pitch;
    }
}

/* this is for chroma only */
void rvimage::shade_image(int x, int y, int w, int h, const int colour)
{
    int i;
    unsigned char *p;

    const int u = (colour>>8) & 0xff;
    const int v = (colour>>0) & 0xff;

    /* coordinates are given in luma pel values */
    x /= hsub;
    w /= hsub;
    y /= vsub;
    h /= vsub;

    p = plane[1] + y*pitch/hsub + x;
    for (i=0; i<h; i++) {
        memset(p, u, w);
        p += pitch/hsub;
    }

    p = plane[2] + y*pitch/hsub + x;
    for (i=0; i<h; i++) {
        memset(p, v, w);
        p += pitch/hsub;
    }
}

void rvimage::draw_line(int x, int y, int dx, int dy, const int colour)
{
    const int l = (colour>>16) & 0xff;
    const int u = (colour>>8)  & 0xff;
    const int v = (colour>>0)  & 0xff;

    int i;

    int n = y*pitch + x;
    int m = y*pitch/4 + x/2;
    if (dx==0) {
        int d = dy<0? -pitch : pitch;
        for (i=0; i<abs(dy); i++) {
            plane[0][n] = l;
            plane[1][m] = u;
            plane[2][m] = v;
            n += d;
            m += d/2;
        }
    } else if (dy==0) {
        int d = dx<0? -1 : 1;
        for (i=0; i<abs(dx); i++) {
            plane[0][n] = l;
            plane[1][m] = u;
            plane[2][m] = v;
            n += d;
            if (i&1)
                m += d;
        }
    }
}

void rvimage::draw_dot(int x, int y, const int colour)
{
    const int l = (colour>>16) & 0xff;
    //const int u = (colour>>8)  & 0xff;
    //const int v = (colour>>0)  & 0xff;

    plane[0][y*pitch + x] = l;
}

void rvimage::draw_box(int x, int y, int dx, int dy, const int colour)
{
    const int l = (colour>>16) & 0xff;
    //const int u = (colour>>8)  & 0xff;
    //const int v = (colour>>0)  & 0xff;

    int i, j;

    /* top line */
    int n = y*pitch + x;
    for (i=0; i<dx; i++)
        plane[0][n++] = l;
    /* bottom line */
    n = (y+dy)*pitch + x;
    for (i=0; i<dx; i++)
        plane[0][n++] = l;
    /* side lines */
    n = (y+1)*pitch + x;
    for (j=1; j<dy; j++) {
        plane[0][n] = l;
        plane[0][n+dx] = l;
        n += pitch;
    }
}

void rvimage::draw_half_box(int x, int y, int dx, int dy, const int colour)
{
    const int l = (colour>>16) & 0xff;
    //const int u = (colour>>8)  & 0xff;
    //const int v = (colour>>0)  & 0xff;

    int i, j;

    /* top line */
    int n = y*pitch + x;
    for (i=0; i<dx; i++)
        plane[0][n++] = l;
    /* side line */
    n = (y+1)*pitch + x;
    for (j=1; j<dy; j++) {
        plane[0][n] = l;
        n += pitch;
    }
}

void rvimage::draw_4x4box(int x, int y, int colour)
{
    int i, j;
    for (j=0; j<4; j++)
        for (i=0; i<4; i++)
            draw_half_box(x+i*4, y+j*4, 4, 4, colour);
}

/*
 * rvimage image manipulation
 */
void rvimage::crop(const rvimage &src, int xoff, int yoff)
{
    /* sanity check */
    if (width==0 || height==0) {
        rvmessage("cannot crop into an uninitialised image");
        return;
    }

    /*
     * write cropped luma plane
     */

    /* write leading blank lines */
    int bytes, p=0;
    int lines = mmax(-yoff, 0);
    if (lines>0) {
        bytes = lines*width;
        memset(plane[0]+p, 0x00, bytes);
        p += bytes;
    }

    /* write data lines */
    int i;
    for (i=0; i<mmin(height-lines, src.height-mmax(yoff,0)); i++) {
        int offset = mmax(yoff,0)*src.width + i*src.width + mmax(xoff, 0);
        int length = width;

        /* write leading blank  */
        bytes = -xoff;
        if (bytes>0) {
            memset(plane[0]+p, 0x00, bytes);
            length -= bytes;
            p += bytes;
        }

        /* write cropped line */
        bytes = mmin(length, src.width-mmax(xoff,0));
        if (bytes>0) {
            memcpy(plane[0]+p, src.plane[0]+offset, bytes);
            length -= bytes;
            p += bytes;
        }

        /* write trailing blank */
        assert(length>=0);
        bytes = length;
        if (bytes>0) {
            memset(plane[0]+p, 0x00, bytes);
            p += bytes;
        }
    }

    /* write trailing blank lines */
    lines = height-lines-i;
    if (lines>0) {
        bytes = lines*width;
        memset(plane[0]+p, 0x00, bytes);
        p += bytes;
    }

    /*
     * write cropped chroma blue plane
     */
    p = 0;

    /* write leading blank lines */
    lines = mmax(-yoff/vsub, 0);
    if (lines>0) {
        bytes = lines*width/hsub;
        memset(plane[1]+p, 0x80, bytes);
        p += bytes;
    }

    /* write data lines */
    for (i=0; i<mmin(height/vsub-lines, src.height/vsub-mmax(yoff/vsub,0)); i++) {
        int offset = mmax(yoff/vsub,0)*src.width/hsub + i*src.width/hsub + mmax(xoff/hsub, 0);
        int length = width/hsub;

        /* write leading blank  */
        bytes = -xoff/hsub;
        if (bytes>0) {
            memset(plane[1]+p, 0x80, bytes);
            length -= bytes;
            p += bytes;
        }

        /* write cropped line */
        bytes = mmin(length, src.width/hsub-mmax(xoff/hsub,0));
        if (bytes>0) {
            memcpy(plane[1]+p, src.plane[1]+offset, bytes);
            length -= bytes;
            p += bytes;
        }

        /* write trailing blank */
        assert(length>=0);
        bytes = length;
        if (bytes>0) {
            memset(plane[1]+p, 0x80, bytes);
            p += bytes;
        }
    }

    /* write trailing blank lines */
    lines = height/vsub-lines-i;
    if (lines>0) {
        bytes = lines*width/hsub;
        memset(plane[1]+p, 0x80, bytes);
        p += bytes;
    }

    /*
     * write cropped chroma red plane
     */
    p = 0;

    /* write leading blank lines */
    lines = mmax(-yoff/vsub, 0);
    if (lines>0) {
        bytes = lines*width/hsub;
        memset(plane[2]+p, 0x80, bytes);
        p += bytes;
    }

    /* write data lines */
    for (i=0; i<mmin(height/vsub-lines, src.height/vsub-mmax(yoff/vsub,0)); i++) {
        int offset = mmax(yoff/vsub,0)*src.width/hsub + i*src.width/hsub + mmax(xoff/hsub, 0);
        int length = width/hsub;

        /* write leading blank  */
        bytes = -xoff/hsub;
        if (bytes>0) {
            memset(plane[2]+p, 0x80, bytes);
            length -= bytes;
            p += bytes;
        }

        /* write cropped line */
        bytes = mmin(length, src.width/hsub-mmax(xoff/hsub,0));
        if (bytes>0) {
            memcpy(plane[2]+p, src.plane[2]+offset, bytes);
            length -= bytes;
            p += bytes;
        }

        /* write trailing blank */
        assert(length>=0);
        bytes = length;
        if (bytes>0) {
            memset(plane[2]+p, 0x80, bytes);
            p += bytes;
        }
    }

    /* write trailing blank lines */
    lines = height/vsub-lines-i;
    if (lines>0) {
        bytes = lines*width/hsub;
        memset(plane[2]+p, 0x80, bytes);
        p += bytes;
    }
}

void rvimage::split(const rvimage &src, fourcc_t fourcc=I420)
{
    int j;
    unsigned char *topfield, *botfield;
    const unsigned char *p = src.plane[0];

    /* sanity check */
    if (width==0 || height==0) {
        rvmessage("cannot split into an uninitialised image");
        return;
    }

    // TODO implement fourcc properly in rvimage, instead of this cheap workaround.
    //const fourcc_t fourcc = I420;

    switch (fourcc) {
        case I420:
        case YU12:
        case IYUV:
        case I422:
        case YU16:
            topfield = plane[0];
            botfield = plane[0] + width*height/2;

            /* split luma into fields */
            for (j=0; j<height; j+=2) {
                memcpy(topfield, p, width);
                topfield += width;
                p += src.width;
                memcpy(botfield, p, width);
                botfield += width;
                p += src.width;
            }

            /* split chroma into fields */
            topfield = plane[1];
            botfield = plane[1] + width/hsub*height/vsub/2;
            for (j=0; j<height/vsub; j+=2) {
                memcpy(topfield, p, width/hsub);
                topfield += width/hsub;
                p += src.width/src.hsub;
                memcpy(botfield, p, width/hsub);
                botfield += width/hsub;
                p += src.width/src.hsub;
            }
            topfield = plane[2];
            botfield = plane[2] + width/hsub*height/vsub/2;
            for (j=0; j<height/vsub; j+=2) {
                memcpy(topfield, p, width/hsub);
                topfield += width/hsub;
                p += src.width/src.hsub;
                memcpy(botfield, p, width/hsub);
                botfield += width/hsub;
                p += src.width/src.hsub;
            }
            break;

        case UYVY:
        case TWOVUY:
        case VYUY:
        case YUY2:
            topfield = plane[0];
            botfield = plane[0] + width*height;

            for (j=0; j<height; j+=2) {
                memcpy(topfield, p, width*2);
                topfield += width*2;
                p += src.width*2;
                memcpy(botfield, p, width*2);
                botfield += width*2;
                p += src.width*2;
            }
            break;

        default:
            rvexit("unsupported pixel format in split_frame(): %s", fourccname(fourcc));
    }
}


void rvimage::shift(const struct rvimage &src, int xshift, int yshift)
{
    /* sanity check */
    if (width==0 || height==0) {
        rvmessage("cannot shift into an uninitialised image");
        return;
    }

    /* check for nop */
    if (xshift==0 && yshift==0)
        return copy(src);

    /* clear image for borders after shift */
    clear();

    /* copy image data with shift */
    shift_plane(plane[0], src.plane[0], width, pitch, height, xshift, yshift);
    shift_plane(plane[1], src.plane[1], width/hsub, pitch/hsub, height/vsub, xshift/hsub, yshift/vsub);
    shift_plane(plane[2], src.plane[2], width/hsub, pitch/hsub, height/vsub, xshift/hsub, yshift/vsub);
}

/*
 * rvimage overlay drawing functions
 */

void draw_m2v_overlay(struct rvimage &image, struct rvm2vpicture *toppicture, struct rvm2vpicture *botpicture)
{
    struct rvm2vpicture *picture = toppicture;
    int num_macros = image.width*image.height/256;
    int num_pics = 1;

    /* sanity check */
    if (toppicture==NULL) {
        image.clear_chroma();
        return;
    }

    /* show field pictures as over and under */
    if (field_picture(picture)) {
        num_macros /= 2;
        num_pics *=2;
        if (botpicture==NULL) {
            image.clear_chroma();
            return;
        }
    }

    /* loop over macroblocks in frame */
    int i, n;
    for (i=0; i<num_pics; i++) {
        for (n=0; n<num_macros; n++) {

            /* calculate coordinates of macroblock */
            int x = (n % (image.width/16)) * 16;
            int y = (n / (image.width/16)) * 16 + i*image.height/2;

            /* shade based on macroblock type */
            if (macroblock_intra(picture->macro[n].type))
                image.shade_image(x, y, 16, 16, RED);
            else if (picture->macro[n].skipped)
                image.shade_image(x, y, 16, 16, GRAY);
            else if (macroblock_motion_forward(picture->macro[n].type) && macroblock_motion_backward(picture->macro[n].type)) {
                if (macroblock_pattern(picture->macro[n].type))
                    image.shade_image(x, y, 16, 16, ORANGE);
                else
                    image.shade_image(x, y, 16, 16, LIGHT_ORANGE);
            }
            else if (macroblock_motion_forward(picture->macro[n].type)) {
                if (macroblock_pattern(picture->macro[n].type))
                    image.shade_image(x, y, 16, 16, BLUE);
                else
                    image.shade_image(x, y, 16, 16, LIGHT_BLUE);
            }
            else if (macroblock_motion_backward(picture->macro[n].type)) {
                if (macroblock_pattern(picture->macro[n].type))
                    image.shade_image(x, y, 16, 16, YELLOW);
                else
                    image.shade_image(x, y, 16, 16, LIGHT_YELLOW);
            }
            else if (macroblock_pattern(picture->macro[n].type))
                image.shade_image(x, y, 16, 16, GREEN);
            else
                image.shade_image(x, y, 16, 16, BLACK);

            /* add a left hand vertical line to mark errors */
            if (macroblock_error(picture->macro[n].type))
                image.draw_line(x, y, 0, 16, RED);

            /* add a right hand vertical line for zero stuffing */
            if (picture->macro[n].stuff_bits)
                image.draw_line(x+15, y, 0, 16, CYAN);
        }
        picture = botpicture;
    }
}

void draw_m2v_qpmap(struct rvimage &image, struct rvm2vpicture *toppicture, struct rvm2vpicture *botpicture)
{
    struct rvm2vpicture *picture = toppicture;
    int num_macros = image.width*image.height/256;
    int num_pics = 1;

    /* sanity check */
    if (toppicture==NULL) {
        return;
    }

    /* show field pictures as over and under */
    if (field_picture(picture)) {
        num_macros /= 2;
        num_pics *=2;
        if (botpicture==NULL) {
            return;
        }
    }

    /* find mean qp */
    int i, n, qpsum=0;
    for (i=0; i<num_pics; i++) {
        for (n=0; n<num_macros; n++) {
            qpsum += picture->macro[n].quantiser_scale_code;
        }
        picture = botpicture;
    }
    double qpmean = (double)qpsum / (double)(num_macros*num_pics);


    /* loop over macroblocks in frame */
    picture = toppicture;
    for (i=0; i<num_pics; i++) {
        for (n=0; n<num_macros; n++) {

            /* calculate coordinates of macroblock */
            int x = (n % (image.width/16)) * 16;
            int y = (n / (image.width/16)) * 16 + i*image.height/2;

            /* adjust brighness based on quant code compared to mean */
            image.brighten_image(x, y, 16, 16, (qpmean-picture->macro[n].quantiser_scale_code)*40.0);

        }
        picture = botpicture;
    }
}

/* draw a basic colour and lines macroblock information overlay */
void draw_h264_overlay(struct rvimage &image, struct rv264macro macro[], int mbaff, int field_pic_flag, int bottom_field_flag)
{
    int num_macros = image.width*image.height/(field_pic_flag? 512: 256);

    /* loop over macroblocks in frame */
    int n, last_slice=-1;
    for (n=0; n<num_macros; n++) {
        int i, colour;

        /* calculate coordinates of macroblock */
        int x, y;
        if (mbaff) {
            /* first calculate coordinates of macroblock pair */
            x = ((n/2) % (image.width/16));
            y = (n / (image.width/16));
            /* then fix bottom bits of y */
            if (n&1)
                y |= 1;
            else
                y &= ~1;
            /* then scale to pel units */
            x *= 16;
            y *= 16;
        } else {
            x = (n % (image.width/16)) * 16;
            y = (n / (image.width/16)) * 16 + bottom_field_flag * (image.height/32) * 16;
        }

        /* shade based on macroblock type */
        switch (macro[n].mb_type) {
            case I_NxN:
                for (i=0; i<4; i++) {
                    if (macro[n].coded_block_pattern & (1<<i))
                        colour = /*macro[n].transform_size_8x8_flag? PURPLE :*/ RED;
                    else
                        colour = BLACK;
                    image.shade_image(x+((i&1)<<3), y+((i&2)<<2), 8, 8, colour);
                }
                break;

            case I_PCM:
                image.shade_image(x, y, 16, 16, CYAN);
                break;

            case I_16x16_0_0_0:
            case I_16x16_0_1_0:
            case I_16x16_0_2_0:
                image.shade_image(x, y, 16, 16, LIGHT_ORANGE);
                //image.draw_line(x+7, y+7, 0, -8, BLACK);
                break;

            case I_16x16_0_0_1:
            case I_16x16_0_1_1:
            case I_16x16_0_2_1:
                image.shade_image(x, y, 16, 16, ORANGE);
                //image.draw_line(x+7, y+7, 0, -8, BLACK);
                break;

            case I_16x16_1_0_0:
            case I_16x16_1_1_0:
            case I_16x16_1_2_0:
                image.shade_image(x, y, 16, 16, LIGHT_ORANGE);
                //image.draw_line(x+7, y+7, -8, 0, BLACK);
                break;

            case I_16x16_1_0_1:
            case I_16x16_1_1_1:
            case I_16x16_1_2_1:
                image.shade_image(x, y, 16, 16, ORANGE);
                //image.draw_line(x+7, y+7, -8, 0, BLACK);
                break;

            case I_16x16_2_0_0:
            case I_16x16_2_1_0:
            case I_16x16_2_2_0:
                image.shade_image(x, y, 16, 16, LIGHT_ORANGE);
                //draw_dot(&image, x+1, y+1, BLACK);
                break;

            case I_16x16_2_0_1:
            case I_16x16_2_1_1:
            case I_16x16_2_2_1:
                image.shade_image(x, y, 16, 16, ORANGE);
                //draw_dot(&image, x+1, y+1, BLACK);
                break;

            case I_16x16_3_0_0:
            case I_16x16_3_1_0:
            case I_16x16_3_2_0:
                image.shade_image(x, y, 16, 16, LIGHT_ORANGE);
                //image.draw_line(x+1, y+1, 14, 0, BLACK);
                //image.draw_line(x+1, y+1, 0, 14, BLACK);
                break;

            case I_16x16_3_0_1:
            case I_16x16_3_1_1:
            case I_16x16_3_2_1:
                image.shade_image(x, y, 16, 16, ORANGE);
                //image.draw_line(x+1, y+1, 14, 0, BLACK);
                //image.draw_line(x+1, y+1, 0, 14, BLACK);
                break;

            case P_L0_16x16:
            case P_L0_L0_16x8:
            case P_L0_L0_8x16:
            case P_8x8:
            case P_8x8ref0:
                image.shade_image(x,   y,   8, 8, macro[n].coded_block_pattern & 1 ? BLUE : LIGHT_BLUE);
                image.shade_image(x+8, y,   8, 8, macro[n].coded_block_pattern & 2 ? BLUE : LIGHT_BLUE);
                image.shade_image(x,   y+8, 8, 8, macro[n].coded_block_pattern & 4 ? BLUE : LIGHT_BLUE);
                image.shade_image(x+8, y+8, 8, 8, macro[n].coded_block_pattern & 8 ? BLUE : LIGHT_BLUE);
                break;

            case B_Direct_16x16:
                //image.shade_image(x, y, 16, 16, PURPLE);
                image.shade_image(x,   y,   8, 8, macro[n].coded_block_pattern & 1 ? PURPLE : LIGHT_PURPLE);
                image.shade_image(x+8, y,   8, 8, macro[n].coded_block_pattern & 2 ? PURPLE : LIGHT_PURPLE);
                image.shade_image(x,   y+8, 8, 8, macro[n].coded_block_pattern & 4 ? PURPLE : LIGHT_PURPLE);
                image.shade_image(x+8, y+8, 8, 8, macro[n].coded_block_pattern & 8 ? PURPLE : LIGHT_PURPLE);
                break;

            case B_L0_16x16:
            case B_L1_16x16:
            case B_Bi_16x16:
            case B_L0_L0_16x8:
            case B_L0_L0_8x16:
            case B_L1_L1_16x8:
            case B_L1_L1_8x16:
            case B_L0_L1_16x8:
            case B_L0_L1_8x16:
            case B_L1_L0_16x8:
            case B_L1_L0_8x16:
            case B_L0_Bi_16x8:
            case B_L0_Bi_8x16:
            case B_L1_Bi_16x8:
            case B_L1_Bi_8x16:
            case B_Bi_L0_16x8:
            case B_Bi_L0_8x16:
            case B_Bi_L1_16x8:
            case B_Bi_L1_8x16:
            case B_Bi_Bi_16x8:
            case B_Bi_Bi_8x16:
                image.shade_image(x,   y,   8, 8, macro[n].coded_block_pattern & 1 ? YELLOW : LIGHT_YELLOW);
                image.shade_image(x+8, y,   8, 8, macro[n].coded_block_pattern & 2 ? YELLOW : LIGHT_YELLOW);
                image.shade_image(x,   y+8, 8, 8, macro[n].coded_block_pattern & 4 ? YELLOW : LIGHT_YELLOW);
                image.shade_image(x+8, y+8, 8, 8, macro[n].coded_block_pattern & 8 ? YELLOW : LIGHT_YELLOW);
                break;

            case B_8x8:
                for (i=0; i<4; i++) {
                    if (macro[n].sub_mb_type[i] == B_Direct_8x8)
                        //if (macro[n].sh->direct_spatial_mv_pred_flag)
                        //    colour = macro[n].coded_block_pattern & (1<<i) ? GREEN : GREEN;
                        //else
                            colour = macro[n].coded_block_pattern & (1<<i) ? PURPLE : LIGHT_PURPLE;
                    else
                        colour = macro[n].coded_block_pattern & (1<<i) ? YELLOW : LIGHT_YELLOW;
                    image.shade_image(x+((i&1)<<3), y+((i&2)<<2), 8, 8, colour);
                }
                break;

            case P_Skip:
            case B_Skip:
                image.shade_image(x, y, 16, 16, BLACK);
                break;

            default:
                break;
        }

        /* add a left hand vertical line to mark the start of slices */
        if (macro[n].slice_num != last_slice)
            image.draw_line(x, y, 0, 16, GREEN);
        if (!mbaff || n&1) /* draw vertical line in both macroblocks of a pair */
            last_slice = macro[n].slice_num;

    }
}

void draw_265_overlay_pb(struct rvimage &image, const struct rvhevcpicture *pic, int x, int y, int w, int h)
{
    const hevc_motion& mvi = pic->pu.get(x, y)->motion;
    int colour = mvi.predFlag[0]&&mvi.predFlag[1]? YELLOW : mvi.predFlag[1]? ORANGE : mvi.predFlag[0]? BLUE : GREEN; /* GREEN shouldn't happen */
    image.shade_image(x, y, w, h, colour);
}

void draw_265_overlay(struct rvimage &image, const struct rvhevcpicture *pic)
{
    /* sanity check */
    if (pic==NULL) {
        image.clear_chroma();
        return;
    }

    const hevc_seq_parameter_set* sps = pic->sh.pps->sps;
    int minCbSize = sps->MinCbSizeY;

    /* loop over minimum size coding blocks in frame */
    for (int ycb=0; ycb<sps->PicHeightInMinCbsY; ycb++) {
        for (int xcb=0; xcb<sps->PicWidthInMinCbsY; xcb++) {
            const struct rvhevccodingunit *cu = pic->cu.get_in_units(xcb, ycb);
            int log2CbSize = cu->log2CbSize;
            if (log2CbSize==0) {
                continue;
            }
            int CbSize = 1<<log2CbSize;
            int HalfCbSize = 1<<(log2CbSize-1);

            /* calculate coordinates of coding block */
            int x = xcb*minCbSize;
            int y = ycb*minCbSize;

            /* shade based on macroblock type */
            if (cu->pred_mode==HEVC_MODE_INTRA) {
                if (cu->pcm_flag)
                    image.shade_image(x, y, CbSize, CbSize, CYAN);
                /* FIXME test this with a suitable bitstream and add to inter */
                else if (cu->transquant_bypass)
                    image.shade_image(x, y, CbSize, CbSize, PURPLE);
                else
                    image.shade_image(x, y, CbSize, CbSize, RED);
            } else if (cu->pred_mode==HEVC_MODE_SKIP)
                image.shade_image(x, y, CbSize, CbSize, GRAY);
            else if (cu->pred_mode==HEVC_MODE_INTER) {
                hevc_part_mode_t partMode = cu->PartMode;

                switch (partMode) {
                    case HEVC_PART_2Nx2N:
                        draw_265_overlay_pb(image, pic, x           , y,           CbSize,    CbSize);
                        break;
                    case HEVC_PART_NxN:
                        draw_265_overlay_pb(image, pic, x           , y,           CbSize/2,  CbSize/2);
                        draw_265_overlay_pb(image, pic, x+HalfCbSize, y,           CbSize/2,  CbSize/2);
                        draw_265_overlay_pb(image, pic, x           , y+HalfCbSize,CbSize/2,  CbSize/2);
                        draw_265_overlay_pb(image, pic, x+HalfCbSize, y+HalfCbSize,CbSize/2,  CbSize/2);
                        break;
                    case HEVC_PART_2NxN:
                        draw_265_overlay_pb(image, pic, x           , y,           CbSize  ,  CbSize/2);
                        draw_265_overlay_pb(image, pic, x           , y+HalfCbSize,CbSize  ,  CbSize/2);
                        break;
                    case HEVC_PART_Nx2N:
                        draw_265_overlay_pb(image, pic, x           , y,           CbSize/2,  CbSize);
                        draw_265_overlay_pb(image, pic, x+HalfCbSize, y,           CbSize/2,  CbSize);
                        break;
                    case HEVC_PART_2NxnU:
                        draw_265_overlay_pb(image, pic, x           , y,           CbSize  ,  CbSize/4);
                        draw_265_overlay_pb(image, pic, x           , y+CbSize/4  ,CbSize  ,  CbSize*3/4);
                        break;
                    case HEVC_PART_2NxnD:
                        draw_265_overlay_pb(image, pic, x           , y,           CbSize    ,CbSize*3/4);
                        draw_265_overlay_pb(image, pic, x           , y+CbSize*3/4,CbSize    ,CbSize/4);
                        break;
                    case HEVC_PART_nLx2N:
                        draw_265_overlay_pb(image, pic, x           , y,           CbSize/4  ,CbSize);
                        draw_265_overlay_pb(image, pic, x+CbSize/4  , y,           CbSize*3/4,CbSize);
                        break;
                    case HEVC_PART_nRx2N:
                        draw_265_overlay_pb(image, pic, x           , y,           CbSize*3/4,CbSize);
                        draw_265_overlay_pb(image, pic, x+CbSize*3/4, y,           CbSize/4  ,CbSize);
                        break;
                    default:
                        assert(false);
                        break;
                }
#if 0
            else if (macroblock_motion_backward(picture->macro[n].type)) {
                if (macroblock_pattern(picture->macro[n].type))
                    image.shade_image(x, y, CbSize, CbSize, YELLOW);
                else
                    image.shade_image(x, y, CbSize, CbSize, LIGHT_YELLOW);
            }
            else if (macroblock_pattern(picture->macro[n].type))
                image.shade_image(x, y, CbSize, CbSize, GREEN);
#endif
            } else
                image.shade_image(x, y, CbSize, CbSize, BLACK);

            /* add a left hand vertical line to mark errors */
            //if (macroblock_error(picture->macro[n].type))
            //    image.draw_line(x, y, 0, 16, RED);

            /* add a right hand vertical line for zero stuffing */
            //if (picture->macro[n].stuff_bits)
            //    image.draw_line(x+15, y, 0, 16, CYAN);
        }
    }
}
