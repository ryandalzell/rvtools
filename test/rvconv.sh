#!/bin/bash

# standard test functions
fail () { echo $1; exit 1; }
md5 () { line=( $(md5sum $1) ); echo ${line[0]}; }

# test parameters
tmpdir=tmp
image=colourbars

# expected results
declare -A res
res[colourbars.cif]=bedc36581c052ef77dd0d9cfa4c25f1a
res[colourbars.nv12.cif]=f2ad83083df051948afee7cf4a4de611
res[colourbars.i420.cif]=bedc36581c052ef77dd0d9cfa4c25f1a
res[colourbars.nv16.cif]=59c238f5f249a3175c42a535632988c5
res[colourbars.i422.cif]=bff760c36ea960d42c8643c04fd5b88d
res[colourbars.uyvy.cif]=5fd5fe852ad5922c28fa3ccba1df801a
res[colourbars.yuy2.cif]=1f62421ebd954428a573fe6e20a8bb59

# test setup
mkdir -p $tmpdir

# first test 4:2:0 formats
fourcc=i420

# synthesize source file
./rvsynth -s cif -i ${image} -n 3 -o ${tmpdir}/${image}.cif
[[ -n ${tmpdir}/${image}.cif ]] || fail "No output file from rvsynth"
[[ $(stat -c%s ${tmpdir}/${image}.cif) == 456192 ]] || fail "Output file from rvsynth is the wrong size"
[[ $(md5 ${tmpdir}/${image}.cif) == ${res[${image}.cif]} ]] || fail "Output file from rvsynth has the wrong hash"

# convert source file into alternate format and back
for format in nv12
do
    ./rvconv -r -f $format ${tmpdir}/${image}.cif -o ${tmpdir}/${image}.${format}.cif
    [[ $(md5 ${tmpdir}/${image}.${format}.cif) == ${res[${image}.${format}.cif]} ]] || fail "File converted to $format has the wrong hash"
    ./rvconv -f $format ${tmpdir}/${image}.${format}.cif -o ${tmpdir}/${image}.${fourcc}.cif
    [[ $(md5 ${tmpdir}/${image}.${fourcc}.cif) == ${res[${image}.cif]} ]] || fail "File converted back from $format is not the same as the original"
done

# now test 4:2:2 formats
fourcc=i422

# synthesize source filej
./rvsynth -s cif -p i422 -i ${image} -n 3 -o ${tmpdir}/${image}.${fourcc}.cif
[[ -n ${tmpdir}/${image}.${fourcc}.cif ]] || fail "No output file from rvsynth"
[[ $(stat -c%s ${tmpdir}/${image}.${fourcc}.cif) == 608256 ]] || fail "Output file from rvsynth is the wrong size"
[[ $(md5 ${tmpdir}/${image}.${fourcc}.cif) == ${res[${image}.${fourcc}.cif]} ]] || fail "Output file from rvsynth has the wrong hash"

# convert source file into alternate format and back
for format in nv16 uyvy yuy2
do
    ./rvconv -r -f $format ${tmpdir}/${image}.${fourcc}.cif -o ${tmpdir}/${image}.${format}.cif
    [[ $(md5 ${tmpdir}/${image}.${format}.cif) == ${res[${image}.${format}.cif]} ]] || fail "File converted to $format has the wrong hash"
    ./rvconv -f $format ${tmpdir}/${image}.${format}.cif -o ${tmpdir}/${image}.${fourcc}.cif
    [[ $(md5 ${tmpdir}/${image}.${fourcc}.cif) == ${res[${image}.${fourcc}.cif]} ]] || fail "File converted back from $format is not the same as the original"
done

# tidy up
rm -r ${tmpdir}
