#!/bin/bash

# fail test if any command fails
set -e

# test parameters
tmpdir=tmp
image=colournoise

# test setup
mkdir -p $tmpdir

# synthesize source file
./rvsynth -s 720p -i ${image} -n 10 -o ${tmpdir}/${image}.720p
[[ -n ${tmpdir}/${image}.720p ]]
[[ $(stat -c%s ${tmpdir}/${image}.720p) == 13824000 ]]

# resample source file to larger size
# FIXME need to fix bicubic and kaiser in rvscale
for algo in nearest bilinear bartlett vonhann hamming blackman blackmanharris blackmannuttall
do
    ./rvscale -a $algo ${tmpdir}/${image}.720p -o ${tmpdir}/${image}.1080p
    [[ $(stat -c%s ${tmpdir}/${image}.1080p) == 31104000 ]]
done

# crop larger image to original size
./rvcrop -c 1280x720p+180+16 ${tmpdir}/${image}.1080p -o ${tmpdir}/${image}.720p
[[ $(stat -c%s ${tmpdir}/${image}.720p) == 13824000 ]]

# tidy up
rm -r ${tmpdir}
