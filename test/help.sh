#!/bin/sh

# fail test if any command fails
set -e

executables="rv10bit rv8x4 rvbpp rvcheck rvclean rvconv rvcrop rvdecode rvdeint rvdemux rvdenoise rvencap rverrors rvfield rvinfo rvnup rvplay rvquality rvreorder rvrerate rvscale rvsplice rvstream rvsynth rvtrim rvtsmon rvtxt rvvis"

for i in $executables; do
    ./${i} --help
done
