#!/bin/bash
# Shell script to regression test 'rvdecode'
# Copyright � Ryan Dalzell 2007,2011

# simple implementation of colour variables
nocolour="\033[0m"
red="\033[0;31m"
green="\033[0;32m"
yellow="\033[1;33m"
white="\033[0;37m"

tempfile=rvtest.$$

# test h.261 decode

# test mpeg2 decode
function test_m2v
{
    echo -n "${1:t}: "
    info=$(./rvinfo -b $1)
    dims=$(echo $info | awk '{print $2;}' | sed -e 's/@.*$//;')
    mssgm2vd -b $1 -r -f -o6 $tempfile.$dims
    if ./rvdecode -q -r $1 | rvquality -q -t 1 $tempfile.$dims; then
        echo ${white}passed${nocolour}
    else
        echo ${red}FAILED${nocolour}
        echo hint: $info
    fi
    rm $tempfile.$dims
}

if false; then
    echo -e ${yellow}test mpeg2 progressive bitstreams @ $(date)${nocolour}
    for i in /home/codec/m2v/progressive/*.m2v; do
        test_m2v $i
    done
    echo -e ${yellow}test mpeg2 simple profile @ $(date)${nocolour}
    for i in /home/codec/m2v/simple-profile/*.m2v; do
        test_m2v $i
    done
    echo -e ${yellow}test mpeg2 interlaced bitstreams @ $(date)${nocolour}
    for i in /home/codec/m2v/interlaced/*.m2v; do
        test_m2v $i
    done
    echo -e ${yellow}test mpeg2 main profile @ $(date)${nocolour}
    for i in /home/codec/m2v/main-profile/*.m2v; do
        test_m2v $i
    done
    echo -e ${yellow}test mpeg2 422 profile @ $(date)${nocolour}
    for i in /home/codec/m2v/422-profile/*.m2v; do
        test_m2v $i
    done
fi

# test h.264 decode
function test_264
{
    echo -n "${1:t}: "
    info=$(./rvinfo -b $1)
    dims=$(echo $info | awk '{print $2;}' | sed -e 's/@.*$//;')
    jm264d -p InputFile="$1" -p OutputFile="$tempfile.jm.$dims" -p WriteUV=0 >/dev/null
    ./rvdecode -q $1 -o "$tempfile.rv.$dims" 2>/dev/null
    if cmp $tempfile.rv.$dims $tempfile.jm.$dims; then
        echo -e ${white}passed${nocolour}
    else
        echo -e ${red}FAILED${nocolour}
        echo hint:
        ./rvinfo $1 $tempfile.jm.$dims $tempfile.rv.$dims
    fi
    rm $tempfile.*.$dims
}

if true; then
    echo -e ${yellow}test h.264 cavlc bitstreams @ $(date)${nocolour}
    for i in /home/codec/h.264/avc/NL-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/BA-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/MQ-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/SL-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/SQ-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/FM-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/CI-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/{FC,AUD,MIDR,NRF,MPS}-1.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/BS-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/PCM-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/MR-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/WP-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/FI-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/PA-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/MA-?.264; do
        test_264 $i
    done
    #for i in /home/codec/h.264/avc/SP-?.264; do
    #    test_264 $i
    #done
    for i in /home/codec/h.264/avc/LS-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/SE-?.264; do
        test_264 $i
    done
    echo -e ${yellow}test h.264 cabac bitstreams @ $(date)${nocolour}
    for i in /home/codec/h.264/avc/CANL-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/CABA-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/CAIN-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/CAQP-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/CASL-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/CAPCM-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/CAMR-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/CAWP-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/CAFI-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/CAPA-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/CAMA-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/CAPAMA-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/CAMV-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/avc/CVCANLMA-?.264; do
        test_264 $i
    done
    echo -e ${yellow}test h.264 freext bitstreams @ $(date)${nocolour}
    for i in /home/codec/h.264/fre/H-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/fre/H-??.264; do
        test_264 $i
    done
    #for i in /home/codec/h.264/fre/H10-?.264; do
    #    test_264 $i
    #done
    for i in /home/codec/h.264/fre/H422-?.264; do
        test_264 $i
    done
    for i in /home/codec/h.264/fre/H422-??.264; do
        test_264 $i
    done
    # Allegro test bitstreams
    for i in /home/codec/h.264/allegro/Allegro*.264; do
        test_264 $i
    done
fi

# test hevc decode
function test_265
{
    echo -n "${1:t}: "
    info=$(./rvinfo -b $1)
    dims=$(echo $info | awk '{print $2;}' | sed -e 's/@.*$//;')
    hm265d -b "$1" -o "$tempfile.hm.$dims" >/dev/null
    ./rvdecode -q "$1" -o "$tempfile.rv.$dims"
    if cmp $tempfile.rv.$dims $tempfile.hm.$dims; then
        echo -e ${white}passed${nocolour}
    else
        echo -e ${red}FAILED${nocolour}
        echo hint:
        ./rvinfo $1 $tempfile.hm.$dims $tempfile.rv.$dims
    fi
    rm $tempfile.*.$dims
}

if false; then
    echo -e ${yellow}test hevc bitstreams @ $(date)${nocolour}
    for i in /home/codec/hevc/conf/*.265; do
        test_265 $i
    done
fi
