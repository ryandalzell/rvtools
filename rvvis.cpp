/*
 * Description: Visualise a data stream using a format file.
 * Last Author: $Author: ryan $
 * Last Date  : $Date: 2009-06-23 13:26:19 $
 * Revision   : $Revision: 1.25 $
 * Copyright  : (c) 2005,2014 Ryan Dalzell
 */

/*
 * This file is part of rvtools.
 *
 * rvtools is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * rvtools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rvtools; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include "getopt.h"

#include "rvutil.h"

const char *appname;

void usage(int exitcode)
{
    fprintf(stderr, "%s: visualise a data stream using a format file\n", appname);
    fprintf(stderr, "usage: %s [options] [<file>] [<file>...]\n", appname);
    fprintf(stderr, "  -f, --format        : format file\n");
    fprintf(stderr, "  -o, --output        : write output to file\n");
    fprintf(stderr, "  -q, --quiet         : decrease verbosity, can be used multiple times\n");
    fprintf(stderr, "  -v, --verbose       : increase verbosity, can be used multiple times\n");
    fprintf(stderr, "  --                  : disable argument processing\n");
    fprintf(stderr, "  -h, --help, --usage : print this usage message\n");
    exit(exitcode);
}

int main(int argc, char *argv[])
{
    FILE *filein = stdin;
    FILE *fileout = stdout;
    FILE *format = NULL;
    char *filename[RV_MAXFILES] = {0};
    char *outfile = NULL;
    int fileindex = 0;
    int numfiles = 0;
    int totbytes = 0;

    /* command line defaults */
    char *formatfile = NULL;
    int verbose = 0;

    /* get application appname */
    appname = get_basename(argv[0]);

    /* parse command line for options */
    while (1) {
        static struct option long_options[] = {
            {"format",    1, NULL, 'f'},
            {"output",    1, NULL, 'o'},
            {"quiet",     0, NULL, 'q'},
            {"verbose",   0, NULL, 'v'},
            {"usage",     0, NULL, 'h'},
            {"help",      0, NULL, 'h'},
            {NULL,        0, NULL,  0 }
        };

        int optchar = getopt_long(argc, argv, "f:o:qvh", long_options, NULL);
        if (optchar==-1)
            break;

        switch (optchar) {
            case 'f':
                formatfile = optarg;
                break;

            case 'o':
                outfile = optarg;
                break;

            case 'q':
                verbose--;
                break;

            case 'v':
                verbose++;
                break;

            case 'h':
                usage(0);
                break;

            case '?':
                exit(1);
                break;
        }
    }

    /* all non-options are input filenames */
    while (optind<argc) {
        if (numfiles < RV_MAXFILES)
            filename[numfiles++] = argv[optind++];
        else
            rvexit("more than %d input files", numfiles);
    }

    /* open formst file */
    if (formatfile)
        format = fopen(formatfile, "r");
    if (fileout==NULL)
        rverror("failed to open format file \"%s\"", formatfile);

    /* open output file */
    if (outfile)
        fileout = fopen(outfile, "wb");
    if (fileout==NULL)
        rverror("failed to open output file \"%s\"", outfile);

    /* ignore SIGPIPE on output file */
    if (signal(SIGPIPE, SIG_IGN)==SIG_ERR)
        rverror("failed to set ignore on SIGPIPE");

    /* loop over input files */
    do {
        /* open next input file, or use stdin */
        if (numfiles)
            filein = fopen(filename[fileindex], "rb");
        if (filein==NULL)
            rverror("failed to open file \"%s\"", filename[fileindex]);

        /* loop over format definition */
        while (!feof(filein) && !ferror(filein) && !ferror(fileout))
        {
            char line[1024];
            char type[64], name[256];
            int padding;

            if (fgets(line, sizeof(line), format)==NULL) {
                fseek(format, 0, SEEK_SET);
                if (fgets(line, sizeof(line), format)==NULL)
                    rverror("failed to read line from format file \"%s\"", formatfile);
            }

            /* filter blank lines and comments */
            if (line[0]=='\n' || line[0]=='#')
                continue;

            /* lookup format type */
            if (sscanf(line, "%s %s", type, name)==2) {
                if (strcmp(type, "u8")==0) {
                    int data;
                    if (fscanf(filein, "%d", &data)!=1)
                        break;

                    fprintf(fileout, "%s=%u\n", name, data);
                    totbytes++;
                }
                else if (strcmp(type, "u16")==0) {
                    int data0, data1;
                    if (fscanf(filein, "%d %d", &data0, &data1)!=2)
                        break;

                    unsigned int data = (data1<<8) | data0;
                    fprintf(fileout, "%s=%u\n", name, data);
                    totbytes+=2;
                }
            } else if (sscanf(line, "pad[%d]", &padding)==1) {
                int i, data;
                for (i=0; i<padding; i++)
                    if (fscanf(filein, "%d", &data)!=1)
                        break;
                totbytes+=padding;
            } else
                rvexit("unrecognised line in data format file: %s", line);
        }
        if (ferror(filein))
            rverror("failed to read from input");
        if (ferror(fileout) && errno!=EPIPE)
            rverror("failed to write to output");

    } while (++fileindex<numfiles && !ferror(fileout));

    if (verbose>=0)
        rvmessage("processed %d bytes", totbytes);

    return 0;
}
